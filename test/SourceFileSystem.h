#pragma once
#include <jadegit/vfs/NativeFileSystem.h>
#include <source_location>

namespace JadeGit
{
	class SourceFileSystem : public NativeFileSystem
	{
	public:
		SourceFileSystem(const std::filesystem::path& relative, const std::source_location& location = std::source_location::current()) : NativeFileSystem(std::filesystem::path(location.file_name()).parent_path() / relative, false) {}
	};
}