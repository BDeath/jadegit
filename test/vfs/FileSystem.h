#pragma once
#include <catch2/catch_test_macros.hpp>
#include <jadegit/vfs/FileSystem.h>
#include <SourceFileSystem.h>

class FileSystemFactory
{
public:
	virtual JadeGit::FileSystem& empty() = 0;
	virtual JadeGit::FileSystem& clone(const JadeGit::SourceFileSystem& src) = 0;
};

void iterator_test_scenarios(FileSystemFactory& factory);
void readonly_test_scenarios(FileSystemFactory& factory);
void writable_test_scenarios(FileSystemFactory& factory);
