#include "FileSystem.h"
#include <jadegit/vfs/MemoryFileSystem.h>

using namespace std;
using namespace JadeGit;

class MemoryFileSystemFactory : public FileSystemFactory
{
public:
	FileSystem& empty() final
	{
		fs = make_unique<MemoryFileSystem>();
		return *fs;
	}

	FileSystem& clone(const SourceFileSystem& src) final
	{
		fs = make_unique<MemoryFileSystem>(src);
		return *fs;
	}

private:
	unique_ptr<MemoryFileSystem> fs;
};

TEST_CASE("MemoryFileSystem", "[vfs]") 
{
	MemoryFileSystemFactory factory;

	iterator_test_scenarios(factory);
	writable_test_scenarios(factory);
}