#include "TestAppContext.h"

namespace JadeGit
{
	TestAppContext::TestAppContext(const TestDbContext& db) : TestAppContext(db, TEXT("RootSchema"), TEXT("RootSchemaApp"))
	{
	}

	TestAppContext::TestAppContext(const TestDbContext& db, const Character* schema, const Character* app) : Jade::AppContext(&db.node, schema, app)
	{
	}
}