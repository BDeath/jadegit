#include <Approvals.h>
#include <catch2/catch_test_macros.hpp>
#include <registry/Commit.h>
#include <registry/Repository.h>
#include <registry/Root.h>
#include <registry/Schema.h>

using namespace std;
using namespace JadeGit::Registry;

TEST_CASE("Printer.Empty", "[registry]")
{
	Root registry;

	// Check output matches expected
	ApprovalTests::Approvals::verify(registry);
}

TEST_CASE("Printer.Experimental", "[registry]")
{
	Root registry;

	registry.experimental = true;

	// Check output matches expected
	ApprovalTests::Approvals::verify(registry);
}

TEST_CASE("Printer.Platform", "[registry]")
{
	Root registry;
	registry.platform = make_unique<PlatformT>();
	registry.platform->version = "99.0.00";

	SECTION("ANSI")
	{
		registry.platform->charset = Charset_ANSI;

		ApprovalTests::Approvals::verify(registry);
	}
	SECTION("Unicode")
	{
		registry.platform->charset = Charset_Unicode;

		ApprovalTests::Approvals::verify(registry);
	}	
}

TEST_CASE("Printer.Repository", "[registry]")
{
	RepositoryT foo;
	foo.name = "foo";
	foo.origin = "http://gitmock.com/foo.git";
	foo.latest.push_back(make_commit("aaaa"));
	foo.schemas.push_back(make_schema("FooSchema"));

	SECTION("Single")
	{
		ApprovalTests::Approvals::verify(foo);
	}
	SECTION("Multiple")
	{
		Root registry;
		registry.add(foo);

		RepositoryT bar;
		bar.name = "bar";
		bar.origin = "http://gitmock.com/bar.git";
		bar.latest.push_back(make_commit("ffff"));
		bar.latest.push_back(make_commit("bbbb"));
		bar.previous.push_back(make_commit("eeee"));
		bar.previous.push_back(make_commit("aaaa"));
		bar.schemas.push_back(make_schema("BarModelSchema"));
		bar.schemas.push_back(make_schema("BarViewSchema"));

		registry.add(bar);

		ApprovalTests::Approvals::verify(registry);
	}
}