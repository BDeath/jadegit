#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Assembly.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ObjectMeta.h>
#include <jadegit/vfs/MemoryFileSystem.h>
#include <jadegit/vfs/RecursiveFileIterator.h>
#include <data/storage/ObjectFileStorage.h>
#include <data/storage/ObjectLoader.h>
#include <SourceFileSystem.h>

using namespace JadeGit;
using namespace JadeGit::Data;

TEST_CASE("Assembly.Creation", "[data]")
{
	MemoryFileSystem fs;

	SECTION("Default")
	{
		Assembly assembly(fs);
		CHECK(assembly.GetRootSchema().version == Version());
	}
	SECTION("Target")
	{
		Assembly assembly(fs, Version(22,0,1));
		CHECK(assembly.GetRootSchema().version == Version(22,0,1));
	}
}

TEST_CASE("Assembly.Creation2020", "[data]")
{
	SourceFileSystem fs("resources/Assembly/Creation2020");

	SECTION("Default")
	{
		Assembly assembly(fs);
		CHECK(assembly.GetRootSchema().version == Version(20,0,1));
	}
	SECTION("TargetCurrent")
	{
		Assembly assembly(fs, Version(20,0,1));
		CHECK(assembly.GetRootSchema().version == Version(20,0,1));
	}
	SECTION("TargetOlder")
	{
		Assembly assembly(fs, Version(18, 0, 1));
		CHECK(assembly.GetRootSchema().version == Version(20,0,1));	// Should target minimum version
	}
	SECTION("TargetNewer")
	{
		Assembly assembly(fs, Version(22, 0, 1));
		CHECK(assembly.GetRootSchema().version == Version(22,0,1));	// Should target newer version supplied
	}
}

TEST_CASE("Assembly.LoadEntityWithBadNameErrs", "[data]")
{
	SourceFileSystem fs("resources/Class/BadName");
	Assembly assembly(fs);

	CHECK_THROWS_AS(assembly.Load("schemas/TestSchema/types/TestClass.jox", true), std::runtime_error);
}

TEST_CASE("Assembly.RootSchemaValid", "[data]")
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	const RootSchema& rootSchema = assembly.GetRootSchema();

	for (auto klass : rootSchema->classes)
	{
		if (klass.second == static_cast<const Class*>(*rootSchema.object))
		{
			UNSCOPED_INFO("Object class shouldn't have superclass");
			CHECK(!klass.second->superclass);
		}
		else
		{
			UNSCOPED_INFO(klass.second->name << " class doesn't have a superclass");
			CHECK(klass.second->superclass);
		}

		for (auto property : klass.second->properties)
		{
			if (property.second->isReference() && static_cast<const Reference*>(property.second)->isExplicitInverseRef())
			{
				auto& reference = static_cast<const ExplicitInverseRef&>(*property.second);

				for (auto& inverse : reference.inverses)
				{
					auto& counterpart = *inverse->counterpart;

					UNSCOPED_INFO(reference.GetQualifiedName() << " reference kind mismatch with " << counterpart.reference->GetQualifiedName());
					switch (inverse->getReferenceKind())
					{
					case ExplicitInverseRef::ReferenceKind::Parent:
						CHECK(counterpart.getReferenceKind() == ExplicitInverseRef::ReferenceKind::Child);
						break;
					case ExplicitInverseRef::ReferenceKind::Child:
						CHECK(counterpart.getReferenceKind() == ExplicitInverseRef::ReferenceKind::Parent);
						break;
					case ExplicitInverseRef::ReferenceKind::Peer:
						CHECK(counterpart.getReferenceKind() == ExplicitInverseRef::ReferenceKind::Peer);
						break;
					default:
						throw std::logic_error("Unhandled reference kind");
					}

					UNSCOPED_INFO(reference.GetQualifiedName() << " update mode mismatch with " << counterpart.reference->GetQualifiedName());
					switch (inverse->getReferenceUpdateMode())
					{
					case ExplicitInverseRef::UpdateMode::Automatic:
						CHECK(counterpart.getReferenceUpdateMode() == ExplicitInverseRef::UpdateMode::Manual);
						break;
					case ExplicitInverseRef::UpdateMode::Manual:
						CHECK(counterpart.getReferenceUpdateMode() == ExplicitInverseRef::UpdateMode::Automatic);
						break;
					case ExplicitInverseRef::UpdateMode::Manual_Automatic:
						CHECK(counterpart.getReferenceUpdateMode() == ExplicitInverseRef::UpdateMode::Manual_Automatic);
						break;
					default:
						throw std::logic_error("Unhandled update mode");
					}
				}
			}
		}
	}
}

TEST_CASE("Assembly.CircularLoad", "[data]")
{
	// Using repository with circular references which prompt entity retrieval before being explicitly loaded
	SourceFileSystem fs("resources/Assembly/Circular");

	Assembly assembly(fs);

	// Queue load for all files
	for (auto& file : RecursiveFileIterator(fs.open("")))
	{
		if (!file.isDirectory())
			assembly.getStorage().getLoader().queue(file);
	}
	// Load entities
	REQUIRE(assembly.getStorage().getLoader().load());

	// Get schema
	Schema* schema = assembly.schemas.Get("TestSchema");
	REQUIRE(schema);

	// Get classes
	Class* a = schema->classes.Get("A");
	CHECK(a);

	Class* b = schema->classes.Get("B");
	CHECK(b);

	// Get references
	ExplicitInverseRef* left = a->properties.Get<ExplicitInverseRef>("b");
	CHECK(left);

	ExplicitInverseRef* right = b->properties.Get<ExplicitInverseRef>("a");
	CHECK(right);

	// Check there is an inverse between references
	Inverse* inverse = left->getInverse(right);
	CHECK(inverse);
	CHECK(inverse->counterpart);
	CHECK(right->getInverse(left) == inverse->counterpart);
}