#include <Approvals.h>
#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Assembly.h>
#include <jadegit/data/Reference.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ObjectMeta.h>
#include <jadegit/data/MetaSchema/MetaType.h>
#include <jadegit/vfs/MemoryFileSystem.h>
#include <SourceFileSystem.h>

using namespace JadeGit;
using namespace JadeGit::Data;

TEST_CASE("ExplicitInverseRef.BasicWrite", "[data]") 
{
	MemoryFileSystem fs(SourceFileSystem("resources/Schema/Basic"));
	Assembly assembly(fs);

	// Load schema
	Schema* schema = Entity::resolve<Schema>(assembly, "TestSchema", true);
	REQUIRE(schema);

	// Create classes
	Class a(schema, nullptr, "A", static_cast<Class*>(*assembly.GetRootSchema().object));
	a.Created("{437F639B-EBA3-4293-BA15-FB15857E7AC5}");

	Class b(schema, nullptr, "B", static_cast<Class*>(*assembly.GetRootSchema().object));
	b.Created("{64D5927E-0E8B-471D-84E0-117EF4A4CB04}");

	// Create references
	ExplicitInverseRef left(&a, nullptr, "b");
	left.Created("{0F39768B-98F9-4FA1-8042-2ED749D931BD}");
	left.type = &b;
	left.embedded = true;

	ExplicitInverseRef right(&b, nullptr, "a");
	right.Created("{F1C8E0DC-AE9F-43E4-BC73-B0E73C3F63DD}");
	right.type = &a;
	right.embedded = true;

	// Create inverse between
	Inverse inverse(&left, &right);
	inverse.counterpart->Modified();

	// Save changes
	assembly.save();

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("ExplicitInverseRef.BasicLoad", "[data]") 
{
	SourceFileSystem fs("resources/ExplicitInverseRef/Basic");
	Assembly assembly(fs);

	// Load schema
	auto& schema = Entity::resolve<Schema&>(assembly, "TestSchema", true);

	// Load left class
	auto& a = Entity::resolve<Class&>(schema, "A", true);

	// Load left reference
	ExplicitInverseRef* left = Entity::resolve<ExplicitInverseRef>(a, "b");
	CHECK(left);

	// Load right class
	auto& b = Entity::resolve<Class&>(schema, "B", true);

	// Load right reference
	ExplicitInverseRef* right = Entity::resolve<ExplicitInverseRef>(b, "a");
	CHECK(right);

	// Both references should have one inverse loaded
	CHECK(1 == left->children.size());
	CHECK(1 == right->children.size());
	CHECK(1 == left->inverses.size());
	CHECK(1 == right->inverses.size());

	// Verify inverses between have been created as expected
	Inverse* inverse = left->getInverse(right);
	CHECK(inverse);
	CHECK(inverse->counterpart);
	CHECK(inverse->counterpart != inverse);
	CHECK(right->getInverse(left) == inverse->counterpart);
}

TEST_CASE("ExplicitInverseRef.InverseSameClassWrite", "[data]") 
{
	MemoryFileSystem fs(SourceFileSystem("resources/Schema/Basic"));
	Assembly assembly(fs);

	// Load schema
	Schema* schema = Entity::resolve<Schema>(assembly, "TestSchema", true);
	REQUIRE(schema);

	// Create classe
	Class element(schema, nullptr, "Element", static_cast<Class*>(*assembly.GetRootSchema().object));
	element.Created("{437F639B-EBA3-4293-BA15-FB15857E7AC5}");

	// Create references
	ExplicitInverseRef next(&element, nullptr, "next");
	next.Created("{0F39768B-98F9-4FA1-8042-2ED749D931BD}");
	next.type = &element;
	next.embedded = true;

	ExplicitInverseRef previous(&element, nullptr, "previous");
	previous.Created("{F1C8E0DC-AE9F-43E4-BC73-B0E73C3F63DD}");
	previous.type = &element;
	previous.embedded = true;

	// Create inverse between
	Inverse inverse(&next, &previous);
	inverse.counterpart->Modified();

	// Save changes
	assembly.save();

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("ExplicitInverseRef.InverseSameClassLoad", "[data]") 
{
	SourceFileSystem fs("resources/ExplicitInverseRef/InverseSameClass");
	Assembly assembly(fs);

	// Load schema
	auto& schema = Entity::resolve<Schema&>(assembly, "TestSchema", true);

	// Load class
	auto& element = Entity::resolve<Class&>(schema, "Element", true);

	// Load next reference
	ExplicitInverseRef* next = Entity::resolve<ExplicitInverseRef>(element, "next");
	CHECK(next);

	// Load previous reference
	ExplicitInverseRef* previous = Entity::resolve<ExplicitInverseRef>(element, "previous");
	CHECK(previous);

	// Both references should have one inverse loaded
	CHECK(1 == next->children.size());
	CHECK(1 == previous->children.size());
	CHECK(1 == next->inverses.size());
	CHECK(1 == previous->inverses.size());

	// Verify inverse has been created between references as expected
	Inverse* inverse = next->getInverse(previous);
	CHECK(inverse);
	CHECK(inverse->counterpart);
	CHECK(inverse->counterpart != inverse);
	CHECK(previous->getInverse(next) == inverse->counterpart);
}

TEST_CASE("ExplicitInverseRef.InverseSameReferenceWrite", "[data]") 
{
	MemoryFileSystem fs(SourceFileSystem("resources/Schema/Basic"));
	Assembly assembly(fs);

	// Load schema
	Schema* schema = Entity::resolve<Schema>(assembly, "TestSchema", true);
	REQUIRE(schema);

	// Create classe
	Class klass(schema, nullptr, "TestClass", static_cast<Class*>(*assembly.GetRootSchema().object));
	klass.Created("{437F639B-EBA3-4293-BA15-FB15857E7AC5}");

	// Create reference
	ExplicitInverseRef counterpart(&klass, nullptr, "counterpart");
	counterpart.Created("{0F39768B-98F9-4FA1-8042-2ED749D931BD}");
	counterpart.type = &klass;
	counterpart.embedded = true;

	// Create inverse with self
	Inverse inverse(&counterpart, &counterpart);

	// Save changes
	assembly.save();

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("ExplicitInverseRef.InverseSameReferenceLoad", "[data]") 
{
	SourceFileSystem fs("resources/ExplicitInverseRef/InverseSameReference");
	Assembly assembly(fs);

	// Load schema
	auto& schema = Entity::resolve<Schema&>(assembly, "TestSchema", true);

	// Load class
	auto& klass = Entity::resolve<Class&>(schema, "TestClass", true);

	// Load reference
	ExplicitInverseRef* counterpart = Entity::resolve<ExplicitInverseRef>(klass, "counterpart");
	CHECK(counterpart);

	// References should have one inverse loaded
	CHECK(1 == counterpart->children.size());
	CHECK(1 == counterpart->inverses.size());

	// Verify inverse has been created referring back to reference as expected
	Inverse* inverse = counterpart->getInverse(counterpart);
	CHECK(inverse);
	CHECK(inverse->counterpart);
	CHECK(inverse->counterpart == inverse);
}

TEST_CASE("ExplicitInverseRef.MultipleInversesWrite", "[data]") 
{
	MemoryFileSystem fs(SourceFileSystem("resources/Schema/Basic"));
	Assembly assembly(fs);

	// Load schema
	Schema* schema = Entity::resolve<Schema>(assembly, "TestSchema", true);
	REQUIRE(schema);

	// Get superclass
	Class* object = static_cast<Class*>(*assembly.GetRootSchema().object);

	// Create classes
	Class a(schema, nullptr, "A", object);
	a.Created("{437F639B-EBA3-4293-BA15-FB15857E7AC5}");

	Class b(schema, nullptr, "B", object);
	b.Created("{64D5927E-0E8B-471D-84E0-117EF4A4CB04}");

	Class c(schema, nullptr, "C", object);
	c.Created("{7D49848B-FCE0-492D-9510-5660FA5D539A}");

	Class d(schema, nullptr, "D", object);
	d.Created("{FCEE0DF8-7878-4007-9669-8F19284A23C3}");

	// Create references
	ExplicitInverseRef first(&a, nullptr, "first");
	first.Created("{0F39768B-98F9-4FA1-8042-2ED749D931BD}");
	first.type = object;
	first.embedded = true;

	ExplicitInverseRef second(&b, nullptr, "second");
	second.Created("{F1C8E0DC-AE9F-43E4-BC73-B0E73C3F63DD}");
	second.type = object;
	second.embedded = true;

	ExplicitInverseRef third(&c, nullptr, "third");
	third.Created("{F5651DE8-696F-46D3-8023-7DAA57DCA772}");
	third.type = &d;
	third.embedded = true;

	ExplicitInverseRef complex(&d, nullptr, "complex");
	complex.Created("{B75C90E7-EF91-4A1E-99F9-E03FBBCE0641}");
	complex.type = object;
	complex.embedded = true;

	// Create complex inverses in awkward order (which shall be reflected by output)
	Inverse first_inverse(&complex, &second);

	// Create extra inverse between first & second which complicates load later (order of inverse needs to be maintained)
	Inverse extra(&first, &second);
	extra.counterpart->Modified();

	first_inverse.counterpart->Modified();
	Inverse second_inverse(&complex, &third);
	second_inverse.counterpart->Modified();
	Inverse third_inverse(&complex, &first);
	third_inverse.counterpart->Modified();
	
	// Save changes
	assembly.save();

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("ExplicitInverseRef.MultipleInversesLoad", "[data]") 
{
	SourceFileSystem fs("resources/ExplicitInverseRef/MultipleInverses");
	Assembly assembly(fs);

	// Load schema
	auto& schema = Entity::resolve<Schema&>(assembly, "TestSchema", true);

	// Load classes
	auto& a = Entity::resolve<Class&>(schema, "A", true);
	auto& b = Entity::resolve<Class&>(schema, "B", true);
	auto& c = Entity::resolve<Class&>(schema, "C", false);	// Complete load intended
	auto& d = Entity::resolve<Class&>(schema, "D", true);

	// Load first reference, which has the last inverse with complex reference
	auto first = Entity::resolve<ExplicitInverseRef>(a, "first");
	CHECK(first);

	// Load complex reference
	auto complex = Entity::resolve<ExplicitInverseRef>(d, "complex");
	CHECK(complex);

	// Retrieve inverses in order originally added
	Inverse* first_inverse = complex->getInverse(Entity::resolve<ExplicitInverseRef>(b, "second"));
	Inverse* second_inverse = complex->getInverse(c.properties.Get<ExplicitInverseRef>("third"));
	Inverse* third_inverse = complex->getInverse(first);

	// Verify inverses have been positioned in complex children to maintain their order on update
	CHECK(first_inverse == complex->children.at(0));
	CHECK(second_inverse == complex->children.at(1));
	CHECK(third_inverse == complex->children.at(2));
}