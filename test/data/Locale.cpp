#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Assembly.h>
#include <jadegit/vfs/MemoryFileSystem.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Data;

TEST_CASE("Locale.Creation", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");

	{
		Locale locale(&schema, nullptr, "5124");

		// Check name has been set as expected
		CHECK("5124" == locale.GetName());
		CHECK("5124" == locale.GetValue("name").Get<string>());

		// Check locale has been added to collection
		CHECK(&locale == schema.locales.Get("5124"));
	}

	// Check implicit deletion has removed locale from collection
	CHECK(nullptr == schema.locales.Get("5124"));
}

TEST_CASE("Locale.Enums", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");
	Locale locale(&schema, nullptr, "5124");

	// Set access directly
	locale.access = SchemaEntity::Locale_Local;
	CHECK("local" == locale.access.ToString());

	// Set access based on string
	locale.access.Set("inherited");
	CHECK("inherited" == locale.access.ToString());
	CHECK(SchemaEntity::Locale_Inherited == locale.access);
}