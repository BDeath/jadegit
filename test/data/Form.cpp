#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Assembly.h>
#include <jadegit/data/Form.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/vfs/MemoryFileSystem.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Data;

TEST_CASE("Form.ResolveSuperForm", "[data]")
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema testSchema(assembly, nullptr, "TestSchema");
	testSchema.superschema = assembly.GetRootSchema();

	GUIClass testSuperFormClass(&testSchema, nullptr, "TestSuperForm", *assembly.GetRootSchema().form);

	SECTION("Local")
	{
		Locale testBaseLocale(&testSchema, nullptr, "base");
		Form testSuperForm(&testBaseLocale, nullptr, "TestSuperForm");

		GUIClass testSubFormClass(&testSchema, nullptr, "TestForm", &testSuperFormClass);

		SECTION("FromSameLocale")
		{
			Form testForm(&testBaseLocale, nullptr, "TestForm");
			REQUIRE(testForm.GetSuperForm() == &testSuperForm);
		}
		SECTION("FromBaseLocale")
		{
			Locale testLocale(&testSchema, nullptr, "clone");
			testLocale.cloneOf = &testBaseLocale;

			Form testForm(&testLocale, nullptr, "TestForm");
			REQUIRE(testForm.GetSuperForm() == &testSuperForm);
		}
	}
	SECTION("Inherited")
	{
		Schema testSubSchema(assembly, nullptr, "TestSubSchema");
		testSubSchema.superschema = &testSchema;

		GUIClass testSuperFormCopy(&testSubSchema, nullptr, "TestSuperForm");
		REQUIRE(testSuperFormCopy.superschemaType == &testSuperFormClass);

		GUIClass testSubFormClass(&testSubSchema, nullptr, "TestForm", &testSuperFormCopy);

		SECTION("FromSuperLocale")
		{
			Locale testBaseLocale(&testSchema, nullptr, "base");
			Form testSuperForm(&testBaseLocale, nullptr, "TestSuperForm");

			Locale testLocale(&testSubSchema, nullptr, "base");
			Form testForm(&testLocale, nullptr, "TestForm");

			REQUIRE(testForm.GetSuperForm() == &testSuperForm);
		}
		SECTION("FromSuperBaseLocale")
		{
			Locale testBaseLocale(&testSchema, nullptr, "base");
			Form testSuperForm(&testBaseLocale, nullptr, "TestSuperForm");

			Locale testCloneLocale(&testSchema, nullptr, "clone");
			testCloneLocale.cloneOf = &testBaseLocale;

			Locale testLocale(&testSubSchema, nullptr, "clone");
			Form testForm(&testLocale, nullptr, "TestForm");

			REQUIRE(testForm.GetSuperForm() == &testSuperForm);
		}
		SECTION("FromSuperPrimaryLocale")
		{
			Locale testPrimaryLocale(&testSchema, nullptr, "primary");
			testSchema.primaryLocale = &testPrimaryLocale;

			Form testSuperForm(&testPrimaryLocale, nullptr, "TestSuperForm");

			Locale testLocale(&testSubSchema, nullptr, "base");
			Form testForm(&testLocale, nullptr, "TestForm");

			REQUIRE(testForm.GetSuperForm() == &testSuperForm);
		}
	}
}
