#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Assembly.h>
#include <jadegit/data/Constant.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/vfs/MemoryFileSystem.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Data;

TEST_CASE("Constant.Creation", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");
	schema.superschema = assembly.GetRootSchema();

	Class klass(&schema, nullptr, "TestClass");

	{
		Constant constant(&klass, nullptr, "TestConstant");

		// Check name has been set as expected
		CHECK("TestConstant" == constant.GetName());
		CHECK("TestConstant" == constant.GetValue("name").Get<string>());

		// Check constant has been added to collection
		CHECK(&constant == klass.constants.Get("TestConstant"));
	}

	// Check implicit deletion has removed from collection
	CHECK(nullptr == klass.constants.Get("TestConstant"));
}