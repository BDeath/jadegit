#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Enum.h>

namespace JadeGit::Data
{
	enum Fruit
	{
		Apple = 123,
		Orange = 456
	};

	template<> std::map<Fruit, const char*> EnumStrings<Fruit>::data =
	{
		{ Apple, "Apple" }, 
		{ Orange, "Orange" }
	};
}

using namespace JadeGit::Data;

TEST_CASE("Enum.FromString", "[data]") 
{
	Fruit fruit = Apple;

	std::stringstream line("Orange");
	line >> fruit;

	CHECK(Orange == fruit);
}

TEST_CASE("Enum.ToString", "[data]") 
{
	Fruit fruit = Apple;

	std::ostringstream line;
	line << fruit;

	CHECK("Apple" == line.str());
}

TEST_CASE("Enum.ToBasicString", "[data]") 
{
	Any value = Value<Fruit>(Apple);

	CHECK("123" == value.ToBasicString());

	value = Value<Fruit>(Orange);

	CHECK("456" == value.ToBasicString());
}