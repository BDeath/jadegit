#include <Approvals.h>
#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Assembly.h>
#include <jadegit/data/Class.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ObjectMeta.h>
#include <jadegit/vfs/MemoryFileSystem.h>
#include <SourceFileSystem.h>

using namespace JadeGit;
using namespace JadeGit::Data;
using namespace tinyxml2;

TEST_CASE("DbClassMap.Creation", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");
	schema.superschema = assembly.GetRootSchema();

	Class klass(&schema, nullptr, "TestClass");

	Database db(&schema, nullptr, "TestSchemaDb");

	DbFile dbFile(&db, nullptr, "TestClassFile");

	{
		DbClassMap classMap(&klass, nullptr);
		CHECK(klass.classMapRefs.Includes(&classMap));

		classMap.database = &db;
		CHECK(db.classMaps.Includes(&classMap));

		classMap.diskFile = &dbFile;
		CHECK(dbFile.classMapRefs.Includes(&classMap));
	}

	// Check class map has been removed from all collections
	CHECK(klass.classMapRefs.Empty());
	CHECK(db.classMaps.Empty());
	CHECK(dbFile.classMapRefs.Empty());
}

TEST_CASE("DbClassMap.BasicWrite", "[data]") 
{
	MemoryFileSystem fs(SourceFileSystem("resources/Schema/Basic"));
	Assembly assembly(fs);

	// Load schema
	Schema* schema = Entity::resolve<Schema>(assembly, "TestSchema", false);
	REQUIRE(schema);

	// Create database
	Database db(schema, nullptr, "TestSchemaDb");
	db.Created("{B49CEB5E-E36F-45FF-87F5-AEFB1FF8FF00}");

	// Create file
	DbFile dbFile(&db, nullptr, "TestSchema");
	dbFile.Created("{BC3B4242-1BB3-49C9-9B46-2ADE699AC58C}");

	// Create class
	Class klass(schema, nullptr, "TestClass", static_cast<Class*>(*assembly.GetRootSchema().object));
	klass.Created("{15FE0D64-69A6-4A6C-8F35-D45E5FF6ED2E}");

	// Create class mapping
	DbClassMap classMap(&klass, nullptr);
	classMap.database = &db;
	classMap.diskFile = &dbFile;

	// Save changes
	assembly.save();

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("DbClassMap.BasicLoad", "[data]") 
{
	SourceFileSystem fs("resources/DbClassMap/Basic");
	Assembly assembly(fs);

	// Load schema
	auto& schema = Entity::resolve<Schema&>(assembly, "TestSchema", true);

	// Load database
	auto& db = Entity::resolve<Database&>(schema, "TestSchemaDb", true);

	// Load file
	auto& dbFile = Entity::resolve<DbFile&>(db, "TestSchema", true);

	// Load class
	auto& klass = Entity::resolve<Class&>(schema, "TestClass", false);

	// Class should have one class map
	CHECK(1 == klass.classMapRefs.size());

	DbClassMap* classMap = klass.classMapRefs[0];
	REQUIRE(classMap);

	// Verify class mapping has been populated as expected
	CHECK(&db == static_cast<Database*>(classMap->database));
	CHECK(&dbFile == static_cast<DbFile*>(classMap->diskFile));
	CHECK(DbClassMap::Mode::SharedInstances == static_cast<DbClassMap::Mode>(classMap->mode));
}

TEST_CASE("DbClassMap.InheritedFileWrite", "[data]") 
{
	MemoryFileSystem fs(SourceFileSystem("resources/Schema/Basic"));
	Assembly assembly(fs);

	// Inherit RootSchema system file
	DbFile* dbFile = assembly.GetRootSchema()->getDatabase()->files.Get("_usergui");
	REQUIRE(dbFile);

	// Load schema
	Schema* schema = Entity::resolve<Schema>(assembly, "TestSchema", false);
	REQUIRE(schema);

	// Create database
	Database db(schema, nullptr, "TestSchemaDb");
	db.Created("{B49CEB5E-E36F-45FF-87F5-AEFB1FF8FF00}");

	// Create class
	Class klass(schema, nullptr, "TestClass", static_cast<Class*>(*assembly.GetRootSchema().object));
	klass.Created("{15FE0D64-69A6-4A6C-8F35-D45E5FF6ED2E}");

	// Create class mapping
	DbClassMap classMap(&klass, nullptr);
	classMap.database = &db;
	classMap.diskFile = dbFile;

	// Save changes
	assembly.save();

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("DbClassMap.InheritedFileLoad", "[data]") 
{
	SourceFileSystem fs("resources/DbClassMap/InheritedFile");
	Assembly assembly(fs);

	// Inherit RootSchema system file
	DbFile* dbFile = assembly.GetRootSchema()->getDatabase()->files.Get("_usergui");
	REQUIRE(dbFile);

	// Load schema
	auto& schema = Entity::resolve<Schema&>(assembly, "TestSchema", true);

	// Load database
	auto& db = Entity::resolve<Database&>(schema, "TestSchemaDb", true);

	// Load class
	Class* klass = Entity::resolve<Class>(schema, "TestClass", false);
	REQUIRE(klass);

	// Class should have one class map
	CHECK(1 == klass->classMapRefs.size());

	DbClassMap* classMap = klass->classMapRefs[0];
	REQUIRE(classMap);

	// Verify class mapping has been populated as expected
	CHECK(&db == static_cast<Database*>(classMap->database));
	CHECK(dbFile == static_cast<DbFile*>(classMap->diskFile));
}

TEST_CASE("DbClassMap.AllInstancesWrite", "[data]") 
{
	MemoryFileSystem fs(SourceFileSystem("resources/Schema/Basic"));
	Assembly assembly(fs);

	// Load schema
	Schema* schema = Entity::resolve<Schema>(assembly, "TestSchema", false);
	REQUIRE(schema);

	// Create database
	Database db(schema, nullptr, "TestSchemaDb");
	db.Created("{B49CEB5E-E36F-45FF-87F5-AEFB1FF8FF00}");

	// Create file
	DbFile dbFile(&db, nullptr, "TestSchema");
	dbFile.Created("{BC3B4242-1BB3-49C9-9B46-2ADE699AC58C}");

	// Create class
	Class klass(schema, nullptr, "TestClass", static_cast<Class*>(*assembly.GetRootSchema().object));
	klass.Created("{15FE0D64-69A6-4A6C-8F35-D45E5FF6ED2E}");

	// Create class mapping
	DbClassMap classMap(&klass, nullptr);
	classMap.database = &db;
	classMap.diskFile = &dbFile;
	classMap.mode = DbClassMap::Mode::AllInstances;

	// Save changes
	assembly.save();

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("DbClassMap.AllInstancesLoad", "[data]") 
{
	SourceFileSystem fs("resources/DbClassMap/AllInstances");
	Assembly assembly(fs);

	// Load schema
	auto& schema = Entity::resolve<Schema&>(assembly, "TestSchema", true);

	// Load database
	auto& db = Entity::resolve<Database&>(schema, "TestSchemaDb", true);

	// Load file
	DbFile* dbFile = Entity::resolve<DbFile>(db, "TestSchema", true);
	REQUIRE(dbFile);

	// Load class
	Class* klass = Entity::resolve<Class>(schema, "TestClass", false);
	REQUIRE(klass);

	// Class should have one class map
	CHECK(1 == klass->classMapRefs.size());

	DbClassMap* classMap = klass->classMapRefs[0];
	REQUIRE(classMap);

	// Verify class mapping has been populated as expected
	CHECK(&db == static_cast<Database*>(classMap->database));
	CHECK(dbFile == static_cast<DbFile*>(classMap->diskFile));
	CHECK(DbClassMap::Mode::AllInstances == static_cast<DbClassMap::Mode>(classMap->mode));
}