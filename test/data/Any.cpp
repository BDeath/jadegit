#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Any.h>
#include <jadegit/data/Object.h>
#include <jadegit/data/Value.h>

using namespace std;
using namespace JadeGit::Data;

namespace JadeGit::Data
{
	void PrintTo(const Value<double>& v, std::ostream* os)
	{
		*os << v;
	}
}

TEST_CASE("Any.IntegerGet", "[data]") 
{
	Any any(Value(123));
	CHECK(123 == any.Get<int>());
}

TEST_CASE("Any.StringGet", "[data]") 
{
	Any any(Value<string>("Hello World"));
	CHECK("Hello World" == any.Get<string>());
}

TEST_CASE("Any.IntegerComparisons", "[data]") 
{
	Value<int> a(1);
	Value<int> b(1);
	Value<int> c(2);

	CHECK(Any(a) == Any(b));
	CHECK(Any(b) == Any(b));
	CHECK(Any(b) != Any(c));

	CHECK(Any(a) == b);
	CHECK(Any(b) == b);
	CHECK(Any(b) != c);
}
