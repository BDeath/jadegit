#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Assembly.h>
#include <data/config/DeployConfig.h>
#include <jadegit/vfs/MemoryFileSystem.h>
#include <SourceFileSystem.h>
#include <string_view>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Data;

class DeployConfigFixture
{
public:
	DeployConfigFixture() : fs(SourceFileSystem("resources/DeployConfig"))
	{
	}

protected:
	MemoryFileSystem fs;
	unique_ptr<Assembly> assembly;
	unique_ptr<DeployConfig> config;

	void load(const string_view& sv)
	{
		File file = fs.open(".jadegit");
		file.write(sv);

		assembly = make_unique<Assembly>(fs);
		config = make_unique<DeployConfig>(*assembly);
		config->load();
	}
};

TEST_CASE_METHOD(DeployConfigFixture, "DeployConfig.LoadEmpty", "[data][config]")
{
	load(""sv);
	REQUIRE(config->scripts.empty());
}

TEST_CASE_METHOD(DeployConfigFixture, "DeployConfig.LoadScripts", "[data][config]")
{
	SECTION("MissingSchema")
	{
		REQUIRE_THROWS(load(R"(
[[deployment.scripts]]
)"sv));
	}
	SECTION("MissingApp")
	{
		REQUIRE_THROWS(load(R"(
[[deployment.scripts]]
schema="TestSchema"
)"sv));
	}
	SECTION("MissingMethod")
	{
		REQUIRE_THROWS(load(R"(
[[deployment.scripts]]
schema="TestSchema"
app="TestApp"
)"sv));
	}
	SECTION("MissingParam")
	{
		REQUIRE_THROWS(load(R"(
[[deployment.scripts]]
schema="TestSchema"
app="TestApp"
method="testMethodWithParam"
)"sv));
	}
	SECTION("InvalidSchema")
	{
		REQUIRE_THROWS(load(R"(
[[deployment.scripts]]
schema="InvalidSchema"
app="TestApp"
method="testMethod"
)"sv));
	}
	SECTION("InvalidApp")
	{
		REQUIRE_THROWS(load(R"(
[[deployment.scripts]]
schema="TestSchema"
app="InvalidApp"
method="testMethod"
)"sv));
	}
	SECTION("InvalidMethod")
	{
		REQUIRE_THROWS(load(R"(
[[deployment.scripts]]
schema="TestSchema"
app="TestApp"
method="invalidMethod"
)"sv));
	}
	SECTION("InvalidParam")
	{
		REQUIRE_THROWS(load(R"(
[[deployment.scripts]]
schema="TestSchema"
app="TestApp"
method="testMethod"
param="unexpected param value"
)"sv));
	}
	SECTION("InvalidTransient")
	{
		REQUIRE_THROWS(load(R"(
[[deployment.scripts]]
schema="TestSchema"
app="TestApp"
method="TestClass::testClassTypeMethod"
transient=true
)"sv));
	}
	SECTION("TestSchema")
	{
		SECTION("JadeScriptMethod")
		{
			load(R"(
[[deployment.scripts]]
schema="TestSchema"
app="TestApp"
method="testMethod"
)"sv);
			REQUIRE(config->scripts.size() == 1);
			auto& script = config->scripts.front();

			REQUIRE(script.schema);
			REQUIRE(script.app);
			REQUIRE(script.method);
			CHECK(script.schema->name == "TestSchema");
			CHECK(script.app->name == "TestApp");
			CHECK(script.method->schemaType->schema == script.schema);
			CHECK(script.method->schemaType->name == "JadeScript");
			CHECK(script.method->name == "testMethod");
			CHECK(!script.transient);
		}
		SECTION("JadeScriptMethodWithParam")
		{
			load(R"(
[[deployment.scripts]]
schema="TestSchema"
app="TestApp"
method="testMethodWithParam"
param="expected param value"
)"sv);
			REQUIRE(config->scripts.size() == 1);
			auto& script = config->scripts.front();

			REQUIRE(script.schema);
			REQUIRE(script.app);
			REQUIRE(script.method);
			CHECK(script.schema->name == "TestSchema");
			CHECK(script.app->name == "TestApp");
			CHECK(script.method->schemaType->schema == script.schema);
			CHECK(script.method->schemaType->name == "JadeScript");
			CHECK(script.method->name == "testMethodWithParam");
			CHECK(script.param == "expected param value");
			CHECK(!script.transient);
		}
		SECTION("TestClassMethod")
		{
			load(R"(
[[deployment.scripts]]
schema="TestSchema"
app="TestApp"
method="TestClass::testClassMethod"
transient=true
)"sv);
			REQUIRE(config->scripts.size() == 1);
			auto& script = config->scripts.front();

			REQUIRE(script.schema);
			REQUIRE(script.app);
			REQUIRE(script.method);
			CHECK(script.schema->name == "TestSchema");
			CHECK(script.app->name == "TestApp");
			CHECK(script.method->schemaType->schema == script.schema);
			CHECK(script.method->schemaType->name == "TestClass");
			CHECK(script.method->name == "testClassMethod");
			CHECK(script.transient);
		}
		SECTION("TestPeerSchemaMethod")
		{
			load(R"(
[[deployment.scripts]]
schema="TestSchema"
app="TestApp"
method="TestPeerSchema::TestPeerClass::testPeerClassMethod"
)"sv);
			REQUIRE(config->scripts.size() == 1);
			auto& script = config->scripts.front();

			REQUIRE(script.schema);
			REQUIRE(script.app);
			REQUIRE(script.method);
			CHECK(script.schema->name == "TestSchema");
			CHECK(script.app->name == "TestApp");
			CHECK(script.method->schemaType->schema->name == "TestPeerSchema");
			CHECK(script.method->schemaType->name == "TestPeerClass");
			CHECK(script.method->name == "testPeerClassMethod");
		}
	}
	SECTION("Mixed")
	{
		load(R"(
[[deployment.scripts]]
schema="TestSchema"
app="TestApp"
method="testMethod"
transient=true

[[deployment.scripts]]
schema="TestSchema"
app="TestApp"
method="testMethodWithParam"
param="expected param value"

[[deployment.scripts]]
schema="TestSchema"
app="TestApp"
method="TestPeerSchema::TestPeerClass::testPeerClassMethod"
)"sv);

		REQUIRE(config->scripts.size() == 3);

		{
			auto& script = config->scripts[0];
			REQUIRE(script.schema);
			REQUIRE(script.app);
			REQUIRE(script.method);
			CHECK(script.schema->name == "TestSchema");
			CHECK(script.app->name == "TestApp");
			CHECK(script.method->schemaType->schema == script.schema);
			CHECK(script.method->schemaType->name == "JadeScript");
			CHECK(script.method->name == "testMethod");
			CHECK(script.transient);
		}
		{
			auto& script = config->scripts[1];
			REQUIRE(script.schema);
			REQUIRE(script.app);
			REQUIRE(script.method);
			CHECK(script.schema->name == "TestSchema");
			CHECK(script.app->name == "TestApp");
			CHECK(script.method->schemaType->schema == script.schema);
			CHECK(script.method->schemaType->name == "JadeScript");
			CHECK(script.method->name == "testMethodWithParam");
			CHECK(script.param == "expected param value");
			CHECK(!script.transient);
		}
		{
			auto& script = config->scripts[2];
			REQUIRE(script.schema);
			REQUIRE(script.app);
			REQUIRE(script.method);
			CHECK(script.schema->name == "TestSchema");
			CHECK(script.app->name == "TestApp");
			CHECK(script.method->schemaType->schema->name == "TestPeerSchema");
			CHECK(script.method->schemaType->name == "TestPeerClass");
			CHECK(script.method->name == "testPeerClassMethod");
		}
	}
}