#include <Approvals.h>
#include <catch2/catch_test_macros.hpp>
#include <jadegit/Version.h>
#include <jadegit/vfs/MemoryFileSystem.h>
#include <data/config/Config.h>
#include <SourceFileSystem.h>

using namespace JadeGit;
using namespace JadeGit::Data;

TEST_CASE("Config.GetVersion", "[data]")
{
	SourceFileSystem fs("../resources/Assembly/Creation2020");
	Config config(fs, ".jadegit");

	CHECK(config.get<Version>(toml::path("core.platform")) == Version(20, 0, 1));
}

TEST_CASE("Config.SetVersion", "[data]")
{
	MemoryFileSystem fs;
	Config config(fs, ".jadegit");

	// Set version
	config.set(toml::path("core.platform"), Version(22, 0, 1));

	// Save to file
	config.save();

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("Config.UpdateVersion", "[data]")
{
	MemoryFileSystem fs(SourceFileSystem("../resources/Assembly/Creation2020"));
	Config config(fs, ".jadegit");

	// Set version
	config.set(toml::path("core.platform"), Version(22, 0, 1));

	// Save to file
	config.save();

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}