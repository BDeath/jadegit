#include <catch2/catch_test_macros.hpp>
#include <deploy/Manifest.h>
#include <TestAppContext.h>

#include <iostream>
#include <filesystem>

using namespace JadeGit;
using namespace JadeGit::Deploy;

TEST_CASE("Manifest.Basic", "[deploy]")
{
	TestDbContext db;
	TestAppContext app(db);

	auto fake_origin = "fake-origin";
	auto fake_commit = "fake-commit";

	// Update
	Manifest::set(fake_origin, fake_commit);

	// Retrieve
	auto manifest = Manifest::get();
	CHECK(manifest.origin == fake_origin);
	CHECK(manifest.commit == fake_commit);
}