#pragma once
#include <registry/Root.h>

class FakeRegistry : public JadeGit::Registry::Root
{
public:
	static FakeRegistry make_basic();

	FakeRegistry& add_repository(const char* origin);
	FakeRegistry& add_previous_commit(const char* commit);
	FakeRegistry& add_latest_commit(const char* commit);
};