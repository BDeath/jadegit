#pragma once
#include <jadegit/vfs/NativeFileSystem.h>

namespace JadeGit
{
	namespace fs = std::filesystem;

	class TestFileSystem : public NativeFileSystem
	{
	public:
		TestFileSystem(fs::path subfolder);
		~TestFileSystem();

		fs::path path() const;
	};
}