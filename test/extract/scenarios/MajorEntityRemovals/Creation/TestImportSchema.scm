jadeVersionNumber "20.0.02";
schemaDefinition
TestImportSchema subschemaOf RootSchema completeDefinition, patchVersioningEnabled = false;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:03:47.641;
importedPackageDefinitions
	DeprecatedPackage is TestExportSchema::DeprecatedPackage
	(
		documentationText
		`This is an imported package`
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:10:18:14:32.942;
		importedClassDefinitions
			PackagedClass
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:10:18:14:32.942;
		importedInterfaceDefinitions
			IPackagedInterface
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:10:18:14:32.942;
	)

constantDefinitions
localeDefinitions
	2057 "English (United Kingdom)";
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:10:19:09:18.108;
	5129 "English (New Zealand)" schemaDefaultLocale;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:10:19:09:18.108;
	1033 "English (United States)" _cloneOf 5129;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:10:19:09:18.108;
libraryDefinitions
typeHeaders
	TestImportSchema subclassOf RootSchemaApp transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2191;
	DeprecatedClass subclassOf Object highestOrdinal = 1, number = 2195;
	GTestImportSchema subclassOf RootSchemaGlobal transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2192;
	STestImportSchema subclassOf RootSchemaSession transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2193;
	DeprecatedForm subclassOf Form transient, transientAllowed, subclassTransientAllowed, number = 2058;
 
interfaceDefs
	IDeprecatedInterface number = 1289
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:15:37.990;
	subInterfaceOf
		DeprecatedPackage::IPackagedInterface
 
	jadeMethodDefinitions
		localExtensionMethod() number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:15:49.788;
	)
 
	ILocalInterface number = 1288
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:10:43.489;
 
	jadeMethodDefinitions
		localInterfaceMethod() number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:11:07.109;
	)
 
membershipDefinitions
 
typeDefinitions
	DeprecatedPackage::PackagedClass completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:10:18:14:32.942;
	constantDefinitions
		LocalConstant:                 String = "Local" number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:10:05.262;
 
	jadeMethodDefinitions
		localExtensionMethod() protected, number = 1002;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:17:32.043;
		localMethod() number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:17:43.046;
	implementInterfaces
		IDeprecatedInterface
		(
		localExtensionMethod is localExtensionMethod;
		)
		ILocalInterface
		(
		localInterfaceMethod is localMethod;
		)
	)
	Object completeDefinition
	(
	)
	Application completeDefinition
	(
	)
	RootSchemaApp completeDefinition
	(
	)
	TestImportSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:03:47.639;
	)
	DeprecatedClass completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:10:18:16:38.103;
	referenceDefinitions
		packagedClass:                 DeprecatedPackage::PackagedClass  readonly, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:13:33.214;
 
	jadeMethodDefinitions
		packagedInterfaceMethod(): DeprecatedPackage::PackagedClass protected, number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:10:18:14:43.905;
	implementInterfaces
		DeprecatedPackage::IPackagedInterface
		(
		packagedInterfaceMethod is packagedInterfaceMethod;
		)
	)
	Global completeDefinition
	(
	)
	RootSchemaGlobal completeDefinition
	(
	)
	GTestImportSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:03:47.640;
	)
	WebSession completeDefinition
	(
	)
	RootSchemaSession completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "6.1.00" 20031119 2003:12:01:13:54:02.270;
	)
	STestImportSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:03:47.641;
	)
	Window completeDefinition
	(
	)
	Form completeDefinition
	(
	)
	DeprecatedForm completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:10:18:21:28.362;
	)
 
inverseDefinitions
databaseDefinitions
TestImportSchemaDb
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:03:47.641;
	databaseFileDefinitions
		"testimportschema" number = 59;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:03:47.641;
	defaultFileDefinition "testimportschema";
	classMapDefinitions
		STestImportSchema in "_environ";
		TestImportSchema in "_usergui";
		GTestImportSchema in "testimportschema";
		DeprecatedClass in "testimportschema";
	)
schemaViewDefinitions
exportedPackageDefinitions
typeSources
	DeprecatedPackage::PackagedClass (
	jadeMethodSources
localExtensionMethod
{
localExtensionMethod() protected;

begin
end;

}

localMethod
{
localMethod();

begin
end;

}

	)
	DeprecatedClass (
	jadeMethodSources
packagedInterfaceMethod
{
packagedInterfaceMethod(): DeprecatedPackage::PackagedClass protected;

begin
	return packagedClass;
end;

}

	)
	IDeprecatedInterface (
	jadeMethodSources
localExtensionMethod
{
localExtensionMethod();
}

	)
	ILocalInterface (
	jadeMethodSources
localInterfaceMethod
{
localInterfaceMethod();
}

	)
