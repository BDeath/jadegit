jadeVersionNumber "20.0.02";
schemaDefinition
DeprecatedSchema subschemaOf RootSchema completeDefinition, patchVersioningEnabled = false;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:10:18:13:32.628;
importedPackageDefinitions
constantDefinitions
localeDefinitions
	5129 "English (New Zealand)" schemaDefaultLocale;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:10:18:13:32.616;
	1033 "English (United States)" _cloneOf 5129;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:10:18:13:32.616;
libraryDefinitions
typeHeaders
	DeprecatedSchema subclassOf RootSchemaApp transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2055;
	GDeprecatedSchema subclassOf RootSchemaGlobal transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2056;
	SDeprecatedSchema subclassOf RootSchemaSession transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2057;
 
membershipDefinitions
 
typeDefinitions
	Object completeDefinition
	(
	)
	Application completeDefinition
	(
	)
	RootSchemaApp completeDefinition
	(
	)
	DeprecatedSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:10:18:13:32.627;
	)
	Global completeDefinition
	(
	)
	RootSchemaGlobal completeDefinition
	(
	)
	GDeprecatedSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:10:18:13:32.627;
	)
	WebSession completeDefinition
	(
	)
	RootSchemaSession completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "6.1.00" 20031119 2003:12:01:13:54:02.270;
	)
	SDeprecatedSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:10:18:13:32.628;
	)
 
inverseDefinitions
databaseDefinitions
DeprecatedSchemaDb
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:10:18:13:32.628;
	databaseFileDefinitions
		"deprecatedschema" number = 51;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:10:18:13:32.628;
	defaultFileDefinition "deprecatedschema";
	classMapDefinitions
		SDeprecatedSchema in "_environ";
		DeprecatedSchema in "_usergui";
		GDeprecatedSchema in "deprecatedschema";
	)
schemaViewDefinitions
exportedPackageDefinitions
typeSources
