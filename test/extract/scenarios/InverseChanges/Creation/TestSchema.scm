jadeVersionNumber "20.0.02";
schemaDefinition
TestSchema subschemaOf RootSchema completeDefinition, patchVersioningEnabled = false;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:24:02.961;
importedPackageDefinitions
constantDefinitions
localeDefinitions
	5129 "English (New Zealand)" schemaDefaultLocale;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:24:02.947;
	1033 "English (United States)" _cloneOf 5129;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:24:02.947;
libraryDefinitions
typeHeaders
	TestSchema subclassOf RootSchemaApp transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2185;
	Child subclassOf Object transient, sharedTransientAllowed, transientAllowed, highestOrdinal = 4, number = 2190;
	GTestSchema subclassOf RootSchemaGlobal transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2186;
	Parent subclassOf Object transient, sharedTransientAllowed, transientAllowed, highestSubId = 4, highestOrdinal = 4, number = 2188;
	STestSchema subclassOf RootSchemaSession transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2187;
	ChildSet subclassOf ObjectSet loadFactor = 66, transient, sharedTransientAllowed, transientAllowed, number = 2191;
 
membershipDefinitions
	ChildSet of Child ;
 
typeDefinitions
	Object completeDefinition
	(
	)
	Application completeDefinition
	(
	)
	RootSchemaApp completeDefinition
	(
	)
	TestSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:24:02.961;
	)
	Child completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:51:15.587;
	referenceDefinitions
		parent:                        Parent   explicitEmbeddedInverse, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:52:25.955;
		parent_active:                 Parent   explicitEmbeddedInverse, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:54:08.213;
		parent_finished:               Parent   explicitEmbeddedInverse, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:53:17.887;
		parent_inactive:               Parent   explicitEmbeddedInverse, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:58:21.734;
	)
	Global completeDefinition
	(
	)
	RootSchemaGlobal completeDefinition
	(
	)
	GTestSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:24:02.961;
	)
	Parent completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:51:08.238;
	referenceDefinitions
		children:                      ChildSet   explicitInverse, subId = 1, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:52:25.953;
		children_active:               ChildSet   explicitInverse, subId = 3, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:54:08.212;
		children_finished:             ChildSet   explicitInverse, subId = 2, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:53:17.887;
		children_inactive:             ChildSet   explicitInverse, subId = 4, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:58:21.733;
	)
	WebSession completeDefinition
	(
	)
	RootSchemaSession completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "6.1.00" 20031119 2003:12:01:13:54:02.270;
	)
	STestSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:24:02.961;
	)
	Collection completeDefinition
	(
	)
	Btree completeDefinition
	(
	)
	Set completeDefinition
	(
	)
	ObjectSet completeDefinition
	(
	)
	ChildSet completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:51:39.419;
	)
 
inverseDefinitions
	children of Parent peerOf parent of Child;
	children_active of Parent peerOf parent_active of Child;
	children_finished of Parent peerOf parent_finished of Child;
	children_inactive of Parent peerOf parent_inactive of Child;
databaseDefinitions
TestSchemaDb
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:24:02.961;
	databaseFileDefinitions
		"testschema" number = 57;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:24:02.961;
	defaultFileDefinition "testschema";
	classMapDefinitions
		STestSchema in "_environ";
		TestSchema in "_usergui";
		GTestSchema in "testschema";
		ChildSet in "testschema";
	)
schemaViewDefinitions
exportedPackageDefinitions
typeSources
