jadeVersionNumber "20.0.02";
schemaDefinition
TestSchema subschemaOf RootSchema completeDefinition, patchVersioningEnabled = false;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:24:02.961;
importedPackageDefinitions
constantDefinitions
localeDefinitions
	5129 "English (New Zealand)" schemaDefaultLocale;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:24:02.947;
	1033 "English (United States)" _cloneOf 5129;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:24:02.947;
libraryDefinitions
typeHeaders
	TestSchema subclassOf RootSchemaApp transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2185;
	Child subclassOf Object transient, sharedTransientAllowed, transientAllowed, highestOrdinal = 5, number = 2190;
	GTestSchema subclassOf RootSchemaGlobal transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2186;
	Parent subclassOf Object transient, sharedTransientAllowed, transientAllowed, highestSubId = 4, highestOrdinal = 4, number = 2188;
	STestSchema subclassOf RootSchemaSession transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2187;
	ChildSet subclassOf ObjectSet loadFactor = 66, transient, sharedTransientAllowed, transientAllowed, number = 2191;
 
membershipDefinitions
	ChildSet of Child ;
 
typeDefinitions
	Object completeDefinition
	(
	)
	Application completeDefinition
	(
	)
	RootSchemaApp completeDefinition
	(
	)
	TestSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:24:02.961;
	)
	Child completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:51:15.587;
	attributeDefinitions
		state:                         Integer number = 5, ordinal = 5;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:13:00:04:03.530;
	referenceDefinitions
		parent:                        Parent   explicitEmbeddedInverse, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:13:00:13:00.341;
		parent_active:                 Parent  number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:13:00:08:10.904;
		parent_finished:               Parent  number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:13:00:08:24.691;
		parent_inactive:               Parent  number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:13:00:12:24.965;
 
	jadeMethodDefinitions
		isActive(): Boolean condition, number = 1002;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:13:00:05:00.502;
		isFinished(): Boolean condition, number = 1003;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:13:00:05:11.384;
		isInactive(): Boolean condition, number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:13:00:04:45.846;
	)
	Global completeDefinition
	(
	)
	RootSchemaGlobal completeDefinition
	(
	)
	GTestSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:24:02.961;
	)
	Parent completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:51:08.238;
	referenceDefinitions
		children:                      ChildSet   explicitInverse, readonly, subId = 1, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:13:00:06:25.854;
		children_active:               ChildSet  where Child::isActive explicitInverse, readonly, subId = 3, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:13:00:08:40.350;
		children_finished:             ChildSet  where Child::isFinished explicitInverse, readonly, subId = 2, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:13:00:12:54.017;
		children_inactive:             ChildSet  where Child::isInactive explicitInverse, readonly, subId = 4, number = 4, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:13:00:13:00.341;
	)
	WebSession completeDefinition
	(
	)
	RootSchemaSession completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "6.1.00" 20031119 2003:12:01:13:54:02.270;
	)
	STestSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:24:02.961;
	)
	Collection completeDefinition
	(
	)
	Btree completeDefinition
	(
	)
	Set completeDefinition
	(
	)
	ObjectSet completeDefinition
	(
	)
	ChildSet completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:51:39.419;
	)
 
inverseDefinitions
	children of Parent automatic parentOf parent of Child manual;
	children_active of Parent automatic parentOf parent of Child manual;
	children_finished of Parent automatic parentOf parent of Child manual;
	children_inactive of Parent automatic parentOf parent of Child manual;
databaseDefinitions
TestSchemaDb
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:24:02.961;
	databaseFileDefinitions
		"testschema" number = 57;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:08:12:23:24:02.961;
	defaultFileDefinition "testschema";
	classMapDefinitions
		STestSchema in "_environ";
		TestSchema in "_usergui";
		GTestSchema in "testschema";
		ChildSet in "testschema";
	)
schemaViewDefinitions
exportedPackageDefinitions
typeSources
	Child (
	jadeMethodSources
isActive
{
isActive():Boolean condition;

begin
	return state = 1;
end;

}

isFinished
{
isFinished():Boolean condition;

begin
	return state = 2;
end;

}

isInactive
{
isInactive():Boolean condition;

begin
	return state = 0;
end;

}

	)
