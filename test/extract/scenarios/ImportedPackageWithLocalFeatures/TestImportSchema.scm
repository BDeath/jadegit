jadeVersionNumber "20.0.02";
schemaDefinition
TestImportSchema subschemaOf RootSchema completeDefinition, patchVersioningEnabled = false;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:03:47.641;
importedPackageDefinitions
	ImportedPackage is TestExportSchema::ExportedPackage
	(
		documentationText
		`This is an imported package`
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:09:38.952;
		importedClassDefinitions
			PackagedClass
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:17:08.225;
		importedInterfaceDefinitions
			IPackagedInterface
		setModifiedTimeStamp "<unknown>" "" 2023:12:07:15:09:38;
	)

constantDefinitions
localeDefinitions
	5129 "English (New Zealand)" schemaDefaultLocale;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:03:47.615;
	1033 "English (United States)" _cloneOf 5129;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:03:47.615;
libraryDefinitions
typeHeaders
	TestImportSchema subclassOf RootSchemaApp transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2191;
	GTestImportSchema subclassOf RootSchemaGlobal transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2192;
	LocalClass subclassOf Object highestOrdinal = 1, number = 2195;
	STestImportSchema subclassOf RootSchemaSession transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2193;
 
interfaceDefs
	ILocalExtension number = 1289
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:15:37.990;
	subInterfaceOf
		ImportedPackage::IPackagedInterface
 
	jadeMethodDefinitions
		localExtensionMethod() number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:15:49.788;
	)
 
	ILocalInterface number = 1288
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:10:43.489;
 
	jadeMethodDefinitions
		localInterfaceMethod() number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:11:07.109;
	)
 
membershipDefinitions
 
typeDefinitions
	ImportedPackage::PackagedClass completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:17:08.225;
	constantDefinitions
		LocalConstant:                 String = "Local" number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:10:05.262;
 
	jadeMethodDefinitions
		localExtensionMethod() protected, number = 1002;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:17:32.043;
		localMethod() number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:17:43.046;
	implementInterfaces
		ILocalExtension
		(
		localExtensionMethod is localExtensionMethod;
		)
		ILocalInterface
		(
		localInterfaceMethod is localMethod;
		)
	)
	Object completeDefinition
	(
	)
	Application completeDefinition
	(
	)
	RootSchemaApp completeDefinition
	(
	)
	TestImportSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:03:47.639;
	)
	Global completeDefinition
	(
	)
	RootSchemaGlobal completeDefinition
	(
	)
	GTestImportSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:03:47.640;
	)
	LocalClass completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:13:50.025;
	referenceDefinitions
		packagedClass:                 ImportedPackage::PackagedClass  readonly, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:13:33.214;
 
	jadeMethodDefinitions
		packagedInterfaceMethod(): ImportedPackage::PackagedClass protected, number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:14:15.626;
	implementInterfaces
		ImportedPackage::IPackagedInterface
		(
		packagedInterfaceMethod is packagedInterfaceMethod;
		)
	)
	WebSession completeDefinition
	(
	)
	RootSchemaSession completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "6.1.00" 20031119 2003:12:01:13:54:02.270;
	)
	STestImportSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:03:47.641;
	)
 
inverseDefinitions
databaseDefinitions
TestImportSchemaDb
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:03:47.641;
	databaseFileDefinitions
		"testimportschema" number = 59;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:15:03:47.641;
	defaultFileDefinition "testimportschema";
	classMapDefinitions
		STestImportSchema in "_environ";
		TestImportSchema in "_usergui";
		GTestImportSchema in "testimportschema";
		LocalClass in "testimportschema";
	)
schemaViewDefinitions
exportedPackageDefinitions
typeSources
	ImportedPackage::PackagedClass (
	jadeMethodSources
localExtensionMethod
{
localExtensionMethod() protected;

begin
end;

}

localMethod
{
localMethod();

begin
end;

}

	)
	LocalClass (
	jadeMethodSources
packagedInterfaceMethod
{
packagedInterfaceMethod(): ImportedPackage::PackagedClass protected;

begin
	return packagedClass;
end;

}

	)
	ILocalExtension (
	jadeMethodSources
localExtensionMethod
{
localExtensionMethod();
}

	)
	ILocalInterface (
	jadeMethodSources
localInterfaceMethod
{
localInterfaceMethod();
}

	)
