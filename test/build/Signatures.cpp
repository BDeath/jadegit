#include <ApprovalTests.hpp>
#include <catch2/catch_test_macros.hpp>
#include <jadegit/build/Director.h>
#include <jadegit/build/FileSource.h>
#include <vfs/FileSignature.h>
#include <jadegit/vfs/NativeFileSystem.h>
#include <source_location>
#include "MockBuilder.h"

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Build;

void build(filesystem::path path, const FileSignature& signature)
{
	shared_ptr<FileSystem> fs = make_shared<NativeFileSystem>(path, false, &signature);

	MockBuilder builder;
	Director(FileSource(nullptr, fs)).Build(builder);

	ApprovalTests::Approvals::verify(builder.fs);
}

TEST_CASE("Signatures", "[build]")
{
	git_oid commit = { {0} };
	REQUIRE(git_oid_fromstr(&commit, "4a202b346bb0fb0db7eff3cffeb3c70babbd2045") == 0);

	for (const auto& scenario : filesystem::directory_iterator(filesystem::path(source_location::current().file_name()).parent_path() / "signatures"))
	{
		if (!scenario.is_directory())
			continue;

		SECTION(scenario.path().filename().string())
		{
			SECTION("EmptySignature")
			{
				build(scenario.path(), FileSignature());
			}
			SECTION("WithAuthor")
			{
				build(scenario.path(), FileSignature("TestAuthor", sys_days{ December / 11 / 2021 } + 7h + 01min));
			}
			SECTION("WithCommit")
			{
				build(scenario.path(), FileSignature(string(), commit,  sys_days{December / 11 / 2021} + 7h + 01min));
			}
			SECTION("WithPatch")
			{
				build(scenario.path(), FileSignature(string(), 123456, sys_days{December / 11 / 2021} + 7h + 01min));
			}
			SECTION("WithAuthorAndCommit")
			{
				build(scenario.path(), FileSignature("TestCommit", commit, sys_days{ December / 11 / 2021 } + 7h + 01min));
			}
			SECTION("WithAuthorAndPatch")
			{
				build(scenario.path(), FileSignature("TestPatch", 456789, sys_days{December / 11 / 2021} + 7h + 01min));
			}
			SECTION("WithLongAuthorAndCommit")
			{
				build(scenario.path(), FileSignature("TestCommitWithExtremelyLongAuthorName", commit, sys_days{ December / 11 / 2021 } + 7h + 01min));
			}
			SECTION("WithLongAuthorAndPatch")
			{
				build(scenario.path(), FileSignature("TestPatchWithExtremelyLongAuthorName", 456789, sys_days{ December / 11 / 2021 } + 7h + 01min));
			}
		}
	}
}
