#include <build/Graph.h>
#include <build/TaskVisitor.h>
#include <catch2/catch_test_macros.hpp>

using namespace std;

class Graph : public JadeGit::Build::Graph
{
public:

protected:
	friend class Task;
	int children = 0;
};

class TaskVisitor : public JadeGit::Build::TaskVisitor
{
public:
	ostringstream os;

	bool visit(const Task& task);

protected:
	int group = 0;

	void flush(bool final)
	{
		group = 0;
		if (!final) os << "|";
	}

private:
	bool visit(const JadeGit::Build::BuildTask& task) final { return false; }
	bool visit(const JadeGit::Build::CommandTask& task) final { return false; }
	bool visit(const JadeGit::Build::ExtractTask& task) final { return false; }
	bool visit(const JadeGit::Build::ReorgTask& task) final { return false; }
};

class Task : public JadeGit::Build::Task
{
public:
	Task(Graph& graph) : JadeGit::Build::Task(graph), number(++graph.children)
	{
	}

	Task(Graph& graph, Task& parent) : JadeGit::Build::Task(graph, parent), parent(&parent), number(++parent.children)
	{
	}

	operator string() const final
	{
		if (parent)
			return format("{}.{}", static_cast<string>(*parent), number);
		else
			return format("{}", number);
	}

	bool accept(JadeGit::Build::TaskVisitor& v) const final 
	{
		return static_cast<TaskVisitor&>(v).visit(*this);
	}
	
	int group() const
	{
		const Task* parent = this;
		while (parent->parent)
			parent = parent->parent;
		return parent->number;
	}

	int Priority() const { return 0; }

protected:
	int children = 0;
	const Task* const parent = nullptr;
	const int number;
};

bool TaskVisitor::visit(const Task& task)
{
	if (group)
	{
		if (group != task.group())
			return false;
		os << " ";
	}
	else
		group = task.group();

	os << static_cast<string>(task);
	return true;
}

TEST_CASE("Task.isSuccessorOf", "[build]")
{
	Graph graph;
	Task* a = new Task(graph);
	Task* b = new Task(graph);
	Task* c = new Task(graph);

	CHECK(!a->isSuccessorOf(b));
	CHECK(!a->isSuccessorOf(c));
	CHECK(!b->isSuccessorOf(c));

	a->addPredecessor(b);
	CHECK(a->isSuccessorOf(b));

	b->addPredecessor(c);
	CHECK(b->isSuccessorOf(c));

	// Indirect dependency
	CHECK(a->isSuccessorOf(c));
}

TEST_CASE("Task.tryAddPredeccessor", "[build]")
{
	Graph graph;
	Task* a = new Task(graph);
	Task* b = new Task(graph);
	Task* c = new Task(graph);

	CHECK(a->tryAddPredecessor(b));
	CHECK(b->tryAddPredecessor(c));
	CHECK(!c->tryAddPredecessor(a));	// Cyclic dependency should be prevented
}

void traverse(const Graph& graph, const char* expected)
{
	TaskVisitor visitor;
	bool result = graph.traverse(visitor);
	
	CHECK(result);
	CHECK(visitor.os.str() == expected);
}

TEST_CASE("Graph.traverse", "[build]")
{
	Graph graph;
	Task* a = new Task(graph);	// 1
	Task* b = new Task(graph);	// 2
	Task* c = new Task(graph);	// 3

	SECTION("no edges")
	{
		traverse(graph, "1|2|3");
	}
	SECTION("direct edges")
	{
		a->addPredecessor(b);
		b->addPredecessor(c);
		
		traverse(graph, "3|2|1");
	}
	SECTION("composite")
	{
		Task* a1 = new Task(graph, *a);	// 1.1
		Task* a2 = new Task(graph, *a);	// 1.2
		Task* b1 = new Task(graph, *b);	// 2.1
		Task* b2 = new Task(graph, *b);	// 2.2
		Task* c1 = new Task(graph, *c);	// 3.1
		Task* c2 = new Task(graph, *c);	// 3.2

		b1->addPredecessor(c1);		// 2.1 comes after 3.1
		b1->addPredecessor(b2);		// 2.1 comes after 2.2
		a2->addPredecessor(b2);		// 1.2 comes after 2.2
		a->addPredecessor(b, true);	// 2 and all children come before 1 and all children
		c->addPredecessor(a);		// 3 comes after 1

		traverse(graph, "3.1|2.2 2.1 2|1.1 1.2 1|3.1 3.2 3");
	}
	SECTION("peers")
	{
		Task* a1 = new Task(graph, *a);	// 1.1
		Task* a2 = new Task(graph, *a);	// 1.2
		Task* b1 = new Task(graph, *b);	// 2.1
		Task* b2 = new Task(graph, *b);	// 2.2
		Task* c1 = new Task(graph, *c);	// 3.1
		Task* c2 = new Task(graph, *c);	// 3.2

		b1->addPredecessor(c1);		// 2.1 comes after 3.1
		b1->addPredecessor(b2);		// 2.1 comes after 2.2
		a2->addPredecessor(b2);		// 1.2 comes after 2.2
		a->addPredecessor(b);		// 2 and all children come before 1
		c->addPredecessor(a);		// 3 comes after 1
		c1->addPredecessor(a1);		// 3.1 depends on 1.1
		c2->addPredecessor(b2);		// 3.2 depends on 2.2
		c1->addPeer(c2);			// 3.1 must be completed with 3.2

		traverse(graph, "1.1|2.2|3.1 3.2|2.1 2.2 2|1.1 1.2 1|3.1 3.2 3");
	}
}