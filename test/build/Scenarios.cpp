#include <ApprovalTests.hpp>
#include <jadegit/build/Director.h>
#include <jadegit/build/FileSource.h>
#include <vfs/FileSignature.h>
#include <jadegit/vfs/NativeFileSystem.h>
#include <source_location>
#include "Catch.h"
#include "MockBuilder.h"

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Build;

namespace fs = std::filesystem;

void build(filesystem::path path)
{
	auto author = path.filename().string();

	shared_ptr<FileSystem> previous;
	shared_ptr<FileSystem> latest;

	FileSignature previous_signature(author, sys_days{ June / 10 / 2019 } + 9h + 17min);
	FileSignature latest_signature(author, sys_days{ June / 10 / 2020 } + 9h + 17min);

	if (fs::exists((path / "previous").generic_string()))
		previous = make_shared<NativeFileSystem>((path / "previous"), false, &previous_signature);

	if (!previous || fs::exists((path / "latest").generic_string()))
		latest = make_shared<NativeFileSystem>((path / "latest"), false, &latest_signature);

	// Common code for sections handling builds for upgrade/downgrade below
	auto section = [path](const string& section, const shared_ptr<FileSystem>& previous, const shared_ptr<FileSystem>& latest)
	{
		vector<string> platforms;
		platforms.push_back("");

		for (const auto& file : fs::directory_iterator(path))
		{
			if (file.is_directory())
				continue;

			auto stem = file.path().filename().stem().string();
			if (stem.starts_with(section))
				if (auto pos = stem.find('.'); pos != string::npos)
					platforms.push_back(stem.substr(pos + 1));
		}

		for (auto& platform : platforms)
		{
			SECTION(platform.empty() ? section : (section + "." + platform))
			{
				MockBuilder builder(platform);
				Director(FileSource(previous, latest)).Build(builder);

				auto namer = ApprovalTests::TemplatedCustomNamer::create(path.generic_string() + "/" + section + (platform.empty() ? "" : "." + platform) + ".{ApprovedOrReceived}");
				ApprovalTests::Approvals::verify(builder.fs, ApprovalTests::Options().withNamer(namer));
			}
		}
	};

	section("build", previous, latest);

	if (previous && latest)
		section("rollback", latest, previous);
}

class BuildScenariosRegistrar
{
public:
	BuildScenariosRegistrar()
	{
		for (const auto& klass : fs::directory_iterator(filesystem::path(source_location::current().file_name()).parent_path() / "scenarios"))
		{
			if (!klass.is_directory())
				continue;

			for (const auto& scenario : fs::directory_iterator(klass.path()))
			{
				filesystem::path path = scenario.path();

				if (!fs::exists(path / "latest") &&
					!fs::exists(path / "previous"))
					continue;

				REGISTER_TEST_CASE([path](){ build(path); }, std::format("{}.{}", path.parent_path().filename().string(), path.filename().string()), "[build]");
			}
		}
	}
};
static BuildScenariosRegistrar registrar;