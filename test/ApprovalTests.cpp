#include <Approvals.h>
#include <ApprovalTests/namers/TemplatedCustomNamer.h>
#include <ApprovalTests/integrations/FrameworkIntegrations.h>
#include <catch2/catch_test_macros.hpp>
#include <catch2/reporters/catch_reporter_event_listener.hpp>
#include <catch2/reporters/catch_reporter_registrars.hpp>
#include <catch2/catch_test_case_info.hpp>
#include <filesystem>
#include <iostream>

using namespace std;

auto approvalsDirectory = ApprovalTests::Approvals::useApprovalsSubdirectory("approvals");
auto approvalsNamer = ApprovalTests::TemplatedCustomNamer::useAsDefaultNamer("{TestSourceDirectory}/{ApprovalsSubdirectory}/{TestFileName}/{TestCaseName}.{ApprovedOrReceived}");

class Catch2ApprovalListener : public Catch::EventListenerBase
{
    ApprovalTests::TestName currentTest;
    using EventListenerBase::EventListenerBase;

    void testCaseStarting(Catch::TestCaseInfo const& testInfo) final
    {
        currentTest.setFileName(testInfo.lineInfo.file);
        ApprovalTests::FrameworkIntegrations::setCurrentTest(&currentTest);
        ApprovalTests::FrameworkIntegrations::setTestPassedNotification([]() { REQUIRE(true); });
    }

    void testCaseEnded(Catch::TestCaseStats const& /*testCaseStats*/) final
    {
        currentTest.sections.clear();
    }

    void sectionStarting(Catch::SectionInfo const& sectionInfo) final
    {
        // Add first section without filename prefix
        if (currentTest.sections.empty())
        {
            auto pos = sectionInfo.name.find(".");
            auto prefix = sectionInfo.name.substr(0, pos);
            auto file = filesystem::path(currentTest.getFileName()).filename().stem();
            if (prefix == file)
            {
                if (pos != string::npos)
                    currentTest.sections.push_back(sectionInfo.name.substr(pos + 1));
                return;
            }
        }

        currentTest.sections.push_back(sectionInfo.name);
    }

    void sectionEnded(Catch::SectionStats const& /*sectionStats*/) final
    {
        if (!currentTest.sections.empty())
            currentTest.sections.pop_back();
    }
};
CATCH_REGISTER_LISTENER(Catch2ApprovalListener)