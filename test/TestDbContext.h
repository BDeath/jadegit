#pragma once
#include <jade/DbContext.h>
#include <filesystem>

namespace JadeGit
{
	class TestDbContext : public Jade::DbContext
	{
	public:
		TestDbContext();

	private:
		TestDbContext(const std::filesystem::path& path);
	};
}