#pragma once
#include <string>

namespace JadeGit
{
	void log(const std::string& message);
	void log(const std::exception& exception);
}

#define __LOG(s) JadeGit::log(s)
#define LOG(s)					\
	{							\
	   std::stringstream os;	\
	   os << s;					\
	   __LOG(os.str());			\
	}

#define LOG_WARNING(s)	LOG("[Warning] " << s)
#if defined(_DEBUG)
#define LOG_DEBUG(s)	LOG(" <debug> " << s)
#else
#define LOG_DEBUG(s)
#endif
#if defined(_DEBUG) && defined(_TRACE)
#define LOG_TRACE(s)	LOG(" <trace> " << s)
#else
#define LOG_TRACE(s)
#endif