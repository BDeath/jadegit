#pragma once
#include <stdexcept>

namespace JadeGit
{
	void print_exception(std::ostream& output, const std::exception& e);
	void print_exception_with_nested(std::ostream& output, const std::exception& e);
	void print_nested_exceptions(std::ostream& output, const std::exception& e, int level = 0);

	class operation_aborted : public std::runtime_error
	{
	public:
		operation_aborted();
	};

	class unimplemented_feature : public std::runtime_error
	{
	public:
		unimplemented_feature();
		unimplemented_feature(const std::string& feature);
	};

	class AnnotatedVersion;

	class unsupported_feature : public std::runtime_error
	{
	public:
		unsupported_feature();
		unsupported_feature(const std::string& feature);
		unsupported_feature(const std::string& feature, const AnnotatedVersion& requiredVersion);
	};
}