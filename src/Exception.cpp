#include <Exception.h>
#include <Platform.h>

using namespace std;

namespace JadeGit
{
	void print_exception(ostream& output, const exception& e)
	{
		output << e.what() << endl;
	}

	void print_exception_with_nested(ostream& output, const exception& e)
	{
		print_exception(output, e);
		print_nested_exceptions(output, e, 1);
	}

	void print_nested_exceptions(ostream& output, const exception& e, int level)
	{
		try
		{
			rethrow_if_nested(e);
		}
		catch (const exception& nested)
		{
			output << string(level, ' ');
			print_exception(output, nested);
			print_nested_exceptions(output, nested, level + 1);
		}
	}

	operation_aborted::operation_aborted() : runtime_error("Operation aborted by user") {}

	unimplemented_feature::unimplemented_feature() : unimplemented_feature("Feature") {}
	unimplemented_feature::unimplemented_feature(const string& feature) : runtime_error(feature + " is not available as support for this has not been implemented in this release") {}
	
	unsupported_feature::unsupported_feature() : unsupported_feature("Feature") {}
	unsupported_feature::unsupported_feature(const string& feature) : runtime_error(feature + " is not supported") {}
	unsupported_feature::unsupported_feature(const string& feature, const AnnotatedVersion& requiredVersion) : runtime_error(format("{} is not supported (requires {})", feature, static_cast<string>(requiredVersion))) {}
}

#if USE_GIT2
#include <jadegit/git2.h>
#include <git2/sys/errors.h>

namespace JadeGit
{
	git_exception::git_exception(int code, const git_error* error) : runtime_error(format("{} ({})", error ? error->message : "Missing error message", code)), code(code)
	{
		git_error_clear();
	}
}

#endif