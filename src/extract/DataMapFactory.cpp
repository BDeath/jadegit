#include "Assembly.h"
#include "DataMapFactory.h"
#include "DataTranslator.h"
#include "Type.h"
#include <jadegit/data/Class.h>
#include <jadegit/data/EntityFactory.h>
#include <Log.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Extract
{
	void DataMapFactory::Register(ClassNumber key, const Mapper* registrar)
	{
		if (registry.find(key) != registry.end())
			throw logic_error("Duplicate extract data map detected");

		registry[key] = registrar;
	}

	DataMap* DataMapFactory::Create(const DskClass& klass, Assembly& assembly) const
	{
		if (klass.isNull())
			return nullptr;

		// Retrieve class number
		ClassNumber classNo;
		jade_throw(klass.getNumber(&classNo));

		// Check for existing data map
		DataMap* map = assembly.dataMaps[classNo];
		if (map) return map;

		// Resolve root type/subject (cannot map subschema copies)
		DskClass subject;
		jade_throw(klass.getRootType(subject));

		// Retrieve superclass to create base mapping
		DskClass super;
		jade_throw(subject.getSuperclass(super));

		// Create using mapper
		auto iter = registry.find(classNo);
		if (iter != registry.end())
			map = iter->second->Create(assembly, Create(super, assembly));
		else
		{
			// Resolve data class
			Data::Class& data = static_cast<Data::Class&>(Class(subject.oid).resolve(assembly, false));

			// Create map
			map = new DataMap(Create(super, assembly), data);
		}

		// Cache map
		assembly.dataMaps[classNo] = map;

		// Populate properties which haven't been explicitly mapped using default filtering rules
		JomClassFeatureLevel feature = { classNo, 0, map->level };
		OrderedJomColl properties;
		jade_throw(subject.properties(properties));

		JomObjp object = nullptr;
		properties.start();
		while (properties.next(object))
		{
			DskProperty* property = (DskProperty*)object;
			property->getNumber(&feature.number);

			// Check for explicit mapping, ignoring if excluded
			DataMapping* mapping = nullptr;
			if (map->GetMapping(feature, mapping) && !mapping)
				continue;

			// Retrieve property name
			Character name[101];
			jade_throw(property->getName(name, __LINE__));

			// Handle filtering using default rules & resolving properties by name
			if(!mapping)
			{
				// Ignore virtual properties
				bool virtual_;
				jade_throw(property->getVirtual(&virtual_));
				if (virtual_)
					continue;

				AccessType access;
				jade_throw(property->getSubAccess(&access));

				// Ignore system only properties
				if (access == SUBACCESS_SYSTEM_ONLY)
					continue;

				ReferenceKind kind = ReferenceKind::PEER;

				if (property->isExplicitInverseRef())
				{
					const DskExplicitInverseRef* reference = (const DskExplicitInverseRef*)property;

					// Get kind of reference on this side
					jade_throw(reference->getKind(&kind));

					// Ignore references to parent
					if (kind == ReferenceKind::CHILD)
						continue;

					// Get update mode on this side
					InverseUpdateMode updateMode;
					jade_throw(reference->getUpdateMode(&updateMode));

					// Ignore automatic peers by default
					if (kind == ReferenceKind::PEER && updateMode == InverseUpdateMode::AUTOMATIC_UPDATE)
						continue;
				}

				// Ignore peer relationship with non-entity
				if (property->isReference() && kind == ReferenceKind::PEER)
				{
					DskType type;
					jade_throw(property->getType(type));
					if (type.isCollClass())
						jade_throw(DskCollClass(&type.oid).getMemberType(type));
					assert(!type.isNull());

					// Ignore interface references
					if (type.isJadeInterface() || type.isImportedJadeInterface())
						continue;

					// Lookup data type
					auto data = Data::EntityFactory::Get().Resolve<Data::Type>(&assembly, Type(type.oid).GetQualifiedName().get(), false);
					if (!data)
					{
						LOG_WARNING("Ignored peer relationship with unknown data type during extract [" << map->cls.name << "::" << narrow(name) << "]");
						continue;
					}
					else
					{
						// Determine whether data type represents a supported entity
						Data::Class* dataClass = nullptr;
						if (!Data::EntityFactory::Get().Lookup(data->name, &data->getOriginal(), dataClass, false))
						{
							LOG_WARNING("Ignored peer relationship with non-entity during extract [" << map->cls.name << "::" << narrow(name) << "]");
							continue;
						}
					}
				}

				// Lookup data property (don't for those prefixed with an underscore, need to be explicitly mapped)
				const Data::Property* data = (name[0] == TEXT('_') ? nullptr : map->cls.tryGetProperty(narrow(name)));

				// Log warning & ignore if property wasn't found (except for children which don't depend on a property)
				if (!data && kind != ReferenceKind::PARENT)
				{
					LOG_WARNING("Ignored unknown property during extract [" << map->cls.name << "::" << narrow(name) << "]");
					continue;
				}

				// Add property to data mappings
				mapping = map->IncludeProperty(feature, data, nullptr, nullptr);
			}

			assert(mapping);

			// Store property name
			mapping->name = narrow(name);

			// Resolve associated translator needed to copy property
			if (!mapping->translator)
			{
				if (property->isAttribute())
					mapping->translator = AttributeTranslator::Instance();
				else
				{
					assert(property->isReference());

					ReferenceKind kind = ReferenceKind::PEER;
					if (property->isExplicitInverseRef())
						jade_throw(((const DskExplicitInverseRef*)property)->getKind(&kind));

					DskType type;
					jade_throw(property->getType(type));

					if (kind == PARENT)
					{
						if (type.isCollClass())
							mapping->translator = ChildCollectionTranslator::Instance();
						else
							mapping->translator = ChildReferenceTranslator::Instance();
					}
					else
					{
						if (type.isCollClass())
							mapping->translator = PeerCollectionTranslator::Instance();
						else
							mapping->translator = PeerReferenceTranslator::Instance();
					}
				}
			}
		}

		// Return new map created
		return map;
	}
}