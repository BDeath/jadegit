#pragma once
#include "Object.h"
#include <jadegit/data/Entity.h>

namespace JadeGit::Extract
{
	// Entities are objects which can be resolved by name
	class Entity : public Object
	{
	public:
		static Data::Entity* resolve(Data::Assembly& assembly, const DskObjectId& oid, bool shallow = true);

		using Object::Object;

		virtual std::string getName() const = 0;

		virtual ClassNumber GetKind() const;

		std::string GetDisplay() const override;

		// Find the source object
		bool find(const Entity* ancestor, const QualifiedName* path, bool required);
		bool find(const Entity* ancestor, const QualifiedName& path, bool required);

		// Extract from source to destination assembly
		virtual void extract(Assembly& assembly, bool deep) const;

		// Populates set with all associate entities which are deleted implicitly with receiver 
		virtual void getAssociates(std::set<DskObjectId>& associates) const {};

		// Populates set with all child entities
		virtual void children(std::set<DskObjectId>& children) const {};

		// Populates set with dependent entities which refer to receiver
		virtual void dependents(std::set<DskObjectId>& dependents) const {};

		// Populates set with all dependent entities which are impacted by rename
		void dependentsAll(std::set<DskObjectId>& dependents, const DskObjectId& ancestor = NullDskObjectId) const;

		// Populates set with all entities which are renamed implicitly with receiver
		virtual void GetNamesakes(std::set<DskObjectId>& namesakes) const;

		// Retrieve parent entity
		std::unique_ptr<Entity> GetParent() const;
		void GetParentId(DskObjectId& oid) const;
		virtual DskObjectId GetParentId() const = 0;

		// Derives qualified name
		std::unique_ptr<QualifiedName> GetQualifiedName(const Entity* context = nullptr) const;

		// Determines if entity is descendent of ancestor
		virtual bool IsDescendent(const DskObjectId& ancestor) const;

		// Resolves object in assembly, instantiating if it doesn't exist already
		Data::Entity& resolve(Data::Assembly& assembly, bool shallow = true) const;

	protected:
		// Returns basic type name where entities may need to mutate (i.e. ExplicitInverseRef <=> ImplicitInverseRef)
		virtual std::string GetBasicTypeName() const { return GetTypeName(); }

		// Lookup entity for extract
		virtual bool lookup(const Entity* ancestor, const QualifiedName& path) = 0;

		template<class TParent, typename... Rest>
		bool lookup(const Entity* ancestor, const QualifiedName& path, const JomClassFeatureLevel& first, Rest... rest)
		{
			if (const TParent* parent = dynamic_cast<const TParent*>(ancestor))
			{
				assert(!path.parent);
				return lookup(*parent, path, first, rest...);
			}

			// Resolve parent
			TParent parent;
			if (!parent.find(ancestor, path.parent.get(), false))
				return false;
			
			// Resolve child
			return lookup(parent, path, first, rest...);
		}

		template<class TParent, typename... Rest>
		bool lookup(const TParent& parent, const QualifiedName& path, const JomClassFeatureLevel& first, Rest... rest)
		{
			// Lookup child via current parent version
			if (lookup(parent, path.name, first, rest...))
				return true;

			// Lookup child via latest parent version
			if (parent.getVersionState() == ObjectVersionState::OBJVERSTATE_VERSIONED_CURRENT)
			{
				TParent latest;
				jade_throw(parent.getNextVersionObject(&latest));
				if (!latest.isNull() && lookup(latest, path.name, first, rest...))
					return true;
			}

			return false;
		}

		// Resolves child object in the context of parent supplied
		Data::Entity* resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const override;

	private:
		template<class TParent>
		bool lookup(const TParent& parent, const std::string& name, const JomClassFeatureLevel& first)
		{
			// Get collection
			DskMemberKeyDictionary collection;
			jade_throw(parent.getProperty(first, collection));

			// Lookup child
			jade_throw(collection.getAtKey(Jade::widen(name).c_str(), *this));
			return !isNull();
		}

		template<class TParent, typename... Rest>
		bool lookup(const TParent& parent, const std::string& name, const JomClassFeatureLevel& first, Rest... rest)
		{
			// Lookup in first or subsequent collections
			return lookup(parent, name, first) || lookup(parent, name, rest...);
		}
	};
}