#include "Assembly.h"
#include "Entity.h"
#include "EntityFactory.h"
#include "SuppressUserMethods.h"
#include <jadegit/data/EntityFactory.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Extract
{
	Data::Entity* Entity::resolve(Data::Assembly& assembly, const DskObjectId& oid, bool shallow)
	{
		return oid.isNull() ? nullptr : &EntityFactory::Get().Create(oid)->resolve(assembly, shallow);
	}

	ClassNumber Entity::GetKind() const
	{
		return actualClass(oid.classNo);
	}

	string Entity::GetDisplay() const
	{
		return Object::GetDisplay() + " [" + string(*GetQualifiedName()) + "]";
	}

	// Populates set with all dependent entities which are impacted by rename
	void Entity::dependentsAll(set<DskObjectId>& dependents, const DskObjectId& ancestor) const
	{
		// Handle direct dependencies
		if (ancestor.isNull())
		{
			this->dependents(dependents);
		}
		// Handle indirect dependencies
		else
		{
			set<DskObjectId> indirect;
			this->dependents(indirect);
			for (auto& dependent : indirect)
			{
				if (!EntityFactory::Get().Create(dependent)->IsDescendent(ancestor))
					dependents.insert(dependent);
			}
		}

		// Handle indirect child dependencies recursively
		set<DskObjectId> children;
		this->children(children);
		for (auto& child : children)
			EntityFactory::Get().Create(child)->dependentsAll(dependents, ancestor.isNull() ? oid : ancestor);
	}

	// Find the source object
	bool Entity::find(const Entity* ancestor, const QualifiedName* path, bool required)
	{
		if (!path)
			throw invalid_argument("Fully qualified path is required");

		if (lookup(ancestor, *path))
			return true;

		if (required)
			throw runtime_error("Cannot find: " + static_cast<string>(*path));

		return false;
	}

	bool Entity::find(const Entity* ancestor, const QualifiedName& path, bool required)
	{
		return find(ancestor, &path, required);
	}

	// Extract from source to destination assembly
	void Entity::extract(Assembly& assembly, bool deep) const
	{
		// Suppress user mapping methods during extract in order to retrieve underlying data
		SuppressUserMethods suppressor;

		// Resolve target
		auto& target = resolve(assembly, false);
		
		// Flag entity and children as being invalid/dirty (to be cleaned up below if not modified in the meantime)
		target.dirty(deep);

		// Copy object details and sub-objects
		Copy(assembly, &target, *GetQualifiedName(), deep, true);

		// Clean up anything still flagged as invalid/dirty
		target.clean();
	}

	// Populates set with all entities which may be renamed implicitly
	void Entity::GetNamesakes(std::set<DskObjectId>& namesakes) const
	{
		namesakes.insert(oid);

		// Get latest entity
		if (getVersionState() == ObjectVersionState::OBJVERSTATE_VERSIONED_CURRENT)
		{
			DskObject object;
			jade_throw(getNextVersionObject(&object));
			assert(!object.isNull());

			// Include latest namesakes provided latest name matches current name
			unique_ptr<Entity> latest = EntityFactory::Get().Create(object.oid);
			if(latest->getName() == getName())
				latest->GetNamesakes(namesakes);
		}
	}

	unique_ptr<Entity> Entity::GetParent() const
	{
		DskObjectId parent = GetParentId();
		if (parent.isNull())
			return nullptr;

		return EntityFactory::Get().Create(parent);
	}

	void Entity::GetParentId(DskObjectId& oid) const
	{
		oid = GetParentId();
	}

	unique_ptr<QualifiedName> Entity::GetQualifiedName(const Entity* context) const
	{
		unique_ptr<QualifiedName> result = make_unique<QualifiedName>(getName());
		assert(!result->parent);

		QualifiedName* child = result.get();
		std::unique_ptr<Entity> parent = GetParent();
		while (parent && (!context || !context->IsDescendent(parent->oid)))
		{
			child->parent = make_unique<QualifiedName>(parent->getName());
			child = child->parent.get();

			parent = parent->GetParent();
		}

		return result;
	}

	bool Entity::IsDescendent(const DskObjectId& ancestor) const
	{
		if (oid == ancestor)
			return true;

		DskObjectId parent = GetParentId();
		if (parent == ancestor)
			return true;

		if (parent.isNull())
			return false;

		return EntityFactory::Get().Create(parent)->IsDescendent(ancestor);
	}

	// Resolves object in assembly, instantiating if required
	Data::Entity& Entity::resolve(Data::Assembly& assembly, bool shallow) const
	{
		std::unique_ptr<Entity> parent = GetParent();
		return *resolve(assembly, parent ? &(parent->resolve(assembly)) : static_cast<Data::Component*>(&assembly), shallow);
	}

	Data::Entity* Entity::resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const
	{
		/* NOTE: Originally, this would only return an existing entity if we weren't adding or the entity
		   found wasn't 'presumed' that is, we'd just instantiated it in the current transaction.  Change
		   was made to always return existing because JADE sometimes indicates an add rather than update
		   and there may be scenarios where an entity is deleted outside of source control before re-add */

		// Resolve child entity, mutating into new type if found
		auto name = getName();
		if (auto entity = Data::EntityFactory::Get().Resolve(GetBasicTypeName(), parent, QualifiedName(name), false, shallow, false))
			return entity->Mutate(GetTypeName());

		// Instantiate new entity, inferred when receiver may not be extracted because it's a shallow copy/proxy
		return Data::EntityFactory::Get().Create(GetTypeName(), parent, name.c_str())->inferred(this->isShallow());
	}
}