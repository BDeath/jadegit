#include "LocaleFormat.h"
#include "Schema.h"
#include "EntityRegistration.h"
#include "DataMapper.h"
#include <jadegit/data/RootSchema/DateFormatMeta.h>
#include <jadegit/data/RootSchema/CurrencyFormatMeta.h>
#include <jadegit/data/RootSchema/TimeFormatMeta.h>

using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static DataMapper<DateFormatMeta> dateFormatMapper(DSKDATEFORMAT, &RootSchema::dateFormat, {
		{PRP_DateFormat_activeCalendarType, nullptr},
		{PRP_DateFormat_longDayNames, nullptr},
		{PRP_DateFormat_longFormat, nullptr},
		{PRP_DateFormat_longMonthNames, nullptr},
		{PRP_DateFormat_optionalCalendarType, nullptr},
		{PRP_DateFormat_shortDayNames, new DataProperty(&DateFormatMeta::shortDayNames)},
		{PRP_DateFormat_shortFormat, nullptr},
		{PRP_DateFormat_shortMonthNames, nullptr}
		});

	static DataMapper<NumberFormatMeta> numberFormatMapper(DSKNUMBERFORMAT, &RootSchema::numberFormat, {
		{PRP_NumberFormat_negativeSign, nullptr},
		{PRP_NumberFormat_positiveSign, nullptr}
		});

	static DataMapper<CurrencyFormatMeta> currencyFormatMapper(DSKCURRENCYFORMAT, &RootSchema::currencyFormat, {
		{PRP_CurrencyFormat_intlCurrencySymbol, nullptr},
		{PRP_CurrencyFormat_intlDecimalPlaces, nullptr},
		{PRP_CurrencyFormat_negSymbolPrecedesAmount, nullptr},
		{PRP_CurrencyFormat_negSymbolSeparated, nullptr},
		{PRP_CurrencyFormat_negativeSignPosition, nullptr},
		{PRP_CurrencyFormat_posSymbolPrecedesAmount, nullptr},
		{PRP_CurrencyFormat_posSymbolSeparated, nullptr}
		});

	static DataMapper<TimeFormatMeta> timeFormatMapper(DSKTIMEFORMAT, &RootSchema::timeFormat, {
		{PRP_TimeFormat_format, nullptr}
		});

	static EntityRegistration<LocaleFormat> registrar(DSKLOCALEFORMAT);

	bool LocaleFormat::lookup(const Entity* ancestor, const QualifiedName& path)
	{
		return Entity::lookup<Schema>(ancestor, path, PRP_Schema_userFormats);
	}
}