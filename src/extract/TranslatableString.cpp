#include "Constant.h"
#include "DataMapper.h"
#include "Locale.h"
#include "EntityRegistration.h"
#include <jadegit/data/RootSchema/TranslatableStringMeta.h>

using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static DataMapper<TranslatableStringMeta> translatableStringMapper(DSKTRANSLATABLESTRING, &RootSchema::translatableString, {
		{PRP_TranslatableString_code, nullptr},
		{PRP_TranslatableString_paramCount, nullptr},
		{PRP_TranslatableString_retransReqd, nullptr}
		});

	class TranslatableString : public Constant
	{
	public:
		using Constant::Constant;

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			return Entity::lookup<Locale>(ancestor, path, PRP_Locale_translatableStrings);
		}
	};
	static EntityRegistration<TranslatableString> registrar(DSKTRANSLATABLESTRING);
}