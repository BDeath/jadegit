#include "Type.h"
#include "Property.h"
#include "DataMapper.h"
#include "EntityRegistration.h"
#include <jadegit/data/RootSchema/ExternalClassMapMeta.h>
#include <Exception.h>

using namespace std;
using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static DataMapper<ExternalSchemaMapMeta> externalSchemaMapMapper(DSKEXTERNALSCHEMAMAP, &RootSchema::externalSchemaMap, {
		{PRP_ExternalSchemaMap_schemaEntity, new DataProperty(&ExternalSchemaMapMeta::schemaEntity)}
		});

	static DataMapper<ExternalClassMapMeta> externalClassMapMapper(DSKEXTERNALCLASSMAP, &RootSchema::externalClassMap, {
		{PRP_ExternalClassMap_attributeMaps, new DataProperty<ExternalClassMapMeta>(nullptr)},
		{PRP_ExternalClassMap_database, nullptr},
		{PRP_ExternalClassMap_referenceMaps, new DataProperty<ExternalClassMapMeta>(nullptr)},
		{PRP_ExternalClassMap_tables, new DataProperty(&ExternalClassMapMeta::tables)},
		});

	static DataMapper<ExternalAttributeMapMeta> externalAttributeMapMapper(DSKEXTERNALATTRIBUTEMAP, &RootSchema::externalAttributeMap, {
		{PRP_ExternalAttributeMap_classMap, nullptr},
		{PRP_ExternalAttributeMap_column, new DataProperty(&ExternalAttributeMapMeta::column)}
		});

	static DataMapper<ExternalReferenceMapMeta> externalReferenceMapMapper(DSKEXTERNALREFERENCEMAP, &RootSchema::externalReferenceMap, {
		{PRP_ExternalReferenceMap_classMap, nullptr},
		{PRP_ExternalReferenceMap_dirnLeftRight, new DataProperty(&ExternalReferenceMapMeta::dirnLeftRight)},
		{PRP_ExternalReferenceMap_leftColumns, new DataProperty(&ExternalReferenceMapMeta::leftColumns)},
		{PRP_ExternalReferenceMap_otherRefMap, new DataProperty(&ExternalReferenceMapMeta::otherRefMap)},
		{PRP_ExternalReferenceMap_relType, new DataProperty(&ExternalReferenceMapMeta::relType)},
		{PRP_ExternalReferenceMap_rightColumns, new DataProperty(&ExternalReferenceMapMeta::rightColumns)},
		{PRP_ExternalReferenceMap_wherePredicate, new DataProperty(&ExternalReferenceMapMeta::wherePredicate)}
		});

	template <class TOriginal, auto parent>
	class ExternalSchemaMap : public Entity
	{
	public:
		using Entity::Entity;

		string getName() const final
		{
			return getProperty<TOriginal>(PRP_ExternalSchemaMap_schemaEntity).getName();
		}

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) final
		{
			throw unimplemented_feature();
		}

		DskObjectId GetParentId() const override
		{
			return getProperty<DskObjectId>(*parent);
		}
	};

	using ExternalClassMap = ExternalSchemaMap<Class, &PRP_ExternalClassMap_database>;
	using ExternalCollClassMap = ExternalSchemaMap<Class, &PRP_ExternalCollClassMap_database>;
	using ExternalAttributeMap = ExternalSchemaMap<Property, &PRP_ExternalAttributeMap_classMap>;
	using ExternalReferenceMap = ExternalSchemaMap<Property, &PRP_ExternalReferenceMap_classMap>;

	static EntityRegistration<ExternalClassMap> externalClassMap(DSKEXTERNALCLASSMAP);
	static EntityRegistration<ExternalCollClassMap> externalCollClassMap(DSKEXTERNALCOLLCLASSMAP);
	static EntityRegistration<ExternalAttributeMap> externalAttributeMap(DSKEXTERNALATTRIBUTEMAP);
	static EntityRegistration<ExternalReferenceMap> externalReferenceMap(DSKEXTERNALREFERENCEMAP);
}