#include "Schema.h"
#include "DataMapper.h"
#include "EntityRegistration.h"
#include <jadegit/data/RootSchema/SchemaMeta.h>

using namespace Jade;
using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static DataMapper<SchemaMeta> schemaMapper(DSKSCHEMA, &RootSchema::schema, {
		{PRP_Schema__appClass, nullptr},
		{PRP_Schema__applications, new DataProperty<SchemaMeta>(nullptr)},
		{PRP_Schema__classesByNumber, nullptr},
		{PRP_Schema__exposedLists, new DataProperty<SchemaMeta>(nullptr)},
		{PRP_Schema__globalClass, nullptr},
		{PRP_Schema__isBeingDeleted, nullptr},
		{PRP_Schema__jadeHTMLDocuments, new DataProperty<SchemaMeta>(nullptr)},
		{PRP_Schema__jadePatches, nullptr},
		{PRP_Schema__localesByNumber, nullptr},
		{PRP_Schema__patchVersioningEnabled, nullptr},
		{PRP_Schema__permanent, nullptr},
		{PRP_Schema__primitivesByNumber, nullptr},
		{PRP_Schema__pseudoTypesByNumber, nullptr},
		{PRP_Schema__systemBasic, nullptr},
		{PRP_Schema__userFormatsByNumber, nullptr},
		{PRP_Schema__webClass, nullptr},
		{PRP_Schema_activeXLibraries, new DataProperty<SchemaMeta>(nullptr)},
		{PRP_Schema_appliedPatches, nullptr},
		{PRP_Schema_classes, new DataProperty<SchemaMeta>(nullptr)},
		{PRP_Schema_constantCategories, new DataProperty<SchemaMeta>(nullptr)},
		{PRP_Schema_consts, new DataProperty<SchemaMeta>(nullptr)},
		{PRP_Schema_databases, new DataProperty<SchemaMeta>(nullptr)},
		{PRP_Schema_exportedPackages, new DataProperty<SchemaMeta>(nullptr)},
		{PRP_Schema_functions, new DataProperty<SchemaMeta>(nullptr)},
		{PRP_Schema_importedPackages, new DataProperty<SchemaMeta>(nullptr)},
		{PRP_Schema_incomplete, nullptr},
		{PRP_Schema_interfaces, new DataProperty<SchemaMeta>(nullptr)},
		{PRP_Schema_jomVersion, nullptr},
		{PRP_Schema_libraries, new DataProperty<SchemaMeta>(nullptr)},
		{PRP_Schema_locales, new DataProperty<SchemaMeta>(nullptr)},
		{PRP_Schema_name, nullptr},		// Named on creation
		{PRP_Schema_needsReorg, nullptr},
		{PRP_Schema_nextClassNumber, nullptr},
		{PRP_Schema_number, nullptr},
		{PRP_Schema_patchVersion, nullptr},
		{PRP_Schema_primaryLocale, new DataProperty(&SchemaMeta::primaryLocale)},
		{PRP_Schema_primitives, new DataProperty<SchemaMeta>(nullptr)},
		{PRP_Schema_schemaViews, nullptr},		// Schema views are currently ignored (incomplete/inconsistent patch control & schema load behaviour)
		{PRP_Schema_status, nullptr},
		{PRP_Schema_subschemas, nullptr},
		{PRP_Schema_superschema, new DataProperty(&SchemaMeta::superschema)},
		{PRP_Schema_userFormats, new DataProperty<SchemaMeta>(nullptr)},
		{PRP_Schema_uuid, nullptr}
		});

	static EntityRegistration<Schema> registrar(DSKSCHEMA);

	bool Schema::isSupplied(const std::string& name)
	{
		// Schemas supplied/maintained by JADE
		if (name == "CardSchema" || name == "JadeReportWriterSchema")
			return true;

		// Our schema is also supplied
		return name == "JadeGitSchema";
	}

	std::string Schema::getName() const
	{
		return getProperty<std::string>(PRP_Schema_name);
	}

	bool Schema::lookup(const Entity* ancestor, const QualifiedName& path)
	{
		assert(!ancestor);
		assert(!path.parent);

		DskSchema rootSchema(&RootSchemaOid);
		DskSchema schema;
		jade_throw(rootSchema.findSchema(widen(path.name).c_str(), schema));
		oid = schema.oid;
		return !isNull();
	}

	bool Schema::IsDescendent(const DskObjectId& ancestor) const
	{
		if (Entity::IsDescendent(ancestor))
			return true;

		if (ancestor.classNo == DSKSCHEMA)
		{
			DskSchema superschema;
			jade_throw(getProperty(PRP_Schema_superschema, superschema));
			while (!superschema.isNull())
			{
				if (superschema.oid == ancestor)
					return true;
				jade_throw(superschema.getSuperschema(superschema));
			}
		}

		return false;
	}

	Data::Schema* Schema::resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const
	{
		// Resolve schema (creating if required)
		Data::Entity* entity = Entity::resolve(assembly, parent, shallow);
		assert(dynamic_cast<Data::Schema*>(entity));

		Data::Schema* schema = static_cast<Data::Schema*>(entity);

		// Setup superschema if required
		if (!schema->superschema)
		{
			Schema superschema;
			jade_throw(getProperty(PRP_Schema_superschema, superschema));
			if (!superschema.isNull())
				schema->superschema = static_cast<Data::Schema&>(superschema.resolve(assembly));
		}

		return schema;
	}

	void Schema::children(std::set<DskObjectId>& children) const
	{
		Entity::children(children);

		getProperty(PRP_Schema_activeXLibraries, children);
		getProperty(PRP_Schema_classes, children);
		getProperty(PRP_Schema_constantCategories, children);
		getProperty(PRP_Schema_consts, children);
		getProperty(PRP_Schema_databases, children);
		getProperty(PRP_Schema_exportedPackages, children);
		getProperty(PRP_Schema_externalDatabases, children);
		getProperty(PRP_Schema_functions, children);
		getProperty(PRP_Schema_importedPackages, children);
		getProperty(PRP_Schema_interfaces, children);
		getProperty(PRP_Schema_libraries, children);
		getProperty(PRP_Schema_locales, children);
		getProperty(PRP_Schema_primitives, children);
		getProperty(PRP_Schema_relationalViews, children);
		getProperty(PRP_Schema_rpsDatabases, children);
		getProperty(PRP_Schema_userFormats, children);
	}

	void Schema::dependents(std::set<DskObjectId>& dependents) const
	{
		Entity::dependents(dependents);

		// Direct dependencies
		getProperty(PRP_Schema_subschemas, dependents);
	}
}