#include "Assembly.h"
#include "DataMap.h"
#include "SchemaIterator.h"
#include <data/storage/ObjectFileStorage.h>
#include <Platform.h>
#include <regex>

using namespace std;

namespace JadeGit::Extract
{
	Assembly::Assembly(const FileSystem& source) : Data::Assembly(source, jadeVersion)
	{
	}

	Assembly::~Assembly()
	{
		for (auto maps : dataMaps)
			delete maps.second;
	}

	void Assembly::extract(const vector<string>& schemas, function<void(const string&)> print)
	{
		// Treat schemas specified as regular expressions
		vector<regex> patterns;
		for (auto& pattern : schemas)
			patterns.push_back(regex(pattern));

		// Define filter (returns true for schemas to be extracted)
		auto filter = [&](const std::string& name)
		{
			// Supplied schemas which are generally excluded by source control must be explicitly specified for extract
			if (Schema::isSupplied(name))
			{
				if (std::find(schemas.begin(), schemas.end(), name) != schemas.end())
					return true;
			}
			// Include all schemas otherwise if none explicitly specified
			else if (schemas.empty())
			{
				return true;
			}
			else 
			{
				for (const auto& pattern : patterns)
				{
					if (regex_match(name, pattern))
						return true;
				}
			}

			return false;
		};

		// Iterate schemas in dependency order
		Schema schema;
		SchemaIterator iter;
		bool acyclic = false;
		bool extracted = false;
		while (iter.next(schema, acyclic))
		{
			auto name = schema.getName();

			// Perform deep extract for schema if included
			if (filter(name))
			{
				if (print) print("Extracting " + name);
				schema.extract(*this, true);

				extracted = true;
			}

			// Save & unload schemas to recover memory, provided schema just extracted isn't part of a cyclic dependency
			if (extracted && acyclic)
			{
				save(true);
				extracted = false;
			}
		}
		assert(!extracted);

		// Handle schema removal when all schemas are being extracted
		if (schemas.empty())
		{
			vector<Data::Schema*> missing;
			getStorage().loadChildren(filesystem::path(), false);
			for (auto& schema : this->schemas)
			{
				// Ignore static schemas
				if (schema.second->isStatic())
					continue;

				// Ignore schemas that exist (in current database anyways).
				if (Schema().find(nullptr, schema.second->name, false))
					continue;

				// Add to mising list for removal
				missing.push_back(schema.second);
			}
			// Remove missing schemas
			for (auto& schema : missing)
			{
				if (print) print("Removing " + schema->name);
				schema->Delete();
			}
		}
	}

	void Assembly::unload()
	{
		// Unload data maps for non-static schemas which are about to be unloaded
		auto it = dataMaps.begin();
		while (it != dataMaps.end())
		{
			if (!it->second->cls.isStatic())
			{
				delete it->second;
				it = dataMaps.erase(it);
			}
			else
				it++;
		}

		Data::Assembly::unload();
	}
}