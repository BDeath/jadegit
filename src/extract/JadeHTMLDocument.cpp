#include "Schema.h"
#include "DataMapper.h"
#include "EntityRegistration.h"
#include <jadegit/data/RootSchema/JadeHTMLDocumentMeta.h>

using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static DataMapper<JadeHTMLDocumentMeta> jadeHTMLDocumentMapper(DSKJADEHTMLDOCUMENT, &RootSchema::jadeHTMLDocument, {
		{PRP_JadeHTMLDocument_htmlClass, new DataProperty(&JadeHTMLDocumentMeta::htmlClass)},
		{PRP_JadeHTMLDocument_jadeHTMLEntities, nullptr},
		{PRP_JadeHTMLDocument_jadeHTMLInterestingTypes, nullptr},
		{PRP_JadeHTMLDocument_jadeHTMLTypes, nullptr}
		});

	class JadeHTMLDocument : public Entity
	{
	public:
		using Entity::Entity;

		std::string getName() const override
		{
			return getProperty<std::string>(PRP_JadeHTMLDocument_name);
		}

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			return Entity::lookup<Schema>(ancestor, path, PRP_Schema__jadeHTMLDocuments);
		}

		DskObjectId GetParentId() const override
		{
			return getProperty<DskObjectId>(PRP_JadeHTMLDocument_schema);
		}
	};
	static EntityRegistration<JadeHTMLDocument> jadeHTMLDocument(DSKJADEHTMLDOCUMENT);
}