#pragma once
#include "Script.h"

namespace JadeGit::Extract
{
	class Constant : public Script
	{
	public:
		using Script::Script;

		void dependents(std::set<DskObjectId>& dependents) const override;

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override;
	};
}