#pragma once
#include "SchemaEntity.h"

namespace JadeGit::Extract
{
	class ConstantCategory : public SchemaEntity
	{
	public:
		using SchemaEntity::SchemaEntity;

		void dependents(std::set<DskObjectId>& dependents) const override;

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override;
	};
}