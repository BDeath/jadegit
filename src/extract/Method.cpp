#include "Method.h"
#include "DataMapper.h"
#include "EntityRegistration.h"
#include <jadegit/data/RootSchema/MethodMeta.h>
#include <jadegit/data/RootSchema/JadeInterfaceMethodMeta.h>
#include <jadegit/data/RootSchema/JadeMethodMeta.h>
#include <jadegit/data/RootSchema/JadeWebServicesMethodMeta.h>

using namespace std;
using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static EntityRegistration<Method> method(DSKMETHOD);
	static EntityRegistration<JadeInterfaceMethod> interfaceMethod(DSKJADEINTERFACEMETHOD);
	static EntityRegistration<JadeMethod> jadeMethod(DSKJADEMETHOD);

	static DataMapper<MethodMeta> mapper(DSKMETHOD, &RootSchema::method, {
		{PRP_Method_checkedOutMethods, nullptr},
		{PRP_Method_checkedOutUser, nullptr},
		{PRP_Method_condition, new DataProperty(&MethodMeta::condition)},
		{PRP_Method_conditionSafe, new DataProperty(&MethodMeta::conditionSafe)},
		{PRP_Method_controlMethod, new DataProperty(&MethodMeta::controlMethod, [](const DskObject& object) { return Object(&object.oid).getProperty<Character>(PRP_Method_methodInvocation) != 0; })},
		{PRP_Method_delta, nullptr},
		{PRP_Method_exclusiveCheckOut, nullptr},
		{PRP_Method_exportedMethodRefs, nullptr},
		{PRP_Method_final, new DataProperty(&MethodMeta::final)},
		{PRP_Method_methodInvocation, new DataProperty(&MethodMeta::methodInvocation)},
		{PRP_Method_originalMethod, nullptr},
		{PRP_Method_originalSource, nullptr},
		{PRP_Method_partitionMethod, new DataProperty(&MethodMeta::partitionMethod)},
		{PRP_Method_subschemaCopyFinal, new DataProperty(&MethodMeta::subschemaCopyFinal)},
		{PRP_Method_subschemaFinal, new DataProperty(&MethodMeta::subschemaFinal)},
		{PRP_Method_superMethod, nullptr}
		});

	static DataMapper<JadeInterfaceMethodMeta> jadeInterfaceMethodMapper(DSKJADEINTERFACEMETHOD, &RootSchema::jadeInterfaceMethod, {
		{PRP_JadeInterfaceMethod_interfaceImplementors, nullptr}
		});

	static DataMapper<JadeMethodMeta> jadeMethodMapper(DSKJADEMETHOD, &RootSchema::jadeMethod, {
		{PRP_JadeMethod_code, nullptr},
		{PRP_JadeMethod_localVars, nullptr},
		{PRP_JadeMethod_propertyUsages, nullptr},
		{PRP_JadeMethod_referenceRefs, nullptr},
		{PRP_JadeMethod_symbolTable, nullptr}
		});

	static DataMapper<JadeWebServicesMethodMeta> jadeWebServicesMethodMapper(DSKJADEWEBSERVICESMETHOD, &RootSchema::jadeWebServicesMethod, {
		{PRP_JadeWebServicesMethod_inputEncodingStyle, new DataProperty(&JadeWebServicesMethodMeta::inputEncodingStyle)},
		{PRP_JadeWebServicesMethod_inputNamespace, new DataProperty(&JadeWebServicesMethodMeta::inputNamespace)},
		{PRP_JadeWebServicesMethod_inputUsesEncodedFormat, new DataProperty(&JadeWebServicesMethodMeta::inputUsesEncodedFormat)},
		{PRP_JadeWebServicesMethod_outputEncodingStyle, new DataProperty(&JadeWebServicesMethodMeta::outputEncodingStyle)},
		{PRP_JadeWebServicesMethod_outputNamespace, new DataProperty(&JadeWebServicesMethodMeta::outputNamespace)},
		{PRP_JadeWebServicesMethod_outputUsesEncodedFormat, new DataProperty(&JadeWebServicesMethodMeta::outputUsesEncodedFormat)},
		{PRP_JadeWebServicesMethod_soapAction, new DataProperty(&JadeWebServicesMethodMeta::soapAction)},
		{PRP_JadeWebServicesMethod_soapHeaders, new DataProperty(&JadeWebServicesMethodMeta::soapHeaders)},
		{PRP_JadeWebServicesMethod_useBareStyle, new DataProperty(&JadeWebServicesMethodMeta::useBareStyle)},
		{PRP_JadeWebServicesMethod_useSoap11, nullptr},
		{PRP_JadeWebServicesMethod_useSoap12, new DataProperty(&JadeWebServicesMethodMeta::useSoap12)},
		{PRP_JadeWebServicesMethod_usesRPC, new DataProperty(&JadeWebServicesMethodMeta::usesRPC)},
		{PRP_JadeWebServicesMethod_wsdlName, nullptr}	// Superseded by SchemaEntity::_wsdlName
		});

	bool Method::lookup(const Entity* ancestor, const QualifiedName& path)
	{
		return Entity::lookup<Type>(ancestor, path, PRP_Type_methods);
	}

	void Method::dependents(std::set<DskObjectId>& dependents) const
	{
		Routine::dependents(dependents);

		// Direct dependencies
		getProperty(PRP_Method_exportedMethodRefs, dependents);
	}

	string Method::GetBasicTypeName() const
	{
		return "Method";
	}

	void Method::GetNamesakes(set<DskObjectId>& namesakes) const
	{
		Routine::GetNamesakes(namesakes);

		OrderedJomColl implementors;
		jade_throw(valuesFromProperty(PRP_Method_implementors, implementors));

		JomObjp object = nullptr;
		implementors.start();
		while (implementors.next(object))
			Method(((DskObject*)object)->oid).GetNamesakes(namesakes);
	}

	bool JadeInterfaceMethod::lookup(const Entity* ancestor, const QualifiedName& path)
	{
		return Entity::lookup<JadeInterface>(ancestor, path, PRP_Type_methods);
	}

	void JadeInterfaceMethod::dependents(set<DskObjectId>& dependents) const
	{
		Method::dependents(dependents);

		// Direct dependencies
		getProperty(PRP_JadeInterfaceMethod_interfaceImplementors, dependents);
	}

	void JadeMethod::dependents(set<DskObjectId>& dependents) const
	{
		Method::dependents(dependents);

		// Direct dependencies
		getProperty(PRP_JadeMethod_referenceRefs, dependents);
	}
}