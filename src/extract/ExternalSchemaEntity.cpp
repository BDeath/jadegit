#include "ExternalDatabase.h"
#include "DataMapper.h"
#include "EntityRegistration.h"
#include <jadegit/data/RootSchema/ExternalTableMeta.h>

using namespace std;
using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static DataMapper<ExternalSchemaEntityMeta> externalSchemaEntityMapper(DSKEXTERNALSCHEMAENTITY, &RootSchema::externalSchemaEntity, {
		{PRP_ExternalSchemaEntity_name, new DataProperty(&ExternalSchemaEntityMeta::name)},
		{PRP_ExternalSchemaEntity_remarks, new DataProperty(&ExternalSchemaEntityMeta::remarks)},
		{PRP_ExternalSchemaEntity_state, nullptr},
		});

	static DataMapper<ExternalColumnMeta> externalColumnMapper(DSKEXTERNALCOLUMN, &RootSchema::externalColumn, {
		{PRP_ExternalColumn_attributeMaps, nullptr},
		{PRP_ExternalColumn_bufferLength, new DataProperty(&ExternalColumnMeta::bufferLength)},
		{PRP_ExternalColumn_columnSize, new DataProperty(&ExternalColumnMeta::columnSize)},
		{PRP_ExternalColumn_dataType, new DataProperty(&ExternalColumnMeta::dataType)},
		{PRP_ExternalColumn_decimalDigits, new DataProperty(&ExternalColumnMeta::decimalDigits)},
		{PRP_ExternalColumn_kind, new DataProperty(&ExternalColumnMeta::kind)},
		{PRP_ExternalColumn_leftReferenceMaps, nullptr},
		{PRP_ExternalColumn_length, new DataProperty(&ExternalColumnMeta::length)},
		{PRP_ExternalColumn_nullability, new DataProperty(&ExternalColumnMeta::nullability)},
		{PRP_ExternalColumn_ordinalPosition, new DataProperty(&ExternalColumnMeta::ordinalPosition)},
		{PRP_ExternalColumn_rightReferenceMaps, nullptr},
		{PRP_ExternalColumn_scope, new DataProperty(&ExternalColumnMeta::scope)},
		{PRP_ExternalColumn_table, nullptr}
		});

	static DataMapper<ExternalIndexMeta> externalIndexMapper(DSKEXTERNALINDEX, &RootSchema::externalIndex, {
		{PRP_ExternalIndex_cardinality, new DataProperty(&ExternalIndexMeta::cardinality)},
		{PRP_ExternalIndex_duplicatesAllowed, new DataProperty(&ExternalIndexMeta::duplicatesAllowed)},
		{PRP_ExternalIndex_filterCondition, new DataProperty(&ExternalIndexMeta::filterCondition)},
		{PRP_ExternalIndex_keys, new DataProperty<ExternalIndexMeta>(nullptr)},
		{PRP_ExternalIndex_table, nullptr},
		});

	static DataMapper<ExternalIndexKeyMeta> externalIndexKeyMapper(DSKEXTERNALINDEXKEY, &RootSchema::externalIndexKey, {
		{PRP_ExternalIndexKey_column, new DataProperty(&ExternalIndexKeyMeta::column)},
		{PRP_ExternalIndexKey_descending, new DataProperty(&ExternalIndexKeyMeta::descending)},
		{PRP_ExternalIndexKey_index, nullptr},
		{PRP_ExternalIndexKey_keySequence, new DataProperty(&ExternalIndexKeyMeta::keySequence)}
		});

	static DataMapper<ExternalTableMeta> externalTableMapper(DSKEXTERNALTABLE, &RootSchema::externalTable, {
		{PRP_ExternalTable_aliasName, new DataProperty(&ExternalTableMeta::aliasName)},
		{PRP_ExternalTable_cardinality, new DataProperty(&ExternalTableMeta::cardinality)},
		{PRP_ExternalTable_catalogName, new DataProperty(&ExternalTableMeta::catalogName)},
		{PRP_ExternalTable_classMaps, nullptr},
		{PRP_ExternalTable_columns, new DataProperty<ExternalTableMeta>(nullptr)},
		{PRP_ExternalTable_database, nullptr},
		{PRP_ExternalTable_excluded, new DataProperty(&ExternalTableMeta::excluded)},
		{PRP_ExternalTable_foreignKeys, new DataProperty<ExternalTableMeta>(nullptr)},
		{PRP_ExternalTable_indexes, new DataProperty<ExternalTableMeta>(nullptr)},
		{PRP_ExternalTable_primaryKey, new DataProperty<ExternalTableMeta>(nullptr)},
		{PRP_ExternalTable_referencedBy, nullptr},
		{PRP_ExternalTable_referencesTo, nullptr},
		{PRP_ExternalTable_schemaName, new DataProperty(&ExternalTableMeta::schemaName)},
		{PRP_ExternalTable_specialColumns, new DataProperty(&ExternalTableMeta::specialColumns)},
		{PRP_ExternalTable_tableType, new DataProperty(&ExternalTableMeta::tableType)},
		{PRP_ExternalTable_uuid, nullptr}
		});

	class ExternalSchemaEntity : public Entity
	{
	public:
		using Entity::Entity;

		string getName() const final
		{
			return getProperty<string>(PRP_ExternalSchemaEntity_name);
		}
	};

	class ExternalTable;

	class ExternalColumn : public ExternalSchemaEntity
	{
	public:
		using ExternalSchemaEntity::ExternalSchemaEntity;

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) final
		{
			return Entity::lookup<ExternalTable>(ancestor, path, PRP_ExternalTable_columns);
		}

		DskObjectId GetParentId() const final
		{
			return getProperty<DskObjectId>(PRP_ExternalColumn_table);
		}
	};
	static EntityRegistration<ExternalColumn> externalColumn(DSKEXTERNALCOLUMN);

	class ExternalForeignKey : public ExternalSchemaEntity
	{
	public:
		using ExternalSchemaEntity::ExternalSchemaEntity;

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) final
		{
			return Entity::lookup<ExternalTable>(ancestor, path, PRP_ExternalTable_foreignKeys);
		}

		DskObjectId GetParentId() const final
		{
			return getProperty<DskObjectId>(PRP_ExternalForeignKey_table);
		}
	};
	static EntityRegistration<ExternalForeignKey> externalForeignKey(DSKEXTERNALFOREIGNKEY);

	class ExternalIndex : public ExternalSchemaEntity
	{
	public:
		using ExternalSchemaEntity::ExternalSchemaEntity;

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) final
		{
			return Entity::lookup<ExternalTable>(ancestor, path, PRP_ExternalTable_indexes);
		}

		DskObjectId GetParentId() const final
		{
			return getProperty<DskObjectId>(PRP_ExternalIndex_table);
		}
	};
	static EntityRegistration<ExternalIndex> externalIndex(DSKEXTERNALINDEX);

	class ExternalIndexKey : public Object
	{
	public:
		using Object::Object;
	};
	static ObjectRegistration<ExternalIndexKey> externalIndexKey(DSKEXTERNALINDEXKEY);


	class ExternalTable : public ExternalSchemaEntity
	{
	public:
		using ExternalSchemaEntity::ExternalSchemaEntity;

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) final
		{
			return Entity::lookup<ExternalDatabase>(ancestor, path, PRP_ExternalDatabase__tables);
		}

		DskObjectId GetParentId() const final
		{
			return getProperty<DskObjectId>(PRP_ExternalTable_database);
		}
	};
	static EntityRegistration<ExternalTable> externalTable(DSKEXTERNALTABLE);
}