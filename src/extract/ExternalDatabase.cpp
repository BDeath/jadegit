#include "ExternalDatabase.h"
#include "Schema.h"
#include "DataMapper.h"
#include "DataTranslator.h"
#include "EntityRegistration.h"
#include <jadegit/data/RootSchema/ExternalDatabaseMeta.h>
#include <jadegit/data/RootSchema/ExternalDbDriverInfoMeta.h>
#include <jadegit/data/RootSchema/ExternalDbProfileMeta.h>

using namespace std;
using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static DataMapper<ExternalDatabaseMeta> externalDatabaseMapper(DSKEXTERNALDATABASE, &RootSchema::externalDatabase, {
		{PRP_ExternalDatabase__applications, nullptr},
		{PRP_ExternalDatabase__classMaps, new DataProperty<ExternalDatabaseMeta>(nullptr, nullptr, ChildCollectionTranslator::Instance())},
		{PRP_ExternalDatabase__classes, nullptr},
		{PRP_ExternalDatabase__collClassMaps, new DataProperty<ExternalDatabaseMeta>(nullptr, nullptr, ChildCollectionTranslator::Instance())},
		{PRP_ExternalDatabase__collClasses, nullptr},
		{PRP_ExternalDatabase__dataSource, new DataProperty(&ExternalDatabaseMeta::_dataSource)},
		{PRP_ExternalDatabase__dbConnection, nullptr},
		{PRP_ExternalDatabase__driverInfo, new DataProperty<ExternalDatabaseMeta>(nullptr)},
		{PRP_ExternalDatabase__lockProxy, nullptr},
		{PRP_ExternalDatabase__modification, nullptr},
		{PRP_ExternalDatabase__modified, nullptr},																	// TODO: Add check to prevent extracting incomplete database definition
		{PRP_ExternalDatabase__modifiedBy, nullptr},
		{PRP_ExternalDatabase__modifiedTimestamp, nullptr},
		{PRP_ExternalDatabase__patchVersion, nullptr},
		{PRP_ExternalDatabase__profile, new DataProperty<ExternalDatabaseMeta>(nullptr)},
		{PRP_ExternalDatabase__schema, nullptr},
		{PRP_ExternalDatabase__sourceParcel, nullptr},
		{PRP_ExternalDatabase__state, nullptr},
		{PRP_ExternalDatabase__storedProcs, new DataProperty<ExternalDatabaseMeta>(nullptr)},
		{PRP_ExternalDatabase__systemVersion, nullptr},
		{PRP_ExternalDatabase__tables, new DataProperty<ExternalDatabaseMeta>(nullptr)},
		{PRP_ExternalDatabase__version, new DataProperty(&ExternalDatabaseMeta::_version)},
		{PRP_ExternalDatabase_connectionString, new DataProperty(&ExternalDatabaseMeta::connectionString)},
		{PRP_ExternalDatabase_name, new DataProperty(&ExternalDatabaseMeta::name)},
		{PRP_ExternalDatabase_password, new DataProperty(&ExternalDatabaseMeta::password)},
		{PRP_ExternalDatabase_serverName, new DataProperty(&ExternalDatabaseMeta::serverName)},
		{PRP_ExternalDatabase_userName, new DataProperty(&ExternalDatabaseMeta::userName)},
		{PRP_ExternalDatabase_uuid, nullptr}
		});

	static DataMapper<ExternalDbDriverInfoMeta> externalDbDriverInfoMapper(DSKEXTERNALDBDRIVERINFO, &RootSchema::externalDbDriverInfo, {
		{PRP_ExternalDbDriverInfo_catalogLocation, new DataProperty(&ExternalDbDriverInfoMeta::catalogLocation)},
		{PRP_ExternalDbDriverInfo_catalogName, new DataProperty(&ExternalDbDriverInfoMeta::catalogName)},
		{PRP_ExternalDbDriverInfo_catalogNameSeparator, new DataProperty(&ExternalDbDriverInfoMeta::catalogNameSeparator)},
		{PRP_ExternalDbDriverInfo_catalogUsage, new DataProperty(&ExternalDbDriverInfoMeta::catalogUsage)},
		{PRP_ExternalDbDriverInfo_columnAlias, new DataProperty(&ExternalDbDriverInfoMeta::columnAlias)},
		{PRP_ExternalDbDriverInfo_correlationName, new DataProperty(&ExternalDbDriverInfoMeta::correlationName)},
		{PRP_ExternalDbDriverInfo_database, nullptr},
		{PRP_ExternalDbDriverInfo_driverODBCVersion, new DataProperty(&ExternalDbDriverInfoMeta::driverODBCVersion)},
		{PRP_ExternalDbDriverInfo_driverVersion, new DataProperty(&ExternalDbDriverInfoMeta::driverVersion)},
		{PRP_ExternalDbDriverInfo_identifierCase, new DataProperty(&ExternalDbDriverInfoMeta::identifierCase)},
		{PRP_ExternalDbDriverInfo_identifierQuoteChar, new DataProperty(&ExternalDbDriverInfoMeta::identifierQuoteChar)},
		{PRP_ExternalDbDriverInfo_maxCatalogNameLength, new DataProperty(&ExternalDbDriverInfoMeta::maxCatalogNameLength)},
		{PRP_ExternalDbDriverInfo_maxColumnNameLength, new DataProperty(&ExternalDbDriverInfoMeta::maxColumnNameLength)},
		{PRP_ExternalDbDriverInfo_maxColumnsInOrderBy, new DataProperty(&ExternalDbDriverInfoMeta::maxColumnsInOrderBy)},
		{PRP_ExternalDbDriverInfo_maxColumnsInSelect, new DataProperty(&ExternalDbDriverInfoMeta::maxColumnsInSelect)},
		{PRP_ExternalDbDriverInfo_maxIdentifierLength, new DataProperty(&ExternalDbDriverInfoMeta::maxIdentifierLength)},
		{PRP_ExternalDbDriverInfo_maxSchemaNameLength, new DataProperty(&ExternalDbDriverInfoMeta::maxSchemaNameLength)},
		{PRP_ExternalDbDriverInfo_maxStatementLength, new DataProperty(&ExternalDbDriverInfoMeta::maxStatementLength)},
		{PRP_ExternalDbDriverInfo_maxTableNameLength, new DataProperty(&ExternalDbDriverInfoMeta::maxTableNameLength)},
		{PRP_ExternalDbDriverInfo_maxTablesInSelect, new DataProperty(&ExternalDbDriverInfoMeta::maxTablesInSelect)},
		{PRP_ExternalDbDriverInfo_orderByColumnsInSelect, new DataProperty(&ExternalDbDriverInfoMeta::orderByColumnsInSelect)},
		{PRP_ExternalDbDriverInfo_quotedIdentifierCase, new DataProperty(&ExternalDbDriverInfoMeta::quotedIdentifierCase)},
		{PRP_ExternalDbDriverInfo_schemaUsage, new DataProperty(&ExternalDbDriverInfoMeta::schemaUsage)}
		});

	static DataMapper<ExternalDbProfileMeta> externalDbProfileMapper(DSKEXTERNALDBPROFILE, &RootSchema::externalDbProfile, {
		{PRP_ExternalDbProfile_clsQueryClass, nullptr},
		{PRP_ExternalDbProfile_collQueryClass, nullptr},
		{PRP_ExternalDbProfile_database, nullptr},
		{PRP_ExternalDbProfile_defineAttrClass, nullptr},
		{PRP_ExternalDbProfile_defineRefFromClass, nullptr},
		{PRP_ExternalDbProfile_defineRefToClass, nullptr},
		{PRP_ExternalDbProfile_includeKeys, new DataProperty(&ExternalDbProfileMeta::includeKeys)},
		{PRP_ExternalDbProfile_offline, nullptr},
		{PRP_ExternalDbProfile_prefixAttr, new DataProperty(&ExternalDbProfileMeta::prefixAttr)},
		{PRP_ExternalDbProfile_prefixClass, new DataProperty(&ExternalDbProfileMeta::prefixClass)},
		{PRP_ExternalDbProfile_prefixKeys, new DataProperty(&ExternalDbProfileMeta::prefixKeys)},
		{PRP_ExternalDbProfile_prefixRef, new DataProperty(&ExternalDbProfileMeta::prefixRef)},
		{PRP_ExternalDbProfile_refQueryClass, nullptr},
		{PRP_ExternalDbProfile_replaceUScore, new DataProperty(&ExternalDbProfileMeta::replaceUScore)},
		{PRP_ExternalDbProfile_savePswd, new DataProperty(&ExternalDbProfileMeta::savePswd)},
		{PRP_ExternalDbProfile_sheet, nullptr},
		{PRP_ExternalDbProfile_suffixArray, new DataProperty(&ExternalDbProfileMeta::suffixArray)},
		{PRP_ExternalDbProfile_suffixDict, new DataProperty(&ExternalDbProfileMeta::suffixDict)},
		{PRP_ExternalDbProfile_suffixSet, new DataProperty(&ExternalDbProfileMeta::suffixSet)},
		{PRP_ExternalDbProfile_toMixedCase, new DataProperty(&ExternalDbProfileMeta::toMixedCase)},
		{PRP_ExternalDbProfile_toSingular, new DataProperty(&ExternalDbProfileMeta::toSingular)},
		{PRP_ExternalDbProfile_useUnderscore, new DataProperty(&ExternalDbProfileMeta::useUnderscore)}
		});

	static EntityRegistration<ExternalDatabase> externalDatabase(DSKEXTERNALDATABASE);

	string ExternalDatabase::getName() const
	{
		return getProperty<string>(PRP_ExternalDatabase_name);
	}

	bool ExternalDatabase::lookup(const Entity* ancestor, const QualifiedName& path)
	{
		return Entity::lookup<Schema>(ancestor, path, PRP_Schema_externalDatabases);
	}

	DskObjectId ExternalDatabase::GetParentId() const
	{
		return getProperty<DskObjectId>(PRP_ExternalDatabase__schema);
	}

	class ExternalDbDriverInfo : public Object
	{
	public:
		using Object::Object;

	protected:
		Data::Object* resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const final
		{
			// Resolve existing
			if (auto db = dynamic_cast<Data::ExternalDatabase*>(parent))
				if (db->driverInfo)
					return db->driverInfo;

			// Default to creating new instance
			return Object::resolve(assembly, parent, shallow);
		}
	};
	static ObjectRegistration<ExternalDbDriverInfo> externalDbDriverInfo(DSKEXTERNALDBDRIVERINFO);

	class ExternalDbProfile : public Object
	{
	public:
		using Object::Object;

	protected:
		Data::Object* resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const final
		{
			// Resolve existing
			if (auto db = dynamic_cast<Data::ExternalDatabase*>(parent))
				if (db->profile)
					return db->profile;

			// Default to creating new instance
			return Object::resolve(assembly, parent, shallow);
		}
	};
	static ObjectRegistration<ExternalDbProfile> externalDbProfile(DSKEXTERNALDBPROFILE);
}