#include "Type.h"
#include "DataMapper.h"
#include "EntityRegistration.h"

using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	class JadeDynamicPropertyCluster : public Entity
	{
	public:
		using Entity::Entity;

		void dependents(std::set<DskObjectId>& dependents) const override
		{
			Entity::dependents(dependents);

			// TODO: Dynamic properties in cluster?
		}

		std::string getName() const override
		{
			return getProperty<std::string>(PRP_JadeDynamicPropertyCluster_name);
		}

		bool isShallow() const final
		{
			return true;
		}

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			return Entity::lookup<Class>(ancestor, path, PRP_Class_dynamicPropertyClusters);
		}

		DskObjectId GetParentId() const override
		{
			return getProperty<DskObjectId>(PRP_JadeDynamicPropertyCluster_schemaType);
		}
	};
	static EntityRegistration<JadeDynamicPropertyCluster> dynamicPropertyCluster(DSKJADEDYNAMICPROPERTYCLUSTER);
}