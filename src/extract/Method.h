#pragma once
#include "Routine.h"
#include "Type.h"

namespace JadeGit::Extract
{
	class Method : public Routine
	{
	public:
		using Routine::Routine;

		void dependents(std::set<DskObjectId>& dependents) const override;
		void GetNamesakes(std::set<DskObjectId>& namesakes) const override;

	protected:
		std::string GetBasicTypeName() const final;

		bool lookup(const Entity* ancestor, const QualifiedName& path) override;
	};

	class JadeInterfaceMethod : public Method
	{
	public:
		using Method::Method;

		void dependents(std::set<DskObjectId>& dependents) const override;

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override;
	};

	class JadeMethod : public Method
	{
	public:
		using Method::Method;

		void dependents(std::set<DskObjectId>& dependents) const override;
	};
}