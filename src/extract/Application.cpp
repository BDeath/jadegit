#include "Schema.h"
#include "DataMapper.h"
#include "EntityRegistration.h"
#include <jadegit/data/JadeWebServiceManager.h>
#include <jadegit/data/RootSchema/ApplicationMeta.h>
#include <jadegit/data/RootSchema/JadeWebServiceManagerMeta.h>

using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static DataMapper<ApplicationMeta> applicationMapper(DSKAPPLICATION, &RootSchema::application, {
		{PRP_Application___overridePatchNumber, nullptr},
		{PRP_Application__aboutFormName, new DataProperty(&ApplicationMeta::aboutFormName)},
		{PRP_Application__consoleMemRefs, nullptr},
		{PRP_Application__defaultLocaleId, new DataProperty(&ApplicationMeta::defaultLocaleId)},
		{PRP_Application__internetPipe, nullptr},
		{PRP_Application__internetPipeName, new DataProperty(&ApplicationMeta::internetPipeName)},
		{PRP_Application__numberOfPipes, new DataProperty(&ApplicationMeta::numberOfPipes)},
		{PRP_Application__propertyBag, nullptr},
		{PRP_Application__restServicesOptions, nullptr},
		{PRP_Application__startupFormName, new DataProperty(&ApplicationMeta::startupFormName)},
		{PRP_Application__useXMLConfigFile, nullptr},
		{PRP_Application__useXMLDocumentParser, nullptr},
		{PRP_Application__webAppDirectory, new DataProperty(&ApplicationMeta::webAppDirectory)},
		{PRP_Application__webBaseURL, new DataProperty(&ApplicationMeta::webBaseURL)},
		{PRP_Application__webDelayBetweenApps, nullptr},
		{PRP_Application__webDisplayMessages, new DataProperty(&ApplicationMeta::webDisplayMessages)},
		{PRP_Application__webDisplayPreference, new DataProperty(&ApplicationMeta::webDisplayPreference)},
		{PRP_Application__webEnabled, nullptr},
		{PRP_Application__webEventClasses, new DataProperty(&ApplicationMeta::webEventClasses)},
		{PRP_Application__webFinalizeMethod, nullptr},
		{PRP_Application__webInitializeMethod, nullptr},
		{PRP_Application__webIsSecure, nullptr},
		{PRP_Application__webHomePage, new DataProperty(&ApplicationMeta::webHomePage)},
		{PRP_Application__webLogFile, nullptr},
		{PRP_Application__webMachineName, new DataProperty(&ApplicationMeta::webMachineName)},
		{PRP_Application__webMaxHTMLSize, new DataProperty(&ApplicationMeta::webMaxHTMLSize)},
		{PRP_Application__webMessage, nullptr},
		{PRP_Application__webMethodExecutionPrefix, nullptr},
		{PRP_Application__webQueue, nullptr},
		{PRP_Application__webServiceClasses, new DataProperty(&ApplicationMeta::webServiceClasses)},
		{PRP_Application__webSession, nullptr},
		{PRP_Application__webSessionManager, nullptr},
		{PRP_Application__webSessionTimeout, new DataProperty(&ApplicationMeta::webSessionTimeout)},
		{PRP_Application__webShowModal, new DataProperty(&ApplicationMeta::webShowModal)},
		{PRP_Application__webStartupForm, nullptr},
		{PRP_Application__webStartupFormName, nullptr},
		{PRP_Application__webStatusLineDisplay, new DataProperty(&ApplicationMeta::webStatusLineDisplay)},
		{PRP_Application__webUseHTML32, new DataProperty(&ApplicationMeta::webUseHTML32)},
		{PRP_Application__webUseSessionHandling, nullptr},
		{PRP_Application__webVirtualDirectory, new DataProperty(&ApplicationMeta::webVirtualDirectory)},
		{PRP_Application__webWaitForReply, nullptr},
		{PRP_Application_aboutForm, nullptr},			// Don't extract (covered by aboutFormName)
		{PRP_Application_appVersion, new DataProperty(&ApplicationMeta::appVersion)},
		{PRP_Application_applicationType, new DataProperty(&ApplicationMeta::applicationType)},
		{PRP_Application_controlSpacing, new DataProperty(&ApplicationMeta::controlSpacing)},
		{PRP_Application_currentLocale, nullptr},		// Don't extract (runtime)
		{PRP_Application_currentLocaleInfo, nullptr},	// Don't extract (runtime)
		{PRP_Application_defaultApp, new DataProperty(&ApplicationMeta::defaultApp)},
		{PRP_Application_defaultMdi, new DataProperty(&ApplicationMeta::defaultMdi)},
		{PRP_Application_exportedPackages, nullptr},
		{PRP_Application_finalizeMethod, new DataProperty(&ApplicationMeta::finalizeMethod)},
		{PRP_Application_fontBold, new DataProperty(&ApplicationMeta::fontBold)},
		{PRP_Application_fontName, new DataProperty(&ApplicationMeta::fontName)},
		{PRP_Application_fontSize, new DataProperty(&ApplicationMeta::fontSize)},
		{PRP_Application_formMargin, new DataProperty(&ApplicationMeta::formMargin)},
		{PRP_Application_heightSingleLineControl, new DataProperty(&ApplicationMeta::heightSingleLineControl)},
		{PRP_Application_helpFile, new DataProperty(&ApplicationMeta::helpFile)},
		{PRP_Application_icon, new DataProperty(&ApplicationMeta::icon)},
		{PRP_Application_initializeMethod, new DataProperty(&ApplicationMeta::initializeMethod)},
		{PRP_Application_jadeWebServiceManager, new DataProperty<ApplicationMeta>(nullptr)},
		{PRP_Application_name, nullptr},				// Named on creation
		{PRP_Application_printer, nullptr},				// Don't extract (runtime)
		{PRP_Application_showBubbleHelp, nullptr},		// Don't extract (runtime)
		{PRP_Application_startupForm, nullptr},			// Don't extract (covered by startupFormName)
		{PRP_Application_threeDControls, new DataProperty(&ApplicationMeta::threeDControls)},
		{PRP_Application_useBorderStyleOnly, new DataProperty(&ApplicationMeta::useBorderStyleOnly)},
		{PRP_Application_uuid, nullptr},
		{PRP_Application_webMinimumResponseTime, new DataProperty(&ApplicationMeta::webMinimumResponseTime)},
		});

	static DataMapper<JadeWebServiceManagerMeta> jadeWebServiceManagerMapper(DSKJADEWEBSERVICEMANAGER, &RootSchema::jadeWebServiceManager, {
		{PRP_JadeWebServiceManager__allowedSchemes, new DataProperty(&JadeWebServiceManagerMeta::allowedSchemes)},
		{PRP_JadeWebServiceManager__useSOAP11, new DataProperty(&JadeWebServiceManagerMeta::useSOAP11)},
		{PRP_JadeWebServiceManager__useSOAP12, new DataProperty(&JadeWebServiceManagerMeta::useSOAP12)},
		{PRP_JadeWebServiceManager_name, nullptr},		// Don't extract (not used)
		{PRP_JadeWebServiceManager_handleCircularReferences, new DataProperty(&JadeWebServiceManagerMeta::useEncodedFormat)},
		{PRP_JadeWebServiceManager_provider, new DataProperty(&JadeWebServiceManagerMeta::provider)},
		{PRP_JadeWebServiceManager_secureService, new DataProperty(&JadeWebServiceManagerMeta::secureService)},
		{PRP_JadeWebServiceManager_sessionHandling, new DataProperty(&JadeWebServiceManagerMeta::sessionHandling)},
		{PRP_JadeWebServiceManager_supportLibrary, new DataProperty(&JadeWebServiceManagerMeta::supportLibrary)},
		{PRP_JadeWebServiceManager_targetNamespace, new DataProperty(&JadeWebServiceManagerMeta::targetNamespace)},
		{PRP_JadeWebServiceManager_useHttpGet, new DataProperty(&JadeWebServiceManagerMeta::useHttpGet)},
		{PRP_JadeWebServiceManager_useHttpPost, new DataProperty(&JadeWebServiceManagerMeta::useHttpPost)},
		{PRP_JadeWebServiceManager_useRPC, new DataProperty(&JadeWebServiceManagerMeta::useRPC)},
		{PRP_JadeWebServiceManager_versionControl, new DataProperty(&JadeWebServiceManagerMeta::versionControl)}
		});

	class Application : public Entity
	{
	public:
		using Entity::Entity;

		void dependents(std::set<DskObjectId>& dependents) const override
		{
			Entity::dependents(dependents);

			// Direct dependencies
			getProperty(PRP_Application_exportedPackages, dependents);
		}

		std::string getName() const override
		{
			return getProperty<std::string>(PRP_Application_name);
		}

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			return Entity::lookup<Schema>(ancestor, path, PRP_Schema__applications);
		}

		DskObjectId GetParentId() const override
		{
			return getProperty<DskObjectId>(PRP_Application_schema);
		}
	};
	static EntityRegistration<Application> application(DSKAPPLICATION);

	class JadeWebServiceManager : public Object
	{
	public:
		using Object::Object;

	protected:
		Data::Object* resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const override
		{
			// Resolve existing web service manager
			if (auto application = dynamic_cast<Data::Application*>(parent))
				if (application->webServiceManager)
					return application->webServiceManager;

			// Default to creating new instance
			return Object::resolve(assembly, parent, shallow);
		}
	};
	static ObjectRegistration<JadeWebServiceManager> jadeWebServiceManager(DSKJADEWEBSERVICEMANAGER);
}