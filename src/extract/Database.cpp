#include "Database.h"
#include "Schema.h"
#include "DataMapper.h"
#include "EntityRegistration.h"
#include <jadegit/data/RootSchema/DatabaseMeta.h>
#include <jadegit/data/RootSchema/DbClassMapMeta.h>
#include <jadegit/data/RootSchema/DbFileMeta.h>

using namespace Jade;
using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static EntityRegistration<Database> database(DSKDATABASE);
	static ObjectRegistration<DbClassMap> dbClassMap(DSKDBCLASSMAP);
	static EntityRegistration<DbFile> dbFile(DSKDBFILE);

	static DataMapper<DatabaseMeta> databaseMapper(DSKDATABASE, &RootSchema::database, {
		{PRP_Database_classMaps, nullptr},	// Extracted via Class
		{PRP_Database_defaultFile, new DataProperty(&DatabaseMeta::defaultFile)}
		});

	static DataMapper<DbClassMapMeta> dbClassMapMapper(DSKDBCLASSMAP, &RootSchema::dbClassMap, {
		{PRP_DbClassMap__systemBasic, nullptr},
		{PRP_DbClassMap_database, new DataProperty(&DbClassMapMeta::database)},
		{PRP_DbClassMap_diskFile, new DataProperty(&DbClassMapMeta::diskFile)},
		{PRP_DbClassMap_mode, new DataProperty(&DbClassMapMeta::mode)},
		{PRP_DbClassMap_remapMode, nullptr}
		});

	static DataMapper<DbFileMeta> dbFileMapper(DSKDBFILE, &RootSchema::dbFile, {
		{PRP_DbFile_classMapRefs, nullptr},	// Extracted via Class
		{PRP_DbFile_kind, nullptr}
		});

	bool Database::lookup(const Entity* ancestor, const QualifiedName& path)
	{
		return Entity::lookup<Schema>(ancestor, path, PRP_Schema_databases);
	}

	void DbFile::dependents(std::set<DskObjectId>& dependents) const
	{
		SchemaEntity::dependents(dependents);

		// Handle indirect class dependendencies
		std::set<DskObjectId> classMaps;
		getProperty(PRP_DbFile_classMapRefs, classMaps);
		for (auto& oid : classMaps)
		{
			DskObject classMap(oid);
			DskObjectId diskClass = NullDskObjectId;
			jade_throw(classMap.getProperty(PRP_DbClassMap_diskClass, &diskClass));
			if (!diskClass.isNull())
				dependents.insert(diskClass);
		}
	}

	bool DbFile::isShallow() const
	{
		auto name = getName();
		if (name[0] != '_')
			return false;

		// treat duplicate system files as shallow (can't create in new environments, but may exist in old ones)
		Object db(&RootDatabaseOid);
		auto dbFiles = db.getProperty<DskMemberKeyDictionary>(PRP_Database_dbFiles);
		DskObject dbFile;
		jade_throw(dbFiles.getAtKey(widen(name).c_str(), dbFile));
		return !dbFile.isNull();
	}

	bool DbFile::lookup(const Entity* ancestor, const QualifiedName& path)
	{
		return Entity::lookup<Database>(ancestor, path, PRP_Database_dbFiles);
	}
}