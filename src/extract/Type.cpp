#include "Type.h"
#include "Schema.h"
#include "DataMapper.h"
#include "DataTranslator.h"
#include "EntityRegistration.h"
#include <jade/Iterator.h>
#include <jadegit/data/RootSchema/ClassMeta.h>
#include <jadegit/data/RootSchema/CollClassMeta.h>
#include <jadegit/data/RootSchema/ExternalClassMeta.h>
#include <jadegit/data/RootSchema/ExternalCollClassMeta.h>
#include <jadegit/data/RootSchema/GUIClassMeta.h>
#include <jadegit/data/RootSchema/HTMLClassMeta.h>
#include <jadegit/data/RootSchema/JadeInterfaceMeta.h>
#include <jadegit/data/RootSchema/JadeWebServiceConsumerClassMeta.h>
#include <jadegit/data/RootSchema/JadeWebServiceProviderClassMeta.h>
#include <jadegit/data/RootSchema/JadeWebServiceSoapHeaderClassMeta.h>
#include <Log.h>

using namespace Jade;
using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static EntityRegistration<Class> klass(DSKCLASS);
	static EntityRegistration<Class> collClass(DSKCOLLCLASS);
	static EntityRegistration<GUIClass> guiClass(DSKGUICLASS);
	static EntityRegistration<JadeInterface> interface(DSKJADEINTERFACE);
	static EntityRegistration<Type> primType(DSKPRIMTYPE);
	static EntityRegistration<Type> pseudoType(DSKPSEUDOTYPE);
	static EntityRegistration<Type> type(DSKTYPE);

	bool hasPredefinedLength(const DskType& type)
	{
		PFormat number = PFormat::DSKUNDEF;
		jade_throw(type.getNumber(&number));
		if (number == PFormat::DSKANY)
			return true;

		DskParam param;
		paramSetInteger(param);
		jade_throw(type.sendMsg(TEXT("maxLength"), nullptr, &param));

		int result;
		paramGetInteger(param, result);

		// Type has a predefined size if maxLength is set
		return result != 0;
	}

	static DataMapper<TypeMeta> typeMapper(DSKTYPE, &RootSchema::type, {
		{PRP_Type__sysConstantRefs, nullptr},
		{PRP_Type__sysPropertyRefs, nullptr},
		{PRP_Type__sysScriptRefs, nullptr},
		{PRP_Type_accessTypes, nullptr},
		{PRP_Type_constantRefs, nullptr},
		{PRP_Type_final, new DataProperty(&TypeMeta::final)},
		{PRP_Type_methods, new DataProperty<TypeMeta>(nullptr)},
		{PRP_Type_persistentAllowed, new DataProperty(&TypeMeta::persistentAllowed)},
		{PRP_Type_propertyRefs, nullptr},
		{PRP_Type_sharedTransientAllowed, new DataProperty(&TypeMeta::sharedTransientAllowed)},
		{PRP_Type_transientAllowed, new DataProperty(&TypeMeta::transientAllowed)},
		{PRP_Type_scriptRefs, nullptr},
		{PRP_Type_subclassPersistentAllowed, new DataProperty(&TypeMeta::subclassPersistentAllowed)},
		{PRP_Type_subclassSharedTransientAllowed, new DataProperty(&TypeMeta::subclassSharedTransientAllowed)},
		{PRP_Type_subclassTransientAllowed, new DataProperty(&TypeMeta::subclassTransientAllowed)},
		{PRP_Type_subschemaFinal, new DataProperty(&TypeMeta::subschemaFinal)}
		});

	static DataMapper<ClassMeta> classMapper(DSKCLASS, &RootSchema::class_, {
		{PRP_Class__sysExposedClassRefs, nullptr},
		{PRP_Class__xmlInnerClass, new DataProperty(&ClassMeta::xmlInnerClass)},
		{PRP_Class_activeXClass, nullptr},
		{PRP_Class_classMapRefs, new DataProperty<ClassMeta>(nullptr)},
		{PRP_Class_dynamicPropertyClusters, nullptr},
		{PRP_Class_exportedClassRefs, nullptr},
		{PRP_Class_exposedClassRefs, nullptr},
		{PRP_Class_highestOrdinal, nullptr},
		{PRP_Class_highestSubId, nullptr},
		{PRP_Class_instances, nullptr},
		{PRP_Class_instanceVolatility, new DataProperty(&ClassMeta::instanceVolatility)},
		{PRP_Class_propertyOrder, nullptr},
		{PRP_Class_replicationAllowed, nullptr},
		{PRP_Class_replicationControl, nullptr},
		{PRP_Class_replicationDefault, nullptr},
		{PRP_Class_rpsClassMaps, nullptr},
		{PRP_Class_subclasses, nullptr},
		{PRP_Class_superclass, new DataProperty(&ClassMeta::superclass)},
		{PRP_Class_transient, new DataProperty(&ClassMeta::transient)},
		{PRP_Class_usedInWebService, new DataProperty(&ClassMeta::webService)},
		{PRP_Class_versionNumber, nullptr}
		});

	bool hasVariableMemberLength(const DskObject& object)
	{
		DskType memberType;
		jade_throw(DskCollClass(&object.oid).getMemberType(memberType));
		return !hasPredefinedLength(memberType);
	}

	static DataMapper<CollClassMeta> collClassMapper(DSKCOLLCLASS, &RootSchema::collClass, {
		{PRP_CollClass__memberTypeWSDLName, new DataProperty(&CollClassMeta::memberTypeWSDLName)},
		{PRP_CollClass_actualBlockEntries, nullptr},
		{PRP_CollClass_maxBlockSize, nullptr},
		{PRP_CollClass_maxLogicalBlockSize, nullptr},
		{PRP_CollClass_memberTypeSFactor, new DataProperty(&CollClassMeta::memberTypeScaleFactor)},
		// Don't extract member size/length when it's predefined & can't be changed
		{PRP_CollClass_memberTypeSize, new DataProperty(&CollClassMeta::memberTypeSize, &hasVariableMemberLength)}
		});

	static DataMapper<ExternalClassMeta> externalClassMapper(DSKEXTERNALCLASS, &RootSchema::externalClass, {
		{PRP_ExternalClass_columnAttributeInfoList, new DataProperty(&ExternalClassMeta::columnAttributeInfoList)},
		{PRP_ExternalClass_extensionString, new DataProperty(&ExternalClassMeta::extensionString)},
		{PRP_ExternalClass_extensionStringMap, new DataProperty(&ExternalClassMeta::extensionStringMap)},
		{PRP_ExternalClass_externalDatabase, new DataProperty(&ExternalClassMeta::externalDatabase)},
		{PRP_ExternalClass_externalSchemaMap, nullptr},
		{PRP_ExternalClass_fromList, new DataProperty(&ExternalClassMeta::fromList)},
		{PRP_ExternalClass_orderByList, new DataProperty(&ExternalClassMeta::orderByList)},
		{PRP_ExternalClass_rowidPredicate, new DataProperty(&ExternalClassMeta::rowidPredicate)},
		{PRP_ExternalClass_rowidPredicateInfo, new DataProperty(&ExternalClassMeta::rowidPredicateInfo)},
		{PRP_ExternalClass_selectList, new DataProperty(&ExternalClassMeta::selectList)},
		{PRP_ExternalClass_updatable, new DataProperty(&ExternalClassMeta::updatable)},
		{PRP_ExternalClass_wherePredicate, new DataProperty(&ExternalClassMeta::wherePredicate)}
		});

	static DataMapper<ExternalCollClassMeta> externalCollClassMapper(DSKEXTERNALCOLLCLASS, &RootSchema::externalCollClass, {
		{PRP_ExternalCollClass_extensionString, new DataProperty(&ExternalCollClassMeta::extensionString)},
		{PRP_ExternalCollClass_extensionStringMap, new DataProperty(&ExternalCollClassMeta::extensionStringMap)},
		{PRP_ExternalCollClass_externalDatabase, new DataProperty(&ExternalCollClassMeta::externalDatabase)},
		{PRP_ExternalCollClass_externalSchemaMap, nullptr},
		{PRP_ExternalCollClass_firstPredicate, new DataProperty(&ExternalCollClassMeta::firstPredicate)},
		{PRP_ExternalCollClass_firstPredicateInfo, new DataProperty(&ExternalCollClassMeta::firstPredicateInfo)},
		{PRP_ExternalCollClass_groupByList, new DataProperty(&ExternalCollClassMeta::groupByList)},
		{PRP_ExternalCollClass_havingPredicate, new DataProperty(&ExternalCollClassMeta::havingPredicate)},
		{PRP_ExternalCollClass_havingPredicateInfo, new DataProperty(&ExternalCollClassMeta::havingPredicateInfo)},
		{PRP_ExternalCollClass_includesPredicate, new DataProperty(&ExternalCollClassMeta::includesPredicate)},
		{PRP_ExternalCollClass_includesPredicateInfo, new DataProperty(&ExternalCollClassMeta::includesPredicateInfo)},
		{PRP_ExternalCollClass_keyEqPredicate, new DataProperty(&ExternalCollClassMeta::keyEqPredicate)},
		{PRP_ExternalCollClass_keyEqPredicateInfo, new DataProperty(&ExternalCollClassMeta::keyEqPredicateInfo)},
		{PRP_ExternalCollClass_keyGeqPredicate, new DataProperty(&ExternalCollClassMeta::keyGeqPredicate)},
		{PRP_ExternalCollClass_keyGeqPredicateInfo, new DataProperty(&ExternalCollClassMeta::keyGeqPredicateInfo)},
		{PRP_ExternalCollClass_keyGtrPredicate, new DataProperty(&ExternalCollClassMeta::keyGtrPredicate)},
		{PRP_ExternalCollClass_keyGtrPredicateInfo, new DataProperty(&ExternalCollClassMeta::keyGtrPredicateInfo)},
		{PRP_ExternalCollClass_keyLeqPredicate, new DataProperty(&ExternalCollClassMeta::keyLeqPredicate)},
		{PRP_ExternalCollClass_keyLeqPredicateInfo, new DataProperty(&ExternalCollClassMeta::keyLeqPredicateInfo)},
		{PRP_ExternalCollClass_keyLssPredicate, new DataProperty(&ExternalCollClassMeta::keyLssPredicate)},
		{PRP_ExternalCollClass_keyLssPredicateInfo, new DataProperty(&ExternalCollClassMeta::keyLssPredicateInfo)},
		{PRP_ExternalCollClass_lastPredicate, new DataProperty(&ExternalCollClassMeta::lastPredicate)},
		{PRP_ExternalCollClass_lastPredicateInfo, new DataProperty(&ExternalCollClassMeta::lastPredicateInfo)},
		{PRP_ExternalCollClass_orderByList, new DataProperty(&ExternalCollClassMeta::orderByList)}
		});

	static DataMapper<GUIClassMeta> guiClassMapper(DSKGUICLASS, &RootSchema::guiClass, {
		{PRP_GUIClass_controlType, new DataProperty<GUIClassMeta>(nullptr)}
		});

	static DataMapper<HTMLClassMeta> htmlClassMapper(DSKHTMLCLASS, &RootSchema::htmlClass, {
		{PRP_HTMLClass__jadeHTMLDocument, nullptr}
		});

	static DataMapper<JadeWebServicesClassMeta> jadeWebServicesClassMapper(DSKJADEWEBSERVICESCLASS, &RootSchema::jadeWebServicesClass, {
		{PRP_JadeWebServicesClass_additionalInfo, new DataProperty(&JadeWebServicesClassMeta::additionalInfo)},
		{PRP_JadeWebServicesClass_wsdl, new DataProperty(&JadeWebServicesClassMeta::wsdl)}
		});

	static DataMapper<JadeWebServiceConsumerClassMeta> jadeWebServiceConsumerClassMapper(DSKJADEWEBSERVICECONSUMERCLASS, &RootSchema::jadeWebServiceConsumerClass, {
		{PRP_JadeWebServiceConsumerClass__useSOAP12, new DataProperty(&JadeWebServiceConsumerClassMeta::useSOAP12)},
		{PRP_JadeWebServiceConsumerClass_endPointURL, new DataProperty(&JadeWebServiceConsumerClassMeta::endPointURL)},
		{PRP_JadeWebServiceConsumerClass_useAsyncCalls, new DataProperty(&JadeWebServiceConsumerClassMeta::useAsyncCalls)},
		{PRP_JadeWebServiceConsumerClass_useNewPrimTypes, new DataProperty(&JadeWebServiceConsumerClassMeta::useNewPrimTypes)},
		});

	static DataMapper<JadeWebServiceProviderClassMeta> jadeWebServiceProviderClassMapper(DSKJADEWEBSERVICEPROVIDERCLASS, &RootSchema::jadeWebServiceProviderClass, {
		{PRP_JadeWebServiceProviderClass_secureService, new DataProperty(&JadeWebServiceProviderClassMeta::secureService)}
		});

	// Custom translator for JadeWebServiceSoapHeaderClass::soapHeaderDirection which handles fixing legacy systems where direction is undefined, which should be treated as input/output
	class JadeWebServiceSoapHeaderDirectionTranslator : public DataTranslator, public Singleton<JadeWebServiceSoapHeaderDirectionTranslator>
	{
	public:
		JadeWebServiceSoapHeaderDirectionTranslator() : DataTranslator(true) {}

		void Copy(Assembly& assembly, const Object& source, const DskParam& value, Data::Object* target, const Data::Property* property, const std::string& name, const std::string& trail, bool deep) const final
		{
			assert(property);
			assert(name == "soapHeaderDirection");
			assert(value.header.format == PFormat::DSKCHARACTER);

			LOG_TRACE("Extracting attribute: soapHeaderDirection");

			char direction = static_cast<char>(value.body.character);

			// Set undefined to input/output
			if (!direction)
				direction = 'B'; // For 'B'oth

			target->SetValue(property, direction);
		}
	};

	static DataMapper<JadeWebServiceSoapHeaderClassMeta> jadeWebServiceSoapHeaderClassMapper(DSKJADEWEBSERVICESOAPHEADERCLASS, &RootSchema::jadeWebServiceSoapHeaderClass, {
		{PRP_JadeWebServiceSoapHeaderClass_actor, new DataProperty(&JadeWebServiceSoapHeaderClassMeta::actor)},
		{PRP_JadeWebServiceSoapHeaderClass_mustUnderstand, new DataProperty(&JadeWebServiceSoapHeaderClassMeta::mustUnderstand)},
		{PRP_JadeWebServiceSoapHeaderClass_soapHeaderDirection, new DataProperty(&JadeWebServiceSoapHeaderClassMeta::direction, nullptr, JadeWebServiceSoapHeaderDirectionTranslator::Instance())}
		});

	static DataMapper<JadeInterfaceMeta> jadeInterfaceMapper(DSKJADEINTERFACE, &RootSchema::jadeInterface, {
		{PRP_JadeInterface_exportedInterfaceRefs, nullptr},
		{PRP_JadeInterface_implementorClasses, nullptr},
		{PRP_JadeInterface_properties, nullptr},
		{PRP_JadeInterface_subinterfaces, nullptr},
		{PRP_JadeInterface_superinterfaces, new DataProperty(&JadeInterfaceMeta::superinterfaces)}
		});

	bool Type::isOriginal() const
	{
		// Handle imported types
		ClassNumber cls = actualClass(oid.classNo);
		if (cls == DSKJADEIMPORTEDCLASS || cls == DSKJADEIMPORTEDINTERFACE)
			return false;

		// Handle subschema copies
		return getProperty<DskObjectId>(PRP_Type_superschemaType).isNull();
	}

	bool Type::lookup(const Entity* ancestor, const QualifiedName& path)
	{
		return Entity::lookup<Schema>(ancestor, path, PRP_Schema_classes, PRP_Schema_interfaces, PRP_Schema_primitives, PRP_Schema_pseudoTypes);
	}

	void Type::children(std::set<DskObjectId>& children) const
	{
		SchemaEntity::children(children);

		getProperty(PRP_Type_consts, children);
		getProperty(PRP_Type_methods, children);
	}

	void Type::dependents(std::set<DskObjectId>& dependents) const
	{
		SchemaEntity::dependents(dependents);

		// Direct dependencies
		getProperty(PRP_Type_collClassRefs, dependents);
		getProperty(PRP_Type_propertyRefs, dependents);

		// Handle indirect dependencies via external key
		std::set<DskObjectId> keys;
		getProperty(PRP_Type_keyRefs, keys);
		for (auto& key : keys)
		{
			DskObject externalKey(key);
			DskObjectId collClass = NullDskObjectId;
			jade_throw(externalKey.getProperty(PRP_Key_collClass, &collClass));
			if (!collClass.isNull())
				dependents.insert(collClass);
		}
	}

	void Type::GetNamesakes(std::set<DskObjectId>& namesakes) const
	{
		SchemaEntity::GetNamesakes(namesakes);

		// Get subschema copies recursively
		OrderedJomColl subschemaTypes;
		jade_throw(valuesFromProperty(PRP_Type_subschemaTypes, subschemaTypes));
		JomObjp subschemaType = nullptr;
		subschemaTypes.start();
		while (subschemaTypes.next(subschemaType))
			Type(((DskType*)subschemaType)->oid).GetNamesakes(namesakes);
	}

	Data::Type* Type::resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const
	{
		// Resolve type (creating if required)
		Data::Entity* entity = SchemaEntity::resolve(assembly, parent, shallow);
		assert(dynamic_cast<Data::Type*>(entity));

		auto type = static_cast<Data::Type*>(entity);

		// Setup superschema type if required
		if (!type->superschemaType)
		{
			DskObjectId superschemaType;
			jade_throw(getProperty(PRP_Type_superschemaType, &superschemaType));
			if (!superschemaType.isNull())
			{
				auto entity = EntityFactory::Get().Create(superschemaType);
				type->superschemaType = static_cast<Data::Type&>(entity->resolve(assembly));
				type->inferred();
			}
		}

		return type;
	}

	bool Class::lookup(const Entity* ancestor, const QualifiedName& path)
	{
		return Entity::lookup<Schema>(ancestor, path, PRP_Schema_classes);
	}

	void Class::children(std::set<DskObjectId>& children) const
	{
		Type::children(children);

		getProperty(PRP_Class_properties, children);
	}

	void Class::dependents(std::set<DskObjectId>& dependents) const
	{
		Type::dependents(dependents);

		// Direct dependencies
		getProperty(PRP_Class_exportedClassRefs, dependents);
		getProperty(PRP_Class_exposedClassRefs, dependents);
		getProperty(PRP_Class_subclasses, dependents);
	}

	Data::Class* Class::resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const
	{
		// Resolve class (creating if required)
		Data::Entity* entity = SchemaEntity::resolve(assembly, parent, shallow);
		assert(dynamic_cast<Data::Class*>(entity));

		auto type = static_cast<Data::Class*>(entity);

		// Setup superclass if required
		if (!type->superschemaType && !type->superclass)
		{
			DskObjectId superclass;
			jade_throw(getProperty(PRP_Class_superclass, &superclass));
			if (!superclass.isNull())
			{
				auto entity = EntityFactory::Get().Create(superclass);
				type->superclass = static_cast<Data::Class&>(entity->resolve(assembly));
			}
		}

		return type;
	}

	void GUIClass::getAssociates(std::set<DskObjectId>& associates) const
	{
		// Check if form class
		bool form = false;
		jade_throw(DskClass(&oid).inheritsFrom(DSKFORM, &form));
		if (form)
		{
			// Get name
			auto name = getName();

			// Get schema
			DskSchema schema;
			jade_throw(getProperty(PRP_Type_schema, schema));

			// Iterate locales
			DskObject locale;
			Jade::Iterator<DskObject> locales(schema, PRP_Schema_locales);
			while (locales.next(locale))
			{
				// Get forms collection
				DskMemberKeyDictionary forms;
				jade_throw(locale.getProperty(PRP_Locale_forms, forms));

				// Lookup form
				DskObject form;
				jade_throw(forms.getAtKey(widen(name).c_str(), form));
				if (!form.isNull())
					associates.insert(form.oid);
			}
		}
	}

	bool JadeInterface::lookup(const Entity* ancestor, const QualifiedName& path)
	{
		return Entity::lookup<Schema>(ancestor, path, PRP_Schema_interfaces);
	}

	void JadeInterface::dependents(std::set<DskObjectId>& dependents) const
	{
		Type::dependents(dependents);

		// Direct dependencies
		getProperty(PRP_JadeInterface_exportedInterfaceRefs, dependents);
		getProperty(PRP_JadeInterface_implementorClasses, dependents);
		getProperty(PRP_JadeInterface_subinterfaces, dependents);
	}

	bool JadeInterface::isShallow() const
	{
		// Subschema copies of an interface are proxies to the original
		// They cannot be added to, nor extracted/loaded in their own right
		DskObjectId superschemaType = NullDskObjectId;
		getProperty(PRP_Type_superschemaType, &superschemaType, __LINE__);
		return !superschemaType.isNull();
	}
}