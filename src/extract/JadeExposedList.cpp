#include "Schema.h"
#include "DataMapper.h"
#include "EntityRegistration.h"
#include <jadegit/data/RootSchema/JadeExposedListMeta.h>
#include <jadegit/data/RootSchema/JadeExposedClassMeta.h>
#include <jadegit/data/RootSchema/JadeExposedFeatureMeta.h>

using namespace Jade;
using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static DataMapper<JadeExposedListMeta> jadeExposedListMapper(DSKJADEEXPOSEDLIST, &RootSchema::jadeExposedList, {
		{PRP_JadeExposedList_exposedClasses, new DataProperty<JadeExposedListMeta>(nullptr)},
		{PRP_JadeExposedList_exposedListVersion, new DataProperty(&JadeExposedListMeta::version)},
		{PRP_JadeExposedList_name, nullptr},	// Named on creation
		{PRP_JadeExposedList_priorExposedListVersion, new DataProperty(&JadeExposedListMeta::priorVersion)},
		{PRP_JadeExposedList_registryId, new DataProperty(&JadeExposedListMeta::registryId)},
		{PRP_JadeExposedList_ws_handleCircularReferences, new DataProperty(&JadeExposedListMeta::useEncodedFormat)},
		{PRP_JadeExposedList_ws_nonWrappedDocLit, new DataProperty(&JadeExposedListMeta::useBareFormat)},
		{PRP_JadeExposedList_ws_secureService, new DataProperty(&JadeExposedListMeta::secureService)},
		{PRP_JadeExposedList_ws_sessionHandling, new DataProperty(&JadeExposedListMeta::sessionHandling)},
		{PRP_JadeExposedList_ws_useHttpGet, new DataProperty(&JadeExposedListMeta::useHttpGet)},
		{PRP_JadeExposedList_ws_useHttpPost, new DataProperty(&JadeExposedListMeta::useHttpPost)},
		{PRP_JadeExposedList_ws_useRPC, new DataProperty(&JadeExposedListMeta::useRPC)},
		{PRP_JadeExposedList_ws_useSOAP11, new DataProperty(&JadeExposedListMeta::useSOAP11)},
		{PRP_JadeExposedList_ws_useSOAP12, new DataProperty(&JadeExposedListMeta::useSOAP12)},
		{PRP_JadeExposedList_ws_versionControl, new DataProperty(&JadeExposedListMeta::versionControl)}
		});

	static DataMapper<JadeExposedClassMeta> jadeExposedClassMapper(DSKJADEEXPOSEDCLASS, &RootSchema::jadeExposedClass, {
		{PRP_JadeExposedClass__memberTypeWSDLName, nullptr},
		{PRP_JadeExposedClass_classAutoAdded, new DataProperty(&JadeExposedClassMeta::autoAdded)},
		{PRP_JadeExposedClass_consts, new DataProperty(&JadeExposedClassMeta::consts)},
		{PRP_JadeExposedClass_defaultStyle, new DataProperty(&JadeExposedClassMeta::defaultStyle)},
		{PRP_JadeExposedClass_exposedFeatures, new DataProperty<JadeExposedClassMeta>(nullptr)},
		{PRP_JadeExposedClass_javaName, new DataProperty(&JadeExposedClassMeta::exposedName)},
		{PRP_JadeExposedClass_methods, new DataProperty(&JadeExposedClassMeta::methods)},
		{PRP_JadeExposedClass_name, new DataProperty(&JadeExposedClassMeta::name)},
		{PRP_JadeExposedClass_properties, new DataProperty(&JadeExposedClassMeta::properties)},
		{PRP_JadeExposedClass_relatedClass, new DataProperty(&JadeExposedClassMeta::relatedClass)}
		});

	static DataMapper<JadeExposedFeatureMeta> jadeExposedFeatureMapper(DSKJADEEXPOSEDFEATURE, &RootSchema::jadeExposedFeature, {
		{PRP_JadeExposedFeature_dbField, new DataProperty(&JadeExposedFeatureMeta::dbField)},
		{PRP_JadeExposedFeature_javaName, new DataProperty(&JadeExposedFeatureMeta::exposedName)},
		{PRP_JadeExposedFeature_javaType, new DataProperty(&JadeExposedFeatureMeta::exposedType)},
		{PRP_JadeExposedFeature_lazyRead, new DataProperty(&JadeExposedFeatureMeta::lazyRead)},
		{PRP_JadeExposedFeature_name, new DataProperty(&JadeExposedFeatureMeta::name)},
		{PRP_JadeExposedFeature_relatedFeature, new DataProperty(&JadeExposedFeatureMeta::relatedFeature)}
		});

	class JadeExposedList : public Entity
	{
	public:
		using Entity::Entity;

		// Always perform deep extract for exposures (to include classes which aren't updated individually)
		void extract(Assembly& assembly, bool deep) const override
		{
			Entity::extract(assembly, true);
		}

		void dependents(std::set<DskObjectId>& dependents) const override
		{
			Entity::dependents(dependents);

			// Handle indirect dependencies via web service managers
			std::set<DskObjectId> managers;
			getProperty(PRP_JadeExposedList__webServiceManagers, managers);
			for (auto& oid : managers)
			{
				DskObject manager(oid);
				DskObjectId application = NullDskObjectId;
				jade_throw(manager.getProperty(PRP_JadeWebServiceManager_application, &application));
				if (!application.isNull())
					dependents.insert(application);
			}
		}

		std::string getName() const override
		{
			return getProperty<std::string>(PRP_JadeExposedList_name);
		}

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			return Entity::lookup<Schema>(ancestor, path, PRP_Schema__exposedLists);
		}

		DskObjectId GetParentId() const override
		{
			return getProperty<DskObjectId>(PRP_JadeExposedList_schema);
		}
	};
	static EntityRegistration<JadeExposedList> jadeExposedList(DSKJADEEXPOSEDLIST);

	class JadeExposedClass : public Entity
	{
	public:
		using Entity::Entity;

		std::string getName() const override
		{
			return getProperty<std::string>(PRP_JadeExposedClass_name);
		}

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			return Entity::lookup<JadeExposedList>(ancestor, path, PRP_JadeExposedList_exposedClasses);
		}

		DskObjectId GetParentId() const override
		{
			return getProperty<DskObjectId>(PRP_JadeExposedClass_exposedList);
		}
	};
	static EntityRegistration<JadeExposedClass> jadeExposedClass(DSKJADEEXPOSEDCLASS);

	class JadeExposedFeature : public Entity
	{
	public:
		using Entity::Entity;

		std::string getName() const override
		{
			return getProperty<std::string>(PRP_JadeExposedFeature_name);
		}

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			return Entity::lookup<JadeExposedClass>(ancestor, path, PRP_JadeExposedClass_exposedFeatures);
		}

		DskObjectId GetParentId() const override
		{
			return getProperty<DskObjectId>(PRP_JadeExposedFeature_exposedClass);
		}
	};
	static EntityRegistration<JadeExposedFeature> jadeExposedFeature(DSKJADEEXPOSEDFEATURE);
}