#include "Type.h"
#include "DataMapper.h"
#include "DataTranslator.h"
#include "EntityRegistration.h"
#include <jade/DynaDictionary.h>
#include <jade/Iterator.h>
#include <jade/Transient.h>
#include <jadegit/data/RootSchema/ActiveXClassMeta.h>
#include <jadegit/data/RootSchema/ActiveXFeatureMeta.h>
#include <jadegit/data/RootSchema/ActiveXLibraryMeta.h>
#include <Exception.h>

using namespace std;
using namespace Jade;
using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	// Custom translator to sort classes by name
	class ChildActiveXClassTranslator : public ChildTranslator, public Singleton<ChildActiveXClassTranslator>
	{
	public:
		void Copy(Assembly& assembly, const Object& source, const DskParam& value, Data::Object* target, const Data::Property* property, const string& name, const string& trail, bool deep) const final
		{
			Transient<DynaDictionary> sorted;
			sorted.setMembership(DSKACTIVEXCLASS);
			sorted.addMemberKey(TEXT("baseClass.name"));
			sorted.endKeys();

			DskObjectId oid;
			jade_throw(paramGetOid(value, oid));

			DskCollection children(oid);
			children.sendMsg(TEXT("copy"), sorted);

			DskObject child;
			Jade::Iterator<DskObject> iter(sorted);
			int index = 0;
			while (iter.next(child))
			{
				if (!ChildTranslator::Copy(assembly, target, child.oid, name, ++index, trail, deep))
					break;
			}
		}
	};

	// Custom translator for implied ActiveXClass::activeXFeatures collection
	class ChildActiveXFeatureTranslator : public ChildTranslator, public Singleton<ChildActiveXFeatureTranslator>
	{
	public:
		void Copy(Assembly& assembly, const Object& source, const DskParam& value, Data::Object* target, const Data::Property* property, const string& name, const string& trail, bool deep) const final
		{
			Class baseClass = source.getProperty<Class>(PRP_ActiveXClass_baseClass);
			
			int index = 0;
			for (auto collection : { PRP_Type_consts, PRP_Class_properties, PRP_Type_methods })
			{
				Object feature;
				Jade::Iterator<Object> iter(baseClass, collection);

				while (iter.next(feature))
				{
					if (!ChildTranslator::Copy(assembly, target, feature.getProperty<DskObjectId>(PRP_Feature_activeXFeature), name, ++index, trail, deep))
						break;
				}
			}
		}
	};

	static DataMapper<ActiveXLibraryMeta> activeXLibraryMapper(DSKACTIVEXLIBRARY, &RootSchema::activeXLibrary, {
		{PRP_ActiveXLibrary_activeXClass, new DataProperty<ActiveXLibraryMeta>(nullptr, nullptr, ChildReferenceTranslator::Instance())},
		{PRP_ActiveXLibrary_coClasses, new DataProperty<ActiveXLibraryMeta>(nullptr, nullptr, ChildActiveXClassTranslator::Instance())},
		{PRP_ActiveXLibrary_dispInterfaces, new DataProperty<ActiveXLibraryMeta>(nullptr, nullptr, ChildActiveXClassTranslator::Instance())}
		});

	static DataMapper<ActiveXClassMeta> activeXClassMapper(DSKACTIVEXCLASS, &RootSchema::activeXClass, {
		{PRP_ActiveXClass_activeXLibrary, new DataProperty<ActiveXClassMeta>(nullptr, nullptr, ChildActiveXFeatureTranslator::Instance())},
		{PRP_ActiveXClass_activeXName, new DataProperty(&ActiveXClassMeta::activeXName)},
		{PRP_ActiveXClass_baseClass, new DataProperty(&ActiveXClassMeta::baseClass)},
		{PRP_ActiveXClass_defaultEventInterfaceGuid, new DataProperty(&ActiveXClassMeta::defaultEventInterfaceGuid)},
		{PRP_ActiveXClass_defaultInterfaceGuid, new DataProperty(&ActiveXClassMeta::defaultInterfaceGuid)},
		{PRP_ActiveXClass_dotNetName, new DataProperty(&ActiveXClassMeta::dotNetName)},
		{PRP_ActiveXClass_executeMode, new DataProperty(&ActiveXClassMeta::executeMode)},
		{PRP_ActiveXClass_guid, new DataProperty(&ActiveXClassMeta::guid)},
		{PRP_ActiveXClass_isEventInterface, new DataProperty(&ActiveXClassMeta::isEventInterface)},
		{PRP_ActiveXClass_key, new DataProperty(&ActiveXClassMeta::key)}
		});

	static DataMapper<ActiveXFeatureMeta> activeXFeatureMapper(DSKACTIVEXFEATURE, &RootSchema::activeXFeature, {
		{PRP_ActiveXFeature_feature, new DataProperty(&ActiveXFeatureMeta::feature)},
		{PRP_ActiveXFeature_memid, new DataProperty(&ActiveXFeatureMeta::memid)}
		});

	class ActiveXLibrary : public Entity
	{
	public:
		using Entity::Entity;

		string getName() const final
		{
			// Return name for base class, resolved via ActiveXClass
			return getProperty<Object>(PRP_ActiveXLibrary_activeXClass).getProperty<Class>(PRP_ActiveXClass_baseClass).getName();
		}

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			// Resolve base class
			Class baseClass;
			if (!baseClass.find(ancestor, &path, false))
				return false;

			// Dereference ActiveXClass
			auto activeXClass = baseClass.getProperty<Object>(PRP_Class_activeXClass);
			if (activeXClass.isNull())
				return false;

			// Dereference ActiveXLibrary
			oid = activeXClass.getProperty<DskObjectId>(PRP_ActiveXClass_activeXLibrary);
			return !oid.isNull();
		}

		DskObjectId GetParentId() const override
		{
			return getProperty<DskObjectId>(PRP_ActiveXLibrary__schema);
		}
	};
	static EntityRegistration<ActiveXLibrary> activeXLibrary(DSKACTIVEXLIBRARY);

	class ActiveXClass : public Entity
	{
	public:
		using Entity::Entity;

		string getName() const final
		{
			// Return name of related class
			return getProperty<Class>(PRP_ActiveXClass_baseClass).getName();
		}

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			throw unimplemented_feature();
		}
		
		DskObjectId GetParentId() const final
		{
			return getProperty<DskObjectId>(PRP_ActiveXClass_activeXLibrary);
		}
	};
	static EntityRegistration<ActiveXClass> activeXClass(DSKACTIVEXCLASS);

	class ActiveXFeature : public Entity
	{
	public:
		using Entity::Entity;

		string getName() const final
		{
			// Return name of related feature
			return getProperty<Object>(PRP_ActiveXFeature_feature)
				.getProperty<string>(PRP_SchemaEntity_name);
		}

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			throw unimplemented_feature();
		}

		DskObjectId GetParentId() const final
		{
			return getProperty<Object>(PRP_ActiveXFeature_feature)
				.getProperty<Class>(PRP_Feature_schemaType)
				.getProperty<DskObjectId>(PRP_Class_activeXClass);
		}
	};
	static EntityRegistration<ActiveXFeature> activeXFeature(DSKACTIVEXFEATURE);
}