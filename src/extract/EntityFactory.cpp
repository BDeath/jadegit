#include "EntityFactory.h"

using namespace std;

namespace JadeGit::Extract
{
	EntityFactory& EntityFactory::Get()
	{
		static EntityFactory f; return f;
	}

	void EntityFactory::Register(const ClassNumber& key, const Registration* registrar)
	{
		ObjectFactory::Register(key, registrar);
	}

	std::unique_ptr<Entity> EntityFactory::Create(const DskObjectId& oid) const
	{
		return std::unique_ptr<Entity>(static_cast<const Registration*>(Lookup(oid.classNo))->Create(oid));
	}

	std::unique_ptr<Entity> EntityFactory::Create(const ClassNumber& key) const
	{
		return  std::unique_ptr<Entity>(static_cast<const Registration*>(Lookup(key))->Create());
	}

	std::unique_ptr<Entity> EntityFactory::Create(const string& key) const
	{
		return std::unique_ptr<Entity>(static_cast<const Registration*>(Lookup(key))->Create());
	}

	unique_ptr<Entity> EntityFactory::resolve(const ClassNumber& klass, const QualifiedName& path, bool required) const
	{
		return resolve(klass, nullptr, path, required);
	}

	unique_ptr<Entity> EntityFactory::resolve(const ClassNumber& klass, const Entity* ancestor, const QualifiedName& path, bool required) const
	{
		std::unique_ptr<Entity> entity = EntityFactory::Get().Create(klass);

		if (!entity->find(ancestor, &path, required))
			return nullptr;

		return Create(entity->oid);
	}

	unique_ptr<Entity> EntityFactory::resolve(const string& klass, const QualifiedName& path, bool required) const
	{
		return resolve(klass, nullptr, path, required);
	}

	unique_ptr<Entity> EntityFactory::resolve(const string& klass, const Entity* ancestor, const QualifiedName& path, bool required) const
	{
		std::unique_ptr<Entity> entity = EntityFactory::Get().Create(klass);
		
		if (!entity->find(ancestor, &path, required))
			return nullptr;

		return Create(entity->oid);
	}
}