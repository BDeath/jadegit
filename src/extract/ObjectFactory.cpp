#include "ObjectFactory.h"

using namespace std;
using namespace Jade;

namespace JadeGit::Extract
{
	ObjectFactory& ObjectFactory::Get()
	{
		static ObjectFactory f; return f;
	}

	void ObjectFactory::Register(const ClassNumber& key, const Registration* registrar)
	{
		registry[key] = registrar;
	}

	unique_ptr<Object> ObjectFactory::Create(const DskObjectId& oid) const
	{
		return  unique_ptr<Object>(Lookup(oid.classNo)->Create(oid));
	}

	const ObjectFactory::Registration* ObjectFactory::Lookup(const string& key) const
	{
		/* Lookup class, validating name supplied */
		DskSchema rootSchema(&RootSchemaOid);
		DskClass klass;
		jade_throw(rootSchema.getLocalClass(widen(key).c_str(), klass));
		if (klass.isNull())
			throw logic_error("Unknown source object (" + key + ")");

		/* Lookup registration for class */
		if (auto result = Lookup(klass))
			return result;

		throw logic_error("Unhandled source object (" + key + ")");
	}

	const ObjectFactory::Registration* ObjectFactory::Lookup(const ClassNumber& key) const
	{
		/* Attempt basic find without needing to resolve class */
		auto iter = registry.find(actualClass(key));
		if (iter != registry.end())
			return iter->second;

		/* Lookup class, validating number supplied */
		DskSchema rootSchema(&RootSchemaOid);
		DskClass klass;
		jade_throw(rootSchema.getClassByNumber(actualClass(key), klass));
		if (klass.isNull())
			throw logic_error("Unknown source class number");

		/* Lookup registration for superclass */
		DskClass superclass;
		jade_throw(klass.getSuperclass(superclass));
		if (auto result = Lookup(superclass))
			return result;

		Character className[100];
		jade_throw(klass.getName(className, __LINE__));
		throw logic_error("Unhandled source component (" + narrow(className) + ")");
	}

	const ObjectFactory::Registration* ObjectFactory::Lookup(const DskClass& klass) const
	{
		DskClass cls(klass);
		while (!cls.isNull())
		{
			/* Get class number */
			ClassNumber number = 0;
			jade_throw(cls.getNumber(&number, __LINE__));

			/* Return registration if found */
			auto iter = registry.find(number);
			if (iter != registry.end())
				return iter->second;

			/* Get next super class */
			jade_throw(cls.getSuperclass(cls));
		}

		/* Unhandled component */
		return nullptr;
	}
}