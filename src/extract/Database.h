#pragma once
#include "SchemaEntity.h"

namespace JadeGit::Extract
{
	class Database : public SchemaEntity
	{
	public:
		using SchemaEntity::SchemaEntity;
	
	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override;
	};

	class DbFile;

	class DbClassMap : public Object
	{
	public:
		using Object::Object;
	};

	class DbFile : public SchemaEntity
	{
	public:
		using SchemaEntity::SchemaEntity;

		bool isShallow() const final;

	protected:
		void dependents(std::set<DskObjectId>& dependents) const override;
		bool lookup(const Entity* ancestor, const QualifiedName& path) override;
	};
}