#pragma once
#include <jade/Exception.h>
#include <jadegit/data/Object.h>
#include <jomobj.hpp>
#include <set>

namespace JadeGit::Extract
{
	class Assembly;

	class Object : public DskObject
	{
	public:
		using DskObject::DskObject;

		virtual std::string GetDisplay() const { return GetTypeName(); }
		virtual std::string GetTypeName() const;

		void Copy(Assembly& assembly, Data::Object* parent, const std::string& trail, bool deep, bool self = false) const;

		using DskObject::getProperty;
		int getProperty(const JomClassFeatureLevel& feature, Jade::String* value) const;

		template<class T>
		T getProperty(const JomClassFeatureLevel& feature) const
		{
			if (isNull())
				return T();

			T result;

			if constexpr (std::is_base_of_v<DskObject, T>)
				jade_throw(getProperty(feature, result));
			else if constexpr (std::is_same_v<Character, T>)
				jade_throw(getCharProperty(feature, &result));
			else
				jade_throw(getProperty(feature, &result));

			return result;
		}

#if defined(UNICODE)
		template<>
		std::string getProperty(const JomClassFeatureLevel& feature) const
		{
			return Jade::narrow(getProperty<Jade::String>(feature));
		}
#endif

		Integer64 getUpdateTranID() const;

		bool isKindOf(ClassNumber number) const;

		// Determines whether receiver has just been created as part of current transaction
		bool isNew() const;

		// Determines whether receiver is an original or copy of
		virtual bool isOriginal() const { return true; }

		// Determines if object is shallow (empty or proxy to another), which shouldn't be extracted
		virtual bool isShallow() const { return false; }

		Edition latestEdition() const;

	protected:
		// Resolves child object in the context of parent supplied
		virtual Data::Object* resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow = true) const;

		// Retrieves reference and inserts oids into set supplied (single or collection)
		void getProperty(const JomClassFeatureLevel& feature, std::set<DskObjectId>& oids) const;
	};
}