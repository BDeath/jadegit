#include "Constant.h"
#include "Type.h"
#include "DataMapper.h"
#include "EntityRegistration.h"
#include <jadegit/data/RootSchema/ConstantMeta.h>

using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static DataMapper<ConstantMeta> mapper(DSKCONSTANT, &RootSchema::constant, {
		{PRP_Constant_exportedConstantRefs, nullptr},
		{PRP_Constant_importedConstantUsages, nullptr},		// TODO: Need to extract these in future when support added for imported packages
		{PRP_Constant_length, nullptr},
		{PRP_Constant_precision, nullptr},
		{PRP_Constant_scaleFactor, nullptr},
		{PRP_Constant_value, nullptr}
		});

	static EntityRegistration<Constant> registrar(DSKCONSTANT);

	bool Constant::lookup(const Entity* ancestor, const QualifiedName& path)
	{
		return Entity::lookup<Type>(ancestor, path, PRP_Type_consts);
	}

	void Constant::dependents(std::set<DskObjectId>& dependents) const
	{
		Script::dependents(dependents);

		// Direct dependencies
		getProperty(PRP_Constant_exportedConstantRefs, dependents);
	}
}