#include "Development.h"
#include "DataMapper.h"
#include "DataTranslator.h"
#include "ObjectRegistration.h"
#include <jadegit/data/Development.h>
#include <jadegit/data/RootSchema/DevControlTypesMeta.h>
#include <jadegit/data/RootSchema/DevControlPropertiesMeta.h>
#include <Log.h>

using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	// Custom translator for DevControlProperties::cntrlType which handles fixing legacy systems where
	// cntrlType is 5 (now invalid) & name is helpContextId, which should now be 4. Refer to #90.
	class DevControlTypeTranslator : public DataTranslator, public Singleton<DevControlTypeTranslator>
	{
	public:
		DevControlTypeTranslator() : DataTranslator(true) {}

		void Copy(Assembly& assembly, const Object& source, const DskParam& value, Data::Object* target, const Data::Property* property, const std::string& name, const std::string& trail, bool deep) const final
		{
			assert(property);
			LOG_TRACE("Extracting attribute: cntrlType");

			assert(value.header.format == PFormat::DSKINTEGER);

			auto cntrlType = value.body.integer;

			// Check for legacy issue where helpContextId properties had a specific input type
			if (cntrlType == 5 && source.getProperty<std::string>(PRP_DevControlProperties_name) == "helpContextId")
			{
				cntrlType = 4;	// Reset to integer input type
			}

			target->SetValue(property, cntrlType);
		}
	};

	static DataMapper<DevControlTypesMeta> devControlTypesMapper(DSKDEVCONTROLTYPES, &RootSchema::devControlTypes, {
		{PRP_DevControlTypes_bitmap, new DataProperty(&DevControlTypesMeta::bitmap)},
		{PRP_DevControlTypes_bitmapDisabled, new DataProperty(&DevControlTypesMeta::bitmapDisabled)},
		{PRP_DevControlTypes_bitmapDown, new DataProperty(&DevControlTypesMeta::bitmapDown)},
		{PRP_DevControlTypes_bitmapID, new DataProperty(&DevControlTypesMeta::bitmapID)},
		{PRP_DevControlTypes_bitmapOver, new DataProperty(&DevControlTypesMeta::bitmapOver)},
		{PRP_DevControlTypes_controlPropsNameDict, new DataProperty<DevControlTypesMeta>(nullptr)},
		{PRP_DevControlTypes_hideFromPainterControlPalette, new DataProperty(&DevControlTypesMeta::hideFromPainterControlPalette)},
		{PRP_DevControlTypes_logicTypesSet, nullptr},
		{PRP_DevControlTypes_name, new DataProperty(&DevControlTypesMeta::name)},
		{PRP_DevControlTypes_windowClass, new DataProperty(&DevControlTypesMeta::windowClass)}
		});

	static DataMapper<DevControlPropertiesMeta> devControlPropertiesMapper(DSKDEVCONTROLPROPERTIES, &RootSchema::devControlProperties, {
		{PRP_DevControlProperties_cntrlType, new DataProperty(&DevControlPropertiesMeta::cntrlType, nullptr, DevControlTypeTranslator::Instance())},
		{PRP_DevControlProperties_name, new DataProperty(&DevControlPropertiesMeta::name)},
		{PRP_DevControlProperties_optionsList, new DataProperty(&DevControlPropertiesMeta::optionsList)},
		});

	static ObjectRegistration<DevControlClass> devControlTypes(DSKDEVCONTROLTYPES);
	static ObjectRegistration<DevControlProperties> devControlProperties(DSKDEVCONTROLPROPERTIES);

	Data::Object* DevControlClass::resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const
	{
		// Resolve existing control type
		if (Data::GUIClass* guiClass = dynamic_cast<Data::GUIClass*>(parent))
			if (guiClass->controlType)
				return guiClass->controlType;

		// Default to creating new instance
		return Development::resolve(assembly, parent, shallow);
	}

	Data::Object* DevControlProperties::resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const
	{
		// Resolve existing control properties
		if (Data::DevControlTypes* controlType = dynamic_cast<Data::DevControlTypes*>(parent))
			if (Data::DevControlProperties* prop = controlType->controlProps.Get(getProperty<std::string>(PRP_DevControlProperties_name)))
				return prop;

		// Default to creating new instance
		return Development::resolve(assembly, parent, shallow);
	}
}