#pragma once
#include "Entity.h"
#include <jadegit/data/Schema.h>

namespace JadeGit::Extract
{
	class Schema : public Entity
	{
	public:
		static bool isSupplied(const std::string& name);

		using Entity::Entity;
		using Entity::resolve;

		std::string getName() const override;

		bool IsDescendent(const DskObjectId& ancestor) const override;

		void children(std::set<DskObjectId>& children) const override;
		void dependents(std::set<DskObjectId>& dependents) const override;

	protected:
		DskObjectId GetParentId() const override { return NullDskObjectId; }

		bool lookup(const Entity* ancestor, const QualifiedName& path) override;
		Data::Schema* resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const override;
	};
}