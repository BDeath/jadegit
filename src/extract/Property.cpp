#include "Property.h"
#include "Type.h"
#include "Method.h"
#include "DataMapper.h"
#include "EntityRegistration.h"
#include <jadegit/data/RootSchema/PropertyMeta.h>

using namespace Jade;
using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static DataMapper<PropertyMeta> mapper(DSKPROPERTY, &RootSchema::property, {
		{PRP_Property__sysKeyPathRefs, nullptr},
		{PRP_Property__sysKeyRefs, nullptr},
		{PRP_Property__xmlAttribute, new DataProperty(&PropertyMeta::xmlAttribute)},
		{PRP_Property__xmlNillable, new DataProperty(&PropertyMeta::xmlNillable)},
		{PRP_Property_embedded, new DataProperty(&PropertyMeta::embedded)},
		{PRP_Property_exportedPropertyRefs, nullptr},
		{PRP_Property_hasControlInstance, nullptr},
		{PRP_Property_isHTMLProperty, new DataProperty(&PropertyMeta::isHTMLProperty)},
		{PRP_Property_ordinal, nullptr},
		{PRP_Property_key, nullptr},
		{PRP_Property_keyPathRefs, nullptr},
		{PRP_Property_keyRefs, nullptr},
		{PRP_Property_mappingCount, nullptr},
		{PRP_Property_required, new DataProperty(&PropertyMeta::required)},
		{PRP_Property_subId, nullptr},
		{PRP_Property_usedInCondition, nullptr},
		{PRP_Property_usedInWebService, new DataProperty(&PropertyMeta::webService)},
		{PRP_Property_virtual, new DataProperty(&PropertyMeta::virtual_)}
		});

	static EntityRegistration<Property> registrar(DSKPROPERTY);

	bool Property::lookup(const Entity* ancestor, const QualifiedName& path)
	{
		return Entity::lookup<Class>(ancestor, path, PRP_Class_properties);
	}

	void Property::dependents(std::set<DskObjectId>& dependents) const
	{
		Feature::dependents(dependents);

		// Direct dependencies
		getProperty(PRP_Property_exportedPropertyRefs, dependents);

		// Handle indirect dependencies via member key
		std::set<DskObjectId> keys;
		getProperty(PRP_Property_keyPathRefs, keys);
		getProperty(PRP_Property_keyRefs, keys);
		for (auto& key : keys)
		{
			DskObject memberKey(key);
			DskObjectId collClass = NullDskObjectId;
			jade_throw(memberKey.getProperty(PRP_Key_collClass, &collClass));
			if (!collClass.isNull())
				dependents.insert(collClass);
		}
	}

	void Property::GetNamesakes(std::set<DskObjectId>& namesakes) const
	{
		Feature::GetNamesakes(namesakes);

		// Get any associated mapping methods
		Integer mappingCount = 0;
		jade_throw(getProperty(PRP_Property_mappingCount, &mappingCount));

		if (mappingCount)
		{
			DskClass schemaType;
			jade_throw(getProperty(PRP_Feature_schemaType, schemaType));
			GetNamesakes(namesakes, &schemaType, widen(getName()));
		}
	}

	void Property::GetNamesakes(std::set<DskObjectId>& namesakes, const DskClass* schemaType, const String& name) const
	{
		// Check for local method 
		DskMethod local;
		jade_throw(schemaType->getLocalMethod(name.c_str(), local));

		if (!local.isNull())
		{
			Method(local.oid).GetNamesakes(namesakes);
		}
		else
		{
			// Recursively check subschema copies
			OrderedJomColl subschemaTypes;
			jade_throw(schemaType->subschemaTypes(subschemaTypes));
			JomObjp subschemaType = nullptr;
			subschemaTypes.start();
			while (subschemaTypes.next(subschemaType))
				GetNamesakes(namesakes, (DskClass*)subschemaType, name);

			// Recursively check subclasses
			OrderedJomColl subclasses;
			jade_throw(schemaType->subclasses(subclasses));
			JomObjp subclass = nullptr;
			subclasses.start();
			while (subclasses.next(subclass))
				GetNamesakes(namesakes, (DskClass*)subclass, name);
		}
	}
}