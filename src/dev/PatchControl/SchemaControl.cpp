#include "SchemaControl.h"
#include <dev/Development.h>
#include <dev/Command.h>
#include <dev/MsgBox.h>
#include <jade/Transaction.h>
#include <Exception.h>
#include <Platform.h>

using namespace std;
using namespace Jade;
using namespace JadeGit::Schema;

namespace JadeGit::Development
{
	bool SchemaControl::prelude(const ClassNumber& entityType, const QualifiedName& entityName, bool removal)
	{
		// Passthrough on reset
		if (!entityType)
			return PatchControl::prelude(entityType, entityName, removal);

		// Get schema name
		auto& name = entityName.first().name;

		// Query schema status, setting up if required
		switch (status(name))
		{
		case GitSchema::Status::Excluded:
		{
			// Reset rest of chain and ignore
			return PatchControl::prelude(0, string(), removal);
		}
		case GitSchema::Status::Included:
		{
			// Pass onto next in chain
			return PatchControl::prelude(entityType, entityName, removal);
		}
		default:
			return false;
		}
	}

	bool SchemaControl::execute(Session& session, const string& entityType, const QualifiedName& entityName, const string& operation)
	{
		// Passthrough control rename operations (entityName is not prefixed with schema, so can't be checked below)
		if (operation == "CR")
			return PatchControl::execute(session, entityType, entityName, operation);
		
		// Get schema name
		auto& name = entityName.first().name;

		// Determine if schema is being added or deleted directly
		bool adding = false;
		bool deleting = false;
		
		if (entityType == "Schema")
		{
			if (operation == "A" || operation == "AS")
				adding = true;
			else if (operation == "D")
				deleting = true;
		}

		// Query schema status, setting up if not deleting, prompting if not adding explicitly
		switch (status(name, operation != "D", adding))
		{
		case GitSchema::Status::Excluded:
		{
			// Forget excluded schemas on deletion
			if (deleting)
				GitSchema::forget(name);

			// Suppress source control when schema has been excluded
			return true;
		}
		case GitSchema::Status::Included:
		{
			// Schema deletion currently unsupported as it cannot be tracked atomically (DSL hook isn't called in transaction state anymore)
			if (deleting && !Transaction::InTransactionState() && jadeVersion < par68417)
				throw unsupported_feature("Deleting schemas", par68417);
			
			// Pass onto next in chain
			return PatchControl::execute(session, entityType, entityName, operation);
		}
		case GitSchema::Status::Unknown:
		{
			// Ignore unregistered schema deletion, or child entity deletion, which may be part of deleting the whole schema
			// Instead, we rely on add/update operations to prompt the initial setup of a unknown schema
			return (deleting || operation == "D");
		}
		default:
			return false;
		}
	}

	// Handles querying schema status, setting up if required
	GitSchema::Status SchemaControl::status(const string& name, bool setup, bool adding)
	{
		// Query current status
		auto status = GitSchema::status(name);

		// Setup if required
		if (setup && status == GitSchema::Status::Unknown)
		{
			// If not adding explicitly, prompt user to confirm whether schema should be added to source control
			MsgBox::Result prompt = MsgBox::Result::Cancel;
			if (!adding)
			{
				prompt = MsgBox::display(name + " has not been associated with a repository.\n\n" +
					"Would you like to add this schema to source control?", MsgBox::Style::Warning, MsgBox::Options::Yes_No_Cancel, MsgBox::DefaultOption::Third);

				if (prompt == MsgBox::Result::No)		// Schema needs to be excluded
				{
					GitSchema::exclude(name);
					return GitSchema::Status::Excluded;
				}
				else if (prompt != MsgBox::Result::Yes)	// User cancelled
					return GitSchema::Status::Blocked;
			}

			// Setup schema
			SetupSchemaCommand command(name);
			if (!command.execute())
				return GitSchema::Status::Blocked;

			// Repeat status query to verify setup
			status = GitSchema::status(name);

			// Add schema implicitly (ensuring we're in transaction state), if not adding explicitly
			if (!adding && status == GitSchema::Status::Included)
			{
				Transaction transaction;
				if (!PatchControl::execute("Schema", name, "A"))
					return GitSchema::Status::Blocked;
				transaction.commit();
			}
		}

		return status;
	}
}