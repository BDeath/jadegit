#include "LocaleControl.h"
#include <jade/Exception.h>
#include <jomobj.hpp>

using namespace Jade;

namespace JadeGit::Development
{
	bool LocaleControl::execute(Session& session, const std::string& entityType, const QualifiedName& entityName, const std::string& operation)
	{
		if (entityType == "Locale")
		{
			// Lookup schema
			DskSchema schema;
			DskSchema(&RootSchemaOid).findSchema(widen(entityName.first().name).c_str(), schema);

			// Cater for JADE fault where hook is called for adding/deleting first/last locale rather than schema
			if (operation == "A" || operation == "D")
			{
				// Get locales
				OrderedJomColl locales;
				schema.locales(locales);

				// Schema is being added or deleted if this is the first or last locale
				if (locales.size() == 1)
					return PatchControl::execute(session, "Schema", entityName.first(), operation);
			}
			
			if (operation != "D")
			{
				// Retrieve primary locale
				DskObject locale;
				jade_throw(schema.getProperty(PRP_Schema_primaryLocale, locale));

				if (!locale.isNull())
				{
					// Retieve primary locale name
					Character buffer[101];
					jade_throw(locale.getProperty(PRP_SchemaEntity_name, buffer));

					// Schema update is implied when primary/default locale is affected by change
					if (narrow(buffer) == entityName.name && !PatchControl::execute(session, "Schema", entityName.first(), "U"))
						return false;
				}
			}
		}

		return PatchControl::execute(session, entityType, entityName, operation);
	}
}