#pragma once
#include <jomoid.hpp>
#include <set>
#include <string>

namespace JadeGit::Extract
{
	class Entity;
}

namespace JadeGit::Schema
{
	class GitChange;
}

namespace JadeGit::Development
{
	class EntityRenameStrategy
	{
	public:
		bool rename(const Extract::Entity& actual, Schema::GitChange& change);
		Schema::GitChange rename(const Extract::Entity& actual, const std::string& previousName) const;
		virtual void reset();
		virtual void save(const Extract::Entity& actual) {}

	protected:
		virtual bool previousName(const Extract::Entity& actual, std::string& name) const = 0;

	private:
		Integer64 tranid = 0;
		std::set<DskObjectId> renamed;
		std::set<DskObjectId> changes;
	};

	// Rename strategy which relies on prelude notification of entity before update to store current name
	class EntityRenamePreludeStrategy : public EntityRenameStrategy
	{
	public:
		virtual void reset() override;
		virtual void save(const Extract::Entity& actual) override;

	protected:
		bool previousName(const Extract::Entity& actual, std::string& name) const override;

	private:
		// Details about last entity opened (which may be renamed)
		std::string name;
		std::set<DskObjectId> targets;
	};

	// Rename strategy which uses background thread to retrieve current name of entity already updated, if it's not been stored before update
	class EntityRenameBackgroundStrategy : public EntityRenamePreludeStrategy
	{
	protected:
		bool previousName(const Extract::Entity& actual, std::string& name) const override;
	};
}