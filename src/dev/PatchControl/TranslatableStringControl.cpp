#include "TranslatableStringControl.h"
#include <jade/Transaction.h>
#include <Exception.h>
#include <Platform.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Development
{
	bool TranslatableStringControl::execute(Session& session, const string& entityType, const QualifiedName& entityName, const string& operation)
	{
		if (operation == "D" && entityType == "TranslatableString")
		{
			// Deletion currently unsupported as it cannot be tracked atomically (DSL hook isn't called in transaction state)
			if (!Transaction::InTransactionState() && jadeVersion < par68418)
				throw unsupported_feature("Deleting translatable strings", par68418);
		}

		return PatchControl::execute(session, entityType, entityName, operation);
	}
}