#pragma once
#include "PatchControl.h"

namespace JadeGit::Development
{
	class TranslatableStringControl : public PatchControl
	{
	public:
		bool execute(Session& session, const std::string& entityType, const QualifiedName& entityName, const std::string& operation) override;
	};
}