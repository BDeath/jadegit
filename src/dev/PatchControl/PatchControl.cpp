#include "ActiveXControl.h"
#include "DeploymentControl.h"
#include "EntityRenameStrategy.h"
#include "FormControl.h"
#include "HTMLDocumentControl.h"
#include "LocaleControl.h"
#include "SchemaControl.h"
#include "TranslatableStringControl.h"
#include <dev/Development.h>
#include <dev/MsgBox.h>
#include <dev/Session.h>
#include <extract/Entity.h>
#include <jade/AppContext.h>
#include <jade/Loader.h>
#include <jade/Transaction.h>
#include <schema/data/Change.h>
#include <Log.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Development
{
	PatchControl& PatchControl::get()
	{
		// Patch control in chain of responsibility
		static thread_local unique_ptr<PatchControl> root = nullptr;
		if (!root)
		{
			root = make_unique<DeploymentControl>();
			root->add(make_unique<LocaleControl>());
			root->add(make_unique<SchemaControl>());
			root->add(make_unique<ActiveXControl>());
			root->add(make_unique<FormControl>());
			root->add(make_unique<HTMLDocumentControl>());
			root->add(make_unique<TranslatableStringControl>());
			root->add(make_unique<EntityControl>());
		}

		return *root;
	}
	
	// Parse patch details into separate fields
	void parsePatchDetails(string patchDetails, int& entityPatch, int& userPatch, string& entityPatchStatus)
	{
		size_t start = 0;
		size_t end = patchDetails.find(':');
		if (end == string::npos)
		{
			if (patchDetails == "")
				userPatch = 0;
			else
				userPatch = stoi(patchDetails);

			entityPatch = 0;
			entityPatchStatus = "";
			return;
		}

		entityPatch = stoi(patchDetails.substr(start, end - start));

		start = end + 1;
		end = patchDetails.find(':', start);

		if (end == String::npos)
		{
			userPatch = stoi(patchDetails.substr(start));
			entityPatchStatus = "";
		}
		else
		{
			userPatch = stoi(patchDetails.substr(start, end - start));
			entityPatchStatus = patchDetails.substr(end + 1);
		}
	}

	bool PatchControl::execute(const string& patchDetails, const string& entityType, const string& entityName, const string& operation)
	{
		// Handle sign-on scenario
		if (entityType.empty() && entityName.empty() && operation.empty())
		{
			// Parse patch details
			int entityPatch;				// Patch under which entity was previously modified
			int userPatch;					// Current patch set by user
			string entityPatchStatus;		// N if entity is being modified for the first time under user patch, O if has been done previously, C if user patch is closed

			parsePatchDetails(patchDetails, entityPatch, userPatch, entityPatchStatus);
			
			// SET implied if user patch number is supplied  
			if (userPatch)
				return false;	// not using patch functionality in general

			// Ignore scenario where there's no operation or entity update specified otherwise
			else
				return true;
		}

		// Just using patch control hooks, not patch functionality in general
		if (operation == "SET" || operation == "UNSET")
			return false;

		// Ignore any 'after commit' operations
		if (operation.size() > 2 && 0 == operation.compare(operation.size() - 2, 2, "AC"))
			return true;

		// Convert entity name to qualified name
		QualifiedName qualifiedName(entityName);

		// Ignore entities being deleted that are being converted to another type (i.e. ExplicitInverseRef <=> ImplicitInverseRef)
		if (operation == "D" && qualifiedName.name == "__deleted")
			return true;

		// Ignore imported types, no need to extract these as they're inferred from the exported types when imported package is loaded
		if (entityType == "JadeImportedClass" || entityType == "JadeImportedInterface")
			return true;

		// Schema views are currently ignored (incomplete/inconsistent patch control & schema load behaviour, but hook is called when saving schema view text)
		if (entityType == "SchemaView")
			return true;

		// Pass onto chain of responsibiliy to handle
		return execute(entityType, qualifiedName, operation);
	}

	void PatchControl::add(unique_ptr<PatchControl> n)
	{
		if (next)
			next->add(move(n));
		else
			next = move(n);
	}

	bool PatchControl::execute(const string& entityType, const QualifiedName& entityName, const string& operation)
	{
		return execute(Session::resolve(), entityType, entityName, operation);
	}

	bool PatchControl::execute(Session& session, const string& entityType, const QualifiedName& entityName, const string& operation)
	{
		return next->execute(session, entityType, entityName, operation);
	}

	bool PatchControl::prelude(const ClassNumber& entityType, const string& entityName, bool removal)
	{
		return prelude(entityType, QualifiedName(entityName), removal);
	}

	bool PatchControl::prelude(const ClassNumber& entityType, const QualifiedName& entityName, bool removal)
	{
		return !next || next->prelude(entityType, entityName, removal);
	}

	int jadePatchControl(const Character* pUserName, const Character* pPatchDetails, const Character* pEntityName, const Character* pEntityType, const Character* pOperation)
	{
		try
		{
			auto userName = narrow(pUserName);
			auto patchDetails = narrow(pPatchDetails);
			auto entityName = narrow(pEntityName);
			auto entityType = narrow(pEntityType);
			auto operation = narrow(pOperation);

			// Build log message, appending asterisk when called outside of transaction
			auto msg = format("PatchControl: userName={}, patchDetails={}, entityName={}, entityType={}, operation={}", userName, patchDetails, entityName, entityType, operation);
			if (!Transaction::InTransactionState())
				msg += "*";

			// Use trace logging during automated load
			if (Jade::Loader::isLoader())
			{
				LOG_TRACE(msg);
			}
			else
				log(msg);

			return PatchControl::get().execute(patchDetails, entityType, entityName, operation) ? JADEGIT_ALLOW : JADEGIT_DENY;
		}
		catch (exception& e)
		{
			MsgBox::display(e);
		}

		return JADEGIT_ERROR;
	}
}