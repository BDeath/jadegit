#pragma once
#include <jadegit/data/QualifiedName.h>
#include <jomtypes.h>

namespace JadeGit::Development
{
	class Session;

	class PatchControl
	{
	public:
		static PatchControl& get();

		bool prelude(const ClassNumber& entityType, const std::string& entityName, bool removal = false);
		bool execute(const std::string& patchDetails, const std::string& entityType, const std::string& entityName, const std::string& operation);

	protected:
		virtual bool prelude(const ClassNumber& entityType, const QualifiedName& entityName, bool removal);
		virtual bool execute(const std::string& entityType, const QualifiedName& entityName, const std::string& operation);
		virtual bool execute(Session& session, const std::string& entityType, const QualifiedName& entityName, const std::string& operation);

	private:
		std::unique_ptr<PatchControl> next = nullptr;

		void add(std::unique_ptr<PatchControl> next);
	};
}