#include "MsgBox.h"
#include <jade/AppContext.h>
#include <jade/String.h>
#include <Exception.h>
#include <Log.h>
#include <sstream>

using namespace std;
using namespace Jade;

namespace JadeGit::Development
{
	MsgBox::Result MsgBox::display(string message, const char* title, MsgBox::Style style, MsgBox::Options options, MsgBox::DefaultOption defaultOption, MsgBox::Scope scope)
	{
		ShortDskParam meth;
		paramSetCString(meth, TEXT("msgBox"));

		DskParamCString pMessage(message);
		DskParamCString pTitle(title);
		DskParam pFlags;
		DskParam pIn;
		DskParam pOut;

		paramSetInteger(pFlags, style + options + defaultOption + scope);
		paramSetParamList(pIn, &pMessage, &pTitle, &pFlags);

		jomSendMsg(nullptr, AppContext::GetApp(), (DskParam*)&meth, &pIn, &pOut, __LINE__);

		int result;
		paramGetInteger(pOut, result);

		return (MsgBox::Result)result;
	}

	MsgBox::Result MsgBox::display(string message, Style style, Options options, DefaultOption defaultOption, Scope scope)
	{
		return display(move(message), "Jade-Git", style, options, defaultOption, scope);
	}

	void MsgBox::display(const exception& exception)
	{
		log(exception);
		displayError(exception.what());
	}

	void MsgBox::displayError(string message)
	{
		display(move(message), Error);
	}

	bool MsgBox::displayWarning(string message)
	{
		return display(move(message), Warning, Yes_No, Second) == Yes;
	}
}