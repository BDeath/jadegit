#pragma once
#include <schema/Session.h>
#include <schema/data/User.h>
#include <mutex>

namespace JadeGit::Development
{
	class ICommand
	{
	public:
		virtual void GetInfo(DskParam& userInfo) const = 0;
		virtual bool ProcessReply(DskParam& params) = 0;
	};

	class Session : public Schema::Session
	{
	public:
		static Session* find(DskObjectId browser);
		static Session& resolve();
		static Session* signOn(const std::string& userName);

		void SignOn(DskObjectId* user);
		void SignOff();
		bool Start();

		std::string Initialized();
		void Stalled(int errorCode);
		void Finalized();

		bool Send(ICommand& command, NoteEventType eventType);
		bool ProcessReply(DskParam& params);

		const std::string userName;
		const Schema::User& user() const final { return user_; }

		~Session();

	private:
		Session(const std::string& userName, DskObjectId browser);

		enum State
		{
			Stopped = 0,
			Starting = 1,
			Started = 2,
			SignedOn = 3
		};

		State state = Stopped;
		int stalledError = 0;

		// Associated user
		Schema::User user_;

		// Associated processes
		DskObjectId interop = NullDskObjectId;
		DskObjectId browser = NullDskObjectId;
	
		// Concurrency variables
		std::mutex mtx;
		std::condition_variable cv;

		// Remote command execution
		ICommand* command = nullptr;
		bool result = false;
	};
}