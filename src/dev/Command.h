#pragma once
#include "Session.h"

namespace JadeGit::Development
{
	class Command : protected ICommand
	{
	public:
		Command(NoteEventType eventType) : eventType(eventType) {}

		bool execute();

	private:
		const NoteEventType eventType;
	};

	class SetupSchemaCommand : public Command
	{
	public:
		SetupSchemaCommand(const std::string& schemaName);

	protected:
		void GetInfo(DskParam& userInfo) const override;
		bool ProcessReply(DskParam& params) override;

		const Jade::String schemaName;
	};
}