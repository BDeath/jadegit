#include "Function.h"
#include <dev/PatchControl/PatchControl.h>

namespace JadeGit::Development
{
	// Delete function (those for which we need to remember associates of entity being deleted)
	class DeleteFunction : public Function
	{
	public:
		DeleteFunction(std::initializer_list<const char*> taskNames, ClassNumber entityType) : Function(taskNames), entityType(entityType) {}
		DeleteFunction(const char* taskName, ClassNumber entityType) : DeleteFunction({ taskName }, entityType) {};

	protected:
		bool execute(const std::string& entityName) const override final
		{
			// Notify patch control of entity that's about to be deleted
			return PatchControl::get().prelude(entityType, entityName, true);
		}

	private:
		ClassNumber entityType;
	};

	// Map delete functions to entity types that may have associates to delete simultaneously
	static DeleteFunction removeForm(Function::RemoveForm, DSKFORM);
	static DeleteFunction removeType(Function::RemoveType, DSKTYPE);
	static DeleteFunction removeActiveXLibrary({ Function::RemoveActiveX, Function::RemoveDotNetLib }, DSKACTIVEXLIBRARY);
	static DeleteFunction removeHTMLDocument(Function::RemoveHTMLDocument, DSKJADEHTMLDOCUMENT);
}