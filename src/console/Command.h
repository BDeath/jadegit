#pragma once
#include <CLI/CLI.hpp>

namespace JadeGit::Console
{
	class Session;

	class Command
	{
	public:
		Command(CLI::App& cmd, Session& session) : session(session) {}

		virtual void execute() = 0;

		void print(std::string message) const;
		void print_time() const;

	protected:
		Session& session;
	};
}