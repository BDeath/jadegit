#pragma once
#include <jadegit/arch.h>

namespace JadeGit::Console
{
	JADEGIT_EXPORT int run(int argc, char* argv[]);
}