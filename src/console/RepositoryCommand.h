#pragma once
#include "Command.h"
#include <jadegit/git2.h>

namespace JadeGit::Console
{
	class RepositoryCommand : public Command
	{
	public:
		RepositoryCommand(CLI::App& cmd, Session& session, bool updating = false) : Command(cmd, session), updating(updating) {}

		void execute() override;

	protected:
		bool updating = false;
		void open() const;
		void open(const std::string& path) const;
		void open(std::unique_ptr<git_reference> branch) const;
	};
}