#include "RepositoryCommand.h"
#include "CommandRegistration.h"
#include "Session.h"
#include <data/EntityIDPreserver.h>
#include <extract/Assembly.h>
#include <vfs/FileSignature.h>
#include <vfs/GitFileSystem.h>

using namespace std;

namespace JadeGit::Console
{
	class Extract : public RepositoryCommand
	{
	public:
		static constexpr const char* description = "Extract schemas";

		Extract(CLI::App& cmd, Session& context) : RepositoryCommand(cmd, context, true)
		{
			cmd.add_flag("--commit-force", this->commit_force, "Force creating new commit")->needs(
				cmd.add_option("--commit-message", this->commit_message, "Commit message")
			);
			cmd.add_option("--original-commit", this->original_commit, "Commit created previously while performing same extract");
			cmd.add_option("--schemas", this->schemas, "Specific schemas to extract");
		}

		void execute() final
		{
			// open repository if required
			RepositoryCommand::execute();

			// Get repository
			git_repository* repo = session.repo.get();

			// Lookup original commit
			unique_ptr<git_commit> original_commit;
			if (!this->original_commit.empty())
			{
				git_oid original_commit_id;
				git_throw(git_oid_fromstrp(&original_commit_id, this->original_commit.c_str()));
				git_commit_lookup(git_ptr(original_commit), repo, &original_commit_id);
			}

			// Setup entity ID preserver for original commit
			unique_ptr<Data::EntityIDPreserver> original_preserver;
			if (original_commit)
				original_preserver = make_unique<Data::EntityIDPreserver>(make_unique<GitFileSystem>(original_commit.get(), FileSignature::suppressed()));

			// Get repository index
			unique_ptr<git_index> index;
			git_throw(git_repository_index(git_ptr(index), repo));

			// Setup file system
			GitFileSystem files(index.get(), FileSignature::suppressed());

			// Extract schemas (implicitly saves/stages changes)
			JadeGit::Extract::Assembly(files).extract(schemas, [&](const string& message) { print(message); });

			// Write index to tree
			git_oid tree_id;
			git_throw(git_index_write_tree(&tree_id, index.get()));

			// Lookup parent commit, except for initial commit (unborn repository)
			unique_ptr<git_commit> parent;
			{
				unique_ptr<git_reference> head;
				auto error = git_repository_head(git_ptr(head), repo);
				if (error != GIT_EUNBORNBRANCH)
				{
					git_throw(error);
					git_throw(git_commit_lookup(git_ptr(parent), repo, git_reference_target(head.get())));
				}
			}

			// Determine if we're done on basis there's been no changes
			if (!commit_force && parent && git_oid_equal(git_commit_tree_id(parent.get()), &tree_id))
			{
				print("Done (no changes)");
				return;
			}

			// Done if we don't need to commit (depends on message being supplied)
			if (commit_message.empty())
			{
				print("Done");
				return;
			}

			// Preserve original commit if message, tree & parent match
			if (original_commit && !commit_force &&
				git_commit_message_raw(original_commit.get()) == commit_message &&
				git_oid_equal(git_commit_tree_id(original_commit.get()), &tree_id) &&
				git_commit_parentcount(original_commit.get()) == (parent ? 1 : 0) && 
				(!parent || git_oid_equal(git_commit_parent_id(original_commit.get(), 0), git_commit_id(parent.get()))))
			{
				git_throw(git_reset(repo, (git_object*)(original_commit.get()), git_reset_t::GIT_RESET_SOFT, nullptr));
				print("Done (preserved original commit)");
				return;
			}

			// Lookup tree
			unique_ptr<git_tree> tree;
			git_throw(git_tree_lookup(git_ptr(tree), repo, &tree_id));

			// Get signature
			unique_ptr<git_signature> author;
			git_throw(git_signature_default(git_ptr(author), repo));

			// Create commit with zero or one parent commit
			const git_commit* parents[] = { parent.get() };
			git_oid id;
			git_throw(git_commit_create(&id, repo, "HEAD", author.get(), author.get(), nullptr, commit_message.c_str(), tree.get(), parent ? 1 : 0, parents));
			print(format("Done (new commit: {})", git_oid_tostr_s(&id)));
		}

	protected:
		bool commit_force = false;
		string commit_message;
		string original_commit;
		vector<string> schemas;
	};
	static CommandRegistration<Extract> registration("extract");
}