#include "Command.h"
#include "CommandRegistration.h"
#include "Session.h"
#include <deploy/Commands.h>
#include <deploy/Manifest.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Deploy;

namespace JadeGit::Console
{
	class DeployCommand : public Command
	{
	public:
		static constexpr const char* description = "Deployment commands";

		DeployCommand(CLI::App& cmd, Session& session) : Command(cmd, session)
		{
			// Need at least one subcommand
			cmd.require_subcommand(1);

			// Setup manifest subcommands
			{
				auto manifest = cmd.add_subcommand("manifest", "Manifest commands (deprecated)")->require_subcommand(1);

				{
					auto get = manifest->add_subcommand("get", "Retrieve manifest")->require_subcommand();
					get->add_subcommand("origin", "Display origin")->callback([this]() { cout << Manifest::get().origin << endl; });
					get->add_subcommand("commit", "Display commit")->callback([this]() { cout << Manifest::get().commit << endl; });
				}

				{
					auto set = manifest->add_subcommand("set", "Update manifest")->require_option();
					set->add_option("-o,--origin", this->origin, "Source repository")->capture_default_str();
					set->add_option("-c,--commit", this->commit, "Commit ID")->capture_default_str();
					set->callback([this]() { Manifest::set(this->origin, this->commit); });
				}
			}

			// Start
			auto start = cmd.add_subcommand("start", "Start deployment");
			start->add_option("--registry", this->registry, "Path to registry update file to be validated")->check(CLI::ExistingFile)->required();
			start->callback([this]() { Deploy::start(this->registry); });

			// Finish
			auto finish = cmd.add_subcommand("finish", "Finish deployment");
			finish->add_option("--registry", this->registry, "Path to registry update file to be loaded")->check(CLI::ExistingFile)->required();
			finish->callback([this]() { Deploy::finish(this->registry); });
		}

		void execute() final
		{
			// Not applicable
		}

	protected:
		string origin;
		string commit;
		string registry;
	};
	static CommandRegistration<DeployCommand, true> registration("deploy");
}