#include "RepositoryCommand.h"
#include "Session.h"
#include <git2/sys/repository.h>

using namespace std;

namespace JadeGit::Console
{
	void RepositoryCommand::execute()
	{
		// open repository if required, assuming current working directory
		if (!session.repo)
			open(".");
		
		// check bare repository has been opened for updates 
		if (updating && !git_repository_is_bare(session.repo.get()))
			throw runtime_error("Bare repository expected");
	}
	
	void RepositoryCommand::open() const
	{
		// reset session
		session.branch.reset();
		session.commit.reset();
		session.index.reset();

		// lookup head
		unique_ptr<git_reference> branch;
		auto error = git_repository_head(git_ptr(branch), session.repo.get());
		if (error == GIT_EUNBORNBRANCH)
		{
			// create empty index for initial commit
			git_throw(git_index_new(git_ptr(session.index)));
			git_repository_set_index(session.repo.get(), session.index.get());
			return;
		}
		else
			git_throw(error);

		open(move(branch));
	}

	void RepositoryCommand::open(const std::string& path) const
	{
		git_throw(git_repository_open(git_ptr(session.repo), path.c_str()));

		open();
	}

	void RepositoryCommand::open(unique_ptr<git_reference> branch) const
	{
		// lookup commit
		unique_ptr<git_commit> commit;
		git_throw(git_commit_lookup(git_ptr(commit), session.repo.get(), git_reference_target(branch.get())));

		// lookup tree
		unique_ptr<git_tree> tree;
		git_throw(git_commit_tree(git_ptr(tree), commit.get()));

		// setup index
		unique_ptr<git_index> index;
		git_throw(git_index_new(git_ptr(index)));

		// load index
		git_throw(git_index_read_tree(index.get(), tree.get()));

		// swap session references on success
		session.branch.swap(branch);
		session.commit.swap(commit);
		session.index.swap(index);
		git_repository_set_index(session.repo.get(), session.index.get());
	}
}