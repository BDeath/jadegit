#include "Command.h"
#include "Session.h"

using namespace std;
using namespace std::chrono;

namespace JadeGit::Console
{
	void Command::print(std::string message) const
	{
		print_time();
		cout << message << endl;
	}

	void Command::print_time() const
	{
		duration<double, milli> ms = high_resolution_clock::now() - session.started;

		auto sec = duration_cast<seconds>(ms).count();
		auto min = sec / 60;
		sec -= (min * 60);

		cout << "[" << setw(2) << setfill('0') << min << ":" << setw(2) << setfill('0') << sec << "] ";
	}
}