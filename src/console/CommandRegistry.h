#pragma once
#include <CLI/CLI.hpp>
#include <vector>

namespace JadeGit::Console
{
	class Session;

	class CommandRegistry
	{
	public:
		static CommandRegistry& get();

		class Registration
		{
		public:
			Registration();

			virtual void setup(CLI::App& app, Session& session, bool shell) const = 0;
		};

		void setup(CLI::App& app, Session& session, bool shell) const;

	protected:
		CommandRegistry();

	private:
		std::vector<const Registration*> registry;
	};
}