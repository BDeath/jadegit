#include "Progress.h"
#include "Session.h"
#include <iostream>
#include <iomanip>

using namespace std;
using namespace std::chrono;

namespace JadeGit::Console
{
	Progress::Progress(const Command& command, const Session& session) : command(command), session(session) {}

	Progress::~Progress()
	{
		reset();

		if (!uncaught_exceptions())
		{
			command.print("Done");
		}
	}

	void Progress::message(const string& message)
	{
		reset();

		command.print(message.c_str());	// TODO: Handle embedded \n characters?

		display();
	}

	void Progress::update(double progress, const char* caption)
	{
		if (!caption && static_cast<unsigned int>(progress * 100) == this->progress)
			return;

		this->progress = static_cast<unsigned int>(progress * 100);

		if (caption)
		{
			reset();
			command.print_time();
			cout << caption << endl;
		}

		// Log progress if format specified (non-interative only)
		if (!session.log_progress_format.empty())
			cout << vformat(session.log_progress_format, make_format_args(this->progress)) << endl;

		display();
	}

	void Progress::display()
	{
		if (!session.interactive)
			return;

		stringstream bar;
		bar << "[" << string((progress / 4), '=') << string(25 - (progress / 4), ' ') << "]" << setw(4) << setfill(' ') << progress << "%";

		auto newlen = bar.str().length();		
		cerr << (length ? "\r" : "") << bar.str() << (length > newlen ? string(length - newlen, ' ') : "");
		length = newlen;
	}

	void Progress::reset()
	{
		if (!length)
			return;

		cerr << "\r" << string(length, ' ') << "\r";
		length = 0;
	}
}