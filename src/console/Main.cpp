#include "Application.h"
#include <git2.h>
#include <filesystem>
#include <iomanip>
#include <sstream>
#include <thread>

#include <Windows.h>
#include <DbgHelp.h>
#pragma comment(lib, "dbghelp.lib")

using namespace std;

filesystem::path minidump_filename(filesystem::path module_filename)
{
    time_t rawtime;
    time(&rawtime);

    struct tm timeinfo;
    localtime_s(&timeinfo, &rawtime);

    char buffer[80];
    strftime(buffer, sizeof(buffer), "%Y%m%d_%H%M%S", &timeinfo);

    ostringstream name;
    name << module_filename.stem().generic_string() << "_" << buffer << "_" << hex << this_thread::get_id() << ".dmp";

    return module_filename.parent_path().parent_path() / "ProcessDumps" / name.str();
}

BOOL CALLBACK minidump_callback(PVOID param, const PMINIDUMP_CALLBACK_INPUT input, PMINIDUMP_CALLBACK_OUTPUT output)
{
	// Check parameters
	if (!input || !output)
		return false;

	// Process the callback
	switch (input->CallbackType)
	{
	case IncludeModuleCallback:		// Include all modules in the dump
	case IncludeThreadCallback:		// Include all threads in the dump
	case ThreadCallback:			// Include all thread information
	case ThreadExCallback:
	{
		return true;
	}
	case ModuleCallback:
	{
		// Filter module data sections
		if (output->ModuleWriteFlags & ModuleWriteDataSeg)
		{
			// Extract module name
			auto name = filesystem::path(input->Module.FullPath).stem().generic_string();

			// Exclude all modules except ntdll and any with jadegit prefix
			if (name != "ntdll" && name.find("jadegit") != 0)
				output->ModuleWriteFlags &= (~ModuleWriteDataSeg);
		}

		return true;
	}
	default:
		return false;
	}
}

LONG WINAPI minidump(EXCEPTION_POINTERS* ExceptionInfo)
{
    // Get executable path
    char buffer[MAX_PATH];
    DWORD bufSize = sizeof(char) * MAX_PATH;
    if(GetModuleFileNameA(nullptr, buffer, bufSize) == bufSize)
        return EXCEPTION_CONTINUE_SEARCH;

    // Determine minidump filename
    auto filename = minidump_filename(buffer);

    // Ensure directory exits
    filesystem::create_directories(filename.parent_path());

	// Open file
    HANDLE file = CreateFileW(filename.c_str(), GENERIC_READ | GENERIC_WRITE, 0, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr);
    if (!file)
        return EXCEPTION_CONTINUE_SEARCH;

	// Define type
	MINIDUMP_TYPE mdt = (MINIDUMP_TYPE)(MiniDumpWithPrivateReadWriteMemory |
										MiniDumpWithDataSegs |
										MiniDumpWithHandleData |
										MiniDumpWithFullMemoryInfo |
										MiniDumpWithThreadInfo |
										MiniDumpWithUnloadedModules);

	// Setup exception info
	MINIDUMP_EXCEPTION_INFORMATION mei;
	mei.ThreadId = GetCurrentThreadId();
	mei.ClientPointers = true;
	mei.ExceptionPointers = ExceptionInfo;

	// Setup callback info
	MINIDUMP_CALLBACK_INFORMATION mci;
	mci.CallbackRoutine = minidump_callback;
	mci.CallbackParam = 0;

	// Produce minidump
	bool result = MiniDumpWriteDump(GetCurrentProcess(), GetCurrentProcessId(), file, mdt, &mei, nullptr, &mci);

	// Release lock on file
	CloseHandle(file);
	return result ? EXCEPTION_EXECUTE_HANDLER : EXCEPTION_CONTINUE_SEARCH;
}

namespace JadeGit::Console
{
    class Main
    {
    public:
        Main()
        {
            git_libgit2_init();
        }

        ~Main()
        {
            git_libgit2_shutdown();
        }
    };
}

int main(int argc, char *argv[])
{
    // Setup exception handler to create minidump
    SetUnhandledExceptionFilter(minidump);

    // Initialize libgit2
    JadeGit::Console::Main main;

    // Run command(s)
	return JadeGit::Console::run(argc, argv);
}