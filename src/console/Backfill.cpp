#include "Command.h"
#include "CommandRegistration.h"
#include <git2/sys/repository.h>
#include <registry/Commit.h>
#include <registry/Repository.h>
#include <registry/Root.h>
#include <registry/Storage.h>
#include <extract/Assembly.h>
#include <vfs/FileSignature.h>
#include <vfs/GitFileSystem.h>
#include <Exception.h>
#include <Environment.h>

using namespace std;
using namespace JadeGit::Registry;

namespace JadeGit::Console
{
	class Backfill : public Command
	{
	public:
		static constexpr const char* description = "Backfill local changes";

		Backfill(CLI::App& cmd, Session& context) : Command(cmd, context)
		{
			cmd.add_option("--commit-message", this->commit_message, "Commit message")->required();
			cmd.add_option("--branch", this->branch, "Remote branch to push latest commit to (forced)")->required();
			cmd.add_option("--user-name", this->user_name, "User name");
			cmd.add_option("--user-email", this->user_email, "User email");
			cmd.add_option("--access-token", this->access_token, "Access token for authorization");
		}

		void execute() final
		{
			// Setup custom headers for access token if supplied
			custom_headers.clear();
			if (!access_token.empty())
			{
				// Define authorization header with base64 encoded <user>:<token>
				string usertoken = ":" + access_token;
				authorization_header = "Authorization: Basic " + static_cast<string>(Data::Binary(reinterpret_cast<const Byte*>(usertoken.c_str()), usertoken.size()));;
				custom_headers.push_back(const_cast<char*>(authorization_header.c_str()));
			}

			// Load registry from database
			DatabaseStorage storage;
			Root registry(storage);

			// Check registry has repositories to backfill against
			if (registry.repos.empty())
				throw runtime_error("No repositories registered");

			// Check registry doesn't have multiple repositories (implementating support for this depends on determining which repository new schemas need to be added to)
			if (registry.repos.size() > 1)
				throw unimplemented_feature("Backfilling changes for multiple repositories");

			// Dereference the repository to backfill
			RepositoryT& registry_repo = registry.repos.front();

			// Safeguard against unknown repository
			if (repo_unknown(registry_repo))
				throw logic_error("Cannot backfill changes for unknown repository");

			// Safeguard against jadegit repository
			if (registry_repo.name == "jadegit")
				throw runtime_error("Cannot backfill changes for jadegit repository");

			// Info
			print(format("Backfilling changes for {} repository ...", registry_repo.name));
			
			// Open repository
			auto repo = open(registry_repo);

			// Get repository index
			unique_ptr<git_index> index;
			git_throw(git_repository_index(git_ptr(index), repo.get()));

			// Setup file system
			GitFileSystem files(index.get(), FileSignature::suppressed());

			// Extract schemas (implicitly saves/stages changes)
			JadeGit::Extract::Assembly(files).extract(vector<string>(), [&](const string& message) { print(message); });

			// Write index to tree
			git_oid tree_id;
			git_throw(git_index_write_tree(&tree_id, index.get()));

			// Lookup parent commit
			unique_ptr<git_commit> parent;
			{
				unique_ptr<git_reference> head;
				git_throw(git_repository_head(git_ptr(head), repo.get()));
				git_throw(git_commit_lookup(git_ptr(parent), repo.get(), git_reference_target(head.get())));
			}

			// Create new commit if there's been any changes extracted
			if (!git_oid_equal(git_commit_tree_id(parent.get()), &tree_id))
			{
				// Lookup tree
				unique_ptr<git_tree> tree;
				git_throw(git_tree_lookup(git_ptr(tree), repo.get(), &tree_id));

				// Create signature
				unique_ptr<git_signature> author;
				if (!user_name.empty() && !user_email.empty())
					git_throw(git_signature_now(git_ptr(author), user_name.c_str(), user_email.c_str()));
				else
					git_throw(git_signature_default(git_ptr(author), repo.get()));

				// Create commit with one parent commit
				const git_commit* parents[] = { parent.get() };
				git_oid id;
				git_throw(git_commit_create(&id, repo.get(), "HEAD", author.get(), author.get(), nullptr, commit_message.c_str(), tree.get(), 1, parents));
				print(format("Done (new commit: {})", git_oid_tostr_s(&id)));

				// Update registry with latest commit, with previous updated to reflect initial commit
				if (registry_repo.previous.empty())
					registry_repo.previous = registry_repo.latest;

				registry_repo.latest.clear();
				registry_repo.latest.push_back(make_commit(id));

				// Save registry changes to database
				registry.save(storage);
			}
			else
			{
				print("Done (no changes)");
			}

			// Push current commit back to origin
			push(*repo);

			// Success
			print("Backfill Completed");
		}

	protected:
		string access_token;
		string branch;
		string commit_message;
		string user_name;
		string user_email;
		
		// Handles opening repository in temporary folder, cloning/updating as needed to match registry
		unique_ptr<git_repository> open(const RepositoryT& registry_repo)
		{
			// Safeguard against repository with no origin registered
			if (registry_repo.origin.empty())
				throw runtime_error("Cannot backfill changes for repository with no origin registered");
			
			// Safeguard against repository with no commits registered
			if (registry_repo.latest.empty())
				throw runtime_error("Cannot backfill changes for repository with no commits registered");
			
			// Check repository doesn't have multiple commits deployed (not supported, while a merge commit could be created, this would suggest jadegit is already being used for deployments)
			if (registry_repo.latest.size() > 1)
				throw unsupported_feature("Backfilling changes after deploying multiple commits");

			// Use temp folder
			auto path = makeTempDirectory("backfill") / registry_repo.name;

			// Open existing
			unique_ptr<git_repository> repo;
			if (auto result = git_repository_open_bare(git_ptr(repo), path.string().c_str()); result == GIT_ENOTFOUND)
			{
				// Info
				print(format("Cloning {}", registry_repo.origin));

				// Clone repository
				git_clone_options opts = GIT_CLONE_OPTIONS_INIT;
				opts.bare = 1;
				opts.fetch_opts.custom_headers = { custom_headers.data(), custom_headers.size() };
				opts.fetch_opts.proxy_opts.type = git_proxy_t::GIT_PROXY_AUTO;
				git_throw(git_clone(git_ptr(repo), registry_repo.origin.c_str(), path.string().c_str(), &opts));
			}
			else
				git_throw(result);

			// Convert commit oid from registry
			git_oid oid = { 0 };
			git_throw(git_oid_fromraw(&oid, registry_repo.latest.front().id.data()));

			// Lookup commit
			unique_ptr<git_commit> commit;
			if (auto result = git_commit_lookup(git_ptr(commit), repo.get(), &oid); result == GIT_ENOTFOUND)
			{
				// Info
				print(format("Fetching current commit ({})", git_oid_tostr_s(&oid)));

				// Lookup remote
				const char* remote_name = "origin";
				unique_ptr<git_remote> remote;
				git_throw(git_remote_lookup(git_ptr(remote), repo.get(), remote_name));

				// Update remote url
				if (git_remote_url(remote.get()) != registry_repo.origin)
				{
					git_throw(git_remote_set_url(repo.get(), remote_name, registry_repo.origin.c_str()));
					git_throw(git_remote_lookup(git_ptr(remote), repo.get(), remote_name));
				}

				// Setup fetch options
				git_fetch_options opts = GIT_FETCH_OPTIONS_INIT;
				opts.custom_headers = { custom_headers.data(), custom_headers.size() };
				opts.proxy_opts.type = git_proxy_t::GIT_PROXY_AUTO;
				opts.update_fetchhead = 0;

				// Define refspec to fetch specific commit
				char* refspec[] = { git_oid_tostr_s(&oid) };
				git_strarray refspecs = { refspec, 1 };

				// Fetch commit
				if (auto result = git_remote_fetch(remote.get(), &refspecs, &opts, nullptr); result != GIT_EEOF)
					git_throw(result);
				
				// Lookup commit
				git_throw(git_commit_lookup(git_ptr(commit), repo.get(), &oid));
			}
			else
				git_throw(result);

			// Lookup tree
			unique_ptr<git_tree> tree;
			git_throw(git_commit_tree(git_ptr(tree), commit.get()));

			// Setup index
			unique_ptr<git_index> index;
			git_throw(git_index_new(git_ptr(index)));

			// Load index
			git_throw(git_index_read_tree(index.get(), tree.get()));

			// Set repository index
			git_throw(git_repository_set_index(repo.get(), index.get()));

			// Set repository HEAD to commit
			git_throw(git_repository_set_head_detached(repo.get(), git_commit_id(commit.get())));

			// Success
			return repo;
		}
	
		// Push current commit back to origin
		void push(git_repository& repo)
		{
			// Info
			print(format("Pushing to origin/{}", branch));

			// Lookup remote
			unique_ptr<git_remote> remote;
			git_throw(git_remote_lookup(git_ptr(remote), &repo, "origin"));

			// Define refspec to push current commit
			string push_refspec = format("+HEAD:refs/heads/{}", branch);
			char* refspec[] = { const_cast<char*>(push_refspec.c_str()) };
			git_strarray refspecs = { refspec, 1 };

			// Push commit
			git_push_options opts = GIT_PUSH_OPTIONS_INIT;
			opts.custom_headers = { custom_headers.data(), custom_headers.size() };
			opts.proxy_opts.type = git_proxy_t::GIT_PROXY_AUTO;
			git_throw(git_remote_push(remote.get(), &refspecs, &opts));
		}

	private:
		vector<char*> custom_headers;
		string authorization_header;
	};
	static CommandRegistration<Backfill, true> registration("backfill");
}