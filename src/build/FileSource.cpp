#include <jadegit/build/FileSource.h>
#include <jadegit/vfs/RecursiveFileIterator.h>

namespace JadeGit::Build
{
	FileSource::FileSource(std::shared_ptr<FileSystem> previous, std::shared_ptr<FileSystem> latest) : previous(std::move(previous)), latest(std::move(latest)) {}
	FileSource::FileSource(std::shared_ptr<FileSystem> latest) : FileSource(std::shared_ptr<FileSystem>(), std::move(latest)) {}
	
	const FileSystem* FileSource::Previous() const
	{
		return previous.get();
	}
	
	const FileSystem* FileSource::Latest() const
	{
		return latest.get();
	}

	bool FileSource::read(Source::Reader reader, IProgress* progress) const
	{
		if (progress && !progress->start(0, "Reading"))
			return false;

		auto read_tail = [reader](RecursiveFileIterator& iter, bool latest)
		{
			while (iter != end(iter))
			{
				if(!iter->isDirectory())
					reader(*iter, latest);
				++iter;
			}
		};

		RecursiveFileIterator previous;
		if (this->previous)
			previous = RecursiveFileIterator(this->previous->open(""));

		RecursiveFileIterator latest;
		if (this->latest)
			latest = RecursiveFileIterator(this->latest->open(""));

		while (true)
		{
			if (previous == end(previous))
			{
				read_tail(latest, true);
				break;
			}
			else if (latest == end(latest))
			{
				read_tail(previous, false);
				break;
			}
			else
			{
				if (previous->path() < latest->path())
				{
					if (!previous->isDirectory())
						reader(*previous, false);
					++previous;
				}
				else if (latest->path() < previous->path())
				{
					if (!latest->isDirectory())
						reader(*latest, true);
					++latest;
				}
				else
				{
					// Read contents if different
					// TODO: could compare input streams to find first difference in buffered manner
					if (previous->isDirectory() || latest->isDirectory() || previous->read() != latest->read())
					{
						if (!previous->isDirectory())
							reader(*previous, false);

						if (!latest->isDirectory())
							reader(*latest, true);
					}

					++previous;
					++latest;
				}
			}
		}
		
		return !progress || progress->finish();
	}
}