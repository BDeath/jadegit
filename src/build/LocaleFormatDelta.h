#pragma once
#include "FeatureDelta.h"
#include <jadegit/data/LocaleFormat.h>

namespace JadeGit::Build
{
	template<class TComponent>
	class LocaleFormatDelta : public FeatureDelta<TComponent>
	{
	public:
		LocaleFormatDelta(Graph& graph) : FeatureDelta<TComponent>(graph, "LocaleFormat") {}

	};
}