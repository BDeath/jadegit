#pragma once
#include "Task.h"
#include "Delta.h"
#include <jadegit/data/Assembly.h>
#include <jadegit/data/Entity.h>
#include <jadegit/data/EntityVisitor.h>
#include <assert.h>

namespace JadeGit::Build
{
	class Graph : public Data::EntityVisitor
	{
	public:
		Graph() = default;
		~Graph();

		void Add(Data::Entity* entity, bool latest);
		bool analyze(IProgress* progress);
		bool traverse(TaskVisitor& visitor, IProgress* progress = nullptr, const char* task = nullptr) const;

		template<class TDelta = IDelta>
		TDelta* Find(const Data::Entity& entity) const
		{
			static_assert(std::is_base_of<IDelta, TDelta>());

			auto iter = deltasByEntity.find(&entity);
			return iter != deltasByEntity.end() ? dynamic_cast<TDelta*>(iter->second) : nullptr;
		}

		template<class TDelta = IDelta>
		TDelta* Find(const Data::Entity* entity) const
		{
			return entity ? Find<TDelta>(*entity) : nullptr;
		}

		template<class TDelta>
		TDelta* Find(const Data::BaseValue<Data::Object*>& value) const
		{
			auto object = static_cast<const Data::Object*>(value);
			return object ? Find<TDelta>(object->asEntity()) : nullptr;
		}

		template<class TDelta, class TEntity>
		TDelta* Analyze(const TEntity& entity)
		{
			auto delta = Find<TDelta>(entity);

			if (delta)
				delta->Analyze();

			return delta;
		}

	private:
		Graph(const Graph&) = delete;

		friend Task;
		friend TaskGroup;

		size_t size = 0;
		std::vector<Task*> children;
		std::vector<IDelta*> deltas;
		std::map<Data::EntityKey, IDelta*> deltasById;
		std::map<const Data::Entity*, IDelta*> deltasByEntity;
		std::map<const Data::Entity*, TaskGroup*> groups;

		bool latest = false;

		void DuplicateCheck(const Data::Entity* prior, const Data::Entity* current) const;

		template<class TDelta, class TEntity, typename... Args>
		void Deltify(TEntity* entity, Args... args)
		{
			assert(!entity->isStatic() && !entity->isShallow());

			auto key = entity->getKey();

			IDelta* d = deltasById[key];
			TDelta* delta = dynamic_cast<TDelta*>(d);
			assert(!d == !delta);
			if (!delta)
			{
				deltasById[key] = delta = new TDelta(*this, args...);
				deltas.push_back(delta);
			}

			if (latest)
			{
				DuplicateCheck(delta->latest, entity);
				delta->latest = entity;
			}
			else
			{
				DuplicateCheck(delta->previous, entity);
				delta->previous = entity;
			}

			deltasByEntity[entity] = delta;
		}

		// EntityVisitor (ordered by type alphabetically)
		void Visit(Data::ActiveXAttribute* attribute) final;
		void Visit(Data::ActiveXClass* cls) final;
		void Visit(Data::ActiveXConstant* constant) final;
		void Visit(Data::ActiveXLibrary* library) final;
		void Visit(Data::ActiveXMethod* method) final;
		void Visit(Data::Application* application) final;
		void Visit(Data::Class* cls) final;
		void Visit(Data::CollClass* cls) final;
		void Visit(Data::CompAttribute* attribute) final;
		void Visit(Data::Constant* constant) final;
		void Visit(Data::ConstantCategory* category) final;
		void Visit(Data::Control* control) final;
		void Visit(Data::CurrencyFormat* format) final;
		void Visit(Data::Database* database) final;
		void Visit(Data::DateFormat* format) final;
		void Visit(Data::DbFile* dbFile) final;
		void Visit(Data::ExplicitInverseRef* reference) final;
		void Visit(Data::ExternalDatabase* database) final;
		void Visit(Data::ExternalMethod* method) final;
		void Visit(Data::Form* form) final;
		void Visit(Data::Function* function) final;
		void Visit(Data::GlobalConstant* constant) final;
		void Visit(Data::ImplicitInverseRef* reference) final;
		void Visit(Data::JadeExportedClass* cls) final;
		void Visit(Data::JadeExportedConstant* constant) final;
		void Visit(Data::JadeExportedInterface* interface) final;
		void Visit(Data::JadeExportedMethod* method) final;
		void Visit(Data::JadeExportedPackage* package) final;
		void Visit(Data::JadeExportedProperty* property) final;
		void Visit(Data::JadeExposedClass* cls) final;
		void Visit(Data::JadeExposedFeature* feature) final;
		void Visit(Data::JadeExposedList* list) final;
		void Visit(Data::JadeHTMLDocument* document) final;
		void Visit(Data::JadeImportedClass* cls) final;
		void Visit(Data::JadeImportedConstant* constant) final;
		void Visit(Data::JadeImportedInterface* interface) final;
		void Visit(Data::JadeImportedMethod* method) final;
		void Visit(Data::JadeImportedPackage* package) final;
		void Visit(Data::JadeImportedProperty* property) final;
		void Visit(Data::JadeInterface* interface) final;
		void Visit(Data::JadeInterfaceMethod* method) final;
		void Visit(Data::JadeMethod* method) final;
		void Visit(Data::JadeWebServicesClass* cls) final;
		void Visit(Data::JadeWebServicesMethod* method) final;
		void Visit(Data::JadeWebServiceConsumerClass* cls) final;
		void Visit(Data::JadeWebServiceProviderClass* cls) final;
		void Visit(Data::JadeWebServiceSoapHeaderClass* cls) final;
		void Visit(Data::Library* library) final;
		void Visit(Data::Locale* locale) final;
		void Visit(Data::NumberFormat* format) final;
		void Visit(Data::PrimAttribute* attribute) final;
		void Visit(Data::PrimType* primitive) final;
		void Visit(Data::PseudoType* pseudoType) final;
		void Visit(Data::RelationalAttribute* attribute) final;
		void Visit(Data::RelationalTable* table) final;
		void Visit(Data::RelationalView* view) final;
		void Visit(Data::Schema* schema) final;
		void Visit(Data::TimeFormat* format) final;
		void Visit(Data::TranslatableString* string) final;
	};
}
