#include "Graph.h"
#include "PropertyDelta.h"
#include <jadegit/data/PropertyIterator.h>

using namespace std;
using namespace JadeGit::Data;

namespace JadeGit::Build
{
	void IDelta::Analyze()
	{
		/* Ignore repeated calls to analyze */
		if (analyzed)
			return;

		/* Guard against recursive calls to re-analyze self */
		if (analyzing)
			throw logic_error("Cyclic build analysis");

		/* Set flag indicating analysis is in progress */
		analyzing = true;

		/* Analyze */
		AnalyzeEnter();
		AnalyzeExit();

		/* Reset analysis in progress flag */
		analyzing = false;
	}

	void IDelta::AnalyzeExit()
	{
		/* Set flag indicating analysis has been completed */
		analyzed = true;
	}

	bool IDelta::compare(const Data::Object& previous, const Data::Object& latest, bool deep) const
	{
		// Compare data classes
		assert(previous.dataClass && latest.dataClass);
		if (!match(previous.dataClass, latest.dataClass))
			return false;

		// Compare property values
		PropertyIterator iter(latest.dataClass);
		for (auto& latest_property : iter)
		{
			// Resolve previous property
			const Property* previous_property = nullptr;
			if (auto delta = graph.Find<IPropertyDelta>(&latest_property))
			{
				previous_property = delta->getPrevious();
			}
			else
			{
				previous_property = previous.dataClass->tryGetProperty(latest_property.name);
			}

			// Ignore previous properties which don't exist (isn't a change requiring update as deleting property will take care of it)
			if (!previous_property)
				continue;

			auto& type = latest_property.getType();
			if (previous_property->isAttribute() || latest_property.isAttribute())
			{
				if (type.isCollClass())
				{
					// Compare primitive collection contents
					if (!compare(static_cast<const CollClass&>(type).getMemberType(), previous.GetValue<Collection*>(*previous_property), latest.GetValue<Collection*>(latest_property)))
						return false;
				}
				else
				{
					// Compare primitive value directly
					if (previous.GetValue(*previous_property) != latest.GetValue(latest_property))
						return false;
				}
			}
			else
			{
				assert(previous_property->isReference() && latest_property.isReference());

				// Ignore automatic references
				if (static_cast<const Reference&>(latest_property).isExplicitInverseRef() && static_cast<const ExplicitInverseRef&>(latest_property).isAutomatic())
					continue;

				if (type.isCollClass())
				{
					// Compare collections
					if (!compare(static_cast<const CollClass&>(type).getMemberType(), previous.GetValue<Collection*>(*previous_property), latest.GetValue<Collection*>(latest_property)))
						return false;
				}
				else
				{
					// Compare object references
					if (!compare(previous.GetValue<Object*>(*previous_property), latest.GetValue<Object*>(latest_property)))
						return false;
				}
			}
		}

		// If required, perform deep comparison for all children
		// NOTE: This approach is used to cover scenarios where parent/child reference/collection properties don't exist (i.e. ActiveXClass -> ActiveXFeature)
		if (deep)
		{
			// Quick check on size
			if (previous.children.size() != latest.children.size())
				return false;

			// Iterate & compare children
			auto previous_iter = previous.children.begin();
			auto latest_iter = latest.children.begin();

			while (previous_iter != end(previous.children) && latest_iter != end(latest.children))
			{
				if (!compare(**previous_iter, **latest_iter, deep))
					return false;

				previous_iter++;
				latest_iter++;
			}
		}

		// Objects are the same
		return true;
	}

	bool IDelta::compare(const Data::Object* previous, const Data::Object* latest) const
	{
		// Check both have been supplied
		if (!previous || !latest)
			return !previous && !latest;

		// Match entities
		if (const Entity* previous_entity = previous->asEntity())
		{
			const Entity* latest_entity = latest->asEntity();
			if (!latest_entity)
				return false;

			// Match reference
			return match(previous_entity, latest_entity);
		}
		else
		{
			// Compare object properties
			return compare(*previous, *latest);
		}
	}

	bool IDelta::compare(const Data::Type& memberType, const Data::Collection* previous, const Data::Collection* latest) const
	{
		// Check both have been supplied
		if (!previous || !latest)
			return !previous && !latest;

		// Check collection size matches
		if (previous->size() != latest->size())
			return false;

		// Determine if we're dealing with a primitive type
		bool primitive = memberType.isPrimType();

		// Iterate & compare collection contents
		auto previous_iter = previous->CreateIterator();
		auto latest_iter = latest->CreateIterator();

		Any previous_value;
		Any latest_value;
		while (previous_iter->Next(previous_value) && latest_iter->Next(latest_value))
		{
			// Compare primitive values directly
			if (primitive)
			{
				if (previous_value != latest_value)
					return false;
			}
			// Compare references
			else
			{
				if (!compare(previous_value.Get<Object*>(), latest_value.Get<Object*>()))
					return false;
			}
		}

		// Collections are the same
		return true;
	}

	bool IDelta::match(const Data::Entity* previous, const Data::Entity* latest) const
	{
		// Check both have been supplied
		if (!previous || !latest)
			return !previous && !latest;

		// Always compare static entities by key directly
		if (previous->isStatic())
			return latest->isStatic() ? previous->getKey() == latest->getKey() : false;

		// Match by delta
		auto previous_delta = graph.Find<IDelta>(previous);
		auto latest_delta = graph.Find<IDelta>(latest);
		if (previous_delta && latest_delta)
			return previous_delta == latest_delta;

		// Safeguard against implied entities (assumed to exist, but hasn't been loaded yet)
		if (previous->isImplied())
			throw logic_error("Cannot match previous implied entity [" + previous->GetQualifiedName() + "]");
		else if (latest->isImplied())
			throw logic_error("Cannot match latest implied entity [" + latest->GetQualifiedName() + "]");

		// Handle purely inferred entities (like dynamic clusters, which don't have a key), by matching name, type & parent
		if (previous->isInferred() && latest->isInferred())
			return previous->name == latest->name && match(previous->dataClass, latest->dataClass) && match(previous->getParentEntity(), latest->getParentEntity());

		// Match by key directly
		return previous->getKey() == latest->getKey();
	}
}