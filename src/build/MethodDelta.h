#pragma once
#include "FeatureDelta.h"
#include "ClassDelta.h"

namespace JadeGit::Build
{
	template<class TComponent = Method>
	class MethodDelta : public FeatureDelta<TComponent>
	{
	public:
		MethodDelta(Graph& graph) : FeatureDelta<TComponent>(graph, "Method") {}

		using FeatureDelta<TComponent>::previous;
		using FeatureDelta<TComponent>::latest;

	protected:
		using FeatureDelta<TComponent>::graph;
		using FeatureDelta<TComponent>::GetDefinition;
		using FeatureDelta<TComponent>::GetDeletion;

		bool AnalyzeEnter() override
		{
			// Basic analysis
			if (!FeatureDelta<TComponent>::AnalyzeEnter())
			{
				// Handle deletion before event method
				if (previous->controlMethod != previous)
					this->addDeletionDependency(previous->controlMethod, GetDeletion());

				// Handle deletion after interface methods are remapped
				if (auto typeDelta = graph.Analyze<ITypeDelta>(previous->schemaType); typeDelta && typeDelta->getLatest())
				{
					for (auto& interfaceMethodPrevious : previous->interfaceImplements)
					{
						auto interfaceMethodDelta = graph.Analyze<ISchemaComponentDelta>(interfaceMethodPrevious);

						for (auto& method : static_cast<const Data::Type*>(typeDelta->getLatest())->methods)
						{
							bool done = false;
							for (auto& interfaceMethodLatest : method.second->interfaceImplements)
							{
								if (interfaceMethodDelta ? graph.Find<ISchemaComponentDelta>(interfaceMethodLatest) == interfaceMethodDelta : interfaceMethodLatest->getKey() == interfaceMethodPrevious->getKey())
								{
									this->addDefinitionDependency(method.second, GetDeletion());

									done = true;
									break;
								}
							}

							if (done)
								break;
						}
					}
				}

				return false;
			}

			return true;
		}

		Task* handleDefinition(Task* parent) override
		{
			// Parent definition required (stub/group task or not), for dependencies added below
			assert(parent);

			auto definition = FeatureDelta<TComponent>::handleDefinition(parent);

			// Defining event method mappings depends on event methods being created/renamed
			if (latest->controlMethod != latest)
				this->addCreationDependency(latest->controlMethod, parent);

			// Defining mapping methods depends on inherited properties being created beforehand
			// This is needed to ensure property mapping count is maintained correctly during load, otherwise corruption has knock-on impacts
			// as method removal asserts mapping count must be greater than zero and methods referring to virtual properties may not compile
			if (latest->invocation == Method::Invocation::Mapping)
			{
				if (auto cls = dynamic_cast<Class*>(static_cast<Type*>(latest->schemaType)))
					if (auto prop = cls->tryGetProperty(latest->GetName()); prop && prop->schemaType != latest->schemaType)
						this->addCreationDependency(prop, parent);
			}

			return definition;
		}

		Task* handleRename() final
		{
			/*
				Mapping methods are implicitly renamed when properties are renamed.
				May also apply when super-methods are renamed (not supported by JADE yet).
			*/

			Class* previousClass = dynamic_cast<Class*>(static_cast<Type*>(previous->schemaType));
			Class* latestClass = dynamic_cast<Class*>(static_cast<Type*>(latest->schemaType));

			if (previousClass && latestClass)
			{
				// Resolve properties with the same name
				const Property* previousProperty = previousClass->tryGetProperty(previous->GetName());
				const Property* latestProperty = latestClass->tryGetProperty(latest->GetName());

				if (previousProperty && latestProperty)
				{
					// Analyze property changes
					const ISchemaComponentDelta* previousPropertyDelta = graph.Analyze<ISchemaComponentDelta>(previousProperty);
					const ISchemaComponentDelta* latestPropertyDelta = graph.Analyze<ISchemaComponentDelta>(latestProperty);

					if (previousPropertyDelta && previousPropertyDelta == latestPropertyDelta)
					{
						// Method will be implicitly renamed when property is renamed
						return latestPropertyDelta->GetRename();
					}
				}
			}

			// Default to renaming method explicitly
			return FeatureDelta<TComponent>::handleRename();
		}
	};
}