#include "SchemaDelta.h"
#include "BuildTask.h"
#include "CommandStrategy.h"
#include "Graph.h"

using namespace std;
using namespace JadeGit::Data;

namespace JadeGit::Build
{
	class SchemaBuildTask : public BuildTask
	{
	public:
		SchemaBuildTask(Graph& graph, Schema& schema) : BuildTask(graph), schema(schema) {}

	protected:
		Schema& schema;
	};

	class RegisterSchemaTask : public SchemaBuildTask
	{
	public:
		using SchemaBuildTask::SchemaBuildTask;

		operator string() const final
		{
			return format("Schema {} registration", schema.name.c_str());
		}

		bool execute(Builder& builder) const final
		{
			builder.RegisterSchema(schema.name.c_str());
			return true;
		}
	};

	class DeregisterSchemaTask : public SchemaBuildTask
	{
	public:
		using SchemaBuildTask::SchemaBuildTask;

		operator string() const final
		{
			return format("Schema {} deregistration", schema.name.c_str());
		}

		bool execute(Builder& builder) const final
		{
			builder.DeregisterSchema(schema.name.c_str());
			return true;
		}
	};

	class SchemaDeletionTask : public DeleteTask
	{
	public:
		using DeleteTask::DeleteTask;

		void execute(CommandStrategy& strategy) const final
		{
			DeleteTask::execute(strategy);

			// Delete schemas via separate command files as JADE commits their deletion immediately so can't be repeated during a retry
			strategy.Flush();
		}
	};

	class ModifySchemaTask : public CommandTask
	{
	public:
		string schemaName;
		string key;
		string value;

		ModifySchemaTask(Graph& graph, string schemaName, string key, string value) : CommandTask(graph, nullptr, LoadStyle::Current),
			schemaName(move(schemaName)),
			key(move(key)),
			value(move(value))
		{}

		operator string() const final
		{
			return format("Schema {} modification ({} = {})", schemaName, key, value);
		}

		void execute(CommandStrategy& strategy) const final
		{
			strategy.ModifySchema(schemaName, key, value);
		}
	};

	SchemaDelta::SchemaDelta(Graph& graph) : SchemaComponentDelta(graph, "Schema") {}

	Task* SchemaDelta::GetCreation() const
	{
		// Declaration task establishes new schema
		if (declaration)
			return declaration;

		return SchemaComponentDelta<Schema>::GetCreation();
	}

	bool SchemaDelta::AnalyzeEnter()
	{
		// Basic analysis
		if (!SchemaComponentDelta::AnalyzeEnter())
		{
			// Deregister schema
			new DeregisterSchemaTask(graph, *previous);

			// Handle deletion before deleting previous superschema
			this->addDeletionDependency(previous->superschema, GetDeletion());

			return false;
		}

		// Register schema on creation
		if (!previous)
			new RegisterSchemaTask(graph, *latest);

		// Definition dependent on superschema being created
		this->addCreationDependency(latest->superschema, GetDefinition());

		// TODO: Handle schema moves

		return true;
	}

	void SchemaDelta::AnalyzeExit()
	{
		SchemaComponentDelta::AnalyzeExit();

		// Handle schema modifications
		if (previous && latest)
		{
			// Handle switching default locale
			if (previous->primaryLocale && latest->primaryLocale && previous->primaryLocale->name != latest->primaryLocale->name)
			{
				auto modification = new ModifySchemaTask(graph, QualifiedName(), "DefaultLocale", latest->primaryLocale->name);
				modification->addPredecessor(GetRename());

				// Create new primary locale before switch
				this->addCreationDependency(latest->primaryLocale, modification);

				// Delete previous primary locale after switch
				this->addDeletionDependency(previous->primaryLocale, modification);

				// Define new primary locale after switch
				this->GetDefinition()->addPredecessor(modification, true);
			}
		}

		// Handle defining schema entities required for partial schema creation/declaration
		if (!previous)
		{
			// Define singleton classes
			this->addCreationPeer(latest->getApplicationClass(), declaration);
			this->addCreationPeer(latest->getGlobalClass(), declaration);
			this->addCreationPeer(latest->getSessionClass(), declaration);

			// Define default application
			this->addCreationPeer(latest->getDefaultApplication(), declaration);

			// Define locales
			for (auto& locale : latest->locales)
				if (locale.second->clones.empty())
					this->addCreationPeer(locale.second, declaration);
		}
	}

	Task* SchemaDelta::handleDefinition(Task* parent)
	{
		auto definition = SchemaComponentDelta<Schema>::handleDefinition(parent);

		// Use partial definition as schema declaration ahead of complete definition
		if (!previous)
		{
			declaration = new ExtractDefinitionTask<Schema>(graph, definition, *latest, false, true, false);
			definition->addCommonPredecessor(*declaration);
		}

		return definition;
	}

	Task* SchemaDelta::HandleDeletion(Task* parent)
	{
		return new SchemaDeletionTask(graph, parent, entityType, QualifiedName());
	}
}