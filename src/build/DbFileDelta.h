#pragma once
#include "SchemaComponentDelta.h"
#include <jadegit/data/Database.h>

namespace JadeGit::Build
{
	class DbFileDelta : public SchemaComponentDelta<DbFile>
	{
	public:
		DbFileDelta(Graph& graph) : SchemaComponentDelta(graph, "DbFile") {}

	protected:
		bool AnalyzeEnter() final
		{
			if (!SchemaComponentDelta::AnalyzeEnter())
				return false;

			// Define default map file before any others
			if (latest->database->defaultFile == latest)
			{
				if (auto delta = graph.Analyze<IDelta>(latest->database))
					delta->GetDefinition()->addCommonPredecessor(this->GetDefinition());
			}

			return true;
		}
	};
}