#pragma once
#include "PropertyDelta.h"
#include <jadegit/data/Reference.h>

namespace JadeGit::Build
{
	template<class TComponent>
	class ReferenceDelta : public PropertyDelta<TComponent>
	{
	public:
		using PropertyDelta<TComponent>::PropertyDelta;

		using PropertyDelta<TComponent>::previous;
		using PropertyDelta<TComponent>::latest;

	protected:
		using PropertyDelta<TComponent>::graph;

		bool AnalyzeEnter() override
		{
			// Basic analysis
			if (!PropertyDelta<TComponent>::AnalyzeEnter())
			{
				auto deletion = this->GetDeletion();
				
				// Handle deletion before deleting previous constraint
				this->addDeletionDependency(previous->constraint, deletion);

				// Handle deletion before moving previous type
				if (previous->type)
				{
					if (auto delta = graph.Analyze<IClassDelta>(previous->type->getRootType()))
						delta->getMove()->addPredecessor(deletion);
				}

				return false;
			}

			auto definition = this->GetDefinition();

			// Definition is dependent on latest constraint
			this->addDefinitionDependency(latest->constraint, definition);

			// Handle update before deleting previous constraint
			if (previous)
				this->addDeletionDependency(previous->constraint, definition);

			// Handle definition before/after type moves
			auto previousType = previous && previous->type ? graph.Analyze<IClassDelta>(previous->type->getRootType()) : nullptr;
			auto latestType = latest && latest->type ? graph.Analyze<IClassDelta>(latest->type->getRootType()) : nullptr;

			if (previousType && previousType != latestType)
			{
				if (auto previousTypeLatest = previousType->getLatest(); previousTypeLatest && previousTypeLatest->inheritsFrom(latest->type))
				{
					// Handle definition after previous type is moved under latest type
					definition->addPredecessor(previousType->getMove());
				}
				else
				{
					// Handle definition before previous type is moved
					previousType->getMove()->addPredecessor(definition);
				}
			}

			if (latestType && latestType != previousType)
			{
				// Handle definition after latest type is moved
				definition->addPredecessor(latestType->getMove());
			}

			return true;
		}
	};

	class ExplicitInverseRefDelta : public ReferenceDelta<Data::ExplicitInverseRef>
	{
	public:
		using ReferenceDelta<Data::ExplicitInverseRef>::ReferenceDelta;

	protected:
		bool compare(const Data::ExplicitInverseRef& previous, const Data::ExplicitInverseRef& latest, bool deep) const final
		{
			// Quick check if number of inverses have changed
			if (previous.inverses.size() != latest.inverses.size())
				return false;

			// Compare reference details
			if (!ReferenceDelta<Data::ExplicitInverseRef>::compare(previous, latest, deep))
				return false;

			// Retrieve previous/latest inverse references
			auto previous_inverses = previous.getInverseReferences();
			auto latest_inverses = latest.getInverseReferences();

			// Match each previous inverse, looking for any match in latest to ignore inconsistent order
			for (auto previous_inverse : previous_inverses)
			{
				bool matched = false;

				for (auto iter = latest_inverses.begin(); iter != latest_inverses.end(); iter++)
				{
					if (match(previous_inverse, *iter))
					{
						latest_inverses.erase(iter);
						matched = true;
						break;
					}
				}

				if (!matched)
					return false;
			}

			// Sanity check (all should've been matched/cleared above)
			if (!latest_inverses.empty())
				return false;

			// Everything appears the same
			return true;
		}
	};

	class ImplicitInverseRefDelta : public ReferenceDelta<Data::ImplicitInverseRef>
	{
	public:
		using ReferenceDelta::ReferenceDelta;

	protected:
		Task* HandleDeletion(Task* parent) final
		{
			// Control properties cannot be deleted explicitly, but can be implicitily deleted by complete parent definition
			if (previous->isFormControlProperty())
			{
				assert(parent && parent->complete);
				return parent;
			}

			return ReferenceDelta::HandleDeletion(parent);
		}
	};
}