#pragma once
#include "ExtractDefinitionTask.h"

namespace JadeGit::Build
{
	template<class TEntity>
	class ExtractDefinitionRenameTask : public ExtractDefinitionTask<TEntity>
	{
	public:
		ExtractDefinitionRenameTask(Graph& graph, Task* parent, const TEntity& entity, const char* oldName) : ExtractDefinitionTask<TEntity>(graph, parent, entity), oldName(oldName)
		{
		};

		operator std::string() const override
		{
			if (!oldName)
				return ExtractDefinitionTask<TEntity>::operator string();

			return std::format("{} {} definition (was {})", this->entity.dataClass->name.c_str(), this->entity.GetQualifiedName(), oldName);
		}

		bool execute(ExtractStrategy& strategy) const final
		{
			return strategy.Define(this->entity, oldName);
		}

	protected:
		const char* oldName = nullptr;
	};
}