#pragma once
#include "SchemaDelta.h"
#include "CommandStrategy.h"
#include <jadegit/data/Locale.h>

namespace JadeGit::Build
{
	class CreateLocaleTask : public CommandTask
	{
	public:
		std::string qualifiedName;
		std::string baseLocale;

		CreateLocaleTask(Graph& graph, std::string qualifiedName, std::string baseLocale = std::string()) : CommandTask(graph), 
			qualifiedName(std::move(qualifiedName)),
			baseLocale(std::move(baseLocale))
		{}

		operator std::string() const final
		{
			return baseLocale.empty() ? format("Locale {} creation", qualifiedName) : format("Locale {} creation (clone of {})", qualifiedName, baseLocale);
		}

		void execute(CommandStrategy& strategy) const final
		{
			strategy.CreateLocale(qualifiedName.c_str(), baseLocale.empty() ? nullptr : baseLocale.c_str());
		}
	};

	class LocaleDelta : public SchemaComponentDelta<Locale>
	{
	public:
		LocaleDelta(Graph& graph) : SchemaComponentDelta(graph, "Locale") {}
		
	protected:
		Task* GetCreation() const override
		{
			return creation ? creation : SchemaComponentDelta::GetCreation();
		}

		Task* GetDeletion() const override
		{
			return deletion ? deletion : SchemaComponentDelta::GetDeletion();
		}

		bool AnalyzeEnter() override
		{
			// Analyze previous base
			const LocaleDelta* previousBaseLocale = nullptr;
			if (previous)
				previousBaseLocale = graph.Analyze<LocaleDelta>(previous->cloneOf);
			
			// Basic analysis
			if (!SchemaComponentDelta::AnalyzeEnter())
			{
				// Handle deletion before deleting previous base
				this->addDeletionDependency(previous->cloneOf, GetDeletion());
				
				return false;
			}

			// Analyze latest base
			const LocaleDelta* latestBaseLocale = nullptr;
			latestBaseLocale = graph.Analyze<LocaleDelta>(latest->cloneOf);

			// Determine if schema is being loaded for the first time
			auto schema = graph.Find<SchemaDelta>(latest->schema);
			if (!schema || schema->previous)
			{
				// Determine if locale needs to be created
				if (!previous || previous->baseLocaleName() != latest->baseLocaleName())
				{
					// Handle creation
					creation = new CreateLocaleTask(graph, QualifiedName(), latest->baseLocaleName());

					// Definition depends on creation
					GetDefinition()->addPredecessor(creation, true);

					// Delete previous
					if (previous)
					{
						// Handle deletion
						deletion = HandleDeletion(nullptr);

						// Handle deletion before recreating
						creation->addPredecessor(deletion);

						// Handle deletion before deleting previous base
						this->addDeletionDependency(previous->cloneOf, deletion);
					}
				}
			}

			// Definition depends on base being created
			this->addCreationDependency(latest->cloneOf, GetCreation());

			return true;
		}

		Task* handleDefinition(Task* parent)
		{
			auto definition = SchemaComponentDelta::handleDefinition(parent);

			// Use partial definition as creation ahead of complete definition
			if (!previous)
			{
				creation = new ExtractDefinitionTask<Locale>(graph, definition, *latest, false, true, false);
				definition->addCommonPredecessor(*creation);
			}

			return definition;
		}

	private:
		Task* creation = nullptr;
		Task* deletion = nullptr;
	};
}