#pragma once
#include "ClassDelta.h"
#include "JadeInterfaceDelta.h"
#include "EmptyTask.h"
#include <jadegit/data/JadeImportedPackage.h>

using namespace JadeGit::Data;

namespace JadeGit::Build
{
	class JadeImportedPackageDelta : public SchemaComponentDelta<JadeImportedPackage>
	{
	public:
		JadeImportedPackageDelta(Graph& graph) : SchemaComponentDelta(graph, "Package") {}

	protected:
		bool AnalyzeEnter() override
		{
			if (!SchemaComponentDelta::AnalyzeEnter())
			{
				// Exported package cannot be deleted until imported package is
				this->addDeletionDependency(previous->exportedPackage, GetDeletion(), previous->GetRootSchema().version < par66557);

				return false;
			}

			// Defining imported package depends on exported package
			this->addCreationDependency(latest->exportedPackage, GetDefinition());
			 
			return true;
		}
	};

	template<class TEntity, template<class> class TBase> 
	class JadeImportedEntityDelta : public TBase<TEntity>
	{
	public:
		using TBase<TEntity>::TBase;

		using TBase<TEntity>::previous;
		using TBase<TEntity>::latest;

	protected:
		using TBase<TEntity>::graph;
		using TBase<TEntity>::GetCreation;
		using TBase<TEntity>::GetDeletion;

		bool AnalyzeEnter() override
		{
			if (!TBase<TEntity>::AnalyzeEnter())
			{
				// Exported entity cannot be deleted until imported entity is
				this->addDeletionDependency(previous->GetExportedEntity(), GetDeletion(), previous->GetRootSchema().version < par66557);

				return false;
			}

			// Dependent on exported entity
			this->addCreationDependency(latest->GetExportedEntity(), GetCreation());

			return true;
		}

		Task* HandleDeletion(Task* parent) override
		{
			// Use stub deletion task for imported entities as they're not explicitly deleted
			return new DeleteTask(graph, parent, "imported entity");
		}
	};

	template<class TEntity, template<class> class TBase>
	class JadeImportedTypeDelta : public JadeImportedEntityDelta<TEntity, TBase>
	{
	public:
		using JadeImportedTypeDelta<TEntity, TBase>::JadeImportedTypeDelta;

	protected:
		Task* handleDefinition(Task* parent) final
		{
			// For incomplete parent definitions, use empty task for imported types as they're not explicitly defined,
			// unless complete definition is required for changes made (removing local features in particular)
			if (this->latest->isInferred() && (!parent || !parent->complete) && (!this->previous || !this->requiresCompleteDefinition(parent)))
				return new EmptyTask(this->graph, parent);

			return JadeImportedEntityDelta<TEntity, TBase>::handleDefinition(parent);
		}
	};

	using JadeImportedClassDelta = JadeImportedTypeDelta<JadeImportedClass, ClassDelta>;
	using JadeImportedInterfaceDelta = JadeImportedTypeDelta<JadeImportedInterface, JadeInterfaceDelta>;

	template<class TFeature>
	class JadeImportedFeatureDelta : public JadeImportedEntityDelta<TFeature, Delta>
	{
	public:
		using JadeImportedEntityDelta<TFeature, Delta>::JadeImportedEntityDelta;

	protected:
		Task* handleDefinition(Task* parent) final
		{
			// Always use empty task for imported features as they're not explicitly defined
			return new EmptyTask(this->graph, parent);
		}
	};
}