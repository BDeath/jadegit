#pragma once
#include "CommandTask.h"

namespace JadeGit::Build
{
	class RenameTask : public CommandTask
	{
	public:
		RenameTask(Graph& graph, const char* entityType, std::string qualifiedName, std::string newName);

		operator std::string() const final;

	protected:
		const char* entityType;
		std::string qualifiedName;
		std::string newName;

		void execute(CommandStrategy &strategy) const final;
	};
}