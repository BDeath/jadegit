#pragma once

namespace JadeGit::Build
{
	class BuildTask;
	class CommandTask;
	class ExtractTask;
	class ReorgTask;

	class TaskVisitor
	{
	public:
		virtual bool visit(const BuildTask& task) = 0;
		virtual bool visit(const CommandTask& task) = 0;
		virtual bool visit(const ExtractTask& task) = 0;
		virtual bool visit(const ReorgTask& task) = 0;
		virtual void flush(bool final) = 0;
	};
}