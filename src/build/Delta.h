#pragma once
#include <jadegit/data/Object.h>
#include "ExtractDefinitionTask.h"
#include "TaskGroup.h"
#include <Platform.h>
#include <stack>

namespace JadeGit::Build
{
	class Graph;
	class Task;

	class IDelta
	{
	public:
		IDelta(Graph& graph) : graph(graph) {}
		virtual ~IDelta() = default;

		void Analyze();

		virtual Task* GetCreation() const = 0;
		virtual Task* GetDefinition() const = 0;
		virtual Task* GetDeletion() const = 0;
		virtual const Data::Entity* getPrevious() const = 0;
		virtual const Data::Entity* getLatest() const = 0;

		virtual int Level() const = 0;

	protected:
		bool analyzed = false;
		Graph& graph;

		virtual bool AnalyzeEnter() = 0;	/* Returns false for deletion */
		virtual void AnalyzeExit();

		bool compare(const Data::Object& previous, const Data::Object& latest, bool deep = false) const;
		bool match(const Data::Entity* previous, const Data::Entity* latest) const;

	private:
		bool analyzing = false;

		bool compare(const Data::Object* previous, const Data::Object* latest) const;
		bool compare(const Data::Type& memberType, const Data::Collection* previous, const Data::Collection* latest) const;
	};

	template<class TEntity, class TInterface = IDelta>
	class Delta : virtual public MemoryAllocated, public TInterface
	{
	public:
		using TInterface::TInterface;
		using TInterface::graph;

		TEntity* previous = nullptr;
		TEntity* latest = nullptr;

		Task* GetCreation() const override
		{
			return previous ? nullptr : GetDefinition();
		}

		Task* GetDefinition() const final
		{
			return definition;
		}

		Task* GetDeletion() const override
		{
			return deletion;
		}

		int Level() const final
		{
			return (previous ? previous->getLevel() : latest->getLevel());
		}

	protected:
		template <typename TEntity>
		void addCreationDependency(const TEntity& entity, Task* successor)
		{
			if (!successor)
				return;

			if (auto delta = graph.Analyze<IDelta>(entity))
				successor->addPredecessor(delta->GetCreation(), true);
		}

		template <typename TEntity>
		void addCreationPeer(const TEntity& entity, Task* peer)
		{
			if (!peer)
				return;

			if (auto delta = graph.Analyze<IDelta>(entity))
				peer->addPeer(delta->GetCreation());
		}

		template <typename TEntity>
		void addDefinitionDependency(const TEntity& entity, Task* successor)
		{
			if (!successor)
				return;

			if (auto delta = graph.Analyze<IDelta>(entity))
				successor->addPredecessor(delta->GetDefinition());
		}
		
		template <typename TEntity>
		void addDeletionDependency(const TEntity& entity, Task* predecessor, bool needsReorg)
		{
			if (!predecessor)
				return;

			if (auto delta = graph.Analyze<IDelta>(entity))
				delta->GetDeletion()->addPredecessor(predecessor, false, needsReorg);
		}

		template <typename TEntity>
		void addDeletionDependency(const TEntity& entity, Task* predecessor)
		{
			if (!predecessor)
				return;

			if constexpr (std::is_base_of_v<Data::SchemaEntity, TEntity>)
			{
				addDeletionDependency(entity, predecessor, entity.GetRootSchema().version < par66557 && static_cast<const Data::SchemaEntity&>(entity).isImported());
			}
			else
			{
				addDeletionDependency(entity, predecessor, false);
			}
		}

		bool AnalyzeEnter() override
		{
			// Handle deletion
			if (!latest)
			{
				Task* parent_deletion = nullptr;
				Task* parent_definition = nullptr;

				// Analyze previous parent
				auto parent = previous->getParentEntity();
				while (parent)
				{
					if (const IDelta* delta = graph.Analyze<IDelta>(parent))
					{
						parent_deletion = delta->GetDeletion();
						if ((parent_definition = delta->GetDefinition()) && !parent_definition->complete)
							parent_definition = nullptr;

						if (parent_deletion || parent_definition)
							break;
					}

					// Check grandparent
					parent = parent->getParentEntity();
				}

				// Setup deletion task, supplying complete parent deletion/definition task
				deletion = HandleDeletion(parent_deletion ? parent_deletion : parent_definition);
				return false;
			}

			Task* parent_definition = nullptr;
			
			// Analyze latest parent
			auto parent = latest->getParentEntity();
			std::stack<Data::Entity*> parents;
			while (parent)
			{
				// Get definition task for parent (if it's been changed)
				if (const IDelta* delta = graph.Analyze<IDelta>(parent))
				{
					if (parent_definition = delta->GetDefinition())
						break;
				}

				// Add parent to stack for creating stub/group task
				parents.push(parent);

				// Check grandparent
				parent = parent->getParentEntity();
			}

			// Create stub/group task for each intermediary parent without delta or definition task
			while (!parents.empty())
			{
				parent_definition = TaskGroup::make(graph, parent_definition, parents.top());
				parents.pop();
			}

			// Setup definition task, supplying parent definition task
			definition = handleDefinition(parent_definition);

			return true;
		}

		virtual bool compare(const TEntity& previous, const TEntity& latest, bool deep) const
		{
			return IDelta::compare(previous, latest, deep);
		}

		virtual Task* handleDefinition(Task* parent)
		{
			assert(latest);
			if constexpr (Definable<TEntity>)
			{
				// Determine if complete definition is required due to changes made
				bool complete = requiresCompleteDefinition(parent);

				// Determine if there's changes for which definition needs to reflect last modified
				bool modified = complete || (!previous && !latest->isCopy()) || (previous && !compare(*previous, *latest, !PartiallyDefinable<TEntity>));

				// Determine if complete definition is required as part of complete parent definition
				if (!complete)
					complete = parent && parent->complete;

				// Setup definition task when either a complete definition is needed or there's been a change
				if (complete || modified)
				{
					if constexpr (PartiallyDefinable<TEntity>)
						return new ExtractDefinitionTask<TEntity>(graph, parent, *latest, complete, modified, previous && !previous->text.empty() && latest->text.empty());
					else
						return new ExtractDefinitionTask<TEntity>(graph, parent, *latest);
				}
			}

			return nullptr;
		}

		virtual Task* HandleDeletion(Task* parent)
		{
			// TODO: Make pure virtual, need to implement for exposed & relational entities
			return nullptr;
		}

		// Determines if whether previous/latest versions of an entity appear the same
		template<class TEntity, class TDelta = IDelta>
		bool match(const TDelta* previousDelta, const TDelta* latestDelta, const TEntity* previous, const TEntity* latest) const
		{
			if (!previous || !latest)
				return !previous && !latest;

			return previousDelta ? previousDelta == latestDelta : previous->GetName() == latest->GetName();
		}

		template<class TEntity, class TDelta = IDelta>
		bool match(const TDelta* latestDelta, const TEntity* previous, const TEntity* latest) const
		{
			return match(graph.Find<TDelta>(previous), latestDelta, previous, latest);
		}

		template<class TEntity, class TDelta = IDelta>
		bool match(const TEntity* previous, const TEntity* latest) const
		{
			return match(graph.Find<TDelta>(latest), previous, latest);
		}

		virtual bool requiresCompleteDefinition(const Task* parent) const
		{
			return !previous;
		}

	private:
		Task* definition = nullptr;
		Task* deletion = nullptr;

		const TEntity* getPrevious() const final
		{
			return previous;
		}

		const TEntity* getLatest() const final
		{
			return latest;
		}
	};
}