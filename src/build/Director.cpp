#include <jadegit/build/Director.h>
#include <jadegit/build/Source.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/vfs/RecursiveFileIterator.h>
#include <build/Builder.h>
#include <data/config/DeployConfig.h>
#include <data/storage/ObjectFileStorage.h>
#include <data/storage/ObjectLoader.h>
#include "Graph.h"
#include "classic/ClassicFileBuilder.h"

using namespace std;
using namespace JadeGit::Data;

namespace JadeGit::Build
{
	class Director::Impl
	{
	public:
		// Setup director to build from source
		Impl(const Source& source, IProgress* progress) : source(source), progress(progress) {}

		// Build deployment
		bool build(Builder& builder)
		{
			if (progress && !progress->start(4))
				return false;

			if (!load(builder.platformVersion()))
				return false;

			// Analyze differences
			if (!graph.analyze(progress))
				return false;

			// TODO: Use factory for file builder?
			unique_ptr<FileBuilder> fileBuilder = make_unique<Classic::ClassicFileBuilder>(builder, (latest ? latest : previous)->GetRootSchema().version);

			// Traverse graph and build
			if (!graph.traverse(*fileBuilder, progress, "Building"))
				return false;

			// Add deployment scripts
			buildScripts(builder);

			return !progress || progress->finish();
		}

	private:
		const Source& source;
		IProgress* progress = nullptr;
		Graph graph;
		unique_ptr<Assembly> previous;
		unique_ptr<Assembly> latest;
		unique_ptr<DeployConfig> config;

		void buildScripts(Builder& builder)
		{
			if (!this->config)
				return;

			for (auto& config : this->config->scripts)
			{
				Build::Script script;

				if (config.schema)
					script.schema = config.schema->name;

				if (config.app)
					script.app = config.app->name;

				if (config.method)
				{
					if (config.method->schemaType->schema != config.schema)
						script.executeSchema = config.method->schemaType->schema->name;

					script.executeClass = config.method->schemaType->name;
					script.executeMethod = config.method->name;
					script.executeTypeMethod = config.method->isTypeMethod();
				}

				script.executeParam = config.param;
				script.executeTransient = config.transient;

				builder.addScript(script);
			}
		}

		bool load(const Version& platform)
		{
			// Initialise assemblies
			if (auto fs = source.Previous())
				previous = make_unique<Assembly>(*fs, platform);

			if (auto fs = source.Latest())
			{
				latest = make_unique<Assembly>(*fs, platform);
				config = make_unique<DeployConfig>(*latest);
			}

			// Read source, queuing entities to be loaded & added to graph for analysis
			if (!source.read([&](const File& file, bool side) { (side ? latest : previous)->getStorage().getLoader().queue(file, (side || !latest), bind(&Graph::Add, &graph, placeholders::_1, side)); }, progress))
				return false;

			// Load entities
			if (progress && !progress->start((previous ? previous->getStorage().getLoader().pending() : 0) + (latest ? latest->getStorage().getLoader().pending() : 0), "Loading"))
				return false;

			// Load config upfront (may resolve entities)
			if (config)
				config->load();

			if (previous && !previous->getStorage().getLoader().load(progress))
				return false;

			if (latest && !latest->getStorage().getLoader().load(progress))
				return false;

			return !progress || progress->finish();
		}
	};

	Director::Director(const Source& source, IProgress* progress)
	{
		impl = make_unique<Impl>(source, progress);
	}

	Director::~Director() = default;

	bool Director::Build(Builder& builder)
	{
		return impl->build(builder);
	}
}