#pragma once
#include <jadegit/Progress.h>

namespace JadeGit::Build
{
	class Task;
	class TaskVisitor;

	class TaskState
	{
	public:
		static const TaskState* initialState();

		virtual void reset(const Task& task, int depth = 0) const;
		virtual bool traversable(const Task& task) const;
		virtual bool traverse(const Task& task, size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth = 0, const Task* cascade = nullptr) const;

		bool isProcessing() const;

	protected:
		TaskState() = default;
		virtual ~TaskState() = default;

		virtual bool executeEnter(const Task& task, const Task* cascade) const;
		virtual bool executeExit(const Task& task, bool result, size_t& visited, bool& aborted, IProgress* progress) const;

		const TaskState* get(const Task& task) const;
		void set(const Task& task, const TaskState* state) const;
	};
}