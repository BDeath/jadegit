#pragma once
#include "SchemaComponentDelta.h"
#include <jadegit/data/Schema.h>

namespace JadeGit::Build
{
	class SchemaDelta : public SchemaComponentDelta<Data::Schema>
	{
	public:
		SchemaDelta(Graph& graph);

	protected:
		bool AnalyzeEnter() final;
		void AnalyzeExit() final;

		Task* GetCreation() const final;

		Task* handleDefinition(Task* parent) final;
		Task* HandleDeletion(Task* parent) final;

	private:
		Task* declaration = nullptr;
	};
}