#pragma once
#include "ConstantDelta.h"
#include <jadegit/data/Locale.h>

namespace JadeGit::Build
{
	class TranslatableStringDelta : public ConstantDelta<TranslatableString>
	{
	public:
		TranslatableStringDelta(Graph& graph) : ConstantDelta(graph, "TranslatableString") {}

	protected:
		Task* HandleDeletion(Task* parent) override
		{
			// Deletion may be implied by locale being deleted
			if (auto delta = graph.Analyze<IDelta>(previous->locale))
				if (auto deletion = delta->GetDeletion())
					return deletion;

			// Deletion may be implied by primary version being deleted
			if (!previous->locale->isPrimary())
			{
				if (const Locale* primary = previous->locale->schema->primaryLocale)
					if (auto delta = graph.Analyze<IDelta>(primary->translatableStrings.Get(previous->name)))
						if (auto deletion = delta->GetDeletion())
							return deletion;
			}

			// Deletion needs to be handled explicitly
			return ConstantDelta<TranslatableString>::HandleDeletion(parent);
		}
	};
}