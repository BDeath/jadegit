#pragma once
#include "ExtractTask.h"

namespace JadeGit::Data
{
	class JadeInterfaceMapping;
}

namespace JadeGit::Build
{
	template <Definable TEntity>
	class ExtractDefinitionTask : public ExtractTask
	{
	public:
		ExtractDefinitionTask(Graph& graph, Task* parent, const TEntity& entity) : ExtractTask(graph, parent, true, 1), entity(entity)
		{
		};

		operator std::string() const override
		{
			if constexpr (std::is_same_v<TEntity, Data::JadeInterfaceMapping>)
				return std::format("Class [{}] interface [{}] mapping definition", entity.type.name.c_str(), entity.interface.name.c_str());
			else
				return std::format("{} [{}] definition", entity.dataClass->name.c_str(), entity.GetQualifiedName().c_str());
		}

		bool execute(ExtractStrategy& strategy) const override
		{
			return strategy.Define(entity);
		}

	protected:
		const TEntity& entity;
	};

	template <PartiallyDefinable TEntity>
	class ExtractDefinitionTask<TEntity> : public ExtractTask
	{
	public:
		ExtractDefinitionTask(Graph& graph, Task* parent, const TEntity& entity, bool complete, bool modified, bool had_text) : ExtractTask(graph, parent, complete, 1), entity(entity), modified(modified), had_text(had_text)
		{
		};

		operator std::string() const final
		{
			return format("{} {}{} definition", entity.dataClass->name.c_str(), entity.GetQualifiedName(), complete ? "" : " partial");
		}

		bool execute(ExtractStrategy& strategy) const final
		{
			return strategy.Define(entity, complete, modified, had_text);
		}

	protected:
		const TEntity& entity;
		const bool modified;
		const bool had_text;
	};
}