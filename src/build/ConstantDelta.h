#pragma once
#include "FeatureDelta.h"

namespace JadeGit::Build
{
	template<class TComponent = Constant>
	class ConstantDelta : public FeatureDelta<TComponent>
	{
	public:
		using FeatureDelta<TComponent>::FeatureDelta;
		ConstantDelta(Graph& graph) : ConstantDelta(graph, "Constant") {}

		using FeatureDelta<TComponent>::previous;
		using FeatureDelta<TComponent>::latest;

	protected:
		using FeatureDelta<TComponent>::graph;
		using FeatureDelta<TComponent>::GetDefinition;
		using FeatureDelta<TComponent>::GetDeletion;

		bool AnalyzeEnter() override
		{
			// Basic analysis
			if (!FeatureDelta<TComponent>::AnalyzeEnter())
			{
				// Handle deleting or updating dependent constants before deletion
				if (auto deletion = GetDeletion())
					for (Constant* constant : previous->constantUsages)
						this->addDeletionDependency(constant, deletion);

				return false;
			}

			if (auto definition = GetDefinition())
			{
				// Constant definition depends on constants used being defined first
				for (Constant* constant : latest->constantUsages)
					this->addDefinitionDependency(constant, definition);

				// Deleting constants used previously depends on constant being updated first
				if (previous)
					for (Constant* constant : previous->constantUsages)
						this->addDeletionDependency(constant, definition);
			}

			return true;
		}
	};
}