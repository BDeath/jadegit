#pragma once
#include "Task.h"

namespace JadeGit::Data
{
	class Entity;
}

namespace JadeGit::Build
{
	class TaskGroup : public Task
	{
	public:
		static TaskGroup* make(Graph& graph, Task* parent, const Data::Entity* entity);
		
		operator std::string() const final;

	private:
		TaskGroup(Graph& graph, Task* parent, const Data::Entity& entity);

		const Data::Entity& entity;

		bool accept(TaskVisitor& v) const final { return true; };
	};
}