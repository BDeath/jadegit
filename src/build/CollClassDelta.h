#pragma once
#include "ClassDelta.h"
#include <jadegit/data/CollClass.h>

namespace JadeGit::Build
{
	template<class TComponent = CollClass>
	class CollClassDelta : public ClassDelta<TComponent>
	{
	public:
		CollClassDelta(Graph& graph) : ClassDelta<TComponent>(graph) {}

		using ClassDelta<TComponent>::previous;
		using ClassDelta<TComponent>::latest;

	protected:
		using ClassDelta<TComponent>::graph;
		using ClassDelta<TComponent>::GetDeclaration;
		using ClassDelta<TComponent>::GetDefinition;
		using ClassDelta<TComponent>::GetDeletion;

		bool AnalyzeEnter() override
		{
			// Basic analysis
			if (!ClassDelta<TComponent>::AnalyzeEnter())
			{
				// Handle deletion before member type deletion
				if (previous->memberType)
					this->addDeletionDependency(previous->memberType->getRootType(), GetDeletion());

				// Handle deletion before key properties/types
				for (auto& key : previous->keys)
				{
					if (auto memberKey = dynamic_cast<MemberKey*>(key))
					{
						this->addDeletionDependency(memberKey->property, GetDeletion());
					}
					else if (auto externalKey = dynamic_cast<ExternalKey*>(key))
					{
						this->addDeletionDependency(externalKey->type, GetDeletion());
					}
				}

				return false;
			}

			// Declaration depends on latest member type
			if (latest->memberType)
				this->addCreationDependency(latest->memberType->getRootType(), GetDeclaration());

			// Deleting prior member type depends on update to new member type
			if (previous && previous->memberType)
				this->addDeletionDependency(previous->memberType->getRootType(), GetDeclaration());

			// Definition is dependent on key properties/types
			for (auto& key : latest->keys)
			{
				if (auto memberKey = dynamic_cast<MemberKey*>(key))
				{
					this->addDefinitionDependency(memberKey->property, GetDefinition());
				}
				else if (auto externalKey = dynamic_cast<ExternalKey*>(key))
				{
					this->addCreationDependency(externalKey->type, GetDefinition());
				}
			}

			// Deleting prior key properties/types depends on definition update
			if (previous)
			{
				for (auto& key : previous->keys)
				{
					if (auto memberKey = dynamic_cast<MemberKey*>(key))
					{
						this->addDeletionDependency(memberKey->property, GetDefinition());
					}
					else if (auto externalKey = dynamic_cast<ExternalKey*>(key))
					{
						this->addDeletionDependency(externalKey->type, GetDefinition());
					}
				}
			}

			// Defining keys depends on superclass key definition
			if (latest->superclass)
				this->addDefinitionDependency(latest->superclass->getRootType(), GetDefinition());

			return true;
		}

		// Determine if complete definition is required
		bool requiresCompleteDefinition(const Task* parent) const final
		{
			return ClassDelta<TComponent>::requiresCompleteDefinition(parent) || isChangingKeys();
		}

		// Determine if forward declaration is required
		bool requiresDeclaration(const Task* parent) const final
		{
			return ClassDelta<TComponent>::requiresDeclaration(parent) || isChangingMemberType();
		}

	private:
		bool isChangingKey(const MemberKey* previous, const MemberKey* latest) const
		{
			if (!previous || !latest)
				return true;

			if (!this->match<Data::Property>(previous->property, latest->property))
				return true;

			if (previous->keyPath.size() != latest->keyPath.size())
				return true;

			auto previous_iter = previous->keyPath.begin();
			auto latest_iter = latest->keyPath.begin();
			while (previous_iter != previous->keyPath.end() && latest_iter != latest->keyPath.end())
			{
				if (!this->match<Data::Property>(*previous_iter, *latest_iter))
					return true;

				previous_iter++;
				latest_iter++;
			}

			return false;
		}

		bool isChangingKey(const ExternalKey* previous, const ExternalKey* latest) const
		{
			if (!previous || !latest)
				return true;

			if (previous->length != latest->length ||
				previous->precision != latest->precision ||
				previous->scaleFactor != latest->scaleFactor)
				return true;

			return !this->match<Data::Type>(previous->type, latest->type);
		}

		bool isChangingKey(const Key* previous, const Key* latest) const
		{
			if (!previous || !latest)
				return true;

			if (previous->caseInsensitive != latest->caseInsensitive ||
				previous->descending != latest->descending ||
				previous->sortOrder != latest->sortOrder)
				return true;

			if (auto member_key = dynamic_cast<const MemberKey*>(previous))
				return isChangingKey(member_key, dynamic_cast<const MemberKey*>(latest));
			else if (auto external_key = dynamic_cast<const ExternalKey*>(previous))
				return isChangingKey(external_key, dynamic_cast<const ExternalKey*>(latest));

			throw std::logic_error("Unhandled key type");
		}

		bool isChangingKeys() const
		{
			if (!previous || !latest)
				return false;

			if (previous->keys.size() != latest->keys.size())
				return true;

			auto previous_iter = previous->keys.begin();
			auto latest_iter = latest->keys.begin();
			while (previous_iter != previous->keys.end() && latest_iter != latest->keys.end())
			{
				if (isChangingKey(*previous_iter, *latest_iter))
					return true;

				previous_iter++;
				latest_iter++;
			}

			return false;
		}

		bool isChangingMemberType() const
		{
			return previous && latest && !this->match<Data::Type>(previous->memberType, latest->memberType);
		}
	};
}