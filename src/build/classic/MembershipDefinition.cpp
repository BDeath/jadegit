#include "MembershipDefinition.h"
#include "SchemaDefinition.h"
#include <jadegit/data/ExternalCollClass.h>

namespace JadeGit::Build::Classic
{
	static NodeRegistration<MembershipDefinition, Data::CollClass> collClass;
	static NodeRegistration<MembershipDefinition, Data::ExternalCollClass> externalCollClass;

	MembershipDefinition::MembershipDefinition(SchemaDefinition& schema, const Data::CollClass& klass) : source(klass)
	{
		schema.membershipDefinitions.push_back(this);
	}

	void MembershipDefinition::WriteBody(std::ostream& output, const std::string& indent)
	{
		output << indent << source.GetName() << " of " << source.memberType->GetLocalName();
		
		if (source.memberTypeSize)
			output << "[" << source.memberTypeSize << "]";

		if (source.memberTypePrecision)
		{
			output << "[" << source.memberTypePrecision;
			if (source.memberTypeScaleFactor)
				output << "," << source.memberTypeScaleFactor;
			output << "]";
		}
		
		output << ";\n";
	}
}