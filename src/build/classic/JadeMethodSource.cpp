#include "MethodSource.h"
#include "TypeSource.h"

namespace JadeGit::Build::Classic
{
	class JadeMethodSource : public MethodSource
	{
	public:
		JadeMethodSource(SchemaDefinition& schema, TypeSource& type, const Data::JadeMethod& method) : MethodSource(schema, type, method)
		{
			type.jadeMethodSources.push_back(this);
		}
	};

	static NodeRegistration<JadeMethodSource, Data::JadeMethod> registrar;
}