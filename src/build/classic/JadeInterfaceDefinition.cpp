#include "JadeInterfaceDefinition.h"
#include "SchemaDefinition.h"

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	static NodeRegistration<JadeInterfaceDefinition, JadeInterface> registrar;

	JadeInterfaceDefinition::JadeInterfaceDefinition(SchemaDefinition& schema, const JadeInterface& interface) : TypeDefinition(schema, interface, true)
	{
		schema.interfaceDefinitions.push_back(this);
	}

	void JadeInterfaceDefinition::WriteTypeBody(std::ostream& output, const std::string& indent) const
	{
		auto& source = static_cast<const JadeInterface&>(this->source);

		if (!source.superinterfaces.empty())
		{
			output << indent << "subInterfaceOf" << std::endl;
			for (const JadeInterface* super : source.superinterfaces)
				output << indent << "\t" << super->GetName() << std::endl;
		}
		TypeDefinition::WriteTypeBody(output, indent);
	}
}