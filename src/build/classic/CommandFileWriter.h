#pragma once
#include <build/Builder.h>
#include <jadegit/Version.h>
#include "..\CommandStrategy.h"
#include <functional>

namespace JadeGit::Build::Classic
{
	class CommandFileWriter : public CommandStrategy
	{
	public:
		CommandFileWriter(Builder &builder, const Version& jadeVersion, std::function<void(void)> flush, bool latest);
		~CommandFileWriter();

	protected:
		void CreateLocale(const std::string& qualifiedName, const char* baseLocale) override;
		void Delete(const char* entityType, const std::string& qualifiedName) override;
		void Rename(const char* entityType, const std::string& qualifiedName, const std::string& newName) override;
		void ModifySchema(const std::string& schemaName, const std::string& key, const std::string& value) override;
		void MoveClass(const std::string& qualifiedName, const std::string& newSuperclass) override;
		void Flush() override;

	private:
		const Version& jadeVersion;
		std::unique_ptr<std::ostream> output;
		std::function<void(void)> flush;
	};
}