#pragma once
#include "ClassDefinition.h"

namespace JadeGit::Build::Classic
{
	class JadeInterfaceMappingDefinition : public SchemaDefinitionNode<Data::JadeInterface>
	{
	public:
		using parent_node = ClassDefinition;

		JadeInterfaceMappingDefinition(ClassDefinition& type, const Data::JadeInterfaceMapping& mapping);

	protected:
		void WriteEnter(std::ostream& output, const std::string& indent) override;
		void WriteBody(std::ostream& output, const std::string& indent) override;

	private:
		const Data::JadeInterfaceMapping& source;
	};
}