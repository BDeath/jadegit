#pragma once
#include "SchemaEntityDefinition.h"
#include <jadegit/data/Type.h>

namespace JadeGit::Build::Classic
{
	class TypeDeclaration : public SchemaEntityDefinition<Data::Type, true>
	{
	public:
		using base_node = TypeDeclaration;

		using SchemaEntityDefinition::SchemaEntityDefinition;

		void WriteAttributes(std::ostream& output) override;
	};
}