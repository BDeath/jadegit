#include "TypeDefinition.h"
#include "SchemaDefinition.h"
#include <jadegit/data/PrimType.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	static NodeRegistration<TypeDefinition, Type> type;
	static NodeRegistration<TypeDefinition, PrimType> primType(true);

	TypeDefinition::TypeDefinition(SchemaDefinition& schema, const Type& type, bool interface) :
		SchemaDefinitionNode(type),
		interface(interface),
		constantDefinitions("constantDefinitions"),
		jadeMethodDefinitions("jadeMethodDefinitions"),
		webServicesMethodDefinitions("webServicesMethodDefinitions"),
		externalMethodDefinitions("externalMethodDefinitions")
	{
		if(!interface)
			schema.typeDefinitions.push_back(this);
	}

	void TypeDefinition::WriteEnter(std::ostream& output, const std::string& indent)
	{
		output << indent << source.GetLocalName();
	}

	void TypeDefinition::WriteAttributes(std::ostream& output)
	{
		SchemaDefinitionNode::WriteAttributes(output);
		if(!interface)
			WriteAttribute(output, "completeDefinition", complete);
	}

	void TypeDefinition::WriteBody(std::ostream& output, const std::string& indent)
	{
		SchemaDefinitionNode::WriteBody(output, indent);

		WriteDocumentation(output);
		WriteModifiedTimeStamp(output);

		WriteTypeBody(output, indent);
	}

	void TypeDefinition::WriteTypeBody(std::ostream& output, const std::string& indent) const
	{
		constantDefinitions.Write(output, indent);
		jadeMethodDefinitions.Write(output, indent);
		webServicesMethodDefinitions.Write(output, indent);
		externalMethodDefinitions.Write(output, indent);
		WriteEventMethodMappings(output, indent);
	}

	void TypeDefinition::WriteEventMethodMappings(std::ostream& output, const std::string& indent) const
	{
		if (eventMethodMappings.empty())
			return;

		output << indent << "eventMethodMappings" << std::endl;

		for (auto& mapping : eventMethodMappings)
		{
			output << (indent + "\t") << mapping.first << " = " << mapping.second->GetName() << " of " << mapping.second->schemaType->GetName() << ";" << std::endl;
		}
	}
}