#pragma once
#include "ClassDefinition.h"
#include <jadegit/data/CollClass.h>

namespace JadeGit::Build::Classic
{
	class CollClassDefinition : public ClassDefinition
	{
	public:
		CollClassDefinition(SchemaDefinition& schema, const Data::CollClass& klass);

	};
}