#pragma once
#include "SchemaDefinitionNode.h"
#include "DataNode.h"
#include <jadegit/data/Entity.h>

namespace JadeGit::Build
{
	class Builder;
}

namespace JadeGit::Build::Classic
{
	class SchemaDefinition : public SchemaDefinitionNode<Data::Schema*, false>
	{
	public:
		SchemaDefinition();
		virtual ~SchemaDefinition() override;

		void Extract(Builder &builder, bool latestVersion);

		DataNodes data;

		DefinitionNodes importedPackageDefinitions;
		DefinitionNodes constantDefinitions;
		DefinitionNodes localeDefinitions;
		DefinitionNodes translatableStringDefinitions;
		DefinitionNodes localeFormatDefinitions;
		DefinitionNodes libraryDefinitions;
		DefinitionNodes externalFunctionDefinitions;
		DefinitionNodes typeHeaders;
		DefinitionNodes interfaceDefinitions;
		DefinitionNodes membershipDefinitions;
		DefinitionNodes typeDefinitions;
		DefinitionNodes externalKeyDefinitions;
		DefinitionNodes memberKeyDefinitions;
		DefinitionNodes inverseDefinitions;
		DefinitionNodes databaseDefinitions;
		DefinitionNodes exposedListDefinitions;
		DefinitionNodes exportedPackageDefinitions;
		DefinitionNodes externalFunctionSources;
		DefinitionNodes typeSources;

	protected:
		void WriteEnter(std::ostream& output, const std::string& indent) override;
		void WriteBody(std::ostream& output, const std::string& indent) override;
		void WriteData(Builder& builder, bool latestVersion);
	};
}