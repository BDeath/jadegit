#pragma once
#include <build/Builder.h>
#include "../FileBuilder.h"
#include "CommandFileWriter.h"
#include "SchemaFileWriter.h"
#include <jadegit/Version.h>

namespace JadeGit::Build::Classic
{
	class ClassicFileBuilder : public FileBuilder
	{
	public:
		ClassicFileBuilder(Builder &builder, const Version& jadeVersion);

	private:
		Version jadeVersion;
		Builder& builder;

		bool processing = false;
		bool reorging = false;
		bool latest = false;

		CommandFileWriter* commander = nullptr;
		SchemaFileWriter* extractor = nullptr;

		bool visit(const BuildTask& task) final;
		bool visit(const CommandTask& task) final;
		bool visit(const ExtractTask& task) final;
		bool visit(const ReorgTask& task) final;
		void flush(bool final) final;
	};
}