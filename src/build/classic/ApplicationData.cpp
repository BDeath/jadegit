#include "DataNode.h"
#include "SchemaDefinition.h"
#include <jadegit/data/Application.h>
#include <jadegit/data/JadeExportedPackage.h>
#include <jadegit/data/JadeWebServiceManager.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ApplicationMeta.h>
#include <jadegit/data/RootSchema/JadeWebServiceManagerMeta.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	class ApplicationData : public DataNode
	{
	public:
		using parent_node = SchemaDefinition;

		ApplicationData(SchemaDefinition& schema, const Application& application) : DataNode(schema.data, application) {}

	protected:
		void PrintAttributes(XMLPrinter& printer) const override
		{
			auto& source = static_cast<const Application&>(this->source);

			printer.PushAttribute("name", source.name.c_str());
		}

		void PrintData(XMLPrinter& printer) const override
		{
			DataNode::PrintData(printer);

			auto& source = static_cast<const Application&>(this->source);

			if (source.webServiceManager)
			{
				printer.OpenElement("jadeWebServiceManager");
				DataNode::PrintData(printer, source.webServiceManager);
				printer.CloseElement();
			}
		}

		bool PrintPropertyFilter(const RootSchema& rootSchema, const Property* property) const override
		{
			// Exclude parent reference
			if (property == rootSchema.application->schema)
				return false;
			
			// Filter out properties which aren't used by application type
			auto& source = static_cast<const Application&>(this->source);
			if (!isPropertyForApplicationType(rootSchema.application, property, source.applicationType))
				return false;

			return DataNode::PrintPropertyFilter(rootSchema, property);
		}

		void PrintCollection(XMLPrinter& printer, const RootSchema& rootSchema, const Property* property, const CollClass& type, const Type& memberType, const Collection& coll) const final
		{
			// Print class collections using expected entry tags
			if (property == rootSchema.application->threeDControls)
				return DataNode::PrintCollection(printer, coll, "GUIClass", false);
			else if (property == rootSchema.application->webEventClasses)
				return DataNode::PrintCollection(printer, coll, "Class", false);

			// Passback to base implementation otherwise
			return DataNode::PrintCollection(printer, rootSchema, property, type, memberType, coll);
		}

	private:
		bool isPropertyForApplicationType(const ApplicationMeta* meta, const Property* property, const Application::Type& applicationType) const
		{
			if (property == meta->internetPipeName ||
				property == meta->numberOfPipes ||
				property == meta->webAppDirectory ||
				property == meta->webBaseURL ||
				property == meta->webDisplayMessages ||
				property == meta->webDisplayPreference ||
				property == meta->webHomePage ||
				property == meta->webMachineName ||
				property == meta->webMaxHTMLSize ||
				property == meta->webServiceClasses ||
				property == meta->webSessionTimeout ||
				property == meta->webShowModal ||
				property == meta->webStatusLineDisplay ||
				property == meta->webUseHTML32 ||
				property == meta->webVirtualDirectory)
			{
				return (applicationType == Application::Type::REST_Service || 
						applicationType == Application::Type::REST_Service_Non_GUI ||
						applicationType == Application::Type::Web_Enabled || 
						applicationType == Application::Type::Web_Enabled_Non_GUI);
			}

			// Assume it is used otherwise
			return true;
		}
	};
	static NodeRegistration<ApplicationData, Application> registrar;
}