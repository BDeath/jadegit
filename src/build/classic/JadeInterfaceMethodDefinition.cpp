#include "MethodDefinition.h"
#include "JadeInterfaceDefinition.h"

namespace JadeGit::Build::Classic
{
	class JadeInterfaceMethodDefinition : public MethodDefinition
	{
	public:
		using parent_node = JadeInterfaceDefinition;

		JadeInterfaceMethodDefinition(JadeInterfaceDefinition& interface, const Data::JadeInterfaceMethod& method) : MethodDefinition(interface, method)
		{
			interface.jadeMethodDefinitions.push_back(this);	// Yep, it's inconsistent 
		}
	};

	static RoutineDefinitionRegistration<JadeInterfaceMethodDefinition, Data::JadeInterfaceMethod> registrar;
}