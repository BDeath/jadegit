#include "SchemaEntityDefinition.h"
#include "SchemaDefinition.h"
#include <jadegit/data/Schema.h>

namespace JadeGit::Build::Classic
{
	class LocaleDefinition : public SchemaEntityDefinition<Data::Locale>
	{
	public:
		using parent_node = SchemaDefinition;

		LocaleDefinition(SchemaDefinition& schema, const Data::Locale& locale) : SchemaEntityDefinition(locale)
		{
			schema.localeDefinitions.push_back(this);
		}

		void WriteEnter(std::ostream& output, const std::string& indent) override
		{
			SchemaEntityDefinition::WriteEnter(output, indent);

			if(source.cloneOf)
				output << " _cloneOf " << source.cloneOf->GetName();
		}

		void WriteAttributes(std::ostream& output) override
		{
			SchemaEntityDefinition::WriteAttributes(output);

			WriteAttribute(output, "schemaDefaultLocale", source.isPrimary());
		}
	};
	static NodeRegistration<LocaleDefinition, Data::Locale> registrar;	
}