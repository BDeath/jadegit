#pragma once
#include "SchemaDefinitionNode.h"
#include <jadegit/data/Type.h>

namespace JadeGit::Build::Classic
{
	class TypeDefinition : public SchemaDefinitionNode<Data::Type>
	{
	public:
		using parent_node = SchemaDefinition;

		TypeDefinition(SchemaDefinition& schema, const Data::Type& type, bool interface = false);

		DefinitionNodes constantDefinitions;
		DefinitionNodes jadeMethodDefinitions;
		DefinitionNodes webServicesMethodDefinitions;
		DefinitionNodes externalMethodDefinitions;
		std::map<std::string, const Data::Method*> eventMethodMappings;

	protected:
		void WriteEnter(std::ostream& output, const std::string& indent) override;
		void WriteAttributes(std::ostream& output) override;
		void WriteBody(std::ostream& output, const std::string& indent) override final;
		virtual void WriteTypeBody(std::ostream& output, const std::string& indent) const;
		void WriteEventMethodMappings(std::ostream& output, const std::string& indent) const;

	private:
		const bool interface;
	};
}