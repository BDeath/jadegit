#include "DictionaryDefinition.h"

namespace JadeGit::Build::Classic
{
	DictionaryDefinition::DictionaryDefinition(SchemaDefinition& schema, const Data::CollClass& klass) : SchemaDefinitionNode(klass), keys(nullptr)
	{
		complete = true;	// Always complete, otherwise keys are duplicated
	}

	void DictionaryDefinition::WriteAttributes(std::ostream& output)
	{
		SchemaDefinitionNode::WriteAttributes(output);
		WriteAttribute(output, "completeDefinition", complete);
	}

	void DictionaryDefinition::WriteBody(std::ostream& output, const std::string& indent)
	{
		SchemaDefinitionNode::WriteBody(output, indent);

		keys.Write(output, indent);
	}

	KeyDefinition::KeyDefinition(SchemaDefinition& schema, DictionaryDefinition& dictionary, const Data::Key& key) : source(key)
	{
		dictionary.keys.push_back(this);
	}

	void KeyDefinition::WriteBody(std::ostream& output, const std::string& indent)
	{
		if (source.descending)
		{
			output << " descending";
			if (source.caseInsensitive)
				output << ",";
		}

		if (source.caseInsensitive)
			output << " caseInsensitive";

		if (source.sortOrder == -1)
			output << " Latin1";
		else if (source.sortOrder)
			output << " " << source.sortOrder;

		output << ";" << std::endl;
	}
}