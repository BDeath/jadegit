#include "SchemaDefinition.h"
#include <build/Builder.h>
#include <jadegit/data/Schema.h>
#include <jadegit/data/RootSchema.h>
#include <xml/XMLPrinter.h>

using namespace std;
using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	// Specialization for schema definition to handle checking/updating supplied instead of creating
	template<>
	class NodeRegistration<SchemaDefinition, Schema> : NodeFactory<DefinitionNode>::Registration
	{
	public:
		DefinitionNode* create(SchemaDefinition& schema, const Component& origin, bool complete, bool modified, bool had_text) const final
		{
			// Resolve schema from origin using factory, as it isn't necessarily its direct parent
			Schema* component = ObjectFactory::Get().Resolve<Schema>(&origin);
			assert(component);

			if (!schema.source)
				schema.source = component;
			else if (schema.source != component)
				return nullptr;

			if (complete)
				schema.complete = true;

			if (modified)
				schema.modified = true;

			if (had_text)
				schema.had_text = true;

			return &schema;
		}

		NodeRegistration()
		{
			NodeFactory<DefinitionNode>::get().register_(type_index(typeid(Schema)), this);
			NodeFactory<DefinitionNode>::get().register_(type_index(typeid(SchemaDefinition)), this);
		}
	};

	static NodeRegistration<SchemaDefinition, Schema> registrar;

	SchemaDefinition::SchemaDefinition() : SchemaDefinitionNode(nullptr),
		importedPackageDefinitions("importedPackageDefinitions"),
		constantDefinitions("constantDefinitions"),
		localeDefinitions("localeDefinitions"),
		translatableStringDefinitions("translatableStringDefinitions"),
		localeFormatDefinitions("localeFormatDefinitions"),
		libraryDefinitions("libraryDefinitions"),
		externalFunctionDefinitions("externalFunctionDefinitions"),
		typeHeaders("typeHeaders"),
		interfaceDefinitions("interfaceDefs"),
		membershipDefinitions("membershipDefinitions"),
		typeDefinitions("typeDefinitions"),
		externalKeyDefinitions("externalKeyDefinitions"),
		memberKeyDefinitions("memberKeyDefinitions"),
		inverseDefinitions("inverseDefinitions"),
		databaseDefinitions("databaseDefinitions"),
		exposedListDefinitions("_exposedListDefinitions"),
		exportedPackageDefinitions("exportedPackageDefinitions"),
		externalFunctionSources("externalFunctionSources"),
		typeSources("typeSources") {}

	SchemaDefinition::~SchemaDefinition()
	{
		// Clean-up constant category definitions (not added to keyed children collection as they may be duplicated)
		for (auto def : constantDefinitions)
			delete def;
	}

	void SchemaDefinition::Extract(Builder& builder, bool latestVersion)
	{
		// Safeguard applicable in scenarios where there's unhandled components
		if (!source)
			return;

		// Always write definition file (it'll at least declare schema)
		auto definition = builder.AddSchemaFile(source->name.c_str(), latestVersion);
		Write(*definition);
		*definition << flush;

		// Write data file if required
		WriteData(builder, latestVersion);
	}

	void SchemaDefinition::WriteEnter(ostream& output, const string& indent)
	{
		output << "jadeVersionNumber \"" << string(source->GetRootSchema().version) << "\";\n";
		output << "schemaDefinition\n";

		output << source->name << " subschemaOf " << source->GetSuperSchema()->name << (complete ? " completeDefinition" : " partialDefinition");

		if (auto& signature = source->getSignature(); signature.patch)
			output << format(", patchVersion={}, patchVersioningEnabled = true", signature.patch);
	}

	void SchemaDefinition::WriteBody(ostream& output, const string& indent)
	{
		SchemaDefinitionNode::WriteBody(output, indent);

		WriteDocumentation(*source, output);
		WriteModifiedTimeStamp(output);

		importedPackageDefinitions.Write(output);
		constantDefinitions.Write(output);
		localeDefinitions.Write(output);
		translatableStringDefinitions.Write(output);
		localeFormatDefinitions.Write(output);
		libraryDefinitions.Write(output);
		externalFunctionDefinitions.Write(output);
		typeHeaders.Write(output);
		interfaceDefinitions.Write(output);
		membershipDefinitions.Write(output);
		typeDefinitions.Write(output);
		externalKeyDefinitions.Write(output);
		memberKeyDefinitions.Write(output);
		inverseDefinitions.Write(output);
		databaseDefinitions.Write(output);
		exposedListDefinitions.Write(output);
		exportedPackageDefinitions.Write(output);
		externalFunctionSources.Write(output);
		typeSources.Write(output);
	}

	void SchemaDefinition::WriteData(Builder& builder, bool latestVersion)
	{
		if (data.empty())
			return;

		XMLPrinter printer;
		printer.PushHeader(false, true);
			
		printer.OpenElement("schema");
		printer.PushAttribute("name", source->name.c_str());
		printer.PushAttribute("JadeVersionNumber", string(source->GetRootSchema().version).c_str());
		printer.PushAttribute("JadePatchNumber", to_string(source->getSignature().patch).c_str());
		printer.PushAttribute("CompleteDefinition", complete);

		data.Print(printer);
		printer.CloseElement();

		auto output = builder.AddSchemaDataFile(source->name.c_str(), latestVersion);
		*output << printer.CStr() << flush;
	}
}