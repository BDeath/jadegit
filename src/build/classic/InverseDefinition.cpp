#include "InverseDefinition.h"
#include "SchemaDefinition.h"
#include <jadegit/data/Type.h>

using namespace std;
using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	static NodeRegistration<InverseDefinition, Inverse> registrar;

	InverseDefinition::InverseDefinition(SchemaDefinition& schema, const Inverse& inverse) : source(inverse)
	{
		schema.inverseDefinitions.push_back(this);
	}

	ostream& operator<< (ostream& stream, const ExplicitInverseRef* reference)
	{
		stream << reference->GetName() << " of " << reference->schemaType->GetName();
		return stream;
	}

	ostream& operator<< (ostream& stream, ExplicitInverseRef::ReferenceKind kind)
	{
		switch (kind)
		{
		case ExplicitInverseRef::ReferenceKind::Peer:
			stream << " peerOf ";
			break;
		case ExplicitInverseRef::ReferenceKind::Parent:
			stream << " parentOf ";
			break;
		default:
			throw logic_error("Unexpected reference kind");
		}

		return stream;
	}

	ostream& operator<< (ostream& stream, ExplicitInverseRef::UpdateMode mode)
	{
		switch (mode)
		{
		case ExplicitInverseRef::UpdateMode::Manual:
			stream << " manual";
			break;
		case ExplicitInverseRef::UpdateMode::Manual_Automatic:
			// default/implied
			break;
		case ExplicitInverseRef::UpdateMode::Automatic:
			stream << " automatic";
			break;
		case ExplicitInverseRef::UpdateMode::Automatic_Deferred:
			stream << " automaticDeferred";
			break;
		case ExplicitInverseRef::UpdateMode::Manual_Automatic_Deferred:
			stream << " manualAutomaticDeferred";
			break;
		default:
			throw logic_error("Unexpected reference update mode");
		}

		return stream;
	}

	ostream& operator<< (ostream& stream, const Inverse& inverse)
	{
		stream << inverse.reference << inverse.getReferenceUpdateMode();
		return stream;
	}

	void InverseDefinition::WriteBody(ostream& output, const string& indent)
	{
		output << indent << source << source.getReferenceKind() << *source.counterpart << ";" << endl;
	}
}