#include "SchemaEntityDefinition.h"
#include "SchemaDefinition.h"
#include <jadegit/data/Library.h>

namespace JadeGit::Build::Classic
{
	class LibraryDefinition : public SchemaEntityDefinition<Data::Library>
	{
	public:
		using parent_node = SchemaDefinition;

		LibraryDefinition(SchemaDefinition& schema, const Data::Library& library) : SchemaEntityDefinition(library)
		{
			schema.libraryDefinitions.push_back(this);
		}

		void WriteEnter(std::ostream& output, const std::string& indent) override
		{
			output << indent << "\"" << source.GetName() << "\"";
		}
	};

	static NodeRegistration<LibraryDefinition, Data::Library> registrar;
}