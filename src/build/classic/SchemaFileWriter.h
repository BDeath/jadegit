#pragma once
#include "..\ExtractStrategy.h"
#include "SchemaDefinition.h"

namespace JadeGit::Build
{
	class Builder;
}

namespace JadeGit::Build::Classic
{
	class SchemaFileWriter : public ExtractStrategy
	{
	public:
		SchemaFileWriter(Builder &builder, bool latest);
		void Write();

	protected:
		bool Declare(const Data::Class& klass) final;

		bool Define(const Data::ActiveXLibrary& library) final;
		bool Define(const Data::Application& application) final;
		bool Define(const Data::ExternalDatabase& database) final;
		bool Define(const Data::JadeExposedClass& cls) final;
		bool Define(const Data::JadeExposedFeature& feature) final;
		bool Define(const Data::JadeExposedList& list) final;
		bool Define(const Data::JadeHTMLDocument& document, const char* old_name) final;
		bool Define(const Data::JadeInterfaceMapping& mapping) final;
		bool Define(const Data::Form& form) final;
		bool Define(const Data::RelationalAttribute& attribute) final;
		bool Define(const Data::RelationalTable& table) final;
		bool Define(const Data::RelationalView& view) final;
		bool Define(const Data::Schema& schema, bool complete, bool modifie, bool had_text) final;
		bool Define(const Data::SchemaEntity& entity, bool complete, bool modified, bool had_text) final;

	private:
		Builder &builder;
		SchemaDefinition root;
		const bool latest;
	};
}