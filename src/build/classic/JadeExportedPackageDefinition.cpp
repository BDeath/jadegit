#include "SchemaDefinition.h"
#include "SchemaEntityDefinition.h"
#include <jadegit/data/JadeExportedPackage.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	class JadeExportedPackageDefinition : public SchemaDefinitionNode<JadeExportedPackage>
	{
	public:
		using parent_node = SchemaDefinition;

		DefinitionNodes exportedClassDefinitions;
		DefinitionNodes exportedInterfaceDefinitions;

		JadeExportedPackageDefinition(SchemaDefinition& schema, const JadeExportedPackage& package) : SchemaDefinitionNode(package),
			exportedClassDefinitions("exportedClassDefinitions"),
			exportedInterfaceDefinitions("exportedInterfaceDefinitions")
		{
			schema.exportedPackageDefinitions.push_back(this);

			// Setup data node to link associated application
			NodeFactory<DataNode>::get().create(std::type_index(typeid(package)), schema, package);
		}

	protected:
		void WriteAttributes(std::ostream& output) override
		{
			SchemaDefinitionNode::WriteAttributes(output);
			WriteAttribute(output, "partialDefinition", !complete);
		}

		void WriteBody(std::ostream& output, const std::string& indent) override
		{
			SchemaDefinitionNode::WriteBody(output, indent);

			WriteDocumentation(output);
			WriteModifiedTimeStamp(output);

			exportedClassDefinitions.Write(output, indent);
			exportedInterfaceDefinitions.Write(output, indent);
		}
	};

	class JadeExportedTypeDefinition : public SchemaDefinitionNode<JadeExportedType>
	{
	public:
		using parent_node = JadeExportedPackageDefinition;

		DefinitionNodes exportedConstantDefinitions;
		DefinitionNodes exportedPropertyDefinitions;
		DefinitionNodes exportedMethodDefinitions;

		JadeExportedTypeDefinition(JadeExportedPackageDefinition& package, const JadeExportedType& type) : SchemaDefinitionNode(type),
			exportedConstantDefinitions("exportedConstantDefinitions"),
			exportedPropertyDefinitions("exportedPropertyDefinitions"),
			exportedMethodDefinitions("exportedMethodDefinitions") {}

	protected:
		void WriteBody(std::ostream& output, const std::string& indent) override
		{
			SchemaDefinitionNode::WriteBody(output, indent);

			WriteModifiedTimeStamp(output);

			exportedConstantDefinitions.Write(output, indent);
			exportedPropertyDefinitions.Write(output, indent);
			exportedMethodDefinitions.Write(output, indent);
		}
	};

	class JadeExportedClassDefinition : public JadeExportedTypeDefinition
	{
	public:
		JadeExportedClassDefinition(JadeExportedPackageDefinition& package, const JadeExportedClass& type) : JadeExportedTypeDefinition(package, type)
		{
			package.exportedClassDefinitions.push_back(this);
		}

	protected:
		void WriteAttributes(std::ostream& output) override
		{
			JadeExportedTypeDefinition::WriteAttributes(output);

			WriteAttribute(output, "partialDefinition", !complete);

			auto& source = static_cast<const JadeExportedClass&>(this->source);

			WriteAttribute(output, "persistentAllowed", source.persistentAllowed);
			WriteAttribute(output, "sharedTransientAllowed", source.sharedTransientAllowed);
			WriteAttribute(output, "transientAllowed", source.transientAllowed);

			WriteAttribute(output, "transient", source.transient || !source.persistentAllowed);
		}
	};

	class JadeExportedInterfaceDefinition : public JadeExportedTypeDefinition
	{
	public:
		JadeExportedInterfaceDefinition(SchemaDefinition& schema, JadeExportedPackageDefinition& package, const JadeExportedInterface& type) : JadeExportedTypeDefinition(package, type)
		{
			package.exportedInterfaceDefinitions.push_back(this);
		}

	protected:
		void WriteBody(std::ostream& output, const std::string& indent) override
		{
			// Oddly, we don't need the semi-colon for these
			output << "\n";

			WriteModifiedTimeStamp(output);

			// Schema extracts don't actually include the interface features
			// For methods (and potentially properties in future), this makes sense for an interface to
			// fulfill it's obligations, but it's plausible we might need to explicitly export constants
			// in future, in order to gain the ability to hide the ones we don't want to expose
		}

		void WriteExit(std::ostream& output, const std::string& indent) override
		{
			// No brackets
		}
	};

	template <class TExportedFeature>
	class JadeExportedFeatureDefinition : public SchemaEntityDefinition<TExportedFeature>
	{
	public:
		using parent_node = JadeExportedTypeDefinition;

		using SchemaEntityDefinition<TExportedFeature>::SchemaEntityDefinition;
	};

	class JadeExportedConstantDefinition : public JadeExportedFeatureDefinition<JadeExportedConstant>
	{
	public:
		JadeExportedConstantDefinition(JadeExportedTypeDefinition& type, const JadeExportedConstant& constant) : JadeExportedFeatureDefinition(constant)
		{
			type.exportedConstantDefinitions.push_back(this);
		}
	};

	class JadeExportedMethodDefinition : public JadeExportedFeatureDefinition<JadeExportedMethod>
	{
	public:
		JadeExportedMethodDefinition(JadeExportedTypeDefinition& type, const JadeExportedMethod& method) : JadeExportedFeatureDefinition(method)
		{
			type.exportedMethodDefinitions.push_back(this);
		}
	};

	class JadeExportedPropertyDefinition : public JadeExportedFeatureDefinition<JadeExportedProperty>
	{
	public:
		JadeExportedPropertyDefinition(JadeExportedTypeDefinition& type, const JadeExportedProperty& property) : JadeExportedFeatureDefinition(property)
		{
			type.exportedPropertyDefinitions.push_back(this);
		}
	};

	static NodeRegistration<JadeExportedPackageDefinition, JadeExportedPackage> package;
	static NodeRegistration<JadeExportedTypeDefinition, JadeExportedType> type;
	static NodeRegistration<JadeExportedClassDefinition, JadeExportedClass> cls;
	static NodeRegistration<JadeExportedInterfaceDefinition, JadeExportedInterface> interface;
	static NodeRegistration<JadeExportedConstantDefinition, JadeExportedConstant> constant;
	static NodeRegistration<JadeExportedMethodDefinition, JadeExportedMethod> method;
	static NodeRegistration<JadeExportedPropertyDefinition, JadeExportedProperty> property;
}