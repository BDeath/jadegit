#pragma once
#include "TypeDeclaration.h"
#include <jadegit/data/Class.h>

namespace JadeGit::Build::Classic
{
	class TypeHeader : public TypeDeclaration
	{
	public:
		using parent_node = SchemaDefinition;

		TypeHeader(SchemaDefinition& schema, const Data::Class& klass);

		void WriteEnter(std::ostream& output, const std::string& indent) override;
		void WriteAttributes(std::ostream& output) override;
	};
}