#include "PropertyDefinition.h"
#include "ClassDefinition.h"
#include <jadegit/data/Attribute.h>

namespace JadeGit::Build::Classic
{
	template <class TAttribute>
	class AttributeDefinition : public PropertyDefinition<TAttribute>
	{
	public:
		AttributeDefinition(ClassDefinition& klass, const TAttribute& attribute) : PropertyDefinition<TAttribute>(attribute)
		{
			klass.attributeDefinitions.push_back(this);
		}

		void WriteEnter(std::ostream& output, const std::string& indent) override
		{
			PropertyDefinition<TAttribute>::WriteEnter(output, indent);

			if (this->source.length)
				output << "[" << this->source.length << "]";
		}
	};

	class CompAttributeDefinition : public AttributeDefinition<Data::CompAttribute>
	{
	public:
		using AttributeDefinition::AttributeDefinition;
	};
	static NodeRegistration<CompAttributeDefinition, Data::CompAttribute> compAttribute;
	static NodeRegistration<DynamicPropertyDefinition<CompAttributeDefinition, Data::JadeDynamicCompAttribute>, Data::JadeDynamicCompAttribute> dynamicCompAttribute(true);

	class PrimAttributeDefinition : public AttributeDefinition<Data::PrimAttribute>
	{
	public:
		using AttributeDefinition::AttributeDefinition;

	protected:
		void WriteEnter(std::ostream& output, const std::string& indent) override
		{
			AttributeDefinition::WriteEnter(output, indent);

			if (source.precision)
			{
				output << "[" << source.precision;
				if (source.scaleFactor)
					output << "," << source.scaleFactor;
				output << "]";
			}
		}

		void WriteAttributes(std::ostream& output) override
		{
			AttributeDefinition::WriteAttributes(output);

			WriteAttribute(output, "_xmlType", source.xmlType);
		}
	};
	static NodeRegistration<PrimAttributeDefinition, Data::PrimAttribute> primAttribute;
	static NodeRegistration<PrimAttributeDefinition, Data::ExternalPrimAttribute> externalPrimAttribute(true);
	static NodeRegistration<DynamicPropertyDefinition<PrimAttributeDefinition, Data::JadeDynamicPrimAttribute>, Data::JadeDynamicPrimAttribute> dynamicPrimAttribute(true);
}