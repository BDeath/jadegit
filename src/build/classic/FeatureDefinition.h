#pragma once
#include "SchemaEntityDefinition.h"

namespace JadeGit::Build::Classic
{
	template <class TFeature>
	class FeatureDefinition : public SchemaEntityDefinition<TFeature>
	{
	public:
		using SchemaEntityDefinition<TFeature>::SchemaEntityDefinition;
	};
}