#pragma once
#include "DefinitionNode.h"
#include <jadegit/data/Routine.h>
#include <jadegit/data/Type.h>

namespace JadeGit::Build::Classic
{
	class RoutineSource : public DefinitionNode
	{
	public:
		using base_node = RoutineSource;

		RoutineSource(SchemaDefinition& schema, const Data::Routine& routine) : source(routine) {}

		void WriteBody(std::ostream& output, const std::string& indent) override;

	protected:
		const Data::Routine& source;
	};
}