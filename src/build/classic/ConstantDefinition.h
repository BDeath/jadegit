#pragma once
#include "FeatureDefinition.h"
#include <jadegit/data/Constant.h>

namespace JadeGit::Build::Classic
{
	class TypeDefinition;

	class ConstantDefinition : public FeatureDefinition<Data::Constant>
	{
	public:
		using parent_node = TypeDefinition;

		ConstantDefinition(TypeDefinition& type, const Data::Constant& constant);

		void WriteEnter(std::ostream& output, const std::string& indent) override;

	protected:
		using FeatureDefinition::FeatureDefinition;
	};
}