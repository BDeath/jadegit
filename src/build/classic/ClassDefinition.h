#pragma once
#include "TypeDefinition.h"
#include <jadegit/data/Class.h>

namespace JadeGit::Build::Classic
{
	class ClassDefinition : public TypeDefinition
	{
	public:
		ClassDefinition(SchemaDefinition& schema, const Data::Class& klass);

		DefinitionNodes attributeDefinitions;
		DefinitionNodes referenceDefinitions;
		DefinitionNodes interfaceMappingDefinitions;

	protected:
		void WriteTypeBody(std::ostream& output, const std::string& indent) const override;
	};
}