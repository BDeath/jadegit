#pragma once
#include "DefinitionNode.h"
#include <jadegit/data/RootSchema.h>
#include <vfs/FileSignature.h>
#include <iomanip>

namespace JadeGit::Build::Classic
{
	template<class TEntity, bool bracketed = true, bool terminated = true>
	class SchemaDefinitionNode : public DefinitionNode
	{
	public:
		SchemaDefinitionNode(const TEntity& entity) : source(entity) {}

		std::conditional_t<std::is_pointer_v<TEntity>, std::add_pointer_t<std::add_const_t<std::remove_pointer_t<TEntity>>>, const TEntity&> source;

	protected:
		virtual void WriteAttributes(std::ostream& output) {}

		void WriteEnter(std::ostream& output, const std::string& indent) override
		{
			if constexpr (std::is_pointer_v<TEntity>)
			{
				assert(source);
				output << indent << source->GetName();
			}
			else
				output << indent << source.GetName();
		}

		void WriteBody(std::ostream& output, const std::string& indent) override
		{
			WriteAttributes(output);

			if constexpr (bracketed)
				output << "\n" << indent << "(\n";
			else if constexpr (terminated)
				output << ";\n";
			else
				output << "\n";
		}		

		void WriteExit(std::ostream& output, const std::string& indent) override
		{
			if constexpr(bracketed)
				output << indent << ")\n";
		}

		template<class TSource>
		void WriteDocumentation(const TSource& source, std::ostream& output) const
		{
			if (!modified && !complete)
				return;

			if (source.text != "" || (Data::is_major_entity<TSource> && had_text && !complete))
			{
				output << "\tdocumentationText\n`";

				for (auto c : source.text)
				{
					if (c == '`')
						output << "``";
					else
						output << c;
				}
					
				output << "`\n";
			}
		}

		void WriteDocumentation(std::ostream& output) const
		{
			if constexpr (std::is_pointer_v<TEntity>)
				WriteDocumentation(*source, output);
			else
				WriteDocumentation(source, output);
		}

		template <class TSource>
		void WriteModifiedTimeStamp(const TSource& source, std::ostream& output) const
		{
			if (!modified)
				return;

			if (auto& signature = source.getSignature(); !signature.empty())
			{
				output << std::format("\t\tsetModifiedTimeStamp \"{}\" \"{}\"", signature.modifiedBy(30), std::string(source.GetRootSchema().version));

				if (signature.patch)
					output << std::format(" {}", signature.patch);

				output << std::format(" {0:%Y:%m:%d:%T};\n", floor<milliseconds>(signature.modified));
			}
		}

		void WriteModifiedTimeStamp(std::ostream& output) const
		{
			if constexpr (std::is_pointer_v<TEntity>)
				WriteModifiedTimeStamp(*source, output);
			else
				WriteModifiedTimeStamp(source, output);
		}
	};
}