#include "DataNode.h"
#include <jadegit/data/CollClass.h>
#include <jadegit/data/PropertyIterator.h>
#include <jadegit/data/Reference.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ApplicationMeta.h>
#include <jadegit/data/RootSchema/CollectionMeta.h>
#include <jadegit/data/RootSchema/SchemaEntityMeta.h>
#include <jadegit/data/MetaSchema/MetaType.h>
#include <vfs/FileSignature.h>
#include <xml/XMLPrinter.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	DataNode::DataNode(const Object& source, const char* type) : source(source), type(type ? type : source.dataClass->name.c_str()) {}

	DataNode::DataNode(DataNodes& nodes, const Object& source, const char* type) : DataNode(source, type)
	{
		nodes.push_back(this);
	}

	void DataNodes::Print(XMLPrinter& printer) const
	{
		for (auto node : *this)
			node->Print(printer);
	}

	void DataNode::Print(XMLPrinter& printer) const
	{
		printer.OpenElement(type);

		PrintAttributes(printer);
		PrintData(printer);

		printer.CloseElement();
	}

	void DataNode::PrintData(XMLPrinter& printer) const
	{
		PrintData(printer, source);
	}

	void DataNode::PrintData(XMLPrinter& printer, const Object& source) const
	{
		const RootSchema& rootSchema = source.GetRootSchema();

		// Print modified by details for major entities, using null property filter to suppress when specific properties included only
		if (source.isMajor() && PrintPropertyFilter(rootSchema, nullptr))
			PrintSignatureData(printer, static_cast<const Entity&>(source));
		
		// Print property values
		PropertyIterator iter(source.dataClass);
		for (auto& prop : iter)
		{
			if (!PrintPropertyFilter(rootSchema, &prop))
				continue;

			auto value = source.GetValue(prop);

			if (auto collClass = dynamic_cast<const CollClass*>(static_cast<const Type*>(prop.type)))
			{
				const Collection* coll = value.Get<Collection*>();
				if (coll && !coll->Empty())
				{
					printer.OpenElement(prop.GetAlias());
					PrintCollection(printer, rootSchema, &prop, *collClass, collClass->getMemberType(), *coll);
					printer.CloseElement();
				}
			}
			else
			{
				printer.OpenElement(prop.GetAlias());
				if (!value.empty())
				{
					PrintValue(printer, *static_cast<const Type*>(prop.type), value, useBasicFormatting(rootSchema, &prop));
				}
				printer.CloseElement();
			}
		}
	}

	bool DataNode::PrintPropertyFilter(const RootSchema& rootSchema, const Property* property) const
	{
		if (const ExplicitInverseRef* ref = dynamic_cast<const ExplicitInverseRef*>(property))
		{
			// Generally need to filter out automatic references
			if (ref->updateMode == ExplicitInverseRef::Automatic)
				return false;
		}
		else if (const ImplicitInverseRef* ref = dynamic_cast<const ImplicitInverseRef*>(property))
		{
			// Filter out exclusive collections
			if (ref->memberTypeInverse)
				return false;
		}

		// Include property
		return true;
	}

	void DataNode::PrintSignatureData(XMLPrinter& printer, const Data::Entity& source) const
	{
		if (auto& signature = source.getSignature(); !signature.empty())
		{
			printer.OpenElement("_modifiedBy");
			printer.PushText(signature.modifiedBy(30).c_str());
			printer.CloseElement();

			printer.OpenElement("_modifiedTimestamp");
			printer.PushText(format("{0:%Y:%m:%d:%T}", floor<milliseconds>(signature.modified)).c_str());
			printer.CloseElement();

			printer.OpenElement("_patchVersion");
			printer.PushText(signature.patch);
			printer.CloseElement();

			printer.OpenElement("_systemVersion");
			printer.PushText(string(source.GetRootSchema().version).c_str());
			printer.CloseElement();
		}
	}

	void DataNode::PrintCollection(XMLPrinter& printer, const Data::RootSchema& rootSchema, const Data::Property* property, const Data::CollClass& type, const Data::Type& memberType, const Data::Collection& coll) const
	{
		// Default primitive format, which uses the primitive type name as the member tag
		if (memberType.isPrimType())
			return PrintCollection(printer, coll, memberType.name.c_str(), true);
		
		// Default object format, which uses the specific object class name as the member tag
		Any entry;
		auto iter = coll.CreateIterator();
		while (iter->Next(entry))
		{
			auto object = entry.Get<Object*>();
			printer.OpenElement(object->dataClass->name.c_str());
			PrintValue(printer, object);
			printer.CloseElement();
		}
	}

	void DataNode::PrintCollection(XMLPrinter& printer, const Data::Collection& coll, const char* tag, bool primitive) const
	{
		Any entry;
		auto iter = coll.CreateIterator();
		if (primitive)
		{
			while (iter->Next(entry))
			{
				printer.OpenElement(tag);
				PrintValue(printer, entry);
				printer.CloseElement();
			}
		}
		else
		{
			while (iter->Next(entry))
			{
				printer.OpenElement(tag);	
				PrintValue(printer, entry.Get<Object*>());
				printer.CloseElement();
			}
		}
	}

	void DataNode::PrintValue(XMLPrinter& printer, const Type& type, const Any& value, bool basic) const
	{
		if (value.empty())
			return;

		if (type.isPrimType())
			PrintValue(printer, value, basic);
		else
			PrintValue(printer, value.Get<Object*>());	
	}

	void DataNode::PrintValue(XMLPrinter& printer, const Any& value, bool basic) const
	{
		auto text = basic ? value.ToBasicString() : value.ToString();
		if (!text.empty())
			printer.PushText(text.c_str());
	}

	void DataNode::PrintValue(XMLPrinter& printer, const Object* value) const
	{
		if (auto named = dynamic_cast<const INamedObject*>(value))
			printer.PushAttribute("name", named->GetName().c_str());
		else
			throw logic_error("Unhandled value");
	}

	bool DataNode::useBasicFormatting(const RootSchema& rootSchema, const Property* property) const
	{
		return true;
	}
}