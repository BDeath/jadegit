#include "MemberKeyDictionaryDefinition.h"
#include "SchemaDefinition.h"
#include <jadegit/data/ExternalCollClass.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	static NodeRegistration<MemberKeyDictionaryDefinition, CollClass> collClass;
	static NodeRegistration<MemberKeyDictionaryDefinition, ExternalCollClass> externalCollClass(true);
	static NodeRegistration<MemberKeyDefinition, MemberKey> key;

	MemberKeyDictionaryDefinition::MemberKeyDictionaryDefinition(SchemaDefinition& schema, const CollClass& klass) : DictionaryDefinition(schema, klass)
	{
		schema.memberKeyDefinitions.push_back(this);
	}

	void MemberKeyDefinition::WriteBody(std::ostream& output, const std::string& indent)
	{
		auto& source = static_cast<const MemberKey&>(this->source);

		output << indent;
		
		for (const Property* property : source.keyPath)
			output << property->GetName() << ".";

		output << source.property->GetName();

		KeyDefinition::WriteBody(output, indent);
	}
}