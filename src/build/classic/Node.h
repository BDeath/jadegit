#pragma once
#include <jadegit/data/ObjectFactory.h>
#include <typeindex>
#include <type_traits>
#include <vector>
#include <map>
#include <assert.h>

namespace JadeGit::Build::Classic
{
	class Node : virtual public MemoryAllocated
	{
	public:
		std::map<std::pair<const Data::Component*, std::type_index>, Node*> children;

		bool complete = false;
		bool modified = false;
		bool had_text = false;

		virtual ~Node();
	};

	class SchemaDefinition;
	class DefinitionNode;

	template <class TBase, typename... Args>
	class NodeFactory
	{
	public:
		static NodeFactory& get() { static NodeFactory f; return f; }

		class Registration
		{
		public:
			virtual TBase* create(SchemaDefinition& schema, const Data::Component& origin, bool complete, bool modified, bool had_text, Args... args) const = 0;
		};

		void register_(const std::type_index key, const Registration* registrar)
		{
			registry[key] = registrar;
		}

		TBase* create(const std::type_index key, SchemaDefinition& schema, const Data::Component& origin, Args... args)
		{
			return create(key, schema, origin, false, true, false, args...);
		}

		TBase* create(const std::type_index key, SchemaDefinition& schema, const Data::Component& origin, bool complete, bool modified, bool had_text, Args... args)
		{
			return lookup(key).create(schema, origin, complete, modified, had_text, args...);
		}

		TBase* resolve(const std::type_index key, SchemaDefinition& schema, const Data::Component& origin, Args... args)
		{
			return lookup(key).create(schema, origin, false, false, false, args...);
		}

	protected:
		NodeFactory() {}

		const Registration& lookup(const std::type_index& key) const
		{
			auto iter = registry.find(key);
			if (iter != registry.end())
				return *iter->second;

			throw std::logic_error("Missing node factory registration");
		}

	private:
		typedef std::map<std::type_index, const Registration*> Registry;
		Registry registry;
	};

	template <class TDerived, class TComponent, typename... Args>
	class NodeRegistration : NodeFactory<typename TDerived::base_node, Args...>::Registration
	{
	public:
		using TBase = TDerived::base_node;
		using TParent = TDerived::parent_node;
		using TParentBase = TParent::base_node;

		TBase* create(SchemaDefinition& schema, const Data::Component& origin, bool complete, bool modified, bool had_text, Args... args) const final
		{
			// Check origin is expected component
			const TComponent* component = dynamic_cast<const TComponent*>(&origin);
			if (!component)
			{
				// Resolve using object factory and recycle back to node factory to subclass registration
				if constexpr (std::is_base_of_v<Data::Object, TComponent>)
				{
					component = Data::ObjectFactory::Get().Resolve<TComponent>(&origin);
					assert(component);
					return NodeFactory<TBase, Args...>::get().create(std::type_index(typeid(*component)), schema, *component, complete, modified, had_text, args...);
				}
				assert(component);
			}

			// Resolve parent
			auto parent = NodeFactory<TParentBase>::get().resolve(std::type_index(typeid(TParent)), schema, *component);
			if (!parent)
				return nullptr;

			assert(dynamic_cast<TParent*>(parent));

			// Resolve child (creating if required)
			return create(schema, static_cast<TParent&>(*parent), *component, complete, modified, had_text, args...);
		}

		NodeRegistration(bool subclass = false)
		{
			NodeFactory<TBase, Args...>::get().register_(std::type_index(typeid(TComponent)), this);

			if (!subclass)
				NodeFactory<TBase, Args...>::get().register_(std::type_index(typeid(TDerived)), this);
		}

	protected:
		virtual TDerived* create(SchemaDefinition& schema, TParent& parent, const TComponent& component, bool complete, bool modified, bool had_text, Args... args) const
		{
			// Resolve child (creating if required)
			auto result = resolve(schema, parent, component, args...);

			// Set complete/modified/had_text flags
			if (result)
			{
				if (complete)
					result->complete = true;

				if (modified)
					result->modified = true;

				if (had_text)
					result->had_text = true;
			}

			return result;
		}

		virtual TDerived* resolve(SchemaDefinition& schema, TParent& parent, const TComponent& component, Args... args) const
		{
			std::pair<const Data::Component*, std::type_index> key(&component, std::type_index(typeid(TBase)));
			TDerived* child = static_cast<TDerived*>(parent.children[key]);
			if (!child)
			{
				if constexpr (std::is_constructible_v<TDerived, TParent&, const TComponent&, Args...>)
					child = new TDerived(parent, component, args...);
				else
					child = new TDerived(schema, parent, component, args...);
			}
			parent.children[key] = child;
			return child;
		}
	};
}