#pragma once
#include "DictionaryDefinition.h"
#include <jadegit/data/Key.h>

namespace JadeGit::Build::Classic
{
	class ExtKeyDictionaryDefinition : public DictionaryDefinition
	{
	public:
		using base_node = ExtKeyDictionaryDefinition;
		using parent_node = SchemaDefinition;

		ExtKeyDictionaryDefinition(SchemaDefinition& schema, const Data::CollClass& klass);
	};

	class ExternalKeyDefinition : public KeyDefinition
	{
	public:
		using parent_node = ExtKeyDictionaryDefinition;

		ExternalKeyDefinition(SchemaDefinition& schema, ExtKeyDictionaryDefinition& dictionary, const Data::ExternalKey& key) : KeyDefinition(schema, dictionary, key) {}

		void WriteBody(std::ostream& output, const std::string& indent) override;
	};
}