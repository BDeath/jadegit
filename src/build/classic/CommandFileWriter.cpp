#include "CommandFileWriter.h"
#include <ostream>
#include <Platform.h>

using namespace std;

namespace JadeGit::Build::Classic
{
	CommandFileWriter::CommandFileWriter(Builder &builder, const Version& jadeVersion, function<void(void)> flush, bool latest) : 
		jadeVersion(jadeVersion), 
		flush(move(flush))
	{
		output = builder.AddCommandFile(latest);

		*output << "JadeCommandFile\n";
		*output << "JadeVersionNumber " << static_cast<string>(jadeVersion) << "\n";
		*output << "Commands\n";
	}

	CommandFileWriter::~CommandFileWriter()
	{
		*output << std::flush;
	}

	void CommandFileWriter::CreateLocale(const string& qualifiedName, const char* baseLocale)
	{
		*output << "Create Locale " << qualifiedName << " ";
		
		if (baseLocale)
			*output << "CloneOf " << baseLocale;
		else
			*output << "CopyFrom 0";
		
		*output << "\n";
	}

	void CommandFileWriter::Delete(const char* entityType, const string& qualifiedName)
	{
		// Determine if delete command is supported
		bool supported = true;
		string kind(entityType);

		// Not supported at all
		if (jadeVersion < jedi457)
		{
			if (kind == "ExposedList")
				supported = false;
		}
		
		// Not supported prior to JADE 2022 SP1
		if (jadeVersion < jade2022SP1)
		{
			if (kind == "Application" || kind == "Library")
				supported = false;
		}

		// Suppress unsupported commands
		// TODO: Ideally, an exception would be thrown here, but this needs to be handled for internal deployments
		// during development when switching branches, which could perhaps be achieved using manual deployment steps
		if (!supported)
			*output << "// NOT SUPPORTED: ";
		
		*output << "Delete " << entityType << " " << qualifiedName << "\n";	
	}

	void CommandFileWriter::Rename(const char* entityType, const string& qualifiedName, const string& newName)
	{
		// Determine if rename command is supported
		bool supported = true;
		string kind(entityType);

		// Not supported at all
		if (kind == "DbFile" || 
			kind == "GlobalConstantCategory" || 
			kind == "ExternalFunction" ||
			kind == "Library" ||
			kind == "Locale" ||
			kind == "LocaleFormat" ||
			kind == "Method" ||
			kind == "TranslatableString")
			supported = false;

		// Not supported prior to JADE 2022 SP1
		if (jadeVersion < jade2022SP1)
		{
			if (kind == "Application" || kind == "Package")
				supported = false;
		}

		// Where rename command isn't supported, try deleting instead
		if (!supported)
		{
			*output << "// NOT SUPPORTED: Rename " << entityType << " " << qualifiedName << " " << newName << "\n";
			Delete(entityType, qualifiedName);
		}
		else
			*output << "Rename " << entityType << " " << qualifiedName << " " << newName << "\n";
	}

	void CommandFileWriter::ModifySchema(const string& schemaName, const string& key, const string& value)
	{
		*output << "Modify Schema " << schemaName << " " << key << " " << value << "\n";
	}

	void CommandFileWriter::MoveClass(const string& qualifiedName, const string& newSuperclass)
	{
		*output << "Move Class " << qualifiedName << " " << newSuperclass << "\n";
	}

	void CommandFileWriter::Flush()
	{
		this->flush();
	}
}