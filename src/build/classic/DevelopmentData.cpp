#include "DataNode.h"
#include "SchemaDefinition.h"
#include <jadegit/data/Development.h>
#include <jadegit/data/Reference.h>
#include <jadegit/data/RootSchema.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	class DevControlPropertyData : public DataNode
	{
	public:
		DevControlPropertyData(const DevControlProperties& source) : DataNode(source, "PainterControlProperty") {}

	protected:
		void PrintAttributes(XMLPrinter& printer) const
		{
			auto& source = static_cast<const DevControlProperties&>(this->source);

			printer.PushAttribute("name", source.name.c_str());
		}
	};

	class DevControlTypesData : public DataNode
	{
	public:
		using parent_node = SchemaDefinition;

		DevControlTypesData(SchemaDefinition& schema, const Data::DevControlTypes& development) : DataNode(schema.data, development, "PainterControlDefinition") {}

	protected:
		void PrintAttributes(XMLPrinter& printer) const
		{
			auto& source = static_cast<const DevControlTypes&>(this->source);

			printer.PushAttribute("name", source.name.c_str());
		}

		bool PrintPropertyFilter(const RootSchema& rootSchema, const Property* property) const final
		{
			// Exclude parent reference
			if (property == rootSchema.devControlClass->guiClass)
				return false;

			return DataNode::PrintPropertyFilter(rootSchema, property);
		}

		void PrintData(XMLPrinter& printer) const
		{
			DataNode::PrintData(printer);

			printer.OpenElement("PainterControlProperties");

			auto& source = static_cast<const DevControlTypes&>(this->source);
			for (auto& prop : source.controlProps)
				DevControlPropertyData(*prop.second).Print(printer);

			printer.CloseElement();
		}
	};
	static NodeRegistration<DevControlTypesData, DevControlTypes> registrar;
}