#include "DataNode.h"
#include "SchemaDefinition.h"
#include <jadegit/data/Class.h>
#include <jadegit/data/RelationalView.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/RelationalAttributeMeta.h>
#include <jadegit/data/RootSchema/RelationalTableMeta.h>
#include <jadegit/data/RootSchema/RelationalViewMeta.h>
#include <xml/XMLPrinter.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	class RelationalViewData : public DataNode
	{
	public:
		using parent_node = SchemaDefinition;

		RelationalViewData(SchemaDefinition& schema, const RelationalView& view) : DataNode(schema.data, view) {}

		DataNodes tables;

	protected:
		void PrintAttributes(XMLPrinter& printer) const override
		{
			auto& source = static_cast<const RelationalView&>(this->source);

			printer.PushAttribute("name", source.name.c_str());
		}

		void PrintData(XMLPrinter& printer) const override
		{
			DataNode::PrintData(printer);

			printer.OpenElement("rpsClassMaps");
			printer.CloseElement();

			printer.OpenElement("tables");
			tables.Print(printer);
			printer.CloseElement();
		}

		void PrintValue(XMLPrinter& printer, const Data::Object* value) const override
		{
			if (auto klass = dynamic_cast<const Class*>(value->getParent()))
				printer.PushAttribute("className", klass->GetName().c_str());

			DataNode::PrintValue(printer, value);
		}
	};
	static NodeRegistration<RelationalViewData, RelationalView> relationalView;

	class RelationalTableData : public DataNode
	{
	public:
		using parent_node = RelationalViewData;

		RelationalTableData(RelationalViewData& view, const RelationalTable& table) : DataNode(view.tables, table) {}

		DataNodes attributes;

	protected:
		void PrintAttributes(XMLPrinter& printer) const override
		{
			auto& source = static_cast<const RelationalTable&>(this->source);

			printer.PushAttribute("name", source.name.c_str());
		}

		void PrintData(XMLPrinter& printer) const override
		{
			printer.OpenElement("rpsClassMap");
			printer.CloseElement();

			DataNode::PrintData(printer);

			printer.OpenElement("attributes");
			attributes.Print(printer);
			printer.CloseElement();

			printer.OpenElement("classes");		// Appears specific to RPS mappings?
			printer.CloseElement();
		}
	};
	static NodeRegistration<RelationalTableData, RelationalTable> relationalTable;

	class RelationalAttributeData : public DataNode
	{
	public:
		using parent_node = RelationalTableData;

		RelationalAttributeData(RelationalTableData& table, const RelationalAttribute& attribute) : DataNode(table.attributes, attribute) {}

	protected:
		void PrintAttributes(XMLPrinter& printer) const override
		{
			auto& source = static_cast<const RelationalAttribute&>(this->source);

			printer.PushAttribute("name", source.name.c_str());
		}
	};
	static NodeRegistration<RelationalAttributeData, RelationalAttribute> relationalAttribute;

	class RAMethodData : public RelationalAttributeData
	{
	public:
		using parent_node = RelationalTableData;

		using RelationalAttributeData::RelationalAttributeData;

	protected:
		void PrintData(XMLPrinter& printer) const override
		{
			printer.OpenElement("rpsClassMap");
			printer.CloseElement();

			RelationalAttributeData::PrintData(printer);
		}
	};
	static NodeRegistration<RAMethodData, RAMethod> raMethod;
}