#pragma once
#include "TypeDefinition.h"
#include <jadegit/data/JadeInterface.h>

namespace JadeGit::Build::Classic
{
	class JadeInterfaceDefinition : public TypeDefinition
	{
	public:
		JadeInterfaceDefinition(SchemaDefinition& schema, const Data::JadeInterface& interface);

	protected:
		void WriteTypeBody(std::ostream& output, const std::string& indent) const override;
	};
}