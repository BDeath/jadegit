#include "Graph.h"
#include "Task.h"
#include "TaskVisitor.h"

#include <jadegit/data/Application.h>
#include <jadegit/data/Attribute.h>
#include <jadegit/data/ExternalDatabase.h>
#include <jadegit/data/Function.h>
#include <jadegit/data/JadeWebServicesClass.h>
#include <jadegit/data/JadeWebServicesMethod.h>
#include <jadegit/data/Library.h>
#include <jadegit/data/PrimType.h>

#include "ActiveXLibraryDelta.h"
#include "CollClassDelta.h"
#include "ConstantCategoryDelta.h"
#include "DbFileDelta.h"
#include "FormDelta.h"
#include "GlobalConstantDelta.h"
#include "JadeExportedPackageDelta.h"
#include "JadeExposedListDelta.h"
#include "JadeHTMLDocumentDelta.h"
#include "JadeImportedPackageDelta.h"
#include "JadeInterfaceDelta.h"
#include "JadeInterfaceMethodDelta.h"
#include "LocaleDelta.h"
#include "LocaleFormatDelta.h"
#include "MethodDelta.h"
#include "PropertyDelta.h"
#include "ReferenceDelta.h"
#include "SchemaDelta.h"
#include "TranslatableStringDelta.h"

using namespace std;
using namespace JadeGit::Data;

namespace JadeGit::Build
{
	void Graph::Add(Entity* entity, bool latest)
	{
		if (!entity)
			return;

		this->latest = latest;
		entity->AcceptAll(*this);
	}

	bool Graph::analyze(IProgress* progress)
	{
		if (progress && !progress->start(deltas.size(), "Analyzing"))
			return false;

		std::stable_sort(deltas.begin(), deltas.end(), [this](const IDelta * a, const IDelta * b) { return a->Level() < b->Level(); });
		
		for (IDelta* delta : deltas)
		{
			delta->Analyze();

			if (progress && !progress->step())
				return false;
		}

		return !progress || progress->finish();
	}

	bool Graph::traverse(TaskVisitor& visitor, IProgress* progress, const char* task) const
	{
		if (progress && !progress->start(size, task))
			return false;

		// Sort direct child tasks
		auto children = this->children;
		stable_sort(children.begin(), children.end(), [](const Task* a, const Task* b) { return *a < *b; });

		size_t visited = 0;
		size_t before = 0;

		do
		{
			// Remember how many have been successfully processed
			before = visited;

			if (visited)
			{
				// Perform flush operation between traversals
				visitor.flush(false);

				// Reset task states ready for next iteration
				for (auto& task : children)
					task->reset();
			}

			// Traverse all tasks
			bool aborted = false;
			for (Task* task : children)
			{
				task->traverse(visited, visitor, aborted, progress);
				if (aborted)
					return false;
			}

		} while (visited < size && before < visited);

		// Throw exception when we've failed to visit everything
		if (size != visited)
			throw logic_error("Graph traversal failed");

		// Perform final flush operation
		visitor.flush(true);

		return !progress || progress->finish();
	}

	Graph::~Graph()
	{
		for (IDelta* delta : deltas)
			delete delta;

		for (Task* task : children)
			delete task;
	}

	void Graph::DuplicateCheck(const Entity* prior, const Entity* current) const
	{
		if (prior && prior != current)
			throw runtime_error(format("{} {} [{}] duplicates id already used by {} [{}]", latest ? "Latest" : "Previous", current->dataClass->name.c_str(), current->GetQualifiedName(), prior->dataClass->name.c_str(), prior->GetQualifiedName()));
	}

	void Graph::Visit(ActiveXAttribute* attribute)
	{
		Deltify<ActiveXComponentDelta<ActiveXAttribute>>(attribute);
	}

	void Graph::Visit(ActiveXClass* cls)
	{
		Deltify<ActiveXComponentDelta<ActiveXClass>>(cls);
	}

	void Graph::Visit(ActiveXConstant* constant)
	{
		Deltify<ActiveXComponentDelta<ActiveXConstant>>(constant);
	}

	void Graph::Visit(ActiveXLibrary* library)
	{
		Deltify<ActiveXLibraryDelta>(library);
	}

	void Graph::Visit(ActiveXMethod* method)
	{
		Deltify<ActiveXComponentDelta<ActiveXMethod>>(method);
	}

	void Graph::Visit(Application* application)
	{
		Deltify<SchemaComponentDelta<Application>>(application, "Application");
	}

	void Graph::Visit(Class* cls)
	{
		Deltify<ClassDelta<Class>>(cls);
	}

	void Graph::Visit(CollClass* cls)
	{
		Deltify<CollClassDelta<CollClass>>(cls);
	}

	void Graph::Visit(CompAttribute* attribute)
	{
		Deltify<PropertyDelta<CompAttribute>>(attribute);
	}

	void Graph::Visit(Constant* constant)
	{
		Deltify<ConstantDelta<>>(constant);
	}

	void Graph::Visit(Data::Control* control)
	{
		// Possibly unrequired, so Control doesn't need to be an entity?
	}

	void Graph::Visit(ConstantCategory* category)
	{
		Deltify<ConstantCategoryDelta>(category);
	}

	void Graph::Visit(CurrencyFormat* format)
	{
		Deltify<LocaleFormatDelta<CurrencyFormat>>(format);
	}

	void Graph::Visit(Database* database)
	{
		Deltify<SchemaComponentDelta<Database>>(database, "Database");
	}

	void Graph::Visit(DateFormat* format)
	{
		Deltify<LocaleFormatDelta<DateFormat>>(format);
	}

	void Graph::Visit(DbFile* dbFile)
	{
		Deltify<DbFileDelta>(dbFile);
	}

	void Graph::Visit(ExplicitInverseRef* reference)
	{
		Deltify<ExplicitInverseRefDelta>(reference);
	}

	void Graph::Visit(ExternalDatabase* database)
	{
		Deltify<SchemaComponentDelta<ExternalDatabase>>(database, "ExternalDatabase");
	}

	void Graph::Visit(ExternalMethod* method)
	{
		Deltify<MethodDelta<>>(method);
	}

	void Graph::Visit(Form* form)
	{
		Deltify<FormDelta>(form);
	}

	void Graph::Visit(Function* function)
	{
		Deltify<SchemaComponentDelta<Function>>(function, "ExternalFunction");
	}

	void Graph::Visit(GlobalConstant* constant)
	{
		Deltify<GlobalConstantDelta>(constant);
	}

	void Graph::Visit(ImplicitInverseRef* reference)
	{
		Deltify<ImplicitInverseRefDelta>(reference);
	}

	void Graph::Visit(JadeExportedClass* cls)
	{
		Deltify<JadeExportedClassDelta>(cls);
	}

	void Graph::Visit(JadeExportedConstant* constant)
	{
		Deltify<JadeExportedConstantDelta>(constant);
	}

	void Graph::Visit(JadeExportedInterface* interface)
	{
		Deltify<JadeExportedInterfaceDelta>(interface);
	}

	void Graph::Visit(JadeExportedMethod* method)
	{
		Deltify<JadeExportedMethodDelta>(method);
	}

	void Graph::Visit(JadeExportedPackage* package)
	{
		Deltify<JadeExportedPackageDelta>(package);
	}

	void Graph::Visit(JadeExportedProperty* property)
	{
		Deltify<JadeExportedPropertyDelta>(property);
	}

	void Graph::Visit(JadeExposedClass* cls)
	{
		Deltify<JadeExposedClassDelta>(cls);
	}

	void Graph::Visit(JadeExposedFeature* feature)
	{
		Deltify<JadeExposedFeatureDelta>(feature);
	}

	void Graph::Visit(JadeExposedList* list)
	{
		Deltify<JadeExposedListDelta>(list);
	}

	void Graph::Visit(JadeHTMLDocument* document)
	{
		Deltify<JadeHTMLDocumentDelta>(document);
	}

	void Graph::Visit(JadeImportedClass* cls)
	{
		Deltify<JadeImportedClassDelta>(cls);
	}

	void Graph::Visit(JadeImportedConstant* constant)
	{
		Deltify<JadeImportedFeatureDelta<JadeImportedConstant>>(constant);
	}

	void Graph::Visit(JadeImportedInterface* interface)
	{
		Deltify<JadeImportedInterfaceDelta>(interface);
	}

	void Graph::Visit(JadeImportedMethod* method)
	{
		Deltify<JadeImportedFeatureDelta<JadeImportedMethod>>(method);
	}

	void Graph::Visit(JadeImportedPackage* package)
	{
		Deltify<JadeImportedPackageDelta>(package);
	}

	void Graph::Visit(JadeImportedProperty* property)
	{
		Deltify<JadeImportedFeatureDelta<JadeImportedProperty>>(property);
	}

	void Graph::Visit(JadeInterface* interface)
	{
		Deltify<JadeInterfaceDelta<JadeInterface>>(interface);
	}

	void Graph::Visit(JadeInterfaceMethod* method)
	{
		Deltify<JadeInterfaceMethodDelta>(method);
	}

	void Graph::Visit(JadeMethod* method)
	{
		Deltify<MethodDelta<>>(method);
	}

	void Graph::Visit(JadeWebServicesClass* cls)
	{
		Deltify<ClassDelta<JadeWebServicesClass>>(cls);
	}

	void Graph::Visit(JadeWebServicesMethod* method)
	{
		Deltify<MethodDelta<>>(method);
	}

	void Graph::Visit(JadeWebServiceConsumerClass* cls)
	{
		Deltify<ClassDelta<JadeWebServiceConsumerClass>>(cls);
	}

	void Graph::Visit(JadeWebServiceProviderClass* cls)
	{
		Deltify<ClassDelta<JadeWebServiceProviderClass>>(cls);
	}

	void Graph::Visit(JadeWebServiceSoapHeaderClass* cls)
	{
		Deltify<ClassDelta<JadeWebServiceSoapHeaderClass>>(cls);
	}

	void Graph::Visit(Library* library)
	{
		Deltify<SchemaComponentDelta<Library>>(library, "Library");
	}

	void Graph::Visit(Locale* locale)
	{
		Deltify<LocaleDelta>(locale);
	}

	void Graph::Visit(NumberFormat* format)
	{
		Deltify<LocaleFormatDelta<NumberFormat>>(format);
	}

	void Graph::Visit(PrimAttribute* attribute)
	{
		Deltify<PropertyDelta<PrimAttribute>>(attribute);
	}

	void Graph::Visit(Data::PrimType* primitive)
	{
		// Don't currently need to handle primitive types explicitly, as these are all subschema copies
		// that cannot be renamed, moved or deleted.  Any method changes are handled individually.
	}

	void Graph::Visit(Data::PseudoType* pseudoType)
	{
		// Not applicable in this context (we can't define our own custom pseudo types)
	}

	void Graph::Visit(RelationalAttribute* attribute)
	{
		Deltify<Delta<RelationalAttribute>>(attribute);
	}

	void Graph::Visit(RelationalTable* table)
	{
		Deltify<Delta<RelationalTable>>(table);
	}

	void Graph::Visit(RelationalView* view)
	{
		Deltify<SchemaComponentDelta<RelationalView>>(view, "RelationalView");
	}

	void Graph::Visit(Schema* schema)
	{
		Deltify<SchemaDelta>(schema);
	}

	void Graph::Visit(TimeFormat* format)
	{
		Deltify<LocaleFormatDelta<TimeFormat>>(format);
	}

	void Graph::Visit(TranslatableString* string)
	{
		Deltify<TranslatableStringDelta>(string);
	}
}