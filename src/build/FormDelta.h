#pragma once
#include "ClassDelta.h"
#include <jadegit/data/Form.h>

namespace JadeGit::Build
{
	class FormDelta : public Delta<Form>
	{
	public:
		using Delta::Delta;

	protected:
		bool AnalyzeEnter() override
		{
			// Basic analysis
			if (!Delta::AnalyzeEnter())
			{
				// Forms are deleted implicitly when class or locale are deleted
				return false;
			}

			auto definition = GetDefinition();

			// Dependent on super form definition
			const Form* superForm = latest->GetSuperForm();
			while (superForm)
			{
				if (auto delta = graph.Analyze<FormDelta>(superForm))
				{
					definition->addPredecessor(delta->GetDefinition());
					break;
				}

				superForm = superForm->GetSuperForm();
			}

			// Dependent on control class definitions
			for (auto& control : latest->controlList)
			{
				if (!control->dataClass->isStatic())
					this->addDefinitionDependency(control->dataClass, definition);
			}

			// Dependent on form class definition
			this->addDefinitionDependency(latest->GetClass(), definition);

			return true;
		}

		Task* HandleDeletion(Task* parent) override
		{
			// Forms are deleted implicitly when class or locale are deleted

			// TODO: Resolve relevant deletion task, which may become a prerequite to control class changes

			return nullptr;
		}
	};
}