#pragma once

namespace JadeGit::Data
{
	class ActiveXLibrary;
	class Application;
	class Class;
	class ExternalDatabase;
	class JadeExposedClass;
	class JadeExposedFeature;
	class JadeExposedList;
	class JadeHTMLDocument;
	class JadeInterfaceMapping;
	class Form;
	class RelationalAttribute;
	class RelationalTable;
	class RelationalView;
	class Schema;
	class SchemaEntity;
}

namespace JadeGit::Build
{
	class ExtractStrategy
	{
	public:
		virtual bool Declare(const Data::Class& klass) = 0;

		virtual bool Define(const Data::ActiveXLibrary& library) = 0;
		virtual bool Define(const Data::Application& application) = 0;
		virtual bool Define(const Data::ExternalDatabase& database) = 0;
		virtual bool Define(const Data::JadeExposedClass& cls) = 0;
		virtual bool Define(const Data::JadeExposedFeature& feature) = 0;
		virtual bool Define(const Data::JadeExposedList& list) = 0;
		virtual bool Define(const Data::JadeHTMLDocument& document, const char* old_name = nullptr) = 0;
		virtual bool Define(const Data::JadeInterfaceMapping& mapping) = 0;
		virtual bool Define(const Data::Form& form) = 0;
		virtual bool Define(const Data::RelationalAttribute& attribute) = 0;
		virtual bool Define(const Data::RelationalTable& table) = 0;
		virtual bool Define(const Data::RelationalView& view) = 0;
		virtual bool Define(const Data::Schema& schema, bool complete, bool modified, bool had_text) = 0;
		virtual bool Define(const Data::SchemaEntity& entity, bool complete, bool modified, bool had_text) = 0;
	};

	template <class TEntity>
	concept Declarable = requires (ExtractStrategy& strategy, const TEntity& entity) {
		strategy.Declare(entity);
	};

	template <class TEntity>
	concept PartiallyDefinable = requires (ExtractStrategy& strategy, const TEntity& entity, bool complete, bool modified, bool had_text) {
		strategy.Define(entity, complete, modified, had_text);
	};

	template <class TEntity>
	concept Definable = PartiallyDefinable<TEntity> || requires (ExtractStrategy& strategy, const TEntity& entity) {
		strategy.Define(entity);
	};
}