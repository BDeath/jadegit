#pragma once
#include "ExtractTask.h"

namespace JadeGit::Build
{
	template<Declarable TEntity>
	class ExtractDeclarationTask : public ExtractTask
	{
	public:
		ExtractDeclarationTask(Graph& graph, Task* parent, const TEntity& entity) : ExtractTask(graph, parent), entity(entity) {};

		operator std::string() const override
		{
			return std::format("{} {} declaration", entity.dataClass->name.c_str(), entity.GetQualifiedName());
		}

		bool execute(ExtractStrategy& strategy) const final
		{
			return strategy.Declare(entity);
		}

	private:
		const TEntity& entity;
	};
}