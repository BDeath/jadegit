#pragma once
#include "TypeDelta.h"
#include <jadegit/data/Feature.h>

namespace JadeGit::Build
{
	class IFeatureDelta : public ISchemaComponentDelta
	{
	public:
		using ISchemaComponentDelta::ISchemaComponentDelta;

		// No feature specific commands (as yet)
	};

	template<class TComponent, class TInterface = IFeatureDelta>
	class FeatureDelta : public SchemaComponentDelta<TComponent, TInterface>
	{
	public:
		using SchemaComponentDelta<TComponent, TInterface>::SchemaComponentDelta;
		using SchemaComponentDelta<TComponent, TInterface>::graph;
		using SchemaComponentDelta<TComponent, TInterface>::previous;
		using SchemaComponentDelta<TComponent, TInterface>::latest;
		using SchemaComponentDelta<TComponent, TInterface>::GetDefinition;

		// TODO: Moving classes is dependent on features first being renamed or deleted to prevent conflicts with features in hierarchy to which they're being moved 
	
		bool AnalyzeEnter() override
		{
			// Basic analysis
			if (!SchemaComponentDelta<TComponent, TInterface>::AnalyzeEnter())
			{
				auto deletion = this->GetDeletion();

				// Handle deleting before type deletion
				if (previous->type)
				{
					this->addDeletionDependency(previous->type->getRootType(), deletion);

					// Need to explicitly delete references to interfaces before class deletion
					if (previous->type->isInterface() && previous->GetRootSchema().version < par70018)
					{
						if (auto delta = graph.Analyze<IDelta>(previous->schemaType))
							delta->GetDeletion()->addPredecessor(deletion);
					}
				}

				// Handle deleting subschema copy features before deleting root superschema type
				if (previous->schemaType && !previous->schemaType->isRootType())
					this->addDeletionDependency(previous->schemaType->getRootType(), deletion);

				return false;
			}

			if (auto definition = GetDefinition())
			{
				// Definition depends on latest type
				if (latest->type)
					this->addCreationDependency(latest->type->getRootType(), definition);
					
				// Deleting previous type depends on update to latest type
				if (previous && previous->type)
					this->addDeletionDependency(previous->type->getRootType(), definition, previous->type->isInterface() && previous->GetRootSchema().version < par70018);
			}

			return true;
		}
	};
}