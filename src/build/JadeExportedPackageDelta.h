#pragma once
#include "Delta.h"
#include <jadegit/data/JadeExportedPackage.h>
#include <Platform.h>

namespace JadeGit::Build
{
	class JadeExportedPackageDelta : public SchemaComponentDelta<JadeExportedPackage>
	{
	public:
		JadeExportedPackageDelta(Graph& graph) : SchemaComponentDelta(graph, "Package") {}

		Task* GetCreation() const final
		{
			// Declaration task establishes new package
			if (declaration)
				return declaration;

			return SchemaComponentDelta::GetCreation();
		}

	protected:
		bool AnalyzeEnter() override
		{
			// Basic analysis
			if (!SchemaComponentDelta::AnalyzeEnter())
			{
				// Handle deleting package before application
				this->addDeletionDependency(previous->application, GetDeletion());

				return false;
			}

			// Definition depends on latest application
			this->addCreationDependency(latest->application, GetDefinition());

			// Deleting previous application type depends on update to latest application
			if (previous)
				this->addDeletionDependency(previous->application, GetDefinition());

			return true;
		}

		Task* handleDefinition(Task* parent) final
		{
			auto definition = SchemaComponentDelta::handleDefinition(parent);

			// Use partial definition as package declaration ahead of complete definition
			if (!previous)
				declaration = new ExtractDefinitionTask<JadeExportedPackage>(graph, definition, *latest, false, true, false);

			return definition;
		}

	private:
		Task* declaration = nullptr;

		bool requiresCompleteDefinition(const Task* parent) const final
		{
			if (SchemaComponentDelta::requiresCompleteDefinition(parent))
				return true;

			// Determine if child types has been removed or renamed, for which a complete definition is needed
			for (auto& child : previous->children)
			{
				auto entity = child->asEntity();
				if (!entity || entity->isMajor())
					continue;

				if (auto delta = graph.Find<IDelta>(entity))
				{
					auto latest = delta->getLatest();
					if (!latest || entity->GetName() != latest->GetName())
						return true;
				}

				// Check child features
				for (auto& grandchild : entity->children)
				{
					if (auto feature = grandchild->asEntity())
					{
						if (auto delta = graph.Find<IDelta>(feature))
						{
							auto latest = delta->getLatest();
							if (!latest || feature->GetName() != latest->GetName())
								return true;
						}
					}
				}
			}

			return false;
		}
	};

	template<class TComponent>
	class JadeExportedEntityDelta : public SchemaComponentDelta<TComponent>
	{
	public:
		JadeExportedEntityDelta(Graph& graph, const char* entityType) : SchemaComponentDelta<TComponent>(graph, entityType) {}

		using SchemaComponentDelta<TComponent>::previous;
		using SchemaComponentDelta<TComponent>::latest;

	protected:
		using SchemaComponentDelta<TComponent>::graph;
		using SchemaComponentDelta<TComponent>::GetDefinition;
		using SchemaComponentDelta<TComponent>::GetDeletion;

		Task* HandleDeletion(Task* parent) override
		{
			// Exported entities cannot be deleted explicitly prior to JADE 2022 SP2, but can be implicitily deleted by complete parent definition
			if (previous->GetRootSchema().version < jade2022SP2)
			{
				assert(parent && parent->complete);
				return parent;
			}

			return SchemaComponentDelta<TComponent>::HandleDeletion(parent);
		}

		bool AnalyzeEnter() override
		{
			// Basic analysis
			if (!SchemaComponentDelta<TComponent>::AnalyzeEnter())
			{
				// Original entity cannot be deleted until exported entity is
				this->addDeletionDependency(previous->getOriginal(), GetDeletion(), previous->GetRootSchema().version < par66557);

				return false;
			}

			// Exported entity definition is dependent on original entity being created/renamed
			this->addCreationDependency(latest->getOriginal(), GetDefinition());
			
			return true;
		}
	};

	template<class TComponent>
	class JadeExportedTypeDelta : public JadeExportedEntityDelta<TComponent>
	{
	public:
		JadeExportedTypeDelta(Graph& graph) : JadeExportedEntityDelta<TComponent>(graph, "ExportedType") {}

	};

	using JadeExportedClassDelta = JadeExportedTypeDelta<JadeExportedClass>;
	using JadeExportedInterfaceDelta = JadeExportedTypeDelta<JadeExportedInterface>;

	template<class TComponent>
	class JadeExportedFeatureDelta : public JadeExportedEntityDelta<TComponent>
	{
	public:
		JadeExportedFeatureDelta(Graph& graph) : JadeExportedEntityDelta<TComponent>(graph, "ExportedFeature") {}

	};

	using JadeExportedConstantDelta = JadeExportedFeatureDelta<JadeExportedConstant>;
	using JadeExportedMethodDelta = JadeExportedFeatureDelta<JadeExportedMethod>;
	using JadeExportedPropertyDelta = JadeExportedFeatureDelta<JadeExportedProperty>;
}