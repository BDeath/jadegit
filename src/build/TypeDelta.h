#pragma once
#include "SchemaComponentDelta.h"

namespace JadeGit::Build
{
	class ITypeDelta : public ISchemaComponentDelta
	{
	public:
		using ISchemaComponentDelta::ISchemaComponentDelta;

		virtual Task* GetDeclaration() const { return GetDefinition(); }
	};

	template<class TEntity, class TInterface = ITypeDelta>
	class TypeDelta : public SchemaComponentDelta<TEntity, TInterface>
	{
	public:
		using SchemaComponentDelta<TEntity, TInterface>::SchemaComponentDelta;

	protected:
		using SchemaComponentDelta<TEntity, TInterface>::graph;
		using SchemaComponentDelta<TEntity, TInterface>::previous;
		using SchemaComponentDelta<TEntity, TInterface>::latest;
		using SchemaComponentDelta<TEntity, TInterface>::GetDeletion;

		bool AnalyzeEnter() override
		{
			// Basic analysis
			if (!SchemaComponentDelta<TEntity, TInterface>::AnalyzeEnter())
			{
				return false;
			}

			// Handle defining subschema copies after root type
			if (latest->isSubschemaCopy())
			{
				if (auto delta = graph.Analyze<ITypeDelta>(&latest->getRootType()))
					this->GetDefinition()->addPredecessor(delta->GetCreation(), true);
			}

			return true;
		}

		Task* handleDefinition(Task* parent) override
		{
			// Not applicable to inferred subschema copies, except when previous wasn't inferred
			if (latest->isInferred() && latest->isSubschemaCopy() && (!previous || previous->isInferred()))
				return nullptr;

			return SchemaComponentDelta<TEntity, TInterface>::handleDefinition(parent);
		}

		bool requiresCompleteDefinition(const Task* parent) const override
		{
			if (SchemaComponentDelta<TEntity, TInterface>::requiresCompleteDefinition(parent))
				return true;

			// Determine if child entity has been removed or renamed, for which a complete definition is needed
			for (auto& child : previous->children)
			{
				auto entity = child->asEntity();
				if (!entity || entity->isMajor())
					continue;

				if (auto delta = graph.Find<IDelta>(entity))
				{
					auto latest = delta->getLatest();
					if (!latest || entity->GetName() != latest->GetName())
						return true;
				}
			}

			return false;
		}
	};
}