#pragma once
#include "SchemaComponentDelta.h"
#include <jadegit/data/JadeExposedList.h>

namespace JadeGit::Build
{
	class JadeExposedListDelta : public SchemaComponentDelta<JadeExposedList>
	{
	public:
		JadeExposedListDelta(Graph& graph) : SchemaComponentDelta(graph, "ExposedList") {}
	};

	template<class TComponent>
	class JadeExposedEntityDelta : public Delta<TComponent>
	{
	public:
		using Delta<TComponent>::Delta;

		using Delta<TComponent>::previous;
		using Delta<TComponent>::latest;

	protected:
		using Delta<TComponent>::GetDefinition;
		using Delta<TComponent>::GetDeletion;

		bool AnalyzeEnter() final
		{
			// Basic analysis
			if (!Delta<TComponent>::AnalyzeEnter())
			{
				// Related entity cannot be deleted until exposure is
				this->addDeletionDependency(previous->getOriginal(), GetDeletion());

				return false;
			}

			// Exposure definition depends on related entity being created/renamed
			this->addCreationDependency(latest->getOriginal(), GetDefinition());

			return true;
		}

		Task* HandleDeletion(Task* parent)
		{
			// Deletion implied by parent update or deletion
			assert(parent);
			return parent;
		}
	};

	using JadeExposedClassDelta = JadeExposedEntityDelta<JadeExposedClass>;
	using JadeExposedFeatureDelta = JadeExposedEntityDelta<JadeExposedFeature>;
}