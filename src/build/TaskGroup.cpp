#include "TaskGroup.h"
#include "Graph.h"
#include <jadegit/data/Entity.h>

using namespace std;
using namespace JadeGit::Data;

namespace JadeGit::Build
{
	TaskGroup* TaskGroup::make(Graph& graph, Task* parent, const Data::Entity* entity)
	{
		if (!entity)
			return nullptr;

		if (TaskGroup* task = graph.groups[entity])
		{
			assert(task->parent == parent);
			return task;
		}

		return graph.groups[entity] = new TaskGroup(graph, parent, *entity);
	}

	TaskGroup::TaskGroup(Graph& graph, Task* parent, const Data::Entity& entity) : Task(graph, parent), entity(entity)
	{
	}

	TaskGroup::operator string() const
	{
		return format("{} {}", entity.dataClass->name.c_str(), entity.GetQualifiedName());
	}
}