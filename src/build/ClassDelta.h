#pragma once
#include "TypeDelta.h"
#include "ExtractDeclarationTask.h"
#include "MoveClassTask.h"
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/FormMeta.h>
#include <map>
#include <stack>

using namespace JadeGit::Data;

namespace JadeGit::Build
{
	class IClassDelta : public ITypeDelta
	{
	public:
		using ITypeDelta::ITypeDelta;

		const Data::Class* getPrevious() const override = 0;
		const Data::Class* getLatest() const override = 0;

		virtual Task* getMove() const = 0;
		virtual Task* GetInterfaceMappingDefinition(const JadeInterface* interface) const = 0;
	};

	template<class TComponent, class TInterface = IClassDelta>
	class ClassDelta : public TypeDelta<TComponent, TInterface>
	{
	public:
		ClassDelta(Graph& graph) : TypeDelta<TComponent, TInterface>(graph, "Class") {}

		using TypeDelta<TComponent, TInterface>::previous;
		using TypeDelta<TComponent, TInterface>::latest;

	protected:
		using TypeDelta<TComponent, TInterface>::graph;
		using TypeDelta<TComponent, TInterface>::GetRename;

		Task* GetCreation() const override
		{
			// Declaration task establishes the new class
			if (declaration)
				return declaration;

			return TypeDelta<TComponent, TInterface>::GetCreation();
		}

		Task* GetDeclaration() const final
		{
			return declaration;
		}

		Task* getMove() const final
		{
			return move;
		}

		bool AnalyzeEnter() override
		{
			// Basic analysis
			if (!TypeDelta<TComponent, TInterface>::AnalyzeEnter())
			{
				auto deletion = this->GetDeletion();

				// Handle deleting after subclass moves (or deletions)
				handleMovingSubclassesBefore(deletion);

				// Handle deletion before previous interfaces
				for (auto& interface : previous->implementedInterfaces)
					this->addDeletionDependency(interface->getRootType(), deletion, interface->isSubschemaCopy() && previous->GetRootSchema().version < par70019);

				return false;
			}

			return true;
		}
		
		Task* handleDefinition(Task* parent) override
		{
			// Build list of deltas for interface implementations
			std::map<EntityKey, std::pair<JadeInterface*, JadeInterface*>> interfaceDeltas;

			for (auto& interface : latest->implementedInterfaces)
				interfaceDeltas[interface->getRootType().getKey()].second = interface;
			
			if (previous)
			{
				for (auto& interface : previous->implementedInterfaces)
					interfaceDeltas[interface->getRootType().getKey()].first = interface;

				// Determine if interface is being de-implemented
				for (auto& delta : interfaceDeltas)
				{
					// Ignore deltas where previous interface isn't being removed from class
					if (!delta.second.first || delta.second.second)
						continue;

					// Interface is being de-implemented, for which we need to load a complete definition
					interfaceDeimplemented = true;
					break;
				}
			}

			auto definition = TypeDelta<TComponent, TInterface>::handleDefinition(parent);

			// Analyze latest base class (closest superschema type or ancestor that's being updated)
			const Class* baseClass = latest->superclass ? latest->superclass : static_cast<const Class*>(static_cast<const Type*>(latest->superschemaType));
			while (baseClass && !baseClass->isStatic())
			{
				if (latestBaseClass = graph.Analyze<IClassDelta>(baseClass))
					break;

				baseClass = baseClass->superclass ? baseClass->superclass : static_cast<const Class*>(static_cast<const Type*>(baseClass->superschemaType));
			}

			// Resolve super class root types
			const Class* previousSuperClass = previous && previous->superclass ? &previous->superclass->getRootType() : nullptr;
			const Class* latestSuperClass = latest && latest->superclass ? &latest->superclass->getRootType() : nullptr;

			// Handle move
			if (previous && previousSuperClass && latestSuperClass)
			{
				if (!this->match(previousSuperClass, latestSuperClass))
				{
					move = new MoveClassTask(graph, this->QualifiedName(), latestSuperClass->GetName());
					move->addPredecessor(GetRename());

					// Handle move after latest superclass has been created, and subsequently renamed and/or moved
					if (auto delta = graph.Analyze<IClassDelta>(latestSuperClass))
					{
						move->addPredecessor(delta->GetCreation());
						move->addPredecessor(delta->getMove());
					}

					// Handle moving after subclass moves (or deletions)
					handleMovingSubclassesBefore(move);
				}
				else if (auto delta = graph.Analyze<IClassDelta>(latestSuperClass))
				{
					move = delta->getMove();
				}
			}

			// Handle forward declaration
			if (requiresDeclaration(parent))
			{
				declaration = new ExtractDeclarationTask<TComponent>(graph, parent, *latest);
				declaration->addPredecessor(GetRename());
				declaration->addPredecessor(move);
				definition->addPredecessor(declaration, true);	// Common predecessor inherited by child definition tasks

				if (auto delta = graph.Analyze<IClassDelta>(latestSuperClass))
				{
					declaration->addPredecessor(delta->GetCreation());
					declaration->addPredecessor(delta->GetDeclaration());
				}
			}

			// Handle interface mappings
			for (auto& delta : interfaceDeltas)
			{
				JadeInterface* previous = delta.second.first;
				JadeInterface* latest = delta.second.second;

				if (latest)
				{
					// Setup definition task for interface mappings
					interfaceMappingDefinitions[&latest->getRootType()] = new ExtractDefinitionTask<JadeInterfaceMapping>(graph, definition, this->latest->GetInterfaceMapping(latest));
					
					// Definition is dependent on defining interface
					this->addDefinitionDependency(latest->getRootType(), definition);
				}
				// Deleting previous interface is dependent on de-implementation
				else if (previous)
				{
					this->addDeletionDependency(previous->getRootType(), definition, previous->isSubschemaCopy() && this->latest->GetRootSchema().version < par70019);
				}
			}

			// Handle super-interface mapping dependencies
			for (auto& it : interfaceMappingDefinitions)
			{
				for (auto& superInterface : it.first->superinterfaces)
					it.second->addPredecessor(GetInterfaceMappingDefinition(&superInterface->getRootType()));
			}

			// Definition is dependent on defining map files
			for (auto& classMap : latest->classMapRefs)
				this->addDefinitionDependency(classMap->diskFile, declaration ? declaration : definition);

			return definition;
		}
		
		// Determine if complete definition is required
		bool requiresCompleteDefinition(const Task* parent) const override
		{
			return interfaceDeimplemented || TypeDelta<TComponent, TInterface>::requiresCompleteDefinition(parent);
		}
		
		// Determine if forward declaration is required
		virtual bool requiresDeclaration(const Task* parent) const
		{
			// Not applicable to subschema copies
			if (latest->isSubschemaCopy())
				return false;

			// Required for complete definitions
			if (parent && parent->complete)
				return true;

			// Required on creation
			if (!previous)
				return true;

			// Required on move
			if (move)
				return true;

			// Required on rename
			if (previous->name != latest->name)
				return true;

			// Required if class header details are changing
			return previous->access != latest->access ||
				previous->abstract != latest->abstract ||
				previous->subAccess != latest->subAccess ||
				previous->final != latest->final ||
				previous->subschemaFinal != latest->subschemaFinal ||
				previous->persistentAllowed != latest->persistentAllowed ||
				previous->sharedTransientAllowed != latest->sharedTransientAllowed ||
				previous->transient != latest->transient ||
				previous->transientAllowed != latest->transientAllowed ||
				previous->subclassPersistentAllowed != latest->subclassPersistentAllowed ||
				previous->subclassSharedTransientAllowed != latest->subclassSharedTransientAllowed ||
				previous->subclassTransientAllowed != latest->subclassTransientAllowed;
		}

	private:
		ExtractDeclarationTask<TComponent>* declaration = nullptr;
		Task* move = nullptr;
		bool interfaceDeimplemented = false;
		std::map<const JadeInterface*, Task*> interfaceMappingDefinitions;
		const IClassDelta* latestBaseClass = nullptr;

		virtual Task* GetInterfaceMappingDefinition(const JadeInterface* interface) const override final
		{
			// Check locally
			auto iter = interfaceMappingDefinitions.find(interface);
			if (iter != interfaceMappingDefinitions.end())
				return iter->second;

			// Check base class
			return latestBaseClass ? latestBaseClass->GetInterfaceMappingDefinition(interface) : nullptr;
		}

		void handleMovingSubclassesBefore(Task* dependent)
		{
			std::stack<const Class*> stack;
			stack.push(previous);

			while (!stack.empty())
			{
				auto copy = stack.top();
				
				for (auto subclass : copy->subclasses)
				{
					if (const IClassDelta* delta = graph.Analyze<IClassDelta>(subclass))
					{
						dependent->addPredecessor(delta->GetDeletion());
						dependent->addPredecessor(delta->getMove());
					}
				}

				stack.pop();
				for (auto type : copy->subschemaTypes)
					stack.push(static_cast<Class*>(type));
			}
		}
	};
}