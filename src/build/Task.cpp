#include "Task.h"
#include "BuildTask.h"
#include "CommandTask.h"
#include "Graph.h"
#include "ReorgTask.h"
#include "TaskState.h"
#include "TaskVisitor.h"
#include <stack>
#include <queue>

using namespace std;

namespace JadeGit::Build
{
	Task::Task(Graph& graph, bool complete, short priority) : Task(graph, nullptr, complete, priority)
	{
	}

	Task::Task(Graph& graph, Task& parent, bool complete, short priority) : Task(graph, &parent, complete, priority)
	{
	}

	Task::Task(Graph& graph, Task* parent, bool complete, short priority) : graph(graph), parent(parent), complete(complete), priority(priority)
	{
		// Set initial state
		state = TaskState::initialState();
		
		// Add to parent
		if (parent)
			parent->children.push_back(this);
		else
			graph.children.push_back(this);

		// Increment graph size
		graph.size++;
	}

	Task::~Task()
	{
		for (const Task* task : children)
			delete task;
	}

	bool Task::operator<(const Task& rhs) const
	{
		return priority < rhs.priority;		
	}

	void Task::addCommonPredecessor(Task& predecessor)
	{
		if (!this || &predecessor == this)
			return;

		predecessors_common.push_back(&predecessor);
	}

	void Task::addPeer(Task& peer)
	{
		if (!this || &peer == this)
			return;

		assert(!peer.isChildOf(*this));
		assert(!isChildOf(peer));
		assert(!peer.isSuccessorOf(*this));
		assert(!isSuccessorOf(peer));

		// Save current peers
		auto current = peers;

		// Add peers of peer
		for (auto other : peer.peers)
		{
			assert(other != this);
			peers.push_back(other);
			const_cast<Task*>(other)->peers.push_back(this);
		}

		// Add current peers to new peer
		for (auto other : current)
		{
			assert(other != &peer);
			peer.peers.push_back(other);
			const_cast<Task*>(other)->peers.push_back(&peer);
		}

		// Finally add each other to complete the group
		peers.push_back(&peer);
		peer.peers.push_back(this);
	}

	void Task::addPredecessor(Task& predecessor, bool common, bool needsReorg)
	{
		if (!this || &predecessor == this)
			return;

		// Add predecessor with reorg task if needed
		if (needsReorg)
		{
			auto reorg = new ReorgTask(graph);
			reorg->addPredecessor(predecessor);
			return addPredecessor(*reorg, common);
		}

		assert(!predecessor.isSuccessorOf(*this));

		predecessors.push_back(&predecessor);
		predecessor.successors.push_back(this);

		// Add common predecessor (which are inherited by children)
		if (common)
			addCommonPredecessor(predecessor);
	}

	bool Task::isChildOf(const Task& parent) const
	{
		const Task* child = this;
		while (child)
		{
			if (child->parent == &parent)
				return true;

			child = child->parent;
		}

		return false;
	}

	bool Task::isChildOf(const std::vector<const Task*>& parents) const
	{
		for (auto& parent : parents)
			if (isChildOf(parent))
				return true;

		return false;
	}
	
	bool Task::isPeerOf(const Task& peer) const
	{
		if (&peer == this)
			return true;

		return find(peers.begin(), peers.end(), &peer) != peers.end();
	}

	bool Task::isSuccessorOf(const Task& predecessor) const
	{
		if (&predecessor == this)
			return false;

		vector<const Task*> group;
		group.push_back(this);

		for (auto& peer : peers)
		{
			if (&predecessor == peer)
				return false;

			group.push_back(peer);
		}

		for (auto& peer : group)
		{
			if (find(peer->predecessors.begin(), peer->predecessors.end(), &predecessor) != peer->predecessors.end())
				return true;

			for (auto& task : peer->predecessors)
			{
				if (task->isSuccessorOf(predecessor))
					return true;
			}
		}

		return false;
	}

	vector<const Task*> Task::prerequisites(const Task* cascade) const
	{
		vector<const Task*> prerequisites;

		// Add common predecessors inherited from parent tasks
		if (!cascade)
		{
			stack<const Task*> stack;
			const Task* parent = this->parent;
			while (parent)
			{
				stack.push(parent);
				parent = parent->parent;
			}

			while (!stack.empty())
			{
				parent = stack.top();
				stack.pop();

				// Add common predecessors, ignoring any peers or siblings which are already processing (mutual dependency)
				for (auto& predecessor : parent->predecessors_common)
					if (!isPeerOf(predecessor) && (!predecessor->state->isProcessing() || !predecessor->isChildOf(parent)))
					{
						assert(!predecessor->state->isProcessing());
						prerequisites.push_back(predecessor);
					}
			}
		}

		// Add common predecessors inherited from parents of peer tasks, which are not a parent of this task
		for (auto& peer : peers)
		{
			if (!cascade || !peer->isChildOf(cascade))
			{
				stack<const Task*> stack;
				const Task* parent = peer->parent;
				while (parent && !isChildOf(parent))
				{
					stack.push(parent);
					parent = parent->parent;
				}

				while (!stack.empty())
				{
					parent = stack.top();
					stack.pop();

					// Add common predecessors, ignoring any peers or siblings which are already processing (mutual dependency)
					for (auto& predecessor : parent->predecessors_common)
						if (!isPeerOf(predecessor) && (!predecessor->state->isProcessing() || !predecessor->isChildOf(parent)))
						{
							assert(!predecessor->state->isProcessing());
							prerequisites.push_back(predecessor);
						}
				}
			}
		}

		// Add direct predecessors
		for (auto& predecessor : predecessors)
			prerequisites.push_back(predecessor);

		// Add direct predecessors of peer tasks
		for (auto& peer : peers)
		{
			for (auto& predecessor : peer->predecessors)
				prerequisites.push_back(predecessor);
		}

		// Add child predecessors, which are not children of this or any peer
		queue<const Task*> queue;
		for (auto& child : children)
			queue.push(child);

		for (auto& peer : peers)
			for (auto& child : children)
				queue.push(child);

		while (!queue.empty())
		{
			auto& next = *queue.front();
			queue.pop();

			for (auto& predecessor : next.predecessors)
			{
				if (!predecessor->isChildOf(this) && !predecessor->isChildOf(peers))
					prerequisites.push_back(predecessor);
			}

			for (auto& child : next.children)
				queue.push(child);
		}

		return prerequisites;
	}

	void Task::reset() const
	{
		state->reset(*this);
	}

	bool Task::traverse(size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade) const
	{
		assert(!aborted);
		assert(!children_processing && !predecessors_processing);

		return state->traverse(*this, visited, visitor, aborted, progress, depth, cascade);
	}

	bool Task::tryAddPredecessor(Task& predecessor)
	{
		if (predecessor.isSuccessorOf(*this))
			return false;

		addPredecessor(predecessor);
		return true;
	}

	bool BuildTask::accept(TaskVisitor& v) const
	{
		return v.visit(*this);
	}

	bool CommandTask::accept(TaskVisitor& v) const
	{
		return v.visit(*this);
	}
}