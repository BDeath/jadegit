#pragma once
#include "CommandTask.h"

namespace JadeGit::Build
{
	class MoveClassTask : public CommandTask
	{
	public:
		MoveClassTask(Graph& graph, std::string qualifiedName, std::string newSuperClass);

		operator std::string() const final;

	protected:
		std::string qualifiedName;
		std::string newSuperClass;

		void execute(CommandStrategy& strategy) const final;
	};
}