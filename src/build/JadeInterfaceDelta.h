#pragma once
#include "TypeDelta.h"
#include <jadegit/data/JadeInterface.h>

namespace JadeGit::Build
{
	class IJadeInterfaceDelta : public ITypeDelta
	{
	public:
		using ITypeDelta::ITypeDelta;

		// Nothing specific as yet
	};

	template<class TComponent, class TInterface = IJadeInterfaceDelta>
	class JadeInterfaceDelta : public TypeDelta<TComponent, TInterface>
	{
	public:
		JadeInterfaceDelta(Graph& graph) : TypeDelta<TComponent, TInterface>(graph, "Interface") {}

		using TypeDelta<TComponent, TInterface>::previous;
		using TypeDelta<TComponent, TInterface>::latest;

	protected:
		bool AnalyzeEnter() override
		{
			// Basic analysis
			if (!TypeDelta<TComponent, TInterface>::AnalyzeEnter())
			{
				auto deletion = this->GetDeletion();

				// Handle deletion before deleting previous super-interfaces
				for (JadeInterface* interface : previous->superinterfaces)
					this->addDeletionDependency(interface->getRootType(), deletion, interface->isSubschemaCopy() && previous->GetRootSchema().version < par70019);

				return false;
			}

			auto definition = this->GetDefinition();

			// Latest definition is dependent on latest super-interfaces being created
			for (JadeInterface* interface : latest->superinterfaces)
				this->addCreationDependency(interface->getRootType(), definition);

			// Deleting previous super-interfaces depends on interface being updated
			if (previous)
				for (JadeInterface* interface : previous->superinterfaces)
					this->addDeletionDependency(interface->getRootType(), definition, interface->isSubschemaCopy() && previous->GetRootSchema().version < par70019);

			return true;
		}

		bool requiresCompleteDefinition(const Task* parent) const final
		{
			// Always create a 'complete' definition for original interfaces (otherwise, omitted features are deleted during load)
			return (parent && parent->complete) || !latest->isInferred();
		}
	};
}