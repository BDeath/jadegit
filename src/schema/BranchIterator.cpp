#include "Branch.h"
#include "Repository.h"
#include "Exception.h"
#include "ObjectRegistration.h"

using namespace std;

namespace JadeGit::Schema
{
	class BranchIterator : public GitObject<git_branch_iterator>
	{
	public:
		using GitObject::GitObject;
		BranchIterator(const Repository& repo, git_branch_t list_flags);

		Branch next() const
		{
			git_branch_t type;
			unique_ptr<git_reference> ptr;
			int result = git_branch_next(git_ptr(ptr), &type, *this);
			if (result == GIT_ITEROVER)
				return Branch();
			git_throw(result);

			return Branch::make(getProperty<Repository>(TEXT("parent")), move(ptr), type);
		}
	};
	static GitObjectRegistration<BranchIterator> registration(TEXT("BranchIterator"));

	BranchIterator::BranchIterator(const Repository& repo, git_branch_t list_flags) : GitObject(registration, nullptr)
	{
		jade_throw(setProperty(TEXT("parent"), repo));
		git_throw(git_branch_iterator_new(git_ptr(*this), repo, list_flags));
	}

	int JOMAPI jadegit_branch_iterator_new(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				DskParam* pRepo = nullptr;
				DskParam* pListFlags = nullptr;
				JADE_RETURN(paramGetParameter(*pParams, 1, pRepo));
				JADE_RETURN(paramGetParameter(*pParams, 2, pListFlags));

				Repository repo;
				JADE_RETURN(paramGetOid(*pRepo, repo.oid));

				int list_flags = 0;
				JADE_RETURN(paramGetInteger(*pListFlags, list_flags));

				BranchIterator iter(repo, static_cast<git_branch_t>(list_flags));
				return paramSetOid(pReturn, iter.oid);
			});
	}

	int JOMAPI jadegit_branch_iterator_next(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				auto branch = BranchIterator(pBuffer).next();

				JADE_RETURN(paramSetOid(*pParams, branch.oid));
				return paramSetBoolean(*pReturn, !branch.isNull());
			});
	}
}