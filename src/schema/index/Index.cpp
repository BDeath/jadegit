#include "Index.h"
#include <jade/Lock.h>
#include <jade/Transaction.h>
#include <schema/data/Index.h>
#include <schema/data/Worktree.h>
#include <schema/ObjectRegistration.h>
#include <git2/sys/repository.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	Index Index::open(const Repository& repo)
	{
		auto worktree = repo.worktree();

		// Handle opening worktree index stored internally
		if (!worktree.isNull())
		{
			return Index(repo, worktree);
		}
		// Handle opening standard index stored on-disk
		else
		{
			unique_ptr<git_index> ptr;
			git_throw(git_repository_index(git_ptr(ptr), repo));
			return Index(repo, move(ptr));
		}
	}

	static GitObjectRegistration<Index> registration(TEXT("Index"));

	Index::Index(const Repository& repo, const WorktreeData& worktree) : GitObject(registration, nullptr, repo)
	{
		jade_throw(setProperty(TEXT("worktree"), worktree));

		auto data = worktree.index();
		
		// Lock data while loading, exclusively on first use
		Lock lock(data.oid, data.edition() ? LockType::SHARED_LOCK : LockType::EXCLUSIVE_LOCK);

		// Open index
		GitObject::reset(data.open());

		// Store edition
		edition_store(data);

		// Set repository index
		git_throw(git_repository_set_index(repo, *this));

		// Initialize index based on current head on first use
		if (!data.edition())
		{
			auto head = repo.head();
			if (head)
			{
				unique_ptr<git_commit> commit;
				git_throw(git_commit_lookup(git_ptr(commit), repo, git_reference_target(head.get())));

				reset(commit.get());
				write();
			}
		}
	}

	Index::Index(const Repository& repo, std::unique_ptr<git_index> ptr) : GitObject(registration, move(ptr), repo)
	{
	}

	void Index::add_from_buffer(const string& path, const string& blob) const
	{
		git_index* index = *this;
		git_repository* repo = git_index_owner(index);

		unique_ptr<git_filter_list> filters;
		git_throw(git_filter_list_load(git_ptr(filters), repo, NULL, path.c_str(), git_filter_mode_t::GIT_FILTER_TO_ODB, git_filter_flag_t::GIT_FILTER_DEFAULT));

		unique_ptr<git_buf> buf = make_unique<git_buf>();
		git_throw(git_filter_list_apply_to_buffer(buf.get(), filters.get(), blob.c_str(), blob.size()));

		/* Start transaction */
		Transaction transaction;

		git_index_entry entry;
		entry.path = path.c_str();
		entry.mode = GIT_FILEMODE_BLOB;
		entry.flags = GIT_INDEX_ADD_FORCE | GIT_INDEX_ADD_CHECK_PATHSPEC;
		git_throw(git_index_add_from_buffer(index, &entry, buf->ptr, buf->size));

		/* Success */
		transaction.commit();
	}

	bool Index::edition_check(IndexData& data) const
	{
		// Check edition/transaction match what was stored previously
		return getProperty<Edition>(TEXT("edition_")) == data.edition() && 
			getProperty<SequenceNumber>(TEXT("transaction")) == data.getUpdateTranID();
	}

	void Index::edition_store(IndexData& data) const
	{
		jade_throw(setProperty(TEXT("edition_"), data.edition()));
		jade_throw(setProperty(TEXT("transaction"), data.getUpdateTranID()));
	}

	bool Index::has_conflicts() const
	{
		return git_index_has_conflicts(*this);
	}

	const Index& Index::read(bool force) const
	{
		auto worktree = this->worktree();
		
		// Handle refreshing worktree index stored internally
		if (!worktree.isNull())
		{
			auto data = worktree.index();

			// Lock data while loading
			Lock lock(data.oid, LockType::SHARED_LOCK);

			// Suppress reload when it's not required
			if (!force && edition_check(data))
				return *this;

			// Read index
			data.read(*this);

			// Store edition
			edition_store(data);
		}
		// Handle refreshing standard index stored on-disk
		else
		{
			git_throw(git_index_read(*this, force));
		}

		return *this;
	}

	void Index::reset(const git_commit* commit) const
	{
		unique_ptr<git_tree> tree;
		git_throw(git_commit_tree(git_ptr(tree), commit));

		reset(tree.get());
	}

	void Index::reset(const git_tree* tree) const
	{
		// Reset index to match tree
		git_throw(git_index_read_tree(*this, tree));
	}

	void Index::write() const
	{
		auto worktree = this->worktree();

		// Handle saving worktree index stored internally
		if (!worktree.isNull())
		{
			auto data = worktree.index();

			// Lock data for update
			Lock lock(data.oid, LockType::EXCLUSIVE_LOCK);

			// Check edition matches what was loaded/saved previously
			if (!edition_check(data))
				throw runtime_error("Index edition mismatch");

			Transaction transaction;

			// Save index data
			data.write(*this);

			// Store edition
			edition_store(data);

			transaction.commit();
		}
		// Handle writing standard index stored on-disk
		else
		{
			git_throw(git_index_write(*this));
		}
	}

	WorktreeData Index::worktree() const
	{
		return getProperty<WorktreeData>(TEXT("worktree"));
	}

	int JOMAPI jadegit_index_add_from_buffer(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				DskParam* pPath = nullptr;
				DskParam* pBlob = nullptr;
				JADE_RETURN(paramGetParameter(*pParams, 1, pPath));
				JADE_RETURN(paramGetParameter(*pParams, 2, pBlob));

				string path;
				JADE_RETURN(paramGetString(*pPath, path));

				string blob;
				JADE_RETURN(paramGetString(*pBlob, blob));

				Index(pBuffer).add_from_buffer(path, blob);
				return J_OK;
			});
	}

	int JOMAPI jadegit_index_has_conflicts(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				return paramSetBoolean(*pReturn, Index(pBuffer).has_conflicts());
			});
	}

	int JOMAPI jadegit_index_read(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				bool force = false;
				JADE_RETURN(paramGetBoolean(*pParams, force));

				Index(pBuffer).read(force);
				return paramSetOid(*pReturn, pBuffer->oid);
			});
	}

	int JOMAPI jadegit_index_write(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				Index(pBuffer).write();
				return J_OK;
			});
	}
}