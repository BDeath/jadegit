#pragma once
#include "Index.h"

namespace JadeGit::Schema
{
	class IndexEntry : public GitObject<const git_index_entry*>
	{
	public:
		using GitObject::GitObject;
		IndexEntry(const Index& parent, const git_index_entry* ptr);

	};
}