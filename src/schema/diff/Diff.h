#pragma once
#include <schema\Repository.h>

namespace JadeGit::Schema
{
	class Tree;

	class Diff : public GitObject<git_diff>
	{
	public:
		static Diff tree_to_tree(const Repository& repo, const Tree& old_tree, const Tree& new_tree);

		using GitObject::GitObject;

	protected:
		Diff(const Repository& repo, std::unique_ptr<git_diff> ptr);
	};
}