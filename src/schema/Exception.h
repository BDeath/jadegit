#pragma once
#include "Object.h"
#include <jadegit/git2.h>

namespace JadeGit::Schema
{
	class GitException : public Object
	{
	public:
		GitException(std::exception &ex);
		GitException(git_exception& ex);
		~GitException() {}

        template <typename T>
        static int wrapper(T func)
        {
			try
			{
				return func();
			}
			catch (git_exception& e)
			{
				GitException ex(e);
			}
			catch (std::exception& e)
			{
				GitException ex(e);
			}

            return 0;
        }

	protected:
		GitException(std::exception& ex, const int code);
	};
}