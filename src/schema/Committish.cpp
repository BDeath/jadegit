#include "Committish.h"
#include <jade/String.h>
#include <jadegit/git2.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	git_oid Committish::id() const
	{
		DskParamString pResult;
		invokeInterfaceMethod(TEXT("ICommittish"), TEXT("id"), nullptr, &pResult);

		string id;
		jade_throw(paramGetString(pResult, id));

		git_oid oid = { 0 };
		git_throw(git_oid_fromstrn(&oid, id.c_str(), id.length()));
		return oid;
	}
}