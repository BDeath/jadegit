#include "Reference.h"
#include "Repository.h"
#include "Exception.h"
#include "ObjectRegistration.h"

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	class ReferenceIterator : public GitObject<git_reference_iterator>
	{
	public:
		using GitObject::GitObject;
		ReferenceIterator(const Repository& repo, string glob);

		Reference next() const
		{
			unique_ptr<git_reference> ptr;
			int result = git_reference_next(git_ptr(ptr), *this);
			if (result == GIT_ITEROVER)
				return Reference();
			git_throw(result);

			return Reference::make(getProperty<Repository>(TEXT("parent")), move(ptr));
		}
	};
	static GitObjectRegistration<ReferenceIterator> registration(TEXT("ReferenceIterator"));

	ReferenceIterator::ReferenceIterator(const Repository& repo, string glob) : GitObject(registration, nullptr)
	{
		jade_throw(setProperty(TEXT("parent"), repo));
		if (!glob.empty())
			git_throw(git_reference_iterator_glob_new(git_ptr(*this), repo, glob.c_str()));
		else
			git_throw(git_reference_iterator_new(git_ptr(*this), repo));
	}

	int JOMAPI jadegit_reference_iterator_new(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				DskParam* pRepo = nullptr;
				DskParam* pGlob = nullptr;
				JADE_RETURN(paramGetParameter(*pParams, 1, pRepo));
				JADE_RETURN(paramGetParameter(*pParams, 2, pGlob));

				Repository repo;
				JADE_RETURN(paramGetOid(*pRepo, repo.oid));
				
				string glob;
				JADE_RETURN(paramGetString(*pGlob, glob));

				ReferenceIterator iter(repo, glob);
				return paramSetOid(pReturn, iter.oid);
			});
	}

	int JOMAPI jadegit_reference_iterator_next(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				auto reference = ReferenceIterator(pBuffer).next();

				JADE_RETURN(paramSetOid(*pParams, reference.oid));
				return paramSetBoolean(*pReturn, !reference.isNull());
			});
	}
}