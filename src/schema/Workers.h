#pragma once
#include <ctpl_stl.h>

namespace JadeGit::Schema::Backend
{
	extern ctpl::thread_pool workers;

	void SignOn();
}