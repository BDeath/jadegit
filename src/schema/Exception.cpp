#include "Exception.h"
#include "ObjectRegistration.h"
#include <jade/ExceptionHandler.h>
#include <Exception.h>
#include <Log.h>
#include <sstream>

using namespace std;

namespace JadeGit::Schema
{
	static GitObjectRegistration<GitException> registration(TEXT("GitException"));

	GitException::GitException(std::exception& ex, const int code) : Object(registration)
	{
		// Unpack any nested exceptions
		stringstream ss;
		print_nested_exceptions(ss, ex);
		auto extendedErrorText = ss.str();

		// Log entire exception if there's nested exceptions
		if (!extendedErrorText.empty())
			log(ex);
		
		// Create exception object
		createTransientObject();
		setProperty(TEXT("errorCode"), code);
		setProperty(TEXT("errorText"), string(ex.what()));
		setProperty(TEXT("extendedErrorText"), extendedErrorText);

		// Raise exception
		jomRaiseException(nullptr, &oid, ExceptionCause::EC_PRECONDITION, __LINE__);
	}

	GitException::GitException(std::exception& ex) : GitException(ex, 64000)
	{
	}

	GitException::GitException(git_exception& ex) : GitException(ex, 64100 + ex.code)
	{
	}

	int JOMAPI jadegit_exception_handler(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return Jade::ExceptionHandler::handler(pBuffer, pParams, pReturn);
	}
}