#include "Deployment.h"
#include <jade/Loader.h>
#include <jadegit/vfs/File.h>
#include <Exception.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema::Install
{
	class DeploymentCommand
	{
	public:
		virtual void execute(Loader& loader) = 0;
	};

	class LoadFile : public DeploymentCommand
	{
	public:
		LoadFile(filesystem::path path, bool latest) : path(path), latest(latest) {}

	protected:
		filesystem::path path;
		bool latest = false;
	};

	class LoadCommandFile : public LoadFile
	{
	public:
		using LoadFile::LoadFile;

	private:
		void execute(Loader& loader) final
		{
			loader.loadCommandFile(path, latest);
		}
	};

	class LoadSchemaFile : public LoadFile
	{
	public:
		using LoadFile::LoadFile;

	private:
		void execute(Loader& loader) final
		{
			loader.loadSchemaFile(path, filesystem::path(), latest);
		}
	};

	class LoadSchemaDataFile : public LoadFile
	{
	public:
		using LoadFile::LoadFile;

	private:
		void execute(Loader& loader) final
		{
			loader.loadSchemaFile(filesystem::path(), path, latest);
		}
	};

	class ReorgCommand : public DeploymentCommand
	{
	private:
		void execute(Loader& loader) final
		{
			loader.reorg(TEXT("JadeGitSchema"));
		}
	};

	Deployment::Deployment() : fs("install/deployment")
	{
	}

	Deployment::~Deployment() {}

	void Deployment::execute()
	{
		// Setup loader
		Loader loader;

		// Validate registry changes
		Registry::Root::validate(registry, false);

		// Execute deployment commands
		for (auto& command : commands)
			command->execute(loader);

		// Update registry
		Registry::Root::load(registry, false);
	}

	void Deployment::start()
	{
		fs.purge();
	}

	void Deployment::start(const Registry::Root& registry)
	{
		// Can only support single stage currently
		assert(this->registry.repos.empty());

		// Registry copied during finish for validation/update during deploy
	}

	void Deployment::finish(const Registry::Root& registry)
	{
		if (needsReorg)
			reorg();

		// Save copy of registry for validation/update during deploy
		this->registry = registry;
	}

	void Deployment::finish()
	{
	}

	void Deployment::changeLoadStyle(bool latestVersion)
	{
		// Reset flag indicating re-org has just been added
		reorging = false;

		// Set flag indicating reorg is needed
		if (latestVersion)
			needsReorg = true;

		// Perform re-org before loading into current schema version
		else if (needsReorg)
			reorg();
	}

	unique_ptr<ostream> Deployment::AddCommandFile(bool latestVersion)
	{
		// Change load style
		changeLoadStyle(latestVersion);

		auto file = makeFile("Commands", "jcf");
		commands.push_back(make_unique<LoadCommandFile>(fs.path() / file.path(), latestVersion));
		return file.createOutputStream();
	}

	unique_ptr<ostream> Deployment::AddSchemaFile(const string& schema, bool latestVersion)
	{
		// Change load style
		changeLoadStyle(latestVersion);

		auto file = makeFile(schema, "scm");
		commands.push_back(make_unique<LoadSchemaFile>(fs.path() / file.path(), latestVersion));
		return file.createOutputStream();
	}

	unique_ptr<ostream> Deployment::AddSchemaDataFile(const string& schema, bool latestVersion)
	{
		// Change load style
		changeLoadStyle(latestVersion);

		auto file = makeFile(schema, "ddx");
		commands.push_back(make_unique<LoadSchemaDataFile>(fs.path() / file.path(), latestVersion));
		return file.createOutputStream();
	}

	void Deployment::addScript(const Build::Script& script)
	{
		throw unimplemented_feature("Deploying scripts during install");
	}

	File Deployment::makeFile(const string& schema, const char* extension)
	{
		// Increment file counter
		files++;

		// Derive filename
		stringstream filename;
		filename << "schemas/" << setfill('0') << setw(2) << files << "-" << schema << "." << extension;

		// Open file
		File file = fs.open(filename.str());

		// Check file doesn't already exist
		if (file.exists())
			throw runtime_error("File already exists (" + filename.str() + ")");

		return file;
	}

	void Deployment::reorg()
	{
		// Suppress duplicate re-orgs
		if (reorging)
			return;

		commands.push_back(make_unique<ReorgCommand>());

		// Set flag indicating we've just added re-org
		reorging = true;

		// Reset flag indicating reorg is needed
		needsReorg = false;
	}
}