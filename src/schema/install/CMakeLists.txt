# Add source files
target_sources(jadegit PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/Deployment.cpp
	${CMAKE_CURRENT_LIST_DIR}/Install.cpp
	${CMAKE_CURRENT_LIST_DIR}/Repository.cpp
)