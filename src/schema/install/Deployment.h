#pragma once
#include <deploy/build/Builder.h>
#include <vfs/TempFileSystem.h>
#include <vector>

namespace JadeGit::Schema::Install
{
	class DeploymentCommand;

	class Deployment : public Deploy::Builder
	{
	public:
		Deployment();
		~Deployment();

		void execute();

	private:
		TempFileSystem fs;
		std::vector<std::unique_ptr<DeploymentCommand>> commands;
		Registry::Root registry;
		
		int files = 0;
		bool latestVersion = false;
		bool needsReorg = false;
		bool reorging = false;

		void start() final;
		void start(const Registry::Root& registry) final;
		void finish(const Registry::Root& registry) final;
		void finish() final;

		void changeLoadStyle(bool latestVersion);
		std::unique_ptr<std::ostream> AddCommandFile(bool latestVersion) final;
		std::unique_ptr<std::ostream> AddSchemaFile(const std::string& schema, bool latestVersion) final;
		std::unique_ptr<std::ostream> AddSchemaDataFile(const std::string& schema, bool latestVersion) final;
		void addScript(const Build::Script& script) final;
		File makeFile(const std::string& schema, const char* extension);
		void reorg() final;
	};
}