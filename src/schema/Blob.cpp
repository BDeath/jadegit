#include "Blob.h"
#include "Exception.h"
#include "ObjectRegistration.h"

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	static GitObjectRegistration<Blob> registration(TEXT("Blob"));

	Blob Blob::lookup(const Repository& repo, const git_oid& oid)
	{
		// Dereference transient collection
		auto blobs = repo.getProperty<DskMemberKeyDictionary>(TEXT("blobs"));

		// Return existing
		char id[GIT_OID_MAX_HEXSIZE + 1] = "";
		Blob blob;
		jade_throw(blobs.getAtKey(widen(git_oid_tostr(id, GIT_OID_MAX_HEXSIZE + 1, &oid)).c_str(), blob));
		if (!blob.isNull())
			return blob;

		// Lookup blob
		unique_ptr<git_blob> ptr;
		auto error = git_blob_lookup(git_ptr(ptr), repo, &oid);
		if (error == GIT_ENOTFOUND)
			return Blob();
		git_throw(error);

		// Return new
		return Blob(repo, move(ptr));
	}

	Blob::Blob(const Repository& repo, unique_ptr<git_blob> ptr) : GitObject(registration, move(ptr))
	{
		setProperty(TEXT("id"), git_blob_id(static_cast<git_blob*>(*this)));
		jade_throw(setProperty(TEXT("parent"), repo));
	}

	int JOMAPI jadegit_blob_content(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return jadegit_proxy_mapping_method<Blob>(pBuffer, pParams, [&](git_blob* blob, DskParam& value)
			{
				unique_ptr<git_buf> buf = make_unique<git_buf>();
				git_throw(git_blob_filter(buf.get(), blob, "", nullptr));
				return paramSetString(value, widen(string(buf->ptr, buf->size)));
			});
	}

	int JOMAPI jadegit_blob_lookup(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				DskParam* pRepo = nullptr;
				DskParam* pId = nullptr;
				JADE_RETURN(paramGetParameter(*pParams, 1, pRepo));
				JADE_RETURN(paramGetParameter(*pParams, 2, pId));

				Repository repo;
				JADE_RETURN(paramGetOid(*pRepo, repo.oid));

				string id;
				JADE_RETURN(paramGetString(*pId, id));

				git_oid oid = { 0 };
				git_throw(git_oid_fromstrn(&oid, id.c_str(), id.length()));

				auto blob = Blob::lookup(repo, oid);
				return paramSetOid(pReturn, blob.oid);
			});
	}
}