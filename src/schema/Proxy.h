#pragma once
#include "Object.h"
#include "Exception.h"
#include <jadegit/git2.h>
#include <functional>

namespace JadeGit::Schema
{
	// Proxy object template
	template <class T>
	class GitObject : public Object
	{
	public:
		using Object::Object;
		using element_type = T;	

		operator element_type* () const
		{
			return getPtr();
		}

		void dispose() const
		{
			if (getProperty<bool>(TEXT("disposable")))
			{
				std::unique_ptr<element_type> ptr(getPtr());
				setPtr(nullptr);
			}
		}

		void reset(element_type* ptr) const
		{
			dispose();
			setPtr(ptr);
		}

		void reset(std::unique_ptr<element_type> ptr) const
		{
			reset(ptr.get());
			ptr.release();
		}

	protected:
		GitObject(ClassNumber classNo, const Object& parent, element_type* ptr, bool disposable = true) : Object(classNo)
		{
			jade_throw(createTransientObject());
			jade_throw(setProperty(TEXT("parent"), parent));
			jade_throw(setProperty(TEXT("disposable"), disposable));
			setPtr(ptr);
		}

		GitObject(ClassNumber classNo, std::unique_ptr<element_type> ptr, const Object& parent = Object()) : GitObject(classNo, parent, ptr.get())
		{
			ptr.release();
		}

	private:
		void setPtr(element_type* ptr) const
		{
			DskParam address;
			jade_throw(paramSetMemoryAddress(address, ptr, jomGetMemorySpaceId()));
			jade_throw(setProperty(TEXT("ptr"), &address));
		}
	
		element_type* getPtr() const
		{
			if (isNull())
				return nullptr;

			DskParam address;
			jade_throw(paramSetMemoryAddress(address));

			DskParam prop;
			jade_throw(paramSetCString(prop, TEXT("ptr")));

			jade_throw(jomGetProperty(nullptr, &oid, &prop, &address, __LINE__));

			void* result;
			jade_throw(paramGetMemoryAddress(address, result, jomGetMemorySpaceId()));
			return static_cast<element_type*>(result);
		}
	};

	template <class T>
	class GitObject<const T*> : public Object
	{
	public:
		using Object::Object;
		using element_type = T;

		operator element_type* () const
		{
			if (isNull())
				return nullptr;

			DskParam address;
			jade_throw(paramSetMemoryAddress(address));

			DskParam prop;
			jade_throw(paramSetCString(prop, TEXT("ptr")));

			jade_throw(jomGetProperty(nullptr, &oid, &prop, &address, __LINE__));

			void* result;
			jade_throw(paramGetMemoryAddress(address, result, jomGetMemorySpaceId()));
			return static_cast<element_type*>(result);
		}

	protected:
		GitObject(ClassNumber classNo, const T* ptr, const Object& parent = Object(), const Character* prop = nullptr) : Object(classNo)
		{
			// Suppress creation where underlying object doesn't exist
			if (!ptr)
				return;

			// Instantiate
			jade_throw(createTransientObject());

			// Attach to parent
			if (!parent.isNull())
			{
				jade_throw(setProperty(TEXT("parent"), parent));

				// Set parent property
				if (prop)
					jade_throw(parent.setProperty(prop, this));
			}
			else
			{
				// Set flag to clean-up via destructor (as this has no parent to clean-up)
				disposable = true;
			}

			// Set pointer
			DskParam address;
			jade_throw(paramSetMemoryAddress(address, const_cast<T*>(ptr), jomGetMemorySpaceId()));
			jade_throw(setProperty(TEXT("ptr"), &address));
		}

		~GitObject()
		{
			if (disposable)
				jade_throw(deleteObject());
		}

	private:
		bool disposable = false;
	};

	template <class T>
	int jadegit_proxy_mapping_method(DskBuffer* pBuffer, DskParam* pParams, std::function<int(typename T::element_type*, DskParam&)> getter, std::function<int(T&, DskParam&)> setter = std::function<int(T&, DskParam&)>())
	{
		DskParam* pSet = nullptr;
		DskParam* pValue = nullptr;
		JADE_RETURN(paramGetParameter(*pParams, 1, pSet));
		JADE_RETURN(paramGetParameter(*pParams, 2, pValue));

		bool set = false;
		JADE_RETURN(paramGetBoolean(*pSet, set));

		if (set && !setter)
			return INVALID_PARAMETER_VALUE;

		return GitException::wrapper([&]()
			{
				T proxy(pBuffer);
				return set ? setter(proxy, *pValue) : getter(proxy, *pValue);
			});
	}

	template <class T>
	int jadegit_proxy_mapping_string(DskBuffer* pBuffer, DskParam* pParams, std::function<const char*(typename T::element_type*)> getter, std::function<int(T&, DskParam&)> setter = std::function<int(T&, const DskParam&)>())
	{
		return jadegit_proxy_mapping_method<T>(pBuffer, pParams, [&](T::element_type* p, DskParam& value)
			{
				return Jade::paramSetString(value, getter(p));
			},
			setter);
	}

	template <class T>
	int jadegit_proxy_mapping_string(DskBuffer* pBuffer, DskParam* pParams, char* T::element_type::* member)
	{
		return jadegit_proxy_mapping_string<T>(pBuffer, pParams, [&](T::element_type* p)
			{
				return p->*member;
			});
	}
}