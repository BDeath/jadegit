#include "Config.h"
#include "ConfigEntry.h"
#include <schema/Repository.h>
#include <schema/ObjectRegistration.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	static GitObjectRegistration<Config> registration(TEXT("Config"));

	Config Config::default_()
	{
		unique_ptr<git_config> config;
		git_throw(git_config_open_default(git_ptr(config)));
		return Config(move(config));
	}

	Config::Config(const Repository& parent, unique_ptr<git_config> ptr) : GitObject(registration, move(ptr), parent)
	{
	}

	Config::Config(unique_ptr<git_config> ptr) : Config(Repository(), move(ptr))
	{
	}

	string Config::default_branch() const
	{
		// Retrieve default branch setting (may be configured globally)
		unique_ptr<git_buf> buf = make_unique<git_buf>();
		auto error = git_config_get_string_buf(buf.get(), *this, "init.defaultbranch");

		// Return hard-coded default when it hasn't been configured
		// Matches default used by libgit2 git_repository_initialbranch
		if (error == GIT_ENOTFOUND)
			return "master";

		// Return configured default, provided there was no other error
		git_throw(error);
		return string(buf->ptr, buf->size);
	}

	ConfigEntry Config::get_entry(const string& name) const
	{
		git_config_entry* ptr = nullptr;
		git_throw(git_config_get_entry(&ptr, *this, name.c_str()));
		return ConfigEntry(*this, ptr);
	}

	void Config::set_string(const std::string& name, const std::string& value) const
	{
		git_throw(git_config_set_string(*this, name.c_str(), value.c_str()));
	}

	Config Config::snapshot() const
	{
		unique_ptr<git_config> snapshot;
		git_throw(git_config_snapshot(git_ptr(snapshot), *this));
		return Config(move(snapshot));
	}

	int JOMAPI jadegit_config_default(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				return paramSetOid(pReturn, Config::default_().oid);
			});
	}

	int JOMAPI jadegit_config_get_entry(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				string name;
				jade_throw(paramGetString(*pParams, name));

				return paramSetOid(pReturn, Config(pBuffer).get_entry(name).oid);
			});
	}

	int JOMAPI jadegit_config_set_string(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				DskParam* pName = nullptr;
				DskParam* pValue = nullptr;
				paramGetParameter(*pParams, 1, pName);
				paramGetParameter(*pParams, 2, pValue);

				string name;
				jade_throw(paramGetString(*pName, name));

				string value;
				jade_throw(paramGetString(*pValue, value));

				Config(pBuffer).set_string(name, value);
				return J_OK;
			});
	}

	int JOMAPI jadegit_config_snapshot(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				return paramSetOid(pReturn, Config(pBuffer).snapshot().oid);
			});
	}
}