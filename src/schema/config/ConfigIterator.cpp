#include "Config.h"
#include "ConfigEntry.h"
#include <schema/Exception.h>
#include <schema/ObjectRegistration.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	class ConfigIterator : public GitObject<git_config_iterator>
	{
	public:
		using GitObject::GitObject;
		ConfigIterator(const Config& config, string glob);

		ConfigEntry next() const
		{
			auto entry = getProperty<ConfigEntry>(TEXT("current_"));
			if (entry.isNull())
				entry = ConfigEntry(*this, nullptr, false);

			int result = git_config_next(git_ptr(entry), *this);
			if (result == GIT_ITEROVER)
			{
				entry.deleteObject();
				return ConfigEntry();
			}
			git_throw(result);

			return entry;
		}
	};
	static GitObjectRegistration<ConfigIterator> registration(TEXT("ConfigIterator"));

	ConfigIterator::ConfigIterator(const Config& config, string glob) : GitObject(registration, nullptr)
	{
		jade_throw(setProperty(TEXT("parent"), config));
		if (!glob.empty())
			git_throw(git_config_iterator_glob_new(git_ptr(*this), config, glob.c_str()));
		else
			git_throw(git_config_iterator_new(git_ptr(*this), config));
	}

	int JOMAPI jadegit_config_iterator_new(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				DskParam* pConfig = nullptr;
				DskParam* pGlob = nullptr;
				JADE_RETURN(paramGetParameter(*pParams, 1, pConfig));
				JADE_RETURN(paramGetParameter(*pParams, 2, pGlob));

				Config config;
				JADE_RETURN(paramGetOid(*pConfig, config.oid));

				string glob;
				JADE_RETURN(paramGetString(*pGlob, glob));

				ConfigIterator iter(config, glob);
				return paramSetOid(pReturn, iter.oid);
			});
	}

	int JOMAPI jadegit_config_iterator_next(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				auto entry = ConfigIterator(pBuffer).next();

				JADE_RETURN(paramSetOid(*pParams, entry.oid));
				return paramSetBoolean(*pReturn, !entry.isNull());
			});
	}
}