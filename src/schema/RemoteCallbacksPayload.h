#pragma once
#include <jadegit/schema/IRemoteCallbacks.h>

namespace JadeGit::Schema
{
	class RemoteCallbacksPayload
	{
	public:
		RemoteCallbacksPayload(git_remote_callbacks& cb, IRemoteCallbacks& callbacks);

		IRemoteCallbacks& callbacks;
	};
}