#include "Remote.h"
#include "Exception.h"
#include "RemoteCallbacks.h"
#include "RemoteCallbacksPayload.h"
#include "ObjectRegistration.h"
#include "Task.h"
#include <jadegit/git2.h>
#include <jade/Iterator.h>
#include <jade/Transaction.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{	
	static GitObjectRegistration<Remote> registration(TEXT("Remote"));

	Remote Remote::add(const Repository& repo, const string& name, const string& url)
	{
		// Create remote
		unique_ptr<git_remote> ptr;
		git_throw(git_remote_create(git_ptr(ptr), repo, name.c_str(), url.c_str()));

		return Remote(repo, move(ptr));
	}

	Remote Remote::lookup(const Repository& repo, const string& name)
	{
		// Lookup remote
		unique_ptr<git_remote> ptr;
		auto error = git_remote_lookup(git_ptr(ptr), repo, name.c_str());
		if (error == GIT_ENOTFOUND)
			return Remote();
		git_throw(error);

		// Return new
		return Remote(repo, move(ptr));
	}

	Remote::Remote(const Repository& repo, unique_ptr<git_remote> ptr) : GitObject(registration, move(ptr))
	{
		jade_throw(setProperty(TEXT("parent"), repo));
	}

	bool Remote::fetch(bool prune, bool allTags, IProgress* progress) const
	{
		if (progress && !progress->start(1, "Fetching from " + string(git_remote_name(*this))))
			return false;

		// Configure fetch options
		git_fetch_options options;
		git_throw(git_fetch_options_init(&options, GIT_FETCH_OPTIONS_VERSION));

		// Suppress updating fetch head
		options.update_fetchhead = 0;

		// Auto detect proxy
		options.proxy_opts.type = git_proxy_t::GIT_PROXY_AUTO;

		RemoteCallbacks callbacks(progress);
		RemoteCallbacksPayload payload(options.callbacks, callbacks);

		if (prune)
			options.prune = git_fetch_prune_t::GIT_FETCH_PRUNE;

		if (allTags)
			options.download_tags = git_remote_autotag_option_t::GIT_REMOTE_DOWNLOAD_TAGS_ALL;

		// Fetch
		Transaction transaction;
		git_strarray refspecs = { 0 };
		if (git_throw(git_remote_fetch(*this, &refspecs, &options, NULL)) == GIT_EUSER)
			return false;	// Operation cancelled via callback

		transaction.commit();
		return (!progress || progress->finish());
	}

	bool Remote::pull(IProgress* progress) const
	{
		auto repo = this->repo();

		// Retrieve current branch
		auto head = repo.head();
		if (!head || !git_reference_is_branch(head.get()))
			throw runtime_error("Cannot pull into detached head");

		if (progress && !progress->start(2, "Pulling"))
			return false;

		if (!pull(repo, head.get(), progress))
			return false;

		return (!progress || progress->finish());
	}

	bool Remote::pull(const Repository& repo, const git_reference* branch, IProgress* progress) const
	{
		// Fetch latest changes
		if (!fetch(false, false, progress))
			return false;

		// TODO: Support pull with rebase, without creating a merge commit.

		// Merge upstream branch
		if (!repo.merge(upstream(branch).get(), progress))
			return false;

		return true;
	}

	bool Remote::push(bool force, IProgress* progress) const
	{
		// Retrieve current branch
		auto head = repo().head();
		if (!head || !git_reference_is_branch(head.get()))
			throw runtime_error("Cannot push detached head");

		return push(head.get(), force, progress);
	}

	bool has_upstream(const git_reference* branch)
	{
		unique_ptr<git_buf> name = make_unique<git_buf>();
		auto error = git_branch_upstream_merge(name.get(), git_reference_owner(branch), git_reference_name(branch));
		if (error == GIT_ENOTFOUND)
			return false;
		git_throw(error);
		return true;
	}

	bool Remote::push(git_reference* branch, bool force, IProgress* progress) const
	{
		// Determine if upstream should be configured
		bool set_upstream = !has_upstream(branch);

		vector<string> refspecs;
		refspecs.push_back(string(force ? "+" : "") + git_reference_name(branch));

		if (!push(refspecs, progress))
			return false;

		// Configure upstream if required
		if (set_upstream)
		{
			auto name = upstream_name(git_reference_name(branch));
			if (!name.empty())
			{
				unique_ptr<git_reference> upstream_ref;
				git_throw(git_reference_lookup(git_ptr(upstream_ref), git_reference_owner(branch), name.c_str()));

				const char* branch_name = nullptr;
				git_throw(git_branch_name(&branch_name, upstream_ref.get()));

				Transaction transaction;
				git_throw(git_branch_set_upstream(branch, branch_name));
				transaction.commit();
			}
		}

		return true;
	}

	bool Remote::push(const vector<string>& refspec_strings, IProgress* progress) const
	{
		if (progress && !progress->start(1, "Pushing to " + string(git_remote_name(*this))))
			return false;

		// Setup refspecs
		size_t refspec_size = refspec_strings.size();
		auto refspec_ptrs = make_unique<const char* []>(refspec_size);

		for (int i = 0; i < refspec_size; i++)
			refspec_ptrs[i] = refspec_strings.at(i).c_str();

		git_strarray refspecs;
		refspecs.strings = (char**)refspec_ptrs.get();
		refspecs.count = refspec_size;

		// Configure push options
		git_push_options options;
		git_throw(git_push_options_init(&options, GIT_PUSH_OPTIONS_VERSION));

		// Auto detect proxy
		options.proxy_opts.type = git_proxy_t::GIT_PROXY_AUTO;

		RemoteCallbacks callbacks(progress);
		RemoteCallbacksPayload payload(options.callbacks, callbacks);

		// Push to remote
		Transaction transaction;
		if (git_throw(git_remote_push(*this, &refspecs, &options)) == GIT_EUSER)
			return false;	// Operation cancelled via callback

		transaction.commit();
		return (!progress || progress->finish());
	}

	Repository Remote::repo() const
	{
		return getProperty<Repository>(TEXT("parent"));
	}

	bool Remote::sync(IProgress* progress) const
	{
		auto repo = this->repo();

		// Retrieve current branch
		auto head = repo.head();
		if (!head || !git_reference_is_branch(head.get()))
			throw runtime_error("Cannot sync with detached head");

		if (progress && !progress->start(3, "Syncing"))
			return false;

		// Pull (fetch + merge/rebase)
		if (!pull(repo, head.get(), progress))
			return false;

		// Push
		if (!push(head.get(), progress))
			return false;

		return (!progress || progress->finish());
	}

	unique_ptr<git_reference> Remote::upstream(const git_reference* branch, git_direction direction) const
	{
		unique_ptr<git_reference> ref;
		git_throw(git_reference_lookup(git_ptr(ref), git_reference_owner(branch), upstream_name(branch, direction).c_str()));
		return ref;
	}

	string Remote::upstream_name(const git_reference* ref, git_direction direction) const
	{
		auto name = make_unique<git_buf>();
		git_throw(git_branch_upstream_merge(name.get(), git_reference_owner(ref), git_reference_name(ref)));
		return upstream_name(name->ptr, direction);
	}

	string Remote::upstream_name(const char* refname, git_direction direction) const
	{
		auto remote = static_cast<const git_remote*>(*this);
		auto count = git_remote_refspec_count(remote);
		for (size_t index = 0; index < count; index++)
		{
			auto refspec = git_remote_get_refspec(remote, index);

			if (git_refspec_direction(refspec) != direction)
				continue;

			if (!git_refspec_src_matches(refspec, refname))
				continue;

			auto name = make_unique<git_buf>();
			git_throw(git_refspec_transform(name.get(), refspec, refname));
			return string(name->ptr, name->size);
		}

		// No match
		throw runtime_error("Failed to resolve remote tracking branch");
	}

	int JOMAPI jadegit_remote_add(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				DskParam* pRepo;
				DskParam* pName;
				DskParam* pUrl;

				JADE_RETURN(paramGetParameter(*pParams, 1, pRepo));
				JADE_RETURN(paramGetParameter(*pParams, 2, pName));
				JADE_RETURN(paramGetParameter(*pParams, 3, pUrl));

				Repository repo;
				JADE_RETURN(paramGetOid(*pRepo, repo.oid));

				string name;
				JADE_RETURN(paramGetString(*pName, name));

				string url;
				JADE_RETURN(paramGetString(*pUrl, url));

				auto remote = Remote::add(repo, name, url);
				return paramSetOid(*pReturn, remote.oid);
			});
	}

	int JOMAPI jadegit_remote_fetch(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		DskParam* pPrune;
		DskParam* pAllTags;
		JADE_RETURN(paramGetParameter(*pParams, 1, pPrune));
		JADE_RETURN(paramGetParameter(*pParams, 2, pAllTags));

		bool prune = false;
		JADE_RETURN(paramGetBoolean(*pPrune, prune));

		bool allTags = false;
		JADE_RETURN(paramGetBoolean(*pAllTags, allTags));

		Remote remote(pBuffer);
		return Task::make(pReturn, [=](IProgress* progress) { return remote.fetch(prune, allTags, progress); });
	}

	int JOMAPI jadegit_remote_list(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				DskParam* pRepo = nullptr;
				DskParam* pList = nullptr;
				JADE_RETURN(paramGetParameter(*pParams, 1, pRepo));
				JADE_RETURN(paramGetParameter(*pParams, 2, pList));

				Repository repo;
				JADE_RETURN(paramGetOid(*pRepo, repo.oid));

				DskArray list;
				JADE_RETURN(paramGetOid(*pList, list.oid));

				unique_ptr<git_strarray> remotes = make_unique<git_strarray>();
				git_throw(git_remote_list(remotes.get(), repo));

				for (int i = 0; i < remotes->count; i++)
				{
					auto name = const_cast<const char*>(remotes->strings[i]);

					auto str = Jade::widen(string(name));

					jade_throw(list.add(str.c_str()));
				}
				
				return J_OK;
			});
	}

	int JOMAPI jadegit_remote_lookup(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				DskParam* pRepo = nullptr;
				DskParam* pName = nullptr;
				JADE_RETURN(paramGetParameter(*pParams, 1, pRepo));
				JADE_RETURN(paramGetParameter(*pParams, 2, pName));

				Repository repo;
				JADE_RETURN(paramGetOid(*pRepo, repo.oid));

				string name;
				JADE_RETURN(paramGetString(*pName, name));

				auto remote = Remote::lookup(repo, name);
				return paramSetOid(pReturn, remote.oid);
			});
	}

	int JOMAPI jadegit_remote_name(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return jadegit_proxy_mapping_string<Remote>(pBuffer, pParams, git_remote_name);
	}

	int JOMAPI jadegit_remote_pull(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		Remote remote(pBuffer);
		return Task::make(pReturn, [=](IProgress* progress)
			{
				return remote.pull(progress);
			});
	}

	int JOMAPI jadegit_remote_push(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		Remote remote(&pBuffer->oid);

		DskParam* pRefspec = nullptr;
		DskParam* pForce = nullptr;
		JADE_RETURN(paramGetParameter(*pParams, 1, pRefspec));
		JADE_RETURN(paramGetParameter(*pParams, 2, pForce));

		DskArray refspec_array;
		JADE_RETURN(paramGetOid(*pRefspec, refspec_array.oid));

		bool force = false;
		JADE_RETURN(paramGetBoolean(*pForce, force));
		
		if (refspec_array.isNull())
			return Task::make(pReturn, [=](IProgress* progress) { return remote.push(force, progress); });

		vector<string> refspec_strings;
		String refspec;
		Iterator<String> refspec_iter(refspec_array);
		while (refspec_iter.next(refspec))
			refspec_strings.push_back(narrow(refspec));

		return Task::make(pReturn, [=](IProgress* progress) { return remote.push(refspec_strings, progress); });
	}

	int JOMAPI jadegit_remote_remove(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				Remote remote(pBuffer);
				git_throw(git_remote_delete(git_remote_owner(remote), git_remote_name(remote)));
				return J_OK;
			});
	}

	int JOMAPI jadegit_remote_rename(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				string new_name;
				JADE_RETURN(paramGetString(*pParams, new_name));

				Remote remote(pBuffer);
				auto repo = git_remote_owner(remote);

				// Rename
				Transaction transaction;
				auto problems = make_unique<git_strarray>();
				git_throw(git_remote_rename(problems.get(), repo, git_remote_name(remote), new_name.c_str()));

				// Reload remote
				git_throw(git_remote_lookup(git_ptr(remote), repo, new_name.c_str()));
				transaction.commit();

				return J_OK;
			});
	}

	int JOMAPI jadegit_remote_sync(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		Remote remote(pBuffer);
		return Task::make(pReturn, [=](IProgress* progress)
			{
				return remote.sync(progress);
			});
	}

	int JOMAPI jadegit_remote_url(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return jadegit_proxy_mapping_string<Remote>(pBuffer, pParams, git_remote_url, [&](Remote& remote, const DskParam& value)
			{
				string url;
				JADE_RETURN(paramGetString(value, url));

				auto repo = git_remote_owner(remote);
				auto name = git_remote_name(remote);

				// Update URL
				git_throw(git_remote_set_url(repo, name, url.c_str()));

				// Reload remote
				git_throw(git_remote_lookup(git_ptr(remote), repo, name));

				return J_OK;
			});
	}
}