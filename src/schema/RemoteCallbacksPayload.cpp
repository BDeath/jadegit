#include "RemoteCallbacksPayload.h"

namespace JadeGit::Schema
{
	int credentials_cb(git_credential**cred, const char *url, const char *username_from_url, unsigned int allowed_types, void *payload)
	{
		return static_cast<RemoteCallbacksPayload*>(payload)->callbacks.credentials(cred, url, username_from_url, allowed_types);
	}

	int push_transfer_progress_cb(unsigned int current, unsigned int total, size_t bytes, void* payload)
	{
		return static_cast<RemoteCallbacksPayload*>(payload)->callbacks.push_transfer_progress(current, total, bytes);
	}

	int sideband_progress_cb(const char* str, int len, void* payload)
	{
		return static_cast<RemoteCallbacksPayload*>(payload)->callbacks.sideband_progress(str, len);
	}

	int transfer_progress_cb(const git_indexer_progress *stats, void *payload)
	{
		return static_cast<RemoteCallbacksPayload*>(payload)->callbacks.transfer_progress(stats);
	}
	
	RemoteCallbacksPayload::RemoteCallbacksPayload(git_remote_callbacks& cb, IRemoteCallbacks& callbacks) : callbacks(callbacks)
	{
		cb.payload = this;
		cb.credentials = &credentials_cb;
		cb.push_transfer_progress = &push_transfer_progress_cb;
		cb.sideband_progress = &sideband_progress_cb;
		cb.transfer_progress = &transfer_progress_cb;
	}
}