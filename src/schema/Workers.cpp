#include "Workers.h"
#include "main.h"
#include <jade/AppContext.h>
#include <jade/Exception.h>
#include <jade/ExceptionHandler.h>
#include <jade/String.h>
#include <schema/data/ChangeSet.h>
#include <schema/data/EntitySet.h>
#include <Log.h>
#include <sstream>

using namespace Jade;

namespace JadeGit::Schema::Backend
{
	class Background
	{
	public:
		void SignOn()
		{
			if (signedOn)
				return;

			signedOn = true;

			/* Check if JADE process hasn't already been signed on */
			if (AppContext::process() != NullDskObjectId)
				return;

			/* Sign on as a background process */
			DskParam pSchemaName;
			paramSetCString(pSchemaName, TEXT("JadeGitSchema"));
			DskParam pAppName;
			paramSetCString(pAppName, TEXT("JadeGitBackground"));
			DskParam pUserName;
			paramSetCString(pUserName, TEXT(""));
			DskParam pPassword;
			paramSetCString(pPassword, TEXT(""));
			DskParam pDbMode;
			paramSetInteger(pDbMode, DB_SHARED);
			DskParam pDbUsage;
			paramSetInteger(pDbUsage, DB_UPDATE);

			DskHandle process = { 0 };
			DskHandle security = { 0 };
			jade_throw(jomSignOn(&node, &process, &security, &pSchemaName, &pAppName, &pUserName, &pPassword, &pDbMode, &pDbUsage, __LINE__));

			/* Set flag to sign-off when thread detaches */
			signOff = true;

			/* Initialize application */
			jade_throw(jomInitializeApplication(nullptr, true, nullptr, __LINE__));

			/* Arm global exception handler */
			Jade::ExceptionHandler::arm(*AppContext::GetApp(), TEXT("externalExceptionHandler"));

			// Create/delete transients of critical classes to ensure JOM has opened associated class agents
			// This is needed to prevent lock exceptions when the JOM attempts to open class agents later when change is
			// being recorded, for a new class being added to JadeGitSchema (which exclusively locks class collection)
			Transient<GitChange> change;
			Transient<GitChangeSet> changeset;
			Transient<GitEntity> entity;
			Transient<GitEntitySet> entityset;

			log("Background worker started");
		}

		~Background()
		{
			if (!signOff)
				return;

			jomFinalizeApplication(nullptr, true, __LINE__);
			jomSignOff(NULL, __LINE__);

			signOff = false;
			signedOn = false;

			log("Background worker stopped");
		}

	private:
		bool signedOn = false;
		bool signOff = false;
	};
	thread_local static Background background;

	void SignOn()
	{
		background.SignOn();
	}

	int dependencies = 0;
	std::mutex mutex;
	ctpl::thread_pool workers(SignOn);

	void Startup()
	{
		std::lock_guard<std::mutex> lock(mutex);

		// Increase number of dependencies
		dependencies++;

		// Resize backend worker thread pool to match dependencies, up to maximum of five
		// TODO: In future, this could be changed to scale the number of workers proportionally
		workers.resize(dependencies <= 5 ? dependencies : 5);
	}

	void Shutdown()
	{
		std::lock_guard<std::mutex> lock(mutex);

		// Reduce number of dependencies
		--dependencies;

		// Resize backend worker thread pool to match
		workers.resize(dependencies <= 5 ? dependencies : 5);
	}
}