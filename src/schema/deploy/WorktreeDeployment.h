#pragma once
#include "Deployment.h"
#include <jadegit/git2.h>

namespace JadeGit::Schema
{
	class WorktreeData;

	class WorktreeDeployment : public GitDeployment
	{
	public:
		enum Action
		{
			Load = 1,
			Reset,
			Unload,
			Merge,
			Switch
		};

		using GitDeployment::GitDeployment;
		WorktreeDeployment();
		WorktreeDeployment(const GitDeployment& parent, const WorktreeData& worktree, Action action);

		Action action() const;
		std::unique_ptr<git_commit> commit(git_repository* repo) const;
		std::unique_ptr<git_reference> reference(git_repository* repo) const;
		WorktreeData worktree() const;

		void setAction(Action action) const;
		void setCommit(const git_commit* commit) const;
		void setCommit(const std::unique_ptr<git_commit>& commit) const;
		void setReference(const git_reference* ref) const;
		void setWorktree(const WorktreeData& worktree) const;

	protected:
		void executeExit() const final;
	};
}