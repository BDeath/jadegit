#include "RepositoryDeploymentBuilder.h"
#include "RepositoryDeployment.h"
#include "WorktreeDeployment.h"
#include <jade/Iterator.h>
#include <jadegit/build/Director.h>
#include <schema/data/Worktree.h>
#include <schema/data/refdb/NodeDict.h>
#include <vfs/GitDiffSource.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	RepositoryDeploymentBuilder::RepositoryDeploymentBuilder(const RepositoryData& repo) : RepositoryDeploymentBuilder(make_shared<GitRepositoryDeployment>(), repo) {}

	RepositoryDeploymentBuilder::RepositoryDeploymentBuilder(shared_ptr<GitRepositoryDeployment> deployment, const RepositoryData& repo) : DeploymentBuilder(deployment, repo), deployment(deployment) {}

	bool RepositoryDeploymentBuilder::load(IProgress* progress)
	{
		// Determine if load is required
		if (jade_repo.isActive())
			throw runtime_error("Repository is already loaded");

		if (!build(progress, false, false))
			return false;

		// Save details for post-load updates
		deployment->SetAction(GitRepositoryDeployment::Load);
		return true;
	}

	bool RepositoryDeploymentBuilder::reset(IProgress* progress)
	{
		// Determine if reset is required
		if (!jade_repo.isActive())
			throw runtime_error("Repository isn't active");

		if (!build(progress, true, false))
			return false;

		// Save details for post-load updates
		deployment->SetAction(GitRepositoryDeployment::Reset);
		return true;
	}

	bool RepositoryDeploymentBuilder::unload(IProgress* progress)
	{
		// Determine if unload is required
		if (!jade_repo.isActive())
			throw runtime_error("Repository isn't loaded");

		if (!build(progress, false, true))
			return false;

		// Save details for post-load updates
		deployment->SetAction(GitRepositoryDeployment::Unload);
		return true;
	}

	bool RepositoryDeploymentBuilder::build(IProgress* progress, bool reset, bool unload)
	{
		assert(!reset || !unload);

		unique_ptr<git_repository> repo = jade_repo.open();
		
		// Setup target to either load or unload
		GitFileSystem target(repo.get());

		// Iterate through all active worktrees
		WorktreeData worktree;
		Iterator<WorktreeData> iter(jade_repo, TEXT("worktrees"));
		while (iter.next(worktree))
		{
			if (!worktree.isActive())
				continue;

			// Add unmodified commit during reset
			if (reset)
			{
				target.add(worktree.refs().head<git_commit>(repo.get()));
			}
			else
			{
				// Check for uncommitted changes before unloading
				if (unload && worktree.isModified())
					throw runtime_error("Changes must be committed before unloading");

				// Add to target being either loaded or unloaded
				worktree.addSelf(repo.get(), target);
			}
		}

		// Build deployment to load or unload
		return Build::Director(GitDiffSource(repo.get(), unload ? &target : nullptr, unload ? nullptr : &target), progress).Build(*this);
	}
}