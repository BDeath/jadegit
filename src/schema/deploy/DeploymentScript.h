#pragma once
#include "DeploymentCommand.h"
#include <build/Script.h>

namespace JadeGit::Schema
{
	class GitDeploymentScript : public GitDeploymentCommand
	{
	public:
		using GitDeploymentCommand::GitDeploymentCommand;
		GitDeploymentScript(const GitDeploymentCommand& parent, const Build::Script& script);

	protected:
		void abort(Jade::Transaction& transaction, Jade::Loader& loader, bool& done) const final {};
		void execute(Jade::Transaction& transaction, Jade::Loader& loader) const final;
	};
}