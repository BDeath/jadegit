#pragma once
#include "DeploymentBuilder.h"
#include "RepositoryDeployment.h"
#include <schema/data/Repository.h>

namespace JadeGit::Schema
{
	class RepositoryDeploymentBuilder : public DeploymentBuilder
	{
	public:
		RepositoryDeploymentBuilder(const RepositoryData& repo);

		const std::shared_ptr<GitRepositoryDeployment> deployment;

		bool load(IProgress* progress);
		bool reset(IProgress* progress);
		bool unload(IProgress* progress);

	protected:
		RepositoryDeploymentBuilder(std::shared_ptr<GitRepositoryDeployment> deployment, const RepositoryData& repo);

		bool build(IProgress* progress, bool reset, bool unload);
	};
}