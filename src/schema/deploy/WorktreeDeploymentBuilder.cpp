#include "WorktreeDeploymentBuilder.h"
#include "WorktreeDeployment.h"
#include <jade/Iterator.h>
#include <jadegit/build/Director.h>
#include <schema/data/Worktree.h>
#include <schema/data/refdb/NodeDict.h>
#include <schema/Session.h>
#include <vfs/GitDiffSource.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{	
	WorktreeDeploymentBuilder::WorktreeDeploymentBuilder(const WorktreeData& worktree, git_repository* repo) : WorktreeDeploymentBuilder(make_shared<WorktreeDeployment>(), worktree, repo) {}
		
	WorktreeDeploymentBuilder::WorktreeDeploymentBuilder(std::shared_ptr<WorktreeDeployment> deployment, const WorktreeData& worktree, git_repository* repo) : DeploymentBuilder(deployment, worktree), deployment(deployment), worktree(worktree), repo(repo)
	{
		// Ensure repository is stable
		if (jade_repo.isUnstable())
			throw runtime_error("Repository is unstable (reset required)");
		
		// Ensure repository is active
		if (!jade_repo.isActive())
			throw runtime_error("Repository is inactive");

		deployment->setWorktree(worktree);
	}

	bool WorktreeDeploymentBuilder::load(IProgress* progress)
	{
		// Determine if load is required
		if (worktree.isActive())
			throw runtime_error("Worktree is already loaded");

		// Throw error if there is uncommitted changes (not supported yet)
		if (worktree.isModified())
			throw runtime_error("Reloading worktree in modified state isn't supported");

		// Lookup commit
		auto commit = worktree.refs().head<git_commit>(repo);

		// Save details for post-load updates
		deployment->setAction(WorktreeDeployment::Load);
		deployment->setCommit(commit);

		// Build deployment
		GitFileSystem target(move(commit));
		return build(progress, target);
	}

	bool WorktreeDeploymentBuilder::merge(IProgress* progress, unique_ptr<git_commit> commit)
	{
		// Save details for post-load updates
		deployment->setAction(WorktreeDeployment::Merge);
		deployment->setCommit(commit);

		// Build deployment
		GitFileSystem target(move(commit));
		return build(progress, target);
	}
	
	bool WorktreeDeploymentBuilder::reset(IProgress* progress, const git_commit* commit)
	{
		// Save details for post-load updates
		deployment->setAction(WorktreeDeployment::Reset);
		deployment->setCommit(commit);

		// Latest changes to be reversed
		GitFileSystem current(repo);
		worktree.addSelf(repo, current);

		// Target unmodified commit
		GitFileSystem target(commit);
		
		// Build deployment
		return Build::Director(GitDiffSource(repo, &current, &target), progress).Build(*this);
	}

	bool WorktreeDeploymentBuilder::switch_(IProgress* progress, const git_reference* branch)
	{
		assert(branch);

		// Prevent switching when there's uncommitted changes (stashing/applying to new branch not supported yet)
		if (worktree.isModified())
			throw runtime_error("Changes must be committed before switching");

		// Resolve target commit
		unique_ptr<git_reference> ref;
		unique_ptr<git_commit> commit;
		git_throw(git_reference_resolve(git_ptr(ref), branch));
		git_throw(git_commit_lookup(git_ptr(commit), repo, git_reference_target(ref.get())));

		// Save details for post-load updates
		deployment->setAction(WorktreeDeployment::Switch);
		deployment->setCommit(commit);	
		deployment->setReference(branch);

		// Build deployment
		GitFileSystem target(move(commit));
		return build(progress, target);
	}

	bool WorktreeDeploymentBuilder::switch_(IProgress* progress, const git_commit* commit)
	{
		assert(commit);

		// Prevent switching when there's uncommitted changes (stashing/applying to new branch not supported yet)
		if (worktree.isModified())
			throw runtime_error("Changes must be committed before switching");

		// Save details for post-load updates
		deployment->setAction(WorktreeDeployment::Switch);
		deployment->setCommit(commit);

		// Build deployment
		GitFileSystem target(commit);
		return build(progress, target);
	}

	bool WorktreeDeploymentBuilder::unload(IProgress* progress)
	{
		// Determine if unload is required
		if (!worktree.isActive())
			throw runtime_error("Branch isn't loaded");

		// Prevent unloading when there's uncommitted changes (stashing not supported yet)
		if (worktree.isModified())
			throw runtime_error("Changes must be committed before unloading branch");

		// Save details for post-load updates
		deployment->setAction(WorktreeDeployment::Unload);

		// Build deployment
		GitFileSystem target(repo);
		return build(progress, target, false);
	}

	bool WorktreeDeploymentBuilder::build(IProgress* progress, GitFileSystem& target, bool loading)
	{
		// TODO: Make experimental setting
		bool solo = true;

		GitFileSystem current(repo);
		
		// Add current worktree if it's already active
		if (this->worktree.isActive())
			worktree.addSelf(repo, current);

		// Iterate through all other active worktrees
		WorktreeData worktree;
		Iterator<WorktreeData> iter(RepositoryData(this->worktree), TEXT("worktrees"));
		while (iter.next(worktree))
		{
			if (worktree.oid == this->worktree.oid)
				continue;

			if (!worktree.isActive())
				continue;

			// Check for uncommitted changes if other worktree needs to be implicitly unloaded
			if (loading && solo)
			{
				if (worktree.isModified())
					throw runtime_error("Changes must be committed before switching worktrees");
			}
			// Add to target if it doesn't need to be unloaded
			else
				worktree.addSelf(repo, target);

			// Add to current
			worktree.addSelf(repo, current);
		}

		// Build deployment to transition from current to target
		if (!Build::Director(GitDiffSource(repo, &current, &target), progress).Build(*this))
			return false;

		// Setup commands to update worktrees unloaded implicitly
		if (loading && solo)
		{
			jade_throw(iter.reset());
			while (iter.next(worktree))
			{
				if (worktree.oid == this->worktree.oid)
					continue;

				if (!worktree.isActive())
					continue;

				WorktreeDeployment command(*deployment, worktree, WorktreeDeployment::Unload);
			}
		}

		return true;
	}
}