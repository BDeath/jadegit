#include "Session.h"
#include "data/Repository.h"
#include "data/User.h"
#include "data/Worktree.h"
#include <jade/Iterator.h>
#include <memory>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	static thread_local const Session* current = nullptr;

	class LocalSession : public Session
	{
	public:
		User user_;

		const User& user() const final { return user_; }
	};

	// Local session must be created on demand, it cannot be instantiated when library is loaded before GitUser class has been registered/loaded
	static thread_local unique_ptr<LocalSession> local;

	void Session::set(const Session* session)
	{
		current = session;
	}

	const Session& Session::get()
	{
		if (current)
			return *current;

		if (!local)
			local = make_unique<LocalSession>();

		return *local;
	}

	WorktreeData Session::getActiveWorktree(const RepositoryData& repo) const
	{
		// TODO: Implement support for setting an active worktree (currently, there can be just one)

		WorktreeData worktree;
		Iterator<WorktreeData> iter(repo, TEXT("worktrees"));
		while (iter.next(worktree))
		{
			if (worktree.isActive())
				return worktree;
		}

		throw runtime_error("No active worktree");
	}

	int JOMAPI jadegit_session_signon(DskBuffer *pBuffer, DskParam *pParams, DskParam *pReturn)
	{
		if (!local)
			local = make_unique<LocalSession>();

		return paramGetOid(*pParams, local->user_.oid);
	}
}