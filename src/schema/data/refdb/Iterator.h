#pragma once
#include "Backend.h"
#include <jade/Iterator.h>
#include <stack>

namespace JadeGit::Schema::Backend
{
	class jadegit_refdb_iterator : public git_reference_iterator
	{
	public:
		jadegit_refdb_iterator(const jadegit_refdb_backend& backend, const char* glob);

		bool next(ReferenceData& reference);

	private:
		std::stack<Jade::Iterator<ReferenceNode>> iters;
	};
}