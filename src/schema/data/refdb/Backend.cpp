#include "Backend.h"
#include "Iterator.h"
#include <git2/sys/errors.h>
#include <Exception.h>

using namespace std;

namespace JadeGit::Schema::Backend
{
	template <typename T>
	static int wrapper(git_refdb_backend* backend, T func)
	{
		try
		{
			return func(*static_cast<jadegit_refdb_backend*>(backend));
		}
		catch (const exception& e)
		{
			git_error_set_str(GIT_ERROR_REFERENCE, e.what());
		}

		return GIT_ERROR;
	}

	int jadegit_refdb_backend__exists(int* exists, git_refdb_backend* backend, const char* ref_name)
	{
		return wrapper(backend, [&](jadegit_refdb_backend& backend)
			{
				*exists = backend.lookup(ref_name).isNull() ? 0 : 1;
				return GIT_OK;
			});
	}

	int jadegit_refdb_backend__lookup(git_reference** out, git_refdb_backend* backend, const char* ref_name)
	{
		return wrapper(backend, [&](jadegit_refdb_backend& backend)
			{
				auto reference = backend.lookup(ref_name);
				if (reference.isNull())
				{
					git_error_set(GIT_ERROR_REFERENCE, "reference '%s' not found", ref_name);
					return GIT_ENOTFOUND;
				}

				reference.read(out);
				return GIT_OK;
			});
	}

	int jadegit_refdb_backend__iterator(git_reference_iterator** iter, struct git_refdb_backend* backend, const char* glob)
	{
		return wrapper(backend, [&](jadegit_refdb_backend& backend)
			{
				*iter = new jadegit_refdb_iterator(backend, glob);
				return GIT_OK;
			});
	}

	int jadegit_refdb_backend__write(git_refdb_backend* backend, const git_reference* ref, int force, const git_signature* who, const char* message, const git_oid* old, const char* old_target)
	{
		return wrapper(backend, [&](jadegit_refdb_backend& backend)
			{
				if (!backend.write(ref, force))
				{
					git_error_set(GIT_ERROR_REFERENCE, "failed to write reference '%s': a reference with that name already exists.", git_reference_name(ref));
					return GIT_EEXISTS;
				}

				return GIT_OK;
			});
	}

	int jadegit_refdb_backend__rename(git_reference** out, git_refdb_backend* backend, const char* old_name, const char* new_name, int force, const git_signature* who, const char* message)
	{
			/*			
				GitReference reference;
				if(!reference.lookup(backend->repo, old_name))
					return GIT_ENOTFOUND;

				// TODO: Handle updating worktree references where branch may be checked out

				if (!reference.rename(new_name, force))
				{
					git_error_set_str(GIT_ERROR_REFERENCE, "Reference with that name already exists");
					return GIT_EEXISTS;
				}

				reference.read(out);
				return GIT_OK;
			*/

		return wrapper(backend, [&](jadegit_refdb_backend& backend)
			{
				throw unimplemented_feature("Renaming references");
				return GIT_ERROR;
			});
	}

	int jadegit_refdb_backend__del(git_refdb_backend* backend, const char* ref_name, const git_oid* old, const char* old_target)
	{
		return wrapper(backend, [&](jadegit_refdb_backend& backend)
			{
				backend.remove(ref_name);
				return GIT_OK;
			});
	}

	void jadegit_refdb_backend__free(git_refdb_backend* backend)
	{
		delete static_cast<jadegit_refdb_backend*>(backend);
	}

	int jadegit_refdb_backend__has_log(git_refdb_backend* _backend, const char* refname)
	{
		return 0;
	}

	int jadegit_refdb_backend__ensure_log(git_refdb_backend* _backend, const char* refname)
	{
		return GIT_ERROR;
	}

	int jadegit_refdb_backend__reflog_read(git_reflog** out, git_refdb_backend* _backend, const char* name)
	{
		return GIT_ERROR;
	}

	int jadegit_refdb_backend__reflog_write(git_refdb_backend* _backend, git_reflog* reflog)
	{
		return GIT_ERROR;
	}

	int jadegit_refdb_backend__reflog_rename(git_refdb_backend* _backend, const char* old_name, const char* new_name)
	{
		return GIT_ERROR;
	}

	int jadegit_refdb_backend__reflog_delete(git_refdb_backend* _backend, const char* name)
	{
		return GIT_ERROR;
	}

	jadegit_refdb_backend::jadegit_refdb_backend(const RepositoryData& repo, const WorktreeData& worktree) : repo(repo), worktree(worktree)
	{
		git_throw(git_refdb_init_backend(this, GIT_REFDB_BACKEND_VERSION));

		git_refdb_backend::exists = &jadegit_refdb_backend__exists;
		git_refdb_backend::lookup = &jadegit_refdb_backend__lookup;
		git_refdb_backend::iterator = &jadegit_refdb_backend__iterator;
		git_refdb_backend::write = &jadegit_refdb_backend__write;
		git_refdb_backend::del = &jadegit_refdb_backend__del;
		git_refdb_backend::rename = &jadegit_refdb_backend__rename;
		git_refdb_backend::compress = nullptr;
		git_refdb_backend::free = &jadegit_refdb_backend__free;

		git_refdb_backend::has_log = &jadegit_refdb_backend__has_log;
		git_refdb_backend::ensure_log = &jadegit_refdb_backend__ensure_log;
		git_refdb_backend::reflog_read = &jadegit_refdb_backend__reflog_read;
		git_refdb_backend::reflog_write = &jadegit_refdb_backend__reflog_write;
		git_refdb_backend::reflog_rename = &jadegit_refdb_backend__reflog_rename;
		git_refdb_backend::reflog_delete = &jadegit_refdb_backend__reflog_delete;
	}

	bool is_worktree_ref(const string& name)
	{
		return !name.starts_with("refs/") || name.starts_with("refs/bisect/");
	}

	ReferenceNodeDict jadegit_refdb_backend::refs(string& name) const
	{
		// Handle accessing main-worktree refs
		if (name.starts_with("main-worktree/"))
		{
			name = name.substr(14);
			return repo.refs();
		}

		// Handle accessing other worktree refs
		if (name.starts_with("worktrees/"))
			throw unimplemented_feature("Accessing refs for another worktree");

		// Handle per-worktree
		if (!worktree.isNull() && is_worktree_ref(name))
			return worktree.refs();
		
		// Handle shared refs
		return repo.refs();
	}

	ReferenceData jadegit_refdb_backend::lookup(string name) const
	{
		return refs(name).lookup(name);
	}

	void jadegit_refdb_backend::remove(string name) const
	{
		return refs(name).remove(name);
	}

	bool jadegit_refdb_backend::write(const git_reference* ref, bool force) const
	{
		string name(git_reference_name(ref));
		return refs(name).write(ref, force);
	}
}