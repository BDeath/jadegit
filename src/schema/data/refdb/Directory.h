#pragma once
#include "Node.h"

namespace JadeGit::Schema
{
	class ReferenceNodeDict;

	class ReferenceDirectory : public ReferenceNode
	{
	public:
		using ReferenceNode::ReferenceNode;
		ReferenceDirectory();

		ReferenceNodeDict children() const;
	};
}