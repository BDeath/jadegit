#pragma once
#include <schema/Object.h>

namespace JadeGit::Schema
{
	class ReferenceNode : public Object
	{
	public:
		using Object::Object;

		std::string path() const;
		bool isDirectory() const;
	};
}