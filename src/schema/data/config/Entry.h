#pragma once
#include "Data.h"
#include <git2/config.h>
#include <map>
#include <string>

namespace JadeGit::Schema::Backend
{
	class jadegit_config_entry : public git_config_entry
	{
	public:
		jadegit_config_entry(std::string name, std::string value);

		std::string name;
		std::string value;
		jadegit_config_entry* next = nullptr;
	};

	class jadegit_config_backend;

	class jadegit_config_entries
	{
	public:
		jadegit_config_entries(const jadegit_config_backend& backend);
		~jadegit_config_entries();

		jadegit_config_entry* get(const char* key) const;

		jadegit_config_entry* first = nullptr;
		jadegit_config_entry* last = nullptr;
		std::map<std::string, jadegit_config_entry*> index;

	protected:
		void add(const std::string& name, const std::string& value);
		void copy(const ConfigData& data, std::string path);
		void copy(const ConfigDataDict& data, const std::string& path);
	};
}