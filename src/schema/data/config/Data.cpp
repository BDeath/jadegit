#include "Data.h"
#include <jade/Iterator.h>
#include <schema/ObjectRegistration.h>
#include <regex>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	static GitObjectRegistration<ConfigData> registration(TEXT("ConfigData"));

	ConfigData::ConfigData(const DskObjectId& parent, const std::string& name) : Object(registration)
	{
		jade_throw(createObject());
		setProperty(TEXT("name"), name);
		jade_throw(setProperty(TEXT("parent"), parent));
	}

	ConfigDataDict ConfigData::children() const
	{
		return getProperty<ConfigDataDict>(TEXT("children"));
	}

	bool ConfigData::empty() const
	{
		return children().isEmpty() && values().isEmpty();
	}

	string ConfigData::name() const
	{
		return getProperty<string>(TEXT("name"));
	}

	void ConfigData::remove_multivar(const char* regexp) const
	{
		regex filter(regexp);

		auto values = this->values();
		auto index = values.size();

		DskParamString pValue;
		DskParam pIndex;

		while (index > 0)
		{
			// Retrieve value for current index
			jade_throw(paramSetInteger64(pIndex, index));
			jade_throw(values.sendMsg(MTH_Array_at, &pIndex, &pValue));

			string value;
			jade_throw(paramGetString(pValue, value));

			// Remove values matching regular expression
			if (regex_match(value, filter))
				jade_throw(values.sendMsg(MTH_Array_removeAt, &pIndex, &pValue));

			index--;
		}
	}

	void ConfigData::set(const string& value) const
	{
		DskParam pIndex;
		paramSetInteger64(pIndex, 1);

		DskParamCString pValue(value);

		DskParam params;
		paramSetParamList(params, &pIndex, &pValue);

		jade_throw(values().sendMsg(MTH_Array_atPut, &params));
	}

	void ConfigData::set_multivar(const char* regexp, const std::string& value) const
	{
		remove_multivar(regexp);
		values().add(widen(value).c_str());
	}

	Collection<DskArray> ConfigData::values() const
	{
		return getProperty<Collection<DskArray>>(TEXT("values"));
	}

	void ConfigDataDict::cleanup() const
	{
		Iterator<ConfigData> iter(*this);
		ConfigData data;
		while (iter.next(data))
		{
			// Cleanup children recursively
			data.children().cleanup();

			// Delete empty nodes
			if (data.empty())
				data.deleteObject();
		}
	}

	ConfigData ConfigDataDict::getAtKey(const std::string& name) const
	{
		ConfigData data;
		jade_throw(DskMemberKeyDictionary::getAtKey(widen(name).c_str(), data));
		return data;
	}

	ConfigData ConfigDataDict::lookup(const std::string& path, bool instantiate) const
	{
		auto pos = path.find(".");
		if (pos == path.length())
			throw invalid_argument("Invalid config name");

		auto name = path.substr(0, pos);
		auto data = getAtKey(name);

		if (data.isNull())
		{
			if (!instantiate)
				return data;

			DskObjectId parent;
			oid.getParent(parent);

			data = ConfigData(parent, name);
		}

		return (pos == path.npos) ? data : data.children().lookup(path.substr(pos + 1), instantiate);
	}

	void ConfigDataDict::remove(const char* key) const
	{
		auto data = lookup(key);
		if (data.isNull())
			return;

		data.deleteObject();
		cleanup();
	}

	void ConfigDataDict::remove_multivar(const char* key, const char* regexp) const
	{
		auto data = lookup(key);
		if (data.isNull())
			return;

		data.remove_multivar(regexp);

		if (data.empty())
		{
			data.deleteObject();
			cleanup();
		}
	}

	void ConfigDataDict::set(const char* key, const char* value) const
	{
		lookup(key, true).set(value);
	}

	void ConfigDataDict::set_multivar(const char* key, const char* regexp, const char* value) const
	{
		lookup(key, true).set_multivar(regexp, value);
	}
}