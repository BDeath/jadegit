#include "Schema.h"
#include "Repository.h"
#include "Root.h"
#include <extract/Schema.h>
#include <jade/AppContext.h>
#include <jade/Iterator.h>
#include <jade/Transaction.h>
#include <schema/ObjectRegistration.h>
#include <schema/Workers.h>
#include <Log.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	static GitObjectRegistration<GitSchema> registration(TEXT("GitSchema"));

	GitSchema::GitSchema() : GitEntity(registration) {}

	GitSchema::GitSchema(const Root& root, const string& name) : GitSchema(root, name, RepositoryData(NullDskObjectId)) {}

	GitSchema::GitSchema(const Root& root, const string& name, const RepositoryData& repo) : GitSchema(root, name, repo, GitSchema(NullDskObjectId)) {}

	GitSchema::GitSchema(const Root& root, const string& name, const RepositoryData& repo, const GitSchema& prior) : GitSchema()
	{
		jade_throw(createObject());
		jade_throw(setProperty(TEXT("kind"), DSKSCHEMA));
		setProperty(TEXT("name"), name);
		jade_throw(setProperty(TEXT("prior"), prior));
		jade_throw(setProperty(TEXT("repo"), repo));
		jade_throw(setProperty(TEXT("root"), root));
	}

	void GitSchema::exclude(const string& name)
	{
		// Redirect to backend thread if required
		if (AppContext::IsSystem())
		{
			assert(Backend::workers.size());
			return Backend::workers.push(bind(&GitSchema::exclude, ref(name))).get();
		}

		Transaction transaction;
		GitSchema excluded(Root::get(), name);
		transaction.commit();
	}

	void GitSchema::forget(const string& name)
	{
		// Redirect to backend thread if required
		if (AppContext::IsSystem())
		{
			assert(Backend::workers.size());
			return Backend::workers.push(bind(&GitSchema::forget, ref(name))).get();
		}

		if (unique_ptr<GitSchema> schema = Root::get().getSchema(name))
		{
			if (!schema->repo().isNull())
				throw runtime_error("Schema cannot be forgotten as it's associated with a repository");

			Transaction transaction;
			jade_throw(schema->deleteObject());
			transaction.commit();
		}
	}

	bool GitSchema::isRegistered(const string& name)
	{
		return !!Root::get().getSchema(name);
	}

	RepositoryData GitSchema::repo() const
	{
		return getProperty<RepositoryData>(TEXT("repo"));
	}

	unique_ptr<GitSchema> GitSchema::resolve(const Extract::Entity& source, const string& name)
	{
		ObjectVersionState versioned = source.getVersionState();
		DskObjectId current = NullDskObjectId;
		DskObjectId latest = NullDskObjectId;

		switch (versioned)
		{
		case ObjectVersionState::OBJVERSTATE_VERSIONED_CURRENT:
		{
			DskObject object;
			jade_throw(source.getNextVersionObject(&object));
			latest = object.oid;
		}
		case ObjectVersionState::OBJVERSTATE_CURRENT_ONLY:
			current = source.oid;
			break;

		case ObjectVersionState::OBJVERSTATE_VERSIONED_LATEST:
		{
			DskObject object;
			jade_throw(source.getPriorVersionObject(&object));
			current = object.oid;
		}
		case ObjectVersionState::OBJVERSTATE_LATEST_ONLY:
			latest = source.oid;
			break;
		}

		if (AppContext::IsSystem())
		{
			assert(Backend::workers.size());
			return Backend::workers.push(bind(&GitSchema::resolver, ref(name), versioned, ref(current), ref(latest))).get();
		}
		else
			return GitSchema::resolver(name, versioned, current, latest);
	}

	unique_ptr<GitSchema> GitSchema::resolver(const string& name, ObjectVersionState versioned, const DskObjectId& current, const DskObjectId& latest)
	{
		auto schema = Root::get().getSchema(name);
		if (!schema)
			throw runtime_error("Schema has not been initialized");

		// Get associated repository
		auto repo = schema->repo();
		if (repo.isNull())
			// Ignore excluded schema changes
			return nullptr;

		// Ensure repository is in a stable state
		// TODO: Obtain repository level deployment lock?
		if (repo.isUnstable())
			throw runtime_error("Repository [" + repo.GetName() + "] is unstable (reset required)");

		// Ensure repository is in an active state
		if (!repo.isActive())
			throw runtime_error("Repository [" + repo.GetName() + "] is inactive");

		// Ensure schema has been transitioned, swapping schema to next version if required
		transition(schema, repo, name, versioned, current, latest);
		return schema;
	}

	void GitSchema::selected(const string& name)
	{
		if (AppContext::IsSystem())
		{
			assert(Backend::workers.size());
			return Backend::workers.push(bind(&GitSchema::selected, ref(name))).get();
		}

		auto target = Root::get().getSchema(name);
		if (!target)
			return;

		DskSchema rootSchema(&RootSchemaOid);
		if (!transition(target, rootSchema))
			return;

		GitSchema schema;
		Iterator<GitSchema> schemas(Root::get(), TEXT("schemas"));
		while (schemas.next(schema))
		{
			if (!schema.isEqual(*target))
			{
				auto ptr = make_unique<GitSchema>(schema.oid);
				transition(ptr, rootSchema);
			}
		}
	}

	GitSchema::Status GitSchema::status(const string& name)
	{
		// Get registered schema
		auto schema = Root::get().getSchema(name);

		// Schema is included in source control if repo is set
		if (schema && !schema->repo().isNull())
			return Status::Included;

		// Block supplied schemas
		// NOTE: This applies to JadeGitSchema & those supplied/maintained by JADE
		if (Extract::Schema::isSupplied(name))
			return Status::Blocked;

		// Schema has been excluded from source control if registered without repo set
		if (schema)
			return Status::Excluded;

		// Schema status is unknown as it hasn't been configured yet
		return Status::Unknown;
	}

	bool GitSchema::transition(unique_ptr<GitSchema>& schema, const DskSchema& rootSchema)
	{
		// Ignore excluded schemas
		auto repo = schema->repo();
		if (repo.isNull())
			return false;

		DskSchema current;
		auto name = schema->GetName();
		jade_throw(rootSchema.findSchema(widen(name).c_str(), current));
		if (current.isNull())
			return false;

		ObjectVersionState versioned = current.getVersionState();
		DskSchema latest;

		if (versioned)
		{
			assert(versioned == ObjectVersionState::OBJVERSTATE_VERSIONED_CURRENT);
			jade_throw(current.getNextVersionObject(&latest));
		}

		return transition(schema, repo, name, versioned, current.oid, latest.oid);
	}

	bool GitSchema::transition(unique_ptr<GitSchema>& schema, const RepositoryData& repo, const string& name, ObjectVersionState versioned, const DskObjectId& current, const DskObjectId& latest)
	{
		bool result = false;
		DskObjectId source = NullDskObjectId;

		// Check for next version
		if (auto next = schema->next<GitSchema>())
		{
			next->getProperty(TEXT("source"), source);

			// Transition if required
			if (source == current)
			{
				result = true;

				Transaction transaction;
				jade_throw(schema->setProperty(TEXT("repo"), NullDskObjectId));
				jade_throw(schema->setProperty(TEXT("root"), NullDskObjectId));
				schema->transition(GitEntity(), *next);
				transaction.commit();

				schema.swap(next);
			}
			// Discard if invalid
			else if (source != latest)
			{
				result = true;

				Transaction transaction;
				jade_throw(next->setProperty(TEXT("repo"), NullDskObjectId));
				jade_throw(next->setProperty(TEXT("root"), NullDskObjectId));
				jade_throw(next->setProperty(TEXT("prior"), NullDskObjectId));
				jade_throw(next->deleteObject());
				transaction.commit();
			}
			// Swap if applicable
			else if (versioned & ObjectVersionState::OBJVERSTATE_VERSIONED_LATEST)
			{
				schema.swap(next);
				return false;
			}
		}

		// Ensure current version source is up-to-date
		schema->getProperty(TEXT("source"), source);
		if (source != current)
		{
			result = true;

			if (!source.isNull())
				LOG_WARNING(name << " source mis-match");

			Transaction transaction;
			jade_throw(schema->setProperty(TEXT("source"), current));
			transaction.commit();
		}

		// Setup next version if applicable
		if (versioned & ObjectVersionState::OBJVERSTATE_VERSIONED_LATEST)
		{
			result = true;

			Transaction transaction;
			auto next = make_unique<GitSchema>(Root::get(), name, repo, *schema);
			jade_throw(next->setProperty(TEXT("source"), latest));
			transaction.commit();

			schema.swap(next);
		}

		return result;
	}
}