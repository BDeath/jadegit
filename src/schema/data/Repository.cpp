#include "Repository.h"
#include "Root.h"
#include "Schema.h"
#include "Worktree.h"
#include <jadegit/data/Assembly.h>
#include <extract/SchemaIterator.h>
#include <jade/AppContext.h>
#include <jade/Transaction.h>
#include <git2/sys/config.h>
#include <git2/sys/errors.h>
#include <git2/sys/repository.h>
#include <git2/sys/refdb_backend.h>
#include <registry/Commit.h>
#include <registry/Repository.h>
#include <registry/Root.h>
#include <registry/Schema.h>
#include <registry/storage/Database.h>
#include <schema/data/config/Backend.h>
#include <schema/data/odb/Backend.h>
#include <schema/data/refdb/Backend.h>
#include <schema/deploy/RepositoryDeploymentBuilder.h>
#include <schema/Exception.h>
#include <schema/ObjectRegistration.h>
#include <schema/RemoteCallbacks.h>
#include <schema/RemoteCallbacksPayload.h>
#include <schema/Repository.h>
#include <schema/Session.h>
#include <schema/Task.h>
#include <vfs/FileSignature.h>
#include <vfs/GitFileSystem.h>
#include <Exception.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	static GitObjectRegistration<RepositoryData> registration(TEXT("RepositoryData"));

	RepositoryData::RepositoryData() : Object(registration) {}

	RepositoryData::RepositoryData(const IGitRepositoryData &child)
	{
		child.GetRepository(*this);
	}

	RepositoryData::RepositoryData(const Root& root, const string& name) : RepositoryData()
	{
		// Create repository
		createObject();
		setProperty(TEXT("name"), name);
		setProperty(TEXT("root"), root);

		// Open repository
		auto repo = open();

		// Open config
		unique_ptr<git_config> config;
		git_throw(git_repository_config(git_ptr(config), repo.get()));

		// Set default/required configuration
		git_throw(git_config_set_bool(config.get(), "core.autocrlf", true));
	}

	unique_ptr<git_repository> repo_initial_open(RepositoryData& data, const string& name)
	{
		// Create repository
		data = RepositoryData(Root::get(), name);

		// Create first worktree
		WorktreeData worktree(data);

		// Open repository for worktree
		return data.open(worktree);
	}

	int repo_clone_setup(git_repository** out, const char* path, int bare, void* payload)
	{
		try
		{
			*out = repo_initial_open(*static_cast<RepositoryData*>(payload), path).release();
			return GIT_OK;
		}
		catch (const exception& e)
		{
			git_error_set_str(GIT_ERROR_REPOSITORY, e.what());
			return GIT_ERROR;
		}
	}

	string RepositoryData::GetName() const
	{
		return getProperty<string>(TEXT("name"));
	}

	void RepositoryData::GetRepository(RepositoryData& repo) const
	{
		repo = *this;
	}

	bool RepositoryData::GetSchema(const string& name, GitSchema& schema) const
	{
		DskMemberKeyDictionary schemas;
		getProperty(TEXT("schemas"), schemas);

		ClassNumber classNo = schema.oid.classNo;
		schemas.getAtKey(widen(name).c_str(), schema);

		if (!schema.isNull())
			return true;

		schema.oid.classNo = classNo;
		return false;
	}

	RepositoryData::State RepositoryData::GetState() const
	{
		Byte state;
		getProperty(TEXT("state"), &state);
		return (RepositoryData::State)state;
	}

	void RepositoryData::SetState(State value)
	{
		jade_throw(setProperty(TEXT("state"), (Byte)value));
	}

	bool RepositoryData::clone(const string& remote, string name, const string& access_token, const git_oid* current_commit_id, IProgress* progress)
	{
		if (remote.empty())
			throw invalid_argument("Remote repository path required");

		if (name.empty())
			name = Registry::repo_name(remote);

		if (progress && !progress->start(1, "Cloning " + name))
			return false;

		// Start transaction (entire creation needs to be atomic)
		Transaction transaction;

		// Initialize registry for prior version if specified
		if (current_commit_id)
		{
			using namespace JadeGit::Registry;

			RepositoryT repo;
			repo.name = name;
			repo.origin = remote;
			repo.latest.push_back(make_commit(*current_commit_id));

			Registry::Root::load(repo, false);
		}

		// Setup clone options
		git_clone_options clone_opts = GIT_CLONE_OPTIONS_INIT;
		clone_opts.repository_cb = repo_clone_setup;
		clone_opts.repository_cb_payload = this;

		// Setup remote callbacks
		RemoteCallbacks callbacks(progress);
		RemoteCallbacksPayload callbacks_payload(clone_opts.fetch_opts.callbacks, callbacks);

		// Suppress updating fetch head
		clone_opts.fetch_opts.update_fetchhead = 0;

		// Auto detect proxy
		clone_opts.fetch_opts.proxy_opts.type = git_proxy_t::GIT_PROXY_AUTO;

		// Do not bypass git-aware transport when cloning repository from local filesystem
		// Prevents attempt to copy object database folders directly, which fails as repository is being created in the database (no directories) 
		clone_opts.local = GIT_CLONE_NO_LOCAL;
		
		// Use vector to collect custom headers
		vector<char*> custom_headers;

		// Use access token if supplied
		string authorization_header;
		if (!access_token.empty())
		{
			// Disable credentials callback
			clone_opts.fetch_opts.callbacks.credentials = nullptr;

			// Define authorization header with base64 encoded <user>:<token>
			string usertoken = ":" + access_token;
			authorization_header = "Authorization: Basic " + static_cast<string>(Data::Binary(reinterpret_cast<const Byte*>(usertoken.c_str()), usertoken.size()));;
			custom_headers.push_back(const_cast<char*>(authorization_header.c_str()));
		}

		// Populate custom headers
		clone_opts.fetch_opts.custom_headers = { custom_headers.data(), custom_headers.size() };

		// Clone remote repository
		unique_ptr<git_repository> repo;
		if (git_throw(git_clone(git_ptr(repo), remote.c_str(), name.c_str(), &clone_opts)) == GIT_EUSER)
			return false;	// Operation cancelled via callback

		// Set unborn repository as active upfront (no deployment required)
		if (git_repository_head_unborn(repo.get()))
		{
			SetState(State::Active);
		}
		else
		{
			// Lookup existing registration for repository
			Registry::DatabaseStorage storage;
			Registry::Root registry(storage);
			Registry::RepositoryT* registration = registry.find_repo([&](Registry::RepositoryT& lhs) { return Registry::repo_match(lhs, *repo); });

			// Restore current commit
			if (registration && !registration->latest.empty())
			{
				// Support limited to one current commit
				if (registration->latest.size() > 1)
					throw unimplemented_feature("Restoring repository with multiple commits previously deployed");

				// Lookup commit
				unique_ptr<git_commit> commit = Registry::commit_lookup(*repo, registration->latest.front());

				// Set HEAD to current commit
				// NOTE: Worktree index is initialized to match on first use
				unique_ptr<git_reference> head;
				git_reference_create(git_ptr(head), repo.get(), "HEAD", git_commit_id(commit.get()), true, nullptr);

				// Register schemas for prior version if supplied
				if (registration->schemas.empty() && current_commit_id)
				{
					// Setup assembly to query schemas
					GitFileSystem fs(move(commit), FileSignature::suppressed());
					Data::Assembly assembly(fs);

					// Iterate installed schemas
					Extract::Schema installed;
					Extract::SchemaIterator iter;
					while (iter.next(installed))
					{
						// Register installed schema if sourced from repository
						auto name = installed.getName();
						auto schema = Data::Entity::resolve<Data::Schema>(assembly, name);
						if (schema && !schema->isStatic())
							registration->schemas.push_back(Registry::make_schema(name));
					}

					// Save registry changes
					registry.save(storage);
				}

				// Restore schema associations
				for (auto& schema : registration->schemas)
					GitSchema association(Root::get(), schema.name, *this);

				// Set repository as active (no deployment required)
				SetState(State::Active);
			}
		}

		// Commit transaction
		transaction.commit();

		// Sucesss
		return (!progress || progress->finish());
	}

	bool RepositoryData::load(IProgress* progress) const
	{
		if (progress && !progress->start(2, "Loading " + GetName()))
			return false;

		// Build deployment
		Transaction transaction;
		RepositoryDeploymentBuilder builder(*this);
		if (!builder.load(progress))
			return false;

		transaction.commit();

		// Deploy
		if (!builder.deployment->execute(progress))
			return false;

		return (!progress || progress->finish());
	}

	unique_ptr<git_repository> RepositoryData::open() const
	{
		return open(WorktreeData());
	}

	unique_ptr<git_repository> RepositoryData::open(const WorktreeData& worktree) const
	{
		// TODO: Implement changes needed to support multiple users/thinclients
		// Need to retrieve/copy global user configuration file to local temp file that can used by libgit2 running with the application server
		// Until then, we'll throw an error indicating it's not supported yet
		if (isApplicationServer(AppContext::GetNodeType()))
			throw unimplemented_feature("Opening user configuration on application server");

		// Initialise system/global config
		unique_ptr<git_config> config;
		git_throw(git_config_open_default(git_ptr(config)));

		// Initialise blank repository
		unique_ptr<git_repository> repo;
		git_throw(git_repository_new(git_ptr(repo)));

		// Set repository config
		git_repository_set_config(repo.get(), config.get());

		// Add repository config backend 
		Backend::jadegit_config_backend::add(config.get(), git_config_level_t::GIT_CONFIG_LEVEL_LOCAL, repo.get(), oid);	

		// Initialise odb backend
		unique_ptr<git_odb> odb;
		git_throw(git_odb_new(git_ptr(odb)));
		git_throw(git_odb_add_backend(odb.get(), new Backend::jadegit_odb_backend(oid), 1));
		git_throw(git_repository_set_odb(repo.get(), odb.get()));

		// Initialise refdb backend
		unique_ptr<git_refdb> refdb;
		git_throw(git_refdb_new(git_ptr(refdb), repo.get()));		
		git_throw(git_refdb_set_backend(refdb.get(), new Backend::jadegit_refdb_backend(*this, worktree)));
		git_throw(git_repository_set_refdb(repo.get(), refdb.get()));

		// Success
		return repo;
	}

	ReferenceNodeDict RepositoryData::refs() const
	{
		return ReferenceNodeDict(*this, TEXT("references"));
	}

	bool RepositoryData::reset(IProgress* progress) const
	{
		if (progress && !progress->start(2, "Resetting " + GetName()))
			return false;

		// Build deployment
		Transaction transaction;
		RepositoryDeploymentBuilder builder(*this);
		if (!builder.reset(progress))
			return false;
		transaction.commit();

		// Deploy
		if (!builder.deployment->execute(progress))
			return false;

		return (!progress || progress->finish());
	}

	void RepositoryData::restore(const Root& root, Registry::RepositoryT& registry)
	{
		// Check repository can be initialised based on registry data
		if (registry.name.empty() || registry.origin.empty() || registry.latest.empty() || registry.schemas.empty())
			return;

		// Skip jadegit repository (can be restored via clone)
		if (registry.schemas.front().name == "JadeGitSchema")
			return;

		// Create repository
		RepositoryData data(root, registry.name);

		// Configure origin
		auto repo = data.open();
		unique_ptr<git_remote> remote;
		git_throw(git_remote_create(git_ptr(remote), repo.get(), "origin", registry.origin.c_str()));

		// Create worktree for each commit
		for (auto& commit : registry.latest)
			WorktreeData worktree(data, commit);

		// Restore schema associations
		for (auto& schema : registry.schemas)
			GitSchema association(root, schema.name, data);

		// Set repository as active (no deployment required)
		data.SetState(State::Active);

		// Update registry data so we don't try restore again in future (unreliable once development underway)
		registry.previous = registry.latest;
		registry.latest.clear();
	}

	bool RepositoryData::unload(IProgress* progress) const
	{
		if (progress && !progress->start(2, "Unloading " + GetName()))
			return false;

		// Build deployment
		Transaction transaction;
		RepositoryDeploymentBuilder builder(*this);
		if (!builder.unload(progress))
			return false;
		transaction.commit();

		// Deploy
		if (!builder.deployment->execute(progress))
			return false;

		return (!progress || progress->finish());
	}

	int JOMAPI jadegit_repo_clone(DskBuffer *pBuffer, DskParam *pParams, DskParam *pReturn)
	{
		DskParam* pName;
		DskParam* pRemote;
		DskParam* pLoad;
		JADE_RETURN(paramGetParameter(*pParams, 1, pName));
		JADE_RETURN(paramGetParameter(*pParams, 2, pRemote));
		JADE_RETURN(paramGetParameter(*pParams, 3, pLoad));

		string name;
		JADE_RETURN(paramGetString(*pName, name));

		string remote;
		JADE_RETURN(paramGetString(*pRemote, remote));

		bool load = false;
		JADE_RETURN(paramGetBoolean(*pLoad, load));

		return Task::make(pReturn, [=](IProgress* progress) 
			{
				if (progress && !progress->start(load ? 2 : 1))
					return false;
				
				// Clone repository
				RepositoryData repo;
				if (!repo.clone(remote, name, string(), nullptr, progress))
					return false;

				// Load repository
				if (load && !repo.isActive() && !repo.load(progress))
					return false;

				// Success
				return (!progress || progress->finish());
			});
	}
	
	int JOMAPI jadegit_repo_init(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				string name;
				paramGetString(*pParams, name);

				// Determine correct repository sub-class to create
				DskClass klass(&pBuffer->oid);
				ClassNumber classNo;
				jade_throw(klass.getNumber(&classNo));

				// Start transaction (entire creation needs to be atomic)
				Transaction transaction;

				// Create/open repo
				RepositoryData repo(classNo);
				unique_ptr<git_repository> git = repo_initial_open(repo, name.c_str());

				// Set repository as active upfront (no deployment necessary)
				repo.SetState(RepositoryData::State::Active);

				// Commit transaction
				transaction.commit();

				// Set return value
				return paramSetOid(pReturn, repo.oid);
			});
	}

	int JOMAPI jadegit_repo_load(DskBuffer *pBuffer, DskParam *pParams, DskParam *pReturn)
	{
		RepositoryData repo(&pBuffer->oid);
		return Task::make(pReturn, [repo](IProgress* progress) { return repo.load(progress); });
	}

	int JOMAPI jadegit_repo_open(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				RepositoryData data(pBuffer);
				Repository repo(data.open());
				jade_throw(repo.setProperty(TEXT("data"), data));
				return paramSetOid(*pReturn, repo.oid);
			});
	}

	int JOMAPI jadegit_repo_reset(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		RepositoryData repo(&pBuffer->oid);
		return Task::make(pReturn, [repo](IProgress* progress) { return repo.reset(progress); });
	}

	int JOMAPI jadegit_repo_unload(DskBuffer *pBuffer, DskParam *pParams, DskParam *pReturn)
	{
		RepositoryData repo(&pBuffer->oid);
		return Task::make(pReturn, [repo](IProgress* progress) { return repo.unload(progress); });
	}
}