#pragma once
#include "Change.h"

namespace JadeGit::Extract
{
	class Entity;
}

namespace JadeGit::Schema
{
	class AddOperation;
	class UpdateOperation;
	class RenameOperation;
	class DeleteOperation;

	class ChangeOperation
	{
	public:
		static const ChangeOperation& Get(GitChange::Operation operation);

		virtual GitChange add(const WorktreeData& worktree, const GitChange& current, const GitEntity& entity) const;
		virtual GitChange update(const WorktreeData& worktree, const GitChange& current, const GitEntity& entity) const;
		virtual GitChange rename(const WorktreeData& worktree, const GitChange& current, const GitEntity& previous, const GitEntity& latest, const Extract::Entity& actual) const;
		virtual GitChange dele(const WorktreeData& worktree, const GitChange& current, const GitEntity& entity, const Extract::Entity* actual) const;

		virtual void Combine(GitChange& lhs, GitChange& rhs, const AddOperation& operation) const = 0;
		virtual void Combine(GitChange& lhs, GitChange& rhs, const UpdateOperation& operation) const = 0;
		virtual void Combine(GitChange& lhs, GitChange& rhs, const RenameOperation& operation) const = 0;
		virtual void Combine(GitChange& lhs, GitChange& rhs, const DeleteOperation& operation) const = 0;

	protected:
		friend GitChange;
		virtual void stagePreload(const GitChange& change, Extract::Assembly& assembly, std::set<DskObjectId>& loaded) const;
		virtual void stage(GitChange& change, Extract::Assembly& assembly, GitChangeSet& extract) const;
		virtual void stageExtract(GitChange& change, Extract::Assembly& assembly) const = 0;

		virtual void Compact(GitChange& change) const;
		virtual void Compact(GitChange& lhs, GitChange& rhs, const ChangeOperation& operation) const = 0;

		void Merge(GitChange& lhs, GitChange& rhs) const;
		void Merge(GitChange& lhs, GitChange& rhs, GitChange::Operation operation) const;

	private:
		void stagePreload(const GitEntity& entity, Extract::Assembly& assembly) const;
	};
}