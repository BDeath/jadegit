#include "WorktreeState.h"
#include "ChangeSet.h"
#include "Index.h"
#include <jade/Iterator.h>
#include <jade/Transaction.h>
#include <schema/Branch.h>
#include <schema/Commit.h>
#include <schema/Progress.h>
#include <schema/Repository.h>
#include <schema/data/refdb/NodeDict.h>
#include <schema/deploy/DeploymentLock.h>
#include <schema/deploy/WorktreeDeploymentBuilder.h>
#include <schema/index/Index.h>
#include <vfs/GitFileSystem.h>
#include <Exception.h>
#include <Singleton.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	DEFINE_ENUM_FLAG_OPERATORS(git_merge_analysis_t);

	class InactiveState : public WorktreeState, public Singleton<InactiveState>
	{
	protected:
		bool load(const WorktreeData& worktree, IProgress* progress) const final
		{
			if (progress && !progress->start(2, "Loading"))
				return false;

			// Open repository
			Transient<Repository> repo = worktree.open();

			// Build deployment
			Transaction transaction;
			WorktreeDeploymentBuilder builder(worktree, repo);
			if (!builder.load(progress))
				return false;
			transaction.commit();

			// Install
			if (!builder.deployment->execute(progress))
				return false;

			return (!progress || progress->finish());
		}

		void deployed(const WorktreeData& worktree, const WorktreeDeployment& deployment) const final
		{
			switch (deployment.action())
			{
			case WorktreeDeployment::Action::Load:
			{
				// Set active state
				set(worktree, WorktreeData::State::Active);
				break;
			}
			default:
				throw logic_error("Unhandled deployment scenario");
			}
		}

		git_repository_state_t state(const WorktreeData& worktree) const final
		{
			return git_repository_state_t::GIT_REPOSITORY_STATE_NONE;
		}
	};

	class ActiveState : public WorktreeState, public Singleton<ActiveState>
	{
	protected:
		void amend(const WorktreeData& worktree, const Repository& repo, const std::string& message) const final
		{
			// Check changes have been made that need to be committed
			if (!worktree.isModified())
				throw runtime_error("No changes to amend");

			// Open index
			auto index = repo.index().read(true);

			// Start transaction
			Transaction transaction;

			// Stage all changes if required
			if (!worktree.isStaged())
			{
				if (!GitChangeSet(worktree).stage(GitFileSystem(index)))
					throw runtime_error("No current changes to commit");
				assert(worktree.isStaged());
			}

			// Lookup head commit
			unique_ptr<git_commit> commit;
			{
				unique_ptr<git_reference> head;
				git_throw(git_repository_head(git_ptr(head), repo));
				git_throw(git_commit_lookup(git_ptr(commit), repo, git_reference_target(head.get())));
			}
			
			// Write index to tree
			git_oid tree_id;
			git_throw(git_index_write_tree(&tree_id, index));

			// Lookup tree
			unique_ptr<git_tree> tree;
			git_throw(git_tree_lookup(git_ptr(tree), repo, &tree_id));

			// Amend commit
			git_oid id;
			git_throw(git_commit_amend(&id, commit.get(), "HEAD", nullptr, nullptr, nullptr, message.empty() ? nullptr : message.c_str(), tree.get()));

			// Save index changes
			index.write();

			// Purge changes committed (or void)
			GitChange change;
			Iterator<GitChange> iter(worktree.changes());
			while (iter.next(change))
			{
				if (change.isStaged() || change.isVoid())
					change.deleteObject();
			}

			// Success
			transaction.commit();
		}

		using WorktreeState::commit;
		void commit(const WorktreeData& worktree, const Repository& repo, const string& message) const final
		{
			// Check changes have been made that need to be committed
			if (!worktree.isModified())
				throw runtime_error("No changes to commit");

			// Open index
			auto index = repo.index().read(true);

			// Start transaction
			Transaction transaction;

			// Stage all changes if required
			if (!worktree.isStaged())
			{
				if (!GitChangeSet(worktree).stage(GitFileSystem(index)))
					throw runtime_error("No current changes to commit");
				assert(worktree.isStaged());
			}

			// Lookup parent commit, except for initial commit (unborn repository)
			unique_ptr<git_commit> parent;
			{
				unique_ptr<git_reference> head;
				auto error = git_repository_head(git_ptr(head), repo);
				if (error != GIT_EUNBORNBRANCH)
				{
					git_throw(error);
					git_throw(git_commit_lookup(git_ptr(parent), repo, git_reference_target(head.get())));
				}
			}

			// Create commit with zero or one parent commit
			// TODO: Handle including MERGE_HEAD to cater for scenario where creating merge commit is deferred
			const git_commit* parents[] = { parent.get() };
			git_oid id;
			commit(id, repo, "HEAD", message, index, parent ? 1 : 0, parents);

			// Save index changes
			index.write();

			// Purge changes committed (or void)
			GitChange change;
			Iterator<GitChange> iter(worktree.changes());
			while (iter.next(change))
			{
				if (change.isStaged() || change.isVoid())
					change.deleteObject();
			}

			// Success
			transaction.commit();
		}

		bool unload(const WorktreeData& worktree, IProgress* progress) const final
		{
			if (progress && !progress->start(2, "Unloading"))
				return false;

			// Open repository
			Transient<Repository> repo = worktree.open();

			// Build deployment
			Transaction transaction;
			WorktreeDeploymentBuilder builder(worktree, repo);
			if (!builder.unload(progress))
				return false;
			transaction.commit();

			// Install
			if (!builder.deployment->execute(progress))
				return false;

			return (!progress || progress->finish());
		}

		// Initiate merge operation
		bool merge(const WorktreeData& worktree, const Repository& repo, const git_annotated_commit* theirs, IProgress* progress) const final
		{
			// TODO: Locking strategy

			// Cannot merge without committing previous changes
			if (worktree.isModified())
				throw runtime_error("Changes must be committed before merging");

			// Lookup head
			unique_ptr<git_reference> head;
			git_throw(git_repository_head(git_ptr(head), repo));

			// Analysis
			git_merge_analysis_t analysis = git_merge_analysis_t::GIT_MERGE_ANALYSIS_NONE;
			git_merge_preference_t preference = git_merge_preference_t::GIT_MERGE_PREFERENCE_NONE;
			git_throw(git_merge_analysis_for_ref(&analysis, &preference, repo, head.get(), &theirs, 1));

			// Merge not required when branch is already up-to-date
			if (analysis == git_merge_analysis_t::GIT_MERGE_ANALYSIS_UP_TO_DATE)
			{
				// TODO: Return user friendly message without treating as an error
				throw runtime_error("'" + string(git_reference_shorthand(head.get())) + "' is already up-to-date");
				return true;
			}

			// Resolve preferred merge strategy
			switch (preference)
			{
			case git_merge_preference_t::GIT_MERGE_PREFERENCE_NONE:
			{
				// Break if fast-forward only below isn't possible (default preference)
				if (!(analysis & git_merge_analysis_t::GIT_MERGE_ANALYSIS_FASTFORWARD))
					break;
			}
			case git_merge_preference_t::GIT_MERGE_PREFERENCE_FASTFORWARD_ONLY:
			{
				analysis &= git_merge_analysis_t::GIT_MERGE_ANALYSIS_FASTFORWARD | git_merge_analysis_t::GIT_MERGE_ANALYSIS_UNBORN;
				break;
			}
			case git_merge_preference_t::GIT_MERGE_PREFERENCE_NO_FASTFORWARD:
			{
				analysis &= git_merge_analysis_t::GIT_MERGE_ANALYSIS_NORMAL;
				break;
			}
			default:
				throw logic_error("Unhandled merge preference");
			}
			if (!analysis)
				throw runtime_error("Preferred merge strategy isn't possible");

			// Start transaction to setup merge state
			Transaction transaction;

			// Lookup their commit being merged
			unique_ptr<git_commit> commit;
			git_throw(git_commit_lookup(git_ptr(commit), repo, git_annotated_commit_id(theirs)));

			// Handle merge scenario
			switch (analysis)
			{
			case git_merge_analysis_t::GIT_MERGE_ANALYSIS_FASTFORWARD:
			case git_merge_analysis_t::GIT_MERGE_ANALYSIS_FASTFORWARD | git_merge_analysis_t::GIT_MERGE_ANALYSIS_UNBORN:
			{
				if (progress && !progress->start(2, "Merging (fast-forward)"))
					return false;

				// Build deployment
				// Fast-forward merges are the equivalent of a reset/switch, so don't need to setup merge state/index like a normal merge below
				WorktreeDeploymentBuilder builder(worktree, repo);
				if (!builder.merge(progress, move(commit)))
					return false;

				// Commit transaction to setup deployment
				transaction.commit();

				// Install
				if (!builder.deployment->execute(progress))
					return false;

				return (!progress || progress->finish());
			}
			case git_merge_analysis_t::GIT_MERGE_ANALYSIS_NORMAL:
			{
				// Set merge state
				set(worktree, WorktreeData::State::Merge);

				// Store MERGE_HEAD
				unique_ptr<git_reference> merge_head;
				git_throw(git_reference_create(git_ptr(merge_head), repo, "MERGE_HEAD", git_annotated_commit_id(theirs), false, nullptr));

				// Lookup our commit being merged
				unique_ptr<git_commit> ours;
				git_throw(git_commit_lookup(git_ptr(ours), repo, git_reference_target(head.get())));

				// Merge commits
				git_merge_options opts = GIT_MERGE_OPTIONS_INIT;
				unique_ptr<git_index> merge_index;
				git_throw(git_merge_commits(git_ptr(merge_index), repo, ours.get(), commit.get(), &opts));

				// Save merge index
				worktree.index().write(merge_index.get());

				// Derive/save merge commit message
				// TODO: Need a shorthand version of git_annotated_commit_ref
				auto message = "Merge '" + string(git_annotated_commit_ref(theirs)) + "' into '" + git_reference_shorthand(head.get()) + "'";
				worktree.setMessage(message);

				// Commit transaction to save merge state
				transaction.commit();

				// Cannot continue when there's merge conflicts to resolve
				if (git_index_has_conflicts(merge_index.get()))
					return false;

				// Continue merge operation
				return worktree.continue_(repo, message, progress);
			}
			default:
				throw logic_error("Unhandled merge scenario");
			}

			return true;
		}

		// Initiate reset operation
		bool reset(const WorktreeData& worktree, const Repository& repo, const git_commit* commit, bool hard, IProgress* progress) const final
		{
			// Lookup head
			unique_ptr<git_reference> head;
			git_throw(git_repository_head(git_ptr(head), repo));

			// Hard reset requires deployment to unload/discard changes
			if (hard)
			{
				// Check there are changes to be reset
				if (git_oid_equal(git_reference_target(head.get()), git_commit_id(commit)) && !worktree.isModified())
					throw runtime_error("Worktree has no pending changes to be reset");

				if (progress && !progress->start(2, "Resetting"))
					return false;

				// Open index
				auto index = repo.index();

				// Build deployment
				Transaction transaction;
				WorktreeDeploymentBuilder builder(worktree, repo);
				if (!builder.reset(progress, commit))
					return false;
				transaction.commit();

				// Install
				if (!builder.deployment->execute(progress))
					return false;

				return (!progress || progress->finish());
			}
			else
			{
				// Check attempt isn't being made to reset to a different commit (further work required to re-evaluate changes/differences that would be unstaged as a result)
				if (!git_oid_equal(git_reference_target(head.get()), git_commit_id(commit)))
					throw unimplemented_feature("Keeping changes while resetting to a different commit");

				Transaction transaction;

				// Reset changes (unstage all)
				GitChange change;
				Iterator<GitChange> iter(worktree.changes());
				while (iter.next(change))
					change.Reset();

				// Open index
				auto index = repo.index();

				// Reset & save index
				index.reset(commit);
				index.write();
				
				transaction.commit();

				return true;
			}
		}

		// Handle switching to branch
		bool switch_(const WorktreeData& worktree, const Repository& repo, const git_reference* branch, IProgress* progress) const final
		{
			// Lookup current head
			unique_ptr<git_reference> head;
			git_throw(git_repository_head(git_ptr(head), repo));

			// No deployment necessary if references refer to the same commit
			if (git_reference_cmp(head.get(), branch) == 0)
			{
				// Update head
				Transaction transaction;
				git_throw(git_repository_set_head(repo, git_reference_name(branch)));
				transaction.commit();
				return true;
			}

			if (progress && !progress->start(2, format("Switching to {}", git_reference_shorthand(branch))))
				return false;

			// Build deployment
			Transaction transaction;
			WorktreeDeploymentBuilder builder(worktree, repo);
			if (!builder.switch_(progress, branch))
				return false;
			transaction.commit();

			// Install
			if (!builder.deployment->execute(progress))
				return false;

			return (!progress || progress->finish());
		}

		// Handle switching to commit (detached)
		bool switch_(const WorktreeData& worktree, const Repository& repo, const git_commit* commit, IProgress* progress) const final
		{
			// Lookup current head
			unique_ptr<git_reference> head;
			git_throw(git_repository_head(git_ptr(head), repo));

			// No deployment necessary if head already resolves to the same commit
			if (git_oid_equal(git_reference_target(head.get()), git_commit_id(commit)))
			{
				// Detach head
				Transaction transaction;
				git_throw(git_repository_detach_head(repo));
				transaction.commit();
				return true;
			}
			
			if (progress && !progress->start(2, format("Switching to {}", string_view(git_oid_tostr_s(git_commit_id(commit)), 8))))
				return false;

			// Build deployment
			Transaction transaction;
			WorktreeDeploymentBuilder builder(worktree, repo);
			if (!builder.switch_(progress, commit))
				return false;
			transaction.commit();

			// Install
			if (!builder.deployment->execute(progress))
				return false;

			return (!progress || progress->finish());
		}

		void deployed(const WorktreeData& worktree, const WorktreeDeployment& deployment) const final
		{
			auto action = deployment.action();

			if (action == WorktreeDeployment::Action::Unload)
			{
				// Set inactive state
				set(worktree, WorktreeData::State::Inactive);

				// TODO: Preserve staged state, with index & associated changes.
				// Reloading branch will need to support using the saved index, rather than last commit

				// Purge changes
				jade_throw(worktree.getProperty<DskObjectSet>(TEXT("changes")).purge());

				// Purge index
				jade_throw(worktree.getProperty<Jade::JadeBytes>(TEXT("index")).purge());

				return;
			}

			// Open repository
			Transient<Repository> repo = worktree.open();

			// Lookup commit
			auto commit = deployment.commit(repo);

			// Reset & save index
			auto index = repo.index();
			index.reset(commit.get());
			index.write();

			// Purge changes
			jade_throw(worktree.changes().purge());

			// Handle updates for specific action
			switch (action)
			{
			case WorktreeDeployment::Action::Merge:
			case WorktreeDeployment::Action::Reset:
			{
				// Update head
				unique_ptr<git_reference> head;
				git_throw(git_repository_head(git_ptr(head), repo));
				git_throw(git_reference_set_target(git_ptr(head), head.get(), git_commit_id(commit.get()), nullptr));
				break;
			}
			case WorktreeDeployment::Action::Switch:
			{
				// Lookup reference
				if (auto ref = deployment.reference(repo))
				{
					// Resolve direct reference
					unique_ptr<git_reference> resolved;
					git_throw(git_reference_resolve(git_ptr(resolved), ref.get()));

					// Check commits still match
					if (git_oid_equal(git_commit_id(commit.get()), git_reference_target(resolved.get())))
					{
						// Set head to reference
						git_throw(git_repository_set_head(repo, git_reference_name(ref.get())));
						break;
					}
				}

				// Set head to commit (detached)
				git_throw(git_repository_set_head_detached(repo, git_commit_id(commit.get())));
				break;
			}
			default:
				throw logic_error("Unhandled deployment action");
			}
		}

		git_repository_state_t state(const WorktreeData& worktree) const final
		{
			return git_repository_state_t::GIT_REPOSITORY_STATE_NONE;
		}
	};

	class MergeState : public WorktreeState, public Singleton<MergeState>
	{
	public:
		// Abort merge operation
		void abort(const WorktreeData& worktree, const Repository& repo) const override
		{
			// Cannot abort while deploying
			DeploymentLock lock;

			// Lookup head
			unique_ptr<git_reference> head;
			git_throw(git_repository_head(git_ptr(head), repo));

			// Lookup commit
			unique_ptr<git_commit> commit;
			git_throw(git_commit_lookup(git_ptr(commit), repo, git_reference_target(head.get())));
			
			Transaction transaction;

			// Reset index
			Transient<Index> index = repo.index();
			index.reset(commit.get());
			index.write();

			// Reset merge head
			git_throw(git_reference_remove(repo, "MERGE_HEAD"));

			// Reset message
			worktree.setMessage(string());

			// Reset state
			set(worktree, WorktreeData::State::Active);

			transaction.commit();
		}

		// Continue merge operation
		bool continue_(const WorktreeData& worktree, const Repository& repo, const string& message, IProgress* progress) const override
		{
			// Open index
			Transient<Index> index = repo.index().read(true);

			// Throw error if there's still conflicts
			if (git_index_has_conflicts(index))
				throw runtime_error("Merge conflicts have not been resolved");

			// Lookup commits being merged
			unique_ptr<git_reference> head;
			git_throw(git_repository_head(git_ptr(head), repo));

			unique_ptr<git_commit> head_commit;
			git_throw(git_commit_lookup(git_ptr(head_commit), repo, git_reference_target(head.get())));

			unique_ptr<git_reference> merge_head;
			git_throw(git_reference_lookup(git_ptr(merge_head), repo, "MERGE_HEAD"));

			unique_ptr<git_commit> merge_commit;
			git_throw(git_commit_lookup(git_ptr(merge_commit), repo, git_reference_target(merge_head.get())));

			// Start transaction
			Transaction transaction;

			// Create merge commit
			// TODO: Add abilty to defer creating commit post-deployment (may want to make changes beforehand)
			const git_commit* parents[] = { head_commit.get(), merge_commit.get() };
			git_oid id;
			commit(id, repo, nullptr, message, index, 2, parents);
			
			// Lookup commit created
			unique_ptr<git_commit> commit;
			git_throw(git_commit_lookup(git_ptr(commit), repo, &id));

			if (progress && !progress->start(2, "Merging"))
				return false;

			// Build deployment
			WorktreeDeploymentBuilder builder(worktree, repo);
			if (!builder.merge(progress, move(commit)))
				return false;

			// Save index changes
			index.write();

			// Commit transaction
			transaction.commit();

			// Install
			if (!builder.deployment->execute(progress))
				return false;

			return (!progress || progress->finish());
		}

		void deployed(const WorktreeData& worktree, const WorktreeDeployment& deployment) const final
		{		
			// Open repository
			Transient<Repository> repo = worktree.open();

			// Lookup commit
			auto commit = deployment.commit(repo);

			// Handle updates for specific action
			switch (deployment.action())
			{
			case WorktreeDeployment::Action::Merge:
			{
				if (commit)
				{
					// Update head
					unique_ptr<git_reference> head;
					git_throw(git_repository_head(git_ptr(head), repo));
					git_throw(git_reference_set_target(git_ptr(head), head.get(), git_commit_id(commit.get()), nullptr));

					// Reset merge head
					git_throw(git_reference_remove(repo, "MERGE_HEAD"));

					// Reset message
					worktree.setMessage(string());
				}

				// Set active state
				set(worktree, WorktreeData::State::Active);
				break;
			}
			default:
				throw logic_error("Unhandled deployment action");
			}
		}

		git_repository_state_t state(const WorktreeData& worktree) const final
		{
			return git_repository_state_t::GIT_REPOSITORY_STATE_MERGE;
		}
	};

	const WorktreeState& WorktreeState::get(WorktreeData::State state)
	{
		switch (state)
		{
		case WorktreeData::State::Inactive:
			return *InactiveState::Instance();
		case WorktreeData::State::Active:
			return *ActiveState::Instance();
		case WorktreeData::State::Merge:
			return *MergeState::Instance();
		}

		throw invalid_argument("Invalid state");
	}

	bool WorktreeState::isActive() const
	{
		return this != InactiveState::Instance();
	}

	void WorktreeState::abort(const WorktreeData& worktree, const Repository& repo) const
	{
		throw runtime_error("Cannot abort (no operation in progress)");
	}

	void WorktreeState::amend(const WorktreeData& worktree, const Repository& repo, const std::string& message) const
	{
		throw runtime_error("Invalid operation");
	}

	bool WorktreeState::continue_(const WorktreeData& worktree, const Repository& repo, const string& message, IProgress* progress) const
	{
		throw runtime_error("Cannot continue (no operation in progress)");
	}

	void WorktreeState::commit(const WorktreeData& worktree, const Repository& repo, const string& message) const
	{
		throw runtime_error("Invalid operation");
	}

	void WorktreeState::commit(git_oid& id, git_repository* repo, const char* update_ref, const string& message, git_index* index, size_t parent_count, const git_commit* parents[]) const
	{
		// Write index to tree
		git_oid tree_id;
		git_throw(git_index_write_tree(&tree_id, index));

		// Lookup tree
		unique_ptr<git_tree> tree;
		git_throw(git_tree_lookup(git_ptr(tree), repo, &tree_id));

		// Get signature
		unique_ptr<git_signature> author;
		git_throw(git_signature_default(git_ptr(author), repo));

		// Create commit
		git_throw(git_commit_create(&id, repo, update_ref, author.get(), author.get(), nullptr, message.c_str(), tree.get(), parent_count, parents));
	}

	bool WorktreeState::load(const WorktreeData& worktree, IProgress* progress) const
	{
		throw runtime_error("Invalid operation");
	}

	bool WorktreeState::merge(const WorktreeData& worktree, const Repository& repo, const git_annotated_commit* theirs, IProgress* progress) const
	{
		throw runtime_error("Invalid operation");
	}

	bool WorktreeState::reset(const WorktreeData& worktree, const Repository& repo, const git_commit* commit, bool hard, IProgress* progress) const
	{
		throw runtime_error("Invalid operation");
	}

	void WorktreeState::set(const WorktreeData& worktree, WorktreeData::State state) const
	{
		worktree.setState(state);
	}

	bool WorktreeState::switch_(const WorktreeData& worktree, const Repository& repo, const git_reference* branch, IProgress* progress) const
	{
		throw runtime_error("Invalid operation");
	}

	bool WorktreeState::switch_(const WorktreeData& worktree, const Repository& repo, const git_commit* commit, IProgress* progress) const
	{
		throw runtime_error("Invalid operation");
	}

	bool WorktreeState::unload(const WorktreeData& worktree, IProgress* progress) const
	{
		throw runtime_error("Invalid operation");
	}
}