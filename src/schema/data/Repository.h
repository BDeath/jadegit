#pragma once
#include <schema/Object.h>
#include <jadegit/git2.h>
#include "RepositoryData.h"

namespace JadeGit
{
	class IProgress;
}

namespace JadeGit::Registry
{
	class RepositoryT;
}

namespace JadeGit::Schema
{
	class GitReference;
	class GitSchema;

	class ReferenceNodeDict;
	class Root;
	class WorktreeData;

	class RepositoryData : public Object, public IGitRepositoryData
	{
	public:
		enum State
		{
			Active = (1 << 0),
			Unstable = (1 << 1)
		};

		using Object::Object;
		RepositoryData();
		RepositoryData(const IGitRepositoryData &child);
		RepositoryData(const Root& root, const std::string& name);

		std::string GetName() const;
		bool GetSchema(const std::string& name, GitSchema& schema) const;

		std::unique_ptr<git_repository> open() const;
		std::unique_ptr<git_repository> open(const WorktreeData& worktree) const;

		bool clone(const std::string& remote, std::string name, const std::string& access_token, const git_oid* current_commit_id, IProgress* progress);
		bool load(IProgress* progress) const;
		ReferenceNodeDict refs() const;
		bool reset(IProgress* progress) const;
		bool unload(IProgress* progress) const;

		bool isActive() const { return (GetState() & State::Active) > 0; }
		bool isUnstable() const { return (GetState() & State::Unstable) > 0; }

		void SetState(State value);
		State GetState() const;

	protected:
		void GetRepository(RepositoryData& repo) const override;

	private:
		friend int JOMAPI jadegit_root_create(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn);
		static void restore(const Root& root, Registry::RepositoryT& registry);
	};
	DEFINE_ENUM_FLAG_OPERATORS(RepositoryData::State);
}