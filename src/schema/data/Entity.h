#pragma once
#include <jadegit/data/Entity.h>
#include <extract/Entity.h>
#include <schema/Object.h>
#include <vector>
#include <map>

namespace JadeGit::Schema
{
	class GitChange;
	class GitUser;
	class RepositoryData;
	class WorktreeData;

	class GitEntity : public Object
	{
	public:
		using Object::Object;
		GitEntity();
		GitEntity(const GitEntity& parent, ClassNumber kind, const std::string& name);
		GitEntity(const GitEntity& parent, ClassNumber kind, const std::string& name, const DskObjectId* prior);

		ClassNumber GetKind() const;
		std::string GetKindName() const;
		std::string GetName() const;

		std::unique_ptr<QualifiedName> GetQualifiedName() const;
		std::unique_ptr<Extract::Entity> GetSource(bool required = true) const;
		Data::Entity* GetTarget(Data::Assembly &assembly) const;

		// Register associates (which are implicitly deleted with receiver)
		void updateAssociates(const Extract::Entity& source);

		// Retrieve most recent change
		GitChange getChange() const;
		void getChanges(std::vector<GitChange>& changes) const;

		// Lookup child entity
		std::unique_ptr<GitEntity> GetChild(ClassNumber kind, const std::string& name) const;

		// Resolve repository
		virtual RepositoryData repo() const;

		// Retrieve parent
		std::unique_ptr<GitEntity> parent() const;

		// Lookup prior/next versions
		template <class TEntity = GitEntity>
		std::unique_ptr<TEntity> prior() const
		{
			return getProperty<std::unique_ptr<TEntity>>(TEXT("prior"));
		}

		template <class TEntity = GitEntity>
		std::unique_ptr<TEntity> next() const
		{
			return getProperty<std::unique_ptr<TEntity>>(TEXT("next"));
		}

		inline bool isParent(const GitEntity& parent) const
		{
			DskObjectId oid;
			getProperty(TEXT("parent"), oid);
			return parent.oid == oid;
		}

		inline bool isVersioned() const
		{
			return !isNull() && !!prior();
		}

		// Resolve entity (creating if required)
		static std::unique_ptr<GitEntity> resolve(const Extract::Entity& source, bool dryrun = false);
		static std::unique_ptr<GitEntity> resolve(const Extract::Entity& source, const std::string& name, bool dryrun = false);

		void getChildChanges(const WorktreeData& worktree, std::vector<GitChange> &changes) const;
		void cloneChildren(const GitEntity& clone, std::map<DskObjectId, DskObjectId> &directory) const;
		void cloneChanges(const WorktreeData& worktree, std::vector<GitChange> &source, std::map<DskObjectId, DskObjectId> &entities, std::map<DskObjectId, DskObjectId> &changes) const;
		
	protected:
		GitEntity(ClassNumber classNo) : Object(classNo) {};

		std::unique_ptr<GitEntity> makeNext() const;

		void transition(const GitEntity& parent);
		void transition(const GitEntity& parent, const GitEntity& next);

	private:
		static std::unique_ptr<GitEntity> resolver(const GitEntity& parent, ClassNumber kind, const std::string& name, bool dryrun = false);
	};
}