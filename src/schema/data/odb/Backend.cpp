#include "Backend.h"
#include "Data.h"
#include "Writepack.h"
#include <git2/sys/errors.h>
#include <schema/Workers.h>

using namespace std;

namespace JadeGit::Schema::Backend
{
	template <typename T>
	static int wrapper(git_odb_backend* backend, T func)
	{
		try
		{
			// Ensure process is signed on (git worker may have been started)
			SignOn();

			return func(*static_cast<jadegit_odb_backend*>(backend));
		}
		catch (const exception& e)
		{
			git_error_set_str(GIT_ERROR_ODB, e.what());
		}

		return GIT_ERROR;
	}

	int jadegit_odb_backend__read(void** data_p, size_t* len_p, git_object_t* type_p, git_odb_backend* backend, const git_oid* oid)
	{
		return wrapper(backend, [&](jadegit_odb_backend& backend)
			{
				auto object = ObjectData::lookup(backend.repo, oid);
				if (object.isNull())
					return GIT_ENOTFOUND;

				return object.read(data_p, len_p, type_p, &backend);
			});
	}

	int jadegit_odb_backend__exists(git_odb_backend* backend, const git_oid* oid)
	{
		return wrapper(backend, [&](jadegit_odb_backend& backend)
			{
				return ObjectData::lookup(backend.repo, oid).isNull() ? 0 : 1;
			});
	}

	int jadegit_odb_backend__write(git_odb_backend* backend, const git_oid* oid, const void* data, size_t len, git_object_t type)
	{
		return wrapper(backend, [&](jadegit_odb_backend& backend)
			{
				ObjectData::write(backend.repo, oid, data, len, type);
				return GIT_OK;
			});
	}

	void jadegit_odb_backend__free(git_odb_backend* backend)
	{
		delete static_cast<jadegit_odb_backend*>(backend);
	}

	int jadegit_odb_backend__writepack(git_odb_writepack** out, git_odb_backend* backend, git_odb* odb, git_indexer_progress_cb progress_cb, void* progress_payload)
	{
		return jadegit_odb_writepack::make(out, backend, odb, progress_cb, progress_payload);
	}

	jadegit_odb_backend::jadegit_odb_backend(const RepositoryData& repo) : repo(repo)
	{
		git_throw(git_odb_init_backend(this, GIT_ODB_BACKEND_VERSION));

		git_odb_backend::read = &jadegit_odb_backend__read;
		git_odb_backend::read_header = nullptr;		// &jadegit_odb_backend__read_header;
		git_odb_backend::write = &jadegit_odb_backend__write;
		git_odb_backend::writepack = &jadegit_odb_backend__writepack;
		git_odb_backend::exists = &jadegit_odb_backend__exists;
		git_odb_backend::free = &jadegit_odb_backend__free;
	}
}