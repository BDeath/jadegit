#include "Change.h"
#include "ChangeOperation.h"
#include "ChangeSet.h"
#include "Entity.h"
#include "Repository.h"
#include "Worktree.h"
#include <extract/Entity.h>
#include <jade/AppContext.h>
#include <jade/Iterator.h>
#include <jade/Transaction.h>
#include <schema/Session.h>
#include <schema/Workers.h>
#include <schema/ObjectRegistration.h>

using namespace std;
using namespace Jade;
using namespace JadeGit::Extract;

namespace JadeGit::Schema
{
	const Character PRP_Change_operation[] = TEXT("operation");
	const Character PRP_Change_predecessors[] = TEXT("predecessors");
	const Character PRP_Change_staged[] = TEXT("staged");
	const Character PRP_Change_successors[] = TEXT("successors");

	GitChange GitChange::add(const GitEntity& entity)
	{
		return change(entity, bind(&ChangeOperation::add, placeholders::_1, placeholders::_2, placeholders::_3, ref(entity)));
	}

	GitChange GitChange::update(const GitEntity& entity)
	{
		return change(entity, bind(&ChangeOperation::update, placeholders::_1, placeholders::_2, placeholders::_3, ref(entity)));
	}

	GitChange GitChange::rename(const GitEntity& previous, const GitEntity& latest, const Extract::Entity& actual)
	{
		// Treat as update if entity hasn't actually been renamed
		if (previous.oid == latest.oid)
			return update(latest);

		return change(previous, bind(&ChangeOperation::rename, placeholders::_1, placeholders::_2, placeholders::_3, ref(previous), ref(latest), ref(actual)));
	}

	GitChange GitChange::dele(const GitEntity& entity, const Extract::Entity& actual)
	{
		return dele(entity, &actual);
	}

	GitChange GitChange::dele(const GitEntity& entity, const Extract::Entity* actual)
	{
		return change(entity, bind(&ChangeOperation::dele, placeholders::_1, placeholders::_2, placeholders::_3, ref(entity), ref(actual)));
	}

	GitChange GitChange::change(const GitEntity& entity, function<GitChange(const ChangeOperation&, const WorktreeData&, const GitChange&)> action)
	{
		// Retrieve active worktree
		WorktreeData worktree = Session::get().getActiveWorktree(entity.repo());	

		// Lock worktree and cleanup any changes previously voided
		GitChange::prelude(worktree);

		// Retrieve prior change
		auto change = entity.getChange();

		// TODO: Check worktree matches existing worktree

		// Apply latest change
		return action(change.getOperation(), worktree, move(change));
	}

	void GitChange::prelude(const WorktreeData& worktree)
	{
		// Throw error if repository is inactive
		if (!RepositoryData(worktree).isActive())
			throw runtime_error("Repository must be loaded before changes can be made");

		// Throw error if worktree is inactive
		if (!worktree.isActive())
			throw runtime_error("Worktree must be loaded before changes can be made");

		// Ignore if process already has an exclusive lock as a result of previous call
		if (worktree.getLockStatus() == EXCLUSIVE_LOCK)
			return;

		// Exclusive lock worktree to single-thread updates
		jade_throw(worktree.lockObject(EXCLUSIVE_LOCK));

		// Clean-up redundant changes that are void (potentially half-created due to previous error)
		if (AppContext::IsSystem())
			Backend::workers.push(std::bind(&GitChange::preludeBackground, worktree)).get();
		else
			preludeBackground(worktree);
	}

	void GitChange::preludeBackground(const WorktreeData& worktree)
	{
		Transaction transaction;

		GitChange change;
		Iterator<GitChange> iter(worktree, TEXT("changes"));
		while (iter.next(change))
		{
			if (change.isVoid())
				change.deleteObject();
		}

		transaction.commit();
	}

	static GitObjectRegistration<GitChange> registration(TEXT("Change"));

	GitChange::GitChange() : Object(registration) {}

	GitChange::GitChange(const WorktreeData& worktree, const GitEntity* previous, const GitEntity* latest, std::set<DskObjectId> &predecessors) : GitChange()
	{
		create(worktree, previous, latest, predecessors);
	}

	GitChange::GitChange(const WorktreeData& worktree, const GitEntity* previous, const GitEntity* latest, const GitChange* predecessor) : GitChange()
	{
		std::set<DskObjectId> predecessors;
		if (predecessor && !predecessor->isNull())
			predecessors.insert(predecessor->oid);

		create(worktree, previous, latest, predecessors);
	}

	void GitChange::addPredecessor(const GitChange& predecessor) const
	{
		if (predecessor.isNull())
			return;

		if (isTransient())
		{
			ObjectSet<GitChange>(*this, PRP_Change_predecessors).tryAdd(predecessor);
			return;
		}

		if (AppContext::IsSystem())
			return Backend::workers.push(bind(&GitChange::addPredecessor, this, ref(predecessor))).get();

		Transaction transaction;
		ObjectSet<GitChange>(*this, PRP_Change_predecessors).tryAdd(predecessor);
		transaction.commit();
	}

	void GitChange::addPredecessors(std::set<DskObjectId>& predecessors)
	{
		if (!predecessors.empty())
		{
			DskCollection coll;
			getProperty(PRP_Change_predecessors, coll);

			for (auto& oid : predecessors)
				jade_throw(coll.sendMsg(MTH_Collection_add, oid));
		}
	}

	void GitChange::compact()
	{
		getOperation().Compact(*this);
	}

	void GitChange::create(const WorktreeData& worktree, const GitEntity* previous, const GitEntity* latest, std::set<DskObjectId> &predecessors)
	{
		// Redirect to backend thread if required
		if (AppContext::IsSystem())
			return Backend::workers.push(std::bind(&GitChange::create, this, std::ref(worktree), previous, latest, std::ref(predecessors))).get();

		Transaction transaction;

		jade_throw(createObject());
		jade_throw(setProperty(TEXT("worktree"), worktree));

		if (previous)
			jade_throw(setProperty(TEXT("previous"), previous));

		if (latest)
			jade_throw(setProperty(TEXT("latest"), latest));

		// Instantiate collections upfront
		Collection<>(*this, PRP_Change_predecessors).instantiate();
		Collection<>(*this, PRP_Change_successors).instantiate();

		addPredecessors(predecessors);

		transaction.commit();
	}

	bool GitChange::isWorktree(const WorktreeData& worktree) const
	{
		return worktree.oid == getProperty<DskObjectId>(TEXT("worktree"));
	}

	bool GitChange::isPrevious(const GitEntity &entity) const
	{
		DskObjectId oid;
		getProperty(TEXT("previous"), oid);
		return !oid.isNull() && entity.oid == oid;
	}

	bool GitChange::isLatest(const GitEntity &entity) const
	{
		DskObjectId oid;
		getProperty(TEXT("latest"), oid);
		return !oid.isNull() && entity.oid == oid;
	}

	bool GitChange::isOperation(Operation operation) const
	{
		return getProperty<Byte>(PRP_Change_operation) == (Byte)operation;
	}

	bool GitChange::isStaged() const
	{
		return getProperty<bool>(PRP_Change_staged);
	}

	bool GitChange::isVersioned() const
	{
		return getLatest().isVersioned() || getPrevious().isVersioned();
	}

	void GitChange::getPrevious(GitEntity &entity) const
	{
		getProperty(TEXT("previous"), entity);
	}

	GitEntity GitChange::getPrevious() const
	{
		GitEntity entity;
		getPrevious(entity);
		return entity;
	}

	void GitChange::getLatest(GitEntity &entity) const
	{
		getProperty(TEXT("latest"), entity);
	}

	GitEntity GitChange::getLatest() const
	{
		GitEntity entity;
		getLatest(entity);
		return entity;
	}

	const ChangeOperation& GitChange::getOperation() const
	{
		Operation operation = Operation::Void;
		if(!isNull())
			getProperty(PRP_Change_operation, (Byte*)&operation);

		return ChangeOperation::Get(operation);
	}

	const GitChange& GitChange::peerOf(const GitChange& peer) const
	{
		if (peer.isNull())
			return *this;

		return predecessorOf(peer).successorOf(peer);
	}

	const GitChange& GitChange::predecessorOf(const GitChange& successor) const
	{
		successor.addPredecessor(*this);
		return *this;
	}

	const GitChange& GitChange::successorOf(const GitChange& predecessor) const
	{
		addPredecessor(predecessor);
		return *this;
	}

	const GitChange& GitChange::setOperation(Operation operation) const
	{
		jade_throw(setProperty(PRP_Change_operation, (Byte)operation));
		return *this;
	}

	const GitChange& GitChange::setOperation(const GitChange& source) const
	{
		return setOperation((Operation)source.getProperty<Byte>(PRP_Change_operation));
	}

	void GitChange::setPrevious(const GitEntity& entity)
	{
		jade_throw(setProperty(TEXT("previous"), entity));
	}

	void GitChange::setLatest(const GitEntity& entity)
	{
		jade_throw(setProperty(TEXT("latest"), entity));
	}

	// Preload entities affected
	// Used to avoid resolution errors while staging, where loading a previous entity may be affected by other changes already staged (delete/rename)
	void GitChange::stagePreload(Assembly& assembly, std::set<DskObjectId>& loaded)
	{
		// Ignore changes staged previously
		if (isStaged())
			return;

		// Ignore latest version changes
		if (isVersioned())
			return;

		// Ignore changes already preloaded
		if (loaded.find(oid) != loaded.end())
			return;
		loaded.insert(oid);

		// Preload entities for operation
		getOperation().stagePreload(*this, assembly, loaded);
	}

	void GitChange::stage(Assembly& assembly, GitChangeSet& extract)
	{
		// Ignore changes already staged
		if (isStaged())
			return;

		// Ignore latest version changes
		if (isVersioned())
			return;

		// Stage operation
		getOperation().stage(*this, assembly, extract);
	}

	void GitChange::stageExtract(Assembly& assembly)
	{
		getOperation().stageExtract(*this, assembly);
	}

	void GitChange::Reset()
	{
		// Reset staged changes recursively
		if (isStaged())
		{
			// Reset staged flag
			SetStaged(false);

			// Reset successors
			GitChange successor;
			Iterator<GitChange> successors(*this, PRP_Change_successors);
			while (successors.next(successor))
				successor.Reset();
		}

		// Compact changes
		getOperation().Compact(*this);
	}

	void GitChange::SetStaged(bool staged)
	{
		jade_throw(setProperty(TEXT("staged"), staged));
	}

	void GitChange::Merge(GitChange& rhs)
	{
		// Copy relationships
		GitChangeSet predecessors(*this, PRP_Change_predecessors);
		predecessors.tryCopy(GitChangeSet(rhs, PRP_Change_predecessors));

		GitChangeSet successors(*this, PRP_Change_successors);
		successors.tryCopy(GitChangeSet(rhs, PRP_Change_successors));

		// Remove relationship with self
		predecessors.tryRemove(*this);

		// Remove redundant change
		rhs.deleteObject();
	}
}