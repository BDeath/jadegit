#pragma once
#include <jade/JadeBytes.h>
#include <jadegit/git2.h>

namespace JadeGit::Schema
{
	class IndexData : public Jade::JadeBytes
	{
	public:
		Edition edition();
		SequenceNumber getUpdateTranID() const;
		std::unique_ptr<git_index> open() const;
		void read(git_index* index) const;
		void write(git_index* index) const;
	};
}