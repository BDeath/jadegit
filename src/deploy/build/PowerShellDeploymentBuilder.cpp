#include "PowerShellDeploymentBuilder.h"
#include <jadegit/vfs/File.h>
#include <format>
#include <registry/Commit.h>
#include <registry/storage/File.h>

using namespace std;

namespace JadeGit::Deploy
{
	PowerShellDeploymentBuilder::PowerShellDeploymentBuilder(const FileSystem& fs) : DirectoryBuilder(fs)
	{
	}

	void PowerShellDeploymentBuilder::start()
	{
		// Setup script file
		deploy = fs.open("deploy.ps1").createOutputStream();

		// Define deploy script parameters, functions & initial processing
		*deploy << R"src(param([string]$bin, [string]$path, [string]$ini, [string]$server = "singleUser")

function jadclient {
	param([Parameter(ValueFromRemainingArguments)][string[]]$params)
	& $bin\jadclient path= $path ini= $ini server=$server @params
	if ($lastexitcode) {
		exit $lastexitcode
	}
}

function jadload {
	param([Parameter(ValueFromRemainingArguments)][string[]]$params)
	& $bin\jadloadb path= $path ini= $ini server=$server deleteIfAbsent=true suppressReorg=true @params
	if ($lastexitcode) {
		if ($lastexitcode -eq 8510) {
			Write-Warning "Load completed with methods in error"
		}
		elseif ($lastexitcode -eq 8511) {
			# Reorganisation required warning
		}
		else {
			exit $lastexitcode
		}
	}
}

function jadegit-deploy {
	param([Parameter(ValueFromRemainingArguments)][string[]]$params)
	& $bin\jadegit --path $path --ini $ini --server $server deploy @params
	if ($lastexitcode) {
		exit $lastexitcode
	}
}

function recompile {
	for ($i=1; $i -le 3; $i++) {
		& $bin\jadclient path= $path ini= $ini server=$server schema=RootSchema app=JadeRecompileAllMethods
		if ($lastexitcode -eq 0) {
			return
		}
		elseif ($lastexitcode -ne 1183) {
			exit $lastexitcode
		}
	}
	Write-Warning "Methods still in error"
}

$root = $PSScriptRoot

Write-Host "Deployment Started (bin: $bin, path: $path, ini: $ini, server: $server)"
)src";
	}

	void PowerShellDeploymentBuilder::start(const Registry::Root& registry)
	{
		// Handle checking registry
		if (!registry.repos.empty())
		{
			changeOperation("Checking Registry");
			*deploy << format("jadegit-deploy start --registry \"$root\\update.jgr\"") << endl;
		}
	}

	void PowerShellDeploymentBuilder::finish(const Registry::Root& registry)
	{
		assert(deploy);

		// Perform reorg if required
		flush(true);

		// Recompile Methods
		*deploy << endl << "Write-Host \"Recompile Methods\"" << endl << "recompile" << endl;

		// Handle updating registry
		if (!registry.repos.empty())
		{
			changeOperation("Updating Registry");
			*deploy << format("jadegit-deploy finish --registry \"$root\\update.jgr\"") << endl;

			// Open file
			registry.save(Registry::FileStorage(fs.open("update.jgr")));
		}
	}

	void PowerShellDeploymentBuilder::finish()
	{
		// Indicate end of deployment
		*deploy << endl << "Write-Host \"Deployment Finished\"" << endl << std::flush;
	}

	void PowerShellDeploymentBuilder::addCommandFile(const std::filesystem::path& jcf, const char* loadStyle)
	{
		addSchemaFiles(jcf, filesystem::path(), filesystem::path(), loadStyle);
	}

	void PowerShellDeploymentBuilder::addSchemaFiles(const std::filesystem::path& scm, const std::filesystem::path& ddb, const char* loadStyle)
	{
		addSchemaFiles(filesystem::path(), scm, ddb, loadStyle);
	}

	void PowerShellDeploymentBuilder::addSchemaFiles(const std::filesystem::path& jcf, const std::filesystem::path& scm, const std::filesystem::path& ddb, const char* loadStyle)
	{
		// Indicate change of operation
		changeOperation("Updating Schemas");

		// Add command to load files
		*deploy << format("jadload loadStyle={}", loadStyle);

		if (!jcf.empty())
			*deploy << format(" commandFile= \"$root\\{}\"", jcf.string());

		if (!scm.empty())
			*deploy << format(" schemaFile= \"$root\\{}\"", scm.string());

		if (!ddb.empty())
			*deploy << format(" ddbFile= \"$root\\{}\"", ddb.string());

		*deploy << endl;
	}

	void PowerShellDeploymentBuilder::addReorg()
	{
		// Indicate change of operation
		changeOperation("Performing Reorganization");

		// Add to deployment script
		*deploy << "jadclient schema=RootSchema app=JadeReorgApp startAppParameters action=initiateReorgAllSchemas" << endl;
	}

	void PowerShellDeploymentBuilder::addScript(const Build::Script& script)
	{
		// Flush pending schema load/re-org (if any)
		flush(true);

		// Indicate change of operation
		changeOperation("Running Scripts");

		// Add script to deployment
		*deploy << format("jadclient schema={} app={}", script.schema, script.app);

		if (!script.executeSchema.empty())
			*deploy << format(" executeSchema={}", script.executeSchema);

		if (!script.executeClass.empty())
			*deploy << format(" executeClass={}", script.executeClass);

		if (!script.executeMethod.empty())
			*deploy << format(" executeMethod={}", script.executeMethod);

		if (!script.executeParam.empty())
			*deploy << format(" executeParam= \"{}\"", script.executeParam);

		if (script.executeTransient)
			*deploy << " executeTransient=true";

		if (script.executeTypeMethod)
			*deploy << " executeTypeMethod=true";
		
		*deploy << endl;
	}

	void PowerShellDeploymentBuilder::changeOperation(const char* operation)
	{
		if (this->operation == operation)
			return;

		// Log change of operation
		*deploy << endl << format("Write-Host \"{}\"", operation) << endl;
		this->operation = operation;
	}
}