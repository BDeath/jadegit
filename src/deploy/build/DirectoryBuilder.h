#pragma once
#include "Builder.h"
#include <jadegit/vfs/FileSystem.h>

namespace JadeGit::Deploy
{
	class DirectoryBuilder : public Builder
	{
	public:
		DirectoryBuilder(const FileSystem& fs, std::filesystem::path schemas = "schemas");

		std::unique_ptr<std::ostream> AddCommandFile(bool latestVersion) final;
		std::unique_ptr<std::ostream> AddSchemaFile(const std::string& schema, bool latestVersion) final;
		std::unique_ptr<std::ostream> AddSchemaDataFile(const std::string& schema, bool latestVersion) final;

	protected:
		const FileSystem& fs;
		const std::filesystem::path schemas;

		virtual void addCommandFile(const std::filesystem::path& jcf, const char* loadStyle) = 0;
		virtual void addSchemaFiles(const std::filesystem::path& scm, const std::filesystem::path& ddb, const char* loadStyle) = 0;
		virtual void addReorg() = 0;

		void flush(bool reorgIfNeeded = false) final;

	private:
		int files = 0;
		std::string lastschema;
		const char* loadStyle = nullptr;
		bool needsReorg = false;

		void changeLoadStyle(bool latestVersion);

		File makeSchemaFile(std::string_view schema, std::string_view extension);
		std::filesystem::path makeSchemaFileName(std::string_view schema, std::string_view extension);

		void reorg() final;
	};
}