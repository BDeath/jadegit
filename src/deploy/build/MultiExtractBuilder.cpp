#include "MultiExtractBuilder.h"
#include <jadegit/vfs/File.h>
#include <Exception.h>

using namespace std;

namespace JadeGit::Deploy
{
	MultiExtractBuilder::MultiExtractBuilder(const FileSystem& fs, const string& name, bool supportCommandFiles) : DirectoryBuilder(fs, ""), name(name), supportCommandFiles(supportCommandFiles)
	{
	}

	void MultiExtractBuilder::start()
	{
		// Mul file setup when there's changes to extract
		// Could add check here to check file doesn't already exist?
	}

	void MultiExtractBuilder::finish()
	{
		if (mul)
			*mul << std::flush;
	}

	void MultiExtractBuilder::addCommandFile(const filesystem::path& jcf, const char* loadStyle)
	{
		if (!supportCommandFiles)
			throw unsupported_feature("Loading command files during MUL deployment");

		writeLine(jcf.string());
	}
	
	void MultiExtractBuilder::addSchemaFiles(const filesystem::path& scm, const filesystem::path& ddb, const char* loadStyle)
	{
		if (!scm.empty() && !ddb.empty())
			writeLine(scm.string() + " " + ddb.string());
		else if (!scm.empty())
			writeLine(scm.string());
		else if (!ddb.empty())
			writeLine(ddb.string());
	}

	void MultiExtractBuilder::writeLine(string_view line)
	{
		// Setup mul file when there's changes to extract
		if (!mul)
		{
			mul = fs.open(name + ".mul").createOutputStream();
			*mul << "#MULTIPLE_SCHEMA_EXTRACT" << endl;
			extracted = true;
		}

		*mul << line << endl;
	}
}