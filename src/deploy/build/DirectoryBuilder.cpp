#include "DirectoryBuilder.h"

using namespace std;

namespace JadeGit::Deploy
{
	DirectoryBuilder::DirectoryBuilder(const FileSystem& fs, filesystem::path schemas) : fs(fs), schemas(move(schemas))
	{
	}

	unique_ptr<ostream> DirectoryBuilder::AddCommandFile(bool latestVersion)
	{
		// Handle changing load style
		changeLoadStyle(latestVersion);

		// Flush prior schema load command & increment file prefix
		flush();
		files++;

		// Setup file
		File file = makeSchemaFile("Commands", "jcf");

		// Add load command
		addCommandFile(file.path(), loadStyle);

		// Setup output stream
		return file.createOutputStream();
	}

	unique_ptr<ostream> DirectoryBuilder::AddSchemaFile(const string& schema, bool latestVersion)
	{
		// Handle changing load style
		changeLoadStyle(latestVersion);

		// Flush prior schema load command & increment file prefix
		flush();
		files++;

		// Set indicator for last schema to be loaded with/without data file
		assert(lastschema.empty());
		lastschema = schema;
		
		// Setup file & output stream
		return makeSchemaFile(schema, "scm").createOutputStream();
	}

	unique_ptr<ostream> DirectoryBuilder::AddSchemaDataFile(const string& schema, bool latestVersion)
	{		
		// Handle changing load style
		changeLoadStyle(latestVersion);

		// Check if target schema changing
		filesystem::path scm;
		if (lastschema != schema)
		{
			// Flush prior schema load command & increment file prefix
			flush();
			files++;
		}
		else
		{
			// Derive schema filename to be loaded with data file & clear last
			scm = makeSchemaFileName(schema, "scm");
			lastschema.clear();
		}
		
		// Setup data file
		File file = makeSchemaFile(schema, "ddx");

		// Add load command
		addSchemaFiles(scm, file.path(), loadStyle);

		// Setup output stream
		return file.createOutputStream();
	}

	void DirectoryBuilder::changeLoadStyle(bool latestVersion)
	{
		// Flush pending commands to load last schema file or re-org before switching load style
		if (latestVersion != needsReorg)
			flush(true);

		// Update load style
		loadStyle = latestVersion ? "latestSchemaVersion" : "currentSchemaVersion";

		// Set flag indicating reorg is needed
		needsReorg = latestVersion;
	}

	File DirectoryBuilder::makeSchemaFile(string_view schema, string_view extension)
	{
		// Open schema file
		File file = fs.open(makeSchemaFileName(schema, extension));

		// Check file doesn't already exist
		if (file.exists())
			throw runtime_error(format("File already exists ({})", file.path().string()));

		return file;
	}

	filesystem::path DirectoryBuilder::makeSchemaFileName(string_view schema, string_view extension)
	{
		stringstream filename;
		filename << setfill('0') << setw(2) << files << "-" << schema << "." << extension;
		return schemas / filename.str();
	}

	void DirectoryBuilder::reorg()
	{
		// Flush pending schema load
		flush(false);

		// Add re-org command
		// TODO: Suppress re-org's if they're not needed, i.e. we've not loaded anything into latest schema
		addReorg();
		needsReorg = false;
	}

	void DirectoryBuilder::flush(bool reorgIfNeeded)
	{
		// Add command to load last schema file (which hasn't been followed by a data file)
		if (!lastschema.empty())
		{
			addSchemaFiles(makeSchemaFileName(lastschema, "scm"), filesystem::path(), loadStyle);
			lastschema.clear();
		}

		// Add re-org command if needed
		if (reorgIfNeeded && needsReorg)
			reorg();
	}
}