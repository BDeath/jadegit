#include "XMLDeploymentBuilder.h"
#include <jadegit/vfs/File.h>
#include <registry/Commit.h>

using namespace std;

namespace JadeGit::Deploy
{
	XMLDeploymentBuilder::XMLDeploymentBuilder(const FileSystem& fs) : DirectoryBuilder(fs, "stage-01")
	{
	}

	void XMLDeploymentBuilder::start()
	{
		printer.ClearBuffer();
		printer.OpenElement("Deployment");
	}

	void XMLDeploymentBuilder::start(const Registry::Root& registry)
	{
		string origin;
		string currentCommit;
		string latestCommit;

		// Derive manifest details from registry
		if (registry.repos.size() == 1)
		{
			auto& repo = registry.repos.front();
			origin = repo.origin;

			if (repo.previous.size() == 1)
			{
				ostringstream oss;
				oss << repo.previous.front();
				currentCommit = oss.str();
			}

			if (repo.latest.size() == 1)
			{
				ostringstream oss;
				oss << repo.latest.front();
				latestCommit = oss.str();
			}
		}

		if (!origin.empty())
			printer.PushAttribute("origin", origin.c_str());

		printer.OpenElement("Stage");

		if (!currentCommit.empty())
			printer.PushAttribute("currentCommit", currentCommit.c_str());

		if (!latestCommit.empty())
			printer.PushAttribute("latestCommit", latestCommit.c_str());
	}

	void XMLDeploymentBuilder::finish(const Registry::Root& registry)
	{
		// Perform reorg if required
		flush(true);
	}

	void XMLDeploymentBuilder::finish()
	{
		printer.CloseElement();
		printer.CloseElement();

		auto file = fs.open("deployment.xml").createOutputStream();
		*file << printer.CStr() << std::flush;
	}

	void XMLDeploymentBuilder::addCommandFile(const std::filesystem::path& jcf, const char* loadStyle)
	{
		addSchemaFile("CommandFile", jcf, loadStyle);
	}

	void XMLDeploymentBuilder::addSchemaFile(const char* type, const std::filesystem::path& path, const char* loadStyle)
	{
		// Add file to deployment
		printer.OpenElement(type);
		pushAttribute("path", path.generic_string());
		pushAttribute("loadStyle", loadStyle);
		printer.CloseElement();
	}

	void XMLDeploymentBuilder::addSchemaFiles(const std::filesystem::path& scm, const std::filesystem::path& ddb, const char* loadStyle)
	{
		if (!scm.empty())
			addSchemaFile("SchemaFile", scm, loadStyle);

		if (!ddb.empty())
			addSchemaFile("SchemaDataFile", ddb, loadStyle);
	}

	void XMLDeploymentBuilder::addReorg()
	{
		// Add re-org to deployment
		printer.OpenElement("Reorg");
		printer.CloseElement();
	}

	void XMLDeploymentBuilder::addScript(const Build::Script& script)
	{
		// Flush pending schema load/re-org (if any)
		flush(true);

		// Add script to deployment
		printer.OpenElement("Script");
		pushAttributeIfNotEmpty("schema", script.schema);
		pushAttributeIfNotEmpty("app", script.app);
		pushAttributeIfNotEmpty("executeSchema", script.executeSchema);
		pushAttributeIfNotEmpty("executeClass", script.executeClass);
		pushAttributeIfNotEmpty("executeMethod", script.executeMethod);
		pushAttributeIfNotEmpty("executeParam", script.executeParam);
		pushAttributeIfNotEmpty("executeTransient", script.executeTransient);
		pushAttributeIfNotEmpty("executeTypeMethod", script.executeTypeMethod);
		printer.CloseElement();
	}
}