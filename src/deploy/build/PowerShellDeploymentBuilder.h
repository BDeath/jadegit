#pragma once
#include "DirectoryBuilder.h"

namespace JadeGit::Deploy
{
	class PowerShellDeploymentBuilder : public DirectoryBuilder
	{
	public:
		PowerShellDeploymentBuilder(const FileSystem& fs);

	protected:
		void start() final;
		void start(const Registry::Root& registry) final;
		void finish(const Registry::Root& registry) final;
		void finish() final;

		void addCommandFile(const std::filesystem::path& jcf, const char* loadStyle) final;
		void addSchemaFiles(const std::filesystem::path& scm, const std::filesystem::path& ddb, const char* loadStyle) final;
		void addReorg() final;
		void addScript(const Build::Script& script) final;

	private:
		std::unique_ptr<std::ostream> deploy;
		const char* operation = nullptr;

		void addSchemaFiles(const std::filesystem::path& jcf, const std::filesystem::path& scm, const std::filesystem::path& ddb, const char* loadStyle);
		void changeOperation(const char* operation);
	};
}