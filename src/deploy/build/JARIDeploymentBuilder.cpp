#include "JARIDeploymentBuilder.h"
#include <jadegit/vfs/File.h>
#include <format>
#include <registry/storage/File.h>

using namespace std;

namespace JadeGit::Deploy
{
	JARIDeploymentBuilder::JARIDeploymentBuilder(const FileSystem& fs, const std::string& name, const std::string& description) : DirectoryBuilder(fs), name(name), description(description)
	{
	}

	void JARIDeploymentBuilder::start()
	{
		// Start writing instruction file
		jri = fs.open("deploy.jri").createOutputStream();
		*jri << "[ReleaseControl]" << endl;
		*jri << "Environment=" << name << endl;
		*jri << "ShortDescription=" << description << endl;
		*jri << endl;

		// Set initial batch file name
		nextbatch = "misc/live_preload.bat";
	}

	void JARIDeploymentBuilder::start(const Registry::Root& registry)
	{
		// Handle checking registry
		if (!registry.repos.empty())
			addBatchDeployCommand("start --registry %env_miscpath%\\temp\\update.jgr");
	}

	void JARIDeploymentBuilder::finish(const Registry::Root& registry)
	{
		// Perform reorg if required
		flush(true);

		// Handle updating registry
		// NOTE: This is appended to last batch file in schemas directory so it's run before recompiling methods, which could cause JARI to abort/fail
		if (!registry.repos.empty())
		{
			addBatchDeployCommand("finish --registry %env_miscpath%\\temp\\update.jgr");

			// Save registry file
			registry.save(Registry::FileStorage(fs.open("misc\\temp\\update.jgr")));
		}

		// Close last batch/deploy file
		batch.reset();
		deploy.reset();

		// Set final batch file name for post-deployment scripts to run after methods have been recompiled
		nextbatch = "misc/live_postload.bat";
	}

	void JARIDeploymentBuilder::finish()
	{
		// Finish writing instruction file if started
		if (!jri)
			return;

		// Write deploy files
		*jri << "[DeployFiles]" << endl;
		if (fs.open(schemas).exists())
			*jri << "SchemaFilesDir=" << (workingDirectory / schemas).string() << endl;
		if (fs.open("misc").exists())
			*jri << "x64-msoft-win64ServerMisc=" << (workingDirectory / "misc").string() << endl;
		*jri << endl;
		
		// Notifications (currently required to specify authorized user)
		*jri << "[Notifications]" << endl;
		if (!developerId.empty())
			*jri << "DeveloperId=" << developerId << endl;
		*jri << endl;

		// Deployment options
		*jri << "[DeploymentOptions]" << endl;
		if (defeatPreBackup)
			*jri << "DefeatPreInstallationBackup=true" << endl;
		if (defeatPostBackup)
			*jri << "DefeatPostInstallationBackup=true" << endl;
	}

	void JARIDeploymentBuilder::addBatchCommand(const std::string& command)
	{
		// Setup batch file if needed
		if (!batch)
		{
			batch = fs.open(nextbatch).createOutputStream();

			// Append to deploy file if commands are being executed during schema loads
			if (deploy)
				*deploy << nextbatch.filename().string() << endl;
		}

		// Add batch command
		*batch << command << endl;
	}

	void JARIDeploymentBuilder::addBatchDeployCommand(const std::string& command)
	{
		addBatchCommand("%env_binpath%\\jadegit.exe --path %env_systempath% --ini %env_inifile% deploy " + command);
	}

	void JARIDeploymentBuilder::addCommandFile(const std::filesystem::path& jcf, const char* loadStyle)
	{
		addDeployFile(jcf);
	}

	void JARIDeploymentBuilder::addDeployFile(std::filesystem::path path)
	{
		assert(path.parent_path() == schemas);

		// Close last batch file
		batch.reset();

		// Setup deploy file if needed
		if (!deploy)
			deploy = fs.open(schemas / "deployin.txt").createOutputStream();

		// Append file
		*deploy << path.filename().string() << endl;

		// Update filename for next batch file
		nextbatch = path.replace_extension("bat");
	}

	void JARIDeploymentBuilder::addSchemaFiles(const std::filesystem::path& scm, const std::filesystem::path& ddb, const char* loadStyle)
	{
		if (!scm.empty())
			addDeployFile(scm);

		if (!ddb.empty())
			addDeployFile(ddb);
	}

	void JARIDeploymentBuilder::addScript(const Build::Script& script)
	{
		// Build batch command
		ostringstream command;
		command << format("%env_binpath%\\jadloadb path=%env_systempath% ini=%env_inifile% schema={} app={}", script.schema, script.app);

		if (!script.executeSchema.empty())
			command << format(" executeSchema={}", script.executeSchema);

		if (!script.executeClass.empty())
			command << format(" executeClass={}", script.executeClass);

		if (!script.executeMethod.empty())
			command << format(" executeMethod={}", script.executeMethod);

		if (!script.executeParam.empty())
			command << format(" executeParam=\"{}\"", script.executeParam);

		if (script.executeTransient)
			command << " executeTransient=true";

		if (script.executeTypeMethod)
			command << " executeTypeMethod=true";

		// Add batch command
		addBatchCommand(command.str());
	}
}