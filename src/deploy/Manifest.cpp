#include "Manifest.h"
#include <jade/AppContext.h>
#include <toml++/toml.h>
#include <fstream>

using namespace std;
using namespace Jade;

namespace JadeGit::Deploy
{
	filesystem::path manifestPath()
	{
		// Define folder path within system directory
		auto path = AppContext::dbPath() / ".jadegit";
		filesystem::create_directories(path);

		// Append manifest filename
		path /= "deploy.manifest";
		return path;
	}

	Manifest Manifest::get()
	{
		ifstream file(manifestPath(), ios::in);
		toml::table table = toml::parse(file);

		Manifest manifest;
		manifest.origin = table["origin"].value_or("");
		manifest.commit = table["commit"].value_or("");
		return manifest;
	}

	void Manifest::set(const Manifest& manifest)
	{
		toml::table table;
		table.insert_or_assign("origin", manifest.origin);
		table.insert_or_assign("commit", manifest.commit);

		fstream file(manifestPath(), ios::out);
		file << table << std::flush;
	}

	void Manifest::set(string origin, string commit)
	{
		auto manifest = get();
		manifest.origin = origin;
		manifest.commit = commit;
		set(manifest);
	}
}