#include <jadegit/Progress.h>
#include <assert.h>

using namespace std;

namespace JadeGit
{
	bool IProgress::start(size_t size, const char* task)
	{
		steps.push_back(0);
		total.push_back(size);
		breadcrumbs.push_back(task ? task : string());

		return (!task || refresh(true));
	}

	bool IProgress::update(size_t completed, const char* task)
	{
		assert(completed <= total.back());
		steps.back() = completed;

		if (task)
			breadcrumbs.back() = task;

		return refresh(!!task);
	}

	bool IProgress::step()
	{
		assert(steps.back() < total.back());
		steps.back()++;
		return refresh();
	}

	bool IProgress::skip()
	{
		if (total.back() == 1)
			return step();

		assert(steps.back() < total.back());
		total.back()--;
		return refresh();
	}

	bool IProgress::finish()
	{
		steps.pop_back();
		total.pop_back();
		breadcrumbs.pop_back();

		return steps.empty() || step();  // Completed a step within parent phase
	}

	bool IProgress::refresh(bool full)
	{
		if (!full)
		{
			update(progress());
			return !wasCancelled();
		}

		string trail;
		for (auto crumb : breadcrumbs)
		{
			if (crumb.empty())
				continue;

			if (!trail.empty())
				trail += " - ";

			trail += crumb;
		}

		update(progress(), trail.c_str());
		return !wasCancelled();
	}

	double IProgress::progress(size_t n) const
	{
		if (n < steps.size())
		{
			auto parts = static_cast<double>(total[n]);
			return (steps[n] / parts) + (1 / parts) * progress(n + 1);
		}
        else
            return 0;
	}
}