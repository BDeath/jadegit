#pragma once
#include <registry/Data_generated.h>

namespace JadeGit::Registry
{
	class Visitor
	{
	public:
		virtual bool visit(const DataT& registry);
		virtual bool visitEnter(const DataT& registry) { return true; }
		virtual bool visitLeave(const DataT& registry) { return true; }

		virtual bool visit(const RepositoryT& repo);
		virtual bool visitEnter(const RepositoryT& repo) { return true; }
		virtual bool visitLeave(const RepositoryT& repo) { return true; }

		virtual bool visit(const CommitT& commit, bool first, bool latest) { return false; }
		virtual bool visit(const SchemaT& schema, bool first) { return false; }
	};
}