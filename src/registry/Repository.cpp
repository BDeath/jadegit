#include "Repository.h"
#include "Commit.h"
#include "Printer.h"
#include "Schema.h"

using namespace std;

namespace JadeGit::Registry
{
	ostream& operator<<(ostream& os, const RepositoryT& repo)
	{
		Printer printer(os);
		printer.visit(repo);
		return os;
	}

	bool operator==(const RepositoryT& lhs, const RepositoryT& rhs)
	{
		return lhs.name == rhs.name &&
			lhs.origin == rhs.origin &&
			lhs.latest == rhs.latest &&
			lhs.previous == rhs.previous &&
			lhs.schemas == rhs.schemas;
	}

	bool repo_match(const RepositoryT& lhs, const RepositoryT& rhs)
	{
		// Match by origin
		if (!lhs.origin.empty() && lhs.origin == rhs.origin)
			return true;

		// Match by commit
		if (!lhs.latest.empty() && (lhs.latest == rhs.previous || lhs.previous == rhs.previous && lhs.latest == rhs.latest))
			return true;

		// Match by name & schemas
		if (!lhs.name.empty() && lhs.name == rhs.name)
		{
			// Match schemas exactly
			if (lhs.schemas == rhs.schemas)
				return true;

			// Match schemas based on any intersection
			for (auto& ls : lhs.schemas)
			{
				for (auto& rs : rhs.schemas)
					if (ls.name == rs.name)
						return true;
			}
		}

		// No match
		return false;
	}

	bool repo_unknown(const RepositoryT& repo)
	{
		return repo.name.empty() && repo.origin.empty();
	}

	void repo_update(RepositoryT& lhs, const RepositoryT& rhs, bool force)
	{
		assert(!repo_unknown(rhs));

		// Check updated version corresponds to prior version
		if (!force && lhs.latest != rhs.previous && (lhs.previous != rhs.previous || lhs.latest != rhs.latest))
			throw runtime_error(format("Repository update doesn't correspond to prior version ({})", lhs.name));

		// Update details
		lhs.name = rhs.name;
		lhs.origin = rhs.origin;
		lhs.latest = rhs.latest;
		lhs.previous = rhs.previous;
		lhs.schemas = rhs.schemas;
	}
}

#if USE_GIT2
#include <jadegit/data/Assembly.h>
#include <vfs/FileSignature.h>
#include <vfs/GitFileSystem.h>
#include <regex>

namespace JadeGit::Registry
{
	string repo_name(const std::string& origin)
	{
		static regex rgx("^.*(?:\\/|\\\\)(.*?)(?:\\.git$|$)");
		smatch matches;
		if (regex_match(origin, matches, rgx))
			return matches[1].str();

		return string();
	}

	string repo_name(const git_repository& repo)
	{
		return repo_name(repo_origin(repo));
	}

	string repo_origin(const git_repository& repo)
	{
		unique_ptr<git_remote> remote;
		auto result = git_remote_lookup(git_ptr(remote), const_cast<git_repository*>(&repo), "origin");
		if (result == GIT_ENOTFOUND)
			return git_repository_path(&repo) ? git_repository_path(&repo) : ".";

		git_throw(result);
		return git_remote_url(remote.get());
	}

	bool repo_match(const RepositoryT& lhs, const git_repository& rhs)
	{
		// Ignore unknown
		if (repo_unknown(lhs))
			return false;

		// Match by origin
		if (!lhs.origin.empty() && lhs.origin == repo_origin(rhs))
			return true;

		// Match by commit (which will attempt to fetch missing commits)
		// NOTE: Checking for schema intersection was also used originally, however this strategy wasn't reliable due to edge cases where there may be no overlap
		if (!lhs.latest.empty())
		{
			for (auto& commit : lhs.latest)
			{
				if (commit_exists(rhs, commit))
					return true;
			}
		}
		
		// No match
		return false;
	}
}
#endif


#if USE_JADE && USE_GIT2
#include <jade/Iterator.h>
#include <jade/Object.h>

namespace JadeGit::Registry
{
	using namespace Jade;

	RepositoryT repo_init(const string& name, const filesystem::path& path)
	{
		// Open repository
		unique_ptr<git_repository> repo;
		git_throw(git_repository_open(git_ptr(repo), path.string().c_str()));

		// Setup registry data
		RepositoryT new_;
		new_.origin = repo_origin(*repo);
		new_.name = name.empty() ? repo_name(new_.origin) : name;

		// Lookup current branch
		unique_ptr<git_reference> head;
		git_throw(git_repository_head(git_ptr(head), repo.get()));

		// Lookup commit
		unique_ptr<git_commit> commit;
		git_throw(git_commit_lookup(git_ptr(commit), repo.get(), git_reference_target(head.get())));

		// Add latest commit
		new_.latest.push_back(make_commit(*commit.get()));

		// Setup assembly to query known schemas
		GitFileSystem fs(move(commit), FileSignature::suppressed());
		JadeGit::Data::Assembly assembly(fs);

		// Iterate installed schemas
		Object object;
		Iterator<Object> iter(DskSchema(&RootSchemaOid), PRP_Schema__schemasByNumber);
		while (iter.next(object))
		{
			// Get installed schema name
			auto name = object.getProperty<string>(TEXT("name"));

			// Query assembly to check if schema is known
			auto schema = JadeGit::Data::Entity::resolve<JadeGit::Data::Schema>(assembly, name);

			// Add to registry if known & not predefined/static
			if (schema && !schema->isStatic())
				new_.schemas.push_back(make_schema(name));
		}

		return new_;
	}
}
#endif
