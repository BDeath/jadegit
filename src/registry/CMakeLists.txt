target_link_libraries(jadegit PRIVATE flatbuffers)
flatbuffers_generate_headers(
	TARGET jadegit_registry 
	INCLUDE_PREFIX registry
	SCHEMAS ${CMAKE_CURRENT_LIST_DIR}/Data.fbs
	FLAGS --gen-object-api --reflect-names
)
target_link_libraries(jadegit PRIVATE jadegit_registry)

target_sources(jadegit PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/Commit.cpp
	${CMAKE_CURRENT_LIST_DIR}/Printer.cpp
	${CMAKE_CURRENT_LIST_DIR}/Repository.cpp
	${CMAKE_CURRENT_LIST_DIR}/Root.cpp
	${CMAKE_CURRENT_LIST_DIR}/Validator.cpp
	${CMAKE_CURRENT_LIST_DIR}/Visitor.cpp
)

if (USE_GIT2)
	target_sources(jadegit PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/Fetch.cpp	
		${CMAKE_CURRENT_LIST_DIR}/Manager.cpp
	)
endif()

include(${CMAKE_CURRENT_LIST_DIR}/storage/CMakeLists.txt)