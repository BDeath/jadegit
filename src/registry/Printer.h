#pragma once
#include "Visitor.h"
#include <ostream>

namespace JadeGit::Registry
{
	class Printer : public Visitor
	{
	public:
		Printer(std::ostream& os);

		using Visitor::visit;

	protected:
		std::ostream& os;

		bool visitEnter(const DataT& registry) final;
		bool visitEnter(const RepositoryT& repo) final;
		bool visitLeave(const RepositoryT& repo) final;
		bool visit(const CommitT& commit, bool first, bool latest) final;
		bool visit(const SchemaT& schema, bool first) final;
	};
}