#include "Fetch.h"
#include <jadegit/git2.h>

using namespace std;

namespace JadeGit::Registry
{
	void fetch(const git_repository& repo, const git_strarray* refspecs)
	{
		// Lookup remote
		unique_ptr<git_remote> remote;
		git_throw(git_remote_lookup(git_ptr(remote), const_cast<git_repository*>(&repo), "origin"));

		// Load any extra headers from config (possibly needed for authorization)
		unique_ptr<git_config> config;
		git_throw(git_repository_config(git_ptr(config), const_cast<git_repository*>(&repo)));

		vector<string> headers;
		unique_ptr<git_config_iterator> iter;
		git_throw(git_config_multivar_iterator_new(git_ptr(iter), config.get(), format("http.{}.extraheader", git_remote_url(remote.get())).c_str(), nullptr));
		while (true)
		{
			git_config_entry* entry = nullptr;
			auto error = git_config_next(&entry, iter.get());
			if (error == GIT_ITEROVER)
				break;
			git_throw(error);

			if (entry->value)
				headers.push_back(entry->value);
		}

		vector<char*> cheaders;
		cheaders.reserve(headers.size());
		for (auto& s : headers)
			cheaders.push_back(&s[0]);

		// Setup fetch options
		git_fetch_options opts = GIT_FETCH_OPTIONS_INIT;
		opts.custom_headers = git_strarray{ cheaders.data(), cheaders.size() };
		opts.depth = git_fetch_depth_t::GIT_FETCH_DEPTH_UNSHALLOW;					// Need commit history for blame to provide entity signatures for changes in prior commits
		opts.proxy_opts.type = git_proxy_t::GIT_PROXY_AUTO;
		opts.update_fetchhead = 0;

		// Fetch commit
		auto result = git_remote_fetch(remote.get(), refspecs, &opts, nullptr);
		if (result != GIT_EEOF)
			git_throw(result);
	}

	void fetch(const git_repository& repo, const git_oid& commit)
	{
		char* refspec[] = { git_oid_tostr_s(&commit) };
		git_strarray refspecs = { refspec, 1 };

		fetch(repo, &refspecs);
	}

	void fetch(const git_repository& repo, const CommitT& commit)
	{
		git_oid oid = { 0 };
		git_throw(git_oid_fromraw(&oid, commit.id.data()));

		fetch(repo, oid);
	}

	void fetch(const git_repository& repo, const std::vector<JadeGit::Registry::CommitT>& commits)
	{
		vector<string> oids;
		oids.reserve(commits.size());
		for (auto& commit : commits)
		{
			git_oid oid = { 0 };
			git_throw(git_oid_fromraw(&oid, commit.id.data()));

			oids.push_back(git_oid_tostr_s(&oid));
		}

		vector<char*> cstrings;
		cstrings.reserve(oids.size());
		for (auto& s : oids)
			cstrings.push_back(&s[0]);

		git_strarray refspecs = { cstrings.data(), cstrings.size() };

		fetch(repo, &refspecs);
	}
}