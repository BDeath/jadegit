#pragma once
#include <jadegit/build.h>
#include <jadegit/Version.h>
#include <registry/Data_generated.h>
#include <filesystem>
#include <iosfwd>

namespace JadeGit::Registry
{
	using namespace std;
	using namespace std::filesystem;

	class Storage;
	class Visitor;

	class Root : public DataT
	{
	public:
#if USE_JADE && USE_GIT2
		static void init(const string& name, const path& repo, const path& path);
#endif
#if USE_JADE 
		static Root extract();
		static void extract(const path& path);
		static void load(const Root& update, bool force);
		static void load(const path& path, bool force = false);
		static void validate(const Root& update, bool force);
		static void validate(const path& path, bool force = false);
#endif
		static void write(const path& path, ostream& os);

		Root();
		Root(const Storage& storage);
		Root(const RepositoryT& repo);
		Root(const Version& platform);

		template<class Predicate>
		typename RepositoryT* find_repo(const Predicate& predicate)
		{
			auto it = find_if(repos.begin(), repos.end(), predicate);
			return it != end(repos) ? &*it : nullptr;
		}

		RepositoryT* find_repo_by_name(const string& name);
		RepositoryT* find_repo_by_origin(const string& origin);

		bool accept(Visitor& visitor) const;
		RepositoryT* add(const RepositoryT& repo);
		void update(const Root& from, bool force);
		void save(const Storage& storage) const;

		bool operator==(const Root& rhs) const;
	};

	ostream& operator<<(ostream& os, const Root& registry);
}