#pragma once
#include <jadegit/git2.h>
#include <vfs/FileSignature.h>
#include <filesystem>

namespace JadeGit
{
	class GitFileBlame;

	class GitFileSystemSource
	{
	public:
		GitFileSystemSource(git_repository* repo) : repo(repo) {}
		virtual ~GitFileSystemSource() {};

		void add(std::unique_ptr<GitFileSystemSource> source);

		virtual std::unique_ptr<GitFileBlame> blame(const std::filesystem::path& path, const git_oid& id) const;
		virtual void merge(std::shared_ptr<git_tree>& dst) const = 0;

	protected:
		git_repository* const repo;

		std::unique_ptr<GitFileBlame> blame(const std::unique_ptr<git_commit>& commit, const std::filesystem::path& path) const;

		bool matches(const git_tree* tree, const std::filesystem::path& path, const git_oid& id) const;
		void merge(const std::shared_ptr<git_tree>& src, std::shared_ptr<git_tree>& dst) const;

	private:
		std::unique_ptr<GitFileSystemSource> next;
	};

	class GitFileCommitSource : public GitFileSystemSource
	{
	public:
		GitFileCommitSource(std::unique_ptr<git_commit> commit);
		GitFileCommitSource(std::unique_ptr<git_reference> reference);

	protected:
		void merge(std::shared_ptr<git_tree>& dst) const override;
		std::unique_ptr<GitFileBlame> blame(const std::filesystem::path& path, const git_oid& id) const override;

	private:
		std::unique_ptr<git_commit> resolve(const std::unique_ptr<git_reference> reference) const;

		std::unique_ptr<git_commit> commit;
		std::shared_ptr<git_tree> tree;
	};

	class GitFileTreeSource : public GitFileSystemSource
	{
	public:
		GitFileTreeSource(std::string author, git_oid tree, std::unique_ptr<git_commit> ours, std::unique_ptr<git_commit> theirs = std::unique_ptr<git_commit>());

	protected:
		void merge(std::shared_ptr<git_tree>& dst) const override;
		std::unique_ptr<GitFileBlame> blame(const std::filesystem::path& path, const git_oid& id) const override;

	private:
		FileSignature signature;
		std::shared_ptr<git_tree> tree;

		std::unique_ptr<git_commit> ours;
		std::unique_ptr<git_tree> our_tree;

		std::unique_ptr<git_commit> theirs;
		std::unique_ptr<git_tree> their_tree;
	};
}