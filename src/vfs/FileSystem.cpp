#include <jadegit/vfs/FileSystem.h>
#include <jadegit/vfs/AnyFileIterator.h>

namespace JadeGit
{
	FileSystem::FileSystem() {}
	FileSystem::~FileSystem() {}

	void FileSystem::flush() const {}

	AnyFileIterator::AnyFileIterator(std::unique_ptr<AnyFile>&& parent) : parent(std::move(parent)) {}
	AnyFileIterator::~AnyFileIterator() {}
}