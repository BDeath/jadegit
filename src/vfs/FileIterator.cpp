#include <jadegit/vfs/FileIterator.h>
#include <jadegit/vfs/AnyFileIterator.h>
#include <jadegit/vfs/File.h>
#include <jadegit/vfs/FileSystem.h>

namespace JadeGit
{
	FileIterator::FileIterator() noexcept {}

	FileIterator::FileIterator(std::unique_ptr<AnyFileIterator>&& i) : iter(std::move(i))
	{
		if (iter && iter->valid())
			current = iter->parent->open(iter->filename());
	}

	/* Copy handle by cloning internal value */
	FileIterator::FileIterator(const FileIterator& handle) : iter(handle.iter ? handle.iter->clone() : nullptr), current(handle.current) {}

	FileIterator::FileIterator(FileIterator&& handle) : iter(std::move(handle.iter)), current(handle.current) {}

	FileIterator::~FileIterator() {}

	FileIterator& FileIterator::operator=(const FileIterator& rhs)
	{
		if (rhs.iter)
			iter = rhs.iter->clone();
		else
			iter.reset(nullptr);

		current = rhs.current;

		return *this;
	}

	const File& FileIterator::operator*() const
	{
		return current;
	}

	const File* FileIterator::operator->() const
	{
		return &current;
	}

	FileIterator& FileIterator::operator++()
	{
		if (iter)
		{
			iter->next();
			current = iter->valid() ? iter->parent->open(iter->filename()) : File();
		}

		return *this;
	}

	bool FileIterator::operator==(const FileIterator& it) const
	{
		const FileSystem* fs1 = nullptr;
		const FileSystem* fs2 = nullptr;
		std::filesystem::path path1;
		std::filesystem::path path2;
		bool valid1 = false;
		bool valid2 = false;

		// Check if both iterators operate on the same file system
		fs1 = (iter ? iter->parent->vfs() : nullptr);
		fs2 = (it.iter ? it.iter->parent->vfs() : nullptr);

		// Check first iterator
		if (iter && iter->valid())
		{
			valid1 = true;
			path1 = iter->parent->path() / iter->filename();
		}

		// Check second iterator
		if (it.iter && it.iter->valid())
		{
			valid2 = true;
			path2 = it.iter->parent->path() / it.iter->filename();
		}

		// Consider iterators to be equal if backed by same filesystem and paths are equal
		// Otherwise, consider them equal if they're both invalid
		return valid1 && valid2 ? fs1 == fs2 && path1 == path2 : valid1 == valid2;
	}

	bool FileIterator::operator!=(const FileIterator& it) const
	{
		return !((*this) == it);
	}

	// Range based support
	FileIterator begin(FileIterator iter) noexcept
	{
		return iter;
	}

	FileIterator end(const FileIterator&) noexcept
	{
		return FileIterator();
	}
}