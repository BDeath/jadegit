#include <jadegit/vfs/MemoryFileSystem.h>
#include <jadegit/vfs/File.h>
#include <jadegit/vfs/RecursiveFileIterator.h>
#include "MemoryFile.h"
#include "MemoryFileHandle.h"

using namespace std;

namespace JadeGit
{
	MemoryFileSystem::MemoryFileSystem() : root(new MemoryFileDirectory()) {}

	MemoryFileSystem::MemoryFileSystem(const FileSystem& src) : root(new MemoryFileDirectory())
	{
		// Copy the source file system
		for (auto& file : RecursiveFileIterator(src.open("")))
		{
			if (!file.isDirectory())
				file.copy(open(file.path()));
		}
	}

	MemoryFileSystem::~MemoryFileSystem()
	{
		delete root;
	}

	File MemoryFileSystem::open(const filesystem::path& path) const
	{
		return File(std::make_unique<MemoryFileHandle>(*this, path));
	}

	ostream& operator<<(ostream& os, const MemoryFileSystem& fs)
	{
		bool first = true;

		for (auto& file : RecursiveFileIterator(fs.open("")))
		{
			if (file.isDirectory())
				continue;

			if (!first)
				os << endl;
			else
				first = false;

			os << "[" << file.path().generic_string() << "]" << endl;
			os << file;
		}
		return os;
	}
}