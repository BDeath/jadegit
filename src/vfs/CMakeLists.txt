target_sources(jadegit PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/AnyFile.cpp
	${CMAKE_CURRENT_LIST_DIR}/File.cpp
	${CMAKE_CURRENT_LIST_DIR}/FileIterator.cpp
	${CMAKE_CURRENT_LIST_DIR}/FileSystem.cpp
	${CMAKE_CURRENT_LIST_DIR}/MemoryFile.cpp
	${CMAKE_CURRENT_LIST_DIR}/MemoryFileHandle.cpp
	${CMAKE_CURRENT_LIST_DIR}/MemoryFileIterator.cpp
	${CMAKE_CURRENT_LIST_DIR}/MemoryFileSystem.cpp
	${CMAKE_CURRENT_LIST_DIR}/NativeFile.cpp
	${CMAKE_CURRENT_LIST_DIR}/NativeFileIterator.cpp
	${CMAKE_CURRENT_LIST_DIR}/NativeFileSystem.cpp
	${CMAKE_CURRENT_LIST_DIR}/RecursiveFileIterator.cpp
	${CMAKE_CURRENT_LIST_DIR}/TempFileSystem.cpp
)

if(USE_GIT2)
	target_sources(jadegit PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/GitDiffSource.cpp
		${CMAKE_CURRENT_LIST_DIR}/GitFileBlame.cpp
		${CMAKE_CURRENT_LIST_DIR}/GitFileSystem.cpp
		${CMAKE_CURRENT_LIST_DIR}/GitFileSystemSource.cpp
	)
endif()