#include "GitDiffSource.h"
#include <jadegit/vfs/File.h>

namespace JadeGit
{
	/*
		Diff callback function called foreach file
	*/
	struct DiffPayload
	{
	public:
		DiffPayload(const GitDiffSource& source, Build::Source::Reader& reader, IProgress* progress) : source(source), reader(reader), progress(progress)
		{
		}

		int foreach_file(const git_diff_delta* delta)
		{
			if (delta->status != git_delta_t::GIT_DELTA_ADDED)
				reader(source.previous->open(delta->old_file.path), false);

			if (delta->status != git_delta_t::GIT_DELTA_DELETED)
				reader(source.latest->open(delta->new_file.path), true);

			return (!progress || progress->step()) ? GIT_OK : GIT_EUSER;
		}

	private:
		const GitDiffSource& source;
		Build::Source::Reader& reader;
		IProgress* progress = nullptr;
	};
	
	int diff_foreach_file(const git_diff_delta *delta, float progress, void *payload)
	{
		return static_cast<DiffPayload*>(payload)->foreach_file(delta);
	}
	
	GitDiffSource::GitDiffSource(git_repository* repo, const GitFileSystem* previous, const GitFileSystem* latest) : repo(repo), previous(previous), latest(latest)
	{
	}

	bool GitDiffSource::read(Source::Reader reader, IProgress* progress) const
	{
		// Perform diff if required
		if (!diff)
		{
			// Initialize diff options
			git_diff_options opts;
			git_throw(git_diff_options_init(&opts, GIT_DIFF_OPTIONS_VERSION));

			// Perform diff between current and target tree
			git_throw(git_diff_tree_to_tree(git_ptr(diff), repo, previous ? static_cast<git_tree*>(*previous) : nullptr, latest ? static_cast<git_tree*>(*latest) : nullptr, &opts));
		}

		if (progress && !progress->start(git_diff_num_deltas(diff.get()), "Reading"))
			return false;

		// Iterate diff deltas
		DiffPayload payload(*this, reader, progress);
		if (git_throw(git_diff_foreach(diff.get(), diff_foreach_file, nullptr, nullptr, nullptr, &payload)) == GIT_EUSER)
			return false;

		return !progress || progress->finish();
	}
}