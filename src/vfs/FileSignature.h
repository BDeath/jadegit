#pragma once
#include <chrono>
#include <string>
#include <string_view>
#include <git2/oid.h>

namespace JadeGit
{
	using namespace std;
	using namespace std::chrono;

	class FileSignature
	{
	public:
		FileSignature() = default;
		FileSignature(FileSignature&&) = default;
		FileSignature(const FileSignature&) = delete;
		FileSignature(string author, system_clock::time_point modified = system_clock::now()) : author(move(author)), modified(modified) {}
		FileSignature(string author, const git_oid& commit, system_clock::time_point modified = system_clock::now()) : author(move(author)), commit(commit), modified(modified) {}
		FileSignature(string author, int patch, system_clock::time_point modified = system_clock::now()) : author(move(author)), patch(patch), modified(modified) {}

		const string author;
		const git_oid commit = { 0 };
		const int patch = 0;
		const system_clock::time_point modified;

		bool operator==(const FileSignature& rhs) const = default;

		inline bool empty() const
		{
			return author.empty() && git_oid_is_zero(&commit) && !patch;
		}

		inline string modifiedBy(size_t max_length) const
		{
			string name = author.empty() ? "<unknown>" : author;

			if (git_oid_is_zero(&commit))
				return name.substr(0, max_length);
			
			name += " ";
			char buffer[9] = "";
			return name.substr(0, max_length - 10) + "(" + git_oid_tostr(buffer, 9, &commit) + ")";
		}

		// Use empty file signature to suppress resolving actual author/commit when it's not required
		static FileSignature* suppressed()
		{
			static FileSignature empty;
			return &empty;
		}
	};
}