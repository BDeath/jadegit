#pragma once
#include <jadegit/vfs/AnyFileIterator.h>

namespace JadeGit
{
	class NativeFileIterator : public AnyFileIterator
	{
	public:
		NativeFileIterator(std::unique_ptr<AnyFile>&& parent, const std::filesystem::directory_iterator& iter);
		virtual ~NativeFileIterator();

		std::unique_ptr<AnyFileIterator> clone() const override;
		bool valid() const override;
		std::filesystem::path filename() const override;
		void next() override;

	protected:
		std::filesystem::directory_iterator iter;
	};
}