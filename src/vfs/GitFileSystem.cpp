#include "GitFileSystem.h"
#include "GitFileBlame.h"
#include "GitFileStream.h"
#include "GitFileSystemSource.h"
#include <jadegit/vfs/AnyFileIterator.h>
#include <jadegit/vfs/File.h>
#include <assert.h>

using namespace std;

namespace JadeGit
{
	template <class File>
	class GitFile : public AnyFile
	{
	public:
		GitFile(const GitFileSystem& fs, const filesystem::path& path) : fs(fs), relative_path(path) {}

		const GitFileSystem* vfs() const final
		{
			return &fs;
		}

		filesystem::path path() const final
		{
			return relative_path;
		}

		unique_ptr<istream> createInputStream(ios_base::openmode mode) const final
		{
			return make_unique<GitFileStream>(fs, relative_path, mode, static_cast<const File*>(this)->blob());
		}

	protected:
		const GitFileSystem& fs;
		filesystem::path relative_path;
	};

	class GitIndexFileIterator : public AnyFileIterator
	{
	public:
		GitIndexFileIterator(unique_ptr<AnyFile>&& parent, git_index* index, string directory) : AnyFileIterator(move(parent)), index(index), prefix(move(directory))
		{
			if (!prefix.empty())
			{
				if (prefix.back() != '/')
					throw logic_error("Invalid directory prefix");

				git_throw(git_index_find_prefix(&iter, index, prefix.c_str()));
			}

			next();
		}

		unique_ptr<AnyFileIterator> clone() const final
		{
			auto clone = make_unique<GitIndexFileIterator>(parent->clone(), index, prefix);
			clone->iter = iter;
			clone->current = current;
			return clone;
		}

		bool valid() const
		{
			return current != filesystem::path();
		}

		filesystem::path filename() const final
		{
			return current;
		}

		void next() final
		{
			while (iter < git_index_entrycount(index))
			{
				auto entry = git_index_get_byindex(index, iter++);
				if (!entry)
					break;

				string_view path(entry->path);
				if (!prefix.empty())
				{
					if (!path.starts_with(prefix))
						break;

					path.remove_prefix(prefix.length());
				}

				auto suffix = path.find('/');
				if (suffix != path.npos)
					path.remove_suffix(path.size() - suffix);

				if (path == current)
					continue;

				current = path;
				return;
			}

			current = filesystem::path();
			iter = git_index_entrycount(index);
		}

	protected:
		git_index* const index;
		string prefix;
		size_t iter = 0;
		filesystem::path current;
	};

	class GitIndexFile : public GitFile<GitIndexFile>
	{
	public:
		using GitFile<GitIndexFile>::GitFile;

		unique_ptr<AnyFileIterator> begin() const final
		{
			return isDirectory() ? make_unique<GitIndexFileIterator>(clone(), fs, relative_path == filesystem::path() ? "" : (relative_path.generic_string() + "/")) : nullptr;
		}

		git_oid blob() const
		{
			// Return entry id if found, otherwise empty oid
			auto entry = index_entry();
			return entry ? entry->id : git_oid();
		}

		unique_ptr<AnyFile> clone() const final
		{
			return make_unique<GitIndexFile>(fs, relative_path);
		}

		unique_ptr<ostream> createOutputStream(ios_base::openmode mode) final
		{
			return make_unique<GitFileStream>(fs, relative_path, mode, mode & ios_base::in ? blob() : git_oid());
		}

		bool exists() const final
		{
			return isDirectory() || index_entry();
		}

		const FileSignature* getSignature(size_t lineFrom, size_t lineTo) const final
		{
			return fs.signature;
		}

		bool isDirectory() const final
		{
			return relative_path == filesystem::path() || git_index_find_prefix(nullptr, fs, (relative_path.generic_string() + "/").c_str()) == GIT_OK;
		}

		void remove() final
		{
			// Remove directory or file from index
			if (isDirectory())
				git_throw(git_index_remove_directory(fs, relative_path.generic_string().c_str(), 0));
			else
				git_throw(git_index_remove_bypath(fs, relative_path.generic_string().c_str()));
		}

	protected:
		const git_index_entry* index_entry() const
		{
			// Convert relative path to generic string
			auto path = relative_path.generic_string();

			// Lookup current index entry
			const git_index_entry* entry = git_index_get_bypath(fs, path.c_str(), 0);

			// If current version wasn't found, check for base version (merge scenario)
			if (!entry)
				entry = git_index_get_bypath(fs, path.c_str(), 1);

			return entry;
		}
	};

	class GitTreeFileIterator : public AnyFileIterator
	{
	public:
		GitTreeFileIterator(unique_ptr<AnyFile>&& parent, git_tree* tree) : AnyFileIterator(move(parent))
		{
			assert(tree);
			git_throw(git_tree_dup(git_ptr(this->tree), tree));

			next();
		}

		unique_ptr<AnyFileIterator> clone() const final
		{
			auto clone = make_unique<GitTreeFileIterator>(parent->clone(), tree.get());
			clone->iter = iter;
			clone->current = current;
			return clone;
		}

		bool valid() const
		{
			return current;
		}

		filesystem::path filename() const final
		{
			return git_tree_entry_name(current);
		}

		void next() final
		{
			current = (iter < git_tree_entrycount(tree.get())) ? git_tree_entry_byindex(tree.get(), iter++) : nullptr;
		}

	private:
		unique_ptr<git_tree> tree;
		size_t iter = 0;
		const git_tree_entry* current = nullptr;
	};

	class GitTreeFile : public GitFile<GitTreeFile>
	{
	public:
		GitTreeFile(const GitFileSystem& fs, const filesystem::path& path, shared_ptr<git_tree_entry> tree_entry) : GitFile(fs, path), tree_entry(tree_entry)
		{
		}

		unique_ptr<AnyFileIterator> begin() const final
		{
			if (tree_entry && git_tree_entry_type(tree_entry.get()) == git_object_t::GIT_OBJECT_TREE)
			{
				unique_ptr<git_tree> tree;
				git_throw(git_tree_lookup(git_ptr(tree), static_cast<git_repository*>(fs), git_tree_entry_id(tree_entry.get())));
				return make_unique<GitTreeFileIterator>(clone(), tree.get());
			}
			else if (relative_path == filesystem::path())
			{
				return make_unique<GitTreeFileIterator>(clone(), static_cast<git_tree*>(fs));
			}

			return nullptr;
		}

		git_oid blob() const
		{
			return git_tree_entry_type(tree_entry.get()) == git_object_t::GIT_OBJECT_BLOB ? *git_tree_entry_id(tree_entry.get()) : git_oid();
		}

		unique_ptr<AnyFile> clone() const final
		{
			return make_unique<GitTreeFile>(fs, relative_path, tree_entry);
		}

		unique_ptr<ostream> createOutputStream(ios_base::openmode mode) final
		{
			throw logic_error("File system is read-only");
		}

		bool exists() const final
		{
			return !!tree_entry || isDirectory();
		}

		const FileSignature* getSignature(size_t lineFrom, size_t lineTo) const final
		{
			// Use file system signature
			if (fs.signature)
				return fs.signature;

			// Generate blame
			if (fs.first && !blame)
				blame = fs.first->blame(relative_path, blob());

			// Use blame to determine signature
			return blame ? blame->getSignature(fs, lineFrom, lineTo) : nullptr;
		}

		bool isDirectory() const final
		{
			return (tree_entry && git_tree_entry_type(tree_entry.get()) == git_object_t::GIT_OBJECT_TREE) || relative_path == filesystem::path();
		}

		void remove() final
		{
			throw logic_error("File system is read-only");
		}

	private:
		shared_ptr<git_tree_entry> tree_entry;
		mutable std::unique_ptr<GitFileBlame> blame;
	};

	GitFileSystem::GitFileSystem(git_repository* repo, const FileSignature* signature) : repo(repo), signature(signature), index(nullptr)
	{
		assert(repo);
	}

	GitFileSystem::GitFileSystem(git_index* index, const FileSignature* signature) : repo(git_index_owner(index)), signature(signature), index(index)
	{
	}

	GitFileSystem::GitFileSystem(const git_commit* commit, const FileSignature* signature) : GitFileSystem(git_commit_owner(commit), signature)
	{
		unique_ptr<git_commit> dup;
		git_throw(git_commit_dup(git_ptr(dup), const_cast<git_commit*>(commit)));
		add(move(dup));
	}

	GitFileSystem::GitFileSystem(unique_ptr<git_commit> commit, const FileSignature* signature) : GitFileSystem(git_commit_owner(commit.get()), signature)
	{
		add(move(commit));
	}

	GitFileSystem::~GitFileSystem() {}

	void GitFileSystem::add(unique_ptr<GitFileSystemSource> source)
	{
		assert(!index);

		source->merge(our_tree);
		first ? first->add(move(source)) : first.swap(source);
	}

	void GitFileSystem::add(unique_ptr<git_commit> commit)
	{
		add(make_unique<GitFileCommitSource>(move(commit)));
	}

	void GitFileSystem::add(unique_ptr<git_reference> reference)
	{
		add(make_unique<GitFileCommitSource>(move(reference)));
	}

	void GitFileSystem::add(string author, git_oid tree, unique_ptr<git_commit> ours, unique_ptr<git_commit> theirs)
	{
		add(make_unique<GitFileTreeSource>(move(author), tree, move(ours), move(theirs)));
	}

	GitFileSystem::operator git_repository* () const
	{
		assert(repo);
		return repo;
	}

	GitFileSystem::operator git_index* () const
	{
		if (!index)
			throw logic_error("File system is read-only");

		return index;
	}

	GitFileSystem::operator git_tree* () const
	{
		if (index)
			throw logic_error("File system is not read-only");

		return our_tree.get();
	}

	bool GitFileSystem::isReadOnly() const
	{
		// File system is read-only if no index has been provided via which updates can be staged
		return !index;
	}

	File GitFileSystem::open(const std::filesystem::path& path) const
	{
		if (index)
		{
			return File(make_unique<GitIndexFile>(*this, path));
		}
		else
		{
			unique_ptr<git_tree_entry> entry;
			if (auto tree = static_cast<git_tree*>(*this))
				if (auto error = git_tree_entry_bypath(git_ptr(entry), tree, path.generic_string().c_str()); error != GIT_ENOTFOUND)
					git_throw(error);

			return File(make_unique<GitTreeFile>(*this, path, move(entry)));
		}
	}

	void GitFileSystem::flush() const
	{
		if (!index)
			throw logic_error("File system is read-only");
		
		// Save changes to disk
		if (git_index_path(index))
			git_throw(git_index_write(index));
	}
}