#pragma once
#include <jadegit/git2.h>

namespace JadeGit
{
	class GitFileSystem;
	class FileSignature;

	class GitFileBlame
	{
	public:
		GitFileBlame(std::unique_ptr<git_blame> blame);
		GitFileBlame(const FileSignature& signature);

		const FileSignature* getSignature(const GitFileSystem& fs, size_t lineFrom, size_t lineTo) const;

	private:
		std::unique_ptr<git_blame> blame;
		const FileSignature* signature = nullptr;
	};
}