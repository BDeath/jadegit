#pragma once
#include <stdexcept>
#include <tinyxml2.h>

namespace JadeGit
{
	class xml_exception : public std::runtime_error
	{
	public:
		xml_exception(tinyxml2::XMLError error);
		xml_exception(tinyxml2::XMLDocument& doc, tinyxml2::XMLError error);
	};
}

/* Error Handling Macro */
#define XML_THROW(_doc, _expr)	if (tinyxml2::XMLError _error = _expr){ _doc.PrintError(); throw xml_exception(_doc, _error); }