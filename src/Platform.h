#pragma once
#include <jadegit/build.h>
#include <jadegit/Version.h>

namespace JadeGit
{
	constexpr bool use_jade = static_cast<bool>(USE_JADE);
	constexpr bool use_git2 = static_cast<bool>(USE_GIT2);
	constexpr bool use_schema = static_cast<bool>(WITH_SCHEMA);

	constexpr AnnotatedVersion jade2020("JADE 2020", 20, 0, 1);
	constexpr AnnotatedVersion jade2020SP1("JADE 2020 SP1", 20, 0, 2);
	constexpr AnnotatedVersion jade2022("JADE 2022", 22, 0, 1);
	constexpr AnnotatedVersion jade2022SP1("JADE 2022 SP1", 22, 0, 2);
	constexpr AnnotatedVersion jade2022SP2("JADE 2022 SP2", 22, 0, 3);

	constexpr AnnotatedVersion jedi102("JEDI #102");
	constexpr AnnotatedVersion jedi402("JEDI #402");
	constexpr AnnotatedVersion jedi457("JEDI #457");

	constexpr AnnotatedVersion par66557("PAR #66557", 22);
	constexpr AnnotatedVersion par68417("PAR #68417");
	constexpr AnnotatedVersion par68418("PAR #68418", 22);
	constexpr AnnotatedVersion par70018("PAR #70018");
	constexpr AnnotatedVersion par70019("PAR #70019");
}

#if USE_JADE
#include <jombuild.h>

namespace JadeGit
{
#if JADE_BUILD_VERSION_BUILD
	constexpr Version jadeVersion(JADE_BUILD_VERSION_MAJOR, JADE_BUILD_VERSION_MINOR, JADE_BUILD_VERSION_BUILD);
#elif JADE_BUILD_VERSION_MINOR
	constexpr Version jadeVersion(JADE_BUILD_VERSION_MAJOR, JADE_BUILD_VERSION_MINOR);
#else
	constexpr Version jadeVersion(JADE_BUILD_VERSION_MAJOR);
#endif
}
#endif