#include <jadegit/data/Routine.h>
#include <jadegit/data/Library.h>
#include <jadegit/data/RootSchema/LockPolicyMeta.h>
#include <jadegit/data/RootSchema/PathExpressionMeta.h>
#include <jadegit/data/ScriptElement.h>
#include "ObjectRegistration.h"

namespace JadeGit::Data
{
	DEFINE_OBJECT_CAST(Routine)

	static ObjectRegistration<Routine, Object> registrar("Routine");

	std::map<Routine::ExecutionLocation, const char*> EnumStrings<Routine::ExecutionLocation>::data =
	{
		{ Routine::ExecutionLocation::Any, "any" },
		{ Routine::ExecutionLocation::Server, "server" },
		{ Routine::ExecutionLocation::Client, "client" },
		{ Routine::ExecutionLocation::ApplicationServer, "appServer" },
		{ Routine::ExecutionLocation::PresentationClient, "presentationClient" }
	};

	template Value<Routine::ExecutionLocation>;
	template ObjectValue<Library*, &RoutineMeta::library>;

	Routine::Routine(Type* parent, const Class* dataClass, const char* name) : Script(parent, dataClass, name)
	{
	};

	Routine::Routine(Object* parent, const Class* dataClass, const char* name) : Script(parent, dataClass, name)
	{
	};

	ScriptMeta::ScriptMeta(RootSchema& parent, const FeatureMeta& superclass) : RootClass(parent, "Script", superclass),
		compiledOK(NewBoolean("compiledOK")),
		compilerVersion(NewInteger("compilerVersion")),
		errorCode(NewInteger("errorCode")),
		errorLength(NewInteger("errorLength")),
		errorPosition(NewInteger("errorPosition")),
		notImplemented(NewBoolean("notImplemented")),
		source(NewString("source")),
		status(NewInteger("status")),
		warningCount(NewInteger("warningCount"))
	{
		notImplemented->bind(&Script::notImplemented);
		source->bind(&Script::source);
	}

	RoutineMeta::RoutineMeta(RootSchema& parent, const ScriptMeta& superclass) : RootClass(parent, "Routine", superclass),
		entrypoint(NewString("entrypoint", 256)),
		executionLocation(NewCharacter("executionLocation")),
		library(NewReference<ExplicitInverseRef>("library", NewType<Class>("Library"))),
		options(NewInteger("options"))
	{
		entrypoint->bind(&Routine::entrypoint);
		executionLocation->SetDefault(Value(Routine::ExecutionLocation::Any))->bind(&Routine::executionLocation);
		library->manual().bind(&Routine::library);
		options->bind(&Routine::options);
	}

	LockPolicyMeta::LockPolicyMeta(RootSchema& parent, const RoutineMeta& superclass) : RootClass(parent, "LockPolicy", superclass) {}

	PathExpressionMeta::PathExpressionMeta(RootSchema& parent, const RoutineMeta& superclass) : RootClass(parent, "PathExpression", superclass) {}
}