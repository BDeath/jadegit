#include <jadegit/data/ExternalClassMap.h>
#include <jadegit/data/ExternalCollClassMap.h>
#include <jadegit/data/ExternalAttributeMap.h>
#include <jadegit/data/ExternalReferenceMap.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ExternalClassMapMeta.h>
#include <jadegit/data/RootSchema/ExternalCollClassMapMeta.h>
#include <jadegit/data/RootSchema/ExternalAttributeMapMeta.h>
#include <jadegit/data/RootSchema/ExternalReferenceMapMeta.h>
#include "EntityRegistration.h"

using namespace std;

namespace JadeGit::Data
{
	DEFINE_OBJECT_CAST(ExternalSchemaMap);
	DEFINE_OBJECT_CAST(ExternalAttributeMap);
	DEFINE_OBJECT_CAST(ExternalReferenceMap);

	static EntityRegistration<ExternalClassMap, ExternalDatabase> externalClassMap("ExternalClassMap", "external class map", &ExternalDatabase::classMaps);
	static EntityRegistration<ExternalCollClassMap, ExternalDatabase> externalCollClassMap("ExternalCollClassMap", "external collection class map", &ExternalDatabase::collClassMaps);
	static EntityRegistration<ExternalAttributeMap, ExternalClassMap> externalAttributeMap("ExternalAttributeMap", "external attribute map", &ExternalClassMap::attributeMaps);
	static EntityRegistration<ExternalReferenceMap, ExternalClassMap> externalReferenceMap("ExternalReferenceMap", "external reference map", &ExternalClassMap::referenceMaps);

	extern template ObjectValue<SchemaEntity*, &ExternalSchemaMapMeta::schemaEntity>;

	void ExternalSchemaMap::Accept(EntityVisitor& v)
	{
		// Not currently needed/defined by any subclass
	}

	const SchemaEntity& ExternalSchemaMap::getOriginal() const
	{
		return *schemaEntity;
	}

	ExternalClassMap::ExternalClassMap(ExternalDatabase& parent, const Class* dataClass, const char* name) : ExternalSchemaMap(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::externalClassMap), name),
		database(&parent)
	{
	}

	void ExternalClassMap::loaded(bool strict, queue<future<void>>& tasks)
	{
		ExternalSchemaMap::loaded(strict, tasks);

		// Resolve related class
		schemaEntity = &Entity::resolve<ExternalClass&>(*this, name);
	}

	ExternalCollClassMap::ExternalCollClassMap(ExternalDatabase& parent, const Class* dataClass, const char* name) : ExternalSchemaMap(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::externalCollClassMap), name),
		database(&parent)
	{
	}

	void ExternalCollClassMap::loaded(bool strict, queue<future<void>>& tasks)
	{
		ExternalSchemaMap::loaded(strict, tasks);

		// Resolve related collection class
		schemaEntity = &Entity::resolve<ExternalCollClass&>(*this, name);
	}

	void ExternalPropertyMap::loaded(bool strict, queue<future<void>>& tasks)
	{
		ExternalSchemaMap::loaded(strict, tasks);

		// Resolve related property
		tasks.push(async(launch::deferred, [this]() { schemaEntity = &Entity::resolve<Property&>(static_cast<ExternalClassMap*>(parent)->schemaEntity, name); }));
	}

	ExternalAttributeMap::ExternalAttributeMap(ExternalClassMap& parent, const Class* dataClass, const char* name) : ExternalPropertyMap(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::externalAttributeMap), name),
		classMap(&parent)
	{
	}

	ExternalReferenceMap::ExternalReferenceMap(ExternalClassMap& parent, const Class* dataClass, const char* name) : ExternalPropertyMap(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::externalReferenceMap), name),
		classMap(&parent),
		id(parent.database->getNextReferenceMapInstanceId())
	{
	}

	ExternalSchemaMapMeta::ExternalSchemaMapMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "ExternalSchemaMap", superclass),
		schemaEntity(NewReference<ExplicitInverseRef>("schemaEntity", NewType<Class>("SchemaEntity")))
	{
		schemaEntity->manual().unwritten().bind(&ExternalSchemaMap::schemaEntity);
	}

	ExternalClassMapMeta::ExternalClassMapMeta(RootSchema& parent, const ExternalSchemaMapMeta& superclass) : RootClass(parent, "ExternalClassMap", superclass),
		attributeMaps(NewReference<ExplicitInverseRef>("attributeMaps", NewType<CollClass>("ExternalAttributeMapSet"))),
		database(NewReference<ExplicitInverseRef>("database", NewType<Class>("ExternalDatabase"))),
		referenceMaps(NewReference<ExplicitInverseRef>("referenceMaps", NewType<CollClass>("ExternalReferenceMapSet"))),
		tables(NewReference<ExplicitInverseRef>("tables", NewType<CollClass>("ExternalTableSet")))
	{
		attributeMaps->automatic().parent().bind(&ExternalClassMap::attributeMaps);
		database->manual().child().bind(&ExternalClassMap::database);
		referenceMaps->automatic().parent().bind(&ExternalClassMap::referenceMaps);
		tables->manual().bind(&ExternalClassMap::tables);
	}

	ExternalCollClassMapMeta::ExternalCollClassMapMeta(RootSchema& parent, const ExternalSchemaMapMeta& superclass) : RootClass(parent, "ExternalCollClassMap", superclass),
		database(NewReference<ExplicitInverseRef>("database", NewType<Class>("ExternalDatabase")))
	{
		database->manual().child().bind(&ExternalCollClassMap::database);
	}

	ExternalPropertyMapMeta::ExternalPropertyMapMeta(RootSchema& parent, const ExternalSchemaMapMeta& superclass) : RootClass(parent, "ExternalPropertyMap", superclass) {}

	ExternalAttributeMapMeta::ExternalAttributeMapMeta(RootSchema& parent, const ExternalPropertyMapMeta& superclass) : RootClass(parent, "ExternalAttributeMap", superclass),
		classMap(NewReference<ExplicitInverseRef>("classMap", NewType<Class>("ExternalClassMap"))),
		column(NewReference<ExplicitInverseRef>("column", NewType<Class>("ExternalColumn")))
	{
		classMap->manual().child().bind(&ExternalAttributeMap::classMap);
		column->manual().bind(&ExternalAttributeMap::column);
	}

	ExternalReferenceMapMeta::ExternalReferenceMapMeta(RootSchema& parent, const ExternalPropertyMapMeta& superclass) : RootClass(parent, "ExternalReferenceMap", superclass),
		classMap(NewReference<ExplicitInverseRef>("classMap", NewType<Class>("ExternalClassMap"))),
		dirnLeftRight(NewBoolean("dirnLeftRight")),
		leftColumns(NewReference<ExplicitInverseRef>("leftColumns", NewType<CollClass>("ExternalColumnArray"))),
		otherRefMap(NewReference<ImplicitInverseRef>("otherRefMap", NewType<Class>("ExternalReferenceMap"))),
		relType(NewInteger("relType")),
		rightColumns(NewReference<ExplicitInverseRef>("rightColumns", NewType<CollClass>("ExternalColumnArray"))),
		wherePredicate(NewString("wherePredicate"))
	{
		classMap->manual().child().bind(&ExternalReferenceMap::classMap);
		leftColumns->manual().bind(&ExternalReferenceMap::leftColumns);
		otherRefMap->bind(&ExternalReferenceMap::otherRefMap);
		rightColumns->manual().bind(&ExternalReferenceMap::rightColumns);
	}
}