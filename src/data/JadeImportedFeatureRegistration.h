#pragma once
#include <jadegit/data/EntityFactory.h>
#include <jadegit/data/EntityDict.h>
#include <jadegit/data/Class.h>
#include "ObjectRegistration.h"
#include <type_traits>

namespace JadeGit::Data
{
	template<class TDerived>
	class JadeImportedFeatureRegistration : protected ObjectRegistration<TDerived, Type, EntityFactory::Registration>
	{
		static_assert(std::is_base_of<JadeImportedFeature, TDerived>(), "Derived component is not an imported feature");

	public:
		using classGetter = std::function<TDerived* (JadeImportedClass*, const std::string&)>;
		using interfaceGetter = std::function<TDerived* (JadeImportedInterface*, const std::string&)>;

		JadeImportedFeatureRegistration(const char* key, std::string alias, classGetter classGet, interfaceGetter interfaceGet) : ObjectRegistration<TDerived, Type, EntityFactory::Registration>(key),
			alias(std::move(alias)),
			classGet(std::move(classGet)), 
			interfaceGet(std::move(interfaceGet))
		{
			EntityFactory::Get().Register<TDerived>(key, this);
		}

		template<typename TClassCollection, typename TInterfaceCollection>
		JadeImportedFeatureRegistration(const char* key, std::string alias, TClassCollection JadeImportedClass::* classColl, TInterfaceCollection JadeImportedInterface::* interfaceColl) : JadeImportedFeatureRegistration(key,
			std::move(alias),
			[classColl](JadeImportedClass* parent, const std::string& name) {
				return (parent->*classColl).Get<TDerived>(name);
			},
			[interfaceColl](JadeImportedInterface* parent, const std::string& name) {
				return (parent->*interfaceColl).Get<TDerived>(name);
			}) {}

	protected:
		const std::string alias;

		TDerived* Create(Component* parent, const Class* dataClass, const char* name) const override
		{
			if (JadeImportedClass* cls = dynamic_cast<JadeImportedClass*>(parent))
				return new TDerived(cls, dataClass, name);

			if (JadeImportedInterface* interface = dynamic_cast<JadeImportedInterface*>(parent))
				return new TDerived(interface, dataClass, name);

			throw std::runtime_error(std::format("Failed to resolve parent for new {}", alias));
		}

		using ObjectRegistration<TDerived, Type, EntityFactory::Registration>::Resolve;

		TDerived* Resolve(const Component* origin, const QualifiedName* name, bool expected, bool shallow, bool inherit) const override
		{
			// Use basic resolution when no name has been specified
			if (!name)
				return Resolve(origin, expected);

			// Resolve class parent
			if(JadeImportedClass* cls = EntityFactory::Get().Resolve<JadeImportedClass>(origin, name->parent.get(), false, is_major_entity<TDerived>, inherit))
			{
				if (TDerived* result = Load(cls, name->name, shallow, classGet))
					return result;
			}
			// Resolve interface parent
			else if (JadeImportedInterface* interface = EntityFactory::Get().Resolve<JadeImportedInterface>(origin, name->parent.get(), false, is_major_entity<TDerived>, inherit))
			{
				if (TDerived* result = Load(interface, name->name, shallow, interfaceGet))
					return result;
			}

			if (expected)
				throw std::runtime_error(std::format("Failed to resolve {} [{}]", alias, static_cast<std::string>(*name)));

			return nullptr;
		}

		std::function<Object* ()> resolver(const Component* origin, const tinyxml2::XMLElement* source, bool expected, bool shallow, bool inherit) const override
		{
			// Use basic resolution when qualified name hasn't been specified
			auto name = source->Attribute("name");
			if (!name)
				return [=]() { return Resolve(origin, expected); };

			// Resolve using qualified name (above)
			std::string path(name);
			return [=]()
			{
				QualifiedName qualifiedName(path);
				return Resolve(origin, &qualifiedName, expected, shallow, inherit);
			};
		}

		TDerived* load(Component* origin, const Class* dataClass, const FileElement& source) const override
		{
			// Get qualified name attribute
			auto path = source.header("name");
			if (!path) throw std::runtime_error(std::format("Missing {} name attribute", alias));

			// Convert to qualified name
			QualifiedName name(path);

			// Resolve class parent
			if (JadeImportedClass* cls = EntityFactory::Get().Resolve<JadeImportedClass>(origin, name.parent.get(), false, true, false))
			{
				// Find existing
				if (TDerived* result = classGet(cls, name.name))
					return result;

				// Create new
				return new TDerived(cls, dataClass, name.name.c_str());
			}
			// Resolve interface parent
			else if (JadeImportedInterface* interface = EntityFactory::Get().Resolve<JadeImportedInterface>(origin, name.parent.get(), false, true, false))
			{
				// Find existing
				if (TDerived* result = interfaceGet(interface, name.name))
					return result;

				// Create new
				return new TDerived(interface, dataClass, name.name.c_str());
			}
			else
				throw std::runtime_error(std::format("Failed to resolve parent entity for {} [{}]", alias, path));
		}

	private:
		classGetter classGet;
		interfaceGetter interfaceGet;

		TDerived* load(Assembly* assembly, const Class* dataClass, const std::filesystem::path& path) const override
		{
			throw std::logic_error(std::format("{} entities do not have a file path", this->key));
		}

		TDerived* Resolve(const Assembly* assembly, const std::filesystem::path& path, bool expected, bool shallow) const override
		{
			throw std::logic_error(std::format("{} entities do not have a file path", this->key));
		}

		template <class TParent, typename TGetter>
		TDerived* Load(TParent* parent, const std::string& name, bool shallow, const TGetter& get) const
		{
			if (name.empty())
				throw std::invalid_argument(std::format("Missing {} name", alias));

			// Load parent if required (may be in a shallow state)
			parent->Load();

			// Find & return embedded entity
			return get(parent, name);
		}
	};
}