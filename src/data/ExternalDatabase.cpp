#include <jadegit/data/ExternalDatabase.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ExternalDatabaseMeta.h>
#include <jadegit/data/RootSchema/ExternalDbDriverInfoMeta.h>
#include <jadegit/data/RootSchema/ExternalDbProfileMeta.h>
#include <jadegit/data/RootSchema/ExternalColumnMeta.h>
#include <jadegit/data/RootSchema/ExternalForeignKeyMeta.h>
#include <jadegit/data/RootSchema/ExternalIndexMeta.h>
#include <jadegit/data/RootSchema/ExternalIndexKeyMeta.h>
#include <jadegit/data/RootSchema/ExternalParameterMeta.h>
#include <jadegit/data/RootSchema/ExternalPrimaryKeyMeta.h>
#include <jadegit/data/RootSchema/ExternalStoredProcedureMeta.h>
#include <jadegit/data/RootSchema/ExternalTableMeta.h>
#include <jadegit/data/RootSchema/ExternalParameterMapMeta.h>
#include <jadegit/data/RootSchema/ExternalReturnTypeMapMeta.h>
#include "SchemaEntityRegistration.h"
#include "TypeRegistration.h"

// TODO: Remove dependencies for metadata
#include <jadegit/data/CollClass.h>
#include <jadegit/data/Reference.h>

using namespace std;

namespace JadeGit::Data
{
	const std::filesystem::path ExternalDatabase::subFolder("external-databases");

	static SchemaEntityRegistration<ExternalDatabase> externalDatabase("ExternalDatabase", &Schema::externalDatabases);
	static ObjectRegistration<ExternalDbDriverInfo, ExternalDatabase> externalDbDriverInfo("ExternalDbDriverInfo");
	static ObjectRegistration<ExternalDbProfile, ExternalDatabase> externalDbProfile("ExternalDbProfile");
	static EntityRegistration<ExternalTable, ExternalDatabase> externalTable("ExternalTable", &ExternalDatabase::tables);
	static EntityRegistration<ExternalColumn, ExternalTable> externalColumn("ExternalColumn", &ExternalTable::columns);
	static EntityRegistration<ExternalForeignKey, ExternalTable> externalForeignKey("ExternalForeignKey", &ExternalTable::foreignKeys);
	static EntityRegistration<ExternalIndex, ExternalTable> externalIndex("ExternalIndex", &ExternalTable::indexes);
	static ObjectRegistration<ExternalIndexKey, ExternalIndex> externalIndexKey("ExternalIndexKey");

	template ObjectValue<Schema* const, &ExternalDatabaseMeta::_schema>;
	template EntityDict<ExternalTable, &ExternalDatabaseMeta::_tables>;

	ExternalDatabase::ExternalDatabase(Schema* parent, const Class* dataClass, const char* name) : MajorEntity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::externalDatabase), name),
		schema(parent)
	{
	}

	void ExternalDatabase::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	unsigned short ExternalDatabase::getNextReferenceMapInstanceId()
	{
		if (referenceMaps == USHRT_MAX)
			throw std::overflow_error("Cannot allocate new reference map id");

		return ++referenceMaps;
	}

	ExternalDbDriverInfo::ExternalDbDriverInfo(ExternalDatabase& parent, const Class* dataClass) : Object(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::externalDbDriverInfo)),
		database(&parent)
	{
	}

	ExternalDbProfile::ExternalDbProfile(ExternalDatabase& parent, const Class* dataClass) : Object(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::externalDbProfile)),
		database(&parent)
	{
	}

	void ExternalSchemaEntity::Accept(EntityVisitor& v)
	{
		// Not currently needed/defined by any subclass
	}

	ExternalColumn::ExternalColumn(ExternalTable& parent, const Class* dataClass, const char* name) : ExternalSchemaEntity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::externalColumn), name),
		table(&parent)
	{
	}

	ExternalForeignKey::ExternalForeignKey(ExternalTable& parent, const Class* dataClass, const char* name) : ExternalSchemaEntity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::externalForeignKey), name),
		table(&parent)
	{
	}

	ExternalIndex::ExternalIndex(ExternalTable& parent, const Class* dataClass, const char* name) : ExternalSchemaEntity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::externalIndex), name),
		table(&parent)
	{
	}

	ExternalIndexKey::ExternalIndexKey(ExternalIndex& parent, const Class* dataClass) : ExternalSchemaEntity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::externalIndexKey), nullptr),
		index(&parent)
	{
	}

	const ExternalColumn& ExternalIndexKey::getOriginal() const
	{
		if (!column)
			throw runtime_error("Missing external key column");

		return *column;
	}

	ExternalTable::ExternalTable(ExternalDatabase& parent, const Class* dataClass, const char* name) : ExternalSchemaEntity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::externalTable), name),
		database(&parent)
	{
	}

	ExternalDatabaseMeta::ExternalDatabaseMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "ExternalDatabase", superclass),
		_classMaps(NewReference<ExplicitInverseRef>("_classMaps", NewType<CollClass>("ExternalClassMapSet"))),
		_classes(NewReference<ExplicitInverseRef>("_classes", NewType<CollClass>("ClassSet"))),
		_collClassMaps(NewReference<ExplicitInverseRef>("_collClassMaps", NewType<CollClass>("ExternalCollClassMapSet"))),
		_collClasses(NewReference<ExplicitInverseRef>("_collClasses", NewType<CollClass>("CollClassSet"))),
		_dataSource(NewString("_dataSource", 129)),
		_driverInfo(NewReference<ExplicitInverseRef>("_driverInfo", NewType<Class>("ExternalDbDriverInfo"))),
		_profile(NewReference<ExplicitInverseRef>("_profile", NewType<Class>("ExternalDbProfile"))),
		_schema(NewReference<ExplicitInverseRef>("_schema", NewType<Class>("Schema"))),
		_state(NewInteger("_state")),
		_tables(NewReference<ExplicitInverseRef>("_tables", NewType<CollClass>("ExternalTableByNameDict"))),
		_version(NewInteger("_version")),
		connectionString(NewString("connectionString")),
		name(NewString("name", 101)),
		password(NewString("password", 129)),
		serverName(NewString("serverName", 129)),
		userName(NewString("userName", 129)),
		uuid(NewBinary("uuid", 16))
	{
		_classMaps->automatic().parent().bind(&ExternalDatabase::classMaps);
		_classes->automatic().bind(&ExternalDatabase::classes);
		_collClassMaps->automatic().parent().bind(&ExternalDatabase::collClassMaps);
		_collClasses->automatic().bind(&ExternalDatabase::collClasses);
		_driverInfo->automatic().parent().bind(&ExternalDatabase::driverInfo);
		_profile->automatic().parent().bind(&ExternalDatabase::profile);
		_schema->manual().child().bind(&ExternalDatabase::schema);
		_tables->automatic().parent().bind(&ExternalDatabase::tables);
	}

	ExternalDbDriverInfoMeta::ExternalDbDriverInfoMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "ExternalDbDriverInfo", superclass),
		catalogLocation(NewInteger("catalogLocation")),
		catalogName(NewBoolean("catalogName")),
		catalogNameSeparator(NewString("catalogNameSeparator", 6)),
		catalogUsage(NewInteger("catalogUsage")),
		columnAlias(NewBoolean("columnAlias")),
		correlationName(NewInteger("correlationName")),
		database(NewReference<ExplicitInverseRef>("database", NewType<Class>("ExternalDatabase"))),
		driverODBCVersion(NewString("driverODBCVersion", 31)),
		driverVersion(NewString("driverVersion", 31)),
		identifierCase(NewInteger("identifierCase")),
		identifierQuoteChar(NewCharacter("identifierQuoteChar")),
		maxCatalogNameLength(NewInteger("maxCatalogNameLength")),
		maxColumnNameLength(NewInteger("maxColumnNameLength")),
		maxColumnsInOrderBy(NewInteger("maxColumnsInOrderBy")),
		maxColumnsInSelect(NewInteger("maxColumnsInSelect")),
		maxIdentifierLength(NewInteger("maxIdentifierLength")),
		maxSchemaNameLength(NewInteger("maxSchemaNameLength")),
		maxStatementLength(NewInteger("maxStatementLength")),
		maxTableNameLength(NewInteger("maxTableNameLength")),
		maxTablesInSelect(NewInteger("maxTablesInSelect")),
		orderByColumnsInSelect(NewBoolean("orderByColumnsInSelect")),
		quotedIdentifierCase(NewInteger("quotedIdentifierCase")),
		schemaUsage(NewInteger("schemaUsage"))
	{
		database->manual().child().bind(&ExternalDbDriverInfo::database);
	}

	ExternalDbProfileMeta::ExternalDbProfileMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "ExternalDbProfile", superclass),
		clsQueryClass(NewString("clsQueryClass", 101)),
		collQueryClass(NewString("collQueryClass", 101)),
		database(NewReference<ExplicitInverseRef>("database", NewType<Class>("ExternalDatabase"))),
		defineAttrClass(NewString("defineAttrClass", 101)),
		defineRefFromClass(NewString("defineRefFromClass", 101)),
		defineRefToClass(NewString("defineRefToClass", 101)),
		includeKeys(NewBoolean("includeKeys")),
		offline(NewBoolean("offline")),
		prefixAttr(NewString("prefixAttr", 101)),
		prefixClass(NewString("prefixClass", 101)),
		prefixKeys(NewString("prefixKeys", 101)),
		prefixRef(NewString("prefixRef", 101)),
		refQueryClass(NewString("refQueryClass", 101)),
		replaceUScore(NewBoolean("replaceUScore")),
		savePswd(NewBoolean("savePswd")),
		sheet(NewString("sheet", 101)),
		suffixArray(NewString("suffixArray", 101)),
		suffixDict(NewString("suffixDict", 101)),
		suffixSet(NewString("suffixSet", 101)),
		toMixedCase(NewBoolean("toMixedCase")),
		toSingular(NewBoolean("toSingular")),
		useUnderscore(NewBoolean("useUnderscore")) 
	{
		database->manual().child().bind(&ExternalDbProfile::database);
	}

	ExternalSchemaEntityMeta::ExternalSchemaEntityMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "ExternalSchemaEntity", superclass),
		name(NewString("name", 129)),
		remarks(NewString("remarks")),
		state(NewInteger("state"))
	{
		name->unwritten().bind(&ExternalSchemaEntity::name);
	}

	ExternalColumnMeta::ExternalColumnMeta(RootSchema& parent, const ExternalSchemaEntityMeta& superclass) : RootClass(parent, "ExternalColumn", superclass),
		attributeMaps(NewReference<ExplicitInverseRef>("attributeMaps", NewType<CollClass>("ExternalAttributeMapSet"))),
		bufferLength(NewInteger("bufferLength")),
		columnSize(NewInteger("columnSize")),
		dataType(NewInteger("dataType")),
		decimalDigits(NewInteger("decimalDigits")),
		kind(NewInteger("kind")),
		leftReferenceMaps(NewReference<ExplicitInverseRef>("leftReferenceMaps", NewType<CollClass>("ExternalReferenceMapSet"))),
		length(NewInteger("length")),
		nullability(NewInteger("nullability")),
		ordinalPosition(NewInteger("ordinalPosition")),
		rightReferenceMaps(NewReference<ExplicitInverseRef>("rightReferenceMaps", NewType<CollClass>("ExternalReferenceMapSet"))),
		scope(NewInteger("scope")),
		table(NewReference<ExplicitInverseRef>("table", NewType<Class>("ExternalTable")))
	{
		attributeMaps->automatic();
		leftReferenceMaps->automatic();
		rightReferenceMaps->automatic();
		table->manual().child().bind(&ExternalColumn::table);
	}

	ExternalForeignKeyMeta::ExternalForeignKeyMeta(RootSchema& parent, const ExternalSchemaEntityMeta& superclass) : RootClass(parent, "ExternalForeignKey", superclass),
		columns(NewReference<ImplicitInverseRef>("columns", NewType<CollClass>("ExternalColumnArray"))),
		deleteRule(NewInteger("deleteRule")),
		table(NewReference<ExplicitInverseRef>("table", NewType<Class>("ExternalTable"))),
		updateRule(NewInteger("updateRule"))
	{
		table->manual().child().bind(&ExternalForeignKey::table);
	}

	ExternalIndexMeta::ExternalIndexMeta(RootSchema& parent, const ExternalSchemaEntityMeta& superclass) : RootClass(parent, "ExternalIndex", superclass),
		cardinality(NewInteger("cardinality")),
		duplicatesAllowed(NewBoolean("duplicatesAllowed")),
		filterCondition(NewString("filterCondition")),
		keys(NewReference<ExplicitInverseRef>("keys", NewType<CollClass>("ExternalIndexKeyArray"))),
		table(NewReference<ExplicitInverseRef>("table", NewType<Class>("ExternalTable")))
	{
		keys->automatic().parent().bind(&ExternalIndex::keys);
		table->manual().child().bind(&ExternalIndex::table);
	}

	ExternalIndexKeyMeta::ExternalIndexKeyMeta(RootSchema& parent, const ExternalSchemaEntityMeta& superclass) : RootClass(parent, "ExternalIndexKey", superclass),
		column(NewReference<ImplicitInverseRef>("column", NewType<Class>("ExternalColumn"))),
		descending(NewBoolean("descending")),
		index(NewReference<ExplicitInverseRef>("index", NewType<Class>("ExternalIndex"))),
		keySequence(NewInteger("keySequence"))
	{
		column->bind(&ExternalIndexKey::column);
		index->manual().child().bind(&ExternalIndexKey::index);
	}

	ExternalParameterMeta::ExternalParameterMeta(RootSchema& parent, const ExternalSchemaEntityMeta& superclass) : RootClass(parent, "ExternalParameter", superclass),
		bufferLength(NewInteger("bufferLength")),
		columnSize(NewInteger("columnSize")),
		dataType(NewInteger("dataType")),
		decimalDigits(NewInteger("decimalDigits")),
		length(NewInteger("length")),
		nullability(NewInteger("nullability")),
		ordinalPosition(NewInteger("ordinalPosition")),
		type(NewInteger("type")) {}

	ExternalPrimaryKeyMeta::ExternalPrimaryKeyMeta(RootSchema& parent, const ExternalSchemaEntityMeta& superclass) : RootClass(parent, "ExternalPrimaryKey", superclass),
		columns(NewReference<ImplicitInverseRef>("columns", NewType<CollClass>("ExternalColumnArray"))) {}

	ExternalStoredProcedureMeta::ExternalStoredProcedureMeta(RootSchema& parent, const ExternalSchemaEntityMeta& superclass) : RootClass(parent, "ExternalStoredProcedure", superclass),
		catalogName(NewString("catalogName", 129)),
		database(NewReference<ExplicitInverseRef>("database", NewType<Class>("ExternalDatabase"))),
		excluded(NewBoolean("excluded")),
		schemaName(NewString("schemaName", 129)),
		type(NewInteger("type")) {}

	ExternalTableMeta::ExternalTableMeta(RootSchema& parent, const ExternalSchemaEntityMeta& superclass) : RootClass(parent, "ExternalTable", superclass),
		aliasName(NewString("aliasName", 129)),
		cardinality(NewInteger("cardinality")),
		catalogName(NewString("catalogName", 129)),
		classMaps(NewReference<ExplicitInverseRef>("classMaps", NewType<CollClass>("ExternalClassMapSet"))),
		columns(NewReference<ExplicitInverseRef>("columns", NewType<CollClass>("ExternalColumnByNameDict"))),
		database(NewReference<ExplicitInverseRef>("database", NewType<Class>("ExternalDatabase"))),
		excluded(NewBoolean("excluded")),
		foreignKeys(NewReference<ExplicitInverseRef>("foreignKeys", NewType<CollClass>("ExternalForeignKeyByNameDict"))),
		indexes(NewReference<ExplicitInverseRef>("indexes", NewType<CollClass>("ExternalIndexByNameDict"))),
		primaryKey(NewReference<ExplicitInverseRef>("primaryKey", NewType<Class>("ExternalPrimaryKey"))),
		schemaName(NewString("schemaName", 129)),
		specialColumns(NewReference<ImplicitInverseRef>("specialColumns", NewType<CollClass>("ExternalColumnArray"))),
		tableType(NewInteger("tableType")),
		uuid(NewBinary("uuid", 16))
	{
		classMaps->automatic();
		columns->automatic().parent().bind(&ExternalTable::columns);
		database->manual().child().bind(&ExternalTable::database);
		foreignKeys->automatic().parent().bind(&ExternalTable::foreignKeys);
		indexes->automatic().parent().bind(&ExternalTable::indexes);
		primaryKey->automatic();
		specialColumns->bind(&ExternalTable::specialColumns);
	}

	ExternalScriptElementMapMeta::ExternalScriptElementMapMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "ExternalScriptElementMap", superclass) {}

	ExternalParameterMapMeta::ExternalParameterMapMeta(RootSchema& parent, const ExternalScriptElementMapMeta& superclass) : RootClass(parent, "ExternalParameterMap", superclass) {}

	ExternalReturnTypeMapMeta::ExternalReturnTypeMapMeta(RootSchema& parent, const ExternalScriptElementMapMeta& superclass) : RootClass(parent, "ExternalReturnTypeMap", superclass) {}
}