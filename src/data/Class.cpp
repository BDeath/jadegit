#include <jadegit/data/Class.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/ActiveXClass.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/ExternalClass.h>
#include <jadegit/data/Development.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ExceptionClassMeta.h>
#include <jadegit/data/RootSchema/ExternalClassMeta.h>
#include <jadegit/data/RootSchema/ActiveXGuiClassMeta.h>
#include <jadegit/data/RootSchema/JadeUserClassMeta.h>
#include "TypeRegistration.h"

using namespace std;

namespace JadeGit::Data
{
	static TypeRegistration<Class, JadeImportedPackage, Schema> registrar("Class", &JadeImportedPackage::classes, &Schema::classes);
	static TypeRegistration<ExceptionClass, Schema> exceptionClass("ExceptionClass", &Schema::classes);
	static TypeRegistration<ExternalClass, Schema> externalClass("ExternalClass", &Schema::classes);
	static TypeRegistration<GUIClass, Schema> guiClass("GUIClass", &Schema::classes);
	
	map<ObjectVolatility, const char*> EnumStrings<ObjectVolatility>::data =
	{
		{ Volatile, "volatile" },
		{ Frozen, "frozen" },
		{ Stable, "stable" }
	};

	template <>
	void ObjectValue<Class*, &ClassMeta::superclass>::inverseAdd(Object& target) const
	{
		if (!offset)
			offset = member_offset(&Class::superclass);

		static_cast<ObjectReference&>(static_cast<Class&>(target).subclasses).autoAdd(getObject());
	}

	template ObjectValue<ActiveXClass*, &ClassMeta::activeXClass>;
	template ObjectValue<Array<DbClassMap*>, &ClassMeta::classMapRefs>;
	template ObjectValue<Array<JadeInterface*>, &ClassMeta::implementedInterfaces>;
	template Value<ObjectVolatility>;
	template EntityDict<Property, &ClassMeta::properties>;
	template ObjectValue<Array<Class*>, &ClassMeta::subclasses>;
	template ObjectValue<Class*, &ClassMeta::superclass>;

	Class::Class(Schema* parent, const Class* dataClass, const char* name, Class* super) : Type(parent, nullptr, name),
		superclass(const_cast<Class*>(super->getSubschemaCopy(parent)))
	{
		assert(!super || superclass && superclass->schema == parent);

		schema = parent;

		// Special handling required for the first class to be created (which is the "Class" Class)
		SetDataClass(*(dataClass ? dataClass : (string(name) == "Class" ? this : GetDataClass(parent, &RootSchema::class_, "Class"))));
	}

	Class::Class(JadeImportedPackage* parent, const Class* dataClass, const char* name) : Type(parent, dataClass, name)
	{
	}

	void Class::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	AnyValue* Class::CreateValue() const
	{
		return ObjectFactory::Get().Lookup(&getOriginal())->CreateValue();
	}

	AnyValue* Class::CreateValue(Object& object, const Property& property, bool exclusive) const
	{
		return ObjectFactory::Get().Lookup(&getOriginal())->CreateValue(object, property, this, exclusive);
	}

	const JadeInterfaceMapping& Class::GetInterfaceMapping(JadeInterface* interface)
	{
		if (!implementedInterfaces.Includes(interface))
			throw runtime_error("Unimplemented interface");

		return interfaceMappings.try_emplace(interface, *this, interface->getRootType()).first->second;
	}

	bool Class::inheritsFrom(const Type* type) const
	{
		if (!type)
			return false;

		const Type& base = type->getOriginal();
		const Class* super = this;
		while (super)
		{
			super = &super->getOriginal();
			
			if (super == &base)
				return true;

			super = super->getSuperClass();
		}

		return false;
	}

	bool Class::IsSubclass(const Class* superclass) const
	{
		if (!superclass || !this->superclass)
			return false;

		return this->superclass == superclass || this->superclass->IsSubclass(superclass);
	}

	Feature* Class::getFeature(const string& name) const
	{
		if (auto feature = properties.Get(name))
			return feature;

		return Type::getFeature(name);
	}

	const Class& Class::getOriginal() const
	{
		return static_cast<const Class&>(Type::getOriginal());
	}

	const Class& Class::getRootType() const
	{
		return static_cast<const Class&>(Type::getRootType());
	}

	const Class* Class::getSubschemaCopy(const Schema* schema) const
	{
		if (!this || !schema)
			return nullptr;

		if (this->schema == schema)
			return this;

		return Entity::resolve<Class>(*schema, name, true);
	}

	const Class* Class::getSuperClass() const
	{
		// Resolve root type (subschema copies don't define the superclass)
		auto& rootType = getRootType();

		// Ensure root type has been loaded to resolve superclass
		rootType.Load();

		if(!rootType.superclass && GetName() != "Object")
			throw runtime_error(GetQualifiedName() + " has no superclass");

		return rootType.superclass;
	}

	const Property& Class::getProperty(const string& name) const
	{
		if (auto prop = tryGetProperty(name))
			return *prop;

		throw runtime_error(format("Unknown property ({}::{})", GetName(), name));
	}

	const Property* Class::tryGetProperty(const string& name) const
	{
		// Iterate through super classes starting with current
		const Class* superClass = this;
		while (superClass)
		{
			// Resolve root type (subschema copies don't have properties)
			auto& rootType = superClass->getRootType();

			// Ensure root type has been loaded to get property
			rootType.Load();

			// Lookup property
			if (const Property* property = rootType.properties.Get(name))
				return property;

			// Move to next super class
			superClass = rootType.getSuperClass();
		}

		// Not found
		return nullptr;
	}

	void Class::LoadFor(Object &object, const Property& property, const tinyxml2::XMLElement* source, bool strict, queue<future<void>>& tasks) const
	{
		auto resolver = ObjectFactory::Get().resolver(GetName(), &object, source, strict);

		if (property.isStructural())
		{
			property.SetValue(object, Any(Value(resolver())));
		}
		else
		{
			tasks.push(async(launch::deferred, [resolver](Object& object, const Property& property) { property.SetValue(object, Any(Value(resolver()))); }, ref(object), ref(property)));
		}
	}

	void Class::WriteFor(const Object &object, const Property& property, tinyxml2::XMLNode& parent) const
	{
		// Get object and write if set
		if (Object* value = property.GetValue(object).Get<Object*>())
			WriteFor(object, property, parent, value);
	}

	void Class::WriteFor(const Object &object, const Property& property, tinyxml2::XMLNode& parent, const Object* value) const
	{
		// Suppress writing simple references for copies (primary version defines these, anything else is operational)
		if (object.isCopy())
			return;
		
		// Setup property element
		tinyxml2::XMLElement* element = parent.GetDocument()->NewElement(property.name.c_str());
		parent.InsertEndChild(element);

		// Write object, treating it as a reference if it's an entity
		value->Write(parent, element, &object, dynamic_cast<const Entity*>(value));
	}

	JadeInterfaceMapping::JadeInterfaceMapping(Class& type, const JadeInterface& interface) : Component(&type), type(type), interface(interface)
	{
		for (auto interfaceMethod : interface.methods)
		{
			for (auto method : type.methods)
			{
				if (method.second->interfaceImplements.Includes(static_cast<JadeInterfaceMethod*>(interfaceMethod.second)))
				{
					methodMappings[static_cast<JadeInterfaceMethod*>(interfaceMethod.second)->name] = method.second;
				}
			}
		}
	}

	ostream& operator<< (ostream& stream, const JadeInterfaceMapping& mapping)
	{
		stream << mapping.type << " " << mapping.interface.name.c_str() << " Mapping";
		return stream;
	}

	ExceptionClass::ExceptionClass(Schema* parent, const Class* dataClass, const char* name, Class* superclass) : Class(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::exceptionClass), name, superclass) {}

	template ObjectValue<ExternalDatabase*, &ExternalClassMeta::externalDatabase>;
	template ObjectValue<ExternalClassMap*, &ExternalClassMeta::externalSchemaMap>;

	ExternalClass::ExternalClass(Schema* parent, const Class* dataClass, const char* name, Class* superclass) : Class(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::externalClass, "ExternalClass"), name, superclass)
	{
	}

	template ObjectValue<DevControlClass*, &GUIClassMeta::controlType>;

	GUIClass::GUIClass(Schema* parent, const Class* dataClass, const char* name, Class* superclass) : Class(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::guiClass, "GUIClass"), name, superclass) {}

	ClassMeta::ClassMeta(RootSchema& parent, const TypeMeta& superclass) : RootClass(parent, "Class", superclass),
		activeXClass(NewReference<ExplicitInverseRef>("activeXClass", NewType<Class>("ActiveXClass"))),
		classMapRefs(NewReference<ExplicitInverseRef>("classMapRefs", NewType<CollClass>("DbClassMapSet"))),
		dynamicPropertyClusters(NewReference<ExplicitInverseRef>("dynamicPropertyClusters", NewType<CollClass>("JadeDynamicPropertyClusterDict"))),
		exportedClassRefs(NewReference<ExplicitInverseRef>("exportedClassRefs", NewType<CollClass>("JadeExportedClassSet"))),
		exposedClassRefs(NewReference<ExplicitInverseRef>("exposedClassRefs", NewType<CollClass>("JadeExposedClassSet"))),
		implementedInterfaces(NewReference<ExplicitInverseRef>("implementedInterfaces", NewType<CollClass>("JadeInterfaceNDict"))),
		instanceVolatility(NewInteger("instanceVolatility")),
		properties(NewReference<ExplicitInverseRef>("properties", NewType<CollClass>("PropertyNDict"))),
		subclasses(NewReference<ExplicitInverseRef>("subclasses", NewType<CollClass>("ClassNDict"))),
		superclass(NewReference<ExplicitInverseRef>("superclass", NewType<Class>("Class"))),
		transient(NewBoolean("transient")),
		webService(NewBoolean("webService")),
		xmlInnerClass(NewBoolean("xmlInnerClass"))
	{
		activeXClass->automatic().bind(&Class::activeXClass);
		classMapRefs->automatic().parent().bind(&Class::classMapRefs);
		dynamicPropertyClusters->automatic().parent().bind(&Class::dynamicPropertyClusters);
		exportedClassRefs->automatic();
		exposedClassRefs->automatic();
		implementedInterfaces->manual().bind(&Class::implementedInterfaces);
		instanceVolatility->bind(&Class::instanceVolatility);
		properties->automatic().parent().bind(&Class::properties);
		subclasses->automatic().bind(&Class::subclasses);
		this->superclass->manual().structural().bind(&Class::superclass);
		transient->bind(&Class::transient);
		webService->bind(&Class::webService);
		xmlInnerClass->bind(&Class::xmlInnerClass);
	}

	ExceptionClassMeta::ExceptionClassMeta(RootSchema& parent, const ClassMeta& superclass) : RootClass(parent, "ExceptionClass", superclass) {}

	ExternalClassMeta::ExternalClassMeta(RootSchema& parent, const ClassMeta& superclass) : RootClass(parent, "ExternalClass", superclass),
		columnAttributeInfoList(NewBinary("columnAttributeInfoList")),
		extensionString(NewString("extensionString")),
		extensionStringMap(NewBinary("extensionStringMap")),
		externalDatabase(NewReference<ExplicitInverseRef>("externalDatabase", NewType<Class>("ExternalDatabase"))),
		externalSchemaMap(NewReference<ExplicitInverseRef>("externalSchemaMap", NewType<Class>("ExternalClassMap"))),
		fromList(NewString("fromList")),
		orderByList(NewString("orderByList")),
		rowidPredicate(NewString("rowidPredicate")),
		rowidPredicateInfo(NewBinary("rowidPredicateInfo")),
		selectList(NewString("selectList")),
		updatable(NewBoolean("updatable")),
		wherePredicate(NewString("wherePredicate"))
	{
		externalDatabase->manual().bind(&ExternalClass::externalDatabase);
		externalSchemaMap->automatic().bind(&ExternalClass::externalSchemaMap);
	}

	GUIClassMeta::GUIClassMeta(RootSchema& parent, const ClassMeta& superclass) : RootClass(parent, "GUIClass", superclass),
		activeXControlClass(NewReference<ImplicitInverseRef>("activeXControlClass", NewType<Class>("Class"))),
		controlType(NewReference<ExplicitInverseRef>("controlType", NewType<Class>("DevControlClass"))),
		registryId(NewString("registryId"))
	{
		controlType->automatic().parent().bind(&GUIClass::controlType);
	}

	ActiveXGuiClassMeta::ActiveXGuiClassMeta(RootSchema& parent, const GUIClassMeta& superclass) : RootClass(parent, "ActiveXGuiClass", superclass) {}

	JadeUserClassMeta::JadeUserClassMeta(RootSchema& parent, const ClassMeta& superclass) : RootClass(parent, "JadeUserClass", superclass) {}
}