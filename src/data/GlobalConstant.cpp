#include <jadegit/data/GlobalConstant.h>
#include <jadegit/data/Schema.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/ConstantCategory.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/GlobalConstantMeta.h>
#include "SchemaEntityRegistration.h"

namespace JadeGit::Data
{
	DEFINE_OBJECT_CAST(GlobalConstant)

	static SchemaEntityRegistration<GlobalConstant> registrar("GlobalConstant", &Schema::constants);

	template ObjectValue<Schema* const, &GlobalConstantMeta::schema>;
	template ObjectValue<ConstantCategory*, &GlobalConstantMeta::category>;

	GlobalConstant::GlobalConstant(Schema* parent, const Class* dataClass, const char* name) : Constant(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::globalConstant), name), 
		schema(parent)
	{
	}

	void GlobalConstant::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	GlobalConstantMeta::GlobalConstantMeta(RootSchema& parent, const ConstantMeta& superclass) : RootClass(parent, "GlobalConstant", superclass),
		category(NewReference<ExplicitInverseRef>("category", NewType<Class>("ConstantCategory"))),
		schema(NewReference<ExplicitInverseRef>("schema", NewType<Class>("Schema")))
	{
		category->manual().bind(&GlobalConstant::category);
		schema->manual().child().bind(&GlobalConstant::schema);
	}
}