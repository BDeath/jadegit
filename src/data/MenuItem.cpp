#include <jadegit/data/MenuItem.h>
#include <jadegit/data/Form.h>
#include <jadegit/data/Locale.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/MenuItemMeta.h>
#include "ObjectRegistration.h"

namespace JadeGit::Data
{
	static ObjectRegistration<MenuItem, Form> menuItem("MenuItem");

	std::map<MenuItem::ShortCutFlags, const char*> EnumStrings<MenuItem::ShortCutFlags>::data =
	{
		{ MenuItem::ShortCutFlags::None, "" },
		{ MenuItem::ShortCutFlags::Alt, "Alt" },
		{ MenuItem::ShortCutFlags::Ctrl, "Ctrl" },
		{ MenuItem::ShortCutFlags::Ctrl_Alt, "Ctrl+Alt" },
		{ MenuItem::ShortCutFlags::Shift, "Shift" },
		{ MenuItem::ShortCutFlags::Shift_Alt, "Shift+Alt" },
		{ MenuItem::ShortCutFlags::Shift_Ctrl, "Shift+Ctrl" },
		{ MenuItem::ShortCutFlags::Shift_Ctrl_Alt, "Shift+Ctrl+Alt" }
	};

	std::map<MenuItem::ShortCutKey, const char*> EnumStrings<MenuItem::ShortCutKey>::data =
	{
		{ MenuItem::ShortCutKey::None, "" },
		{ MenuItem::ShortCutKey::Bksp, "Bksp" },
		{ MenuItem::ShortCutKey::Tab, "Tab" },
		{ MenuItem::ShortCutKey::Return, "Return" },
		{ MenuItem::ShortCutKey::PageUp, "PageUp" },
		{ MenuItem::ShortCutKey::PageDown, "PageDown" },
		{ MenuItem::ShortCutKey::End, "End" },
		{ MenuItem::ShortCutKey::Home, "Home" },
		{ MenuItem::ShortCutKey::LeftArrow, "LeftArrow" },
		{ MenuItem::ShortCutKey::UpArrow, "UpArrow" },
		{ MenuItem::ShortCutKey::RightArrow, "RightArrow" },
		{ MenuItem::ShortCutKey::DownArrow, "DownArrow" },
		{ MenuItem::ShortCutKey::Ins, "Ins" },
		{ MenuItem::ShortCutKey::Del, "Del" },
		{ MenuItem::ShortCutKey::N0, "0" },
		{ MenuItem::ShortCutKey::N1, "1" },
		{ MenuItem::ShortCutKey::N2, "2" },
		{ MenuItem::ShortCutKey::N3, "3" },
		{ MenuItem::ShortCutKey::N4, "4" },
		{ MenuItem::ShortCutKey::N5, "5" },
		{ MenuItem::ShortCutKey::N6, "6" },
		{ MenuItem::ShortCutKey::N7, "7" },
		{ MenuItem::ShortCutKey::N8, "8" },
		{ MenuItem::ShortCutKey::N9, "9" },
		{ MenuItem::ShortCutKey::A, "A" },
		{ MenuItem::ShortCutKey::B, "B" },
		{ MenuItem::ShortCutKey::C, "C" },
		{ MenuItem::ShortCutKey::D, "D" },
		{ MenuItem::ShortCutKey::E, "E" },
		{ MenuItem::ShortCutKey::F, "F" },
		{ MenuItem::ShortCutKey::G, "G" },
		{ MenuItem::ShortCutKey::H, "H" },
		{ MenuItem::ShortCutKey::I, "I" },
		{ MenuItem::ShortCutKey::J, "J" },
		{ MenuItem::ShortCutKey::K, "K" },
		{ MenuItem::ShortCutKey::L, "L" },
		{ MenuItem::ShortCutKey::M, "M" },
		{ MenuItem::ShortCutKey::N, "N" },
		{ MenuItem::ShortCutKey::O, "O" },
		{ MenuItem::ShortCutKey::P, "P" },
		{ MenuItem::ShortCutKey::Q, "Q" },
		{ MenuItem::ShortCutKey::R, "R" },
		{ MenuItem::ShortCutKey::S, "S" },
		{ MenuItem::ShortCutKey::T, "T" },
		{ MenuItem::ShortCutKey::U, "U" },
		{ MenuItem::ShortCutKey::V, "V" },
		{ MenuItem::ShortCutKey::W, "W" },
		{ MenuItem::ShortCutKey::X, "X" },
		{ MenuItem::ShortCutKey::Y, "Y" },
		{ MenuItem::ShortCutKey::Z, "Z" },
		{ MenuItem::ShortCutKey::F1, "F1" },
		{ MenuItem::ShortCutKey::F2, "F2" },
		{ MenuItem::ShortCutKey::F3, "F3" },
		{ MenuItem::ShortCutKey::F4, "F4" },
		{ MenuItem::ShortCutKey::F5, "F5" },
		{ MenuItem::ShortCutKey::F6, "F6" },
		{ MenuItem::ShortCutKey::F7, "F7" },
		{ MenuItem::ShortCutKey::F8, "F8" },
		{ MenuItem::ShortCutKey::F9, "F9" },
		{ MenuItem::ShortCutKey::F10, "F10" },
		{ MenuItem::ShortCutKey::F11, "F11" },
		{ MenuItem::ShortCutKey::F12, "F12" },
	};

	template Value<MenuItem::ShortCutFlags>;
	template Value<MenuItem::ShortCutKey>;

	MenuItem::MenuItem(Form* parent, const Class* dataClass) : MenuItemData(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::menuItem)),
		form(parent)
	{
	}

	MenuItemDataMeta::MenuItemDataMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "MenuItemData", superclass) {}

	MenuItemMeta::MenuItemMeta(RootSchema& parent, const MenuItemDataMeta& superclass) : RootClass(parent, "MenuItem", superclass),
		backColor(NewInteger("backColor")),
		caption(NewString("caption", 101)),
		checked(NewBoolean("checked")),
		commandId(NewInteger("commandId")),
		description(NewString("description")),
		disableReason(NewString("disableReason")),
		enabled(NewBoolean("enabled")),
		foreColor(NewInteger("foreColor")),
		form(NewReference<ExplicitInverseRef>("form", NewType<GUIClass>("Form"))),
		hasSubMenu(NewBoolean("hasSubMenu")),
		helpContextId(NewInteger("helpContextId")),
		helpKeyword(NewString("helpKeyword")),
		helpList(NewBoolean("helpList")),
		level(NewInteger("level")),
		name(NewString("name", 101)),
		picture(NewBinary("picture")),
		securityLevelEnabled(NewInteger("securityLevelEnabled")),
		securityLevelVisible(NewInteger("securityLevelVisible")),
		shortCutFlags(NewInteger("shortCutFlags")),
		shortCutKey(NewCharacter("shortCutKey")),
		visible(NewBoolean("visible")),
		webFileName(NewString("webFileName")),
		windowList(NewBoolean("windowList"))
	{
		form->manual().child().bind(&MenuItem::form);
		name->bind(&MenuItem::name);
		shortCutFlags->bind(&MenuItem::shortCutFlags);
		shortCutKey->bind(&MenuItem::shortCutKey);

		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "select");
	}
}