#include <jadegit/data/ObjectFactory.h>
#include <jadegit/data/EntityFactory.h>
#include <jadegit/data/Assembly.h>

using namespace std;

namespace JadeGit::Data
{
	ObjectFactory& ObjectFactory::Get()
	{
		static ObjectFactory f; return f;
	}

	Object* ObjectFactory::load(const string& key, Component* parent, const FileElement& source) const
	{
		Class* dataClass = nullptr;
		return Lookup(key, parent, dataClass)->load(parent, dataClass, source);
	}

	const ObjectFactory::Registration* ObjectFactory::Lookup(const string &key) const
	{
		auto iter = registryByName.find(key);
		if (iter != registryByName.end())
			return iter->second;

		// Throw error for unhandled object
		throw logic_error("Unhandled object [" + key + "]");
	}

	const ObjectFactory::Registration* ObjectFactory::Lookup(const string& key, const Component* origin, Class* &dataClass, bool required) const
	{
		// Handle basic/direct lookup
		auto iter = registryByName.find(key);
		if (iter != registryByName.end())
			return static_cast<const ObjectFactory::Registration*>(iter->second);
		
		// Attempt lookup using superclass
		if (dataClass = EntityFactory::Get().Resolve<Class>(origin, QualifiedName(key), required))
			if (auto result = Lookup(dataClass->getSuperClass(), false))
				return result;

		// Throw error for unhandled object
		if (required)
			throw logic_error("Unhandled object [" + key + "]");

		return nullptr;
	}

	const ObjectFactory::Registration* ObjectFactory::Lookup(const type_info &type) const
	{
		auto iter = registryByType.find(type_index(type));
		if (iter != registryByType.end())
			return iter->second;

		// Throw error for unhandled object
		throw logic_error("Unhandled object [" + string(type.name()) + "]");
	}

	const ObjectFactory::Registration* ObjectFactory::Lookup(const Class* dataClass, bool required) const
	{
		// Resolve base component for class
		const Class* superClass = dataClass;
		while (superClass)
		{
			auto iter = registryByName.find(superClass->GetName());
			if (iter != registryByName.end())
				return static_cast<const ObjectFactory::Registration*>(iter->second);

			superClass = superClass->getSuperClass();
		}

		// Throw error for unhandled object
		if (required)
			throw logic_error("Unhandled object [" + dataClass->name + "]");

		return nullptr;
	}

	function<Object* ()> ObjectFactory::resolver(const string& key, const Component* origin, const tinyxml2::XMLElement* source, bool expected, bool shallow, bool inherit) const
	{
		Class* dataClass = nullptr;
		return Lookup(key, origin, dataClass)->resolver(origin, source, expected, shallow, inherit);
	}

	function<Object* ()> ObjectFactory::resolver(const type_info& type, const Component* origin, const tinyxml2::XMLElement* source, bool expected, bool shallow, bool inherit) const
	{
		return Lookup(type)->resolver(origin, source, expected, shallow, inherit);
	}
}