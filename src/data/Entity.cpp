#include <jadegit/data/Assembly.h>
#include <jadegit/data/EntityFactory.h>
#include <vfs/FileSignature.h>
#include "EntityIDAllocator.h"
#include "State.h"
#include "storage/ObjectFileStorage.h"

using namespace std;

namespace JadeGit::Data
{
	Entity* Entity::Load(Component* origin, const filesystem::path& path, bool shallow)
	{
		return origin->getAssembly().Load(path, shallow);
	}

	Entity* Entity::resolve(const type_info& type, const Component& origin, const QualifiedName& name, bool shallow, bool inherit, bool expected)
	{
		return EntityFactory::Get().Lookup(type)->Resolve(&origin, &name, expected, shallow, inherit);
	}

	ostream& operator<< (ostream& stream, const Entity& entity)
	{
		stream << entity.GetQualifiedName();
		return stream;
	}

	void Entity::AcceptAll(EntityVisitor &v, bool deep)
	{
		Accept(v);
		AcceptChildren(v, deep);
	}

	void Entity::AcceptChildren(EntityVisitor &v, bool deep)
	{
		for (Object* object : children)
		{
			Entity* child = dynamic_cast<Entity*>(object);
			if (child && (deep || child->isInferred() || !child->isMajor()))
				child->AcceptAll(v, deep);
		}
	}

	const Entity* Entity::asEntity() const
	{
		return this;
	}

	uuids::uuid Entity::getId() const
	{
		if (id.is_nil())
		{
			// Generate id when needed for static entities
			if (isStatic())
			{
				constexpr uuids::uuid nil;
				thread_local uuids::uuid_name_generator gen(nil);
				id = gen(dataClass->name + ":" + GetQualifiedName());
			}
			// Allocate id when needed for created entities
			else if (state->isCreated())
			{
				id = EntityIDAllocator::get().allocate(*this);
				if (id.is_nil())
					throw runtime_error("Failed to allocate id");
			}
			// Otherwise, throw error to report missing id
			else
				throw runtime_error(dataClass->name + " [" + GetQualifiedName() + "] has no id");
		}

		assert(!id.is_nil());
		return id;
	}

	EntityKey Entity::getKey() const
	{
		EntityKey result = EntityKey();

		auto& original = getOriginal();
		result.first = original.getId();

		if (this != &original)
		{
			auto parent = getParentEntity();
			while (!parent->isOriginal())
				parent = parent->getParentEntity();

			result.second = parent->getId();
		}

		return result;
	}

	const Entity& Entity::getOriginal() const
	{
		return *this;
	}

	bool Entity::isOriginal() const
	{
		return this == &getOriginal();
	}

	Entity* Entity::getParentEntity() const
	{
		auto parent = getParentObject();

		// If parent is an object, it must also be an entity
		assert(!parent || dynamic_cast<Entity*>(parent));

		return static_cast<Entity*>(parent);
	}

	const Entity* Entity::GetQualifiedParent() const
	{
		return GetQualifiedParent(false);	// Base implementation casts parent to entity (below)
	}

	const Entity* Entity::GetQualifiedParent(bool shorthand) const
	{
		return shorthand ? GetQualifiedParent() : getParentEntity();
	}

	string Entity::GetQualifiedName(const Entity* context, bool shorthand) const
	{
		const Entity* parent = GetQualifiedParent(shorthand);
		if (parent && (!context || !context->IsDescendent(parent)))
			return parent->GetQualifiedName(context, shorthand) + "::" + GetName();
		else
			return GetName();
	}

	const FileSignature& Entity::getSignature() const
	{
		if (signature)
			return *signature;

		static FileSignature empty;
		return empty;
	}

	void Entity::Created(const char* guid)
	{
		state->created(this, guid);
	}

	void Entity::clean()
	{
		state->clean(this);
	}

	void Entity::dirty(bool deep)
	{
		state->dirty(*this, deep);
	}

	Entity* Entity::implied()
	{
		state->implied(this);
		return this;
	}

	Entity* Entity::inferred(bool condition)
	{
		if (condition)
			state->inferred(*this);

		return this;
	}

	void Entity::inferredFrom(const Entity* major, const Entity* minor)
	{	
		// Change state
		state->inferred(*this);

		// Use most recent signature
		const Entity* entity = minor->getSignature().modified > major->getSignature().modified ? minor : major;
		this->signature = &entity->getSignature();
	}

	Entity* Entity::Load()
	{
		state->load(*this);
		return this;
	}

	const Entity* Entity::Load() const
	{
		state->load(*const_cast<Entity*>(this));
		return this;
	}

	void Entity::LoadHeader(const FileElement& source)
	{
		NamedObject::LoadHeader(source);

		// Load identifier
		if (const char* id = source.header("id"))
		{
			if (this->id.is_nil())
				this->id = uuids::uuid::from_string(id).value();

			// Verify id matches what was used on construction
			else if (this->id != uuids::uuid::from_string(id).value())
				throw logic_error("Id attribute doesn't match existing");
		}
	}

	void Entity::LoadBody(const FileElement& source)
	{
		NamedObject::LoadBody(source);

		// Retrieve modified by details
		signature = source.getSignature();
	}

	void Entity::Save(bool prelude)
	{
		state->save(this, prelude);
	}

	void Entity::Delete()
	{
		// Delete associated file & possible sub-folder for major entities
		if (isMajor())
		{
			getAssembly().getStorage().dele(*this);
		}
		// Flag parent as modified otherwise
		else
		{
			getParentObject()->Modified();
		}

		// Clean-up deleted entity
		delete this;
	}

	void Entity::Rename(const char* new_name)
	{
		// Ensure existing object has been loaded before update
		Load();

		// Rename asscociated file & possible sub-folder for major entities
		if (isMajor())
		{
			getAssembly().getStorage().rename(*this, new_name);
		}

		// Rename entity
		this->name = new_name;

		// Flag entity as modified to be saved later
		this->Modified();
	}

	void Entity::Write(tinyxml2::XMLDocument &document) const
	{
		tinyxml2::XMLElement* root = nullptr;
		Write(document, root, nullptr, false);
	}

	void Entity::Write(tinyxml2::XMLNode &parent, tinyxml2::XMLElement* &element, const Object* origin, bool reference) const
	{
		// Suppress writing major entities within another
		if (!reference && origin && isMajor())
			return;

		NamedObject::Write(parent, element, origin, reference);		
	}

	void Entity::WriteHeader(tinyxml2::XMLElement* element, const Object* origin, bool reference) const
	{
		NamedObject::WriteHeader(element, origin, reference);

		// Use qualified name for references
		if (reference)
		{
			// Resolve parent entity
			const Object* parent = origin;
			const Entity* context = nullptr;
			while (parent && !context)
			{
				context = dynamic_cast<const Entity*>(parent);
				parent = parent->getParentObject();
			}

			element->SetAttribute("name", GetQualifiedName(context).c_str());
		}

		// Write id if original entity is being written in full
		if (!reference && isOriginal())
			element->SetAttribute("id", to_string(getId()).c_str());
	}

	// Converts entity to another data class (if different)
	Entity* Entity::Mutate(const string& dataClass)
	{
		// Return self if conversion isn't required
		if (this->dataClass->name == dataClass)
			return this;

		string name = this->name;
		this->name = "__mutating";

		// Create replacement
		Entity* replacement = EntityFactory::Get().Create(dataClass, parent, name.c_str());

		// Transfer id
		replacement->id = id;

		// Convert using basic object mutation
		return static_cast<Entity*>(&NamedObject::Mutate(*replacement));
	}

	std::filesystem::path Entity::path() const
	{
		const Entity* major = this;
		while (!major->isMajor())
			major = major->getParentEntity();

		return major->path();
	}
}