#include <jadegit/data/Feature.h>
#include <jadegit/data/ActiveXFeature.h>
#include <jadegit/data/CollClass.h>
#include "FeatureRegistration.h"

using namespace std;

namespace JadeGit::Data
{
	// Feature registration needed to support resolving Feature references during load
	static FeatureRegistration<Feature, Type> feature("Feature", mem_fn(&Type::getFeature));

	template <>
	void ObjectValue<Type*, &FeatureMeta::schemaType>::inverseAdd(Object& target) const
	{
		if (!offset)
			offset = member_offset(&Feature::schemaType);

		auto& feature = getObject();
		auto& schemaType = static_cast<Type&>(target);

		if (auto klass = dynamic_cast<Class*>(&schemaType))
		{
			if (static_cast<ObjectReference&>(klass->properties).autoAdd(feature))
				return;
		}

		if (!static_cast<ObjectReference&>(schemaType.constants).autoAdd(feature) &&
			!static_cast<ObjectReference&>(schemaType.methods).autoAdd(feature))
			throw inverse_maintenance_exception();
	}

	template <>
	void ObjectValue<Type*, &FeatureMeta::type>::inverseAdd(Object& target) const
	{
		if (!offset)
			offset = member_offset(&Feature::type);

		auto& feature = getObject();
		auto& type = static_cast<Type&>(target);

		if (!static_cast<ObjectReference&>(type.constantRefs).autoAdd(feature) &&
			!static_cast<ObjectReference&>(type.propertyRefs).autoAdd(feature))
			throw inverse_maintenance_exception();
	}

	template ObjectValue<ActiveXFeature*, &FeatureMeta::activeXFeature>;
	template ObjectValue<Type*, &FeatureMeta::schemaType>;
	template ObjectValue<Type*, &FeatureMeta::type>;

	Feature::Feature(Type* parent, const Class* dataClass, const char* name) : SchemaEntity(parent, dataClass, name)
	{
	}

	Feature::Feature(Object* parent, const Class* dataClass, const char* name) : SchemaEntity(parent, dataClass, name)
	{
	}

	const Type& Feature::getType() const
	{
		if (!type)
			throw runtime_error(GetQualifiedName() + " missing type");

		return *type;
	}

	FeatureMeta::FeatureMeta(RootSchema& parent, const SchemaEntityMeta& superclass) : RootClass(parent, "Feature", superclass),
		activeXFeature(NewReference<ExplicitInverseRef>("activeXFeature", NewType<Class>("ActiveXFeature"))),
		exposedClassRefs(NewReference<ExplicitInverseRef>("exposedClassRefs", NewType<CollClass>("JadeExposedClassSet"))),
		exposedFeatureRefs(NewReference<ExplicitInverseRef>("exposedFeatureRefs", NewType<CollClass>("JadeExposedFeatureSet"))),
		schemaType(NewReference<ExplicitInverseRef>("schemaType", NewType<Class>("Type"))),
		type(NewReference<ExplicitInverseRef>("type", NewType<Class>("Type")))
	{
		activeXFeature->automatic().bind(&Feature::activeXFeature);
		exposedClassRefs->automatic();
		exposedFeatureRefs->automatic();
		schemaType->manual().child().bind(&Feature::schemaType);
		type->manual().inverseUnrequired().structural().bind(&Feature::type);
	}
}