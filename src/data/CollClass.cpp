#include <jadegit/data/ExternalCollClass.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/Key.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ExternalCollClassMeta.h>
#include <jadegit/data/RootSchema/JadeUserCollClassMeta.h>
#include "TypeRegistration.h"

using namespace std;

namespace JadeGit::Data
{
	DEFINE_OBJECT_CAST(CollClass);

	static TypeRegistration<CollClass, Schema> collClass("CollClass", &Schema::classes);
	static TypeRegistration<ExternalCollClass, Schema> externalCollClass("ExternalCollClass", &Schema::classes);

	template ObjectValue<Type*, &CollClassMeta::memberType>;

	CollClass::CollClass(Schema* parent, const Class* dataClass, const char* name, Class* superclass) : Class(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::collClass, "CollClass"), name, superclass)
	{
	}

	void CollClass::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	const Type& CollClass::getMemberType() const
	{
		// Resolve root type (subschema copies don't define the member type)
		auto& rootType = static_cast<const CollClass&>(getRootType());

		// Ensure root type has been loaded to resolve member type
		rootType.Load();

		if (!rootType.memberType)
			throw runtime_error(GetQualifiedName() + " has no member type");

		return *rootType.memberType;
	}

	void CollClass::LoadFor(Object &object, const Property& property, const tinyxml2::XMLElement* source, bool strict, queue<future<void>>& tasks) const
	{
		/* Load collection */
		Collection* collection = property.GetValue(object, true).Get<Collection*>();
		collection->LoadFor(object, *source, strict, tasks);
	}

	void CollClass::WriteFor(const Object &object, const Property& property, tinyxml2::XMLNode& parent, const Object* value) const
	{
		const Collection* collection = dynamic_cast<const Collection*>(value);

		/* Ignore if collection is empty */
		if (collection && collection->Empty())
			return;

		/* Setup property element */
		tinyxml2::XMLElement* element = parent.GetDocument()->NewElement(property.name.c_str());
		parent.InsertEndChild(element);

		/* Write collection */
		collection->WriteFor(object, *element);
	}

	template ObjectValue<ExternalDatabase*, &ExternalCollClassMeta::externalDatabase>;
	template ObjectValue<ExternalCollClassMap*, &ExternalCollClassMeta::externalSchemaMap>;

	ExternalCollClass::ExternalCollClass(Schema* parent, const Class* dataClass, const char* name, Class* superclass) : CollClass(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::externalCollClass), name, superclass)
	{
	}

	CollClassMeta::CollClassMeta(RootSchema& parent, const ClassMeta& superclass) : RootClass(parent, "CollClass", superclass),
		blockSize(NewInteger("blockSize")),
		duplicatesAllowed(NewBoolean("duplicatesAllowed")),
		expectedPopulation(NewInteger("expectedPopulation")),
		loadFactor(NewInteger("loadFactor")),
		memberType(NewReference<ExplicitInverseRef>("memberType", NewType<Class>("Type"))),
		memberTypePrecision(NewCharacter("memberTypePrecision")),
		memberTypeScaleFactor(NewCharacter("memberTypeScaleFactor")),
		memberTypeSize(NewInteger("memberTypeSize")),
		memberTypeWSDLName(NewString("memberTypeWSDLName"))
	{
		blockSize->bind(&CollClass::blockSize);
		duplicatesAllowed->bind(&CollClass::duplicatesAllowed);
		expectedPopulation->bind(&CollClass::expectedPopulation);
		loadFactor->bind(&CollClass::loadFactor);
		memberType->manual().bind(&CollClass::memberType);
		memberTypePrecision->bind(&CollClass::memberTypePrecision);
		memberTypeScaleFactor->bind(&CollClass::memberTypeScaleFactor);
		memberTypeSize->bind(&CollClass::memberTypeSize);
		memberTypeWSDLName->bind(&CollClass::memberTypeWSDLName);
	}

	ExternalCollClassMeta::ExternalCollClassMeta(RootSchema& parent, const CollClassMeta& superclass) : RootClass(parent, "ExternalCollClass", superclass),
		extensionString(NewString("extensionString")),
		extensionStringMap(NewBinary("extensionStringMap")),
		externalDatabase(NewReference<ExplicitInverseRef>("externalDatabase", NewType<Class>("ExternalDatabase"))),
		externalSchemaMap(NewReference<ExplicitInverseRef>("externalSchemaMap", NewType<Class>("ExternalCollClassMap"))),
		firstPredicate(NewString("firstPredicate")),
		firstPredicateInfo(NewBinary("firstPredicateInfo")),
		groupByList(NewString("groupByList")),
		havingPredicate(NewString("havingPredicate")),
		havingPredicateInfo(NewBinary("havingPredicateInfo")),
		includesPredicate(NewString("includesPredicate")),
		includesPredicateInfo(NewBinary("includesPredicateInfo")),
		keyEqPredicate(NewString("keyEqPredicate")),
		keyEqPredicateInfo(NewBinary("keyEqPredicateInfo")),
		keyGeqPredicate(NewString("keyGeqPredicate")),
		keyGeqPredicateInfo(NewBinary("keyGeqPredicateInfo")),
		keyGtrPredicate(NewString("keyGtrPredicate")),
		keyGtrPredicateInfo(NewBinary("keyGtrPredicateInfo")),
		keyLeqPredicate(NewString("keyLeqPredicate")),
		keyLeqPredicateInfo(NewBinary("keyLeqPredicateInfo")),
		keyLssPredicate(NewString("keyLssPredicate")),
		keyLssPredicateInfo(NewBinary("keyLssPredicateInfo")),
		lastPredicate(NewString("lastPredicate")),
		lastPredicateInfo(NewBinary("lastPredicateInfo")),
		orderByList(NewString("orderByList"))
	{
		externalDatabase->manual().bind(&ExternalCollClass::externalDatabase);
		externalSchemaMap->automatic().bind(&ExternalCollClass::externalSchemaMap);
	}
	
	JadeUserCollClassMeta::JadeUserCollClassMeta(RootSchema& parent, const CollClassMeta& superclass) : RootClass(parent, "JadeUserCollClass", superclass) {}
}