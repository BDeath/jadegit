#include <jadegit/data/LocaleFormat.h>
#include <jadegit/data/Schema.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/DateFormatMeta.h>
#include <jadegit/data/RootSchema/CurrencyFormatMeta.h>
#include <jadegit/data/RootSchema/TimeFormatMeta.h>
#include "EntityRegistration.h"

namespace JadeGit::Data
{
	// TODO: Change to use common base class for resolving locale formats inherited from superschema? 
	// This is currently handled for other schema features (i.e. GlobalConstant), but unclear if it's needed here
	template<class TDerived>
	class LocaleFormatRegistration : protected EntityRegistration<TDerived, Schema>
	{
	public:
		LocaleFormatRegistration(const char* key) : EntityRegistration<TDerived, Schema>(key, &Schema::localeFormats) {}
	};

	static LocaleFormatRegistration<DateFormat> dateFormat("DateFormat");
	static LocaleFormatRegistration<NumberFormat> numberFormat("NumberFormat");
	static LocaleFormatRegistration<CurrencyFormat> currencyFormat("CurrencyFormat");
	static LocaleFormatRegistration<TimeFormat> timeFormat("TimeFormat");

	template ObjectValue<Schema* const, &LocaleFormatMeta::schema>;

	LocaleFormat::LocaleFormat(Schema* parent, const Class* dataClass, const char* name) : Feature(parent, dataClass, name),
		schema(parent)
	{
	}

	DateFormat::DateFormat(Schema* parent, const Class* dataClass, const char* name) : LocaleFormat(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::dateFormat), name) {}

	void DateFormat::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	NumberFormat::NumberFormat(Schema* parent, const Class* dataClass, const char* name) : LocaleFormat(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::numberFormat), name) {}

	void NumberFormat::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	CurrencyFormat::CurrencyFormat(Schema* parent, const Class* dataClass, const char* name) : NumberFormat(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::currencyFormat), name) {}

	void CurrencyFormat::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	TimeFormat::TimeFormat(Schema* parent, const Class* dataClass, const char* name) : LocaleFormat(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::timeFormat), name) {}

	void TimeFormat::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	LocaleFormatMeta::LocaleFormatMeta(RootSchema& parent, const FeatureMeta& superclass) : RootClass(parent, "LocaleFormat", superclass),
		schema(NewReference<ExplicitInverseRef>("schema", NewType<Class>("Schema")))
	{
		schema->manual().child().bind(&LocaleFormat::schema);
	}

	DateFormatMeta::DateFormatMeta(RootSchema& parent, const LocaleFormatMeta& superclass) : RootClass(parent, "DateFormat", superclass),
		dayHasLeadingZeros(NewBoolean("dayHasLeadingZeros")),
		firstDayOfWeek(NewInteger("firstDayOfWeek")),
		firstWeekOfYear(NewInteger("firstWeekOfYear")),
		longFormatOrder(NewInteger("longFormatOrder")),
		monthHasLeadingZeros(NewBoolean("monthHasLeadingZeros")),
		separator(NewString("separator", 21)),
		shortDayNames(NewProperty<CompAttribute>("shortDayNames", NewType<CollClass>("StringArray"))),
		shortFormatOrder(NewInteger("shortFormatOrder")),
		showFullCentury(NewBoolean("showFullCentury"))
	{
		dayHasLeadingZeros->bind(&DateFormat::dayHasLeadingZeros);
		firstDayOfWeek->bind(&DateFormat::firstDayOfWeek);
		firstWeekOfYear->bind(&DateFormat::firstWeekOfYear);
		longFormatOrder->bind(&DateFormat::longFormatOrder);
		monthHasLeadingZeros->bind(&DateFormat::monthHasLeadingZeros);
		separator->bind(&DateFormat::separator);
		shortDayNames->bind(&DateFormat::shortDayNames);
		shortFormatOrder->bind(&DateFormat::shortFormatOrder);
		showFullCentury->bind(&DateFormat::showFullCentury);
	}

	NumberFormatMeta::NumberFormatMeta(RootSchema& parent, const LocaleFormatMeta& superclass) : RootClass(parent, "NumberFormat", superclass),
		decimalPlaces(NewInteger("decimalPlaces")),
		decimalSeparator(NewString("decimalSeparator", 21)),
		groupings(NewString("groupings", 81)),
		negativeFormat(NewInteger("negativeFormat")),
		showLeadingZeros(NewBoolean("showLeadingZeros")),
		thousandSeparator(NewString("thousandSeparator", 21))
	{
		decimalPlaces->bind(&NumberFormat::decimalPlaces);
		decimalSeparator->bind(&NumberFormat::decimalSeperator);
		groupings->bind(&NumberFormat::groupings);
		negativeFormat->bind(&NumberFormat::negativeFormat);
		showLeadingZeros->bind(&NumberFormat::showLeadingZeros);
		thousandSeparator->bind(&NumberFormat::thousandSeparator);
	}

	CurrencyFormatMeta::CurrencyFormatMeta(RootSchema& parent, const NumberFormatMeta& superclass) : RootClass(parent, "CurrencyFormat", superclass),
		positiveFormat(NewInteger("positiveFormat")),
		symbol(NewString("symbol", 21))
	{
		positiveFormat->bind(&CurrencyFormat::positiveFormat);
		symbol->bind(&CurrencyFormat::symbol);
	}

	TimeFormatMeta::TimeFormatMeta(RootSchema& parent, const LocaleFormatMeta& superclass) : RootClass(parent, "TimeFormat", superclass),
		amText(NewString("amText", 101)),
		ampmIsSuffix(NewBoolean("ampmIsSuffix")),
		is12HourFormat(NewBoolean("is12HourFormat")),
		pmText(NewString("pmText", 101)),
		separator(NewString("separator", 11)),
		showLeadingZeros(NewBoolean("showLeadingZeros")),
		showSeconds(NewBoolean("showSeconds"))
	{
		amText->bind(&TimeFormat::amText);
		ampmIsSuffix->bind(&TimeFormat::ampmIsSuffix);
		is12HourFormat->bind(&TimeFormat::is12HourFormat);
		pmText->bind(&TimeFormat::pmText);
		separator->bind(&TimeFormat::separator);
		showLeadingZeros->bind(&TimeFormat::showLeadingZeros);
		showSeconds->bind(&TimeFormat::showSeconds);
	}
}