#include <jadegit/data/JadeHTMLDocument.h>
#include <jadegit/data/HTMLClass.h>
#include <jadegit/data/Schema.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/Reference.h>
#include <jadegit/data/RootSchema.h>
#include "TypeRegistration.h"

namespace JadeGit::Data
{
	const std::filesystem::path JadeHTMLDocument::subFolder("html-documents");

	DEFINE_OBJECT_CAST(HTMLClass)

	static TypeRegistration<HTMLClass, Schema> htmlClass("HTMLClass", &Schema::classes);
	static EntityRegistration<JadeHTMLDocument, Schema> jadeHTMLDocument("JadeHTMLDocument", &Schema::htmlDocuments);

	HTMLClass::HTMLClass(Schema* parent, const Class* dataClass, const char* name, Class* superclass) : Class(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::htmlClass, "HTMLClass"), name, superclass)
	{
	}

	JadeHTMLDocument::JadeHTMLDocument(Schema* parent, const Class* dataClass, const char* name) : MajorEntity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeHTMLDocument), name),
		schema(parent)
	{
	}

	void JadeHTMLDocument::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	HTMLClassMeta::HTMLClassMeta(RootSchema& parent, const ClassMeta& superclass) : RootClass(parent, "HTMLClass", superclass),
		_jadeHTMLDocument(NewReference<ExplicitInverseRef>("_jadeHTMLDocument", NewType<Class>("JadeHTMLDocument")))
	{
		_jadeHTMLDocument->automatic().bind(&HTMLClass::htmlDocument);
	}

	JadeHTMLDocumentMeta::JadeHTMLDocumentMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "JadeHTMLDocument", superclass),
		addCookieCode(NewBoolean("addCookieCode")),
		autoComplete(NewBoolean("autoComplete")),
		bounceSeconds(NewInteger("bounceSeconds")),
		bounceURL(NewString("bounceURL")),
		description(NewString("description")),
		encodingType(NewBoolean("encodingType")),
		fileCreationTime(NewTimeStamp("fileCreationTime")),
		fileModifiedTime(NewTimeStamp("fileModifiedTime")),
		fileName(NewString("fileName")),
		homePage(NewBoolean("homePage")),
		htmlClass(NewReference<ExplicitInverseRef>("htmlClass", NewType<Class>("HTMLClass"))),
		htmlString(NewString("htmlString")),
		htmlStringModifiedTime(NewTimeStamp("htmlStringModifiedTime")),
		includeSessionData(NewBoolean("includeSessionData")),
		jadeClass(NewReference<ImplicitInverseRef>("jadeClass", NewType<Class>("Class"))),
		name(NewString("name", 101)),
		schema(NewReference<ExplicitInverseRef>("schema", NewType<Class>("Schema"))),
		securePage(NewBoolean("securePage")),
		targetWindow(NewString("targetWindow", 10)),
		uuid(NewBinary("uuid", 16))
	{
		htmlClass->manual().bind(&JadeHTMLDocument::htmlClass);
		schema->manual().child().bind(&JadeHTMLDocument::schema);
	}
}