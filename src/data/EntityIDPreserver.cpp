#include "EntityIDPreserver.h"
#include <jadegit/data/Assembly.h>
#include <jadegit/data/EntityFactory.h>

using namespace std;

namespace JadeGit::Data
{
	EntityIDPreserver::EntityIDPreserver(unique_ptr<FileSystem> fs) : next(EntityIDAllocator::get()), fs(move(fs))
	{
		// Override global allocator to use preserver
		EntityIDAllocator::set(this);
	}

	EntityIDPreserver::~EntityIDPreserver()
	{
		// Reset global allocator
		EntityIDAllocator::set(&next);
	}

	uuids::uuid EntityIDPreserver::allocate(const Entity& entity)
	{
		// Setup assembly
		if (!assembly)
			assembly = make_unique<Assembly>(*fs);

		// Resolve static data class
		const Class* dataClass = entity.dataClass;
		while (dataClass)
		{
			if (dataClass->isStatic())
				break;

			dataClass = dataClass->getSuperClass();
		}

		// Try resolving original entity id
		if (dataClass)
			if (const Entity* original = EntityFactory::Get().Resolve(dataClass->name, assembly.get(), entity.GetQualifiedName(), false, true, false))
				if (!original->id.is_nil())
					return original->id;

		// Fallback to default allocation strategy
		return next.allocate(entity);
	}
}