#include <jadegit\data\QualifiedName.h>

using namespace std;

namespace JadeGit
{
	QualifiedName::QualifiedName(string path)
	{
		size_t pos = path.rfind("::");
		if (pos != string::npos)
		{
			parent = make_unique<QualifiedName>(path.substr(0, pos));
			name = path.substr(pos + 2);
		}
		else
		{
			parent = nullptr;
			name = path;
		}
	}

	QualifiedName::QualifiedName(const char* path) : QualifiedName(string(path)) {}

	QualifiedName::operator string() const
	{
		if (parent)
			return (string)*parent + "::" + name;
		else
			return name;
	}

	bool QualifiedName::operator!() const
	{
		return name.empty();
	}
}