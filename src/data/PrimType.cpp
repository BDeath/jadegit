#include <jadegit/data/PrimType.h>
#include <jadegit/data/Chrono.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/PrimTypeMeta.h>
#include "TypeRegistration.h"

namespace JadeGit::Data
{
	static TypeRegistration<PrimType, Schema> registrar("PrimType", "primitive", &Schema::primitives);

	PrimType::PrimType(Schema* parent, const Class* dataClass, const char* name) : Type(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::primType, "PrimType"), name)
	{
		schema = parent;
	}
	
	void PrimType::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	int PrimType::GetDefaultLength() const
	{
		// Return if set
		if (length)
			return length;

		// Set length from root type
		length = getRootType().length;
		if (!length)
			length = -1;

		return length;
	}

	const PrimType& PrimType::getRootType() const
	{
		return static_cast<const PrimType&>(Type::getRootType());
	}

	PrimType* PrimType::SetDefaultLength(int length)
	{
		this->length = length;
		return this;
	}

	AnyValue* PrimType::CreateValue() const
	{
		auto& name = GetName();
		if (name == "Binary")
			return new Value<Binary>();
		else if (name == "Boolean")
			return new Value<bool>(false);
		else if (name == "Byte")
			return new Value<Byte>();
		else if (name == "Character")
			return new Value<char>(0);
		else if (name == "Date")
			return new Value<Date>();
		else if (name == "Decimal")
			return new Value<double>(0);		// TODO: Decimal class
		else if (name == "Integer")
			return new Value<int>(0);
		else if (name == "Integer64")
			return new Value<int64_t>(0);
		else if (name == "Real")
			return new Value<double>(0);
		else if (name == "String")
			return new Value<std::string>();
		else if (name == "StringUtf8")
			return new Value<std::u8string>();
		else if (name == "Time")
			return new Value<Time>();
		else if (name == "TimeStamp")
			return new Value<TimeStamp>();

		throw logic_error("Cannot create primitive value [" + name + "]");
	}

	AnyValue* PrimType::CreateValue(Object& object, const Property& property, bool exclusive) const
	{
		return CreateValue();	// TODO: PrimTypeRegistration
	}

	void PrimType::LoadFor(Object &object, const Property& property, const tinyxml2::XMLElement* source, bool strict, std::queue<std::future<void>>& tasks) const
	{
		auto text = source->GetText();
		property.SetValue<std::string>(object, text ? text : std::string());
	}

	void PrimType::WriteFor(const Object &object, const Property& property, tinyxml2::XMLNode& parent) const
	{
		/* Suppress writing simple attributes for copies (primary version defines these, anything else is operational) */
		if (object.isCopy())
			return;
		
		/* Get property value */
		Any value = property.GetValue(object);

		/* Ignore default values */
		if (value == property.GetDefaultValue())
			return;

		/* Setup property element */
		tinyxml2::XMLElement* element = parent.GetDocument()->NewElement(property.name.c_str());
		parent.InsertEndChild(element);

		/* Set element text to value */
		element->SetText(value.ToString().c_str());
	}

	PrimTypeMeta::PrimTypeMeta(RootSchema& parent, const TypeMeta& superclass) : RootClass(parent, "PrimType", superclass),
		supertype(NewReference<ExplicitInverseRef>("supertype", NewType<Class>("PrimType"))) {}
}