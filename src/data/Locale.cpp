#include <jadegit/data/Locale.h>
#include <jadegit/data/Schema.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/LocaleMeta.h>
#include "EntityRegistration.h"

namespace JadeGit::Data
{
	const std::filesystem::path Locale::subFolder("locales");

	static EntityRegistration<Locale, Schema> registrar("Locale", &Schema::locales);

	template ObjectValue<Schema* const, &LocaleMeta::schema>;
	template ObjectValue<Locale*, &LocaleMeta::cloneOf>;
	template ObjectValue<Set<Locale*>, &LocaleMeta::clones>;
	template EntityDict<Form, &LocaleMeta::forms>;
	template EntityDict<TranslatableString, &LocaleMeta::translatableStrings>;

	Locale::Locale(Schema* parent, const Class* dataClass, const char* name, Locale* cloneOf) : MajorEntity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::locale), name),
		schema(parent),
		cloneOf(cloneOf)
	{
	}

	void Locale::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	bool Locale::isPrimary() const
	{
		schema->Load();

		return schema->primaryLocale == this;
	}

	Locale* Locale::getSuperLocale() const
	{
		if (Schema* schema = this->schema->GetSuperSchema())
		{
			if (Locale* locale = registrar.Resolve(schema, name))
				return locale;
			else
			{
				schema->Load();
				return schema->primaryLocale;
			}
		}
		
		return nullptr;
	}

	LocaleMeta::LocaleMeta(RootSchema& parent, const SchemaEntityMeta& superclass) : RootClass(parent, "Locale", superclass),
		cloneOf(NewReference<ExplicitInverseRef>("cloneOf", NewType<Class>("Locale"))),
		clones(NewReference<ExplicitInverseRef>("clones", NewType<CollClass>("LocaleNDict"))),
		forms(NewReference<ExplicitInverseRef>("forms", NewType<CollClass>("FormNameDict"))),
		schema(NewReference<ExplicitInverseRef>("schema", NewType<Class>("Schema"))),
		translatableStrings(NewReference<ExplicitInverseRef>("translatableStrings", NewType<CollClass>("ConstantNDict")))
	{
		cloneOf->manual().bind(&Locale::cloneOf);
		clones->automatic().bind(&Locale::clones);
		forms->automatic().parent().bind(&Locale::forms);
		schema->manual().child().bind(&Locale::schema);
		translatableStrings->automatic().parent().bind(&Locale::translatableStrings);
	}
}