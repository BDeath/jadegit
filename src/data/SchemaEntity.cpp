#include <jadegit/data/SchemaEntity.h>
#include <jadegit/data/Attribute.h>
#include <jadegit/data/RootSchema/SchemaEntityMeta.h>
#include <jadegit/data/RootSchema/JadeExportedEntityMeta.h>

namespace JadeGit::Data
{
	std::map<SchemaEntity::Access, const char*> EnumStrings<SchemaEntity::Access>::data =
	{
		{ SchemaEntity::Access::Public, "public" },
		{ SchemaEntity::Access::ReadOnly, "readonly" },
		{ SchemaEntity::Access::Protected, "protected" },
		{ SchemaEntity::Access::Hidden, "hidden" },
		{ SchemaEntity::Access::Locale_Local, "local" },
		{ SchemaEntity::Access::Locale_Inherited, "inherited" }
	};

	template Value<SchemaEntity::Access>;

	SchemaEntity::SchemaEntity(Object* parent, const Class* dataClass, const char* name) : Entity(parent, dataClass, name) {};

	bool SchemaEntity::isImported() const
	{
		return dynamic_cast<const JadeImportedEntity*>(this);
	}

	SchemaEntityMeta::SchemaEntityMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "SchemaEntity", superclass),
		abstract(NewBoolean("abstract")),
		access(NewCharacter("access")),
		helpKeyword(NewString("helpKeyword")),
		name(NewString("name", 101)),
		subAccess(NewCharacter("subAccess")),
		text(NewString("text")),
		userText(NewString("userText")),
		wsdlName(NewString("wsdlName"))
	{
		abstract->bind(&SchemaEntity::abstract);
		access->bind(&SchemaEntity::access);
		name->unwritten().bind(&SchemaEntity::name);
		subAccess->bind(&SchemaEntity::subAccess);
		text->bind(&SchemaEntity::text);
		wsdlName->bind(&SchemaEntity::wsdlName);
	}

	JadeExportedEntityMeta::JadeExportedEntityMeta(RootSchema& parent, const SchemaEntityMeta& superclass) : RootClass(parent, "JadeExportedEntity", superclass) {}
}