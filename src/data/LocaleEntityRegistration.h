#pragma once
#include <jadegit/data/Locale.h>
#include "EntityRegistration.h"

namespace JadeGit::Data
{
	template <typename TDerived>
	class LocaleEntityRegistration : public EntityRegistration<TDerived, Locale>
	{
	public:
		using EntityRegistration<TDerived, Locale>::EntityRegistration;
		
	protected:
		TDerived* Resolve(Locale* parent, const std::string& name, bool shallow, bool inherit) const override
		{
			// Iterate through super locales
			Locale* locale = parent;
			while (locale)
			{
				// Attempt standard load
				if (TDerived* entity = EntityRegistration<TDerived, Locale>::Resolve(locale, name, shallow, inherit))
					return entity;

				if (!inherit)
					break;

				// Attempt standard load from base locale
				locale->Load();
				if (Locale* base = locale->cloneOf)
					if (TDerived* entity = EntityRegistration<TDerived, Locale>::Resolve(base, name, shallow, inherit))
						return entity;

				locale = locale->getSuperLocale();
			}

			// Locale entity doesn't exist
			return nullptr;
		}
	};
}