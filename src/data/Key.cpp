#include <jadegit/data/Key.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ExternalKeyMeta.h>
#include <jadegit/data/RootSchema/MemberKeyMeta.h>
#include "ObjectRegistration.h"

namespace JadeGit::Data
{
	static ObjectRegistration<ExternalKey, CollClass> externalKey("ExternalKey");
	static ObjectRegistration<MemberKey, CollClass> memberKey("MemberKey");

	Key::Key(CollClass* parent, const Class* dataClass) : Object(parent, dataClass), collClass(parent)
	{
		collClass->keys.push_back(this);
	}

	ExternalKey::ExternalKey(CollClass* parent, const Class* dataClass, const char* name) : NamedObject(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::externalKey), name) {}

	MemberKey::MemberKey(CollClass* parent, const Class* dataClass) : Key(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::memberKey))
	{
	}

	KeyMeta::KeyMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "Key", superclass),
		caseInsensitive(NewBoolean("caseInsensitive")),
		descending(NewBoolean("descending")),
		sortOrder(NewInteger("sortOrder"))
	{
		caseInsensitive->bind(&Key::caseInsensitive);
		descending->bind(&Key::descending);
		sortOrder->bind(&Key::sortOrder);
	}

	ExternalKeyMeta::ExternalKeyMeta(RootSchema& parent, const KeyMeta& superclass) : RootClass(parent, "ExternalKey", superclass),
		length(NewInteger("length")),
		name(NewString("name", 101)),
		precision(NewCharacter("precision")),
		scaleFactor(NewCharacter("scaleFactor")),
		type(NewReference<ExplicitInverseRef>("type", NewType<Class>("Type")))
	{
		length->bind(&ExternalKey::length);
		name->unwritten().bind(&ExternalKey::name);
		precision->bind(&ExternalKey::precision);
		scaleFactor->bind(&ExternalKey::scaleFactor);
		type->bind(&ExternalKey::type);
	}

	MemberKeyMeta::MemberKeyMeta(RootSchema& parent, const KeyMeta& superclass) : RootClass(parent, "MemberKey", superclass),
		keyPath(NewReference<ExplicitInverseRef>("keyPath", NewType<CollClass>("PropertyColl"))),
		property(NewReference<ExplicitInverseRef>("property", NewType<Class>("Property")))
	{
		keyPath->manual().bind(&MemberKey::keyPath);
		property->manual().bind(&MemberKey::property);
	}
}