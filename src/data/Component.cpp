#include <jadegit/data/Component.h>
#include <jadegit/data/Assembly.h>
#include <assert.h>

namespace JadeGit::Data
{
	Component::Component(Assembly& parent) : parent(&parent)
	{
		assert(&parent == this);
	}

	Component::Component(Component* parent) : parent(parent)
	{
		assert(parent != this);
	}

	Assembly& Component::getAssembly() const
	{
		Component* parent = const_cast<Component*>(this);
		while (parent->parent != parent)
			parent = parent->parent;

		return *static_cast<Assembly*>(parent);
	}

	Component* Component::getParent() const
	{
		if (parent == this)
			return nullptr;

		return parent;
	}

	int Component::getLevel() const
	{
		int level = 0;

		const Component* parent = this;
		while (parent->parent != parent)
		{
			level++;
			parent = parent->parent;
		}

		return level;
	}
}