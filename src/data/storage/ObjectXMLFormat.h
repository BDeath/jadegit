#pragma once
#include "ObjectFileFormat.h"
#include <jadegit/data/Class.h>
#include <jadegit/data/EntityFactory.h>
#include <Epilog.h>
#include <Log.h>
#include <Singleton.h>
#include <tinyxml2.h>
#include <xml/Exception.h>
#include <assert.h>

namespace JadeGit::Data
{
	using namespace std;
	using namespace tinyxml2;

	class XMLFileElement : public FileElement
	{
	public:
		using FileElement::FileElement;

	};

	class XMLFile : public XMLDocument, public XMLFileElement
	{
	public:
		XMLFile(const File& file) : XMLDocument(true, Whitespace::PEDANTIC_WHITESPACE), XMLFileElement(this->file, parse(file)), file(file) {}

	protected:
		File file;

		XMLElement* parse(const File& file)
		{
			XML_THROW((*this), XMLDocument::Parse(file.read().c_str()));
			if (auto root = RootElement())
				return root;

			throw runtime_error("Missing root element");
		}
	};

	class ObjectXMLFormat : public ObjectFileFormat, public Singleton<ObjectXMLFormat>
	{
	public:
		const std::filesystem::path extension() const final
		{
			return ".jox";
		}

		Entity* load(Assembly& assembly, const File& file, bool shallow, bool strict, queue<future<void>>& tasks) const final
		{
			auto path = file.path();
			if (path.extension() != extension())
				return nullptr;

			LOG_TRACE("Loading file [" << path.generic_string() << "] (enter)");

			// Parse file
			auto element = make_unique<XMLFile>(file);

			// Instantiate entity
			Entity* result = EntityFactory::Get().load(element->type(), &assembly, path);

			// Load entity
			load(*result, *element, shallow, strict, tasks);

			LOG_TRACE("Loaded file [" << path.generic_string() << "] (exit)");
			return result;
		}

		void load(Object& object, const FileElement& source, bool shallow, bool strict, queue<future<void>>& tasks) const
		{
			static thread_local int depth = -1;

			depth++;
			Epilog epilog([&]() { depth--; });

			// Performing initial load if object not already in a shallow state
			bool initial = !object.isShallow();

			// Put object into a loading state
			LOG_TRACE(std::string(depth, '\t') << "Loading " << &object << (shallow ? " (shallow)" : ""));
			object.loading(shallow);

			// Load header (key details)
			loadHeader(object, source);

			// Load properties & children
			loadBody(object, source, initial, shallow, strict, tasks);

			// Put object into a loaded state
			object.loaded(shallow, strict, tasks);
			tasks.push(async(launch::deferred, static_cast<void(Object::*)(bool)>(&Object::loaded), &object, shallow));
			LOG_TRACE(std::string(depth, '\t') << "Loaded " << &object << (shallow ? " (shallow)" : ""));
		}

		void loadHeader(Object& object, const FileElement& source) const
		{
			// TODO: Refactor using property metadata tagged as being header values
			object.LoadHeader(source);
		}

		void loadBody(Object& object, const FileElement& source, bool initial, bool shallow, bool strict, queue<future<void>>& tasks) const
		{
			auto dataClass = object.dataClass;
			assert(dataClass);

			const XMLElement* element = static_cast<const XMLElement*>(source)->FirstChildElement();
			while (element)
			{
				auto name = element->Name();

				// Load child (denoted by leading uppercase character which classes must have)
				if (isupper(name[0]))
				{
					// Suppress loading child objects during shallow load
					if (!shallow)
					{
						if (auto child = ObjectFactory::Get().load(name, &object, FileElement(source.file, element)))
							load(*child, FileElement(source.file, element), false, strict, tasks);
					}
				}
				// Queue property load
				else
				{
					if (const Property* property = dataClass->tryGetProperty(name))
					{
						property->LoadFor(object, element, initial, shallow, strict, tasks);
					}
					else
						LOG_WARNING("Ignored unknown property element during load: " << dataClass->GetName() << "::" << name);
				}

				// Get next element to load
				element = element->NextSiblingElement();
			}

			// TODO: Refactor signature specific behaviour, perhaps supply via Object::loaded hook?
			if (!shallow) object.LoadBody(source);
		}

		void write(const Object& object, File& file) const final
		{
			// Write object to document
			XMLDocument document;
			XMLElement* root = nullptr;
			object.Write(document, root, nullptr, false);

			// Print document to file
			XMLPrinter printer;
			document.Print(&printer);
			file.write(string_view(printer.CStr()));
		}
	};
}