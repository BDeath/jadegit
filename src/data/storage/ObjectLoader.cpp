#include "ObjectLoader.h"
#include "ObjectFileFormat.h"
#include "ObjectFileStorage.h"
#include <jadegit/data/Assembly.h>
#include <jadegit/vfs/FileSystem.h>
#include <Epilog.h>

using namespace std;
using namespace std::filesystem;

namespace JadeGit::Data
{
	ObjectLoader::ObjectLoader(const ObjectFileStorage& storage) : storage(storage)
	{
		// Set dogfood flag if loading schemas from own repository
		dogfood = storage.fs.open(path("schemas/JadeGitSchema").replace_extension(storage.format->extension())).exists();
	}

	void ObjectLoader::inferred(Entity& entity)
	{
		// Invoke callback for entity inferred
		if (!callbacks.empty())
		{
			auto callback = *callbacks.top();
			tasks.push(async(launch::deferred, [callback](Entity* entity) { if (entity->isInferred()) callback(entity); }, &entity));
		}
	}

	Entity* ObjectLoader::load(path path, bool shallow, bool strict)
	{
		path.replace_extension(storage.format->extension());

		// Handle deferred load
		if (!shallow && !deferred.empty())
		{
			auto it = deferred.find(path);
			if (it != deferred.end())
				return it->second.get();
		}

		// Throw exception for requests to load files outside scope of assembly
		if (!path.is_relative())
			throw logic_error("Invalid path");

		File file = storage.fs.open(path);
		if (file.exists())
			return load(file, shallow, strict, nullptr);

		return nullptr;
	}

	bool ObjectLoader::load(IProgress* progress)
	{
		if (deferred.empty())
			return true;

		for (auto& future : deferred)
		{
			future.second.get();

			if (progress && !progress->step())
				return false;
		}

		deferred.clear();
		return true;
	}

	Entity* ObjectLoader::load(const File& file, bool shallow, bool strict, const function<void(Entity* entity)>& callback)
	{
		auto path = file.path();

		try
		{
			depth++;
			Epilog epilog([&]() { depth--; });

			// Reset task queue & callbacks stack
			if (depth == 1)
			{
				tasks = {};
				callbacks = {};
			}

			// Push callback onto stack for inferred entities
			if (callback)
				callbacks.push(&callback);

			// Load major entity
			Entity* result = storage.format->load(storage.assembly, file, shallow, strict, tasks);

			if (result)
			{
				// Verify entity name matches filename (without extension)
				if (path.stem() != result->GetName())
					throw runtime_error("Entity name doesn't match filename");

				// Invoke callback for entity loaded
				if (callback) tasks.push(async(launch::deferred, callback, result));

				// Flush task queue
				if (depth == 1)
				{
					while (!tasks.empty())
					{
						tasks.front().get();
						tasks.pop();
					}
				}
			}

			// Pop callback
			if (callback)
				callbacks.pop();

			return result;
		}
		catch (...)
		{
			throw_with_nested(runtime_error(format("Load failed [{}]", path.generic_string())));
		}
	}

	void ObjectLoader::queue(const File& file, bool strict, function<void(Entity* entity)> callback)
	{
		assert(file.vfs() == &storage.fs);

		// Suppress attempts to load test files from own repository
		if (dogfood)
		{
			auto path = file.path();
			while (path.has_parent_path())
				path = path.parent_path();

			// Ignore test files, and for performance include/src files
			if (path == "test" || path == "include" || path == "src")
				return;
		}

		// Setup deferred load
		deferred.insert(make_pair(file.path(), async(launch::deferred, [=]() { return load(file, false, strict, callback); }).share()));
	}
}