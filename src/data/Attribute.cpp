#include <jadegit/data/Attribute.h>
#include <jadegit/data/ExternalClass.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema.h>
#include "FeatureRegistration.h"

namespace JadeGit::Data
{
	static PropertyRegistration<CompAttribute> compAttribute("CompAttribute");
	static PropertyRegistration<JadeDynamicCompAttribute> dynamicCompAttribute("JadeDynamicCompAttribute");
	static PropertyRegistration<PrimAttribute> primAttribute("PrimAttribute");
	static PropertyRegistration<ExternalPrimAttribute, ExternalClass> externalPrimAttribute("ExternalPrimAttribute");
	static PropertyRegistration<JadeDynamicPrimAttribute> dynamicPrimAttribute("JadeDynamicPrimAttribute");

	Attribute::Attribute(Class* parent, const Class* dataClass, const char* name, Type* type) : Property(parent, dataClass, name, type) {}

	int Attribute::GetLength() const
	{
		if (length)
			return length;

		return Property::GetLength();
	}

	CompAttribute::CompAttribute(Class* parent, const Class* dataClass, const char* name, Type* type) : Attribute(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::compAttribute, "CompAttribute"), name, type) {}

	void CompAttribute::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	AnyValue* CompAttribute::InstantiateValue(Object& object) const
	{
		return type->CreateValue(object, *this, true);
	}

	JadeDynamicCompAttribute::JadeDynamicCompAttribute(Class* parent, const Class* dataClass, const char* name) : CompAttribute(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeDynamicCompAttribute), name) {}

	PrimAttribute::PrimAttribute(Class* parent, const Class* dataClass, const char* name, Type* type, int length) : Attribute(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::primAttribute, "PrimAttribute"), name, type)
	{
		this->length = length;
	}

	void PrimAttribute::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	AnyValue* PrimAttribute::InstantiateValue(Object& object) const
	{
		return type->CreateValue(object, *this, false);
	}

	ExternalPrimAttribute::ExternalPrimAttribute(ExternalClass* parent, const Class* dataClass, const char* name) : PrimAttribute(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::externalPrimAttribute), name) {}

	JadeDynamicPrimAttribute::JadeDynamicPrimAttribute(Class* parent, const Class* dataClass, const char* name) : PrimAttribute(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeDynamicPrimAttribute), name) {}

	AttributeMeta::AttributeMeta(RootSchema& parent, const PropertyMeta& superclass) : RootClass(parent, "Attribute", superclass),
		length(NewInteger("length"))
	{
		length->bind(&Attribute::length);
	}

	CompAttributeMeta::CompAttributeMeta(RootSchema& parent, const AttributeMeta& superclass) : RootClass(parent, "CompAttribute", superclass) {}

	JadeDynamicCompAttributeMeta::JadeDynamicCompAttributeMeta(RootSchema& parent, const CompAttributeMeta& superclass) : RootClass(parent, "JadeDynamicCompAttribute", superclass),
		dynamicPropertyCluster(NewReference<ExplicitInverseRef>("dynamicPropertyCluster", NewType<Class>("JadeDynamicPropertyCluster")))
	{
		dynamicPropertyCluster->bind(&JadeDynamicCompAttribute::dynamicPropertyCluster);
	}

	PrimAttributeMeta::PrimAttributeMeta(RootSchema& parent, const AttributeMeta& superclass) : RootClass(parent, "PrimAttribute", superclass),
		precision(NewCharacter("precision")),
		scaleFactor(NewCharacter("scaleFactor")),
		xmlType(NewString("xmlType"))
	{
		precision->bind(&PrimAttribute::precision);
		scaleFactor->bind(&PrimAttribute::scaleFactor);
		xmlType->bind(&PrimAttribute::xmlType);
	}

	ExternalPrimAttributeMeta::ExternalPrimAttributeMeta(RootSchema& parent, const PrimAttributeMeta& superclass) : RootClass(parent, "ExternalPrimAttribute", superclass),
		externalSchemaMap(NewReference<ExplicitInverseRef>("externalSchemaMap", NewType<Class>("ExternalAttributeMap")))
	{
		externalSchemaMap->automatic().bind(&ExternalPrimAttribute::externalSchemaMap);
	}

	JadeDynamicPrimAttributeMeta::JadeDynamicPrimAttributeMeta(RootSchema& parent, const PrimAttributeMeta& superclass) : RootClass(parent, "JadeDynamicPrimAttribute", superclass),
		dynamicPropertyCluster(NewReference<ExplicitInverseRef>("dynamicPropertyCluster", NewType<Class>("JadeDynamicPropertyCluster")))
	{
		dynamicPropertyCluster->bind(&JadeDynamicPrimAttribute::dynamicPropertyCluster);
	}
}