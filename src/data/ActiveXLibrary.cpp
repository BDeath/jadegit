#include <jadegit/data/ActiveXLibrary.h>
#include <jadegit/data/ActiveXClass.h>
#include <jadegit/data/ActiveXAttribute.h>
#include <jadegit/data/ActiveXConstant.h>
#include <jadegit/data/ActiveXMethod.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/Schema.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ActiveXClassMeta.h>
#include <jadegit/data/RootSchema/ActiveXAttributeMeta.h>
#include <jadegit/data/RootSchema/ActiveXConstantMeta.h>
#include <jadegit/data/RootSchema/ActiveXMethodMeta.h>
#include <jadegit/data/RootSchema/ActiveXInterfaceMeta.h>
#include <jadegit/data/RootSchema/ActiveXLibraryMeta.h>
#include "EntityRegistration.h"

using namespace std;

namespace JadeGit::Data
{
	const std::filesystem::path ActiveXLibrary::subFolder("external-components");

	DEFINE_OBJECT_CAST(ActiveXClass)
	DEFINE_OBJECT_CAST(ActiveXFeature)

	static EntityRegistration<ActiveXLibrary, Schema> activeXLibrary("ActiveXLibrary", "external component", &Schema::activeXLibraries);
	static EntityRegistration<ActiveXClass, ActiveXLibrary> activeXClass("ActiveXClass", "external component class", &ActiveXLibrary::classes);
	static EntityRegistration<ActiveXAttribute, ActiveXClass> activeXAttribute("ActiveXAttribute", "external component attribute", mem_fn(&ActiveXClass::getAttribute));
	static EntityRegistration<ActiveXConstant, ActiveXClass> activeXConstant("ActiveXConstant", "external component constant", mem_fn(&ActiveXClass::getConstant));
	static EntityRegistration<ActiveXMethod, ActiveXClass> activeXMethod("ActiveXMethod", "external component method", mem_fn(&ActiveXClass::getMethod));

	template ObjectValue<Array<ActiveXClass*>, &ActiveXLibraryMeta::coClasses>;
	template ObjectValue<Schema* const, &ActiveXLibraryMeta::_schema>;

	ActiveXLibrary::ActiveXLibrary(Schema& parent, const Class* dataClass, const char* name) : MajorEntity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::activeXLibrary), name),
		schema(&parent)
	{
	}

	void ActiveXLibrary::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	Class* ActiveXLibrary::getBaseClass() const
	{
		auto activeXClass = classes.Get(name);
		return activeXClass ? static_cast<Class*>(activeXClass->baseClass) : nullptr;
	}

	template ObjectValue<ActiveXLibrary* const, &ActiveXClassMeta::activeXLibrary>;
	template ObjectValue<Class*, &ActiveXClassMeta::baseClass>;

	ActiveXClass::ActiveXClass(ActiveXLibrary& parent, const Class* dataClass, const char* name) : Entity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::activeXClass), name),
		activeXLibrary(&parent)
	{
	}

	void ActiveXClass::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	template <class TActiveXFeature, class TFeature>
	TActiveXFeature* getFeature(const ActiveXClass& cls, const std::string& name)
	{
		if (!cls.baseClass)
			return nullptr;

		if (auto feature = Entity::resolve<TFeature>(cls.baseClass, name, false))
			return dynamic_cast<TActiveXFeature*>(static_cast<ActiveXFeature*>(feature->activeXFeature));

		return nullptr;
	}

	ActiveXAttribute* ActiveXClass::getAttribute(const std::string& name) const
	{
		return getFeature<ActiveXAttribute, Property>(*this, name);
	}

	ActiveXConstant* ActiveXClass::getConstant(const std::string& name) const
	{
		return getFeature<ActiveXConstant, Constant>(*this, name);
	}

	ActiveXMethod* ActiveXClass::getMethod(const std::string& name) const
	{
		return getFeature<ActiveXMethod, Method>(*this, name);
	}

	const Class& ActiveXClass::getOriginal() const
	{
		return *baseClass;
	}

	void ActiveXClass::loaded(bool strict, queue<future<void>>& tasks)
	{
		Entity::loaded(strict, tasks);

		// Resolve related class
		baseClass = &Entity::resolve<Class&>(*this, name);
	}

	template ObjectValue<Feature* const, &ActiveXFeatureMeta::feature>;

	ActiveXFeature::ActiveXFeature(ActiveXClass& parent, const Class* dataClass, const char* name) : Entity(parent, dataClass, name)
	{
	}

	const Feature& ActiveXFeature::getOriginal() const
	{
		return *feature;
	}

	const ActiveXClass& ActiveXFeature::getParent() const
	{
		return *static_cast<ActiveXClass*>(parent);
	}

	ActiveXAttribute::ActiveXAttribute(ActiveXClass& parent, const Class* dataClass, const char* name) : ActiveXFeature(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::activeXAttribute), name)
	{
	}

	void ActiveXAttribute::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	void ActiveXAttribute::loaded(bool strict, queue<future<void>>& tasks)
	{
		ActiveXFeature::loaded(strict, tasks);

		// Resolve related feature
		tasks.push(async(launch::deferred, [this]() { feature = &Entity::resolve<Property&>(getParent().baseClass, name); }));
	}

	ActiveXConstant::ActiveXConstant(ActiveXClass& parent, const Class* dataClass, const char* name) : ActiveXFeature(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::activeXConstant), name)
	{
	}

	void ActiveXConstant::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	void ActiveXConstant::loaded(bool strict, queue<future<void>>& tasks)
	{
		ActiveXFeature::loaded(strict, tasks);

		// Resolve related feature
		tasks.push(async(launch::deferred, [this]() { feature = &Entity::resolve<Constant&>(getParent().baseClass, name); }));
	}

	ActiveXMethod::ActiveXMethod(ActiveXClass& parent, const Class* dataClass, const char* name) : ActiveXFeature(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::activeXMethod), name)
	{
	}

	void ActiveXMethod::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	void ActiveXMethod::loaded(bool strict, queue<future<void>>& tasks)
	{
		ActiveXFeature::loaded(strict, tasks);

		// Resolve related feature
		tasks.push(async(launch::deferred, [this]() { feature = &Entity::resolve<Method&>(getParent().baseClass, name); }));
	}

	ActiveXClassMeta::ActiveXClassMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "ActiveXClass", superclass),
		activeXLibrary(NewReference<ExplicitInverseRef>("activeXLibrary", NewType<Class>("ActiveXLibrary"))),
		activeXName(NewString("activeXName", 255)),
		baseClass(NewReference<ExplicitInverseRef>("baseClass", NewType<Class>("Class"))),
		defaultEventInterfaceGuid(NewBinary("defaultEventInterfaceGuid", 16)),
		defaultInterfaceGuid(NewBinary("defaultInterfaceGuid", 16)),
		dotNetName(NewStringUtf8("dotNetName")),
		executeMode(NewByte("executeMode")),
		guid(NewBinary("guid", 16)),
		isEventInterface(NewBoolean("isEventInterface")),
		key(NewBinary("key"))
	{
		activeXLibrary->manual().child().bind(&ActiveXClass::activeXLibrary);
		baseClass->manual().unwritten().bind(&ActiveXClass::baseClass);
	}

	ActiveXFeatureMeta::ActiveXFeatureMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "ActiveXFeature", superclass),
		feature(NewReference<ExplicitInverseRef>("feature", NewType<Class>("Feature"))),
		memid(NewInteger("memid"))
	{
		feature->manual().unwritten().bind(&ActiveXFeature::feature);
	}

	ActiveXAttributeMeta::ActiveXAttributeMeta(RootSchema& parent, const ActiveXFeatureMeta& superclass) : RootClass(parent, "ActiveXAttribute", superclass) {}

	ActiveXConstantMeta::ActiveXConstantMeta(RootSchema& parent, const ActiveXFeatureMeta& superclass) : RootClass(parent, "ActiveXConstant", superclass),
		activeXName(NewString("activeXName", 256)) {}

	ActiveXMethodMeta::ActiveXMethodMeta(RootSchema& parent, const ActiveXFeatureMeta& superclass) : RootClass(parent, "ActiveXMethod", superclass) {}

	ActiveXInterfaceMeta::ActiveXInterfaceMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "ActiveXInterface", superclass) {}

	ActiveXLibraryMeta::ActiveXLibraryMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "ActiveXLibrary", superclass),
		_schema(NewReference<ExplicitInverseRef>("_schema", NewType<Class>("Schema"))),
		coClasses(NewReference<ExplicitInverseRef>("coClasses", NewType<CollClass>("ActiveXClassGuidDict"))),
		componentType(NewInteger("componentType")),
		guid(NewBinary("guid", 16)),
		helpFileName(NewString("helpFileName", 257)),
		status(NewInteger("status")),
		uuid(NewBinary("uuid", 16))
	{
		coClasses->automatic().parent().bind(&ActiveXLibrary::classes);
		guid->bind(&ActiveXLibrary::guid);
		_schema->manual().child().bind(&ActiveXLibrary::schema);
	}
}