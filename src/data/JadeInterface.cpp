#include <jadegit/data/JadeInterface.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/JadeInterfaceMeta.h>
#include <jadegit/data/RootSchema/JadeInterfaceMethodMeta.h>
#include <jadegit/data/CollClass.h>
#include "TypeRegistration.h"
#include "FeatureRegistration.h"

using namespace std;

namespace JadeGit::Data
{
	DEFINE_OBJECT_CAST(JadeInterface)
	DEFINE_OBJECT_CAST(JadeInterfaceMethod)

	static TypeRegistration<JadeInterface, JadeImportedPackage, Schema> interface("JadeInterface", "interface", &JadeImportedPackage::interfaces, &Schema::interfaces);

	class JadeInterfaceMethodRegistration : public FeatureRegistration<JadeInterfaceMethod, JadeInterface>
	{
	public:
		JadeInterfaceMethodRegistration() : FeatureRegistration("JadeInterfaceMethod", "interface method", &JadeInterface::methods) {}

		using FeatureRegistration<JadeInterfaceMethod, JadeInterface>::Resolve;

		JadeInterfaceMethod* Resolve(JadeInterface* parent, const string& name, bool shallow, bool inherit) const override
		{
			// Attempt standard load
			if (JadeInterfaceMethod* method = FeatureRegistration<JadeInterfaceMethod, JadeInterface>::Resolve(parent, name, shallow, inherit))
				return method;

			// Resolve original interface
			if (auto originalInterface = &parent->getOriginal(); originalInterface != parent)
			{
				// Resolve original method
				if (auto originalMethod = Resolve(const_cast<JadeInterface*>(originalInterface), name, true, inherit))
				{
					// Instantiate local copy
					JadeInterfaceMethod* method = new JadeInterfaceMethod(parent, nullptr, name.c_str());
					method->inferred();

					return method;
				}
			}

			// Interface method doesn't exist
			return nullptr;
		}
	};
	static JadeInterfaceMethodRegistration interfaceMethod;

	template ObjectValue<Set<JadeInterface*>, &JadeInterfaceMeta::subinterfaces>;
	template ObjectValue<Array<JadeInterface*>, &JadeInterfaceMeta::superinterfaces>;

	JadeInterface::JadeInterface(Schema* parent, const Class* dataClass, const char* name) : Type(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeInterface), name)
	{
		schema = parent;
	}

	JadeInterface::JadeInterface(JadeImportedPackage* parent, const Class* dataClass, const char* name) : Type(parent, dataClass, name)
	{
	}

	void JadeInterface::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	AnyValue* JadeInterface::CreateValue() const
	{
		return new Value<Object*>();
	}

	AnyValue* JadeInterface::CreateValue(Object& object, const Property& property, bool exclusive) const
	{
		if(exclusive)
			throw invalid_argument("Storing exclusive interface values is not supported");

		return new Value<Object*>();
	}

	const JadeInterface& JadeInterface::getOriginal() const
	{
		return static_cast<const JadeInterface&>(Type::getOriginal());
	}

	const JadeInterface& JadeInterface::getRootType() const
	{
		return static_cast<const JadeInterface&>(Type::getRootType());
	}

	void JadeInterface::LoadFor(Object &object, const Property& property, const tinyxml2::XMLElement* source, bool strict, queue<future<void>>& tasks) const
	{
		throw logic_error("Loading interface values is not supported");
	}

	void JadeInterface::WriteFor(const Object &object, const Property& property, tinyxml2::XMLNode& parent) const
	{
		if (!property.GetValue(object).empty())
			throw logic_error("Writing interface values is not supported");
	}

	JadeInterfaceMethod::JadeInterfaceMethod(JadeInterface* parent, const Class* dataClass, const char* name) : Method(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeInterfaceMethod), name)
	{
	};

	void JadeInterfaceMethod::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	const JadeInterfaceMethod& JadeInterfaceMethod::getOriginal() const
	{
		auto type = static_cast<Type*>(schemaType);
		auto& originalType = type->getOriginal();

		// Receiver is original if parent type is original
		if (type == &originalType)
			return *this;

		// Lookup original
		auto original = originalType.methods.Get<JadeInterfaceMethod>(name);

		if (!original)
			throw logic_error("Missing original interface method");

		return *original;
	}

	bool JadeInterfaceMethod::isCopy() const
	{
		// Interface method must be a copy when parent interface is
		return schemaType->isCopy();
	}

	JadeInterfaceMeta::JadeInterfaceMeta(RootSchema& parent, const TypeMeta& superclass) : RootClass(parent, "JadeInterface", superclass),
		exportedInterfaceRefs(NewReference<ExplicitInverseRef>("exportedInterfaceRefs", NewType<CollClass>("JadeExportedInterfaceSet"))),
		genericArgumentEncoding(NewString(Version(20, 0, 1), "genericArgumentEncoding", 512)),
		genericArguments(NewReference<ExplicitInverseRef>(Version(20, 0, 1), "genericArguments", NewType<CollClass>("TypeColl"))),
		implementorClasses(NewReference<ExplicitInverseRef>("implementorClasses", NewType<CollClass>("ClassNDict"))),
		role(NewInteger(Version(20, 0, 1), "role")),
		subinterfaces(NewReference<ExplicitInverseRef>("subinterfaces", NewType<CollClass>("JadeInterfaceNDict"))),
		superinterfaces(NewReference<ExplicitInverseRef>("superinterfaces", NewType<CollClass>("JadeInterfaceNDict"))),
		templateInterface(NewReference<ExplicitInverseRef>(Version(20, 0, 1), "templateInterface", NewType<Class>("JadeInterface")))
	{
		exportedInterfaceRefs->automatic();
		implementorClasses->automatic();
		subinterfaces->automatic().bind(&JadeInterface::subinterfaces);
		superinterfaces->manual().bind(&JadeInterface::superinterfaces);
	}

	JadeInterfaceMethodMeta::JadeInterfaceMethodMeta(RootSchema& parent, const MethodMeta& superclass) : RootClass(parent, "JadeInterfaceMethod", superclass),
		interfaceImplementors(NewReference<ExplicitInverseRef>("interfaceImplementors", NewType<CollClass>("MethodColl")))
	{
		interfaceImplementors->automatic();
	}
}