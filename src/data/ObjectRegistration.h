#pragma once
#include <jadegit/data/ObjectFactory.h>
#include <jadegit/data/Class.h>
#include <jadegit/data/File.h>
#include <type_traits>

namespace JadeGit::Data
{
	class Class;

	template<class TDerived, class TParent, class TInterface = ObjectFactory::Registration>
	class ObjectRegistration : public TInterface
	{
		static_assert(std::is_base_of<Object, TDerived>(), "Derived component is not an object");

	public:
		ObjectRegistration(const char* key) : key(key)
		{
			ObjectFactory::Get().Register<TDerived>(key, this);
		}

	protected:
		const char* const key;

		template<typename TParent, typename... Args>
		TDerived* CreateInstance(TParent* parent, Args... args) const
		{
			if constexpr (std::is_constructible_v<TDerived, TParent*, Args...>)
				return new TDerived(parent, args...);

			if constexpr (std::is_constructible_v<TDerived, TParent&, Args...>)
				return new TDerived(*parent, args...);

			throw std::logic_error("Cannot create " + std::string(key));
		}

		TDerived* Create(Component* parent, const Class* dataClass) const override
		{
			assert(dynamic_cast<TParent*>(parent));
			return CreateInstance<TParent, const Class*>(dynamic_cast<TParent*>(parent), dataClass);
		}

		// Basic resolution attempts to resolve object from origin ancestors
		TDerived* Resolve(const Component* origin, bool expected) const override
		{
			Component* parent = const_cast<Component*>(origin);
			while (parent)
			{
				if (TDerived* result = dynamic_cast<TDerived*>(parent))
					return result;

				parent = parent->getParent();
			}

			if (expected)
				throw std::runtime_error("Failed to resolve " + std::string(key));

			return nullptr;
		}

		// Resolution needs to be re-implemented to use source details to resolve object
		std::function<Object* ()> resolver(const Component* origin, const tinyxml2::XMLElement* source, bool expected, bool shallow, bool inherit) const override
		{
			// Default to basic resolution
			return [=]() { return Resolve(origin, expected); };
		}

		TDerived* load(Component* origin, const Class* dataClass, const FileElement& source) const override
		{
			// Resolve parent
			TParent* parent = ObjectFactory::Get().Resolve<TParent>(origin);
			assert(parent);

			// Create new
			return Create(parent, dataClass);
		}

		AnyValue* CreateValue() const override
		{
			return new Value<TDerived*>();
		}

		AnyValue* CreateValue(Object& parent, const Property& property, const Class* dataClass, bool exclusive) const final
		{
			if (!exclusive)
			{
				if (dynamic_cast<const ExplicitInverseRef*>(&property))
					return new ObjectValue<TDerived*, nullptr, ExplicitInverseRef>(parent, static_cast<const ExplicitInverseRef&>(property));
				else
					return new Value<TDerived*>();
			}

			if constexpr (std::is_base_of_v<Collection, TDerived>)
			{
				if (auto reference = dynamic_cast<const ExplicitInverseRef*>(&property))
				{
					if constexpr (is_object_reference<typename TDerived::MemberType>::value)
					{
						return new ObjectValue<TDerived, nullptr, ExplicitInverseRef>(parent, *reference, dataClass);
					}
				}
				else
				{
					return new ObjectValue<TDerived, nullptr, Property>(parent, property, dataClass);
				}
			}

			throw std::logic_error(std::format("Cannot create exclusive sub-object [{}]", key));
		}
	};
}