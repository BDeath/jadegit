#include <jadegit/data/Assembly.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/EntityFactory.h>
#include <jadegit/vfs/File.h>
#include <jadegit/vfs/FileSystem.h>
#include <stack>
#include "config/Config.h"
#include "storage/ObjectFileStorage.h"

using namespace std;

namespace JadeGit::Data
{
	Assembly::Assembly(const FileSystem& fs, const Version& platformVersion) : Component(*this),
		schemas(nullptr)
	{
		config = make_unique<Config>(fs, ".jadegit");
		storage = make_unique<ObjectFileStorage>(*this, fs);

		// Get minimum platform version
		Version minimumPlatformVersion = config->get<Version>(toml::path("core.platform"));

		// Setup RootSchema using minimum if configured and greater than current version supplied
		rootSchema = new RootSchema(*this, minimumPlatformVersion > platformVersion ? minimumPlatformVersion : platformVersion);
	}

	thread_local bool suppressInverseMaintenance = false;
	bool inverseMaintenanceSuppressed()
	{
		return suppressInverseMaintenance;
	}

	Assembly::~Assembly()
	{
		// Suppress inverse maintenance during clean-up
		suppressInverseMaintenance = true;
		
		// Clean-up metadata
		if(rootSchema)
			delete rootSchema;

		// Clean-up schemas
		auto it = schemas.begin();
		while (it != schemas.end())
		{
			delete it->second;		// Removes self from parent collection
			it = schemas.begin();
		}

		// Reinstate inverse maintenance during clean-up
		suppressInverseMaintenance = false;
	}

	const Config& Assembly::getConfig() const
	{
		return *config;
	}

	const ObjectFileStorage& Assembly::getStorage() const
	{
		return *storage;
	}

	Entity* Assembly::Load(std::filesystem::path path, bool shallow)
	{
		return storage->load(path, shallow);
	}

	void Assembly::save(bool unload)
	{
		// Save configuration
		config->save();

		// Save each schema, invoking prelude pass initially to check for unknown entities
		for (int i = 0; i <= 1; i++)
		{
			for (auto schema : schemas)
				schema.second->Save(i == 0);
		}

		storage->flush();

		// Unload schemas
		if (unload) this->unload();
	}

	void Assembly::unload()
	{
		if (schemas.empty())
			return;

		// Clean up non-static schemas
		stack<Schema*> stack;
		stack.push(*rootSchema);
		while (!stack.empty())
		{
			auto top = stack.top();
			if (top->subschemas.empty())
			{
				if (!top->isStatic())
					Object::dele(top);

				stack.pop();
			}
			else
			{
				if (top->isStatic())
					stack.pop();

				for (auto& subschema : top->subschemas)
					stack.push(subschema);
			}
		}
	}
}