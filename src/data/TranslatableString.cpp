#include <jadegit/data/TranslatableString.h>
#include <jadegit/data/Schema.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/TranslatableStringMeta.h>
#include "LocaleEntityRegistration.h"

namespace JadeGit::Data
{
	static LocaleEntityRegistration<TranslatableString> registrar("TranslatableString", &Locale::translatableStrings);

	template ObjectValue<Locale* const, &TranslatableStringMeta::locale>;

	TranslatableString::TranslatableString(Locale* parent, const Class* dataClass, const char* name) : Constant(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::translatableString), name),
		locale(parent)
	{
	}

	void TranslatableString::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	const Entity* TranslatableString::GetQualifiedParent() const
	{
		return locale->schema;
	}

	TranslatableStringMeta::TranslatableStringMeta(RootSchema& parent, const ConstantMeta& superclass) : RootClass(parent, "TranslatableString", superclass),
		locale(NewReference<ExplicitInverseRef>("locale", NewType<Class>("Locale")))
	{
		locale->manual().child().bind(&TranslatableString::locale);
	}
}