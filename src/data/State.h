#pragma once
#include <Singleton.h>

namespace JadeGit::Data
{
	class Object;
	class Entity;
	class FileElement;

	class State
	{
	public:
		bool isCreated() const;
		bool isCurrent() const;
		bool isDirty() const;
		bool isLoading() const;
		bool isModified() const;
		bool isShallow() const;

		virtual void load(Entity& entity) const;
		virtual void loading(Object* object, bool shallow) const;
		virtual void loaded(Object* object, bool shallow, bool resolved) const;
		virtual void save(Entity* entity, bool prelude) const;

		// Transition to new state when prompted, if applicable to current state
		virtual void dirty(Entity& entity, bool deep) const;
		virtual void dirty(Object& object) const;
		virtual void implied(Entity* entity) const {}
		virtual void inferred(Entity& entity) const {}
		virtual void created(Entity* entity, const char* guid = nullptr) const;
		virtual void modified(Object* object) const;

		// Cleans up dirty objects
		virtual void clean(Object* object) const;

	protected:
		State() {}
		virtual ~State() {}

		void saveChildren(const Object* parent, bool prelude) const;

		// Hook invoked when state is set
		virtual void set(Object* object) const {};

		// Helper methods for manipulating object, without needing to declare all states as a friend
		const State& get(const Object& object) const;
		void set(Object* object, const State* state) const;

		// When state transition confirms existence, object is moved to end of parent collection (maintaining correct order for output)
		void confirm(Object* object) const;
	};

	// Static state for meta-objects created internally
	class StaticState : public State, public Singleton<StaticState>
	{
	public:
		void modified(Object* object) const final {}
		void load(Entity& entity) const final {}
		void save(Entity* entity, bool prelude) const final {}
	};

	// Initial default state
	class InitialState : public State, public Singleton<InitialState>
	{
	public:
		// New object cannot be marked as dirty
		void dirty(Entity& entity, bool deep) const final {};
		void dirty(Object& object) const final {};
		
		// New entity can be implied
		void implied(Entity* entity) const final;

		// New entity can be inferred
		void inferred(Entity& entity) const final;

		// New entity can be explicitly created with guid
		void created(Entity* entity, const char* guid) const final;

		// New object can be modified, implying creation
		void modified(Object* object) const final;

		// Existing object may be loaded
		void loading(Object* object, bool shallow) const final;

		// No pending load to complete
		void load(Entity& entity) const final {}

		// Unless loaded or modified, entity is unknown and cannot be saved
		void save(Entity* entity, bool prelude) const final;
	};

	// Implied state, which is where an unknown entity has been instantiated based on reference from elsewhere implying existence, but that needs to be confirmed by load
	class ImpliedState : public State, public Singleton<ImpliedState>
	{
	public:
		// Implied object may be loaded
		void load(Entity& entity) const final;
		
		// Implied object may be loaded, confirming it's a known entity
		void loading(Object* object, bool shallow) const final;

		// Unless loaded, entity is unknown and cannot be saved
		void save(Entity* entity, bool prelude) const final;
	};

	// Inferred state, which is where an entity has been instantiated for operational reasons, but doesn't need to be saved (unless modified)
	class InferredState : public State, public Singleton<InferredState>
	{
	public:
		// Inferred object cannot be marked as dirty
		void dirty(Entity& entity, bool deep) const final {};
		void dirty(Object& object) const final {};

		// Inferred object can be modified, implying it should be created/saved
		void modified(Object* object) const final;

		// No pending load to complete (if there was, that'd mean it's a known entity, rather than inferred)
		void load(Entity& entity) const final {}

		// Inferred object may be loaded, confirming it's a known entity
		void loading(Object* object, bool shallow) const final;

		// Unless modified implying creation, entity does not need to be saved
		void save(Entity* entity, bool prelude) const final {}
	};
}