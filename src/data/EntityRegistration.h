#pragma once
#include <jadegit/data/EntityFactory.h>
#include <jadegit/data/EntityDict.h>
#include "ObjectRegistration.h"
#include <functional>
#include <type_traits>

namespace JadeGit::Data
{
	template<typename TDerived, class TParent, class... TParents>
	class EntityRegistration : public EntityRegistration<TDerived, TParents...>
	{
	public:
		using getter = std::function<TDerived* (TParent*, const std::string&)>;

		template <typename... Rest>
		EntityRegistration(const char* key, const char* alias, getter get, Rest... rest) : EntityRegistration<TDerived, TParents...>(key, rest...), get(std::move(get)) {}

		template <typename... Rest>
		EntityRegistration(const char* key, getter get, Rest... rest) : EntityRegistration(key, nullptr, get, rest...) {}

		template<typename TCollection, typename... Rest>
		EntityRegistration(const char* key, const char* alias, TCollection TParent::* collection, Rest... rest) : EntityRegistration(key, alias,
			[collection](TParent* parent, const std::string& name) {
				return (parent->*collection).Get<TDerived>(name);
			},
			rest...) {}

		template <typename TCollection, typename... Rest>
		EntityRegistration(const char* key, TCollection TParent::* collection, Rest... rest) : EntityRegistration(key, nullptr, collection, rest...) {}

		TDerived* lookup(TParent* parent, const std::string& name) const
		{
			return get(parent, name);
		}

		using EntityRegistration<TDerived, TParents...>::Resolve;

		TDerived* Resolve(const Component* origin, const QualifiedName* name, bool expected, bool shallow, bool inherit) const override
		{
			// Use basic resolution when no name has been specified
			if (!name)
				return Resolve(origin, expected);

			// Try resolve possible parent
			if (auto parent = EntityFactory::Get().Resolve<TParent>(origin, name->parent.get(), false, is_major_entity<TDerived>, inherit))
			{
				// Try resolve child
				if (auto result = Load(parent, name->name, shallow))
					return result;

				// Try resolve via next parent
				if (auto result = EntityRegistration<TDerived, TParents...>::Resolve(origin, name, false, shallow, inherit))
					return result;

				if (expected)
					throw std::runtime_error(std::format("Failed to resolve {} [{}]", this->alias, static_cast<std::string>(*name)));

				return nullptr;
			}

			// Resolve via next parent
			return EntityRegistration<TDerived, TParents...>::Resolve(origin, name, expected, shallow, inherit);
		}

	private:
		getter get;

		TDerived* Load(TParent* parent, const std::string& name, bool shallow) const
		{
			if (name.empty())
				throw std::invalid_argument(std::format("Missing {} name", this->alias));

			if constexpr (is_major_entity<TDerived>)
			{
				// Find & load existing
				if (TDerived* child = get(parent, name))
				{
					if (!shallow)
						static_cast<Entity*>(child)->Load();
					return child;
				}

				// Nothing to load if parent is static
				if (parent->isStatic())
					return nullptr;

				// Derive child path
				auto path = parent->path() / TDerived::subFolder / name;

				// Load child
				auto child = Entity::Load(parent, path, shallow);

				// Verify type
				if (child && !dynamic_cast<TDerived*>(child))
					throw std::runtime_error(std::format("Failed to load {} as expected [{}]", this->alias, path.generic_string()));

				return static_cast<TDerived*>(child);
			}
			else
			{
				// Load parent if required (may be in a shallow state)
				parent->Load();

				// Find & return embedded entity
				return lookup(parent, name);
			}
		}
	};

	std::string defaultEntityAlias(const std::string& key);

	template<class TDerived, class TParent>
	class EntityRegistration<TDerived, TParent> : protected ObjectRegistration<TDerived, TParent, EntityFactory::Registration>
	{
		static_assert(std::is_base_of<Entity, TDerived>(), "Derived component is not an entity");
		static_assert(std::is_abstract_v<TDerived> || std::is_constructible_v<TDerived, TParent*, const Class*, const char*> || std::is_constructible_v<TDerived, TParent&, const Class*, const char*>, "Derived entity cannot be constructed");

	public:
		using getter = std::function<TDerived* (TParent*, const std::string&)>;

		EntityRegistration(const char* key, const char* alias, getter get) : ObjectRegistration<TDerived, TParent, EntityFactory::Registration>(key), alias(std::move(alias ? std::string(alias) : defaultEntityAlias(key))), get(std::move(get))
		{
			EntityFactory::Get().Register<TDerived>(key, this);
		}

		EntityRegistration(const char* key, getter get) : EntityRegistration(key, nullptr, get) {}

		template<typename TCollection>
		EntityRegistration(const char* key, const char* alias, TCollection TParent::* collection) : EntityRegistration(key, alias,
			[collection](TParent* parent, const std::string& name) {
				return (parent->*collection).Get<TDerived>(name);
			}) {}

		template <typename TCollection>
		EntityRegistration(const char* key, TCollection TParent::* collection) : EntityRegistration(key, nullptr, collection) {}
		
		using ObjectRegistration<TDerived, TParent, EntityFactory::Registration>::Resolve;

		virtual TDerived* Resolve(TParent* parent, const std::string& name, bool shallow = true, bool inherit = false) const
		{
			return Load(parent, name, shallow);
		}

	protected:
		const std::string alias;

		TDerived* Create(Component* parent, const Class* dataClass, const char* name) const override
		{
			assert(dynamic_cast<TParent*>(parent));
			return this->CreateInstance<TParent, const Class*, const char*>(dynamic_cast<TParent*>(parent), dataClass, name);
		}

		TDerived* load(Component* origin, const Class* dataClass, const FileElement& source) const override
		{
			// Get name attribute
			auto name = source.header("name");
			if (!name) throw std::runtime_error("Missing " + alias + " name attribute");

			// Resolve parent
			TParent* parent = ObjectFactory::Get().Resolve<TParent>(origin);
			assert(parent);

			// Find existing
			if (TDerived* result = get(parent, name))
				return result;

			// Create new
			return this->CreateInstance<TParent, const Class*, const char*>(parent, dataClass, name);
		}

		TDerived* load(Assembly* assembly, const Class* dataClass, const std::filesystem::path& path) const override
		{
			// Resolve parent
			TParent* parent = Parent(assembly, path);
			assert(parent);

			// Derive entity name from path stem
			auto name = path.stem().string();

			// Return existing
			if (TDerived* result = get(parent, name))
				return result;

			// Create new
			return this->CreateInstance<TParent, const Class*, const char*>(parent, dataClass, name.c_str());
		}

		TDerived* lookup(TParent* parent, const std::string& name) const
		{
			return get(parent, name);
		}

		TDerived* Resolve(const Component* origin, const QualifiedName* name, bool expected, bool shallow, bool inherit) const override
		{
			// Use basic resolution when no name has been specified
			if (!name)
				return Resolve(origin, expected);

			// Resolve parent
			TParent* parent = EntityFactory::Get().Resolve<TParent>(origin, name->parent.get(), expected, is_major_entity<TDerived>, inherit);
			if (!parent)
				return nullptr;

			// Attempt to resolve from collection
			if (TDerived* result = Resolve(parent, name->name, shallow, inherit))
				return result;

			if (expected)
				throw std::runtime_error(std::format("Failed to resolve {} [{}]", alias, static_cast<std::string>(*name)));

			return nullptr;
		}

		std::function<Object* ()> resolver(const Component* origin, const tinyxml2::XMLElement* source, bool expected, bool shallow, bool inherit) const override
		{
			// Use basic resolution when qualified name hasn't been specified
			auto name = source->Attribute("name");
			if (!name)
				return [=]() { return Resolve(origin, expected); };

			// Resolve using qualified name (above)
			std::string path(name);
			return [=]()
			{ 
				QualifiedName qualifiedName(path);
				return Resolve(origin, &qualifiedName, expected, shallow, inherit);
			};
		}

		TDerived* Resolve(const Assembly* assembly, const std::filesystem::path& path, bool expected, bool shallow) const override
		{
			// Resolve parent
			TParent* parent = Parent(assembly, path, expected);
			if (!parent)
				return nullptr;

			// Derive entity name from path stem
			auto name = path.stem().string();

			// Attempt to resolve from collection
			if (TDerived* result = Resolve(parent, name.c_str(), shallow, false))
				return result;

			if (expected)
				throw std::runtime_error(std::format("Failed to resolve {} [{}]", alias, path.generic_string()));

			return nullptr;
		}

		template<class TParent = TParent>
		TParent* Parent(const Assembly* assembly, const std::filesystem::path& path, bool expected = true) const
		{
			// Get parent path
			std::filesystem::path parent_path = path.parent_path();

			if constexpr (!is_major_entity<TDerived>)
				throw std::runtime_error(std::format("{} entities must be embedded in parent file", this->key));
			else
			{
				// Verify & remove sub-folder used for this kind of entity
				std::filesystem::path subFolder = TDerived::subFolder;
				if (!subFolder.empty())
				{
					if (parent_path.stem() != subFolder)
						throw std::runtime_error(std::format("Unexpected path for {} [{}]", alias, path.generic_string()));

					parent_path = parent_path.parent_path();
				}
			}

			// Resolve parent
			return EntityFactory::Get().Resolve<TParent>(assembly, parent_path, expected, true);
		}

	private:
		getter get;

		TDerived* Load(TParent* parent, const std::string& name, bool shallow) const
		{
			if (name.empty())
				throw std::runtime_error(std::format("Missing {} name", alias));

			if constexpr (is_major_entity<TDerived>)
			{
				// Find & load existing
				if (TDerived* child = get(parent, name))
				{
					if (!shallow)
						static_cast<Entity*>(child)->Load();
					return child;
				}

				// Nothing to load if parent is static
				if (parent->isStatic())
					return nullptr;

				// Derive child path
				auto path = parent->path() / TDerived::subFolder / name;

				// Load child
				auto child = Entity::Load(parent, path, shallow);

				// Verify type
				if (child && !dynamic_cast<TDerived*>(child))
					throw std::runtime_error(std::format("Failed to load {} as expected [{}]", alias, path.generic_string()));

				return static_cast<TDerived*>(child);
			}
			else
			{
				// Load parent if required (may be in a shallow state)
				parent->Load();

				// Find & return embedded entity
				return lookup(parent, name);
			}
		}
	};
}