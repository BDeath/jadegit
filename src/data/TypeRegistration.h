#include "EntityRegistration.h"
#include <jadegit/data/Schema.h>
#include <jadegit/data/JadeImportedPackage.h>

namespace JadeGit::Data
{
	template<class TDerived, class... TParents>
	class TypeRegistration : protected EntityRegistration<TDerived, TParents...>
	{
	public:
		using EntityRegistration<TDerived, TParents...>::EntityRegistration;
		using EntityRegistration<TDerived, TParents...>::Resolve;

		TDerived* Resolve(Schema* parent, const std::string& name, bool shallow, bool inherit) const override
		{
			// Attempt standard load
			if (TDerived* type = EntityRegistration<TDerived, TParents...>::Resolve(parent, name, shallow, inherit))
				return type;

			// Resolve superschema
			if (Schema* superschema = parent->GetSuperSchema())
			{
				// Resolve superschema type
				if (TDerived* superschemaType = Resolve(superschema, name, true, inherit))
				{
					// Instantiate subschema copy
					TDerived* subschemaCopy = EntityFactory::Get().Create<TDerived>(typeid(*superschemaType), parent, name.c_str());

					// Check type constructor has resolved superschema type as expected
					assert(subschemaCopy->superschemaType == superschemaType);

					return subschemaCopy;
				}
			}

			// Type doesn't exist in schema hierarchy
			return nullptr;
		}
	};
}