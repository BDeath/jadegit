#include "EntityRegistration.h"

using namespace std;

namespace JadeGit::Data
{
	std::string defaultEntityAlias(const std::string& key)
	{
		std::string result = "";
		char last = 0;
		for (char const& c : key)
		{
			if (isupper(c))
			{
				if (last)
					result += last;

				last = c;
			}
			else
			{
				if (last)
				{
					if (!result.empty())
						result += " ";

					result += tolower(last);
					last = 0;
				}

				result += c;
			}				
		}

		if (last)
			result += last;

		return result;
	}
}