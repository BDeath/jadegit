#include <jadegit/data/Constant.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/Reference.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ConstantMeta.h>
#include "FeatureRegistration.h"

namespace JadeGit::Data
{
	static FeatureRegistration<Constant, Type> constant("Constant", &Type::constants);
	
	template ObjectValue<Set<Constant*>, &ConstantMeta::constantRefs>;
	template ObjectValue<Array<Constant*>, &ConstantMeta::constantUsages>;

	Constant::Constant(Type* parent, const Class* dataClass, const char* name) : Script(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::constant, "Constant"), name)
	{
		schemaType = parent;
	};

	Constant::Constant(Object* parent, const Class* dataClass, const char* name) : Script(parent, dataClass, name)
	{
	};

	void Constant::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	ConstantMeta::ConstantMeta(RootSchema& parent, const ScriptMeta& superclass) : RootClass(parent, "Constant", superclass),
		constantRefs(NewReference<ExplicitInverseRef>("constantRefs", NewType<CollClass>("ConstantSet"))),
		constantUsages(NewReference<ExplicitInverseRef>("constantUsages", NewType<CollClass>("ConstantColl"))),
		exportedConstantRefs(NewReference<ExplicitInverseRef>("exportedConstantRefs", NewType<CollClass>("JadeExportedConstantSet")))
	{
		// Group constants after class mappings, or in the case of global constants, categories
		subject->bracket(1);

		constantRefs->automatic().bind(&Constant::constantRefs);
		constantUsages->manual().bind(&Constant::constantUsages);
		exportedConstantRefs->automatic();
	}
}