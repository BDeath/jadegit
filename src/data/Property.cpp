#include <jadegit/data/Property.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema/PropertyMeta.h>
#include <Log.h>
#include "FeatureRegistration.h"

using namespace std;

namespace JadeGit::Data
{
	static PropertyRegistration<Property> property("Property");

	Property::Property(Class* parent, const Class* dataClass, const char* name, Type* type) : Feature(parent, dataClass, name)
	{
		schemaType = parent;
		this->type = type;
	}

	AnyValue& Property::getActualValue(const Object& object) const
	{
		return *getActualValue(object, true);
	}

	AnyValue* Property::getActualValue(const Object& object, bool instantiate) const
	{
		// Return value stored explicitly
		if (member)
			return const_cast<AnyValue*>(&member->value(object));

		// Return value stored dynamically
		auto iter = object.values.find(this);
		if (iter != object.values.end())
			return iter->second;

		if (!instantiate)
			return nullptr;

		// Instantiate value
		AnyValue* value = InstantiateValue(const_cast<Object&>(object));
		const_cast<Object&>(object).values[this] = value;
		return value;
	}

	int Property::GetLength() const
	{
		return type->GetDefaultLength();
	}

	Any Property::GetValue(const Object &object, bool instantiate) const
	{
		// Return copy of actual value, instantiating if required
		if (auto value = getActualValue(object, instantiate))
			return *value;

		// Return default value
		return *GetDefaultValue();
	}

	const AnyValue* Property::GetDefaultValue() const
	{
		// Setup default value if required
		if (!default_)
			const_cast<Property*>(this)->default_.reset(type->CreateValue());

		return default_.get();
	}

	AnyValue* Property::InstantiateValue() const
	{
		return type->CreateValue();
	}

	void Property::resetValue(Object& object) const
	{
		if (auto value = getActualValue(object, false))
		{
			// Reset to default value if defined
			if (default_)
				value->Set(*default_.get());
			else
				value->reset();

			// Flag object as being modified
			object.Modified();
		}
	}

	void Property::LoadFor(Object &object, const tinyxml2::XMLElement* source, bool initial, bool shallow, bool strict, queue<future<void>>& tasks) const
	{
		bool structural = isStructural();

		// Don't reload structural properties after initial load
		if (structural && !initial)
			return;

		// Don't load non-structural properties during shallow load
		if (!structural && shallow)
			return;

		LOG_TRACE("Loading property [" << name << "]");
		type->LoadFor(object, *this, source, strict, tasks);
	}

	void Property::WriteFor(const Object &object, tinyxml2::XMLNode& parent) const
	{
		try
		{
			if (WriteFilter())
				type->WriteFor(object, *this, parent);
		}
		catch (const runtime_error& e)
		{
			// TODO: May need to look at building up trail like the extract process to identify parent when object is not an entity
			auto entity = object.asEntity();
			auto trail = entity ? entity->GetQualifiedName() + "." + name : GetQualifiedName();
			throw runtime_error(string(e.what()) + ", while writing property [" + trail + "]");
		}
	}

	PropertyMeta::PropertyMeta(RootSchema& parent, const FeatureMeta& superclass) : RootClass(parent, "Property", superclass),
		embedded(NewBoolean("embedded")),
		exportedPropertyRefs(NewReference<ExplicitInverseRef>("exportedPropertyRefs", NewType<CollClass>("JadeExportedPropertySet"))),
		isHTMLProperty(NewBoolean("isHTMLProperty")),
		keyPathRefs(NewReference<ExplicitInverseRef>("keyPathRefs", NewType<CollClass>("MemberKeySet"))),
		keyRefs(NewReference<ExplicitInverseRef>("keyRefs", NewType<CollClass>("MemberKeySet"))),
		required(NewBoolean("required")),
		virtual_(NewBoolean("virtual")),
		webService(NewBoolean("webService")),
		xmlAttribute(NewBoolean("xmlAttribute")),
		xmlNillable(NewBoolean("xmlNillable"))
	{
		// Group properties after constants
		subject->bracket(2);

		embedded->bind(&Property::embedded);
		exportedPropertyRefs->automatic();
		isHTMLProperty->bind(&Property::isHTMLProperty);
		keyPathRefs->automatic();
		keyRefs->automatic();
		required->bind(&Property::required);
		virtual_->bind(&Property::virtual_);
		webService->bind(&Property::webService);
		xmlAttribute->bind(&Property::xmlAttribute);
		xmlNillable->bind(&Property::xmlNillable);
	}
}