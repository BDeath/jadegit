#include <jadegit/data/Array.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/Schema.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/CollectionMeta.h>
#include "ObjectRegistration.h"

namespace JadeGit::Data
{
	Collection::Collection(Object* parent, const Class* dataClass) : Object(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::collection)) {}

	CollectionMeta::CollectionMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "Collection", superclass)
	{
	}

	template<class TCollection>
	using CollectionRegistration = ObjectRegistration<TCollection, Object>;

	// Use an object array for all Btree subclasses
	static CollectionRegistration<Array<Object*>> btree("Btree");

	// Use a byte array for JadeBytes
	static CollectionRegistration<Array<Byte>> jadeBytes("JadeBytes");

	// Arrays
	static CollectionRegistration<Array<Binary>> binaryArray("BinaryArray");
	static CollectionRegistration<Array<bool>> booleanArray("BooleanArray");
	static CollectionRegistration<Array<Byte>> byteArray("ByteArray");
	static CollectionRegistration<Array<char>> characterArray("CharacterArray");
	static CollectionRegistration<Array<int>> dateArray("DateArray");
//	static CollectionRegistration<Array<Decimal>> decimalArray("DecimalArray");
	static CollectionRegistration<Array<std::string>> hugeStringArray("HugeStringArray");
	static CollectionRegistration<Array<int64_t>> integer64Array("Integer64Array");
	static CollectionRegistration<Array<int>> integerArray("IntegerArray");
	static CollectionRegistration<Array<Object*>> objectArray("ObjectArray");
//	static CollectionRegistration<Array<Point>> pointArray("PointArray");
	static CollectionRegistration<Array<double>> realArray("RealArray");
	static CollectionRegistration<Array<std::string>> stringArray("StringArray");
	static CollectionRegistration<Array<std::string>> stringUtf8Array("StringUtf8Array");
	static CollectionRegistration<Array<int>> timeArray("TimeArray");
//	static CollectionRegistration<Array<TimeStamp>> timeStampArray("TimeStampArray");
//	static CollectionRegistration<Array<TimeStampInterval>> timeStampIntervalArray("TimeStampIntervalArray");
}