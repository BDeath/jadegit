#include <jadegit/data/Any.h>

using namespace std;

namespace JadeGit::Data
{	
	/* Create a new handle by cloning value supplied */
	Any::Any(const AnyValue& v) : v(v.Clone()) {}

	/* Copy handle by cloning internal value */
	Any::Any(const Any& rhs) : v(rhs.v ? rhs.v->Clone() : nullptr) {}

	/* Take ownship of value supplied */
	Any::Any(AnyValue *v) : v(v) {}

	/* Compare values */
	bool Any::operator==(const Any &rhs) const
	{
		return *this == rhs.v.get();
	}

	bool Any::operator==(const AnyValue &rhs) const
	{
		return *this == &rhs;
	}

	bool Any::operator==(const AnyValue* rhs) const
	{
		return (!v && !rhs) || (v && *v.get() == rhs);
	}

	/* Assign new value */
	Any & Any::operator=(const Any& rhs)
	{
		/* Clone rhs if we don't have an internal value if we've already got an internal value */
		if (!v)
			v.reset(rhs.v ? rhs.v->Clone() : nullptr);
		else
		{
			/* Delete our internal value if the other doesn't have one */
			if (!rhs.v)
				v.reset(nullptr);
			/* Copy other value */
			else
				v->Set(rhs);
		}

		return *this;
	}

	/* Return string from internal value, otherwise empty string */
	string Any::ToString() const
	{
		return v ? v->ToString() : string();
	}

	string Any::ToBasicString() const
	{
		return v ? v->ToBasicString() : string();
	}

	invalid_cast::invalid_cast(const type_info& from, const type_info& to) : logic_error(string("Invalid cast from ") + from.name() + " to " + to.name()) {}
}