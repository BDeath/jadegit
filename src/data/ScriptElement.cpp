#include <jadegit/data/ScriptElement.h>
#include <jadegit/data/Routine.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/FeatureUsageMeta.h>
#include <jadegit/data/RootSchema/KeyUsageMeta.h>
#include <jadegit/data/RootSchema/InlineTypeUsageMeta.h>
#include <jadegit/data/RootSchema/JadeLocalVarMeta.h>
#include <jadegit/data/RootSchema/ParameterMeta.h>
#include <jadegit/data/RootSchema/ReturnTypeMeta.h>
#include "ObjectRegistration.h"

using namespace std;

namespace JadeGit::Data
{
	static ObjectRegistration<Parameter, Routine> parameter("Parameter");
	static ObjectRegistration<ReturnType, Routine> returnType("ReturnType");

	map<Parameter::Usage, const char*> EnumStrings<Parameter::Usage>::data =
	{
		{ Parameter::Usage::Constant, "constant" },
		{ Parameter::Usage::Input, "input" },
		{ Parameter::Usage::IO, "io" },
		{ Parameter::Usage::Output, "output" }
	};

	ScriptElement::ScriptElement(Script* parent, const Class* dataClass) : Object(parent, dataClass), schemaScript(parent) {}

	TypeUsage::TypeUsage(Script* parent, const Class* dataClass, const char* name) : NamedObject(parent, dataClass, name) {}

	Type* TypeUsage::GetType(bool expected) const
	{
		if (Type* result = type)
			return result;

		if (expected)
			throw runtime_error("Missing type for " + dataClass->name + " [" + schemaScript->GetQualifiedName() + (name.empty() ? "]" : "::" + name + "]"));

		return nullptr;
	}

	Parameter::Parameter(Routine* parent, const Class* dataClass, const char* name) : TypeUsage(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::parameter), name)
	{
		parent->parameters.push_back(this);
	}

	ReturnType::ReturnType(Routine* parent, const Class* dataClass) : TypeUsage(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::returnType))
	{
		if (parent->returnType)
			throw runtime_error(parent->GetQualifiedName() + " has duplicate return types");

		parent->returnType = this;
	}

	ScriptElementMeta::ScriptElementMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "ScriptElement", superclass),
		schemaScript(NewReference<ExplicitInverseRef>("schemaScript", NewType<Class>("Script")))
	{
		schemaScript->manual().child().bind(&ScriptElement::schemaScript);
	}

	FeatureUsageMeta::FeatureUsageMeta(RootSchema& parent, const ScriptElementMeta& superclass) : RootClass(parent, "FeatureUsage", superclass),
		assignment(NewBoolean("assignment")),
		feature(NewReference<ExplicitInverseRef>("feature", NewType<Class>("Feature"))) {}

	KeyUsageMeta::KeyUsageMeta(RootSchema& parent, const ScriptElementMeta& superclass) : RootClass(parent, "KeyUsage", superclass),
		type(NewReference<ExplicitInverseRef>("type", NewType<Class>("Type"))) {}

	TypeUsageMeta::TypeUsageMeta(RootSchema& parent, const ScriptElementMeta& superclass) : RootClass(parent, "TypeUsage", superclass),
		name(NewString("name", 101)),
		type(NewReference<ExplicitInverseRef>("type", NewType<Class>("Type")))
	{
		name->unwritten().bind(&TypeUsage::name);
		type->bind(&TypeUsage::type);
	}

	InlineTypeUsageMeta::InlineTypeUsageMeta(RootSchema& parent, const TypeUsageMeta& superclass) : RootClass(parent, "InlineTypeUsage", superclass) {}

	JadeLocalVarMeta::JadeLocalVarMeta(RootSchema& parent, const TypeUsageMeta& superclass) : RootClass(parent, "JadeLocalVar", superclass) {}

	ParameterMeta::ParameterMeta(RootSchema& parent, const TypeUsageMeta& superclass) : RootClass(parent, "Parameter", superclass),
		wsdlName(NewString("wsdlName")),
		length(NewInteger("length")),
		usage(NewCharacter("usage"))
	{
		length->bind(&Parameter::length);
		usage->bind(&Parameter::usage);
		wsdlName->bind(&Parameter::wsdlName).SetAlias("_wsdlName");
	}

	ReturnTypeMeta::ReturnTypeMeta(RootSchema& parent, const TypeUsageMeta& superclass) : RootClass(parent, "ReturnType", superclass),
		length(NewInteger("length"))
	{
		// Always extract return types after parameters
		subject->bracket(1);

		length->bind(&ReturnType::length);
	}
}