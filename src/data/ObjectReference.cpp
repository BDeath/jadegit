#include <jadegit/data/ObjectReference.h>
#include <jadegit/data/Reference.h>

namespace JadeGit::Data
{
	inverse_maintenance_exception::inverse_maintenance_exception() : std::runtime_error("Inverse maintenance failed") {}

	void inverseAdd(const ExplicitInverseRef& property, Object& origin, Object& target)
	{
		bool okay = false;

		for (auto& inverse : property.inverses)
		{
			// Skip inverses to self
			auto counterpart = inverse->counterpart;
			if (inverse == counterpart && &origin == &target)
			{
				okay = true;
				continue;
			}

			// Skip inverses to other kinds of objects
			auto reference = counterpart->reference;
			if (!target.isKindOf(reference->schemaType))
				continue;

			// Add origin to target
			okay = dynamic_cast<ObjectReference&>(reference->getActualValue(target)).autoAdd(origin) || okay;
		}

		if (!okay && !property.inverseNotRequired)
			throw inverse_maintenance_exception();
	}

	void inverseRemove(const ExplicitInverseRef& property, Object& origin, Object& target)
	{
		bool okay = false;

		for (auto& inverse : property.inverses)
		{
			// Skip inverses to self
			auto counterpart = inverse->counterpart;
			if (inverse == counterpart && &origin == &target)
			{
				okay = true;
				continue;
			}

			// Skip inverses to other kinds of objects
			auto reference = counterpart->reference;
			if (!target.isKindOf(reference->schemaType))
				continue;

			// Remove origin from target
			okay = dynamic_cast<ObjectReference&>(reference->getActualValue(target)).autoRemove(origin) || okay;
		}

		if (!okay && !property.inverseNotRequired)
			throw inverse_maintenance_exception();
	}
}