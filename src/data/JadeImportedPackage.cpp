#include <jadegit/data/JadeImportedPackage.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/Schema.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/JadeImportedClassMeta.h>
#include <jadegit/data/RootSchema/JadeImportedConstantMeta.h>
#include <jadegit/data/RootSchema/JadeImportedInterfaceMeta.h>
#include <jadegit/data/RootSchema/JadeImportedMethodMeta.h>
#include <jadegit/data/RootSchema/JadeImportedPackageMeta.h>
#include <jadegit/data/RootSchema/JadeImportedPropertyMeta.h>
#include "SchemaEntityRegistration.h"
#include "JadeImportedFeatureRegistration.h"

using namespace std;

namespace JadeGit::Data
{
	DEFINE_OBJECT_CAST(JadeImportedConstant)
	DEFINE_OBJECT_CAST(JadeImportedMethod)
	DEFINE_OBJECT_CAST(JadeImportedProperty)
	DEFINE_OBJECT_CAST(JadeImportedClass)
	DEFINE_OBJECT_CAST(JadeImportedInterface)
	DEFINE_OBJECT_CAST(JadeImportedPackage)

	static JadeImportedFeatureRegistration<JadeImportedConstant> importedConstant("JadeImportedConstant", "imported constant", &JadeImportedClass::importedConstants, &JadeImportedInterface::importedConstants);
	static JadeImportedFeatureRegistration<JadeImportedMethod> importedMethod("JadeImportedMethod", "imported method", &JadeImportedClass::importedMethods, &JadeImportedInterface::importedMethods);
	static JadeImportedFeatureRegistration<JadeImportedProperty> importedProperty("JadeImportedProperty", "imported property", &JadeImportedClass::importedProperties, &JadeImportedInterface::importedProperties);
	static EntityRegistration<JadeImportedClass, JadeImportedPackage> importedClass("JadeImportedClass", "imported class", &JadeImportedPackage::classes);
	static EntityRegistration<JadeImportedInterface, JadeImportedPackage> importedInterface("JadeImportedInterface", "imported interface", &JadeImportedPackage::interfaces);	
	static SchemaEntityRegistration<JadeImportedPackage> importedPackage("JadeImportedPackage", "imported package", &Schema::importedPackages);

	template ObjectValue<JadeImportedClass*, &JadeImportedFeatureMeta::importedClass>;
	template ObjectValue<JadeImportedInterface*, &JadeImportedFeatureMeta::importedInterface>;

	JadeImportedFeature::JadeImportedFeature(JadeImportedClass* parent, const Class* dataClass, const char* name) : Feature(parent, dataClass, name)
	{
	}

	JadeImportedFeature::JadeImportedFeature(JadeImportedInterface* parent, const Class* dataClass, const char* name) : Feature(parent, dataClass, name)
	{
	}

	template ObjectValue<JadeExportedConstant*, &JadeImportedConstantMeta::exportedConstant>;

	JadeImportedConstant::JadeImportedConstant(JadeImportedClass* parent, const Class* dataClass, const char* name) : JadeImportedFeature(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeImportedConstant), name)
	{
		importedClass = parent;
	}

	JadeImportedConstant::JadeImportedConstant(JadeImportedInterface* parent, const Class* dataClass, const char* name) : JadeImportedFeature(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeImportedConstant), name)
	{
		importedInterface = parent;
	}

	void JadeImportedConstant::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	template ObjectValue<JadeExportedMethod*, &JadeImportedMethodMeta::exportedMethod>;

	JadeImportedMethod::JadeImportedMethod(JadeImportedClass* parent, const Class* dataClass, const char* name) : JadeImportedFeature(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeImportedMethod), name)
	{
		importedClass = parent;
	}

	JadeImportedMethod::JadeImportedMethod(JadeImportedInterface* parent, const Class* dataClass, const char* name) : JadeImportedFeature(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeImportedMethod), name)
	{
		importedInterface = parent;
	}

	void JadeImportedMethod::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	template ObjectValue<JadeExportedProperty*, &JadeImportedPropertyMeta::exportedProperty>;

	JadeImportedProperty::JadeImportedProperty(JadeImportedClass* parent, const Class* dataClass, const char* name) : JadeImportedFeature(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeImportedProperty), name)
	{
		importedClass = parent;
	}

	JadeImportedProperty::JadeImportedProperty(JadeImportedInterface* parent, const Class* dataClass, const char* name) : JadeImportedFeature(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeImportedProperty), name)
	{
		importedInterface = parent;
	}

	void JadeImportedProperty::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	template ObjectValue<JadeExportedClass*, &JadeImportedClassMeta::exportedClass>;
	template EntityDict<JadeImportedConstant, &JadeImportedClassMeta::importedConstants>;
	template EntityDict<JadeImportedMethod, &JadeImportedClassMeta::importedMethods>;
	template EntityDict<JadeImportedProperty, &JadeImportedClassMeta::importedProperties>;
	template ObjectValue<JadeImportedPackage* const, &JadeImportedClassMeta::package>;

	JadeImportedClass::JadeImportedClass(JadeImportedPackage* parent, const Class* dataClass, const char* name) : Class(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeImportedClass), name),
		package(parent)
	{
	}

	JadeImportedClass::JadeImportedClass(JadeImportedPackage* parent, JadeExportedClass* exportedClass) : JadeImportedClass(parent, nullptr, exportedClass->name.c_str())
	{
		this->exportedClass = exportedClass;
		this->inferredFrom(parent, exportedClass);
	}

	void JadeImportedClass::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	string JadeImportedClass::GetLocalName() const
	{
		return package->name + "::" + name;
	}

	const Class& JadeImportedClass::getOriginal() const
	{
		if (!exportedClass)
			throw runtime_error("Missing exported class");

		exportedClass->Load();
		return exportedClass->getOriginal();
	}

	template ObjectValue<JadeExportedInterface*, &JadeImportedInterfaceMeta::exportedInterface>;
	template EntityDict<JadeImportedConstant, &JadeImportedInterfaceMeta::importedConstants>;
	template EntityDict<JadeImportedMethod, &JadeImportedInterfaceMeta::importedMethods>;
	template EntityDict<JadeImportedProperty, &JadeImportedInterfaceMeta::importedProperties>;
	template ObjectValue<JadeImportedPackage* const, &JadeImportedInterfaceMeta::package>;

	JadeImportedInterface::JadeImportedInterface(JadeImportedPackage* parent, const Class* dataClass, const char* name) : JadeInterface(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeImportedInterface), name),
		package(parent)
	{
	}

	JadeImportedInterface::JadeImportedInterface(JadeImportedPackage* parent, JadeExportedInterface* exportedInterface) : JadeImportedInterface(parent, nullptr, exportedInterface->name.c_str())
	{
		this->exportedInterface = exportedInterface;
		this->inferredFrom(parent, exportedInterface);
	}

	void JadeImportedInterface::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	string JadeImportedInterface::GetLocalName() const
	{
		return package->name + "::" + name;
	}

	const JadeInterface& JadeImportedInterface::getOriginal() const
	{
		if (!exportedInterface)
			throw runtime_error("Missing exported interface");

		exportedInterface->Load();
		return exportedInterface->getOriginal();
	}

	template EntityDict<JadeImportedClass, &JadeImportedPackageMeta::classes>;
	template ObjectValue<JadeExportedPackage*, &JadeImportedPackageMeta::exportedPackage>;
	template EntityDict<JadeImportedInterface, &JadeImportedPackageMeta::interfaces>;

	JadeImportedPackage::JadeImportedPackage(Schema* parent, const Class* dataClass, const char* name, JadeExportedPackage* exportedPackage) : JadePackage(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeImportedPackage), name),
		exportedPackage(exportedPackage)
	{
		schema = parent;

		if (exportedPackage)
			inferFrom(exportedPackage);
	}

	void JadeImportedPackage::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	JadeExportedPackage* JadeImportedPackage::GetExportedPackage()
	{
		return exportedPackage;
	}

	Type* JadeImportedPackage::getType(const string& name) const
	{
		if (auto type = classes.Get(name))
			return type;

		if (auto type = interfaces.Get(name))
			return type;

		return nullptr;
	}

	void JadeImportedPackage::inferFrom(const JadeExportedPackage* exportedPackage)
	{
		// Ensure exported package has been loaded
		exportedPackage->Load();

		// Infer imported classes
		for (auto& exportedClass : exportedPackage->classes)
			if (!classes.Get(exportedClass.second->name))
				new JadeImportedClass(this, exportedClass.second);

		// Infer imported interfaces
		for (auto& exportedInterface : exportedPackage->interfaces)
			if (!interfaces.Get(exportedInterface.second->name))
				new JadeImportedInterface(this, exportedInterface.second);
	}

	void JadeImportedPackage::loaded(bool strict, queue<future<void>>& tasks)
	{
		// Infer classes from exported package if resolved
		if (exportedPackage)
			inferFrom(exportedPackage);
		else if (strict)
			throw runtime_error("Missing exported package");

		// Try loading imported class files (need to check if they exist as they may contain local features)
		for (auto& importedClass : classes)
		{
			if (!importedClass.second->isInferred())
				continue;

			if (const Entity* entity = importedClass.second->getAssembly().Load(importedClass.second->path(), true); entity && entity != importedClass.second)
				throw logic_error("Loaded unexpected entity(" + entity->GetQualifiedName() + ")");
		}
	}

	JadeImportedFeatureMeta::JadeImportedFeatureMeta(RootSchema& parent, const FeatureMeta& superclass) : RootClass(parent, "JadeImportedFeature", superclass),
		importedClass(NewReference<ExplicitInverseRef>("importedClass", NewType<Class>("JadeImportedClass"))),
		importedInterface(NewReference<ExplicitInverseRef>("importedInterface", NewType<Class>("JadeImportedInterface"))),
		incomplete(NewBoolean("incomplete"))
	{
		importedClass->manual().child().bind(&JadeImportedFeature::importedClass);
		importedInterface->manual().child().bind(&JadeImportedFeature::importedInterface);
	}

	JadeImportedConstantMeta::JadeImportedConstantMeta(RootSchema& parent, const JadeImportedFeatureMeta& superclass) : RootClass(parent, "JadeImportedConstant", superclass),
		exportedConstant(NewReference<ExplicitInverseRef>("exportedConstant", NewType<Class>("JadeExportedConstant")))
	{
		exportedConstant->manual().unwritten().bind(&JadeImportedConstant::exportedConstant);
	}

	JadeImportedMethodMeta::JadeImportedMethodMeta(RootSchema& parent, const JadeImportedFeatureMeta& superclass) : RootClass(parent, "JadeImportedMethod", superclass),
		exportedMethod(NewReference<ExplicitInverseRef>("exportedMethod", NewType<Class>("JadeExportedMethod")))
	{
		exportedMethod->manual().unwritten().bind(&JadeImportedMethod::exportedMethod);
	}

	JadeImportedPropertyMeta::JadeImportedPropertyMeta(RootSchema& parent, const JadeImportedFeatureMeta& superclass) : RootClass(parent, "JadeImportedProperty", superclass),
		exportedProperty(NewReference<ExplicitInverseRef>("exportedProperty", NewType<Class>("JadeExportedProperty")))
	{
		exportedProperty->manual().unwritten().bind(&JadeImportedProperty::exportedProperty);
	}

	JadeImportedClassMeta::JadeImportedClassMeta(RootSchema& parent, const ClassMeta& superclass) : RootClass(parent, "JadeImportedClass", superclass),
		exportedClass(NewReference<ExplicitInverseRef>("exportedClass", NewType<Class>("JadeExportedClass"))),
		incomplete(NewBoolean("incomplete")),
		importedConstants(NewReference<ExplicitInverseRef>("importedConstants", NewType<CollClass>("JadeImportedConstantNDict"))),
		importedMethods(NewReference<ExplicitInverseRef>("importedMethods", NewType<CollClass>("JadeImportedMethodNDict"))),
		importedProperties(NewReference<ExplicitInverseRef>("importedProperties", NewType<CollClass>("JadeImportedPropertyNDict"))),
		package(NewReference<ExplicitInverseRef>("package", NewType<Class>("JadePackage")))
	{
		exportedClass->manual().unwritten().bind(&JadeImportedClass::exportedClass);
		importedConstants->automatic().parent().bind(&JadeImportedClass::importedConstants);
		importedMethods->automatic().parent().bind(&JadeImportedClass::importedMethods);
		importedProperties->automatic().parent().bind(&JadeImportedClass::importedProperties);
		package->manual().child().bind(&JadeImportedClass::package);
	}

	JadeImportedInterfaceMeta::JadeImportedInterfaceMeta(RootSchema& parent, const JadeInterfaceMeta& superclass) : RootClass(parent, "JadeImportedInterface", superclass),
		exportedInterface(NewReference<ExplicitInverseRef>("exportedInterface", NewType<Class>("JadeExportedInterface"))),
		incomplete(NewBoolean("incomplete")),
		importedConstants(NewReference<ExplicitInverseRef>("importedConstants", NewType<CollClass>("JadeImportedConstantNDict"))),
		importedMethods(NewReference<ExplicitInverseRef>("importedMethods", NewType<CollClass>("JadeImportedMethodNDict"))),
		importedProperties(NewReference<ExplicitInverseRef>("importedProperties", NewType<CollClass>("JadeImportedPropertyNDict"))),
		package(NewReference<ExplicitInverseRef>("package", NewType<Class>("JadePackage")))
	{
		exportedInterface->manual().unwritten().bind(&JadeImportedInterface::exportedInterface);
		importedConstants->automatic().parent().bind(&JadeImportedInterface::importedConstants);
		importedMethods->automatic().parent().bind(&JadeImportedInterface::importedMethods);
		importedProperties->automatic().parent().bind(&JadeImportedInterface::importedProperties);
		package->manual().child().bind(&JadeImportedInterface::package);
	}

	JadeImportedPackageMeta::JadeImportedPackageMeta(RootSchema& parent, const JadePackageMeta& superclass) : RootClass(parent, "JadeImportedPackage", superclass),
		classes(NewReference<ExplicitInverseRef>("classes", NewType<CollClass>("JadeImportedClassNDict"))),
		exportedPackage(NewReference<ExplicitInverseRef>("exportedPackage", NewType<Class>("JadeExportedPackage"))),
		interfaces(NewReference<ExplicitInverseRef>("interfaces", NewType<CollClass>("JadeImportedInterfaceNDict")))
	{
		classes->automatic().parent().bind(&JadeImportedPackage::classes);
		exportedPackage->manual().structural().bind(&JadeImportedPackage::exportedPackage);
		interfaces->automatic().parent().bind(&JadeImportedPackage::interfaces);
	}
}