#include <jadegit/data/Method.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ExternalMethodMeta.h>
#include <jadegit/data/RootSchema/JadeMethodMeta.h>
#include <jadegit/data/CollClass.h>
#include "FeatureRegistration.h"

namespace JadeGit::Data
{
	DEFINE_OBJECT_CAST(JadeMethod)

	static MethodRegistration<Method> method("Method");
	static MethodRegistration<ExternalMethod> externalMethod("ExternalMethod");
	static MethodRegistration<JadeMethod> jadeMethod("JadeMethod");

	std::map<Method::Invocation, const char*> EnumStrings<Method::Invocation>::data =
	{
		{ Method::Invocation::Normal, "normal" },
		{ Method::Invocation::Mapping, "mapping" },
		{ Method::Invocation::Event, "event" },
		{ Method::Invocation::Automation, "automation" }
	};

	std::map<Method::UnitTestFlags, const char*> EnumStrings<Method::UnitTestFlags>::data =
	{
		{ Method::UnitTestFlags::None, "" },
		{ Method::UnitTestFlags::Test, "Test" },
		{ Method::UnitTestFlags::Ignore, "Ignore" },
		{ Method::UnitTestFlags::Before, "Before" },
		{ Method::UnitTestFlags::BeforeClass, "BeforeClass" },
		{ Method::UnitTestFlags::BeforeAll, "BeforeAll" },
		{ Method::UnitTestFlags::After, "After" },
		{ Method::UnitTestFlags::AfterClass, "AfterClass" },
		{ Method::UnitTestFlags::AfterAll, "AfterAll" }
	};

	template ObjectValue<Method*, &MethodMeta::controlMethod>;
	template ObjectValue<Array<JadeInterfaceMethod*>, &MethodMeta::interfaceImplements>;
	template Value<Method::Invocation>;
	template Value<Method::UnitTestFlags>;

	Method::Method(Type* parent, const Class* dataClass, const char* name) : Routine(parent, dataClass, name)
	{
		schemaType = parent;
	};

	bool Method::isTypeMethod() const
	{
		// TODO: Split Routine::options during extract into friendly boolean properties
		return (options & 4) > 0;
	}

	ExternalMethod::ExternalMethod(Type* parent, const Class* dataClass, const char* name) : Method(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::externalMethod, "ExternalMethod"), name) {};

	void ExternalMethod::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	JadeMethod::JadeMethod(Type* parent, const Class* dataClass, const char* name) : Method(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeMethod, "JadeMethod"), name)
	{
	};

	void JadeMethod::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	MethodMeta::MethodMeta(RootSchema& parent, const RoutineMeta& superclass) : RootClass(parent, "Method", superclass),
		condition(NewBoolean("condition")),
		conditionSafe(NewBoolean("conditionSafe")),
		controlMethod(NewReference<ExplicitInverseRef>("controlMethod", NewType<Class>("Method"))),
		controlMethodRefs(NewReference<ExplicitInverseRef>("controlMethodRefs", NewType<CollClass>("MethodSet"))),
		exportedMethodRefs(NewReference<ExplicitInverseRef>("exportedMethodRefs", NewType<CollClass>("JadeExportedMethodSet"))),
		final(NewBoolean("final")),
		interfaceImplements(NewReference<ExplicitInverseRef>("interfaceImplements", NewType<CollClass>("MethodColl"))),
		lockReceiver(NewBoolean("lockReceiver")),
		methodInvocation(NewCharacter("methodInvocation")),
		partitionMethod(NewBoolean("partitionMethod")),
		subschemaCopyFinal(NewBoolean("subschemaCopyFinal")),
		subschemaFinal(NewBoolean("subschemaFinal")),
		unitTestFlags(NewInteger("unitTestFlags")),
		updating(NewBoolean("updating"))
	{
		// Group methods after properties
		subject->bracket(3);

		condition->bind(&Method::condition);
		conditionSafe->bind(&Method::conditionSafe);
		controlMethod->manual().bind(&Method::controlMethod);
		controlMethodRefs->automatic();
		exportedMethodRefs->automatic();
		final->bind(&Method::final);
		interfaceImplements->manual().bind(&Method::interfaceImplements);
		lockReceiver->bind(&Method::lockReceiver);
		methodInvocation->bind(&Method::invocation);
		partitionMethod->bind(&Method::partitionMethod);
		subschemaCopyFinal->bind(&Method::subschemaCopyFinal);
		subschemaFinal->bind(&Method::subschemaFinal);
		unitTestFlags->bind(&Method::unitTestFlags);
		updating->bind(&Method::updating);
	}

	ExternalMethodMeta::ExternalMethodMeta(RootSchema& parent, const MethodMeta& superclass) : RootClass(parent, "ExternalMethod", superclass) {}

	JadeMethodMeta::JadeMethodMeta(RootSchema& parent, const MethodMeta& superclass) : RootClass(parent, "JadeMethod", superclass),
		referenceRefs(NewReference<ExplicitInverseRef>("referenceRefs", NewType<CollClass>("ReferencePropertySet")))
	{
		referenceRefs->automatic();
	}
}