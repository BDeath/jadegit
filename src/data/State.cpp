#include "State.h"
#include <jadegit/data/Assembly.h>
#include <jadegit/data/Class.h>
#include <jadegit/data/Entity.h>
#include <jadegit/data/Reference.h>
#include <Epilog.h>
#include "storage/ObjectFileStorage.h"
#include "storage/ObjectLoader.h"

using namespace std;

namespace JadeGit::Data
{
	const State& State::get(const Object& object) const
	{
		return *object.state;
	}

	void State::clean(Object* object) const
	{
		// Copy collection to clean, as deletion from original affects iteration
		pmr::vector<Object*> children(object->children);

		// Clean each child
		for (Object* child : children)
			get(*child).clean(child);
	}

	// When state transition confirms existence, object is moved to end of parent collection (maintaining correct order for output)
	void State::confirm(Object* object) const
	{
		auto parent = object->getParentObject();
		if (!parent)
			return;

		auto& v = parent->children;
		if (v.back() == object)
			return;

		auto pos = find(v.begin(), v.end(), object);
		rotate(pos, next(pos), v.end());
	}

	void State::created(Entity* entity, const char* guid) const
	{
		throw logic_error("Invalid state for creation");
	}

	void State::dirty(Entity& entity, bool deep) const
	{
		// Entity must be loaded before it can be marked as dirty
		load(entity);

		// Verify state changed above
		if (&get(entity) == this)
			throw logic_error("Invalid state for change");

		// Forward call to make entity dirty
		get(entity).dirty(entity, deep);
	}

	void State::dirty(Object& object) const
	{
		throw logic_error("Invalid state for change");
	}

	void State::load(Entity& entity) const
	{
		throw logic_error("Invalid state for load");
	}

	void State::loading(Object* object, bool shallow) const
	{
		throw logic_error("Invalid state for load to start");
	}

	void State::loaded(Object* object, bool shallow, bool resolved) const
	{
		throw logic_error("Invalid state for load to finish");
	}

	void State::modified(Object* object) const
	{
		throw logic_error("Invalid state for change");
	}

	void State::save(Entity* entity, bool prelude) const
	{
		throw logic_error("Invalid state for save");
	}

	void State::saveChildren(const Object* parent, bool prelude) const
	{
		for (Object* child : parent->children)
		{
			Entity* entity = dynamic_cast<Entity*>(child);
			if (entity && entity->isMajor())
				entity->Save(prelude);
		}
	}

	void State::set(Object* object, const State* state) const
	{
		object->state = state;
		state->set(object);
	}

	// Object is up-to-date
	class CurrentState : public State, public Singleton<CurrentState>
	{
	public:
		// Existing object can be marked as dirty
		void dirty(Entity& entity, bool deep) const final;
		void dirty(Object& object) const final;

		// Existing object can be updated
		void modified(Object* object) const final;

		// Load has already been completed
		void load(Entity& entity) const final {}

		// No local changes to save, but do need to recursively check children
		void save(Entity* entity, bool prelude) const final
		{
			saveChildren(entity, prelude);
		}

		// Load cannot be repeated
		void loading(Object* object, bool shallow) const final
		{
			throw logic_error("Object cannot be reloaded");
		}

	protected:
		// When set, need to propagate down
		using State::set;
		void set(Object* parent) const final
		{
			// Replicate state change down to all children, which aren't inferred, nor saved in their own right (minor entities)
			for (Object* child : parent->children)
				if (!child->isInferred() && !child->isMajor())
					set(child, this);
		}
	};

	// Intermediary loaded state used after initial load, until post-load tasks in the queue are completed
	template <bool Shallow>
	class LoadedState : public State, public Singleton<LoadedState<Shallow>>
	{
	public:
		// Suppress calls to mark object as modified while loading
		void modified(Object* object) const final {}

		void load(Entity& entity) const final
		{
			if constexpr (Shallow)
			{
				// Load can only be restarted for entities for which file path can be derived
				assert(entity.isMajor());

				// Reload definition via assembly, which should return current entity
				if (&entity != entity.getAssembly().Load(entity.path(), false))
					throw logic_error("Reload failed (" + entity.GetQualifiedName() + ")");

				// Verify state change occurred
				assert(&get(entity) == LoadedState<false>::Instance());
			}
		}

		// Load has finished
		void loaded(Object* object, bool shallow, bool resolved) const final;

		// Load can be restarted
		void loading(Object* object, bool shallow) const final;
	};

	bool State::isCurrent() const
	{
		return this == CurrentState::Instance() || this == LoadedState<false>::Instance();
	}

	// Intermediary loading state used during initial load
	class LoadingState : public State, public Singleton<LoadingState>
	{
	public:
		// Suppress calls to mark object as modified while loading
		void modified(Object* object) const final {}

		// Load hasn't actually been completed yet, but we're currently ignoring subsequent calls which reflects previous behaviour
		// TODO: Further refactoring needed to distinguish when to ignore during final load phase when references are being resolved, but parent/child structure has been loaded
		void load(Entity& entity) const final
		{
//			throw jadegit_exception("Invalid state for load");
		}

		// Initial load has finished
		void loaded(Object* object, bool shallow, bool resolved) const final
		{
			assert(!resolved);
			if (shallow)
				set(object, LoadedState<true>::Instance());
			else
				set(object, LoadedState<false>::Instance());
		}
	};

	bool State::isLoading() const
	{
		return this == LoadingState::Instance();
	}

	// Load can be restarted
	void LoadedState<true>::loading(Object* object, bool shallow) const
	{
		// Must be completing load in this scenario
		if (shallow)
			throw logic_error("Invalid state for load");

		set(object, LoadingState::Instance());
	}

	void LoadedState<false>::loading(Object* object, bool shallow) const
	{
		State::loading(object, shallow);
	}

	// Known entity has been instantiated, but not fully loaded
	class ShallowState : public State, public Singleton<ShallowState>
	{
	public:
		// Load can be completed
		void load(Entity& entity) const final
		{
			// Load can only be restarted for entities for which file path can be derived
			assert(entity.isMajor());

			// Reload definition via assembly, which should return current entity
			if (&entity != entity.getAssembly().Load(entity.path(), false))
				throw logic_error("Reload failed (" + entity.GetQualifiedName() + ")");

			// Verify state change occurred
			assert(get(entity).isCurrent());
		}

		// Load can be restarted
		void loading(Object* object, bool shallow) const final
		{
			// Must be completing load in this scenario
			if (shallow)
				throw logic_error("Invalid state for load");

			set(object, LoadingState::Instance());
		}

		// No local changes to save, but do need to recursively check children
		void save(Entity* entity, bool prelude) const final
		{
			saveChildren(entity, prelude);
		}
	};

	bool State::isShallow() const
	{
		return this == ShallowState::Instance() || this == LoadedState<true>::Instance();
	}

	void LoadedState<true>::loaded(Object* object, bool shallow, bool resolved) const
	{
		assert(resolved);
		assert(shallow);
		set(object, ShallowState::Instance());
	}

	void LoadedState<false>::loaded(Object* object, bool shallow, bool resolved) const
	{
		assert(resolved);
		if (!shallow) set(object, CurrentState::Instance());
	}

	// Object has been modified in some way which is yet to be saved
	template <bool Adding>
	class ModifiedState : public State
	{
	public:
		// Modified objects can be marked as dirty, with new objects reverting to initial state
		void dirty(Entity& entity, bool deep) const
		{
			CurrentState::Instance()->dirty(entity, deep);

			if constexpr (Adding)
				set(&entity, InitialState::Instance());
		}

		void dirty(Object& object) const
		{
			CurrentState::Instance()->dirty(object);

			if constexpr (Adding)
				set(&object, InitialState::Instance());
		}
		
		// Change has already been registered
		void modified(Object* object) const final {}

		// Load has already been completed
		void load(Entity& entity) const final {}

		// Modified entity can be saved
		void save(Entity* entity, bool prelude) const final
		{
			// Recurse up to parent entity with which definition file is associated
			if (!entity->isMajor())
				return entity->getParentEntity()->Save(prelude);

			// Suppress write during initial prelude to validate whether all entities can be saved
			if (!prelude)
			{
				// Save to file repository
				entity->getAssembly().getStorage().write(*entity, Adding);

				// Set state to current
				set(entity, CurrentState::Instance());
			}

			// Save changes to children stored separately
			saveChildren(entity, prelude);
		}

	protected:
		// When set, need to propagate state up
		using State::set;
		void set(Object* child) const final
		{
			// No need to propagate any further if we've reached a major entity that'll be saved in it's own right
			if (child->isMajor())
				return;

			// Set appropriate modified state via parent method
			child->getParentObject()->Modified();
		}
	};

	// New object is being created
	class CreatedState : public ModifiedState<true>, public Singleton<CreatedState>
	{
	};

	bool State::isCreated() const
	{
		return this == CreatedState::Instance();
	}

	// Existing object is being updated
	class UpdatedState : public ModifiedState<false>, public Singleton<UpdatedState>
	{
	};

	bool State::isModified() const
	{
		return this == CreatedState::Instance() || this == UpdatedState::Instance();
	}

	// Existing object can be updated
	void CurrentState::modified(Object* object) const
	{
		set(object, UpdatedState::Instance());
	}

	// Existing object is dirty, where it may be discarded if not modified before clean-up
	class DirtyState : public State, public Singleton<DirtyState>
	{
	public:
		// Dirty object can be cleaned, in which case it's deleted
		void clean(Object* object) const final
		{
			// Delete associated file & possible sub-folder for major entities
			if (auto entity = object->asEntity(); entity && entity->isMajor())
				object->getAssembly().getStorage().dele(*entity);

			Object::dele(object);
		}

		// Object already marked as dirty
		void dirty(Entity& object, bool deep) const final {}
		void dirty(Object& object) const final {}

		// Dirty object can be updated, making it valid again
		void modified(Object* object) const final
		{
			// Confirm existence again if modified order needs to preserved/reflected by extract
			auto parent = object->getParentObject();
			if (parent && parent->isOrganized())
				confirm(object);

			set(object, UpdatedState::Instance());
		}

		// Load completed before being marked as dirty
		void load(Entity& entity) const final {}
	};

	bool State::isDirty() const
	{
		return this == DirtyState::Instance();
	}

	// Existing object can be marked as dirty
	void CurrentState::dirty(Entity& entity, bool deep) const
	{
		set(&entity, DirtyState::Instance());

		// Force loading children for major entities during deep extract (needed to support reflecting what's been removed during full re-extract)
		if (deep && entity.isMajor())
			entity.getAssembly().getStorage().loadChildren(entity.path().replace_extension(), false);

		// Flag children as being dirty
		for (int i = 0; i < entity.children.size(); i++)
		{
			Object& child = *entity.children[i];
			if (Entity* entity = const_cast<Entity*>(child.asEntity()))
			{
				if (deep)
					get(child).dirty(*entity, true);
			}
			else
			{
				get(child).dirty(child);
			}
		}
	}

	void CurrentState::dirty(Object& object) const
	{
		set(&object, DirtyState::Instance());

		// Flag any children as being dirty
		for (int i = 0; i < object.children.size(); i++)
		{
			Object& child = *object.children[i];
			assert(!child.asEntity());
			get(child).dirty(child);
		}
	}

	// Implied object may be loaded
	void ImpliedState::load(Entity& entity) const
	{
		// Load parent
		assert(entity.getParentEntity());
		entity.getParentEntity()->Load();

		// Verify state change occurred
		assert(get(entity).isCurrent());
	}

	// Implied object may be loaded, confirming it's a known entity
	void ImpliedState::loading(Object* object, bool shallow) const
	{
		// Confirm existence
		confirm(object);

		set(object, LoadingState::Instance());
	}

	// Unless loaded, entity is unknown and cannot be saved
	void ImpliedState::save(Entity* entity, bool prelude) const
	{
		throw runtime_error("Unknown " + entity->dataClass->name + " (" + entity->GetQualifiedName() + ")");
	}

	// Inferred object may be loaded, implying that it's a known entity
	void InferredState::loading(Object* object, bool shallow) const
	{
		// Confirm existence
		confirm(object);

		set(object, LoadingState::Instance());
	}

	// Inferred object can be modified, implying it should be created/saved
	void InferredState::modified(Object* object) const
	{
		// Confirm existence
		confirm(object);

		// Reset back to initial state and repeat
		set(object, InitialState::Instance());
		object->Modified();
	}

	// New entity may be explicitly created with guid supplied
	void InitialState::created(Entity* entity, const char* id) const
	{
		assert(entity->id.is_nil());
		if (id)
			entity->id = uuids::uuid::from_string(id).value();

		set(entity, CreatedState::Instance());
	}

	// New entity can be implied
	void InitialState::implied(Entity* entity) const
	{
		set(entity, ImpliedState::Instance());
	}

	// New entity can be inferred
	void InitialState::inferred(Entity& entity) const
	{
		set(&entity, InferredState::Instance());

		// Notify object loader to callback for inferred entities
		entity.getAssembly().getStorage().getLoader().inferred(entity);
	}

	// Existing object can be loaded
	void InitialState::loading(Object* object, bool shallow) const
	{
		set(object, LoadingState::Instance());
	}

	// New object can be modified, implying creation
	void InitialState::modified(Object* object) const
	{
		// Confirm existence
		confirm(object);

		if (Entity* entity = dynamic_cast<Entity*>(object))
			created(entity, nullptr);
		else
			set(object, CreatedState::Instance());
	}

	// Unless loaded or modified, entity is unknown and cannot be saved
	void InitialState::save(Entity* entity, bool prelude) const
	{
		throw runtime_error("Unknown " + entity->dataClass->name + " (" + entity->GetQualifiedName() + ")");
	}
}