#include <jadegit/data/MetaSchema.h>
#include <jadegit/data/MetaSchema/MetaClass.h>
#include <jadegit/data/Assembly.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/ExternalClass.h>
#include "State.h"

namespace JadeGit::Data
{
	MetaObject::MetaObject(MetaObject& parent)
	{
		parent.children.emplace_back(this);
	}

	const RootSchema& MetaObject::GetRootSchema(const Assembly& assembly) const
	{
		return assembly.GetRootSchema();
	}

	template <typename TType>
	MetaType<TType>::MetaType(MetaSchema& parent, const char* name) : MetaType(parent, dynamic_cast<TType*>(parent->getType(name)), name) {}

	template <typename TType>
	MetaType<TType>::MetaType(MetaSchema& parent, TType* existing, const char* name) : Meta<TType>(parent, existing ? existing : new TType(parent, nullptr, name)), parent(parent) {}

	template <typename TType>
	bool MetaType<TType>::versionCheck(const Version& minVersion) const
	{
		return parent.version.empty() || minVersion <= parent.version;
	}

	template<class TType>
	MetaClass<TType>::MetaClass(MetaSchema& parent, const char* name, Class* superclass) : MetaType<TType>(parent, name)
	{
		assert(superclass || this->subject->GetName() == "Object");
		if (superclass)
			this->subject->bracket(superclass->bracket()).superclass = superclass;
	}

	/* Primitives */
	template<class TType>
	PrimAttribute* MetaClass<TType>::NewBoolean(const char* name)
	{
		return NewProperty<PrimAttribute>(name, GetRootSchema().boolean);
	}

	template<class TType>
	PrimAttribute* MetaClass<TType>::NewBoolean(const Version& minVersion, const char* name)
	{
		return NewProperty<PrimAttribute>(minVersion, name, GetRootSchema().boolean);
	}

	template<class TType>
	PrimAttribute* MetaClass<TType>::NewBinary(const char* name, int length)
	{
		return NewProperty<PrimAttribute>(name, GetRootSchema().binary, length);
	}

	template<class TType>
	PrimAttribute* MetaClass<TType>::NewBinary(const Version& minVersion, const char* name, int length)
	{
		return NewProperty<PrimAttribute>(minVersion, name, GetRootSchema().binary, length);
	}

	template<class TType>
	PrimAttribute* MetaClass<TType>::NewByte(const char* name)
	{
		return NewProperty<PrimAttribute>(name, GetRootSchema().byte);
	}

	template<class TType>
	PrimAttribute* MetaClass<TType>::NewByte(const Version& minVersion, const char* name)
	{
		return NewProperty<PrimAttribute>(minVersion, name, GetRootSchema().byte);
	}

	template<class TType>
	PrimAttribute* MetaClass<TType>::NewCharacter(const char* name)
	{
		return NewProperty<PrimAttribute>(name, GetRootSchema().character);
	}

	template<class TType>
	PrimAttribute* MetaClass<TType>::NewCharacter(const Version& minVersion, const char* name)
	{
		return NewProperty<PrimAttribute>(minVersion, name, GetRootSchema().character);
	}

	template<class TType>
	PrimAttribute* MetaClass<TType>::NewInteger(const char* name)
	{
		return NewProperty<PrimAttribute>(name, GetRootSchema().integer);
	}

	template<class TType>
	PrimAttribute* MetaClass<TType>::NewInteger(const Version& minVersion, const char* name)
	{
		return NewProperty<PrimAttribute>(minVersion, name, GetRootSchema().integer);
	}

	template<class TType>
	PrimAttribute* MetaClass<TType>::NewReal(const char* name)
	{
		return NewProperty<PrimAttribute>(name, GetRootSchema().real);
	}

	template<class TType>
	PrimAttribute* MetaClass<TType>::NewReal(const Version& minVersion, const char* name)
	{
		return NewProperty<PrimAttribute>(minVersion, name, GetRootSchema().real);
	}

	template<class TType>
	PrimAttribute* MetaClass<TType>::NewString(const char* name, int length)
	{
		return NewProperty<PrimAttribute>(name, GetRootSchema().string, length);
	}

	template<class TType>
	PrimAttribute* MetaClass<TType>::NewString(const Version& minVersion, const char* name, int length)
	{
		return NewProperty<PrimAttribute>(minVersion, name, GetRootSchema().string, length);
	}

	template<class TType>
	PrimAttribute* MetaClass<TType>::NewStringUtf8(const char* name, int length)
	{
		return NewProperty<PrimAttribute>(name, GetRootSchema().stringUtf8, length);
	}

	template<class TType>
	PrimAttribute* MetaClass<TType>::NewStringUtf8(const Version& minVersion, const char* name, int length)
	{
		return NewProperty<PrimAttribute>(minVersion, name, GetRootSchema().stringUtf8, length);
	}

	template<class TType>
	PrimAttribute* MetaClass<TType>::NewTimeStamp(const char* name)
	{
		return NewProperty<PrimAttribute>(name, GetRootSchema().timeStamp);
	}

	template<class TType>
	PrimAttribute* MetaClass<TType>::NewTimeStamp(const Version& minVersion, const char* name)
	{
		return NewProperty<PrimAttribute>(minVersion, name, GetRootSchema().timeStamp);
	}

	// Explicit template instantiation
	template class MetaClass<Class>;
	template class MetaClass<CollClass>;
	template class MetaClass<ExternalClass>;
	template class MetaClass<GUIClass>;

	MetaSchema::MetaSchema(Schema* schema, Version version) : Meta(schema), version(version)
	{
		// Make static/predefined
		subject->state = StaticState::Instance();
	}

	MetaSchema::MetaSchema(Assembly& parent, const char* name, Version version, const RootSchema& rootSchema) : MetaSchema(new Schema(parent, nullptr, name, rootSchema), version)
	{
	}

	MetaSchema::MetaSchema(Assembly& parent, const char* name, Schema* superschema, Version version) : MetaSchema(new Schema(parent, nullptr, name), version)
	{
		subject->superschema = superschema;
	}
}