#include <jadegit/data/Type.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/Schema.h>
#include <jadegit/data/RootSchema/TypeMeta.h>
#include "TypeRegistration.h"

namespace JadeGit::Data
{
	const std::filesystem::path Type::subFolder("types");

	DEFINE_OBJECT_CAST(Type)

	// Type registration needed to support resolving Type references during load
	static TypeRegistration<Type, JadeImportedPackage, Schema> type("Type", std::mem_fn(&JadeImportedPackage::getType), std::mem_fn(&Schema::getType));
	
	Type* GetSuperSchemaType(Schema* parent, const char* name)
	{
		Schema* superschema = parent->GetSuperSchema();
		return superschema ? Entity::resolve<Type>(*superschema, name) : nullptr;
	}

	template <>
	void ObjectValue<Schema*, &TypeMeta::schema>::inverseAdd(Object& target) const
	{
		if (!offset)
			offset = member_offset(&Type::schema);

		auto& type = getObject();
		auto& schema = static_cast<Schema&>(target);

		if (!static_cast<ObjectReference&>(schema.classes).autoAdd(type) &&
			!static_cast<ObjectReference&>(schema.interfaces).autoAdd(type) &&
			!static_cast<ObjectReference&>(schema.primitives).autoAdd(type) &&
			!static_cast<ObjectReference&>(schema.pseudoTypes).autoAdd(type))
			throw inverse_maintenance_exception();
	}

	template ObjectValue<Set<CollClass*>, &TypeMeta::collClassRefs>;
	template EntityDict<Constant, &TypeMeta::consts>;
	template EntityDict<Method, &TypeMeta::methods>;
	template ObjectValue<Set<Property*>, &TypeMeta::propertyRefs>;
	template ObjectValue<Schema*, &TypeMeta::schema>;
	template ObjectValue<Set<Type*>, &TypeMeta::subschemaTypes>;
	template ObjectValue<Type*, &TypeMeta::superschemaType>;

	Type::Type(Schema* parent, const Class* dataClass, const char* name) : Type(parent, dataClass, name, parent)
	{
		superschemaType = GetSuperSchemaType(parent, name);
		assert(!superschemaType || superschemaType->name == name);

		// Flag subschema copy as inferred, meaning it doesn't have to be saved unless modified
		inferred(!!superschemaType);
	}

	Type::Type(JadeImportedPackage* parent, const Class* dataClass, const char* name) : Type(parent, dataClass, name, nullptr) {}

	Type::Type(Object* parent, const Class* dataClass, const char* name, Schema* schema) : MajorEntity(parent, dataClass, name)
	{
	}

	int Type::GetDefaultLength() const
	{
		// Assume type is an object reference
		return 6;
	}

	Type* Type::GetExisting(const char* name)
	{
		return schema->getType(name);
	}

	Feature* Type::getFeature(const std::string& name) const
	{
		if (auto feature = methods.Get(name))
			return feature;

		if (auto feature = constants.Get(name))
			return feature;

		return nullptr;
	}

	const Type& Type::getOriginal() const
	{
		return getRootType();
	}

	const Type& Type::getRootType() const
	{
		const Type* result = this;
		while (result->superschemaType)
			result = result->superschemaType;

		return *result;
	}

	bool Type::inheritsFrom(const Type* type) const
	{
		// Check if original/root type matches
		return type && (&getOriginal() == &type->getOriginal());
	}

	void Type::LoadHeader(const FileElement& source)
	{
		SchemaEntity::LoadHeader(source);

		// TODO: Implement a check for repeated calls to LoadHeader to ensure nothing has changed
		// OR .. more preferably, consider whether we can skip LoadHeader altogether (assuming we can guarantee the source hasn't changed).

		// Resolve superschema type for local types (implies whether or not this is a subschema copy)
		if (Schema* superschema = (schema ? schema->GetSuperSchema() : nullptr))
			superschemaType = EntityFactory::Get().Resolve<Type>(superschema, QualifiedName(name), false);
	}

	bool Type::isCopy() const
	{
		return isSubschemaCopy();
	}

	bool Type::isRootType() const
	{
		return !superschemaType;
	}

	bool Type::isSubschemaCopy() const
	{
		return !!superschemaType;
	}

	TypeMeta::TypeMeta(RootSchema& parent, const SchemaEntityMeta& superclass) : RootClass(parent, "Type", superclass),
		collClassRefs(NewReference<ExplicitInverseRef>("collClassRefs", NewType<CollClass>("CollClassSet"))),
		constantRefs(NewReference<ExplicitInverseRef>("constantRefs", NewType<CollClass>("ConstantSet"))),
		consts(NewReference<ExplicitInverseRef>("consts", NewType<CollClass>("ConstantNDict"))),
		final(NewBoolean("final")),
		methods(NewReference<ExplicitInverseRef>("methods", NewType<CollClass>("MethodNDict"))),
		persistentAllowed(NewBoolean("persistentAllowed")),
		propertyRefs(NewReference<ExplicitInverseRef>("propertyRefs", NewType<CollClass>("UserPropertySet"))),
		schema(NewReference<ExplicitInverseRef>("schema", NewType<Class>("Schema"))),
		sharedTransientAllowed(NewBoolean("sharedTransientAllowed")),
		subclassPersistentAllowed(NewBoolean("subclassPersistentAllowed")),
		subclassSharedTransientAllowed(NewBoolean("subclassSharedTransientAllowed")),
		subclassTransientAllowed(NewBoolean("subclassTransientAllowed")),
		subschemaFinal(NewBoolean("subschemaFinal")),
		subschemaTypes(NewReference<ExplicitInverseRef>("subschemaTypes", NewType<CollClass>("UserTypeSet"))),
		superschemaType(NewReference<ExplicitInverseRef>("superschemaType", NewType<Class>("Type"))),
		transientAllowed(NewBoolean("transientAllowed"))
	{
		collClassRefs->automatic().bind(&Type::collClassRefs);
		constantRefs->automatic().bind(&Type::constantRefs);
		consts->automatic().parent().bind(&Type::constants);
		final->bind(&Type::final);
		methods->automatic().parent().bind(&Type::methods);
		persistentAllowed->bind(&Type::persistentAllowed);
		propertyRefs->automatic().bind(&Type::propertyRefs);
		schema->manual().child().bind(&Type::schema);
		sharedTransientAllowed->bind(&Type::sharedTransientAllowed);
		subclassPersistentAllowed->bind(&Type::subclassPersistentAllowed);
		subclassSharedTransientAllowed->bind(&Type::subclassSharedTransientAllowed);
		subclassTransientAllowed->bind(&Type::subclassTransientAllowed);
		subschemaFinal->bind(&Type::subschemaFinal);
		subschemaTypes->automatic().bind(&Type::subschemaTypes);
		superschemaType->manual().bind(&Type::superschemaType);
		transientAllowed->bind(&Type::transientAllowed);
	}
}