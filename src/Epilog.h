#pragma once

template <typename T>
class Epilog
{
public:
	Epilog(const T& epilog) : epilog(epilog) {}
	~Epilog() { epilog(); }
private:
	T epilog;
};