using System;
using System.Diagnostics;

namespace JadeGit.Authentication
{
    public class GitCredential
    {
        public int fill(string url, out string username, out string password, out string errors)
        {
            username = null;
            password = null;
            errors = null;

            try
            {
                ProcessStartInfo startInfo = new ProcessStartInfo
                {
                    FileName = "git",
                    Arguments = "credential fill",
                    UseShellExecute = false,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    CreateNoWindow = true,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true
                };

                Process process = new Process
                {
                    StartInfo = startInfo
                };

                process.Start();

                // Write input parameters to stdin
                process.StandardInput.NewLine = "\n";
                process.StandardInput.WriteLine($"url={url}");
                process.StandardInput.WriteLine();

                // Capture any errors
                string line = null;
                while ((line = process.StandardError.ReadLine()) != null)
                {
                    // Handle user cancellation
                    if (line.StartsWith("fatal: User canceled authentication."))
                        return 0;

                    // Ignore warnings
                    if (line.StartsWith("warning:"))
                        continue;

                    // Suppress errors which occur when git attempts to prompt user to enter username/password
                    if (line.StartsWith("bash: /dev/tty: No such device or address") ||
                        line.StartsWith("error: failed to execute prompt script"))
                        continue;

                    // Append errors
                    if (errors != null)
                        errors += Environment.NewLine;
                    errors += line;
                }
                
                // Get username/password from stdout
                while ((line = process.StandardOutput.ReadLine()) != null)
                {
                    string[] details = line.Split('=');
                    if (details[0] == "username")
                    {
                        username = details[1];
                    }
                    else if (details[0] == "password")
                    {
                        password = details[1];
                    }
                }

                return process.ExitCode;
            }
            catch (Exception e)
            {
                errors = e.Message;
                return -1;
            }
        }
        
        private int update(string command, string url, string username, string password, out string errors)
        {
            errors = null;

            try
            {
                ProcessStartInfo startInfo = new ProcessStartInfo
                {
                    FileName = "git",
                    Arguments = "credential " + command,
                    UseShellExecute = false,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    CreateNoWindow = true,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true
                };

                Process process = new Process
                {
                    StartInfo = startInfo
                };

                process.Start();

                // Write input parameters to stdin
                process.StandardInput.NewLine = "\n";
                process.StandardInput.WriteLine($"url={url}");
                         
                if (username != null)
                    process.StandardInput.WriteLine($"username={username}");

                if (password != null)
                    process.StandardInput.WriteLine($"password={password}");

                process.StandardInput.WriteLine();

                // Capture any errors
                string line = null;
                while ((line = process.StandardError.ReadLine()) != null)
                {
                    if (errors != null)
                        errors += Environment.NewLine;
                    errors += line;
                }

                return process.ExitCode;
            }
            catch (Exception e)
            {
                errors = e.Message;
                return -1;
            }
        }

        public int approve(string url, string username, string password, out string errors)
        {
            return update("approve", url, username, password, out errors);
        }

        public int reject(string url, out string errors)
        {
            return update("reject", url, null, null, out errors);
        }
    }
}