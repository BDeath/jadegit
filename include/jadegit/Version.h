#pragma once
#include <string>

namespace JadeGit
{
	class Version
	{
	public:
		int major, minor, revision, build;

		constexpr Version(int major, int minor = 99, int revision = 99, int build = 0) : major(major), minor(minor), revision(revision), build(build)
		{
		}

		constexpr Version() : Version(0, 0, 0)
		{
		}

		Version(const char* version);
		Version(const std::string& version);

		operator std::string() const;

		bool empty() const;

		auto operator<=>(const Version&) const = default;
	};

	class AnnotatedVersion : public Version
	{
	public:
		const char* description;

		constexpr AnnotatedVersion(const char* description, int major = 99, int minor = 99, int revision = 99, int build = 0) : Version(major, minor, revision, build), description(description)
		{
		}

		constexpr AnnotatedVersion() : AnnotatedVersion(nullptr, 0, 0, 0)
		{
		}

		operator std::string() const
		{
			return description;
		}
	};

	constexpr AnnotatedVersion nullVersion;
}