#pragma once
#include "FileSystem.h"

namespace JadeGit
{
	class MemoryFileDirectory;
	class MemoryFileHandle;

	class MemoryFileSystem : public FileSystem
	{
	public:
		MemoryFileSystem();
		MemoryFileSystem(const FileSystem& src);
		~MemoryFileSystem();

		bool isReadOnly() const override { return false; }
		File open(const std::filesystem::path& path) const override;

	protected:
		friend std::ostream& operator<<(std::ostream& os, const MemoryFileSystem& fs);
		friend MemoryFileHandle;
		MemoryFileDirectory* const root;
	};

	std::ostream& operator<<(std::ostream& os, const MemoryFileSystem& fs);
}