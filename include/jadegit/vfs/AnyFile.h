#pragma once
#include <filesystem>

namespace JadeGit
{
	class File;
	class FileSignature;
	class FileSystem;
	class AnyFileIterator;

	class AnyFile
	{
	public: 
		AnyFile();
		virtual ~AnyFile();

		virtual std::unique_ptr<AnyFile> clone() const = 0;
		virtual const FileSystem* vfs() const = 0;

		virtual std::filesystem::path path() const = 0;

		virtual bool exists() const = 0;
		virtual bool isDirectory() const = 0;

		virtual const FileSignature* getSignature(size_t lineFrom, size_t lineTo) const = 0;

		virtual std::unique_ptr<AnyFileIterator> begin() const = 0;

		// Opens child file
		virtual File open(const std::filesystem::path& path) const;

		virtual void remove() = 0;

		// Copies file by reading/writing to new destination
		// Override to handle direct copy when file systems are the same
		virtual void copy(AnyFile& dest) const;

		// Moves file by copying and the removing original
		// Override to handle direct move when file systems are the same
		virtual void move(AnyFile& dest);

		// Renames file by moving
		// Override to handle rename directly
		virtual void rename(const std::filesystem::path& filename);

		// Override to create input/output for reading/writing file
		virtual std::unique_ptr<std::istream> createInputStream(std::ios_base::openmode mode = std::ios_base::in) const = 0;
		virtual std::unique_ptr<std::ostream> createOutputStream(std::ios_base::openmode mode = std::ios_base::out) = 0;
	};
}