#pragma once
#include <string>
#include <vector>

namespace JadeGit
{
	class IProgress
	{
	public:
		bool start(size_t size, const char* task = nullptr);
		bool start(size_t size, const std::string& task) { return start(size, task.c_str()); }
		bool update(size_t completed, const char* task = nullptr);
		bool update(unsigned int completed, const char* task = nullptr) { return update(static_cast<size_t>(completed), task); }
		bool step();
		bool skip();
		bool finish();

		virtual void message(const std::string& message) = 0;
		virtual bool wasCancelled() = 0;

	protected:
		virtual void update(double progress, const char* caption = nullptr) = 0;

	private:
		std::vector<size_t> steps;
		std::vector<size_t> total;
		std::vector<std::string> breadcrumbs;

		double progress(size_t n = 0) const;
		bool refresh(bool full = false);
	};
}