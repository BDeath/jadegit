#pragma once
#include "Entity.h"
#include "RootSchema/ExternalSchemaMapMeta.h"

namespace JadeGit::Data
{
	class ExternalSchemaMap : public Entity
	{
	public:
		using Entity::Entity;

		ObjectValue<SchemaEntity*, &ExternalSchemaMapMeta::schemaEntity> schemaEntity;

		const SchemaEntity& getOriginal() const final;

	private:
		void Accept(EntityVisitor& v) final;
	};

	extern template ObjectValue<SchemaEntity*, &ExternalSchemaMapMeta::schemaEntity>;
}