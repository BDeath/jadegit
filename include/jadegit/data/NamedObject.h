#pragma once
#include "Object.h"
#include "QualifiedName.h"
#include "File.h"
#include "KeyValue.h"

// TODO: Remove direct dependency on tinyxml2 (need to refactor WriteHeader to use FileElement)
#include <jadegit/xml.h>

namespace JadeGit::Data
{
	class INamedObject
	{
	public:
		virtual const std::string& GetName() const = 0;
	};

	template<class TBase>
	class NamedObject : public TBase, public INamedObject
	{
		static_assert(std::is_base_of<Object, TBase>(), "Base component is not an object");

	public:
		template <typename TParent>
		NamedObject(TParent& parent, const Class* dataClass, const char* name) : TBase(parent, dataClass), name(name ? name : std::string())
		{
		}

		KeyValue<std::string> name;
		
		const std::string& GetName() const final
		{
			return name;
		}

		void SetName(std::string value)
		{
			if (name != value)
			{
				name = value;
			}
		}

	protected:
		void LoadHeader(const FileElement& source) override
		{
			TBase::LoadHeader(source);

			// Load name attribute
			if (auto name = source.header("name"))
			{
				if (this->name.empty())
					SetName(name);

				// Verify name matches what was used on construction
				else if (this->name != name)
					throw std::runtime_error(std::format("Name [{}] doesn't match filename", name));
			}
		}

		void WriteHeader(tinyxml2::XMLElement* element, const Object* origin, bool reference) const override
		{
			if(!name.empty())
				element->SetAttribute("name", name.c_str());
		}
	};
}