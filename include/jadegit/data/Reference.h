#pragma once
#include "Property.h"
#include "RootSchema/ExternalReferenceMeta.h"
#include "RootSchema/JadeDynamicExplicitInverseRefMeta.h"
#include "RootSchema/JadeDynamicImplicitInverseRefMeta.h"

namespace JadeGit::Data
{
	class ExternalReferenceMap;
	class ExternalClass;
	class JadeDynamicPropertyCluster;
	class JadeMethod;

	DECLARE_OBJECT_CAST(ExternalReferenceMap);
	DECLARE_OBJECT_CAST(JadeDynamicPropertyCluster);
	DECLARE_OBJECT_CAST(JadeMethod);

	class Reference : public Property
	{
	public:
		Reference(Class* parent, const Class* dataClass, const char* name, Type* type = nullptr);
		
		ObjectValue<JadeMethod*, &ReferenceMeta::constraint> constraint;

		virtual bool isExplicitInverseRef() const = 0;
		virtual bool isImplicitInverseRef() const = 0;

	protected:
		AnyValue* InstantiateValue(Object& object) const final;

	private:
		bool isAttribute() const final { return false; };
		bool isReference() const final { return true; };
	};
	
	class Inverse;

	class ExplicitInverseRef : public Reference
	{
	public:
		ExplicitInverseRef(Class* parent, const Class* dataClass, const char* name, Type* type = nullptr);

		enum ReferenceKind : char
		{
			Peer = 0,
			Parent = 1,
			Child = 2
		};

		enum UpdateMode : char
		{
			Manual = 0,
			Manual_Automatic = 1,
			Automatic = 2,
			Automatic_Deferred = 4,
			Manual_Automatic_Deferred = 5
		};

		Value<bool> inverseNotRequired = false;
		std::vector<Inverse*> inverses;
		Value<ReferenceKind> kind = Peer;
		Value<bool> transientToPersistentAllowed = false;
		Value<UpdateMode> updateMode = Manual_Automatic;

		Inverse* getInverse(const ExplicitInverseRef* reference) const;
		std::vector<const ExplicitInverseRef*> getInverseReferences() const;

		inline bool isAutomatic() const
		{
			return updateMode == UpdateMode::Automatic || updateMode == UpdateMode::Automatic_Deferred;
		}

		inline bool isChild() const
		{
			return kind == ReferenceKind::Child;
		}

		inline bool isParent() const
		{
			return kind == ReferenceKind::Parent;
		}

		inline ExplicitInverseRef& setKind(ReferenceKind kind)
		{
			this->kind = kind;
			return *this;
		}

		inline ExplicitInverseRef& setUpdateMode(UpdateMode mode)
		{
			updateMode = mode;
			return *this;
		}

		inline ExplicitInverseRef& automatic()
		{
			return setUpdateMode(Automatic);
		}

		inline ExplicitInverseRef& child()
		{
			return setKind(Child);
		}

		inline ExplicitInverseRef& inverseUnrequired()
		{
			inverseNotRequired = true;
			return *this;
		}

		inline ExplicitInverseRef& manual()
		{
			return setUpdateMode(Manual);
		}

		inline ExplicitInverseRef& parent()
		{
			return setKind(Parent);
		}

	protected:
		void Accept(EntityVisitor& v) override;
		bool WriteFilter() const override;

	private:
		bool isExplicitInverseRef() const final { return true; }
		bool isImplicitInverseRef() const final { return false; }
	};

	extern template Value<ExplicitInverseRef::ReferenceKind>;
	extern template Value<ExplicitInverseRef::UpdateMode>;
	extern template std::map<ExplicitInverseRef::ReferenceKind, const char*> EnumStrings<ExplicitInverseRef::ReferenceKind>::data;
	extern template std::map<ExplicitInverseRef::UpdateMode, const char*> EnumStrings<ExplicitInverseRef::UpdateMode>::data;

	class Inverse : public Object
	{
	public:
		Inverse(ExplicitInverseRef* left, ExplicitInverseRef* right);
		~Inverse();

		ExplicitInverseRef* const reference = nullptr;
		Inverse* counterpart = nullptr;

		const Inverse& getPrimary() const;
		ExplicitInverseRef::ReferenceKind getReferenceKind() const;
		ExplicitInverseRef::UpdateMode getReferenceUpdateMode() const;

	protected:
		Inverse(ExplicitInverseRef* parent, Inverse* counterpart, const State* state = nullptr);

		void WriteHeader(tinyxml2::XMLElement* element, const Object* origin, bool reference) const override;

	private:
		bool first = false;
	};

	class ExternalReference : public ExplicitInverseRef
	{
	public:
		ExternalReference(ExternalClass* parent, const Class* dataClass, const char* name);

		ObjectValue<ExternalReferenceMap*, &ExternalReferenceMeta::externalSchemaMap> externalSchemaMap;
	};

	class JadeDynamicExplicitInverseRef : public ExplicitInverseRef
	{
	public:
		JadeDynamicExplicitInverseRef(Class* parent, const Class* dataClass, const char* name);

		ObjectValue<JadeDynamicPropertyCluster*, &JadeDynamicExplicitInverseRefMeta::dynamicPropertyCluster> dynamicPropertyCluster;
	};

	class ImplicitInverseRef : public Reference
	{
	public:
		ImplicitInverseRef(Class* parent, const Class* dataClass, const char* name, Type* type = nullptr);

		Value<bool> memberTypeInverse = false;

		bool isFormControlProperty() const;

	protected:
		void Accept(EntityVisitor &v) override;

	private:
		bool isExplicitInverseRef() const final { return false; }
		bool isImplicitInverseRef() const final { return true; }
	};

	class JadeDynamicImplicitInverseRef : public ImplicitInverseRef
	{
	public:
		JadeDynamicImplicitInverseRef(Class* parent, const Class* dataClass, const char* name);

		ObjectValue<JadeDynamicPropertyCluster*, &JadeDynamicImplicitInverseRefMeta::dynamicPropertyCluster> dynamicPropertyCluster;
	};
}