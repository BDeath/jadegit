#pragma once
#include "Object.h"
#include "ObjectValue.h"
#include "Iterator.h"
#include <tinyxml2.h>

namespace JadeGit::Data
{
	class Collection : public Object
	{
	public:
		Collection(Object* parent = nullptr, const Class* dataClass = nullptr);

		virtual void Add(const Any& value) = 0;
		virtual void Clear() = 0;
		virtual std::unique_ptr<Iterator> CreateIterator() const = 0;
		virtual bool Empty() const = 0;
		virtual bool Includes(const Any& value) const = 0;
		virtual void Remove(const Any& value) = 0;
		virtual std::size_t size() const = 0;

		virtual void LoadFor(const Object& object, const tinyxml2::XMLElement& element, bool strict, std::queue<std::future<void>>& tasks) = 0;
		virtual void WriteFor(const Object& object, tinyxml2::XMLElement& element) const = 0;
	};

	template <typename TMember, typename Enable = void>
	class TCollection : public Collection
	{
	public:
		using Collection::Collection;

		using MemberType = TMember;
		
		virtual void Add(MemberType value) = 0;
		virtual void Remove(MemberType value) = 0;
		virtual bool Includes(const MemberType& value) const = 0;

		void Add(const Any& value) final
		{
			Add(value.Get<MemberType>());
		}

		void Remove(const Any& value) final
		{
			Remove(value.Get<MemberType>());
		}

		bool Includes(const Any& value) const final
		{
			return Includes(value.Get<MemberType>());
		}

		void LoadFor(const Object& object, const tinyxml2::XMLElement& source, bool strict, std::queue<std::future<void>>& tasks) final
		{
			const tinyxml2::XMLElement* element = source.FirstChildElement();
			while (element)
			{
				auto text = element->GetText();
				Add(static_cast<Any>(Value(std::string(text ? text : ""))));
				element = element->NextSiblingElement();
			}
		}

		void WriteFor(const Object& object, tinyxml2::XMLElement& dest) const final
		{
			std::unique_ptr<Iterator> iter = CreateIterator();
			Any value;

			while (iter->Next(value))
			{
				tinyxml2::XMLElement* element = dest.GetDocument()->NewElement("value");
				dest.InsertEndChild(element);
				element->SetText(value.ToString().c_str());
			}
		}
	};

	template <typename TMember>
	class TCollection<TMember, typename std::enable_if_t<is_object_reference<TMember>::value>> : public Collection
	{
	public:
		using Collection::Collection;

		using MemberType = TMember;

		virtual void Add(MemberType value) = 0;
		virtual void Remove(MemberType value) = 0;
		virtual bool Includes(const MemberType& value) const = 0;

		void Add(const Any& value) final
		{
			Add(value.Get<MemberType>());
		}

		void Remove(const Any& value) final
		{
			Remove(value.Get<MemberType>());
		}

		bool Includes(const Any& value) const final
		{
			return Includes(value.Get<MemberType>());
		}

		void LoadFor(const Object& object, const tinyxml2::XMLElement& source, bool strict, std::queue<std::future<void>>& tasks) final
		{
			const tinyxml2::XMLElement* element = source.FirstChildElement();
			while (element)
			{
				auto resolver = Object::resolver(element->Name(), &object, element, strict);
				tasks.push(std::async(std::launch::deferred, [this, resolver]() { if (auto object = resolver()) this->Add(static_cast<Any>(Value(object))); } ));
				element = element->NextSiblingElement();
			}
		}

		void WriteFor(const Object& object, tinyxml2::XMLElement& element) const final
		{
			std::unique_ptr<Iterator> iter = CreateIterator();
			Value<Object*> member;

			while (iter->Next(member))
			{
				tinyxml2::XMLElement* child = nullptr;
				member->Write(element, child, &object, true);
			}
		}
	};

	// Value specialization for exclusive collections, which are treated as a base
	template <typename T>
	class Value<T, typename std::enable_if_t<std::is_base_of<Collection, T>::value>> : public BaseValue<Object*>, public T
	{
	public:
		AnyValue* Clone() const final
		{
			return new Value<T*>(const_cast<T*>(static_cast<const T*>(this)));
		}

		operator Object* () const final
		{
			return (Object*)this;
		}

		bool empty() const final
		{
			return T::empty();
		}

		void reset() final
		{
			this->Clear();
		}

	protected:
		Value(const Class* dataClass = nullptr) : T(nullptr, dataClass) {}

	private:
		void Set(const Any& v) final
		{
			throw invalid_cast(typeid(v), typeid(this));
		}

		void Set(const std::string& v) final
		{
			throw invalid_cast(typeid(v), typeid(this));
		}

		std::string ToString() const final
		{
			throw invalid_cast(typeid(this), typeid(std::string));
		}
	};

	class ExplicitInverseRef;

	// Object value specialization for exclusive collections
	template <typename T, auto prop, class TProperty>
	class ObjectValue<T, prop, TProperty, typename std::enable_if_t<std::is_base_of<Collection, T>::value && !std::is_base_of<ExplicitInverseRef, TProperty>::value>> : public ObjectValueProperty<T, prop, TProperty>
	{
	public:
		using ObjectValueProperty<T, prop>::ObjectValueProperty;

		void Add(typename T::MemberType value) final
		{
			T::Add(value);

			// Flag object as modified
			this->getObject().Modified();
		}

		void Clear() final
		{
			if (this->empty())
				return;

			// Clear self
			T::Clear();

			// Flag object as modified
			this->getObject().Modified();
		}

		void Remove(typename T::MemberType value) final
		{
			T::Remove(value);

			// Flag object as modified
			this->getObject().Modified();
		}
	};

	// Object value specialization for exclusive collections with inverse maintenance
	template <typename T, auto prop, class TProperty>
	class ObjectValue<T, prop, TProperty, typename std::enable_if_t<std::is_base_of<Collection, T>::value && std::is_base_of<ExplicitInverseRef, TProperty>::value>> : public ObjectValueProperty<T, prop, TProperty>, public ObjectReference
	{
	public:
		using ObjectValueProperty<T, prop, TProperty>::ObjectValueProperty;

		~ObjectValue()
		{
			if (!inverseMaintenanceSuppressed())
				inverseRemoveAll();
		}

		void Add(typename T::MemberType value) final
		{
			// Add new value to self
			T::Add(value);

			// Add self to new inverse
			inverseAdd(*reinterpret_cast<Object*>(value));

			// Flag object as modified
			this->getObject().Modified();
		}

		void Clear() final
		{
			if (this->empty())
				return;

			// Remove self from all inverses
			inverseRemoveAll();

			// Clear self
			T::Clear();

			// Flag object as modified
			this->getObject().Modified();
		}

		void Remove(typename T::MemberType value) final
		{
			// Remove value from self
			T::Remove(value);

			// Remove self from inverse
			inverseRemove(*reinterpret_cast<Object*>(value));

			// Flag object as modified
			this->getObject().Modified();
		}

	protected:
		void inverseAdd(Object& target) const
		{
			Data::inverseAdd(this->getProperty(), this->getObject(), target);
		}

		void inverseRemove(Object& target) const
		{
			Data::inverseRemove(this->getProperty(), this->getObject(), target);
		}

		void inverseRemoveAll() const
		{
			std::unique_ptr<Iterator> iter = this->CreateIterator();
			Value<typename T::MemberType> value;

			auto& property = this->getProperty();
			auto& object = this->getObject();

			while (iter->Next(value))
				Data::inverseRemove(property, object, value);
		}

	private:
		bool autoAdd(Object& object) final
		{
			if (auto member = cast_object<std::remove_pointer_t<typename T::MemberType>>(&object))
			{
				assert(!T::Includes(member));
				T::Add(member);
				return true;
			}
			return false;
		}
		
		bool autoRemove(Object& object) final
		{
			if (auto member = cast_object<std::remove_pointer_t<typename T::MemberType>>(&object))
			{
				assert(T::Includes(member));
				T::Remove(member);
				return true;
			}
			return false;
		}
	};
}