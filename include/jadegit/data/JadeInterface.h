#pragma once
#include "Type.h"
#include "JadeInterfaceMethod.h"
#include "RootSchema/JadeInterfaceMeta.h"

namespace JadeGit::Data
{
	class JadeInterface : public Type
	{
	public:
		JadeInterface(Schema* parent, const Class* dataClass, const char* name);

		ObjectValue<Set<JadeInterface*>, &JadeInterfaceMeta::subinterfaces> subinterfaces;
		ObjectValue<Array<JadeInterface*>, &JadeInterfaceMeta::superinterfaces> superinterfaces;

		void Accept(EntityVisitor &v) override;

		const JadeInterface& getOriginal() const override;
		const JadeInterface& getRootType() const;

	protected:
		JadeInterface(JadeImportedPackage* parent, const Class* dataClass, const char* name);

	private:
		AnyValue* CreateValue() const final;
		AnyValue* CreateValue(Object& object, const Property& property, bool exclusive) const final;

		bool isInterface() const final { return true; }

		void LoadFor(Object &object, const Property& property, const tinyxml2::XMLElement* source, bool strict, std::queue<std::future<void>>& tasks) const final;
		void WriteFor(const Object &object, const Property& property, tinyxml2::XMLNode& parent) const final;
	};

	extern template ObjectValue<Set<JadeInterface*>, &JadeInterfaceMeta::subinterfaces>;
	extern template ObjectValue<Array<JadeInterface*>, &JadeInterfaceMeta::superinterfaces>;
}