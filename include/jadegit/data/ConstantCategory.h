#pragma once
#include "SchemaEntity.h"
#include "Set.h"
#include "RootSchema/ConstantCategoryMeta.h"

namespace JadeGit::Data
{
	class GlobalConstant;
	class Schema;

	DECLARE_OBJECT_CAST(GlobalConstant)

	class ConstantCategory : public SchemaEntity
	{
	public:
		ConstantCategory(Schema* parent, const Class* dataClass, const char* name);

		ObjectValue<Schema* const, &ConstantCategoryMeta::schema> schema;
		ObjectValue<Set<GlobalConstant*>, &ConstantCategoryMeta::consts> constants;

		void Accept(EntityVisitor &v) override;
	};

	extern template ObjectValue<Schema* const, &ConstantCategoryMeta::schema>;
	extern template ObjectValue<Set<GlobalConstant*>, &ConstantCategoryMeta::consts>;
}