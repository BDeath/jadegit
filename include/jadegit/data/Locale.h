#pragma once
#include "SchemaEntity.h"
#include "Form.h"
#include "TranslatableString.h"
#include "RootSchema/LocaleMeta.h"

namespace JadeGit::Data
{
	class Schema;

	class Locale : public MajorEntity<Locale, SchemaEntity>
	{
	public:
		static const std::filesystem::path subFolder;

		Locale(Schema* parent, const Class* dataClass, const char* name, Locale* cloneOf = nullptr);

		ObjectValue<Schema* const, &LocaleMeta::schema> schema;
		ObjectValue<Locale*, &LocaleMeta::cloneOf> cloneOf;
		ObjectValue<Set<Locale*>, &LocaleMeta::clones> clones;
		EntityDict<Form, &LocaleMeta::forms> forms;
		EntityDict<TranslatableString, &LocaleMeta::translatableStrings> translatableStrings;

		void Accept(EntityVisitor &v) override;

		inline std::string baseLocaleName() const
		{
			return cloneOf ? cloneOf->name : std::string();
		}

		Locale* getSuperLocale() const;

		bool isPrimary() const;
	};

	extern template ObjectValue<Schema* const, &LocaleMeta::schema>;
	extern template ObjectValue<Locale*, &LocaleMeta::cloneOf>;
	extern template ObjectValue<Set<Locale*>, &LocaleMeta::clones>;
	extern template EntityDict<Form, &LocaleMeta::forms>;
	extern template EntityDict<TranslatableString, &LocaleMeta::translatableStrings>;
}