#pragma once
#include "ExternalPropertyMap.h"
#include "RootSchema/ExternalAttributeMapMeta.h"

namespace JadeGit::Data
{
	class ExternalClassMap;
	class ExternalColumn;

	class ExternalAttributeMap : public ExternalPropertyMap
	{
	public:
		ExternalAttributeMap(ExternalClassMap& parent, const Class* dataClass, const char* name);

		ObjectValue<ExternalClassMap* const, &ExternalAttributeMapMeta::classMap> classMap;
		ObjectValue<ExternalColumn*, &ExternalAttributeMapMeta::column> column;
	};
}