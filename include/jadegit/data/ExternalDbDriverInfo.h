#pragma once
#include "Object.h"
#include "ObjectValue.h"
#include "RootSchema/ExternalDbDriverInfoMeta.h"

namespace JadeGit::Data
{
	class ExternalDatabase;

	class ExternalDbDriverInfo : public Object
	{
	public:
		ExternalDbDriverInfo(ExternalDatabase& parent, const Class* dataClass);

		ObjectValue<ExternalDatabase* const, &ExternalDbDriverInfoMeta::database> database;
	};
}