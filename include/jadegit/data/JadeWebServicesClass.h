#pragma once
#include "Class.h"
#include "RootSchema/JadeWebServiceSoapHeaderClassMeta.h"

namespace JadeGit::Data
{
	class JadeWebServicesMethod;

	DECLARE_OBJECT_CAST(JadeWebServicesMethod)

	class JadeWebServicesClass : public Class
	{
	public:
		JadeWebServicesClass(Schema* parent, const Class* dataClass, const char* name, Class* superclass = nullptr);

		Value<std::string> additionalInfo;
		Value<std::string> wsdl;

		void Accept(EntityVisitor& v) override;
	};

	class JadeWebServiceConsumerClass : public JadeWebServicesClass
	{
	public:
		JadeWebServiceConsumerClass(Schema* parent, const Class* dataClass, const char* name, Class* superclass = nullptr);
	
		Value<std::string> endPointURL;
		Value<bool> useAsyncCalls;
		Value<bool> useNewPrimTypes;
		Value<bool> useSOAP12;

		void Accept(EntityVisitor& v) override;
	};

	class JadeWebServiceProviderClass : public JadeWebServicesClass
	{
	public:
		JadeWebServiceProviderClass(Schema* parent, const Class* dataClass, const char* name, Class* superclass = nullptr);

		enum class SecureService : Byte
		{
			Secure = 1,
			NotSecure = 2,
			Default = 4
		};

		Value<SecureService> secureService = SecureService::Default;

		void Accept(EntityVisitor& v) override;
	};

	extern template std::map<JadeWebServiceProviderClass::SecureService, const char*> EnumStrings<JadeWebServiceProviderClass::SecureService>::data;

	class JadeWebServiceSoapHeaderClass : public JadeWebServicesClass
	{
	public:
		JadeWebServiceSoapHeaderClass(Schema* parent, const Class* dataClass, const char* name, Class* superclass = nullptr);

		enum class Direction : char
		{
			Input = 'I',
			Output = 'O',
			IO = 'B'
		};

		Value<std::string> actor;
		Value<Direction> direction = Direction::Input;
		Value<bool> mustUnderstand;
		ObjectValue<Set<JadeWebServicesMethod*>, &JadeWebServiceSoapHeaderClassMeta::_usedInMethods> webServicesMethodRefs;

		void Accept(EntityVisitor& v) override;
	};

	extern template std::map<JadeWebServiceSoapHeaderClass::Direction, const char*> EnumStrings<JadeWebServiceSoapHeaderClass::Direction>::data;
}