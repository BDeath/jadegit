#pragma once
#include "Entity.h"
#include "Enum.h"
#include "ObjectValue.h"
#include "Set.h"
#include <jadegit/data/RootSchema/ApplicationMeta.h>

namespace JadeGit::Data
{
	class JadeExportedPackage;
	class JadeWebServiceManager;
	class Schema;

	DECLARE_OBJECT_CAST(JadeWebServiceManager);

	class Application : public MajorEntity<Application, Entity>
	{
	public:
		static const std::filesystem::path subFolder;

		Application(Schema* parent, const Class* dataClass, const char* name);

		enum class Type : char
		{
			GUI = 'G',
			GUI_No_Forms = 'F',
			Non_GUI = 'S',
			REST_Service = 'R',
			REST_Service_Non_GUI = 'T',
			Web_Enabled = 'W',
			Web_Enabled_Non_GUI = 'N'
		};

		Value<Type> applicationType = Type::GUI;
		Value<bool> defaultApp;
		ObjectValue<Set<JadeExportedPackage*>, &ApplicationMeta::exportedPackages> exportedPackages;
		ObjectValue<JadeWebServiceManager*, &ApplicationMeta::jadeWebServiceManager> webServiceManager;
		ObjectValue<Schema* const, &ApplicationMeta::schema> schema;

		void Accept(EntityVisitor &v) override;
	};

	extern template ObjectValue<Set<JadeExportedPackage*>, &ApplicationMeta::exportedPackages>;
	extern template ObjectValue<JadeWebServiceManager*, &ApplicationMeta::jadeWebServiceManager>;
	extern template ObjectValue<Schema* const, &ApplicationMeta::schema>;
	extern template Value<Application::Type>;
	extern template std::map<Application::Type, const char*> EnumStrings<Application::Type>::data;
}