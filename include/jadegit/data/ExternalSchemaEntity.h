#pragma once
#include "Entity.h"

namespace JadeGit::Data
{
	class ExternalSchemaEntity : public Entity
	{
	public:
		using Entity::Entity;

	private:
		void Accept(EntityVisitor& v) final;
	};
}