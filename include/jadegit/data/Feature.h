#pragma once
#include "SchemaEntity.h"
#include "ObjectValue.h"
#include "RootSchema/FeatureMeta.h"

namespace JadeGit::Data
{
	class ActiveXFeature;
	class Type;
	
	DECLARE_OBJECT_CAST(ActiveXFeature)
	DECLARE_OBJECT_CAST(Type)

	class Feature : public SchemaEntity
	{
	public:
		Feature(Type* parent, const Class* dataClass, const char* name);

		ObjectValue<ActiveXFeature*, &FeatureMeta::activeXFeature> activeXFeature;
		ObjectValue<Type*, &FeatureMeta::schemaType> schemaType;
		ObjectValue<Type*, &FeatureMeta::type> type;

		const Type& getType() const;

	protected:
		Feature(Object* parent, const Class* dataClass, const char* name);
	};

	template <>
	void ObjectValue<Type*, &FeatureMeta::schemaType>::inverseAdd(Object& target) const;

	template <>
	void ObjectValue<Type*, &FeatureMeta::type>::inverseAdd(Object& target) const;

	extern template ObjectValue<ActiveXFeature*, &FeatureMeta::activeXFeature>;
	extern template ObjectValue<Type*, &FeatureMeta::schemaType>;
	extern template ObjectValue<Type*, &FeatureMeta::type>;
}