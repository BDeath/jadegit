#pragma once
#include "JadeExportedPackage.h"
#include "RootSchema/JadeImportedConstantMeta.h"
#include "RootSchema/JadeImportedMethodMeta.h"
#include "RootSchema/JadeImportedPropertyMeta.h"
#include "RootSchema/JadeImportedClassMeta.h"
#include "RootSchema/JadeImportedInterfaceMeta.h"
#include "RootSchema/JadeImportedPackageMeta.h"

namespace JadeGit::Data
{
	class JadeImportedEntity
	{
	public:
		virtual JadeExportedEntity* GetExportedEntity() const = 0;
	};

	class JadeImportedClass;
	class JadeImportedInterface;

	class JadeImportedFeature : public Feature, public JadeImportedEntity
	{
	public:
		JadeImportedFeature(JadeImportedClass* parent, const Class* dataClass, const char* name);
		JadeImportedFeature(JadeImportedInterface* parent, const Class* dataClass, const char* name);

		ObjectValue<JadeImportedClass*, &JadeImportedFeatureMeta::importedClass> importedClass;
		ObjectValue<JadeImportedInterface*, &JadeImportedFeatureMeta::importedInterface> importedInterface;
	};

	extern template ObjectValue<JadeImportedClass*, &JadeImportedFeatureMeta::importedClass>;
	extern template ObjectValue<JadeImportedInterface*, &JadeImportedFeatureMeta::importedInterface>;

	class JadeImportedConstant : public JadeImportedFeature
	{
	public:
		JadeImportedConstant(JadeImportedClass* parent, const Class* dataClass, const char* name);
		JadeImportedConstant(JadeImportedInterface* parent, const Class* dataClass, const char* name);

		ObjectValue<JadeExportedConstant*, &JadeImportedConstantMeta::exportedConstant> exportedConstant;

		void Accept(EntityVisitor& v) override;
		JadeExportedConstant* GetExportedEntity() const override { return exportedConstant; }
	};

	extern template ObjectValue<JadeExportedConstant*, &JadeImportedConstantMeta::exportedConstant>;

	class JadeImportedMethod : public JadeImportedFeature
	{
	public:
		JadeImportedMethod(JadeImportedClass* parent, const Class* dataClass, const char* name);
		JadeImportedMethod(JadeImportedInterface* parent, const Class* dataClass, const char* name);

		ObjectValue<JadeExportedMethod*, &JadeImportedMethodMeta::exportedMethod> exportedMethod;

		void Accept(EntityVisitor& v) override;
		JadeExportedMethod* GetExportedEntity() const override { return exportedMethod; }
	};

	extern template ObjectValue<JadeExportedMethod*, &JadeImportedMethodMeta::exportedMethod>;

	class JadeImportedProperty : public JadeImportedFeature
	{
	public:
		JadeImportedProperty(JadeImportedClass* parent, const Class* dataClass, const char* name);
		JadeImportedProperty(JadeImportedInterface* parent, const Class* dataClass, const char* name);

		ObjectValue<JadeExportedProperty*, &JadeImportedPropertyMeta::exportedProperty> exportedProperty;

		void Accept(EntityVisitor& v) override;
		JadeExportedProperty* GetExportedEntity() const override { return exportedProperty; }
	};

	extern template ObjectValue<JadeExportedProperty*, &JadeImportedPropertyMeta::exportedProperty>;

	class JadeImportedClass : public Class, public JadeImportedEntity
	{
	public:
		JadeImportedClass(JadeImportedPackage* parent, const Class* dataClass, const char* name);
		JadeImportedClass(JadeImportedPackage* parent, JadeExportedClass* exportedClass);

		ObjectValue<JadeExportedClass*, &JadeImportedClassMeta::exportedClass> exportedClass;
		EntityDict<JadeImportedConstant, &JadeImportedClassMeta::importedConstants> importedConstants;
		EntityDict<JadeImportedMethod, &JadeImportedClassMeta::importedMethods> importedMethods;
		EntityDict<JadeImportedProperty, &JadeImportedClassMeta::importedProperties> importedProperties;
		ObjectValue<JadeImportedPackage* const, &JadeImportedClassMeta::package> package;

		void Accept(EntityVisitor& v) override;
		JadeExportedClass* GetExportedEntity() const override { return exportedClass; }
		std::string GetLocalName() const override;
		const Class& getOriginal() const override;
		bool isCopy() const final { return true; }
	};

	extern template ObjectValue<JadeExportedClass*, &JadeImportedClassMeta::exportedClass>;
	extern template EntityDict<JadeImportedConstant, &JadeImportedClassMeta::importedConstants>;
	extern template EntityDict<JadeImportedMethod, &JadeImportedClassMeta::importedMethods>;
	extern template EntityDict<JadeImportedProperty, &JadeImportedClassMeta::importedProperties>;
	extern template ObjectValue<JadeImportedPackage* const, &JadeImportedClassMeta::package>;

	class JadeImportedInterface : public JadeInterface, public JadeImportedEntity
	{
	public:
		JadeImportedInterface(JadeImportedPackage* parent, const Class* dataClass, const char* name);
		JadeImportedInterface(JadeImportedPackage* parent, JadeExportedInterface* exportedInterface);

		ObjectValue<JadeExportedInterface*, &JadeImportedInterfaceMeta::exportedInterface> exportedInterface;
		EntityDict<JadeImportedConstant, &JadeImportedInterfaceMeta::importedConstants> importedConstants;
		EntityDict<JadeImportedMethod, &JadeImportedInterfaceMeta::importedMethods> importedMethods;
		EntityDict<JadeImportedProperty, &JadeImportedInterfaceMeta::importedProperties> importedProperties;
		ObjectValue<JadeImportedPackage* const, &JadeImportedInterfaceMeta::package> package;

		void Accept(EntityVisitor& v) override;
		JadeExportedInterface* GetExportedEntity() const override { return exportedInterface; }
		std::string GetLocalName() const override;
		const JadeInterface& getOriginal() const override;
		bool isCopy() const final { return true; }
	};

	extern template ObjectValue<JadeExportedInterface*, &JadeImportedInterfaceMeta::exportedInterface>;
	extern template EntityDict<JadeImportedConstant, &JadeImportedInterfaceMeta::importedConstants>;
	extern template EntityDict<JadeImportedMethod, &JadeImportedInterfaceMeta::importedMethods>;
	extern template EntityDict<JadeImportedProperty, &JadeImportedInterfaceMeta::importedProperties>;
	extern template ObjectValue<JadeImportedPackage* const, &JadeImportedInterfaceMeta::package>;

	class JadeImportedPackage : public JadePackage
	{
	public:
		JadeImportedPackage(Schema* parent, const Class* dataClass, const char* name, JadeExportedPackage* exportedPackage = nullptr);

		EntityDict<JadeImportedClass, &JadeImportedPackageMeta::classes> classes;
		ObjectValue<JadeExportedPackage*, &JadeImportedPackageMeta::exportedPackage> exportedPackage;
		EntityDict<JadeImportedInterface, &JadeImportedPackageMeta::interfaces> interfaces;

		void Accept(EntityVisitor& v) override;
		JadeExportedPackage* GetExportedPackage();
		Type* getType(const std::string& name) const;

	protected:
		void inferFrom(const JadeExportedPackage* exportedPackage);
		void loaded(bool strict, std::queue<std::future<void>>& tasks) final;
	};

	extern template EntityDict<JadeImportedClass, &JadeImportedPackageMeta::classes>;
	extern template ObjectValue<JadeExportedPackage*, &JadeImportedPackageMeta::exportedPackage>;
	extern template EntityDict<JadeImportedInterface, &JadeImportedPackageMeta::interfaces>;
}