#pragma once
#include "ExternalClass.h"
#include "ExternalClassMap.h"
#include "ExternalCollClass.h"
#include "ExternalCollClassMap.h"
#include "ExternalDbDriverInfo.h"
#include "ExternalDbProfile.h"
#include "ExternalTable.h"
#include <jadegit/data/RootSchema/ExternalDatabaseMeta.h>

namespace JadeGit::Data
{
	class Schema;

	class ExternalDatabase : public MajorEntity<ExternalDatabase, Entity>
	{
	public:
		static const std::filesystem::path subFolder;

		ExternalDatabase(Schema* parent, const Class* dataClass, const char* name);

		EntityDict<ExternalClassMap, &ExternalDatabaseMeta::_classMaps> classMaps;
		ObjectValue<Array<ExternalClass*>, &ExternalDatabaseMeta::_classes> classes;
		EntityDict<ExternalCollClassMap, &ExternalDatabaseMeta::_collClassMaps> collClassMaps;
		ObjectValue<Array<ExternalCollClass*>, &ExternalDatabaseMeta::_collClasses> collClasses;
		ObjectValue<ExternalDbDriverInfo*, &ExternalDatabaseMeta::_driverInfo> driverInfo;
		ObjectValue<ExternalDbProfile*, &ExternalDatabaseMeta::_profile> profile;
		ObjectValue<Schema* const, &ExternalDatabaseMeta::_schema> schema;
		EntityDict<ExternalTable, &ExternalDatabaseMeta::_tables> tables;

		void Accept(EntityVisitor &v) override;

	protected:
		friend class ExternalReferenceMap;
		unsigned short getNextReferenceMapInstanceId();

	private:
		unsigned short referenceMaps = 0;
	};
}