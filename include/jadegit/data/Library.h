#pragma once
#include "SchemaEntity.h"
#include "Set.h"
#include "RootSchema/LibraryMeta.h"

namespace JadeGit::Data
{
	class Routine;
	class Schema;

	DECLARE_OBJECT_CAST(Routine)

	class Library : public SchemaEntity
	{
	public:
		Library(Schema* parent, const Class* dataClass, const char* name);

		ObjectValue<Schema* const, &LibraryMeta::schema> schema;
		ObjectValue<Set<Routine*>, &LibraryMeta::entrypoints> entrypoints;

		void Accept(EntityVisitor &v) override;
	};

	extern template ObjectValue<Schema* const, &LibraryMeta::schema>;
	extern template ObjectValue<Set<Routine*>, &LibraryMeta::entrypoints>;
}