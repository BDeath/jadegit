#pragma once
#include "Object.h"
#include "RootSchema\JadeWebServiceManagerMeta.h"

namespace JadeGit::Data
{
	class Application;

	class JadeWebServiceManager : public Object
	{
	public:
		JadeWebServiceManager(Application& parent, const Class* dataClass);

		ObjectValue<Application* const, &JadeWebServiceManagerMeta::application> application;
	};

	extern template ObjectValue<Application* const, &JadeWebServiceManagerMeta::application>;
}