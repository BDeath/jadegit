#pragma once
#include "EntityDict.h"
#include "RootSchema/RelationalAttributeMeta.h"
#include "RootSchema/RelationalTableMeta.h"
#include "RootSchema/RelationalViewMeta.h"

namespace JadeGit::Data
{
	class RelationalTable;
	class Schema;

	class RelationalView : public MajorEntity<RelationalView, Entity>
	{
	public:
		static const std::filesystem::path subFolder;

		RelationalView(Schema* parent, const Class* dataClass, const char* name);

		ObjectValue<Schema* const, &RelationalViewMeta::schema> schema;
		EntityDict<RelationalTable, &RelationalViewMeta::tables> tables;

		void Accept(EntityVisitor& v) override;
	};

	extern template ObjectValue<Schema* const, &RelationalViewMeta::schema>;
	extern template EntityDict<RelationalTable, &RelationalViewMeta::tables>;

	class RelationalAttribute;

	class RelationalTable : public Entity
	{
	public:
		RelationalTable(RelationalView* parent, const Class* dataClass, const char* name);

		EntityDict<RelationalAttribute, &RelationalTableMeta::attributes> attributes;
		ObjectValue<RelationalView* const, &RelationalTableMeta::rView> view;

		void Accept(EntityVisitor& v) override;
	};

	extern template EntityDict<RelationalAttribute, &RelationalTableMeta::attributes>;
	extern template ObjectValue<RelationalView* const, &RelationalTableMeta::rView>;

	class RelationalAttribute : public Entity
	{
	public:
		RelationalAttribute(RelationalTable* parent, const Class* dataClass, const char* name);

		ObjectValue<RelationalTable* const, &RelationalAttributeMeta::table> table;

		void Accept(EntityVisitor& v) override;
	};

	extern template ObjectValue<RelationalTable* const, &RelationalAttributeMeta::table>;

	class RAMethod : public RelationalAttribute
	{
	public:
		RAMethod(RelationalTable* parent, const Class* dataClass, const char* name);
	};
}