#pragma once
#include "Script.h"

namespace JadeGit::Data
{
	class ScriptElement : public Object
	{
	public:
		ScriptElement(Script* parent, const Class* dataClass);

		Value<Script* const> schemaScript;
	};

	class TypeUsage : public NamedObject<ScriptElement>
	{
	public:
		TypeUsage(Script* parent, const Class* dataClass, const char* name = nullptr);

		Value<Type*> type;

		Type* GetType(bool expected = true) const;
	};

	class Routine;

	class Parameter : public TypeUsage
	{
	public:
		Parameter(Routine* parent, const Class* dataClass = nullptr, const char* name = nullptr);

		enum Usage
		{
			Constant = 0,
			Input = 1,
			IO = 2,
			Output = 3
		};

		Value<int> length = 0;
		Value<Usage> usage = Constant;
		Value<std::string> wsdlName;
	};

	extern template std::map<Parameter::Usage, const char*> EnumStrings<Parameter::Usage>::data;

	class ReturnType : public TypeUsage
	{
	public:
		ReturnType(Routine* parent, const Class* dataClass);

		Value<int> length = 0;
	};
}