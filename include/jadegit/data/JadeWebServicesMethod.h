#pragma once
#include "Method.h"
#include "RootSchema/JadeWebServicesMethodMeta.h"

namespace JadeGit::Data
{
	class JadeWebServiceSoapHeaderClass;

	DECLARE_OBJECT_CAST(JadeWebServiceSoapHeaderClass)

	class JadeWebServicesMethod : public JadeMethod
	{
	public:
		JadeWebServicesMethod(Type* parent, const Class* dataClass, const char* name);

		enum class RPC : char
		{
			None,
			Default = 'D',
			No = 'N',
			Yes = 'Y'
		};

		Value<std::string> inputEncodingStyle;
		Value<std::string> inputNamespace;
		Value<bool> inputUsesEncodedFormat;
		Value<std::string> outputEncodingStyle;
		Value<std::string> outputNamespace;
		Value<bool> outputUsesEncodedFormat;
		Value<std::string> soapAction;
		ObjectValue<Array<JadeWebServiceSoapHeaderClass*>, &JadeWebServicesMethodMeta::soapHeaders> soapHeaders;
		Value<bool> useBareStyle;
		Value<bool> useSoap12;
		Value<RPC> usesRPC = RPC::None;

		void Accept(EntityVisitor& v) override;
	};

	extern template std::map<JadeWebServicesMethod::RPC, const char*> EnumStrings<JadeWebServicesMethod::RPC>::data;
}