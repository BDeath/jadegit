#pragma once
#include "ExternalSchemaMapMeta.h"

namespace JadeGit::Data
{
	class ExternalClassMapMeta : public RootClass<>
	{
	public:
		static const ExternalClassMapMeta& get(const Object& object);
		
		ExternalClassMapMeta(RootSchema& parent, const ExternalSchemaMapMeta& superclass);
	
		ExplicitInverseRef* const attributeMaps;
		ExplicitInverseRef* const database;
		ExplicitInverseRef* const referenceMaps;
		ExplicitInverseRef* const tables;
	};
};
