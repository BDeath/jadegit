#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class JadeWebServiceManagerMeta : public RootClass<>
	{
	public:
		static const JadeWebServiceManagerMeta& get(const Object& object);
		
		JadeWebServiceManagerMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		PrimAttribute* const allowedSchemes;
		PrimAttribute* const useSOAP11;
		PrimAttribute* const useSOAP12;
		ExplicitInverseRef* const application;
		PrimAttribute* const useEncodedFormat;
		PrimAttribute* const provider;
		PrimAttribute* const secureService;
		PrimAttribute* const sessionHandling;
		PrimAttribute* const supportLibrary;
		PrimAttribute* const targetNamespace;
		PrimAttribute* const useHttpGet;
		PrimAttribute* const useHttpPost;
		PrimAttribute* const useRPC;
		PrimAttribute* const versionControl;
	};
};
