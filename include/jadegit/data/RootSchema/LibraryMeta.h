#pragma once
#include "SchemaEntityMeta.h"

namespace JadeGit::Data
{
	class LibraryMeta : public RootClass<>
	{
	public:
		static const LibraryMeta& get(const Object& object);
		
		LibraryMeta(RootSchema& parent, const SchemaEntityMeta& superclass);
	
		ExplicitInverseRef* const entrypoints;
		ExplicitInverseRef* const schema;
	};
};
