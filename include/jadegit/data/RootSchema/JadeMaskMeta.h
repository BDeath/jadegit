#pragma once
#include "PictureMeta.h"

namespace JadeGit::Data
{
	class JadeMaskMeta : public RootClass<GUIClass>
	{
	public:
		static const JadeMaskMeta& get(const Object& object);
		
		JadeMaskMeta(RootSchema& parent, const PictureMeta& superclass);
	
		PrimAttribute* const activeColor;
		PrimAttribute* const alignment;
		PrimAttribute* const canHaveFocus;
		PrimAttribute* const cancel;
		PrimAttribute* const caption;
		PrimAttribute* const captionHeight;
		PrimAttribute* const captionLeft;
		PrimAttribute* const captionTop;
		PrimAttribute* const captionWidth;
		PrimAttribute* const createRegionFromMask;
		PrimAttribute* const default_;
		PrimAttribute* const disabledForeColor;
		PrimAttribute* const pictureFocus;
		PrimAttribute* const pictureFocusDown;
		PrimAttribute* const pictureMask;
		PrimAttribute* const pictureRollOver;
		PrimAttribute* const pictureRollUnder;
		PrimAttribute* const style;
		PrimAttribute* const value;
	};
};
