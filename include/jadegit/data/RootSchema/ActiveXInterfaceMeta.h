#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class ActiveXInterfaceMeta : public RootClass<>
	{
	public:
		static const ActiveXInterfaceMeta& get(const Object& object);
		
		ActiveXInterfaceMeta(RootSchema& parent, const ObjectMeta& superclass);
	};
};
