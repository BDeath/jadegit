#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class ScriptElementMeta : public RootClass<>
	{
	public:
		static const ScriptElementMeta& get(const Object& object);
		
		ScriptElementMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		ExplicitInverseRef* const schemaScript;
	};
};
