#pragma once
#include "ScriptElementMeta.h"

namespace JadeGit::Data
{
	class FeatureUsageMeta : public RootClass<>
	{
	public:
		static const FeatureUsageMeta& get(const Object& object);
		
		FeatureUsageMeta(RootSchema& parent, const ScriptElementMeta& superclass);
	
		PrimAttribute* const assignment;
		ExplicitInverseRef* const feature;
	};
};
