#pragma once
#include "RelationalAttributeMeta.h"

namespace JadeGit::Data
{
	class RAOidMeta : public RootClass<>
	{
	public:
		static const RAOidMeta& get(const Object& object);
		
		RAOidMeta(RootSchema& parent, const RelationalAttributeMeta& superclass);
	};
};
