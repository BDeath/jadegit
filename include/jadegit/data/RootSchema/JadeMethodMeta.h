#pragma once
#include "MethodMeta.h"

namespace JadeGit::Data
{
	class JadeMethodMeta : public RootClass<>
	{
	public:
		static const JadeMethodMeta& get(const Object& object);
		
		JadeMethodMeta(RootSchema& parent, const MethodMeta& superclass);
	
		ExplicitInverseRef* const referenceRefs;
	};
};
