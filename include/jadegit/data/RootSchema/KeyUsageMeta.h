#pragma once
#include "ScriptElementMeta.h"

namespace JadeGit::Data
{
	class KeyUsageMeta : public RootClass<>
	{
	public:
		static const KeyUsageMeta& get(const Object& object);
		
		KeyUsageMeta(RootSchema& parent, const ScriptElementMeta& superclass);
	
		ExplicitInverseRef* const type;
	};
};
