#pragma once
#include "TypeMeta.h"

namespace JadeGit::Data
{
	class JadeGenericTypeMeta : public RootClass<>
	{
	public:
		static const JadeGenericTypeMeta& get(const Object& object);
		
		JadeGenericTypeMeta(RootSchema& parent, const TypeMeta& superclass);
	
		ExplicitInverseRef* const templateInterface;
		PrimAttribute* const variance;
	};
};
