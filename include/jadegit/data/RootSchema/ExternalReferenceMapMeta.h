#pragma once
#include "ExternalPropertyMapMeta.h"

namespace JadeGit::Data
{
	class ExternalReferenceMapMeta : public RootClass<>
	{
	public:
		static const ExternalReferenceMapMeta& get(const Object& object);
		
		ExternalReferenceMapMeta(RootSchema& parent, const ExternalPropertyMapMeta& superclass);
	
		ExplicitInverseRef* const classMap;
		PrimAttribute* const dirnLeftRight;
		ExplicitInverseRef* const leftColumns;
		ImplicitInverseRef* const otherRefMap;
		PrimAttribute* const relType;
		ExplicitInverseRef* const rightColumns;
		PrimAttribute* const wherePredicate;
	};
};
