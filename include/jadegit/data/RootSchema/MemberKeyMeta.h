#pragma once
#include "KeyMeta.h"

namespace JadeGit::Data
{
	class MemberKeyMeta : public RootClass<>
	{
	public:
		static const MemberKeyMeta& get(const Object& object);
		
		MemberKeyMeta(RootSchema& parent, const KeyMeta& superclass);
	
		ExplicitInverseRef* const keyPath;
		ExplicitInverseRef* const property;
	};
};
