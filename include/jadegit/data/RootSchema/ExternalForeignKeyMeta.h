#pragma once
#include "ExternalSchemaEntityMeta.h"

namespace JadeGit::Data
{
	class ExternalForeignKeyMeta : public RootClass<>
	{
	public:
		static const ExternalForeignKeyMeta& get(const Object& object);
		
		ExternalForeignKeyMeta(RootSchema& parent, const ExternalSchemaEntityMeta& superclass);
	
		ImplicitInverseRef* const columns;
		PrimAttribute* const deleteRule;
		ExplicitInverseRef* const table;
		PrimAttribute* const updateRule;
	};
};
