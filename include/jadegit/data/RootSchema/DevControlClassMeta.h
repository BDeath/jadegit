#pragma once
#include "DevelopmentMeta.h"

namespace JadeGit::Data
{
	class DevControlClassMeta : public RootClass<>
	{
	public:
		static const DevControlClassMeta& get(const Object& object);
		
		DevControlClassMeta(RootSchema& parent, const DevelopmentMeta& superclass);
	
		ExplicitInverseRef* const guiClass;
	};
};
