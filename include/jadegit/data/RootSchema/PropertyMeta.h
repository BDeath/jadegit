#pragma once
#include "FeatureMeta.h"

namespace JadeGit::Data
{
	class PropertyMeta : public RootClass<>
	{
	public:
		static const PropertyMeta& get(const Object& object);
		
		PropertyMeta(RootSchema& parent, const FeatureMeta& superclass);
	
		PrimAttribute* const xmlAttribute;
		PrimAttribute* const xmlNillable;
		PrimAttribute* const embedded;
		ExplicitInverseRef* const exportedPropertyRefs;
		PrimAttribute* const isHTMLProperty;
		ExplicitInverseRef* const keyPathRefs;
		ExplicitInverseRef* const keyRefs;
		PrimAttribute* const required;
		PrimAttribute* const webService;
		PrimAttribute* const virtual_;
	};
};
