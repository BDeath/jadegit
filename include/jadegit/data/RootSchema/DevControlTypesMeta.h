#pragma once
#include "DevControlClassMeta.h"

namespace JadeGit::Data
{
	class DevControlTypesMeta : public RootClass<>
	{
	public:
		static const DevControlTypesMeta& get(const Object& object);
		
		DevControlTypesMeta(RootSchema& parent, const DevControlClassMeta& superclass);
	
		PrimAttribute* const bitmap;
		PrimAttribute* const bitmapDisabled;
		PrimAttribute* const bitmapDown;
		PrimAttribute* const bitmapID;
		PrimAttribute* const bitmapOver;
		ExplicitInverseRef* const controlPropsNameDict;
		PrimAttribute* const darkBitmap;
		PrimAttribute* const darkBitmapDisabled;
		PrimAttribute* const darkBitmapDown;
		PrimAttribute* const darkBitmapOver;
		PrimAttribute* const hideFromPainterControlPalette;
		PrimAttribute* const name;
		PrimAttribute* const windowClass;
	};
};
