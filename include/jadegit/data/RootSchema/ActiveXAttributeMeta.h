#pragma once
#include "ActiveXFeatureMeta.h"

namespace JadeGit::Data
{
	class ActiveXAttributeMeta : public RootClass<>
	{
	public:
		static const ActiveXAttributeMeta& get(const Object& object);
		
		ActiveXAttributeMeta(RootSchema& parent, const ActiveXFeatureMeta& superclass);
	};
};
