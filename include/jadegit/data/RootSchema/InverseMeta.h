#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class InverseMeta : public RootClass<>
	{
	public:
		static const InverseMeta& get(const Object& object);
		
		InverseMeta(RootSchema& parent, const ObjectMeta& superclass);
	};
};
