#pragma once
#include "RelationalAttributeMeta.h"

namespace JadeGit::Data
{
	class RARelationIndexMeta : public RootClass<>
	{
	public:
		static const RARelationIndexMeta& get(const Object& object);
		
		RARelationIndexMeta(RootSchema& parent, const RelationalAttributeMeta& superclass);
	
		PrimAttribute* const fromFirstClass;
		PrimAttribute* const propertyName;
	};
};
