#pragma once
#include "TypeMeta.h"

namespace JadeGit::Data
{
	class ClassMeta : public RootClass<>
	{
	public:
		static const ClassMeta& get(const Object& object);
		
		ClassMeta(RootSchema& parent, const TypeMeta& superclass);
	
		PrimAttribute* const xmlInnerClass;
		ExplicitInverseRef* const activeXClass;
		ExplicitInverseRef* const classMapRefs;
		ExplicitInverseRef* const dynamicPropertyClusters;
		ExplicitInverseRef* const exportedClassRefs;
		ExplicitInverseRef* const exposedClassRefs;
		ExplicitInverseRef* const implementedInterfaces;
		PrimAttribute* const instanceVolatility;
		ExplicitInverseRef* const properties;
		ExplicitInverseRef* const subclasses;
		ExplicitInverseRef* const superclass;
		PrimAttribute* const transient;
		PrimAttribute* const webService;
	};
};
