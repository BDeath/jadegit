#pragma once
#include "PrimAttributeMeta.h"

namespace JadeGit::Data
{
	class JadeDynamicPrimAttributeMeta : public RootClass<>
	{
	public:
		static const JadeDynamicPrimAttributeMeta& get(const Object& object);
		
		JadeDynamicPrimAttributeMeta(RootSchema& parent, const PrimAttributeMeta& superclass);
	
		ExplicitInverseRef* const dynamicPropertyCluster;
	};
};
