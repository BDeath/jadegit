#pragma once
#include "ExplicitInverseRefMeta.h"

namespace JadeGit::Data
{
	class JadeDynamicExplicitInverseRefMeta : public RootClass<>
	{
	public:
		static const JadeDynamicExplicitInverseRefMeta& get(const Object& object);
		
		JadeDynamicExplicitInverseRefMeta(RootSchema& parent, const ExplicitInverseRefMeta& superclass);
	
		ExplicitInverseRef* const dynamicPropertyCluster;
	};
};
