#pragma once
#include "ScrollBarMeta.h"

namespace JadeGit::Data
{
	class HScrollMeta : public RootClass<GUIClass>
	{
	public:
		static const HScrollMeta& get(const Object& object);
		
		HScrollMeta(RootSchema& parent, const ScrollBarMeta& superclass);
	};
};
