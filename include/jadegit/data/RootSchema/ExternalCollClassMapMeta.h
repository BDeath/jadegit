#pragma once
#include "ExternalSchemaMapMeta.h"

namespace JadeGit::Data
{
	class ExternalCollClassMapMeta : public RootClass<>
	{
	public:
		static const ExternalCollClassMapMeta& get(const Object& object);
		
		ExternalCollClassMapMeta(RootSchema& parent, const ExternalSchemaMapMeta& superclass);
	
		ExplicitInverseRef* const database;
	};
};
