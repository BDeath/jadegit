#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class DevelopmentMeta : public RootClass<>
	{
	public:
		static const DevelopmentMeta& get(const Object& object);
		
		DevelopmentMeta(RootSchema& parent, const ObjectMeta& superclass);
	};
};
