#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class FrameMeta : public RootClass<GUIClass>
	{
	public:
		static const FrameMeta& get(const Object& object);
		
		FrameMeta(RootSchema& parent, const ControlMeta& superclass);
	
		PrimAttribute* const alignChildren;
		PrimAttribute* const alignContainer;
		PrimAttribute* const alignment;
		PrimAttribute* const allowDocking;
		PrimAttribute* const bevelColor;
		PrimAttribute* const bevelInner;
		PrimAttribute* const bevelInnerWidth;
		PrimAttribute* const bevelOuter;
		PrimAttribute* const bevelOuterWidth;
		PrimAttribute* const bevelShadowColor;
		PrimAttribute* const boundaryBrush;
		PrimAttribute* const boundaryColor;
		PrimAttribute* const boundaryWidth;
		PrimAttribute* const caption;
		PrimAttribute* const clipControls;
		PrimAttribute* const transparent;
		PrimAttribute* const wordWrap;
	};
};
