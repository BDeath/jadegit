#pragma once
#include "JadePackageMeta.h"

namespace JadeGit::Data
{
	class JadeExportedPackageMeta : public RootClass<>
	{
	public:
		static const JadeExportedPackageMeta& get(const Object& object);
		
		JadeExportedPackageMeta(RootSchema& parent, const JadePackageMeta& superclass);
	
		ExplicitInverseRef* const application;
		ExplicitInverseRef* const classes;
		ExplicitInverseRef* const importedPackageRefs;
		ExplicitInverseRef* const interfaces;
	};
};
