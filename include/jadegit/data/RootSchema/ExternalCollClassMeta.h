#pragma once
#include "CollClassMeta.h"

namespace JadeGit::Data
{
	class ExternalCollClassMeta : public RootClass<>
	{
	public:
		static const ExternalCollClassMeta& get(const Object& object);
		
		ExternalCollClassMeta(RootSchema& parent, const CollClassMeta& superclass);
	
		PrimAttribute* const extensionString;
		PrimAttribute* const extensionStringMap;
		ExplicitInverseRef* const externalDatabase;
		ExplicitInverseRef* const externalSchemaMap;
		PrimAttribute* const firstPredicate;
		PrimAttribute* const firstPredicateInfo;
		PrimAttribute* const groupByList;
		PrimAttribute* const havingPredicate;
		PrimAttribute* const havingPredicateInfo;
		PrimAttribute* const includesPredicate;
		PrimAttribute* const includesPredicateInfo;
		PrimAttribute* const keyEqPredicate;
		PrimAttribute* const keyEqPredicateInfo;
		PrimAttribute* const keyGeqPredicate;
		PrimAttribute* const keyGeqPredicateInfo;
		PrimAttribute* const keyGtrPredicate;
		PrimAttribute* const keyGtrPredicateInfo;
		PrimAttribute* const keyLeqPredicate;
		PrimAttribute* const keyLeqPredicateInfo;
		PrimAttribute* const keyLssPredicate;
		PrimAttribute* const keyLssPredicateInfo;
		PrimAttribute* const lastPredicate;
		PrimAttribute* const lastPredicateInfo;
		PrimAttribute* const orderByList;
	};
};
