#pragma once
#include "JadeExportedFeatureMeta.h"

namespace JadeGit::Data
{
	class JadeExportedPropertyMeta : public RootClass<>
	{
	public:
		static const JadeExportedPropertyMeta& get(const Object& object);
		
		JadeExportedPropertyMeta(RootSchema& parent, const JadeExportedFeatureMeta& superclass);
	
		ExplicitInverseRef* const importedPropertyRefs;
		ExplicitInverseRef* const property;
	};
};
