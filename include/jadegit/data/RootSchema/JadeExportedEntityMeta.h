#pragma once
#include "SchemaEntityMeta.h"

namespace JadeGit::Data
{
	class JadeExportedEntityMeta : public RootClass<>
	{
	public:
		static const JadeExportedEntityMeta& get(const Object& object);
		
		JadeExportedEntityMeta(RootSchema& parent, const SchemaEntityMeta& superclass);
	};
};
