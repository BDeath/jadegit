#pragma once
#include "RelationalAttributeMeta.h"

namespace JadeGit::Data
{
	class RARelationKeyMeta : public RootClass<>
	{
	public:
		static const RARelationKeyMeta& get(const Object& object);
		
		RARelationKeyMeta(RootSchema& parent, const RelationalAttributeMeta& superclass);
	
		PrimAttribute* const keyIndex;
	};
};
