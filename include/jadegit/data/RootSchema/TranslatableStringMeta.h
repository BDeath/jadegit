#pragma once
#include "ConstantMeta.h"

namespace JadeGit::Data
{
	class TranslatableStringMeta : public RootClass<>
	{
	public:
		static const TranslatableStringMeta& get(const Object& object);
		
		TranslatableStringMeta(RootSchema& parent, const ConstantMeta& superclass);
	
		ExplicitInverseRef* const locale;
	};
};
