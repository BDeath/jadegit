#pragma once
#include "ConstantMeta.h"

namespace JadeGit::Data
{
	class GlobalConstantMeta : public RootClass<>
	{
	public:
		static const GlobalConstantMeta& get(const Object& object);
		
		GlobalConstantMeta(RootSchema& parent, const ConstantMeta& superclass);
	
		ExplicitInverseRef* const category;
		ExplicitInverseRef* const schema;
	};
};
