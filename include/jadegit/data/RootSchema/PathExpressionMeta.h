#pragma once
#include "RoutineMeta.h"

namespace JadeGit::Data
{
	class PathExpressionMeta : public RootClass<>
	{
	public:
		static const PathExpressionMeta& get(const Object& object);
		
		PathExpressionMeta(RootSchema& parent, const RoutineMeta& superclass);
	};
};
