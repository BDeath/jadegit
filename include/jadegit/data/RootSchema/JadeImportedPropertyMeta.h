#pragma once
#include "JadeImportedFeatureMeta.h"

namespace JadeGit::Data
{
	class JadeImportedPropertyMeta : public RootClass<>
	{
	public:
		static const JadeImportedPropertyMeta& get(const Object& object);
		
		JadeImportedPropertyMeta(RootSchema& parent, const JadeImportedFeatureMeta& superclass);
	
		ExplicitInverseRef* const exportedProperty;
	};
};
