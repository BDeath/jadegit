#pragma once
#include "RelationalAttributeMeta.h"

namespace JadeGit::Data
{
	class RARpsPropertyMeta : public RootClass<>
	{
	public:
		static const RARpsPropertyMeta& get(const Object& object);
		
		RARpsPropertyMeta(RootSchema& parent, const RelationalAttributeMeta& superclass);
	
		PrimAttribute* const excluded;
		PrimAttribute* const guid;
	};
};
