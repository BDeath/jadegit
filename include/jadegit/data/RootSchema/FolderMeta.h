#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class FolderMeta : public RootClass<GUIClass>
	{
	public:
		static const FolderMeta& get(const Object& object);
		
		FolderMeta(RootSchema& parent, const ControlMeta& superclass);
	
		PrimAttribute* const tabActiveColor;
		PrimAttribute* const tabInactiveColor;
		PrimAttribute* const tabsAlignment;
		PrimAttribute* const tabsFixedWidth;
		PrimAttribute* const tabsHeight;
		PrimAttribute* const tabsLines;
		PrimAttribute* const tabsPosition;
		PrimAttribute* const tabsRaggedRight;
		PrimAttribute* const tabsStyle;
	};
};
