#pragma once
#include "ActiveXFeatureMeta.h"

namespace JadeGit::Data
{
	class ActiveXMethodMeta : public RootClass<>
	{
	public:
		static const ActiveXMethodMeta& get(const Object& object);
		
		ActiveXMethodMeta(RootSchema& parent, const ActiveXFeatureMeta& superclass);
	};
};
