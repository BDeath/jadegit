#pragma once
#include "ScriptMeta.h"

namespace JadeGit::Data
{
	class ConstantMeta : public RootClass<>
	{
	public:
		static const ConstantMeta& get(const Object& object);
		
		ConstantMeta(RootSchema& parent, const ScriptMeta& superclass);
	
		ExplicitInverseRef* const constantRefs;
		ExplicitInverseRef* const constantUsages;
		ExplicitInverseRef* const exportedConstantRefs;
	};
};
