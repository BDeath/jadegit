#pragma once
#include "JadeWebServicesClassMeta.h"

namespace JadeGit::Data
{
	class JadeWebServiceSoapHeaderClassMeta : public RootClass<>
	{
	public:
		static const JadeWebServiceSoapHeaderClassMeta& get(const Object& object);
		
		JadeWebServiceSoapHeaderClassMeta(RootSchema& parent, const JadeWebServicesClassMeta& superclass);
	
		ExplicitInverseRef* const _usedInMethods;
		PrimAttribute* const actor;
		PrimAttribute* const mustUnderstand;
		PrimAttribute* const direction;
	};
};
