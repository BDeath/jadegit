#pragma once
#include "ClassMeta.h"

namespace JadeGit::Data
{
	class JadeAnnotationClassMeta : public RootClass<>
	{
	public:
		static const JadeAnnotationClassMeta& get(const Object& object);
		
		JadeAnnotationClassMeta(RootSchema& parent, const ClassMeta& superclass);
	
		PrimAttribute* const _aliasName;
		ImplicitInverseRef* const _contract;
	};
};
