#pragma once
#include "JadeExportedFeatureMeta.h"

namespace JadeGit::Data
{
	class JadeExportedConstantMeta : public RootClass<>
	{
	public:
		static const JadeExportedConstantMeta& get(const Object& object);
		
		JadeExportedConstantMeta(RootSchema& parent, const JadeExportedFeatureMeta& superclass);
	
		ExplicitInverseRef* const constant;
		ExplicitInverseRef* const importedConstantRefs;
	};
};
