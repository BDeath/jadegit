#pragma once
#include "TypeUsageMeta.h"

namespace JadeGit::Data
{
	class ParameterMeta : public RootClass<>
	{
	public:
		static const ParameterMeta& get(const Object& object);
		
		ParameterMeta(RootSchema& parent, const TypeUsageMeta& superclass);
	
		PrimAttribute* const wsdlName;
		PrimAttribute* const length;
		PrimAttribute* const usage;
	};
};
