#pragma once
#include "ClassMeta.h"

namespace JadeGit::Data
{
	class GUIClassMeta : public RootClass<>
	{
	public:
		static const GUIClassMeta& get(const Object& object);
		
		GUIClassMeta(RootSchema& parent, const ClassMeta& superclass);
	
		ImplicitInverseRef* const activeXControlClass;
		ExplicitInverseRef* const controlType;
		PrimAttribute* const registryId;
	};
};
