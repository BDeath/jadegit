#pragma once
#include "AttributeMeta.h"

namespace JadeGit::Data
{
	class PrimAttributeMeta : public RootClass<>
	{
	public:
		static const PrimAttributeMeta& get(const Object& object);
		
		PrimAttributeMeta(RootSchema& parent, const AttributeMeta& superclass);
	
		PrimAttribute* const xmlType;
		PrimAttribute* const precision;
		PrimAttribute* const scaleFactor;
	};
};
