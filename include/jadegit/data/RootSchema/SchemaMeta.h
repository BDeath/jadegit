#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class SchemaMeta : public RootClass<>
	{
	public:
		static const SchemaMeta& get(const Object& object);
		
		SchemaMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		ExplicitInverseRef* const _applications;
		ExplicitInverseRef* const _exposedLists;
		ExplicitInverseRef* const _jadeHTMLDocuments;
		ExplicitInverseRef* const activeXLibraries;
		ExplicitInverseRef* const classes;
		ExplicitInverseRef* const constantCategories;
		ExplicitInverseRef* const consts;
		ExplicitInverseRef* const databases;
		ExplicitInverseRef* const exportedPackages;
		ExplicitInverseRef* const externalDatabases;
		PrimAttribute* const formsManagement;
		ExplicitInverseRef* const functions;
		PrimAttribute* const helpFileName;
		PrimAttribute* const helpKeyword;
		ExplicitInverseRef* const importedPackages;
		ExplicitInverseRef* const interfaces;
		ExplicitInverseRef* const libraries;
		ExplicitInverseRef* const locales;
		PrimAttribute* const name;
		ImplicitInverseRef* const primaryLocale;
		ExplicitInverseRef* const primitives;
		ExplicitInverseRef* const pseudoTypes;
		ExplicitInverseRef* const relationalViews;
		ExplicitInverseRef* const subschemas;
		ExplicitInverseRef* const superschema;
		PrimAttribute* const text;
		ExplicitInverseRef* const userFormats;
	};
};
