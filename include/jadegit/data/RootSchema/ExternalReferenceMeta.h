#pragma once
#include "ExplicitInverseRefMeta.h"

namespace JadeGit::Data
{
	class ExternalReferenceMeta : public RootClass<>
	{
	public:
		static const ExternalReferenceMeta& get(const Object& object);
		
		ExternalReferenceMeta(RootSchema& parent, const ExplicitInverseRefMeta& superclass);
	
		ExplicitInverseRef* const externalSchemaMap;
		PrimAttribute* const joinPredicate;
		PrimAttribute* const joinPredicateInfo;
	};
};
