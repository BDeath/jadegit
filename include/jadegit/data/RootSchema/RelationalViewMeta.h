#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class RelationalViewMeta : public RootClass<>
	{
	public:
		static const RelationalViewMeta& get(const Object& object);
		
		RelationalViewMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		PrimAttribute* const defaultVisibility;
		PrimAttribute* const includeSystemClasses;
		ImplicitInverseRef* const includedObjectFeatures;
		PrimAttribute* const name;
		PrimAttribute* const resyncOnExecute;
		PrimAttribute* const rootClass;
		PrimAttribute* const rpsDatabaseName;
		PrimAttribute* const rpsDatabaseType;
		PrimAttribute* const rpsDefaultConnectionString;
		PrimAttribute* const rpsDefaultPassword;
		PrimAttribute* const rpsDefaultUserName;
		PrimAttribute* const rpsExceptionCreate;
		PrimAttribute* const rpsExceptionDelete;
		PrimAttribute* const rpsExceptionUpdate;
		PrimAttribute* const rpsLoggingOptions;
		PrimAttribute* const rpsShowMethods;
		PrimAttribute* const rpsShowObjectFeatures;
		PrimAttribute* const rpsShowVirtualProperties;
		PrimAttribute* const rpsTopSchemaName;
		PrimAttribute* const rpsUseOidClassInstMap;
		ExplicitInverseRef* const schema;
		PrimAttribute* const securityMethod;
		ExplicitInverseRef* const tables;
		PrimAttribute* const uuid;
	};
};
