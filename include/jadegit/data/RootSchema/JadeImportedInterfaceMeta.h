#pragma once
#include "JadeInterfaceMeta.h"

namespace JadeGit::Data
{
	class JadeImportedInterfaceMeta : public RootClass<>
	{
	public:
		static const JadeImportedInterfaceMeta& get(const Object& object);
		
		JadeImportedInterfaceMeta(RootSchema& parent, const JadeInterfaceMeta& superclass);
	
		ExplicitInverseRef* const exportedInterface;
		ExplicitInverseRef* const importedConstants;
		ExplicitInverseRef* const importedMethods;
		ExplicitInverseRef* const importedProperties;
		PrimAttribute* const incomplete;
		ExplicitInverseRef* const package;
	};
};
