#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class DbClassMapMeta : public RootClass<>
	{
	public:
		static const DbClassMapMeta& get(const Object& object);
		
		DbClassMapMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		ExplicitInverseRef* const database;
		ExplicitInverseRef* const diskClass;
		ExplicitInverseRef* const diskFile;
		PrimAttribute* const mode;
	};
};
