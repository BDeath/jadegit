#pragma once
#include "FeatureMeta.h"

namespace JadeGit::Data
{
	class LocaleFormatMeta : public RootClass<>
	{
	public:
		static const LocaleFormatMeta& get(const Object& object);
		
		LocaleFormatMeta(RootSchema& parent, const FeatureMeta& superclass);
	
		ExplicitInverseRef* const schema;
	};
};
