#pragma once
#include "Type.h"

namespace JadeGit::Data
{
	class PseudoType : public Type
	{
	public:
		PseudoType(Schema* parent, const Class* dataClass, const char* name);

		void Accept(EntityVisitor &v) override;

	private:
		AnyValue* CreateValue() const final;
		AnyValue* CreateValue(Object& object, const Property& property, bool exclusive) const final;

		void LoadFor(Object &object, const Property& property, const tinyxml2::XMLElement* source, bool strict, std::queue<std::future<void>>& tasks) const final;
		void WriteFor(const Object &object, const Property& property, tinyxml2::XMLNode& parent) const final;
	};
}