#pragma once
#include "SchemaEntity.h"

namespace JadeGit::Data
{
	class JadeExportedEntity : public SchemaEntity
	{
	public:
		using SchemaEntity::SchemaEntity;

		virtual std::unique_ptr<QualifiedName> GetOriginalEntityName() const = 0;
	};
}