#pragma once
#include "Entity.h"
#include "ObjectValue.h"
#include "RootSchema/JadeDynamicPropertyClusterMeta.h"

namespace JadeGit::Data
{
	class Class;
	class Type;

	DECLARE_OBJECT_CAST(Type)

	class JadeDynamicPropertyCluster : public Entity
	{
	public:
		JadeDynamicPropertyCluster(Class* parent, const Class* dataClass, const char* name);

		ObjectValue<Type*, &JadeDynamicPropertyClusterMeta::schemaType> schemaType;

		void Accept(EntityVisitor& v) final;
	};
}