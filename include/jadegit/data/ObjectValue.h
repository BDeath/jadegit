#pragma once
#include "Object.h"
#include "ObjectReference.h"
#include <assert.h>

namespace JadeGit::Data
{
	// Object value template which defines base classes
	template <typename T, typename Enable = void>
	class ObjectValueBase : public Value<T>
	{
	public:
		using Value<T>::Value;

		ObjectValueBase(const ObjectValueBase&) = delete;
	};

	// Object value template which retrieves property
	template <auto prop>
	struct ObjectValuePropertyType;

	template <typename TProperty, typename TMetaObject, TProperty* const TMetaObject::* prop>
	struct ObjectValuePropertyType<prop>
	{
		using type = TProperty;
	};

	template <>
	struct ObjectValuePropertyType<nullptr>
	{
		using type = Property;
	};


	// Object value template which defines how to resolve associated object & property
	template <typename T, auto prop, class TProperty = typename ObjectValuePropertyType<prop>::type>
	class ObjectValueProperty;

	template <typename T, class TProperty>
	class ObjectValueProperty<T, nullptr, TProperty> : public ObjectValueBase<T>
	{
	public:
		template <typename... Args>
		ObjectValueProperty(Object& parent, const TProperty& property, Args... args) : ObjectValueBase<T>(args...), object(parent), property(property) {}

		Object& getObject() const
		{
			return object;
		}

		const TProperty& getProperty() const
		{
			return property;
		}

	private:
		Object& object;
		const TProperty& property;
	};

	template <typename T, typename TProperty, typename TMetaObject, TProperty* const TMetaObject::* prop>
	class ObjectValueProperty<T, prop, TProperty> : public ObjectValueBase<T>
	{
	public:
		template <typename... Args>
		ObjectValueProperty(Args... args) : ObjectValueBase<T>(args...) {}

		inline static std::ptrdiff_t offset = 0;

		Object& getObject() const
		{
			assert(offset);
			return *reinterpret_cast<Object*>(size_t(this) - offset);
		}

		const TProperty& getProperty() const
		{
			return *(TMetaObject::get(getObject()).*prop);
		}
	};

	// Default object value implementation
	template <typename T, auto prop, class TProperty = typename ObjectValuePropertyType<prop>::type, typename Enable = void>
	class ObjectValue : public ObjectValueProperty<T, prop, TProperty>
	{
	public:
		using ObjectValueProperty<T, prop, TProperty>::ObjectValueProperty;
	};

	// Object value specialization for one-sided object reference
	template <typename T, auto prop, class TProperty>
	class ObjectValue<T, prop, TProperty, typename std::enable_if_t<is_object_reference<T>::value && !std::is_const<T>::value>> : public ObjectValueProperty<T, prop, TProperty>, public ObjectReference
	{
	public:
		template <auto p = prop> requires std::is_null_pointer<decltype(p)>::value
		ObjectValue(Object& parent, const TProperty& property, T value = nullptr) : ObjectValueProperty<T, prop, TProperty>(parent, property, value)
		{
			// Add self to initial inverse
			if (value)
				inverseAdd(*value);
		}

		template <auto p = prop> requires !std::is_null_pointer<decltype(p)>::value
		ObjectValue(T value = nullptr) : ObjectValueProperty<T, prop, TProperty>(value)
		{
			// Add self to initial inverse
			if (value)
				inverseAdd(*value);
		}

		~ObjectValue()
		{
			// Remove self from current inverse
			if (!inverseMaintenanceSuppressed())
				if (auto value = static_cast<Object*>(*this))
					inverseRemove(*value);
		}

		ObjectValue& operator=(const T& rhs) final
		{
			// Remove self from previous inverse
			if (!inverseMaintenanceSuppressed())
			{
				if (auto value = static_cast<Object*>(*this))
					inverseRemove(*value);
			}

			// Set new value
			Value<T>::operator=(rhs);

			// Add self to new inverse
			if (rhs && !inverseMaintenanceSuppressed())
				inverseAdd(*reinterpret_cast<Object*>(rhs));

			return *this;
		}

		ObjectValue& operator=(std::remove_pointer_t<T>& rhs)
		{
			return operator=(&rhs);
		}

	protected:
		bool autoAdd(Object& object) final
		{
			if (auto member = cast_object<std::remove_pointer_t<T>>(&object))
			{
				if (static_cast<T>(*this))
					throw std::logic_error("Duplicated relationship");

				Value<T>::operator=(member);
				return true;
			}
			return false;
		}

		bool autoRemove(Object& member) final
		{
			if (&member != static_cast<Object*>(*this))
				return false;

			Value<T>::operator=(nullptr);
			return true;
		}

		void inverseAdd(Object& target) const
		{
			Data::inverseAdd(this->getProperty(), this->getObject(), target);
		}

		void inverseRemove(Object& target) const
		{
			Data::inverseRemove(this->getProperty(), this->getObject(), target);
		}
	};

	// Object value specialization for one-sided const object reference (which can only be set on initialization)
	template <typename T, auto prop, class TProperty>
	class ObjectValue<T, prop, TProperty, typename std::enable_if_t<is_object_reference<T>::value && std::is_const<T>::value>> : public ObjectValueProperty<T, prop>, public ObjectReference
	{
	public:
		template <auto p = prop> requires std::is_null_pointer<decltype(p)>::value
		ObjectValue(Object& parent, const TProperty& property, T value) : ObjectValueProperty<T, prop, TProperty>(parent, property, value)
		{
			// Add self to initial inverse
			if (value)
				inverseAdd(*value);
		}

		template <auto p = prop> requires !std::is_null_pointer<decltype(p)>::value
		ObjectValue(T value) : ObjectValueProperty<T, prop, TProperty>(value)
		{
			// Add self to initial inverse
			if (value)
				inverseAdd(*value);
		}

		~ObjectValue()
		{
			// Remove self from current inverse, subject to removed flag (set implicitly when parents deleted)
			if (!removed && !inverseMaintenanceSuppressed())
				if (auto value = static_cast<Object*>(*this))
					inverseRemove(*value);
		}

	protected:
		bool autoAdd(Object& member) final
		{
			// If re-adding, it should be with same value (i.e. const parent reference)
			assert(&member == static_cast<Object*>(*this));

			// Reset removed flag
			removed = false;

			return true;
		}

		bool autoRemove(Object& member) final
		{
			if (&member != static_cast<Object*>(*this))
				return false;

			// Set flag indicated it's been removed
			removed = true;
			return true;
		}

		void inverseAdd(Object& target) const
		{
			Data::inverseAdd(this->getProperty(), this->getObject(), target);
		}

		void inverseRemove(Object& target) const
		{
			Data::inverseRemove(this->getProperty(), this->getObject(), target);
		}

	private:
		bool removed = false;
	};
}