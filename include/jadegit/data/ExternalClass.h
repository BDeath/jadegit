#pragma once
#include "Class.h"
#include "RootSchema/ExternalClassMeta.h"

namespace JadeGit::Data
{
	class ExternalClassMap;
	class ExternalDatabase;

	class ExternalClass : public Class
	{
	public:
		ExternalClass(Schema* parent, const Class* dataClass, const char* name, Class* superclass = nullptr);

		ObjectValue<ExternalDatabase*, &ExternalClassMeta::externalDatabase> externalDatabase;
		ObjectValue<ExternalClassMap*, &ExternalClassMeta::externalSchemaMap> externalSchemaMap;
	};

	extern template ObjectValue<ExternalDatabase*, &ExternalClassMeta::externalDatabase>;
	extern template ObjectValue<ExternalClassMap*, &ExternalClassMeta::externalSchemaMap>;
}