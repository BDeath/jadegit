#pragma once
#include "Array.h"
#include "Attribute.h"
#include "RootSchema/DateFormatMeta.h"

namespace JadeGit::Data
{
	class Schema;

	class LocaleFormat : public Feature
	{
	public:
		LocaleFormat(Schema* parent, const Class* dataClass, const char* name);

		ObjectValue<Schema* const, &LocaleFormatMeta::schema> schema;
	};

	extern template ObjectValue<Schema* const, &LocaleFormatMeta::schema>;

	class DateFormat : public LocaleFormat
	{
	public:
		DateFormat(Schema* parent, const Class* dataClass, const char* name);

		Value<bool> dayHasLeadingZeros = false;
		Value<int> firstDayOfWeek = 0;
		Value<int> firstWeekOfYear = 0;
		Value<int> longFormatOrder = 0;
		Value<bool> monthHasLeadingZeros = false;
		Value<std::string> separator;
		ObjectValue<Array<std::string>, &DateFormatMeta::shortDayNames> shortDayNames;
		Value<int> shortFormatOrder = 0;
		Value<bool> showFullCentury = false;

		void Accept(EntityVisitor& v) override;
	};

	class NumberFormat : public LocaleFormat
	{
	public:
		NumberFormat(Schema* parent, const Class* dataClass, const char* name);

		Value<int> decimalPlaces = 0;
		Value<std::string> decimalSeperator;
		Value<std::string> groupings;
		Value<int> negativeFormat = 0;	// Enumeration?
		Value<bool> showLeadingZeros = false;
		Value<std::string> thousandSeparator;

		void Accept(EntityVisitor& v) override;
	};

	class CurrencyFormat : public NumberFormat
	{
	public:
		CurrencyFormat(Schema* parent, const Class* dataClass, const char* name);

		Value<int> positiveFormat = 0;	// Enumeration?
		Value<std::string> symbol;

		void Accept(EntityVisitor& v) override;
	};

	class TimeFormat : public LocaleFormat
	{
	public:
		TimeFormat(Schema* parent, const Class* dataClass, const char* name);

		Value<std::string> amText;
		Value<bool> ampmIsSuffix = false;
		Value<bool> is12HourFormat = false;
		Value<std::string> pmText;
		Value<std::string> separator;
		Value<bool> showLeadingZeros = false;
		Value<bool> showSeconds = false;

		void Accept(EntityVisitor& v) override;
	};
}