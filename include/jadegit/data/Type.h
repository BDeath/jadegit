#pragma once
#include "EntityDict.h"
#include "Constant.h"
#include "Method.h"
#include "Property.h"
#include "Set.h"
#include "RootSchema/TypeMeta.h"

namespace JadeGit::Data
{
	class CollClass;
	class JadeImportedPackage;
	class Schema;

	DECLARE_OBJECT_CAST(CollClass)
	DECLARE_OBJECT_CAST(Schema)

	class Type : public MajorEntity<Type, SchemaEntity>
	{
	public:
		static const std::filesystem::path subFolder;

		Type(Schema* parent, const Class* dataClass, const char* name);

		ObjectValue<Set<CollClass*>, &TypeMeta::collClassRefs> collClassRefs;
		ObjectValue<Set<Constant*>, &TypeMeta::constantRefs> constantRefs;
		EntityDict<Constant, &TypeMeta::consts> constants;
		Value<bool> final = false;
		EntityDict<Method, &TypeMeta::methods> methods;
		Value<bool> persistentAllowed = false;
		ObjectValue<Set<Property*>, &TypeMeta::propertyRefs> propertyRefs;
		ObjectValue<Schema*, &TypeMeta::schema> schema;
		Value<bool> sharedTransientAllowed = false;
		Value<bool> subclassPersistentAllowed = false;
		Value<bool> subclassSharedTransientAllowed = false;
		Value<bool> subclassTransientAllowed = false;
		Value<bool> subschemaFinal = false;
		ObjectValue<Set<Type*>, &TypeMeta::subschemaTypes> subschemaTypes;
		ObjectValue<Type*, &TypeMeta::superschemaType> superschemaType;
		Value<bool> transientAllowed = false;

		// Return local name in context of schema, including imported package name if applicable
		virtual std::string GetLocalName() const { return name; }

		// Return original underlying type
		const Type& getOriginal() const override;

		// Resolve root type without a superschema type
		const Type& getRootType() const;

		// Return default/max length for type
		virtual int GetDefaultLength() const;

		// Returns existing type for same schema
		Type* GetExisting(const char* name);

		virtual Feature* getFeature(const std::string& name) const;

		virtual bool inheritsFrom(const Type* type) const;

		template <typename T>
		bool inheritsFromOneOf(T t) const
		{
			return inheritsFrom(t);
		}
		template <typename T, typename... Rest>
		bool inheritsFromOneOf(T t, Rest... rest) const {
			return inheritsFrom(t) || inheritsFromOneOf(rest...);
		}

		virtual bool isCollClass() const { return false; }
		virtual bool isInterface() const { return false; }
		virtual bool isPrimType() const { return false; }

		bool isCopy() const override;
		bool isRootType() const;
		bool isSubschemaCopy() const;

		virtual AnyValue* CreateValue() const = 0;
		virtual AnyValue* CreateValue(Object& object, const Property& property, bool exclusive) const = 0;

	protected:
		Type(JadeImportedPackage* parent, const Class* dataClass, const char* name);

		friend Property;
		virtual void LoadFor(Object &object, const Property& property, const tinyxml2::XMLElement* source, bool strict, std::queue<std::future<void>>& tasks) const = 0;
		virtual void WriteFor(const Object &object, const Property& property, tinyxml2::XMLNode& parent) const = 0;

		void LoadHeader(const FileElement& source) override;

	private:
		Type(Object* parent, const Class* dataClass, const char* name, Schema* schema);
	};

	template <>
	void ObjectValue<Schema*, &TypeMeta::schema>::inverseAdd(Object& target) const;

	extern template ObjectValue<Set<CollClass*>, &TypeMeta::collClassRefs>;
	extern template EntityDict<Constant, &TypeMeta::consts>;
	extern template EntityDict<Method, &TypeMeta::methods>;
	extern template ObjectValue<Set<Property*>, &TypeMeta::propertyRefs>;
	extern template ObjectValue<Schema*, &TypeMeta::schema>;
	extern template ObjectValue<Set<Type*>, &TypeMeta::subschemaTypes>;
	extern template ObjectValue<Type*, &TypeMeta::superschemaType>;
}