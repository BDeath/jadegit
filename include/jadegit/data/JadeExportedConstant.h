#pragma once
#include "JadeExportedFeature.h"
#include "Constant.h"
#include "RootSchema/JadeExportedConstantMeta.h"

namespace JadeGit::Data
{
	class JadeImportedConstant;

	DECLARE_OBJECT_CAST(JadeImportedConstant)

	class JadeExportedConstant : public JadeExportedFeature
	{
	public:
		JadeExportedConstant(JadeExportedType* parent, const Class* dataClass, const char* name);

		ObjectValue<Constant*, &JadeExportedConstantMeta::constant> constant;
		ObjectValue<Set<JadeImportedConstant*>, &JadeExportedConstantMeta::importedConstantRefs> importedConstantRefs;

		void Accept(EntityVisitor& v) override;

		const Constant& getOriginal() const override;

	protected:
		void loaded(bool strict, std::queue<std::future<void>>& tasks) final;
	};

	extern template ObjectValue<Constant*, &JadeExportedConstantMeta::constant>;
	extern template ObjectValue<Set<JadeImportedConstant*>, &JadeExportedConstantMeta::importedConstantRefs>;
}