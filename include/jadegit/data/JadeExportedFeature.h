#pragma once
#include "JadeExportedEntity.h"
#include "ObjectValue.h"
#include "RootSchema/JadeExportedFeatureMeta.h"

namespace JadeGit::Data
{
	class JadeExportedType;

	DECLARE_OBJECT_CAST(JadeExportedType)

	class JadeExportedFeature : public JadeExportedEntity
	{
	public:
		JadeExportedFeature(JadeExportedType* parent, const Class* dataClass, const char* name);

		ObjectValue<JadeExportedType*, &JadeExportedFeatureMeta::exportedType> exportedType;

		std::unique_ptr<QualifiedName> GetOriginalEntityName() const override;
	};

	extern template ObjectValue<JadeExportedType*, &JadeExportedFeatureMeta::exportedType>;
}