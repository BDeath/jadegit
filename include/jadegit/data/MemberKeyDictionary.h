#pragma once
#include "Collection.h"
#include <map>

namespace JadeGit::Data
{
	template <auto TMember>
	struct MemberValueType
	{
		template <typename C, typename T>
		static T GetType(T C::*member);

		typedef decltype(GetType(TMember)) type;
	};

	template <auto TMember>
	struct MemberType
	{
		typedef typename ValueType<typename MemberValueType<TMember>::type>::type type;
	};

	template<typename TMember, int N, auto TKey, auto... TKeys>
	class MemberKeyType : public MemberKeyType<TMember, N + 1, TKeys...>
	{
	public:
		MemberKeyType(const TMember* object) : MemberKeyType(object), value(object->*TKey) {}

		template<typename... Rest>
		MemberKeyType(const typename MemberType<TKey>::type key, Rest... keys) : MemberKeyType(keys...), value(key) {}

		bool operator< (const MemberKeyType& rhs) const
		{
			return value < rhs.value && MemberKeyType::operator<(rhs);
		}

		void Set(int index, const Any& rhs)
		{
			if (index == N)
				value = rhs.Get<typename MemberType<TKey>::type>();
			else
				MemberKeyType::Set(index, rhs);
		}

	private:
		typename MemberType<TKey>::type value;
	};

	template<typename TMember, int N, auto TKey>
	class MemberKeyType<TMember, N, TKey>
	{
	public:
		MemberKeyType(const TMember* object) : value(object->*TKey) {}
		MemberKeyType(const typename MemberType<TKey>::type key) : value(key) {}

		bool operator< (const MemberKeyType& rhs) const
		{
			return value < rhs.value;
		}

		void Set(int index, const Any& rhs)
		{
			if (index == N)
				value = rhs.Get<typename MemberType<TKey>::type>();
			else
				throw std::out_of_range("Invalid key index");
		}

	private:
		typename MemberType<TKey>::type value;
	};

	template <typename TMember, auto... TKeys>
	class MemberKeyDictionary : public TCollection<TMember*>, public std::pmr::map<MemberKeyType<TMember, 0, TKeys...>, typename TCollection<TMember*>::MemberType>
	{
	public:
		using TCollection<TMember*>::TCollection;
		using MemberKey = MemberKeyType<TMember, 0, TKeys...>;

		void Add(typename TCollection<TMember*>::MemberType value) override
		{
			auto entry = this->insert(std::pair<MemberKey, TMember*>(MemberKey(value), value));
			if (!entry.second)
				throw std::runtime_error("Unexpected duplicate");

			Connect<TKeys...>(value);
		}

		void Clear() override
		{
			this->clear();
		}

		std::unique_ptr<Iterator> CreateIterator() const override
		{
			std::unique_ptr<Iterator> iter(new ContainerIterator<std::pmr::map<MemberKey, TMember*>>(*this));
			return iter;
		}

		bool Empty() const override
		{
			return this->empty();
		}

		template<class TDerived = TMember, typename... Keys>
		TDerived* Get(Keys... keys) const
		{
			auto iter = this->find(MemberKey(keys...));
			return iter != this->end() ? dynamic_cast<TDerived*>(iter->second) : nullptr;
		}

		bool Includes(const typename TCollection<TMember*>::MemberType& value) const override
		{
			return this->find(MemberKey(value)) != this->end();
		}

		void Remove(typename TCollection<TMember*>::MemberType value) override
		{
			this->erase(MemberKey(value));
			Disconnect<TKeys...>(value);
		}

		std::size_t size() const final
		{
			return std::pmr::map<MemberKey, typename TCollection<TMember*>::MemberType>::size();
		}

	protected:
		template<auto TMemberKey1, auto TMemberKey2, auto... TMemberKeys>
		void Connect(typename TCollection<TMember*>::MemberType value, int index = 0)
		{
			Connect<TMemberKey1>(value, index);
			Connect<TMemberKey2, TMemberKeys...>(value, index + 1);
		}

		template<auto TMemberKey>
		void Connect(typename TCollection<TMember*>::MemberType value, int index = 0)
		{
			(value->*TMemberKey).OnChange().Connect(this, [=](auto key)
				{
					/* Remove previous entry */
					MemberKey memberKey(value);
					this->erase(memberKey);

					/* Update key to reflect value change */
					memberKey.Set(index, Any(Value(key)));

					/* Add replacement entry */
					auto entry = this->insert(std::pair<MemberKey, TMember*>(memberKey, value));
					if (!entry.second)
						throw std::runtime_error("Unexpected duplicate");
				});
		}

		template<auto TMemberKey1, auto TMemberKey2, auto... TMemberKeys>
		void Disconnect(typename TCollection<TMember*>::MemberType value)
		{
			Disconnect<TMemberKey1>(value);
			Disconnect<TMemberKey2, TMemberKeys...>(value);
		}

		template<auto TMemberKey>
		void Disconnect(typename TCollection<TMember*>::MemberType value)
		{
			(value->*TMemberKey).OnChange().Disconnect(this);
		}
	};
}