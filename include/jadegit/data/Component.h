#pragma once
#include <jadegit/MemoryAllocated.h>

namespace JadeGit::Data
{
	class Assembly;

	class Component : public MemoryAllocated
	{
	public:
		virtual ~Component() = default;

		Assembly& getAssembly() const;
		Component* getParent() const;
		int getLevel() const;

	protected:
		Component* const parent;

		Component(Assembly& parent);
		Component(Component* parent);

	private:
		Component(const Component&) = delete;
	};
}