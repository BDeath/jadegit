#pragma once
#include "Collection.h"
#include "Iterator.h"
#include <set>

namespace JadeGit::Data
{
	template <typename TMember>
	class Set : public TCollection<TMember>, public std::pmr::set<typename TCollection<TMember>::MemberType>
	{
	public:
		using TCollection<TMember>::TCollection;

		void Add(typename TCollection<TMember>::MemberType value) override
		{
			if (!value)
				throw std::logic_error("Cannot add null entry to set");
			
			this->insert(value);
		}

		void Clear() override
		{
			this->clear();
		}

		std::unique_ptr<Iterator> CreateIterator() const override
		{
			std::unique_ptr<Iterator> iter(new ContainerIterator<std::pmr::set<TCollection<TMember>::MemberType>>(*this));
			return iter;
		}

		bool Empty() const override
		{
			return this->empty();
		}

		bool Includes(const typename TCollection<TMember>::MemberType& value) const override
		{
			return this->find(value) != this->end();
		}

		void Remove(typename TCollection<TMember>::MemberType value) override
		{
			this->erase(value);
		}

		std::size_t size() const final
		{
			return std::pmr::set<typename TCollection<TMember>::MemberType>::size();
		}
	};
}