#pragma once
#include "Source.h"
#include <jadegit/vfs/FileSystem.h>

namespace JadeGit::Build
{
	class FileSource : public Source
	{
	public:
		FileSource(std::shared_ptr<FileSystem> previous, std::shared_ptr<FileSystem> latest);
		FileSource(std::shared_ptr<FileSystem> latest);

		bool read(Source::Reader reader, IProgress* progress) const override;

		const FileSystem* Previous() const override;
		const FileSystem* Latest() const override;

	private:
		const std::shared_ptr<FileSystem> previous;
		const std::shared_ptr<FileSystem> latest;
	};
}