#pragma once
#include <jadegit/Progress.h>
#include <jadegit/vfs/FileSystem.h>
#include <functional>

namespace JadeGit::Build
{
	class Source
	{
	public:
		using Reader = std::function<void(const File&, bool)>;

		virtual ~Source() {}

		virtual bool read(Reader reader, IProgress* progress = nullptr) const = 0;
		virtual const FileSystem* Previous() const = 0;
		virtual const FileSystem* Latest() const = 0;
	};
}