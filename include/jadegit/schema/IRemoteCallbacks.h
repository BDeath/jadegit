#pragma once
#include <git2/remote.h>

namespace JadeGit::Schema
{
	class IRemoteCallbacks
	{
	public:
		virtual int credentials(git_credential** cred, const char* url, const char* username_from_url, unsigned int allowed_types) = 0;
		virtual int sideband_progress(const char* str, int len) = 0;
		virtual int transfer_progress(const git_indexer_progress* stats) = 0;
		virtual int push_transfer_progress(unsigned int current, unsigned int total, size_t bytes) = 0;
	};
}