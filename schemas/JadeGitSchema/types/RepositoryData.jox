<Class name="RepositoryData" id="0917d534-c4a6-4597-a835-d0c4d8f810b2">
    <implementedInterfaces>
        <JadeInterface name="IGitRepositoryData"/>
    </implementedInterfaces>
    <superclass name="Data"/>
    <final>true</final>
    <persistentAllowed>true</persistentAllowed>
    <subclassPersistentAllowed>true</subclassPersistentAllowed>
    <subclassSharedTransientAllowed>true</subclassSharedTransientAllowed>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <DbClassMap file="GitRepository"/>
    <Constant name="Branch_All" id="bafdeede-47cc-41ce-9f59-1d8a65810fdd">
        <source>3</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="Branch_Local" id="0679c752-8c90-42af-a1e7-ea9700949302">
        <source>1</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="Branch_Remote" id="47422b12-8e43-46d8-b5a6-c85e9f6045c9">
        <source>2</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="State_Active" id="84ed310f-7959-4d66-afc9-a2c839fb1232">
        <source>(2 ^ 0).Byte</source>
        <type name="Byte"/>
    </Constant>
    <ExplicitInverseRef name="commits" id="259cbd18-0597-4017-9596-c4e5eb61509a">
        <kind>parent</kind>
        <updateMode>automatic</updateMode>
        <type name="GitCommitDict"/>
        <access>readonly</access>
        <Inverse name="ObjectData::repo"/>
    </ExplicitInverseRef>
    <ExplicitInverseRef name="deployment" id="7a520860-7ae9-4728-9b35-45c81d5a2499">
        <kind>parent</kind>
        <updateMode>automatic</updateMode>
        <embedded>true</embedded>
        <type name="GitDeployment"/>
        <access>readonly</access>
        <Inverse name="GitDeployment::repo"/>
    </ExplicitInverseRef>
    <PrimAttribute name="name" id="d0ff2e1c-5a0a-4a7f-8b3a-3dd801084c7a">
        <length>101</length>
        <embedded>true</embedded>
        <type name="String"/>
        <access>readonly</access>
    </PrimAttribute>
    <ExplicitInverseRef name="objects" id="775d3b40-236d-4fae-b594-0bc4569fd628">
        <kind>parent</kind>
        <updateMode>automatic</updateMode>
        <type name="GitObjectDict"/>
        <access>readonly</access>
        <Inverse name="ObjectData::repo"/>
    </ExplicitInverseRef>
    <ExplicitInverseRef name="references" id="71d4fdd4-bd13-4124-a74a-9cb2ae62dd35">
        <kind>parent</kind>
        <updateMode>automatic</updateMode>
        <type name="ReferenceNodeDict"/>
        <access>readonly</access>
        <Inverse name="ReferenceNode::parent"/>
    </ExplicitInverseRef>
    <ExplicitInverseRef name="root" id="28d169f3-7ccb-4502-8a38-9a3fcc82773b">
        <kind>child</kind>
        <updateMode>manual</updateMode>
        <embedded>true</embedded>
        <type name="Root"/>
        <access>protected</access>
        <Inverse name="Root::repositories"/>
    </ExplicitInverseRef>
    <ExplicitInverseRef name="schemas" id="85a92532-2e57-4ad5-83b0-84c9e13f417e">
        <kind>parent</kind>
        <updateMode>automatic</updateMode>
        <constraint name="GitSchema::isActive"/>
        <type name="GitSchemaDict"/>
        <access>readonly</access>
        <Inverse name="GitSchema::repo"/>
    </ExplicitInverseRef>
    <PrimAttribute name="state" id="3d275e27-a414-45bc-b526-188aa512ef6a">
        <embedded>true</embedded>
        <type name="Byte"/>
        <access>protected</access>
    </PrimAttribute>
    <ExplicitInverseRef name="worktrees" id="5fd6c141-b511-42cf-a617-9d7ef644fff1">
        <kind>parent</kind>
        <updateMode>automatic</updateMode>
        <type name="WorktreeDataSet"/>
        <access>readonly</access>
        <Inverse name="WorktreeData::repo"/>
    </ExplicitInverseRef>
    <PrimAttribute name="number" id="39150385-e1e3-49dd-ab8e-12ed86db838a">
        <embedded>true</embedded>
        <type name="Integer"/>
        <access>readonly</access>
    </PrimAttribute>
    <ExplicitInverseRef name="config" id="53b02baa-a6b7-4873-af0f-6c1911b7a464">
        <kind>parent</kind>
        <updateMode>automatic</updateMode>
        <type name="ConfigDataDict"/>
        <access>readonly</access>
        <Inverse name="ConfigData::parent"/>
    </ExplicitInverseRef>
    <ExternalMethod name="clone" id="dc224636-10bb-4539-81eb-367b72973c38">
        <entrypoint>jadegit_repo_clone</entrypoint>
        <library name="jadegitscm"/>
        <options>4</options>
        <source>clone(name: String; remote: String; load: Boolean): Task is jadegit_repo_clone in jadegitscm typeMethod;
</source>
        <Parameter name="name">
            <type name="String"/>
        </Parameter>
        <Parameter name="remote">
            <type name="String"/>
        </Parameter>
        <Parameter name="load">
            <type name="Boolean"/>
        </Parameter>
        <ReturnType>
            <type name="Task"/>
        </ReturnType>
    </ExternalMethod>
    <JadeMethod name="create" id="474ce341-afd9-4c12-ad46-f858f65ef177">
        <updating>true</updating>
        <source>create() updating, protected;

vars
	last : RepositoryData;
	
begin	
	// Determine next sequence number
	// This is used as an index for dynamic controls instead of instance ID which can be duplicated when deleting/creating last instance
	number := system.getSystemSequenceNumberNext(RepositoryData::number.__fullyQualifiedName).Integer;
	if number = 0 then
		last := RepositoryData.lastInstance;
		if last &lt;&gt; null then
			system.createSystemSequenceNumber(RepositoryData::number.__fullyQualifiedName, last.number);
		else
			system.createSystemSequenceNumber(RepositoryData::number.__fullyQualifiedName, 0);
		endif;
		number := system.getSystemSequenceNumberNext(RepositoryData::number.__fullyQualifiedName).Integer;
	endif;
end;
</source>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="getRepository" id="874d08f6-0207-4dee-8f83-5eed1204d89d">
        <interfaceImplements>
            <JadeInterfaceMethod name="IGitRepositoryData::getRepository"/>
        </interfaceImplements>
        <source>getRepository(): RepositoryData protected;

begin
	return self;
end;
</source>
        <access>protected</access>
        <ReturnType>
            <type name="RepositoryData"/>
        </ReturnType>
    </JadeMethod>
    <ExternalMethod name="init" id="55b9c37e-b30e-4fc1-89f2-4bf45cd93796">
        <entrypoint>jadegit_repo_init</entrypoint>
        <library name="jadegitscm"/>
        <options>4</options>
        <source>init(name: String): RepositoryData is jadegit_repo_init in jadegitscm typeMethod;
</source>
        <Parameter name="name">
            <type name="String"/>
        </Parameter>
        <ReturnType>
            <type name="RepositoryData"/>
        </ReturnType>
    </ExternalMethod>
    <JadeMethod name="isActive" id="65aee34d-3bce-44e9-bb97-3b6aea350d0a">
        <condition>true</condition>
        <source>isActive(): Boolean condition;

begin
	return state.bitAnd( State_Active ) &lt;&gt; 0;
end;</source>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <ExternalMethod name="load" id="dbd30edd-99aa-4dc6-b9f3-a1b4b474b165">
        <entrypoint>jadegit_repo_load</entrypoint>
        <library name="jadegitscm"/>
        <source>load(): Task is jadegit_repo_load in jadegitscm;
</source>
        <ReturnType>
            <type name="Task"/>
        </ReturnType>
    </ExternalMethod>
    <ExternalMethod name="reset" id="bc4856e6-7e53-4fa0-9aff-759669371cf8">
        <entrypoint>jadegit_repo_reset</entrypoint>
        <library name="jadegitscm"/>
        <source>reset(): Task is jadegit_repo_reset in jadegitscm;
</source>
        <ReturnType>
            <type name="Task"/>
        </ReturnType>
    </ExternalMethod>
    <ExternalMethod name="unload" id="218c8ecf-d1f0-46aa-9fff-0dd1fbc6d819">
        <entrypoint>jadegit_repo_unload</entrypoint>
        <library name="jadegitscm"/>
        <source>unload(): Task is jadegit_repo_unload in jadegitscm;
</source>
        <ReturnType>
            <type name="Task"/>
        </ReturnType>
    </ExternalMethod>
    <ExternalMethod name="open" id="150e4e89-7644-45c5-9b40-0e99e739b21a">
        <entrypoint>jadegit_repo_open</entrypoint>
        <library name="jadegitscm"/>
        <source>open(): Repository is jadegit_repo_open in jadegitscm;
</source>
        <ReturnType>
            <type name="Repository"/>
        </ReturnType>
    </ExternalMethod>
    <JadeMethod name="worktree" id="ede068fe-331e-4aae-9663-5dc918da55fb">
        <source>worktree(): WorktreeData;

begin
	// TODO: Further refactoring work is needed to support multiple worktrees and determining which is active for a user/session	
	assert("worktrees.size &lt;&gt; 1", worktrees.size = 1);
	return worktrees.first();
end;
</source>
        <ReturnType>
            <type name="WorktreeData"/>
        </ReturnType>
    </JadeMethod>
</Class>
