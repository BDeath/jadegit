<Class name="DiffDelta" id="217ca0bc-869c-46bb-ad46-7a6b22088322">
    <superclass name="GitObject"/>
    <transient>true</transient>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <Constant name="GIT_DELTA_ADDED" id="46769f33-714e-4216-aa3c-37e2dbd5ad7d">
        <source>1</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="GIT_DELTA_DELETED" id="4ae6dee3-58c3-4ddc-90d0-5d3fd98deb06">
        <source>2</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="GIT_DELTA_MODIFIED" id="5643fa55-c396-4650-8a6a-f5e556d97e9a">
        <source>3</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="GIT_DELTA_RENAMED" id="d2c91909-4148-46eb-8344-7070d163a651">
        <source>4</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="GIT_DELTA_UNMODIFIED" id="fc86b099-c0a1-497d-ba36-95220d7b7590">
        <source>0</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="GIT_DELTA_COPIED" id="bbed10c0-11e7-433f-a2fc-a51ae7f391fb">
        <source>5</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="GIT_DELTA_IGNORED" id="39291723-ea95-4d6d-8c4b-12d69075bf9a">
        <source>6</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="GIT_DELTA_UNTRACKED" id="b9fe6747-4a92-4bc3-92db-e06122fc2c64">
        <source>7</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="GIT_DELTA_TYPECHANGE" id="95cbc260-9dfc-4e05-8468-319d0f385ed8">
        <source>8</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="GIT_DELTA_UNREADABLE" id="47b05087-5765-4f7a-90e1-c007c251d1dd">
        <source>9</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="GIT_DELTA_CONFLICTED" id="f8a78923-b0f8-4e38-ae8d-d378f3dcbd58">
        <source>10</source>
        <type name="Integer"/>
    </Constant>
    <PrimAttribute name="flags" id="93d3934e-7f30-4b8e-947c-d0b0bc54bc8f">
        <embedded>true</embedded>
        <type name="Integer"/>
        <access>readonly</access>
    </PrimAttribute>
    <ImplicitInverseRef name="new_file" id="ea0dffde-e8da-425a-a85f-885c899973bb">
        <embedded>true</embedded>
        <type name="DiffFile"/>
        <access>readonly</access>
    </ImplicitInverseRef>
    <PrimAttribute name="nfiles" id="b2341eb0-e05a-48da-8f10-c59d6885ead4">
        <embedded>true</embedded>
        <type name="Integer"/>
        <access>readonly</access>
    </PrimAttribute>
    <ImplicitInverseRef name="old_file" id="fb0d4973-5951-4c6f-a4d5-6899f4f87ece">
        <embedded>true</embedded>
        <type name="DiffFile"/>
        <access>readonly</access>
    </ImplicitInverseRef>
    <PrimAttribute name="similarity" id="5a94948d-bc47-4bb2-809f-93ef88850969">
        <embedded>true</embedded>
        <type name="Integer"/>
        <access>readonly</access>
    </PrimAttribute>
    <PrimAttribute name="status" id="6713a6c3-be03-40a8-97cc-55cd858a023a">
        <embedded>true</embedded>
        <type name="Integer"/>
        <access>readonly</access>
    </PrimAttribute>
    <JadeMethod name="foreach_" id="2b5e65ac-337a-4bca-9fbc-7bdf5a428cd0">
        <source>foreach_(callbacks: IDiffCallbacks);

vars
	repo 	 : Repository;
	old_blob : Blob;
	old_path : String;
	new_blob : Blob;
	new_path : String;
	
begin
	repo := parent.Diff.parent.Repository;
	
	if old_file &lt;&gt; null then
		old_blob := Blob@lookup(repo, old_file.id);
		old_path := old_file.path;
	endif;
	if new_file &lt;&gt; null then
		new_blob := Blob@lookup(repo, new_file.id);
		new_path := new_file.path;
	endif;
	
	Diff@blobs(old_blob, old_path, new_blob, new_path, callbacks);
end;
</source>
        <Parameter name="callbacks">
            <type name="IDiffCallbacks"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="details" id="25fa2039-0499-415f-9ca4-c343c9836fb7">
        <source>details( diffFile	: DiffFile;
		 pathPrefix : String output;
		 pathSuffix : String output 
				   ): Boolean;

vars
	pos		: Integer;
	path	: String;
		
begin
	if diffFile = null then
		return false;
	endif;

	path := diffFile.path;
	pos	 := path.reversePos( "/" );
	if pos &lt;= 1 or pos = path.length then
		pathPrefix	:= "";
		pathSuffix	:= path;
	
	else
		pathPrefix	:= path[1:pos-1];
		pathSuffix	:= path[pos+1:end];
	endif;
		
	return true;
end;
</source>
        <Parameter name="diffFile">
            <type name="DiffFile"/>
        </Parameter>
        <Parameter name="pathPrefix">
            <usage>output</usage>
            <type name="String"/>
        </Parameter>
        <Parameter name="pathSuffix">
            <usage>output</usage>
            <type name="String"/>
        </Parameter>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="statusDescription" id="8cfa92f1-aad6-4a9b-9276-4c73d885637e">
        <source>statusDescription(): String;

vars

begin
	if status = GIT_DELTA_ADDED then
		return "Added";
	elseif status = GIT_DELTA_DELETED then
		return "Deleted";
	elseif status = GIT_DELTA_MODIFIED then
		return "Modified";
	elseif status = GIT_DELTA_RENAMED then
		return "Renamed";
	else
		return "Other";
	endif;
end;
</source>
        <ReturnType>
            <type name="String"/>
        </ReturnType>
    </JadeMethod>
</Class>
