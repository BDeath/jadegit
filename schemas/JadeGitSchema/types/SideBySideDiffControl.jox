<GUIClass name="SideBySideDiffControl" id="686fd577-9494-4e7e-ac55-e42702ae6fa1">
    <superclass name="DiffControl"/>
    <transient>true</transient>
    <sharedTransientAllowed>true</sharedTransientAllowed>
    <subclassSharedTransientAllowed>true</subclassSharedTransientAllowed>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <text>Shows an left and right text control and differences between them.</text>
    <DbClassMap file="_usergui"/>
    <Constant name="MarkerChg" id="55711964-f79a-43b7-abb4-f5ec50189138">
        <source>4</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="MarkerDiff" id="b6bb769c-67e9-456a-8f6e-0eba42475bbd">
        <source>6</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="MarkerLineChg" id="91af6b25-bf4e-4df5-ad28-461e2bf3ef91">
        <source>3</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="MarkerLinePad" id="7fefe9c6-48ca-47c5-b7e2-bd9d547b5ebc">
        <source>1</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="MarkerPad" id="d6a49f2c-9c23-48f7-8ea0-a38eb3360f0b">
        <source>2</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="StyleDiff" id="b87bee26-52f2-4a1e-b6b9-a72ddd0f16cd">
        <source>1</source>
        <type name="Integer"/>
    </Constant>
    <ImplicitInverseRef name="textNew" id="b0a0695e-11d6-43ea-bafe-2257743262d2">
        <embedded>true</embedded>
        <type name="JadeTextEdit"/>
        <access>protected</access>
        <text>Right/new side textbox</text>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="textOld" id="cdf85b0f-e875-4386-b413-3db05c9feafb">
        <embedded>true</embedded>
        <type name="JadeTextEdit"/>
        <access>protected</access>
        <text>Left/old side textbox</text>
    </ImplicitInverseRef>
    <JadeMethod name="formatLines" id="d56ed23e-aaea-41fc-b800-7c1d42c02677">
        <updating>true</updating>
        <source>formatLines() updating, protected;
// build up the formatting rules we will then apply to the controls

vars
	lineOld		: Integer;
	lineNew		: Integer;
	inx			: Integer;
	found		: Boolean;
	inxBack		: Integer;
	newSidePad	: Integer;
	oldSidePad	: Integer;
	
begin
	// declutter the recordings
	while inx &lt; linesNew.size do
		inx		+= 1;
		lineOld	:= linesOld[inx];
		if lineOld = -1 then
			inxBack		:= inx - 1;
			while inxBack &gt; 0 do
				lineNew	:= linesNew[inxBack];
				if lineNew &lt;&gt; -1 then
					break;
				endif;
				found	:= true;
				inxBack	+= -1;
			endwhile;
			if found then
				linesNew[inxBack+1]	:= linesNew[inx];
				linesNew.removeAt(inx);
				linesOld.removeAt(inx);
				inx		+= -1;
			endif;
			found	:= false;
		endif;
	endwhile;

	foreach inx in 1 to linesNew.size do
	
		lineOld	:= linesOld[inx];
		lineNew	:= linesNew[inx];
		
		if lineOld &gt; 0 and lineNew &gt; 0 then
			// lines should be the same
			lineOld	+= oldSidePad;
			lineNew += newSidePad;
			if formatOld.lineText[lineOld] &lt;&gt; formatNew.lineText[lineNew] then
				formatOld.lineFormat[lineOld]		:= DiffFormat.LineChanged;
				formatNew.lineFormat[lineNew]		:= DiffFormat.LineChanged;
			endif;
		
		elseif lineOld = -1 then
			// have to pad the old side
			lineNew += newSidePad;
			formatOld.linePad( lineNew );
			formatNew.lineFormat[ lineNew ]	:= DiffFormat.LineChanged;
			oldSidePad	+= 1;
		
		elseif lineNew = -1 then
			// have to pad the new side
			lineOld	+= oldSidePad;
			formatNew.linePad( lineOld );
			formatOld.lineFormat[ lineOld ]	:= DiffFormat.LineChanged;
			newSidePad += 1;
		
		endif;
	endforeach;
	
	// ensure same #lines either side
	inx		:= formatNew.lineFormat.size;
	while formatOld.lineFormat.size &lt; inx do
		formatOld.linePad( inx );
	endwhile;
	inx		:= formatOld.lineFormat.size;
	while formatNew.lineFormat.size &lt; inx do
		formatNew.linePad( inx );
	endwhile;
end;
</source>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="outlineBuild" id="8921a12f-b179-4738-9369-56d2994cbbdc">
        <updating>true</updating>
        <source>outlineBuild() updating, protected;

vars

begin
	inheritMethod();
	
	// control will be moved into position via resized, this is needed for the outline populate
	outline.height	:= height - OutlinePadScroll*2;
end;
</source>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="resized" id="434aa0c2-6c4a-454f-b56b-697cc8b45b8d">
        <updating>true</updating>
        <source>resized() updating;
// reorientate the left and right textboxes with the overview in the middle

vars
	textWidth	: Real;

begin
	inheritMethod();
	
	textWidth	:= (width - outline.width - OutlinePad * 2) / 2;	
	textOld.move( -1, -1, textWidth + 2, height + 2 );
	outline.move( textOld.left + textOld.width + OutlinePad, textOld.top + OutlinePadScroll, outline.width, height - OutlinePadScroll*2 );
	textNew.move( outline.left + outline.width + OutlinePad, textOld.top, textWidth, height );
end;
</source>
    </JadeMethod>
    <JadeMethod name="textMain" id="fd6e0877-edf3-40ed-a180-d972464e5df8">
        <source>textMain(): JadeTextEdit protected;

begin
	return textNew;
end;
</source>
        <access>protected</access>
        <ReturnType>
            <type name="JadeTextEdit"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="textBuild" id="d64af925-5531-46c4-89fd-027d4e2c35f4">
        <updating>true</updating>
        <source>textBuild() updating, protected;

vars

begin
	delete textNew;
	delete textOld;

	textNew	:= textCreate();
	textOld	:= textCreate();
	textOld.interlockDisplayWith( textNew, 0);	
	
	// formatting for the full line differences

	// change char markers
	textOld.setStyleAttributes( StyleDiff, textOld.fontName, textOld.fontSize, ColourForeRemove, ColourBackRemove, 0, 0, 0, 0 );
	textNew.setStyleAttributes( StyleDiff, textNew.fontName, textNew.fontSize, ColourForeAdd, ColourBackAdd, 0, 0, 0, 0 );
	
	// padding
	textOld.setLinemarkAttributes( MarkerLinePad, JadeTextEdit.SC_MARK_BACKGROUND, ColourForeDefault, ColourBackPad );
	textNew.setLinemarkAttributes( MarkerLinePad, JadeTextEdit.SC_MARK_BACKGROUND, ColourForeDefault, ColourBackPad );
	textOld.setLinemarkAttributes( MarkerPad, JadeTextEdit.SC_MARK_CIRCLE, ColourBackPad, ColourBackPad );
	textNew.setLinemarkAttributes( MarkerPad, JadeTextEdit.SC_MARK_CIRCLE, ColourBackPad, ColourBackPad );	
	// changes
	textOld.setLinemarkAttributes( MarkerLineChg, JadeTextEdit.SC_MARK_BACKGROUND, ColourForeDefault, ColourBackRemove );
	textNew.setLinemarkAttributes( MarkerLineChg, JadeTextEdit.SC_MARK_BACKGROUND, ColourForeDefault, ColourBackAdd );
	textOld.setLinemarkAttributes( MarkerChg, JadeTextEdit.SC_MARK_CIRCLEMINUS, ColourBackRemove, ColourForeRemove );
	textNew.setLinemarkAttributes( MarkerChg, JadeTextEdit.SC_MARK_CIRCLEPLUS, ColourBackAdd, ColourForeAdd );
	// diff lines (amends)
	textOld.setLinemarkAttributes( MarkerDiff, JadeTextEdit.SC_MARK_CIRCLE, ColourForeAdd, ColourBackRemove );
	textNew.setLinemarkAttributes( MarkerDiff, JadeTextEdit.SC_MARK_CIRCLE, ColourForeRemove, ColourBackAdd );	
end;
</source>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="textPopulate" id="14c90107-72b4-48cf-af9d-5104514d9a5b">
        <updating>true</updating>
        <source>textPopulate() updating, protected;
// adds the text to the textboxes and then formats to show the differences

constants
	StyleValue = 1;

vars
	linesPadOld, 
	linesPadNew,
	linesChgOld,
	linesChgNew,
	linesDiff	: IntegerArray;
	fmtOld, 
	fmtNew		: Character;
	diffLine	: DiffLineChars;
    diffPoints 	: PointArray;
    diffPoint	: Point;
	lineStart	: Integer;
	lineNo		: Integer;
	
begin
	inheritMethod();

	textOld.text	:= formatOld.text;
	textNew.text	:= formatNew.text;

	// highlight keywords
	if not rawView then
		textHighlightKeys( textOld );
		textHighlightKeys( textNew );	
	endif;
	
	create diffLine transient;
	
	create linesPadOld transient;
	create linesPadNew transient;
	create linesChgOld transient;
	create linesChgNew transient;
	create linesDiff transient;
		
	foreach lineNo in 1 to formatOld.lineFormat.size do
		fmtOld	:= formatOld.lineFormat[lineNo];
		fmtNew	:= formatNew.lineFormat[lineNo];
		
		if fmtOld = DiffFormat.LineChanged
		and fmtNew = DiffFormat.LineChanged then
			// within the line we'll mark specifically which words are different
			linesDiff.add(lineNo);
			
			diffLine.compare( formatOld.lineText[lineNo], formatNew.lineText[lineNo] );
			
			// old side
			lineStart	:= textOld.getLineStartPosition(lineNo).max(1) - 1;
			diffPoints	:= diffLine.getChangedSections(false);
			foreach diffPoint in diffPoints do
				textOld.setTextRangeToStyle( lineStart+diffPoint.x, diffPoint.y, StyleDiff, StyleValue );
			endforeach;
			
			// new side
			lineStart	:= textNew.getLineStartPosition(lineNo).max(1) - 1;
			diffPoints	:= diffLine.getChangedSections(true);
			foreach diffPoint in diffPoints do
				textNew.setTextRangeToStyle( lineStart+diffPoint.x, diffPoint.y, StyleDiff, StyleValue );
			endforeach;
		else
			// full line formatting
			if fmtOld = DiffFormat.LinePad then
				linesPadOld.add(lineNo);
			elseif fmtOld = DiffFormat.LineChanged then
				linesChgOld.add(lineNo);
			endif;		
			
			if fmtNew = DiffFormat.LinePad then
				linesPadNew.add(lineNo);
			elseif fmtNew = DiffFormat.LineChanged then
				linesChgNew.add(lineNo);
			endif;
		endif;
	endforeach;

	// formatting for the change lines
	
	// textedit control will crash jade if a limit is exceeded, so stay under the threshold
	trimArray( linesPadOld, LineMarkLinesMaxSize );
	trimArray( linesPadNew, LineMarkLinesMaxSize );
	trimArray( linesChgOld, LineMarkLinesMaxSize );
	trimArray( linesChgNew, LineMarkLinesMaxSize );
	trimArray( linesDiff,   LineMarkLinesMaxSize );
		
	textOld.setLinemarkLines( MarkerLinePad, true, true, linesPadOld );
	textNew.setLinemarkLines( MarkerLinePad, true, true, linesPadNew );
	textOld.setLinemarkLines( MarkerPad, true, true, linesPadOld );
	textNew.setLinemarkLines( MarkerPad, true, true, linesPadNew );
	textOld.setLinemarkLines( MarkerLineChg, true, true, linesChgOld );
	textNew.setLinemarkLines( MarkerLineChg, true, true, linesChgNew );
	textOld.setLinemarkLines( MarkerChg, true, true, linesChgOld );
	textNew.setLinemarkLines( MarkerChg, true, true, linesChgNew );
	textOld.setLinemarkLines( MarkerDiff, true, true, linesDiff );
	textNew.setLinemarkLines( MarkerDiff, true, true, linesDiff );
	textOld.setLinemarkLines( MarkerLineChg, true, true, linesDiff );
	textNew.setLinemarkLines( MarkerLineChg, true, true, linesDiff );	

epilog
	delete linesPadOld;
	delete linesPadNew;
	delete linesChgOld;
	delete linesChgNew;
	delete linesDiff;
	delete diffLine;
end;</source>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="firstVisibleLine" id="593455aa-e1a3-476f-9104-6f950921b779">
        <methodInvocation>mapping</methodInvocation>
        <source>firstVisibleLine(set: Boolean; _value: Integer io) mapping;

vars

begin
	inheritMethod(set, _value);
	
	if set then
		if textOld &lt;&gt; null then
			textOld.firstVisibleLine	:= textNew.firstVisibleLine;
		endif;
	endif;
end;</source>
        <Parameter name="set">
            <type name="Boolean"/>
        </Parameter>
        <Parameter name="_value">
            <usage>io</usage>
            <type name="Integer"/>
        </Parameter>
    </JadeMethod>
    <DevControlTypes name="SideBySideDiffControl">
        <hideFromPainterControlPalette>true</hideFromPainterControlPalette>
        <windowClass>AlBaseControl</windowClass>
        <DevControlProperties name="backBrush">
            <cntrlType>picture</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="backBrushStyle">
            <cntrlType>list</cntrlType>
            <optionsList>
                <value>0 - tile</value>
                <value>1 - stretch</value>
                <value>2 - center</value>
                <value>3 - stretch proportional</value>
            </optionsList>
        </DevControlProperties>
        <DevControlProperties name="backColor">
            <cntrlType>color</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="borderColorSingle">
            <cntrlType>color</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="borderStyle">
            <cntrlType>list</cntrlType>
            <optionsList>
                <value>0 - None</value>
                <value>1 - Single</value>
                <value>2 - 3D sunken</value>
                <value>3 - 3D raised</value>
            </optionsList>
        </DevControlProperties>
        <DevControlProperties name="bubbleHelp">
            <cntrlType>multilined-string</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="clipControls">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="description">
            <cntrlType>multilined-string</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="dragCursor">
            <cntrlType>picture</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="enabled">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="focusBackColor">
            <cntrlType>color</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="focusForeColor">
            <cntrlType>color</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="fontBold">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="fontItalic">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="fontName">
            <cntrlType>font</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="fontSize">
            <cntrlType>font-size</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="fontStrikethru">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="fontUnderline">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="foreColor">
            <cntrlType>color</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="height">
            <cntrlType>signed-integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="helpContextId">
            <cntrlType>integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="helpKeyword">
            <cntrlType>string</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="ignoreSkin">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="left">
            <cntrlType>signed-integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="mouseCursor">
            <cntrlType>picture</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="mousePointer">
            <cntrlType>list</cntrlType>
            <optionsList>
                <value> 0 - Default</value>
                <value> 1 - Arrow</value>
                <value> 2 - Cross</value>
                <value> 3 - I-Beam</value>
                <value> 4 - Cursor</value>
                <value> 5 - Size</value>
                <value> 6 - Size NE SW</value>
                <value> 7 - Size N S</value>
                <value> 8 - Size NW SE</value>
                <value> 9 - Size W E</value>
                <value>10 - Up Arrow</value>
                <value>11 - HourGlass</value>
                <value>12 - No Drop</value>
                <value>13 - Drag</value>
                <value>14 - Horizontal line move</value>
                <value>15 - Vertical Line move</value>
                <value>16 - Hand Pointing</value>
            </optionsList>
        </DevControlProperties>
        <DevControlProperties name="name">
            <cntrlType>name</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="parent">
            <cntrlType>parent</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="parentAspect">
            <cntrlType>list</cntrlType>
            <optionsList>
                <value> 0 - None</value>
                <value> 1 - Stretch Right</value>
                <value> 2 - Stretch Bottom</value>
                <value> 3 - Stretch Both</value>
                <value> 4 - Anchor Right</value>
                <value> 6 - Anchor Right &amp; Stretch Bottom</value>
                <value> 8 - Anchor Bottom</value>
                <value> 9 - Anchor Bottom &amp; Stretch Right</value>
                <value>12 - Anchor Both</value>
                <value>16 - Center Horz</value>
                <value>18 - Center Horz &amp; Stretch Bottom</value>
                <value>24 - Center Horz &amp; Anchor bottom</value>
                <value>32 - Center Vert</value>
                <value>33 - Center Vert &amp; Stretch Right</value>
                <value>36 - Center Vert &amp; Anchor Right</value>
                <value>48 - Center Both</value>
            </optionsList>
        </DevControlProperties>
        <DevControlProperties name="parentBottomOffset">
            <cntrlType>signed-integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="parentRightOffset">
            <cntrlType>signed-integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="relativeHeight">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="relativeLeft">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="relativeTop">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="relativeWidth">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="scrollBars">
            <cntrlType>list</cntrlType>
            <optionsList>
                <value>0 - None</value>
                <value>1 - Horizontal</value>
                <value>2 - Vertical</value>
                <value>3 - Both</value>
            </optionsList>
        </DevControlProperties>
        <DevControlProperties name="securityLevelEnabled">
            <cntrlType>integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="securityLevelVisible">
            <cntrlType>integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="show3D">
            <cntrlType>list</cntrlType>
            <optionsList>
                <value>0 - use application default</value>
                <value>1 - Not 3D</value>
                <value>2 - Use 3D</value>
                <value>3 - Use borderStyle</value>
            </optionsList>
        </DevControlProperties>
        <DevControlProperties name="skinCategoryName">
            <cntrlType>string</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="tabIndex">
            <cntrlType>integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="tabStop">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="tag">
            <cntrlType>string</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="top">
            <cntrlType>signed-integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="transparent">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="visible">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="width">
            <cntrlType>signed-integer</cntrlType>
        </DevControlProperties>
    </DevControlTypes>
</GUIClass>
