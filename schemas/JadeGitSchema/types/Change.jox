<Class name="Change" id="1428217d-feec-47d7-8c7a-dde3ac91028a">
    <implementedInterfaces>
        <JadeInterface name="IGitRepositoryData"/>
    </implementedInterfaces>
    <superclass name="Data"/>
    <final>true</final>
    <persistentAllowed>true</persistentAllowed>
    <subclassPersistentAllowed>true</subclassPersistentAllowed>
    <subclassSharedTransientAllowed>true</subclassSharedTransientAllowed>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <DbClassMap file="Git"/>
    <Constant name="Operation_Add" id="1baea77b-0b5d-49b1-9cae-4368cbe09493">
        <source>1.Byte</source>
        <type name="Byte"/>
    </Constant>
    <Constant name="Operation_Delete" id="ba1f1020-9f10-4738-a1b8-36d283622fad">
        <source>2.Byte</source>
        <type name="Byte"/>
    </Constant>
    <Constant name="Operation_Rename" id="fca56be1-0d1e-4f6f-bbba-12ad2e98ab61">
        <source>3.Byte</source>
        <type name="Byte"/>
    </Constant>
    <Constant name="Operation_Update" id="682c2e3c-c41c-4f7a-9450-3a766870d846">
        <source>4.Byte</source>
        <type name="Byte"/>
    </Constant>
    <ExplicitInverseRef name="latest" id="35cc9048-e6d1-487d-8c80-36b7c8797e8d">
        <kind>child</kind>
        <transientToPersistentAllowed>true</transientToPersistentAllowed>
        <updateMode>manual</updateMode>
        <embedded>true</embedded>
        <type name="GitEntity"/>
        <access>readonly</access>
        <Inverse name="GitEntity::changes"/>
    </ExplicitInverseRef>
    <PrimAttribute name="operation" id="e410acef-7a59-45c3-a2e6-05c9300686cd">
        <embedded>true</embedded>
        <type name="Byte"/>
        <access>readonly</access>
    </PrimAttribute>
    <ExplicitInverseRef name="predecessors" id="bee36e2e-bd42-41da-9ace-d923580baa62">
        <type name="GitChangeSet"/>
        <access>readonly</access>
        <Inverse name="successors"/>
    </ExplicitInverseRef>
    <ExplicitInverseRef name="previous" id="fee1abe6-9e0d-40ce-b2ee-649184369432">
        <kind>child</kind>
        <transientToPersistentAllowed>true</transientToPersistentAllowed>
        <updateMode>manual</updateMode>
        <embedded>true</embedded>
        <type name="GitEntity"/>
        <access>readonly</access>
        <Inverse name="GitEntity::changes"/>
    </ExplicitInverseRef>
    <PrimAttribute name="staged" id="c32d0389-fc0e-4faf-9ab4-6d57c093b6e6">
        <embedded>true</embedded>
        <type name="Boolean"/>
        <access>readonly</access>
    </PrimAttribute>
    <ExplicitInverseRef name="successors" id="2976cf4b-a0cd-4ee3-9652-7fe7181fe0ae">
        <type name="GitChangeSet"/>
        <access>readonly</access>
        <Inverse name="predecessors"/>
    </ExplicitInverseRef>
    <ExplicitInverseRef name="worktree" id="dc15412e-1f03-4b55-a26a-8d9704461014">
        <kind>child</kind>
        <updateMode>manual</updateMode>
        <embedded>true</embedded>
        <type name="WorktreeData"/>
        <access>readonly</access>
        <Inverse name="WorktreeData::changes"/>
    </ExplicitInverseRef>
    <JadeMethod name="getName" id="668777ad-5d28-4e53-8513-ec979aa897e2">
        <source>getName(): String;

begin
	if latest &lt;&gt; null then
		return latest.getName() &amp; " - " &amp; getOperation();
	else
		return previous.getName() &amp; " - " &amp; getOperation();
	endif;
end;
</source>
        <ReturnType>
            <type name="String"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="getOperation" id="8e50b946-88bf-4376-99e9-175f644cfb81">
        <source>getOperation(): String;

vars

begin
	if operation = Operation_Add then
		return "add";
	elseif operation = Operation_Delete then
		return "delete";
	elseif operation = Operation_Rename then
		return "rename from " &amp; previous.name;
	elseif operation = Operation_Update then
		return "update";
	else
		return "void";
	endif;
end;
</source>
        <ReturnType>
            <type name="String"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="getSequence" id="21ce4557-a988-4f28-8586-c431ab27bcab">
        <source>getSequence(): String;

vars
	change : Change;
	
begin
	foreach change in predecessors where change.staged = staged and change.operation &lt;&gt; null do
		if previous &lt;&gt; null and previous = change.latest or
		   previous = null and latest = change.previous then
			if change.operation = Operation_Rename and operation = Operation_Rename then
				return change.getSequence();
			else
				return change.getSequence() &amp; ", " &amp; getOperation();
			endif;
			break;
		endif;
	endforeach;

	return getOperation();
end;
</source>
        <ReturnType>
            <type name="String"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="isCurrent" id="0aebc80f-929a-488e-a09e-b03998e88a6b">
        <source>isCurrent(): Boolean;
// Returns true if change is the last in sequence of changes (staged or unstaged)

vars
	change : Change;
	
begin
	foreach change in successors where change.staged = staged and change.operation &lt;&gt; null do
		if (change.previous &lt;&gt; null and change.previous = latest) or
		   (change.previous = null and change.latest = previous) then
			return false;
		endif;
	endforeach;
	
	return true;
end;
</source>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="isVersioned" id="698e1358-ac80-42d9-bf39-47ef14969b8a">
        <source>isVersioned(): Boolean;
// Returns true if change is dependent on versioned entity

begin
	return (latest &lt;&gt; null and latest.isVersioned()) or
		   (previous &lt;&gt; null and previous.isVersioned());
end;
</source>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="repo_" id="ed464821-a706-4b9e-8ccc-1ea8de8b1011">
        <interfaceImplements>
            <JadeInterfaceMethod name="IGitRepositoryData::getRepository"/>
        </interfaceImplements>
        <source>repo_(): RepositoryData protected;

begin
	return worktree.repo;
end;
</source>
        <access>protected</access>
        <ReturnType>
            <type name="RepositoryData"/>
        </ReturnType>
    </JadeMethod>
</Class>
