<Class name="GraphManager" id="7dc03c0c-5675-4fd4-adde-edc4bbe2bb87">
    <superclass name="Presentation"/>
    <transient>true</transient>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <text>Coordinator for the git graph display

The graph is built for the entirety of commits provided.
Importantly the xaml control is built inside a frame and its vertical position moved to match the table scroll.
</text>
    <CompAttribute name="colours" id="f6cfe0bb-ffcb-4a33-af92-f3f0872843fc">
        <type name="StringArray"/>
        <access>protected</access>
        <text>List of all xaml colours which we will use to differentiate branches</text>
    </CompAttribute>
    <PrimAttribute name="controlWidth" id="8c83e6d8-572f-439d-b263-a5b2c92be21f">
        <embedded>true</embedded>
        <type name="Integer"/>
        <access>readonly</access>
        <text>Width of graph, which will be identical to the table column</text>
    </PrimAttribute>
    <ImplicitInverseRef name="frameControl" id="0570a93d-4071-472d-a742-7a2ccf5f5e26">
        <embedded>true</embedded>
        <type name="Frame"/>
        <access>protected</access>
        <text>Covers the full area of the table column and contains the xaml control within it.</text>
    </ImplicitInverseRef>
    <ExplicitInverseRef name="graphControl" id="23736789-1caa-4151-a47d-d184771dc36c">
        <embedded>true</embedded>
        <type name="GraphXaml"/>
        <access>protected</access>
        <text>Xaml control where the graph will be drawn</text>
        <Inverse name="GraphXaml::manager"/>
    </ExplicitInverseRef>
    <PrimAttribute name="maxColumns" id="b1a5cf15-8133-454f-8233-2add43f5118e">
        <embedded>true</embedded>
        <type name="Integer"/>
        <text>Maximum number of columns allowed.
If the table is small and a large number of branches are visible it could otherwise crowd the table content completely.
If the limit is breached, all excess nodes and lines will be displayed in the last column</text>
    </PrimAttribute>
    <PrimAttribute name="maxCommits" id="e6d658ee-dc91-481d-a3be-78835635be87">
        <embedded>true</embedded>
        <type name="Integer"/>
        <text>Limit to how many commits the graph will show.
The graph will rebuild from scratch each time presently, so will be progressively slower as we scroll back.
If the limit is breached, there will be no further graph displayed.</text>
    </PrimAttribute>
    <ImplicitInverseRef name="root" id="9bc51145-7dc9-4b1e-901a-2c99bcfb425f">
        <embedded>true</embedded>
        <type name="GraphModelRoot"/>
        <access>readonly</access>
        <text>Parent node to the graph transient content</text>
    </ImplicitInverseRef>
    <ExplicitInverseRef name="tableControl" id="1c1c3386-072e-4fc7-8aa2-cfcd649aee51">
        <kind>child</kind>
        <embedded>true</embedded>
        <type name="CommitTable"/>
        <access>protected</access>
        <Inverse name="CommitTable::graph"/>
    </ExplicitInverseRef>
    <JadeMethod name="clear" id="4cf78f7a-228d-4ed1-a3ed-333a393c2bfd">
        <updating>true</updating>
        <source>clear() updating;

begin
	delete root;
	create root transient;

	graphControl.xaml	:= null;
end;
</source>
    </JadeMethod>
    <JadeMethod name="clicked" id="f674ccc9-099b-42ee-a362-dcd39bdc53f3">
        <updating>true</updating>
        <source>clicked(index : Integer) updating;
// pass click behaviour back to table

vars
	topCommit	: Commit;
	topRow		: Integer;
	row			: Integer;

begin
	if index &gt; 0 and index &lt;= root.commits.size then
		
		// convert to a row
		topCommit 	:= tableControl.accessRow(2).itemObject.Commit;
		topRow		:= root.commits.indexOf(topCommit);
		row			:= index - topRow + tableControl.fixedRows + 1;

		tableControl.graphClicked(row);
	endif;
end;
</source>
        <Parameter name="index">
            <type name="Integer"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="create" id="083e479d-6467-4038-8e88-d56f41c07211">
        <updating>true</updating>
        <source>create() updating;

begin
	maxColumns	:= 14;
	maxCommits	:= 1500;

	// favourite colours
	colours.add( "Blue" );
	colours.add( "Teal" );
	colours.add( "Brown" );
	colours.add( "DarkGoldenrod" );
	colours.add( "Red" );
	colours.add( "Magenta" );
	colours.add( "DarkOrange" );
	colours.add( "Olive" );
	colours.add( "LimeGreen" );
	
	// best of the rest
	colours.add( "Aquamarine" );
	colours.add( "Bisque" );
	colours.add( "Black" );
	colours.add( "BlueViolet" );
	colours.add( "BurlyWood" );
	colours.add( "CadetBlue" );
	colours.add( "Chartreuse" );
	colours.add( "Chocolate" );
	colours.add( "Coral" );
	colours.add( "CornflowerBlue" );
	colours.add( "Crimson" );
	colours.add( "Cyan" );
	colours.add( "DarkBlue" );
	colours.add( "DarkCyan" );
	colours.add( "DarkOliveGreen" );
	colours.add( "DarkRed" );
	colours.add( "DarkSeaGreen" );
	colours.add( "DarkTurquoise" );
	colours.add( "DeepPink" );
	colours.add( "DimGray" );
	colours.add( "DodgerBlue" );
	colours.add( "ForestGreen" );
	colours.add( "Gold" );
	colours.add( "Goldenrod" );
	colours.add( "GreenYellow" );
	colours.add( "Indigo" );
	colours.add( "LightBlue" );
	colours.add( "Navy" );
	colours.add( "Orange" );
	colours.add( "Peru" );
	colours.add( "Pink" );
	colours.add( "RosyBrown" );
	colours.add( "Salmon" );
	colours.add( "SkyBlue" );
	colours.add( "Violet" );
end;</source>
    </JadeMethod>
    <JadeMethod name="delete" id="0411ef40-3690-4bfd-b443-f04e6cf65a1b">
        <updating>true</updating>
        <source>delete() updating;

begin
	delete root;
end;
</source>
    </JadeMethod>
    <JadeMethod name="hide" id="c9365182-5f2d-4826-9e89-3f9dea4ffea5">
        <updating>true</updating>
        <source>hide() updating;
// turns off the graph control

begin
	clear();
	
	tableControl.columnVisible[1]	:= false;
	frameControl.visible			:= false;
end;
</source>
    </JadeMethod>
    <JadeMethod name="initialise" id="507f36ba-fb87-4ca5-8383-4fe040921b73">
        <updating>true</updating>
        <source>initialise() updating;
// builds the controls ahead of populating

vars
	fixedRowHt		: Integer;
	
begin
	fixedRowHt	:= tableControl.rowHeight[1];
		
	// create a parent frame which the xaml will scroll through
	create frameControl transient;
	frameControl.parent		:= tableControl;
	frameControl.borderStyle		:= Frame.BorderStyle_None;
	frameControl.parentAspect		:= Frame.ParentAspect_StretchBottom;
	frameControl.caption			:= "";
	frameControl.boundaryWidth 		:= 0;
	frameControl.bevelInnerWidth	:= 0;
	frameControl.bevelOuterWidth	:= 0;	
	frameControl.transparent		:= true;
	frameControl.move( 0, fixedRowHt, 100, tableControl.height - fixedRowHt );	
	tableControl.form.addControl( frameControl );
	
	// now the xaml control
	create graphControl transient;
	graphControl.tabStop		:= false;
	graphControl.parent			:= frameControl;
	graphControl.borderStyle	:= JadeXamlControl.BorderStyle_None;
	graphControl.move( 0, 0, frameControl.width, frameControl.height );	
	graphControl.parentAspect	:= JadeXamlControl.ParentAspect_StretchBoth;
	graphControl.sizeRow		:= tableControl.rowHeight[1];
	graphControl.sizeCol		:= 15;
	graphControl.sizePoint		:= 8;
	graphControl.sizeLine		:= 1;
	graphControl.backColor		:= tableControl.backColor;
	tableControl.form.addControl( graphControl );
end;
</source>
    </JadeMethod>
    <JadeMethod name="scrolled" id="5e2caa1d-1995-48a2-8532-e6522c9bc872">
        <updating>true</updating>
        <source>scrolled(topRow : Integer) updating;
// repositions the xaml control based on the table scrolling
	
begin
	graphControl.scrolled(topRow);
end;
</source>
        <Parameter name="topRow">
            <type name="Integer"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="show" id="b9f144b0-7308-45d2-b12d-8293be573fab">
        <updating>true</updating>
        <source>show( commits : CommitArray ) updating;
// builds a new graph based on the param commits
	
begin
	clear();
	
	tableControl.columnVisible[1]	:= true;
	frameControl.visible			:= true;
	
	root.build( commits, maxCommits, maxColumns, colours );

	controlWidth		:= graphControl.show( maxColumns );
	frameControl.width	:= controlWidth;
	
	scrolled(0);
end;
</source>
        <Parameter name="commits">
            <type name="CommitArray"/>
        </Parameter>
    </JadeMethod>
</Class>
