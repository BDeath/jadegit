<GUIClass name="SetupBranchDialog" id="0d4023ba-3cc4-42f9-b515-5ec7d7b46782">
    <superclass name="RepositoryDialog"/>
    <transient>true</transient>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <ImplicitInverseRef name="branch" id="e40404a1-7bed-418d-81eb-1ccdade55e88">
        <embedded>true</embedded>
        <type name="LocalBranch"/>
        <access>readonly</access>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="chkCheckout" id="50b5a9f8-ee65-439a-b424-fd4c15bab02e">
        <embedded>true</embedded>
        <type name="CheckBox"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="chkUpstream" id="babdbb9c-300b-4864-8cd0-dc9d84881ac2">
        <embedded>true</embedded>
        <type name="CheckBox"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="cmbSource" id="d177314c-1957-4fa8-b5cf-fff69c744dbe">
        <embedded>true</embedded>
        <type name="ComboBox"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="lblName" id="15ac4861-bce0-4e6d-90ae-133923be023f">
        <embedded>true</embedded>
        <type name="Label"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="lblSource" id="4f43bb4f-af8f-405f-8d59-d961a549e63e">
        <embedded>true</embedded>
        <type name="Label"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="txtName" id="81618b23-91af-435a-9fb2-b5cb5a671853">
        <embedded>true</embedded>
        <type name="TextBox"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="branches" id="fa30c9a8-5bd9-41b6-a759-88e6f036cc8c">
        <memberTypeInverse>true</memberTypeInverse>
        <type name="BranchList"/>
        <access>protected</access>
    </ImplicitInverseRef>
    <JadeMethod name="chkCheckout_enable" id="958feff1-827e-4d83-a1a3-89b252c92b4c">
        <controlMethod name="Control::enable"/>
        <methodInvocation>event</methodInvocation>
        <source>chkCheckout_enable(cntrl: CheckBox input) protected;

begin
	cntrl.enabled := repo.isActive();
	cntrl.value	  := cntrl.value and cntrl.enabled;
end;
</source>
        <access>protected</access>
        <Parameter name="cntrl">
            <usage>input</usage>
            <type name="CheckBox"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="chkUpstream_enable" id="6cd841b0-43df-4b2a-9216-55791bbbf611">
        <controlMethod name="Control::enable"/>
        <methodInvocation>event</methodInvocation>
        <source>chkUpstream_enable(cntrl: CheckBox input) protected;

begin
	cntrl.enabled := cmbSource.listObject.isKindOf(RemoteBranch);
	cntrl.value	  := cntrl.value and cntrl.enabled;
end;
</source>
        <access>protected</access>
        <Parameter name="cntrl">
            <usage>input</usage>
            <type name="CheckBox"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="cmbSource_change" id="6e826ced-ccd9-421a-b6e7-81e3e7716ac4">
        <controlMethod name="ComboBox::change"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>cmbSource_change(combobox: ComboBox input) updating;

begin
	chkUpstream.value := true;
	enableControls;
end;
</source>
        <Parameter name="combobox">
            <usage>input</usage>
            <type name="ComboBox"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="cmbSource_click" id="fabbc623-53ea-462d-bae2-8003f5d1d7be">
        <controlMethod name="ComboBox::click"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>cmbSource_click(combobox: ComboBox input) updating;

begin
	enableControls;
end;
</source>
        <Parameter name="combobox">
            <usage>input</usage>
            <type name="ComboBox"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="cmbSource_displayEntry" id="81c68b41-25be-4286-bdb8-103114bded97">
        <controlMethod name="ComboBox::displayEntry"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>cmbSource_displayEntry(combobox: ComboBox input; branch: Branch; lstIndex: Integer):String updating;

begin
	return branch.shorthand;
end;
</source>
        <Parameter name="combobox">
            <usage>input</usage>
            <type name="ComboBox"/>
        </Parameter>
        <Parameter name="branch">
            <type name="Branch"/>
        </Parameter>
        <Parameter name="lstIndex">
            <type name="Integer"/>
        </Parameter>
        <ReturnType>
            <type name="String"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="cmbSource_populate" id="f49d0790-41aa-4628-ad62-67b9fdb102e2">
        <controlMethod name="Control::populate"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>cmbSource_populate(cntrl: Control input; object: Object): Boolean updating, protected;

vars
	branch : Branch;
	commit : Commit;
	head   : Reference_;
	
begin
	branches.populate(repo, Branch.Type_All);
	cmbSource.listCollection(branches, false, null);
	
	branch := object.asKindOf(Branch).Branch;
	if branch &lt;&gt; null then
		cmbSource.text := branch.shorthand;
		return true;
	endif;
	
	commit := object.asKindOf(Commit).Commit;
	if commit &lt;&gt; null then
		cmbSource.text := commit.id;
		return true;
	endif;
	
	head := repo.head();
	if head &lt;&gt; null then
		if head.name = "HEAD" then
			cmbSource.text := head.target;
		else
			cmbSource.text := head.shorthand;
		endif;
	endif;
	
	return true;
	
epilog
	delete head;
end;
</source>
        <access>protected</access>
        <Parameter name="cntrl">
            <usage>input</usage>
            <type name="Control"/>
        </Parameter>
        <Parameter name="object">
            <type name="Object"/>
        </Parameter>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="doOK" id="9b1a6913-168a-406b-bb4c-2bdfd70264af">
        <updating>true</updating>
        <source>doOK(): Boolean updating, protected;

vars
	commit : Commit;
	
begin	
	if cmbSource.listObject &lt;&gt; null then
		branch := cmbSource.listObject.Branch.branch(txtName.text.trimBlanks, chkUpstream.enabled and chkUpstream.value);
	else
		commit := Commit@lookup(repo, cmbSource.text.trimBlanks);
		if commit = null then
			app.msgBox("Invalid commit", "Error", MsgBox_OK_Only + MsgBox_Stop_Icon);
			return false;
		endif;
		branch := commit.branch(txtName.text.trimBlanks);
	endif;
	
	return true;
end;

</source>
        <access>protected</access>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="isOK" id="58aa7056-f21b-4ac7-90ab-3786bfa6b050">
        <source>isOK(): Boolean protected;

begin
	return txtName.text.trimBlanks &lt;&gt; null and
		   cmbSource.text &lt;&gt; null;
end;
</source>
        <access>protected</access>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="txtName_firstChange" id="c420b2b7-6177-4669-9226-0948e725528c">
        <controlMethod name="TextBox::firstChange"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>txtName_firstChange(textbox: TextBox input) updating;

begin
	enableControls;
end;
</source>
        <Parameter name="textbox">
            <usage>input</usage>
            <type name="TextBox"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="showModal" id="ed8bf49b-1761-4535-a719-a345ef368f5c">
        <updating>true</updating>
        <executionLocation>client</executionLocation>
        <source>showModal(): Integer updating, clientExecution;

vars
	result : Integer;
	
begin
	result := inheritMethod();
	
	if result.Boolean and chkCheckout.value then
		app.activeForm.execute(branch.switch());
	endif;
	
	return result;
end;
</source>
        <ReturnType>
            <type name="Integer"/>
        </ReturnType>
    </JadeMethod>
</GUIClass>
