<GUIClass name="InlineDiffControl" id="1ed3ef6a-6ba0-4128-8128-0c236e6eaf86">
    <superclass name="DiffControl"/>
    <transient>true</transient>
    <sharedTransientAllowed>true</sharedTransientAllowed>
    <subclassSharedTransientAllowed>true</subclassSharedTransientAllowed>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <text>Shows a single textbox with adds/removes following each other.</text>
    <DbClassMap file="_usergui"/>
    <Constant name="MarkerChg" id="0b357656-bee4-4b49-b0cb-8c1f52e93c95">
        <source>4</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="MarkerLineChg" id="118193c2-c8be-43a9-8a85-479b72cc3a84">
        <source>2</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="MarkerLineOld" id="5b3577f0-f3a9-4cf9-b130-4411fad82415">
        <source>1</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="MarkerOld" id="a32b58de-150f-4948-b5a7-9ad1bf7b4ae2">
        <source>3</source>
        <type name="Integer"/>
    </Constant>
    <ImplicitInverseRef name="text" id="8d8a4690-eefa-46b2-9c5a-4263cc485e63">
        <embedded>true</embedded>
        <type name="JadeTextEdit"/>
        <access>protected</access>
        <text>Main text control showing prior and new content.</text>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="formatMerged" id="d55c4dee-cc9a-47b3-94a0-a43bceddf661">
        <embedded>true</embedded>
        <type name="DiffFormat"/>
        <access>protected</access>
        <text>Stores the 'combined' line info</text>
    </ImplicitInverseRef>
    <JadeMethod name="formatLines" id="b8579840-6d8a-416d-8299-4ed9895ab9e9">
        <updating>true</updating>
        <source>formatLines() updating, protected;
// build up the formatting rules we will then apply to the text control

vars
	lineOld		: Integer;
	lineNew		: Integer;
	inx			: Integer;
	newSidePad	: Integer;
	oldSidePad	: Integer;
	fmtOld		: Character;
	fmtNew		: Character;
	
begin
	delete formatMerged;

	foreach inx in 1 to linesNew.size do
	
		lineOld	:= linesOld[inx];
		lineNew	:= linesNew[inx];
		
		if lineOld &gt; 0 and lineNew &gt; 0 then
			// lines should be the same
			lineOld	+= oldSidePad;
			lineNew += newSidePad;
			if formatOld.lineText[lineOld] &lt;&gt; formatNew.lineText[lineNew] then
				formatOld.lineFormat[lineOld]		:= DiffFormat.LineChanged;
				formatNew.lineFormat[lineNew]		:= DiffFormat.LineChanged;
			endif;
		
		elseif lineOld = -1 then
			// have to pad the old side
			lineNew += newSidePad;
			formatOld.linePad( lineNew );
			formatNew.lineFormat[ lineNew ]	:= DiffFormat.LineChanged;
			oldSidePad	+= 1;
		
		elseif lineNew = -1 then
			// have to pad the new side
			lineOld	+= oldSidePad;
			formatNew.linePad( lineOld );
			formatOld.lineFormat[ lineOld ]	:= DiffFormat.LineChanged;
			newSidePad += 1;
		
		endif;
	endforeach;

	// ensure same #lines either side
	inx		:= formatNew.lineFormat.size;
	while formatOld.lineFormat.size &lt; inx do
		formatOld.linePad( inx );
	endwhile;
	inx		:= formatOld.lineFormat.size;
	while formatNew.lineFormat.size &lt; inx do
		formatNew.linePad( inx );
	endwhile;

	formatMerged	:= create DiffFormat( not rawView) transient;
	foreach inx in 1 to formatOld.lineFormat.size do
	
		fmtOld	:= formatOld.lineFormat[inx];
		fmtNew	:= formatNew.lineFormat[inx];
		
		if fmtOld = DiffFormat.LineNormal then
			formatMerged.lineAppend( inx, formatOld.lineText[inx]);
		
		elseif fmtOld = DiffFormat.LinePad then
			formatMerged.lineAppend( inx, formatNew.lineText[inx]);
			formatMerged.lineFormat[inx]	:= DiffFormat.LineChanged;
			
		elseif fmtNew = DiffFormat.LinePad then
			formatMerged.lineAppend( inx, formatOld.lineText[inx]);
			formatMerged.lineFormat[inx]	:= DiffFormat.LineOld;
				
		endif;
	endforeach;
end;
</source>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="outlineBuild" id="9ea66501-9bb3-4166-b59f-94110c9e9320">
        <updating>true</updating>
        <source>outlineBuild() updating, protected;
// move the outline control to be on the right edge

begin
	inheritMethod();
	
	outline.move( width - outline.width - OutlinePad, OutlinePadScroll, outline.width, height - OutlinePadScroll*2); 
	outline.parentAspect	:= ParentAspect_AnchorRight + ParentAspect_StretchBottom;
end;
</source>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="outlinePadding" id="47f7114b-f7df-490f-8f52-ebefac872fb8">
        <source>outlinePadding(): Boolean protected;
// inline view doesnt have any reason to pad

begin
	return false;
end;
</source>
        <access>protected</access>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="textMain" id="278551c0-18ea-4b3a-bc3f-9b423b02d9ae">
        <source>textMain(): JadeTextEdit protected;

begin
	return text;
end;
</source>
        <access>protected</access>
        <ReturnType>
            <type name="JadeTextEdit"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="delete" id="d4933a45-6a86-465e-bd8d-bfcbd0031849">
        <updating>true</updating>
        <executionLocation>client</executionLocation>
        <source>delete() updating, clientExecution;

vars

begin
	delete formatMerged;
end;
</source>
    </JadeMethod>
    <JadeMethod name="textPopulate" id="995bf1bf-47fb-4880-8186-568eb1f6a534">
        <updating>true</updating>
        <source>textPopulate() updating, protected;
// populate the textbox with text and formatting

vars
	lineNo						: Integer;
	linesChgOld, linesChgNew	: IntegerArray;
	fmt							: Character;
	
begin
	inheritMethod();
	
	text.text	:= formatMerged.text;
	
	if not rawView then
		textHighlightKeys( text );
	endif;

	create linesChgOld transient;
	create linesChgNew transient;
	
	foreach lineNo in 1 to formatMerged.lineFormat.size do
		fmt	:= formatMerged.lineFormat[lineNo];
		
		if fmt = DiffFormat.LineChanged then
			linesChgNew.add(lineNo);
		elseif fmt = DiffFormat.LineOld then
			linesChgOld.add(lineNo);
		endif;
	endforeach;

	// textedit control will crash jade if a limit is exceeded, so stay under the threshold
	trimArray( linesChgOld, LineMarkLinesMaxSize );
	trimArray( linesChgNew, LineMarkLinesMaxSize );
	
	text.setLinemarkLines( MarkerLineOld, true, true, linesChgOld );
	text.setLinemarkLines( MarkerLineChg, true, true, linesChgNew );
	text.setLinemarkLines( MarkerOld, true, true, linesChgOld );
	text.setLinemarkLines( MarkerChg, true, true, linesChgNew );
	
epilog
	delete linesChgOld;
	delete linesChgNew;
end;
</source>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="textBuild" id="a16f803e-2c84-4493-95d1-f0a4652ba6dc">
        <updating>true</updating>
        <source>textBuild() updating, protected;
// build the shared diff textbox
vars

begin
	if text &lt;&gt; null then
		delete text;
	endif;
	
	text	:= textCreate();
	text.move( 0, 0, outline.left - OutlinePad, height );
	text.parentAspect	:= ParentAspect_StretchBoth;
			
	text.setLinemarkAttributes( MarkerLineOld, JadeTextEdit.SC_MARK_BACKGROUND, ColourForeDefault, ColourBackRemove );
	text.setLinemarkAttributes( MarkerLineChg, JadeTextEdit.SC_MARK_BACKGROUND, ColourForeDefault, ColourBackAdd );
	text.setLinemarkAttributes( MarkerOld, JadeTextEdit.SC_MARK_CIRCLEMINUS, ColourBackRemove, ColourForeRemove );
	text.setLinemarkAttributes( MarkerChg, JadeTextEdit.SC_MARK_CIRCLEPLUS, ColourBackAdd, ColourForeAdd );
end;
</source>
        <access>protected</access>
    </JadeMethod>
    <DevControlTypes name="InlineDiffControl">
        <hideFromPainterControlPalette>true</hideFromPainterControlPalette>
        <windowClass>AlBaseControl</windowClass>
        <DevControlProperties name="backBrush">
            <cntrlType>picture</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="backBrushStyle">
            <cntrlType>list</cntrlType>
            <optionsList>
                <value>0 - tile</value>
                <value>1 - stretch</value>
                <value>2 - center</value>
                <value>3 - stretch proportional</value>
            </optionsList>
        </DevControlProperties>
        <DevControlProperties name="backColor">
            <cntrlType>color</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="borderColorSingle">
            <cntrlType>color</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="borderStyle">
            <cntrlType>list</cntrlType>
            <optionsList>
                <value>0 - None</value>
                <value>1 - Single</value>
                <value>2 - 3D sunken</value>
                <value>3 - 3D raised</value>
            </optionsList>
        </DevControlProperties>
        <DevControlProperties name="bubbleHelp">
            <cntrlType>multilined-string</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="clipControls">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="description">
            <cntrlType>multilined-string</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="dragCursor">
            <cntrlType>picture</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="enabled">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="focusBackColor">
            <cntrlType>color</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="focusForeColor">
            <cntrlType>color</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="fontBold">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="fontItalic">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="fontName">
            <cntrlType>font</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="fontSize">
            <cntrlType>font-size</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="fontStrikethru">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="fontUnderline">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="foreColor">
            <cntrlType>color</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="height">
            <cntrlType>signed-integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="helpContextId">
            <cntrlType>integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="helpKeyword">
            <cntrlType>string</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="ignoreSkin">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="left">
            <cntrlType>signed-integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="mouseCursor">
            <cntrlType>picture</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="mousePointer">
            <cntrlType>list</cntrlType>
            <optionsList>
                <value> 0 - Default</value>
                <value> 1 - Arrow</value>
                <value> 2 - Cross</value>
                <value> 3 - I-Beam</value>
                <value> 4 - Cursor</value>
                <value> 5 - Size</value>
                <value> 6 - Size NE SW</value>
                <value> 7 - Size N S</value>
                <value> 8 - Size NW SE</value>
                <value> 9 - Size W E</value>
                <value>10 - Up Arrow</value>
                <value>11 - HourGlass</value>
                <value>12 - No Drop</value>
                <value>13 - Drag</value>
                <value>14 - Horizontal line move</value>
                <value>15 - Vertical Line move</value>
                <value>16 - Hand Pointing</value>
            </optionsList>
        </DevControlProperties>
        <DevControlProperties name="name">
            <cntrlType>name</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="parent">
            <cntrlType>parent</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="parentAspect">
            <cntrlType>list</cntrlType>
            <optionsList>
                <value> 0 - None</value>
                <value> 1 - Stretch Right</value>
                <value> 2 - Stretch Bottom</value>
                <value> 3 - Stretch Both</value>
                <value> 4 - Anchor Right</value>
                <value> 6 - Anchor Right &amp; Stretch Bottom</value>
                <value> 8 - Anchor Bottom</value>
                <value> 9 - Anchor Bottom &amp; Stretch Right</value>
                <value>12 - Anchor Both</value>
                <value>16 - Center Horz</value>
                <value>18 - Center Horz &amp; Stretch Bottom</value>
                <value>24 - Center Horz &amp; Anchor bottom</value>
                <value>32 - Center Vert</value>
                <value>33 - Center Vert &amp; Stretch Right</value>
                <value>36 - Center Vert &amp; Anchor Right</value>
                <value>48 - Center Both</value>
            </optionsList>
        </DevControlProperties>
        <DevControlProperties name="parentBottomOffset">
            <cntrlType>signed-integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="parentRightOffset">
            <cntrlType>signed-integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="relativeHeight">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="relativeLeft">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="relativeTop">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="relativeWidth">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="scrollBars">
            <cntrlType>list</cntrlType>
            <optionsList>
                <value>0 - None</value>
                <value>1 - Horizontal</value>
                <value>2 - Vertical</value>
                <value>3 - Both</value>
            </optionsList>
        </DevControlProperties>
        <DevControlProperties name="securityLevelEnabled">
            <cntrlType>integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="securityLevelVisible">
            <cntrlType>integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="show3D">
            <cntrlType>list</cntrlType>
            <optionsList>
                <value>0 - use application default</value>
                <value>1 - Not 3D</value>
                <value>2 - Use 3D</value>
                <value>3 - Use borderStyle</value>
            </optionsList>
        </DevControlProperties>
        <DevControlProperties name="skinCategoryName">
            <cntrlType>string</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="tabIndex">
            <cntrlType>integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="tabStop">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="tag">
            <cntrlType>string</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="top">
            <cntrlType>signed-integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="transparent">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="visible">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="width">
            <cntrlType>signed-integer</cntrlType>
        </DevControlProperties>
    </DevControlTypes>
</GUIClass>
